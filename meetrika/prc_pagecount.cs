/*
               File: PRC_PageCount
        Description: Quantidade de paginas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:9.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_pagecount : GXProcedure
   {
      public prc_pagecount( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_pagecount( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( long aP0_Registros ,
                           ref short aP1_PreView ,
                           out long aP2_PageCount )
      {
         this.AV9Registros = aP0_Registros;
         this.AV10PreView = aP1_PreView;
         this.AV8PageCount = 0 ;
         initialize();
         executePrivate();
         aP1_PreView=this.AV10PreView;
         aP2_PageCount=this.AV8PageCount;
      }

      public long executeUdp( long aP0_Registros ,
                              ref short aP1_PreView )
      {
         this.AV9Registros = aP0_Registros;
         this.AV10PreView = aP1_PreView;
         this.AV8PageCount = 0 ;
         initialize();
         executePrivate();
         aP1_PreView=this.AV10PreView;
         aP2_PageCount=this.AV8PageCount;
         return AV8PageCount ;
      }

      public void executeSubmit( long aP0_Registros ,
                                 ref short aP1_PreView ,
                                 out long aP2_PageCount )
      {
         prc_pagecount objprc_pagecount;
         objprc_pagecount = new prc_pagecount();
         objprc_pagecount.AV9Registros = aP0_Registros;
         objprc_pagecount.AV10PreView = aP1_PreView;
         objprc_pagecount.AV8PageCount = 0 ;
         objprc_pagecount.context.SetSubmitInitialConfig(context);
         objprc_pagecount.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_pagecount);
         aP1_PreView=this.AV10PreView;
         aP2_PageCount=this.AV8PageCount;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_pagecount)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV10PreView == 0 )
         {
            AV8PageCount = 1;
            this.cleanup();
            if (true) return;
         }
         AV8PageCount = (long)(AV9Registros/ (decimal)(AV10PreView));
         if ( ( AV9Registros > 0 ) && ( AV9Registros /  ( decimal )( AV10PreView ) > Convert.ToDecimal( NumberUtil.Int( (long)(AV9Registros/ (decimal)(AV10PreView))) )) )
         {
            AV8PageCount = (long)(AV8PageCount+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10PreView ;
      private long AV9Registros ;
      private long AV8PageCount ;
      private short aP1_PreView ;
      private long aP2_PageCount ;
   }

}
