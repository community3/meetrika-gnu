/*
               File: ContagemResultadoRequisito_BC
        Description: Contagem Resultado Requisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:12:17.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadorequisito_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadorequisito_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadorequisito_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4Z221( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4Z221( ) ;
         standaloneModal( ) ;
         AddRow4Z221( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z2005ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4Z0( )
      {
         BeforeValidate4Z221( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4Z221( ) ;
            }
            else
            {
               CheckExtendedTable4Z221( ) ;
               if ( AnyError == 0 )
               {
                  ZM4Z221( 3) ;
                  ZM4Z221( 4) ;
               }
               CloseExtendedTableCursors4Z221( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4Z221( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z2006ContagemResultadoRequisito_Owner = A2006ContagemResultadoRequisito_Owner;
            Z2003ContagemResultadoRequisito_OSCod = A2003ContagemResultadoRequisito_OSCod;
            Z2004ContagemResultadoRequisito_ReqCod = A2004ContagemResultadoRequisito_ReqCod;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -2 )
         {
            Z2005ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
            Z2006ContagemResultadoRequisito_Owner = A2006ContagemResultadoRequisito_Owner;
            Z2003ContagemResultadoRequisito_OSCod = A2003ContagemResultadoRequisito_OSCod;
            Z2004ContagemResultadoRequisito_ReqCod = A2004ContagemResultadoRequisito_ReqCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2006ContagemResultadoRequisito_Owner) && ( Gx_BScreen == 0 ) )
         {
            A2006ContagemResultadoRequisito_Owner = false;
         }
      }

      protected void Load4Z221( )
      {
         /* Using cursor BC004Z6 */
         pr_default.execute(4, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound221 = 1;
            A2006ContagemResultadoRequisito_Owner = BC004Z6_A2006ContagemResultadoRequisito_Owner[0];
            A2003ContagemResultadoRequisito_OSCod = BC004Z6_A2003ContagemResultadoRequisito_OSCod[0];
            A2004ContagemResultadoRequisito_ReqCod = BC004Z6_A2004ContagemResultadoRequisito_ReqCod[0];
            ZM4Z221( -2) ;
         }
         pr_default.close(4);
         OnLoadActions4Z221( ) ;
      }

      protected void OnLoadActions4Z221( )
      {
      }

      protected void CheckExtendedTable4Z221( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004Z4 */
         pr_default.execute(2, new Object[] {A2003ContagemResultadoRequisito_OSCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_OSCOD");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004Z5 */
         pr_default.execute(3, new Object[] {A2004ContagemResultadoRequisito_ReqCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Requisito_Req Cod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOREQUISITO_REQCOD");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4Z221( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4Z221( )
      {
         /* Using cursor BC004Z7 */
         pr_default.execute(5, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound221 = 1;
         }
         else
         {
            RcdFound221 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004Z3 */
         pr_default.execute(1, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4Z221( 2) ;
            RcdFound221 = 1;
            A2005ContagemResultadoRequisito_Codigo = BC004Z3_A2005ContagemResultadoRequisito_Codigo[0];
            A2006ContagemResultadoRequisito_Owner = BC004Z3_A2006ContagemResultadoRequisito_Owner[0];
            A2003ContagemResultadoRequisito_OSCod = BC004Z3_A2003ContagemResultadoRequisito_OSCod[0];
            A2004ContagemResultadoRequisito_ReqCod = BC004Z3_A2004ContagemResultadoRequisito_ReqCod[0];
            Z2005ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
            sMode221 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4Z221( ) ;
            if ( AnyError == 1 )
            {
               RcdFound221 = 0;
               InitializeNonKey4Z221( ) ;
            }
            Gx_mode = sMode221;
         }
         else
         {
            RcdFound221 = 0;
            InitializeNonKey4Z221( ) ;
            sMode221 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode221;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4Z0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4Z221( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004Z2 */
            pr_default.execute(0, new Object[] {A2005ContagemResultadoRequisito_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoRequisito"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2006ContagemResultadoRequisito_Owner != BC004Z2_A2006ContagemResultadoRequisito_Owner[0] ) || ( Z2003ContagemResultadoRequisito_OSCod != BC004Z2_A2003ContagemResultadoRequisito_OSCod[0] ) || ( Z2004ContagemResultadoRequisito_ReqCod != BC004Z2_A2004ContagemResultadoRequisito_ReqCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoRequisito"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4Z221( )
      {
         BeforeValidate4Z221( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4Z221( 0) ;
            CheckOptimisticConcurrency4Z221( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Z221( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4Z221( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004Z8 */
                     pr_default.execute(6, new Object[] {A2006ContagemResultadoRequisito_Owner, A2003ContagemResultadoRequisito_OSCod, A2004ContagemResultadoRequisito_ReqCod});
                     A2005ContagemResultadoRequisito_Codigo = BC004Z8_A2005ContagemResultadoRequisito_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4Z221( ) ;
            }
            EndLevel4Z221( ) ;
         }
         CloseExtendedTableCursors4Z221( ) ;
      }

      protected void Update4Z221( )
      {
         BeforeValidate4Z221( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Z221( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Z221( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4Z221( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004Z9 */
                     pr_default.execute(7, new Object[] {A2006ContagemResultadoRequisito_Owner, A2003ContagemResultadoRequisito_OSCod, A2004ContagemResultadoRequisito_ReqCod, A2005ContagemResultadoRequisito_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoRequisito"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4Z221( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4Z221( ) ;
         }
         CloseExtendedTableCursors4Z221( ) ;
      }

      protected void DeferredUpdate4Z221( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4Z221( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4Z221( ) ;
            AfterConfirm4Z221( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4Z221( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004Z10 */
                  pr_default.execute(8, new Object[] {A2005ContagemResultadoRequisito_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoRequisito") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode221 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4Z221( ) ;
         Gx_mode = sMode221;
      }

      protected void OnDeleteControls4Z221( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel4Z221( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4Z221( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4Z221( )
      {
         /* Using cursor BC004Z11 */
         pr_default.execute(9, new Object[] {A2005ContagemResultadoRequisito_Codigo});
         RcdFound221 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound221 = 1;
            A2005ContagemResultadoRequisito_Codigo = BC004Z11_A2005ContagemResultadoRequisito_Codigo[0];
            A2006ContagemResultadoRequisito_Owner = BC004Z11_A2006ContagemResultadoRequisito_Owner[0];
            A2003ContagemResultadoRequisito_OSCod = BC004Z11_A2003ContagemResultadoRequisito_OSCod[0];
            A2004ContagemResultadoRequisito_ReqCod = BC004Z11_A2004ContagemResultadoRequisito_ReqCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4Z221( )
      {
         /* Scan next routine */
         pr_default.readNext(9);
         RcdFound221 = 0;
         ScanKeyLoad4Z221( ) ;
      }

      protected void ScanKeyLoad4Z221( )
      {
         sMode221 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound221 = 1;
            A2005ContagemResultadoRequisito_Codigo = BC004Z11_A2005ContagemResultadoRequisito_Codigo[0];
            A2006ContagemResultadoRequisito_Owner = BC004Z11_A2006ContagemResultadoRequisito_Owner[0];
            A2003ContagemResultadoRequisito_OSCod = BC004Z11_A2003ContagemResultadoRequisito_OSCod[0];
            A2004ContagemResultadoRequisito_ReqCod = BC004Z11_A2004ContagemResultadoRequisito_ReqCod[0];
         }
         Gx_mode = sMode221;
      }

      protected void ScanKeyEnd4Z221( )
      {
         pr_default.close(9);
      }

      protected void AfterConfirm4Z221( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4Z221( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4Z221( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4Z221( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4Z221( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4Z221( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4Z221( )
      {
      }

      protected void AddRow4Z221( )
      {
         VarsToRow221( bcContagemResultadoRequisito) ;
      }

      protected void ReadRow4Z221( )
      {
         RowToVars221( bcContagemResultadoRequisito, 1) ;
      }

      protected void InitializeNonKey4Z221( )
      {
         A2003ContagemResultadoRequisito_OSCod = 0;
         A2004ContagemResultadoRequisito_ReqCod = 0;
         A2006ContagemResultadoRequisito_Owner = false;
         Z2006ContagemResultadoRequisito_Owner = false;
         Z2003ContagemResultadoRequisito_OSCod = 0;
         Z2004ContagemResultadoRequisito_ReqCod = 0;
      }

      protected void InitAll4Z221( )
      {
         A2005ContagemResultadoRequisito_Codigo = 0;
         InitializeNonKey4Z221( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2006ContagemResultadoRequisito_Owner = i2006ContagemResultadoRequisito_Owner;
      }

      public void VarsToRow221( SdtContagemResultadoRequisito obj221 )
      {
         obj221.gxTpr_Mode = Gx_mode;
         obj221.gxTpr_Contagemresultadorequisito_oscod = A2003ContagemResultadoRequisito_OSCod;
         obj221.gxTpr_Contagemresultadorequisito_reqcod = A2004ContagemResultadoRequisito_ReqCod;
         obj221.gxTpr_Contagemresultadorequisito_owner = A2006ContagemResultadoRequisito_Owner;
         obj221.gxTpr_Contagemresultadorequisito_codigo = A2005ContagemResultadoRequisito_Codigo;
         obj221.gxTpr_Contagemresultadorequisito_codigo_Z = Z2005ContagemResultadoRequisito_Codigo;
         obj221.gxTpr_Contagemresultadorequisito_oscod_Z = Z2003ContagemResultadoRequisito_OSCod;
         obj221.gxTpr_Contagemresultadorequisito_reqcod_Z = Z2004ContagemResultadoRequisito_ReqCod;
         obj221.gxTpr_Contagemresultadorequisito_owner_Z = Z2006ContagemResultadoRequisito_Owner;
         obj221.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow221( SdtContagemResultadoRequisito obj221 )
      {
         obj221.gxTpr_Contagemresultadorequisito_codigo = A2005ContagemResultadoRequisito_Codigo;
         return  ;
      }

      public void RowToVars221( SdtContagemResultadoRequisito obj221 ,
                                int forceLoad )
      {
         Gx_mode = obj221.gxTpr_Mode;
         A2003ContagemResultadoRequisito_OSCod = obj221.gxTpr_Contagemresultadorequisito_oscod;
         A2004ContagemResultadoRequisito_ReqCod = obj221.gxTpr_Contagemresultadorequisito_reqcod;
         A2006ContagemResultadoRequisito_Owner = obj221.gxTpr_Contagemresultadorequisito_owner;
         A2005ContagemResultadoRequisito_Codigo = obj221.gxTpr_Contagemresultadorequisito_codigo;
         Z2005ContagemResultadoRequisito_Codigo = obj221.gxTpr_Contagemresultadorequisito_codigo_Z;
         Z2003ContagemResultadoRequisito_OSCod = obj221.gxTpr_Contagemresultadorequisito_oscod_Z;
         Z2004ContagemResultadoRequisito_ReqCod = obj221.gxTpr_Contagemresultadorequisito_reqcod_Z;
         Z2006ContagemResultadoRequisito_Owner = obj221.gxTpr_Contagemresultadorequisito_owner_Z;
         Gx_mode = obj221.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A2005ContagemResultadoRequisito_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4Z221( ) ;
         ScanKeyStart4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z2005ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
         }
         ZM4Z221( -2) ;
         OnLoadActions4Z221( ) ;
         AddRow4Z221( ) ;
         ScanKeyEnd4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars221( bcContagemResultadoRequisito, 0) ;
         ScanKeyStart4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z2005ContagemResultadoRequisito_Codigo = A2005ContagemResultadoRequisito_Codigo;
         }
         ZM4Z221( -2) ;
         OnLoadActions4Z221( ) ;
         AddRow4Z221( ) ;
         ScanKeyEnd4Z221( ) ;
         if ( RcdFound221 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars221( bcContagemResultadoRequisito, 0) ;
         nKeyPressed = 1;
         GetKey4Z221( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4Z221( ) ;
         }
         else
         {
            if ( RcdFound221 == 1 )
            {
               if ( A2005ContagemResultadoRequisito_Codigo != Z2005ContagemResultadoRequisito_Codigo )
               {
                  A2005ContagemResultadoRequisito_Codigo = Z2005ContagemResultadoRequisito_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4Z221( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A2005ContagemResultadoRequisito_Codigo != Z2005ContagemResultadoRequisito_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4Z221( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4Z221( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow221( bcContagemResultadoRequisito) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars221( bcContagemResultadoRequisito, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4Z221( ) ;
         if ( RcdFound221 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A2005ContagemResultadoRequisito_Codigo != Z2005ContagemResultadoRequisito_Codigo )
            {
               A2005ContagemResultadoRequisito_Codigo = Z2005ContagemResultadoRequisito_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A2005ContagemResultadoRequisito_Codigo != Z2005ContagemResultadoRequisito_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "ContagemResultadoRequisito_BC");
         VarsToRow221( bcContagemResultadoRequisito) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoRequisito.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoRequisito.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoRequisito )
         {
            bcContagemResultadoRequisito = (SdtContagemResultadoRequisito)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoRequisito.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoRequisito.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow221( bcContagemResultadoRequisito) ;
            }
            else
            {
               RowToVars221( bcContagemResultadoRequisito, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoRequisito.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoRequisito.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars221( bcContagemResultadoRequisito, 1) ;
         return  ;
      }

      public SdtContagemResultadoRequisito ContagemResultadoRequisito_BC
      {
         get {
            return bcContagemResultadoRequisito ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC004Z6_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004Z6_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         BC004Z6_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         BC004Z6_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         BC004Z4_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         BC004Z5_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         BC004Z7_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004Z3_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004Z3_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         BC004Z3_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         BC004Z3_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         sMode221 = "";
         BC004Z2_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004Z2_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         BC004Z2_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         BC004Z2_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         BC004Z8_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004Z11_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         BC004Z11_A2006ContagemResultadoRequisito_Owner = new bool[] {false} ;
         BC004Z11_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         BC004Z11_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadorequisito_bc__default(),
            new Object[][] {
                new Object[] {
               BC004Z2_A2005ContagemResultadoRequisito_Codigo, BC004Z2_A2006ContagemResultadoRequisito_Owner, BC004Z2_A2003ContagemResultadoRequisito_OSCod, BC004Z2_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               BC004Z3_A2005ContagemResultadoRequisito_Codigo, BC004Z3_A2006ContagemResultadoRequisito_Owner, BC004Z3_A2003ContagemResultadoRequisito_OSCod, BC004Z3_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               BC004Z4_A2003ContagemResultadoRequisito_OSCod
               }
               , new Object[] {
               BC004Z5_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               BC004Z6_A2005ContagemResultadoRequisito_Codigo, BC004Z6_A2006ContagemResultadoRequisito_Owner, BC004Z6_A2003ContagemResultadoRequisito_OSCod, BC004Z6_A2004ContagemResultadoRequisito_ReqCod
               }
               , new Object[] {
               BC004Z7_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               BC004Z8_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004Z11_A2005ContagemResultadoRequisito_Codigo, BC004Z11_A2006ContagemResultadoRequisito_Owner, BC004Z11_A2003ContagemResultadoRequisito_OSCod, BC004Z11_A2004ContagemResultadoRequisito_ReqCod
               }
            }
         );
         Z2006ContagemResultadoRequisito_Owner = false;
         A2006ContagemResultadoRequisito_Owner = false;
         i2006ContagemResultadoRequisito_Owner = false;
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound221 ;
      private int trnEnded ;
      private int Z2005ContagemResultadoRequisito_Codigo ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private int Z2003ContagemResultadoRequisito_OSCod ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int Z2004ContagemResultadoRequisito_ReqCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode221 ;
      private bool Z2006ContagemResultadoRequisito_Owner ;
      private bool A2006ContagemResultadoRequisito_Owner ;
      private bool i2006ContagemResultadoRequisito_Owner ;
      private SdtContagemResultadoRequisito bcContagemResultadoRequisito ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004Z6_A2005ContagemResultadoRequisito_Codigo ;
      private bool[] BC004Z6_A2006ContagemResultadoRequisito_Owner ;
      private int[] BC004Z6_A2003ContagemResultadoRequisito_OSCod ;
      private int[] BC004Z6_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] BC004Z4_A2003ContagemResultadoRequisito_OSCod ;
      private int[] BC004Z5_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] BC004Z7_A2005ContagemResultadoRequisito_Codigo ;
      private int[] BC004Z3_A2005ContagemResultadoRequisito_Codigo ;
      private bool[] BC004Z3_A2006ContagemResultadoRequisito_Owner ;
      private int[] BC004Z3_A2003ContagemResultadoRequisito_OSCod ;
      private int[] BC004Z3_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] BC004Z2_A2005ContagemResultadoRequisito_Codigo ;
      private bool[] BC004Z2_A2006ContagemResultadoRequisito_Owner ;
      private int[] BC004Z2_A2003ContagemResultadoRequisito_OSCod ;
      private int[] BC004Z2_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] BC004Z8_A2005ContagemResultadoRequisito_Codigo ;
      private int[] BC004Z11_A2005ContagemResultadoRequisito_Codigo ;
      private bool[] BC004Z11_A2006ContagemResultadoRequisito_Owner ;
      private int[] BC004Z11_A2003ContagemResultadoRequisito_OSCod ;
      private int[] BC004Z11_A2004ContagemResultadoRequisito_ReqCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class contagemresultadorequisito_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004Z6 ;
          prmBC004Z6 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z4 ;
          prmBC004Z4 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z5 ;
          prmBC004Z5 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z7 ;
          prmBC004Z7 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z3 ;
          prmBC004Z3 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z2 ;
          prmBC004Z2 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z8 ;
          prmBC004Z8 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Owner",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z9 ;
          prmBC004Z9 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Owner",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoRequisito_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z10 ;
          prmBC004Z10 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Z11 ;
          prmBC004Z11 = new Object[] {
          new Object[] {"@ContagemResultadoRequisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004Z2", "SELECT [ContagemResultadoRequisito_Codigo], [ContagemResultadoRequisito_Owner], [ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, [ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod FROM [ContagemResultadoRequisito] WITH (UPDLOCK) WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Z2,1,0,true,false )
             ,new CursorDef("BC004Z3", "SELECT [ContagemResultadoRequisito_Codigo], [ContagemResultadoRequisito_Owner], [ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, [ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Z3,1,0,true,false )
             ,new CursorDef("BC004Z4", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoRequisito_OSCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoRequisito_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Z4,1,0,true,false )
             ,new CursorDef("BC004Z5", "SELECT [Requisito_Codigo] AS ContagemResultadoRequisito_ReqCod FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @ContagemResultadoRequisito_ReqCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Z5,1,0,true,false )
             ,new CursorDef("BC004Z6", "SELECT TM1.[ContagemResultadoRequisito_Codigo], TM1.[ContagemResultadoRequisito_Owner], TM1.[ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, TM1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod FROM [ContagemResultadoRequisito] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo ORDER BY TM1.[ContagemResultadoRequisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Z6,100,0,true,false )
             ,new CursorDef("BC004Z7", "SELECT [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Z7,1,0,true,false )
             ,new CursorDef("BC004Z8", "INSERT INTO [ContagemResultadoRequisito]([ContagemResultadoRequisito_Owner], [ContagemResultadoRequisito_OSCod], [ContagemResultadoRequisito_ReqCod]) VALUES(@ContagemResultadoRequisito_Owner, @ContagemResultadoRequisito_OSCod, @ContagemResultadoRequisito_ReqCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC004Z8)
             ,new CursorDef("BC004Z9", "UPDATE [ContagemResultadoRequisito] SET [ContagemResultadoRequisito_Owner]=@ContagemResultadoRequisito_Owner, [ContagemResultadoRequisito_OSCod]=@ContagemResultadoRequisito_OSCod, [ContagemResultadoRequisito_ReqCod]=@ContagemResultadoRequisito_ReqCod  WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo", GxErrorMask.GX_NOMASK,prmBC004Z9)
             ,new CursorDef("BC004Z10", "DELETE FROM [ContagemResultadoRequisito]  WHERE [ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo", GxErrorMask.GX_NOMASK,prmBC004Z10)
             ,new CursorDef("BC004Z11", "SELECT TM1.[ContagemResultadoRequisito_Codigo], TM1.[ContagemResultadoRequisito_Owner], TM1.[ContagemResultadoRequisito_OSCod] AS ContagemResultadoRequisito_OSCod, TM1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod FROM [ContagemResultadoRequisito] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultadoRequisito_Codigo] = @ContagemResultadoRequisito_Codigo ORDER BY TM1.[ContagemResultadoRequisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Z11,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
