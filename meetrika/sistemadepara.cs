/*
               File: SistemaDePara
        Description: De Para
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:29.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemadepara : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A127Sistema_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbSistemaDePara_Origem.Name = "SISTEMADEPARA_ORIGEM";
         cmbSistemaDePara_Origem.WebTags = "";
         cmbSistemaDePara_Origem.addItem("R", "Redmine", 0);
         if ( cmbSistemaDePara_Origem.ItemCount > 0 )
         {
            A1461SistemaDePara_Origem = cmbSistemaDePara_Origem.getValidValue(A1461SistemaDePara_Origem);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1461SistemaDePara_Origem", A1461SistemaDePara_Origem);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "De Para", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSistema_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public sistemadepara( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public sistemadepara( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbSistemaDePara_Origem = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbSistemaDePara_Origem.ItemCount > 0 )
         {
            A1461SistemaDePara_Origem = cmbSistemaDePara_Origem.getValidValue(A1461SistemaDePara_Origem);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1461SistemaDePara_Origem", A1461SistemaDePara_Origem);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3R172( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3R172e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3R172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3R172( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3R172e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "De Para", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_SistemaDePara.htm");
            wb_table3_28_3R172( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_3R172e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3R172e( true) ;
         }
         else
         {
            wb_table1_2_3R172e( false) ;
         }
      }

      protected void wb_table3_28_3R172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3R172( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_3R172e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaDePara.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaDePara.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_3R172e( true) ;
         }
         else
         {
            wb_table3_28_3R172e( false) ;
         }
      }

      protected void wb_table4_34_3R172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_codigo_Internalname, "Sistema", "", "", lblTextblocksistema_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")), ((edtSistema_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSistema_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemadepara_codigo_Internalname, "De Para_Codigo", "", "", lblTextblocksistemadepara_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistemaDePara_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1460SistemaDePara_Codigo), 6, 0, ",", "")), ((edtSistemaDePara_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1460SistemaDePara_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1460SistemaDePara_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaDePara_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtSistemaDePara_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemadepara_origem_Internalname, "Origem", "", "", lblTextblocksistemadepara_origem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSistemaDePara_Origem, cmbSistemaDePara_Origem_Internalname, StringUtil.RTrim( A1461SistemaDePara_Origem), 1, cmbSistemaDePara_Origem_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbSistemaDePara_Origem.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_SistemaDePara.htm");
            cmbSistemaDePara_Origem.CurrentValue = StringUtil.RTrim( A1461SistemaDePara_Origem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistemaDePara_Origem_Internalname, "Values", (String)(cmbSistemaDePara_Origem.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemadepara_origemid_Internalname, "na Origem", "", "", lblTextblocksistemadepara_origemid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistemaDePara_OrigemId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1462SistemaDePara_OrigemId), 18, 0, ",", "")), ((edtSistemaDePara_OrigemId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1462SistemaDePara_OrigemId), "ZZZZZZZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1462SistemaDePara_OrigemId), "ZZZZZZZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaDePara_OrigemId_Jsonclick, 0, "Attribute", "", "", "", 1, edtSistemaDePara_OrigemId_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemadepara_origemdsc_Internalname, "Descri��o", "", "", lblTextblocksistemadepara_origemdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistemaDePara_OrigemDsc_Internalname, A1463SistemaDePara_OrigemDsc, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, 1, edtSistemaDePara_OrigemDsc_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_SistemaDePara.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3R172e( true) ;
         }
         else
         {
            wb_table4_34_3R172e( false) ;
         }
      }

      protected void wb_table2_5_3R172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaDePara.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3R172e( true) ;
         }
         else
         {
            wb_table2_5_3R172e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A127Sistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               }
               else
               {
                  A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistemaDePara_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistemaDePara_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMADEPARA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSistemaDePara_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1460SistemaDePara_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
               }
               else
               {
                  A1460SistemaDePara_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistemaDePara_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
               }
               cmbSistemaDePara_Origem.CurrentValue = cgiGet( cmbSistemaDePara_Origem_Internalname);
               A1461SistemaDePara_Origem = cgiGet( cmbSistemaDePara_Origem_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1461SistemaDePara_Origem", A1461SistemaDePara_Origem);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistemaDePara_OrigemId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistemaDePara_OrigemId_Internalname), ",", ".") > Convert.ToDecimal( 999999999999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMADEPARA_ORIGEMID");
                  AnyError = 1;
                  GX_FocusControl = edtSistemaDePara_OrigemId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1462SistemaDePara_OrigemId = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1462SistemaDePara_OrigemId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1462SistemaDePara_OrigemId), 18, 0)));
               }
               else
               {
                  A1462SistemaDePara_OrigemId = (long)(context.localUtil.CToN( cgiGet( edtSistemaDePara_OrigemId_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1462SistemaDePara_OrigemId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1462SistemaDePara_OrigemId), 18, 0)));
               }
               A1463SistemaDePara_OrigemDsc = cgiGet( edtSistemaDePara_OrigemDsc_Internalname);
               n1463SistemaDePara_OrigemDsc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1463SistemaDePara_OrigemDsc", A1463SistemaDePara_OrigemDsc);
               n1463SistemaDePara_OrigemDsc = (String.IsNullOrEmpty(StringUtil.RTrim( A1463SistemaDePara_OrigemDsc)) ? true : false);
               /* Read saved values. */
               Z127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z127Sistema_Codigo"), ",", "."));
               Z1460SistemaDePara_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1460SistemaDePara_Codigo"), ",", "."));
               Z1461SistemaDePara_Origem = cgiGet( "Z1461SistemaDePara_Origem");
               Z1462SistemaDePara_OrigemId = (long)(context.localUtil.CToN( cgiGet( "Z1462SistemaDePara_OrigemId"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  A1460SistemaDePara_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3R172( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3R172( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3R0( )
      {
      }

      protected void ZM3R172( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1461SistemaDePara_Origem = T003R3_A1461SistemaDePara_Origem[0];
               Z1462SistemaDePara_OrigemId = T003R3_A1462SistemaDePara_OrigemId[0];
            }
            else
            {
               Z1461SistemaDePara_Origem = A1461SistemaDePara_Origem;
               Z1462SistemaDePara_OrigemId = A1462SistemaDePara_OrigemId;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1460SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
            Z1461SistemaDePara_Origem = A1461SistemaDePara_Origem;
            Z1462SistemaDePara_OrigemId = A1462SistemaDePara_OrigemId;
            Z1463SistemaDePara_OrigemDsc = A1463SistemaDePara_OrigemDsc;
            Z127Sistema_Codigo = A127Sistema_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3R172( )
      {
         /* Using cursor T003R5 */
         pr_default.execute(3, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound172 = 1;
            A1461SistemaDePara_Origem = T003R5_A1461SistemaDePara_Origem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1461SistemaDePara_Origem", A1461SistemaDePara_Origem);
            A1462SistemaDePara_OrigemId = T003R5_A1462SistemaDePara_OrigemId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1462SistemaDePara_OrigemId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1462SistemaDePara_OrigemId), 18, 0)));
            A1463SistemaDePara_OrigemDsc = T003R5_A1463SistemaDePara_OrigemDsc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1463SistemaDePara_OrigemDsc", A1463SistemaDePara_OrigemDsc);
            n1463SistemaDePara_OrigemDsc = T003R5_n1463SistemaDePara_OrigemDsc[0];
            ZM3R172( -2) ;
         }
         pr_default.close(3);
         OnLoadActions3R172( ) ;
      }

      protected void OnLoadActions3R172( )
      {
      }

      protected void CheckExtendedTable3R172( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003R4 */
         pr_default.execute(2, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A1461SistemaDePara_Origem, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Origem fora do intervalo", "OutOfRange", 1, "SISTEMADEPARA_ORIGEM");
            AnyError = 1;
            GX_FocusControl = cmbSistemaDePara_Origem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3R172( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A127Sistema_Codigo )
      {
         /* Using cursor T003R6 */
         pr_default.execute(4, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3R172( )
      {
         /* Using cursor T003R7 */
         pr_default.execute(5, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound172 = 1;
         }
         else
         {
            RcdFound172 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003R3 */
         pr_default.execute(1, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3R172( 2) ;
            RcdFound172 = 1;
            A1460SistemaDePara_Codigo = T003R3_A1460SistemaDePara_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
            A1461SistemaDePara_Origem = T003R3_A1461SistemaDePara_Origem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1461SistemaDePara_Origem", A1461SistemaDePara_Origem);
            A1462SistemaDePara_OrigemId = T003R3_A1462SistemaDePara_OrigemId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1462SistemaDePara_OrigemId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1462SistemaDePara_OrigemId), 18, 0)));
            A1463SistemaDePara_OrigemDsc = T003R3_A1463SistemaDePara_OrigemDsc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1463SistemaDePara_OrigemDsc", A1463SistemaDePara_OrigemDsc);
            n1463SistemaDePara_OrigemDsc = T003R3_n1463SistemaDePara_OrigemDsc[0];
            A127Sistema_Codigo = T003R3_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            Z127Sistema_Codigo = A127Sistema_Codigo;
            Z1460SistemaDePara_Codigo = A1460SistemaDePara_Codigo;
            sMode172 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3R172( ) ;
            if ( AnyError == 1 )
            {
               RcdFound172 = 0;
               InitializeNonKey3R172( ) ;
            }
            Gx_mode = sMode172;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound172 = 0;
            InitializeNonKey3R172( ) ;
            sMode172 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode172;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound172 = 0;
         /* Using cursor T003R8 */
         pr_default.execute(6, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003R8_A127Sistema_Codigo[0] < A127Sistema_Codigo ) || ( T003R8_A127Sistema_Codigo[0] == A127Sistema_Codigo ) && ( T003R8_A1460SistemaDePara_Codigo[0] < A1460SistemaDePara_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003R8_A127Sistema_Codigo[0] > A127Sistema_Codigo ) || ( T003R8_A127Sistema_Codigo[0] == A127Sistema_Codigo ) && ( T003R8_A1460SistemaDePara_Codigo[0] > A1460SistemaDePara_Codigo ) ) )
            {
               A127Sistema_Codigo = T003R8_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               A1460SistemaDePara_Codigo = T003R8_A1460SistemaDePara_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
               RcdFound172 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound172 = 0;
         /* Using cursor T003R9 */
         pr_default.execute(7, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003R9_A127Sistema_Codigo[0] > A127Sistema_Codigo ) || ( T003R9_A127Sistema_Codigo[0] == A127Sistema_Codigo ) && ( T003R9_A1460SistemaDePara_Codigo[0] > A1460SistemaDePara_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003R9_A127Sistema_Codigo[0] < A127Sistema_Codigo ) || ( T003R9_A127Sistema_Codigo[0] == A127Sistema_Codigo ) && ( T003R9_A1460SistemaDePara_Codigo[0] < A1460SistemaDePara_Codigo ) ) )
            {
               A127Sistema_Codigo = T003R9_A127Sistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
               A1460SistemaDePara_Codigo = T003R9_A1460SistemaDePara_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
               RcdFound172 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3R172( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3R172( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound172 == 1 )
            {
               if ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) || ( A1460SistemaDePara_Codigo != Z1460SistemaDePara_Codigo ) )
               {
                  A127Sistema_Codigo = Z127Sistema_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  A1460SistemaDePara_Codigo = Z1460SistemaDePara_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SISTEMA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3R172( ) ;
                  GX_FocusControl = edtSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) || ( A1460SistemaDePara_Codigo != Z1460SistemaDePara_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3R172( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SISTEMA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSistema_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtSistema_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3R172( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A127Sistema_Codigo != Z127Sistema_Codigo ) || ( A1460SistemaDePara_Codigo != Z1460SistemaDePara_Codigo ) )
         {
            A127Sistema_Codigo = Z127Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A1460SistemaDePara_Codigo = Z1460SistemaDePara_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound172 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = cmbSistemaDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSistemaDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3R172( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound172 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSistemaDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound172 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSistemaDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3R172( ) ;
         if ( RcdFound172 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound172 != 0 )
            {
               ScanNext3R172( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbSistemaDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3R172( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3R172( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003R2 */
            pr_default.execute(0, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaDePara"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1461SistemaDePara_Origem, T003R2_A1461SistemaDePara_Origem[0]) != 0 ) || ( Z1462SistemaDePara_OrigemId != T003R2_A1462SistemaDePara_OrigemId[0] ) )
            {
               if ( StringUtil.StrCmp(Z1461SistemaDePara_Origem, T003R2_A1461SistemaDePara_Origem[0]) != 0 )
               {
                  GXUtil.WriteLog("sistemadepara:[seudo value changed for attri]"+"SistemaDePara_Origem");
                  GXUtil.WriteLogRaw("Old: ",Z1461SistemaDePara_Origem);
                  GXUtil.WriteLogRaw("Current: ",T003R2_A1461SistemaDePara_Origem[0]);
               }
               if ( Z1462SistemaDePara_OrigemId != T003R2_A1462SistemaDePara_OrigemId[0] )
               {
                  GXUtil.WriteLog("sistemadepara:[seudo value changed for attri]"+"SistemaDePara_OrigemId");
                  GXUtil.WriteLogRaw("Old: ",Z1462SistemaDePara_OrigemId);
                  GXUtil.WriteLogRaw("Current: ",T003R2_A1462SistemaDePara_OrigemId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SistemaDePara"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3R172( )
      {
         BeforeValidate3R172( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3R172( 0) ;
            CheckOptimisticConcurrency3R172( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3R172( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3R172( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003R10 */
                     pr_default.execute(8, new Object[] {A1460SistemaDePara_Codigo, A1461SistemaDePara_Origem, A1462SistemaDePara_OrigemId, n1463SistemaDePara_OrigemDsc, A1463SistemaDePara_OrigemDsc, A127Sistema_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaDePara") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3R0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3R172( ) ;
            }
            EndLevel3R172( ) ;
         }
         CloseExtendedTableCursors3R172( ) ;
      }

      protected void Update3R172( )
      {
         BeforeValidate3R172( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3R172( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3R172( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3R172( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003R11 */
                     pr_default.execute(9, new Object[] {A1461SistemaDePara_Origem, A1462SistemaDePara_OrigemId, n1463SistemaDePara_OrigemDsc, A1463SistemaDePara_OrigemDsc, A127Sistema_Codigo, A1460SistemaDePara_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaDePara") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaDePara"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3R172( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3R0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3R172( ) ;
         }
         CloseExtendedTableCursors3R172( ) ;
      }

      protected void DeferredUpdate3R172( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3R172( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3R172( ) ;
            AfterConfirm3R172( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3R172( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003R12 */
                  pr_default.execute(10, new Object[] {A127Sistema_Codigo, A1460SistemaDePara_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("SistemaDePara") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound172 == 0 )
                        {
                           InitAll3R172( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3R0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode172 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3R172( ) ;
         Gx_mode = sMode172;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3R172( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3R172( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3R172( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "SistemaDePara");
            if ( AnyError == 0 )
            {
               ConfirmValues3R0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "SistemaDePara");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3R172( )
      {
         /* Using cursor T003R13 */
         pr_default.execute(11);
         RcdFound172 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound172 = 1;
            A127Sistema_Codigo = T003R13_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A1460SistemaDePara_Codigo = T003R13_A1460SistemaDePara_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3R172( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound172 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound172 = 1;
            A127Sistema_Codigo = T003R13_A127Sistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            A1460SistemaDePara_Codigo = T003R13_A1460SistemaDePara_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3R172( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3R172( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3R172( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3R172( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3R172( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3R172( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3R172( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3R172( )
      {
         edtSistema_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Enabled), 5, 0)));
         edtSistemaDePara_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaDePara_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaDePara_Codigo_Enabled), 5, 0)));
         cmbSistemaDePara_Origem.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistemaDePara_Origem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbSistemaDePara_Origem.Enabled), 5, 0)));
         edtSistemaDePara_OrigemId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaDePara_OrigemId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaDePara_OrigemId_Enabled), 5, 0)));
         edtSistemaDePara_OrigemDsc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaDePara_OrigemDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaDePara_OrigemDsc_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3R0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117283016");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemadepara.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1460SistemaDePara_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1461SistemaDePara_Origem", StringUtil.RTrim( Z1461SistemaDePara_Origem));
         GxWebStd.gx_hidden_field( context, "Z1462SistemaDePara_OrigemId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1462SistemaDePara_OrigemId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("sistemadepara.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "SistemaDePara" ;
      }

      public override String GetPgmdesc( )
      {
         return "De Para" ;
      }

      protected void InitializeNonKey3R172( )
      {
         A1461SistemaDePara_Origem = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1461SistemaDePara_Origem", A1461SistemaDePara_Origem);
         A1462SistemaDePara_OrigemId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1462SistemaDePara_OrigemId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1462SistemaDePara_OrigemId), 18, 0)));
         A1463SistemaDePara_OrigemDsc = "";
         n1463SistemaDePara_OrigemDsc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1463SistemaDePara_OrigemDsc", A1463SistemaDePara_OrigemDsc);
         n1463SistemaDePara_OrigemDsc = (String.IsNullOrEmpty(StringUtil.RTrim( A1463SistemaDePara_OrigemDsc)) ? true : false);
         Z1461SistemaDePara_Origem = "";
         Z1462SistemaDePara_OrigemId = 0;
      }

      protected void InitAll3R172( )
      {
         A127Sistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         A1460SistemaDePara_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1460SistemaDePara_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1460SistemaDePara_Codigo), 6, 0)));
         InitializeNonKey3R172( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117283020");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("sistemadepara.js", "?20203117283020");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocksistema_codigo_Internalname = "TEXTBLOCKSISTEMA_CODIGO";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         lblTextblocksistemadepara_codigo_Internalname = "TEXTBLOCKSISTEMADEPARA_CODIGO";
         edtSistemaDePara_Codigo_Internalname = "SISTEMADEPARA_CODIGO";
         lblTextblocksistemadepara_origem_Internalname = "TEXTBLOCKSISTEMADEPARA_ORIGEM";
         cmbSistemaDePara_Origem_Internalname = "SISTEMADEPARA_ORIGEM";
         lblTextblocksistemadepara_origemid_Internalname = "TEXTBLOCKSISTEMADEPARA_ORIGEMID";
         edtSistemaDePara_OrigemId_Internalname = "SISTEMADEPARA_ORIGEMID";
         lblTextblocksistemadepara_origemdsc_Internalname = "TEXTBLOCKSISTEMADEPARA_ORIGEMDSC";
         edtSistemaDePara_OrigemDsc_Internalname = "SISTEMADEPARA_ORIGEMDSC";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "De Para";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtSistemaDePara_OrigemDsc_Enabled = 1;
         edtSistemaDePara_OrigemId_Jsonclick = "";
         edtSistemaDePara_OrigemId_Enabled = 1;
         cmbSistemaDePara_Origem_Jsonclick = "";
         cmbSistemaDePara_Origem.Enabled = 1;
         edtSistemaDePara_Codigo_Jsonclick = "";
         edtSistemaDePara_Codigo_Enabled = 1;
         edtSistema_Codigo_Jsonclick = "";
         edtSistema_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T003R14 */
         pr_default.execute(12, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(12);
         GX_FocusControl = cmbSistemaDePara_Origem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Sistema_codigo( int GX_Parm1 )
      {
         A127Sistema_Codigo = GX_Parm1;
         /* Using cursor T003R14 */
         pr_default.execute(12, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistema_Codigo_Internalname;
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Sistemadepara_codigo( int GX_Parm1 ,
                                              int GX_Parm2 ,
                                              GXCombobox cmbGX_Parm3 ,
                                              long GX_Parm4 ,
                                              String GX_Parm5 )
      {
         A127Sistema_Codigo = GX_Parm1;
         A1460SistemaDePara_Codigo = GX_Parm2;
         cmbSistemaDePara_Origem = cmbGX_Parm3;
         A1461SistemaDePara_Origem = cmbSistemaDePara_Origem.CurrentValue;
         cmbSistemaDePara_Origem.CurrentValue = A1461SistemaDePara_Origem;
         A1462SistemaDePara_OrigemId = GX_Parm4;
         A1463SistemaDePara_OrigemDsc = GX_Parm5;
         n1463SistemaDePara_OrigemDsc = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         cmbSistemaDePara_Origem.CurrentValue = A1461SistemaDePara_Origem;
         isValidOutput.Add(cmbSistemaDePara_Origem);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1462SistemaDePara_OrigemId), 18, 0, ".", "")));
         isValidOutput.Add(A1463SistemaDePara_OrigemDsc);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z127Sistema_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1460SistemaDePara_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1461SistemaDePara_Origem));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1462SistemaDePara_OrigemId), 18, 0, ",", "")));
         isValidOutput.Add(Z1463SistemaDePara_OrigemDsc);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1461SistemaDePara_Origem = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1461SistemaDePara_Origem = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocksistema_codigo_Jsonclick = "";
         lblTextblocksistemadepara_codigo_Jsonclick = "";
         lblTextblocksistemadepara_origem_Jsonclick = "";
         lblTextblocksistemadepara_origemid_Jsonclick = "";
         lblTextblocksistemadepara_origemdsc_Jsonclick = "";
         A1463SistemaDePara_OrigemDsc = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1463SistemaDePara_OrigemDsc = "";
         T003R5_A1460SistemaDePara_Codigo = new int[1] ;
         T003R5_A1461SistemaDePara_Origem = new String[] {""} ;
         T003R5_A1462SistemaDePara_OrigemId = new long[1] ;
         T003R5_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         T003R5_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         T003R5_A127Sistema_Codigo = new int[1] ;
         T003R4_A127Sistema_Codigo = new int[1] ;
         T003R6_A127Sistema_Codigo = new int[1] ;
         T003R7_A127Sistema_Codigo = new int[1] ;
         T003R7_A1460SistemaDePara_Codigo = new int[1] ;
         T003R3_A1460SistemaDePara_Codigo = new int[1] ;
         T003R3_A1461SistemaDePara_Origem = new String[] {""} ;
         T003R3_A1462SistemaDePara_OrigemId = new long[1] ;
         T003R3_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         T003R3_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         T003R3_A127Sistema_Codigo = new int[1] ;
         sMode172 = "";
         T003R8_A127Sistema_Codigo = new int[1] ;
         T003R8_A1460SistemaDePara_Codigo = new int[1] ;
         T003R9_A127Sistema_Codigo = new int[1] ;
         T003R9_A1460SistemaDePara_Codigo = new int[1] ;
         T003R2_A1460SistemaDePara_Codigo = new int[1] ;
         T003R2_A1461SistemaDePara_Origem = new String[] {""} ;
         T003R2_A1462SistemaDePara_OrigemId = new long[1] ;
         T003R2_A1463SistemaDePara_OrigemDsc = new String[] {""} ;
         T003R2_n1463SistemaDePara_OrigemDsc = new bool[] {false} ;
         T003R2_A127Sistema_Codigo = new int[1] ;
         T003R13_A127Sistema_Codigo = new int[1] ;
         T003R13_A1460SistemaDePara_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T003R14_A127Sistema_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemadepara__default(),
            new Object[][] {
                new Object[] {
               T003R2_A1460SistemaDePara_Codigo, T003R2_A1461SistemaDePara_Origem, T003R2_A1462SistemaDePara_OrigemId, T003R2_A1463SistemaDePara_OrigemDsc, T003R2_n1463SistemaDePara_OrigemDsc, T003R2_A127Sistema_Codigo
               }
               , new Object[] {
               T003R3_A1460SistemaDePara_Codigo, T003R3_A1461SistemaDePara_Origem, T003R3_A1462SistemaDePara_OrigemId, T003R3_A1463SistemaDePara_OrigemDsc, T003R3_n1463SistemaDePara_OrigemDsc, T003R3_A127Sistema_Codigo
               }
               , new Object[] {
               T003R4_A127Sistema_Codigo
               }
               , new Object[] {
               T003R5_A1460SistemaDePara_Codigo, T003R5_A1461SistemaDePara_Origem, T003R5_A1462SistemaDePara_OrigemId, T003R5_A1463SistemaDePara_OrigemDsc, T003R5_n1463SistemaDePara_OrigemDsc, T003R5_A127Sistema_Codigo
               }
               , new Object[] {
               T003R6_A127Sistema_Codigo
               }
               , new Object[] {
               T003R7_A127Sistema_Codigo, T003R7_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               T003R8_A127Sistema_Codigo, T003R8_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               T003R9_A127Sistema_Codigo, T003R9_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003R13_A127Sistema_Codigo, T003R13_A1460SistemaDePara_Codigo
               }
               , new Object[] {
               T003R14_A127Sistema_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound172 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z127Sistema_Codigo ;
      private int Z1460SistemaDePara_Codigo ;
      private int A127Sistema_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtSistema_Codigo_Enabled ;
      private int A1460SistemaDePara_Codigo ;
      private int edtSistemaDePara_Codigo_Enabled ;
      private int edtSistemaDePara_OrigemId_Enabled ;
      private int edtSistemaDePara_OrigemDsc_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private long Z1462SistemaDePara_OrigemId ;
      private long A1462SistemaDePara_OrigemId ;
      private String sPrefix ;
      private String Z1461SistemaDePara_Origem ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A1461SistemaDePara_Origem ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSistema_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocksistema_codigo_Internalname ;
      private String lblTextblocksistema_codigo_Jsonclick ;
      private String edtSistema_Codigo_Jsonclick ;
      private String lblTextblocksistemadepara_codigo_Internalname ;
      private String lblTextblocksistemadepara_codigo_Jsonclick ;
      private String edtSistemaDePara_Codigo_Internalname ;
      private String edtSistemaDePara_Codigo_Jsonclick ;
      private String lblTextblocksistemadepara_origem_Internalname ;
      private String lblTextblocksistemadepara_origem_Jsonclick ;
      private String cmbSistemaDePara_Origem_Internalname ;
      private String cmbSistemaDePara_Origem_Jsonclick ;
      private String lblTextblocksistemadepara_origemid_Internalname ;
      private String lblTextblocksistemadepara_origemid_Jsonclick ;
      private String edtSistemaDePara_OrigemId_Internalname ;
      private String edtSistemaDePara_OrigemId_Jsonclick ;
      private String lblTextblocksistemadepara_origemdsc_Internalname ;
      private String lblTextblocksistemadepara_origemdsc_Jsonclick ;
      private String edtSistemaDePara_OrigemDsc_Internalname ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode172 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1463SistemaDePara_OrigemDsc ;
      private String A1463SistemaDePara_OrigemDsc ;
      private String Z1463SistemaDePara_OrigemDsc ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbSistemaDePara_Origem ;
      private IDataStoreProvider pr_default ;
      private int[] T003R5_A1460SistemaDePara_Codigo ;
      private String[] T003R5_A1461SistemaDePara_Origem ;
      private long[] T003R5_A1462SistemaDePara_OrigemId ;
      private String[] T003R5_A1463SistemaDePara_OrigemDsc ;
      private bool[] T003R5_n1463SistemaDePara_OrigemDsc ;
      private int[] T003R5_A127Sistema_Codigo ;
      private int[] T003R4_A127Sistema_Codigo ;
      private int[] T003R6_A127Sistema_Codigo ;
      private int[] T003R7_A127Sistema_Codigo ;
      private int[] T003R7_A1460SistemaDePara_Codigo ;
      private int[] T003R3_A1460SistemaDePara_Codigo ;
      private String[] T003R3_A1461SistemaDePara_Origem ;
      private long[] T003R3_A1462SistemaDePara_OrigemId ;
      private String[] T003R3_A1463SistemaDePara_OrigemDsc ;
      private bool[] T003R3_n1463SistemaDePara_OrigemDsc ;
      private int[] T003R3_A127Sistema_Codigo ;
      private int[] T003R8_A127Sistema_Codigo ;
      private int[] T003R8_A1460SistemaDePara_Codigo ;
      private int[] T003R9_A127Sistema_Codigo ;
      private int[] T003R9_A1460SistemaDePara_Codigo ;
      private int[] T003R2_A1460SistemaDePara_Codigo ;
      private String[] T003R2_A1461SistemaDePara_Origem ;
      private long[] T003R2_A1462SistemaDePara_OrigemId ;
      private String[] T003R2_A1463SistemaDePara_OrigemDsc ;
      private bool[] T003R2_n1463SistemaDePara_OrigemDsc ;
      private int[] T003R2_A127Sistema_Codigo ;
      private int[] T003R13_A127Sistema_Codigo ;
      private int[] T003R13_A1460SistemaDePara_Codigo ;
      private int[] T003R14_A127Sistema_Codigo ;
      private GXWebForm Form ;
   }

   public class sistemadepara__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003R5 ;
          prmT003R5 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R4 ;
          prmT003R4 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R6 ;
          prmT003R6 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R7 ;
          prmT003R7 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R3 ;
          prmT003R3 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R8 ;
          prmT003R8 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R9 ;
          prmT003R9 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R2 ;
          prmT003R2 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R10 ;
          prmT003R10 = new Object[] {
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@SistemaDePara_OrigemId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@SistemaDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R11 ;
          prmT003R11 = new Object[] {
          new Object[] {"@SistemaDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@SistemaDePara_OrigemId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@SistemaDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R12 ;
          prmT003R12 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003R13 ;
          prmT003R13 = new Object[] {
          } ;
          Object[] prmT003R14 ;
          prmT003R14 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003R2", "SELECT [SistemaDePara_Codigo], [SistemaDePara_Origem], [SistemaDePara_OrigemId], [SistemaDePara_OrigemDsc], [Sistema_Codigo] FROM [SistemaDePara] WITH (UPDLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003R2,1,0,true,false )
             ,new CursorDef("T003R3", "SELECT [SistemaDePara_Codigo], [SistemaDePara_Origem], [SistemaDePara_OrigemId], [SistemaDePara_OrigemDsc], [Sistema_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003R3,1,0,true,false )
             ,new CursorDef("T003R4", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003R4,1,0,true,false )
             ,new CursorDef("T003R5", "SELECT TM1.[SistemaDePara_Codigo], TM1.[SistemaDePara_Origem], TM1.[SistemaDePara_OrigemId], TM1.[SistemaDePara_OrigemDsc], TM1.[Sistema_Codigo] FROM [SistemaDePara] TM1 WITH (NOLOCK) WHERE TM1.[Sistema_Codigo] = @Sistema_Codigo and TM1.[SistemaDePara_Codigo] = @SistemaDePara_Codigo ORDER BY TM1.[Sistema_Codigo], TM1.[SistemaDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003R5,100,0,true,false )
             ,new CursorDef("T003R6", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003R6,1,0,true,false )
             ,new CursorDef("T003R7", "SELECT [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003R7,1,0,true,false )
             ,new CursorDef("T003R8", "SELECT TOP 1 [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE ( [Sistema_Codigo] > @Sistema_Codigo or [Sistema_Codigo] = @Sistema_Codigo and [SistemaDePara_Codigo] > @SistemaDePara_Codigo) ORDER BY [Sistema_Codigo], [SistemaDePara_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003R8,1,0,true,true )
             ,new CursorDef("T003R9", "SELECT TOP 1 [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) WHERE ( [Sistema_Codigo] < @Sistema_Codigo or [Sistema_Codigo] = @Sistema_Codigo and [SistemaDePara_Codigo] < @SistemaDePara_Codigo) ORDER BY [Sistema_Codigo] DESC, [SistemaDePara_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003R9,1,0,true,true )
             ,new CursorDef("T003R10", "INSERT INTO [SistemaDePara]([SistemaDePara_Codigo], [SistemaDePara_Origem], [SistemaDePara_OrigemId], [SistemaDePara_OrigemDsc], [Sistema_Codigo]) VALUES(@SistemaDePara_Codigo, @SistemaDePara_Origem, @SistemaDePara_OrigemId, @SistemaDePara_OrigemDsc, @Sistema_Codigo)", GxErrorMask.GX_NOMASK,prmT003R10)
             ,new CursorDef("T003R11", "UPDATE [SistemaDePara] SET [SistemaDePara_Origem]=@SistemaDePara_Origem, [SistemaDePara_OrigemId]=@SistemaDePara_OrigemId, [SistemaDePara_OrigemDsc]=@SistemaDePara_OrigemDsc  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo", GxErrorMask.GX_NOMASK,prmT003R11)
             ,new CursorDef("T003R12", "DELETE FROM [SistemaDePara]  WHERE [Sistema_Codigo] = @Sistema_Codigo AND [SistemaDePara_Codigo] = @SistemaDePara_Codigo", GxErrorMask.GX_NOMASK,prmT003R12)
             ,new CursorDef("T003R13", "SELECT [Sistema_Codigo], [SistemaDePara_Codigo] FROM [SistemaDePara] WITH (NOLOCK) ORDER BY [Sistema_Codigo], [SistemaDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003R13,100,0,true,false )
             ,new CursorDef("T003R14", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003R14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (long)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (long)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
