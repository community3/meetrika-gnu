/*
               File: ContratoServicos_BC
        Description: Contrato Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:54:27.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicos_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratoservicos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0X34( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0X34( ) ;
         standaloneModal( ) ;
         AddRow0X34( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E110X2 */
            E110X2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0X0( )
      {
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0X34( ) ;
            }
            else
            {
               CheckExtendedTable0X34( ) ;
               if ( AnyError == 0 )
               {
                  ZM0X34( 35) ;
                  ZM0X34( 36) ;
                  ZM0X34( 37) ;
                  ZM0X34( 38) ;
                  ZM0X34( 39) ;
                  ZM0X34( 40) ;
                  ZM0X34( 41) ;
                  ZM0X34( 42) ;
                  ZM0X34( 43) ;
                  ZM0X34( 44) ;
               }
               CloseExtendedTableCursors0X34( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode34 = Gx_mode;
            CONFIRM_0X228( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode34;
               IsConfirmed = 1;
            }
            /* Restore parent mode. */
            Gx_mode = sMode34;
         }
      }

      protected void CONFIRM_0X228( )
      {
         s2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         nGXsfl_228_idx = 0;
         while ( nGXsfl_228_idx < bcContratoServicos.gxTpr_Rmn.Count )
         {
            ReadRow0X228( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound228 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_228 != 0 ) )
            {
               GetKey0X228( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound228 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate0X228( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0X228( ) ;
                        if ( AnyError == 0 )
                        {
                        }
                        CloseExtendedTableCursors0X228( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                        O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                        n2073ContratoServicos_QtdRmn = false;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound228 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey0X228( ) ;
                        Load0X228( ) ;
                        BeforeValidate0X228( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0X228( ) ;
                           O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                           n2073ContratoServicos_QtdRmn = false;
                        }
                     }
                     else
                     {
                        if ( nIsMod_228 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate0X228( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0X228( ) ;
                              if ( AnyError == 0 )
                              {
                              }
                              CloseExtendedTableCursors0X228( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                              O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                              n2073ContratoServicos_QtdRmn = false;
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow228( ((SdtContratoServicos_Rmn)bcContratoServicos.gxTpr_Rmn.Item(nGXsfl_228_idx))) ;
            }
         }
         O2073ContratoServicos_QtdRmn = s2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void E120X2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV36Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV37GXV1 = 1;
            while ( AV37GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV37GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Servico_Codigo") == 0 )
               {
                  AV12Insert_Servico_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContratoServicos_UnidadeContratada") == 0 )
               {
                  AV24Insert_ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV37GXV1 = (int)(AV37GXV1+1);
            }
         }
      }

      protected void E110X2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            new prc_insprioridadespadrao(context ).execute(  A160ContratoServicos_Codigo,  A155Servico_Codigo) ;
            new prc_insartefatospadrao(context ).execute( ref  A160ContratoServicos_Codigo, ref  A155Servico_Codigo) ;
         }
         else if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) && ( A557Servico_VlrUnidadeContratada != O557Servico_VlrUnidadeContratada ) )
         {
            new prc_atualizavalorpf(context ).execute( ref  A75Contrato_AreaTrabalhoCod,  A557Servico_VlrUnidadeContratada,  A155Servico_Codigo) ;
         }
         if ( false )
         {
            new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV36Pgmname) ;
         }
         new wwpbaseobjects.audittransaction(context ).execute(  AV29AuditingObject,  AV36Pgmname) ;
      }

      protected void E130X2( )
      {
         /* 'DoReplicar' Routine */
         if ( A1516ContratoServicos_TmpEstAnl + A1501ContratoServicos_TmpEstExc + A1502ContratoServicos_TmpEstCrr > 0 )
         {
            new prc_replicartemposestimados(context ).execute( ref  AV7ContratoServicos_Codigo,  A1516ContratoServicos_TmpEstAnl,  A1501ContratoServicos_TmpEstExc,  A1502ContratoServicos_TmpEstCrr) ;
         }
         else
         {
            GX_msglist.addItem("N�o existem tempos estiimados preenchidos para replicar!");
         }
      }

      protected void E140X2( )
      {
         /* 'DoUpdPrazo' Routine */
         context.wjLoc = formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim(((A911ContratoServicos_Prazos==0) ? "INS" : "UPD"))) + "," + UrlEncode("" +A160ContratoServicos_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
      }

      protected void E150X2( )
      {
         /* Servico_codigo_Click Routine */
      }

      protected void E160X2( )
      {
         /* Subservico_codigo_Click Routine */
         GXt_char1 = AV32Servico_Nome;
         new prc_serviconome(context ).execute( ref  AV15SubServico_Codigo, out  GXt_char1) ;
         AV32Servico_Nome = GXt_char1;
      }

      protected void E170X2( )
      {
         /* ContratoServicos_PrazoCorrecaoTipo_Click Routine */
      }

      protected void ZM0X34( short GX_JID )
      {
         if ( ( GX_JID == 34 ) || ( GX_JID == 0 ) )
         {
            Z1858ContratoServicos_Alias = A1858ContratoServicos_Alias;
            Z555Servico_QtdContratada = A555Servico_QtdContratada;
            Z557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
            Z558Servico_Percentual = A558Servico_Percentual;
            Z607ServicoContrato_Faturamento = A607ServicoContrato_Faturamento;
            Z639ContratoServicos_LocalExec = A639ContratoServicos_LocalExec;
            Z868ContratoServicos_TipoVnc = A868ContratoServicos_TipoVnc;
            Z888ContratoServicos_HmlSemCnf = A888ContratoServicos_HmlSemCnf;
            Z1454ContratoServicos_PrazoTpDias = A1454ContratoServicos_PrazoTpDias;
            Z1152ContratoServicos_PrazoAnalise = A1152ContratoServicos_PrazoAnalise;
            Z1153ContratoServicos_PrazoResposta = A1153ContratoServicos_PrazoResposta;
            Z1181ContratoServicos_PrazoGarantia = A1181ContratoServicos_PrazoGarantia;
            Z1182ContratoServicos_PrazoAtendeGarantia = A1182ContratoServicos_PrazoAtendeGarantia;
            Z1224ContratoServicos_PrazoCorrecao = A1224ContratoServicos_PrazoCorrecao;
            Z1225ContratoServicos_PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
            Z1649ContratoServicos_PrazoInicio = A1649ContratoServicos_PrazoInicio;
            Z1190ContratoServicos_PrazoImediato = A1190ContratoServicos_PrazoImediato;
            Z1191ContratoServicos_Produtividade = A1191ContratoServicos_Produtividade;
            Z1217ContratoServicos_EspelhaAceite = A1217ContratoServicos_EspelhaAceite;
            Z1266ContratoServicos_Momento = A1266ContratoServicos_Momento;
            Z1325ContratoServicos_StatusPagFnc = A1325ContratoServicos_StatusPagFnc;
            Z1340ContratoServicos_QntUntCns = A1340ContratoServicos_QntUntCns;
            Z1341ContratoServicos_FatorCnvUndCnt = A1341ContratoServicos_FatorCnvUndCnt;
            Z1397ContratoServicos_NaoRequerAtr = A1397ContratoServicos_NaoRequerAtr;
            Z1455ContratoServicos_IndiceDivergencia = A1455ContratoServicos_IndiceDivergencia;
            Z1516ContratoServicos_TmpEstAnl = A1516ContratoServicos_TmpEstAnl;
            Z1501ContratoServicos_TmpEstExc = A1501ContratoServicos_TmpEstExc;
            Z1502ContratoServicos_TmpEstCrr = A1502ContratoServicos_TmpEstCrr;
            Z1531ContratoServicos_TipoHierarquia = A1531ContratoServicos_TipoHierarquia;
            Z1537ContratoServicos_PercTmp = A1537ContratoServicos_PercTmp;
            Z1538ContratoServicos_PercPgm = A1538ContratoServicos_PercPgm;
            Z1539ContratoServicos_PercCnc = A1539ContratoServicos_PercCnc;
            Z638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
            Z1723ContratoServicos_CodigoFiscal = A1723ContratoServicos_CodigoFiscal;
            Z1799ContratoServicos_LimiteProposta = A1799ContratoServicos_LimiteProposta;
            Z2074ContratoServicos_CalculoRmn = A2074ContratoServicos_CalculoRmn;
            Z2094ContratoServicos_SolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
            Z155Servico_Codigo = A155Servico_Codigo;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z1212ContratoServicos_UnidadeContratada = A1212ContratoServicos_UnidadeContratada;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 35 ) || ( GX_JID == 0 ) )
         {
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 36 ) || ( GX_JID == 0 ) )
         {
            Z608Servico_Nome = A608Servico_Nome;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1061Servico_Tela = A1061Servico_Tela;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 37 ) || ( GX_JID == 0 ) )
         {
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 38 ) || ( GX_JID == 0 ) )
         {
            Z2132ContratoServicos_UndCntNome = A2132ContratoServicos_UndCntNome;
            Z1712ContratoServicos_UndCntSgl = A1712ContratoServicos_UndCntSgl;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 39 ) || ( GX_JID == 0 ) )
         {
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 40 ) || ( GX_JID == 0 ) )
         {
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 41 ) || ( GX_JID == 0 ) )
         {
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 42 ) || ( GX_JID == 0 ) )
         {
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 43 ) || ( GX_JID == 0 ) )
         {
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( ( GX_JID == 44 ) || ( GX_JID == 0 ) )
         {
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
         if ( GX_JID == -34 )
         {
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1858ContratoServicos_Alias = A1858ContratoServicos_Alias;
            Z555Servico_QtdContratada = A555Servico_QtdContratada;
            Z557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
            Z558Servico_Percentual = A558Servico_Percentual;
            Z607ServicoContrato_Faturamento = A607ServicoContrato_Faturamento;
            Z639ContratoServicos_LocalExec = A639ContratoServicos_LocalExec;
            Z868ContratoServicos_TipoVnc = A868ContratoServicos_TipoVnc;
            Z888ContratoServicos_HmlSemCnf = A888ContratoServicos_HmlSemCnf;
            Z1454ContratoServicos_PrazoTpDias = A1454ContratoServicos_PrazoTpDias;
            Z1152ContratoServicos_PrazoAnalise = A1152ContratoServicos_PrazoAnalise;
            Z1153ContratoServicos_PrazoResposta = A1153ContratoServicos_PrazoResposta;
            Z1181ContratoServicos_PrazoGarantia = A1181ContratoServicos_PrazoGarantia;
            Z1182ContratoServicos_PrazoAtendeGarantia = A1182ContratoServicos_PrazoAtendeGarantia;
            Z1224ContratoServicos_PrazoCorrecao = A1224ContratoServicos_PrazoCorrecao;
            Z1225ContratoServicos_PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
            Z1649ContratoServicos_PrazoInicio = A1649ContratoServicos_PrazoInicio;
            Z1190ContratoServicos_PrazoImediato = A1190ContratoServicos_PrazoImediato;
            Z1191ContratoServicos_Produtividade = A1191ContratoServicos_Produtividade;
            Z1217ContratoServicos_EspelhaAceite = A1217ContratoServicos_EspelhaAceite;
            Z1266ContratoServicos_Momento = A1266ContratoServicos_Momento;
            Z1325ContratoServicos_StatusPagFnc = A1325ContratoServicos_StatusPagFnc;
            Z1340ContratoServicos_QntUntCns = A1340ContratoServicos_QntUntCns;
            Z1341ContratoServicos_FatorCnvUndCnt = A1341ContratoServicos_FatorCnvUndCnt;
            Z1397ContratoServicos_NaoRequerAtr = A1397ContratoServicos_NaoRequerAtr;
            Z1455ContratoServicos_IndiceDivergencia = A1455ContratoServicos_IndiceDivergencia;
            Z1516ContratoServicos_TmpEstAnl = A1516ContratoServicos_TmpEstAnl;
            Z1501ContratoServicos_TmpEstExc = A1501ContratoServicos_TmpEstExc;
            Z1502ContratoServicos_TmpEstCrr = A1502ContratoServicos_TmpEstCrr;
            Z1531ContratoServicos_TipoHierarquia = A1531ContratoServicos_TipoHierarquia;
            Z1537ContratoServicos_PercTmp = A1537ContratoServicos_PercTmp;
            Z1538ContratoServicos_PercPgm = A1538ContratoServicos_PercPgm;
            Z1539ContratoServicos_PercCnc = A1539ContratoServicos_PercCnc;
            Z638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
            Z1723ContratoServicos_CodigoFiscal = A1723ContratoServicos_CodigoFiscal;
            Z1799ContratoServicos_LimiteProposta = A1799ContratoServicos_LimiteProposta;
            Z2074ContratoServicos_CalculoRmn = A2074ContratoServicos_CalculoRmn;
            Z2094ContratoServicos_SolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
            Z155Servico_Codigo = A155Servico_Codigo;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z1212ContratoServicos_UnidadeContratada = A1212ContratoServicos_UnidadeContratada;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z608Servico_Nome = A608Servico_Nome;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1061Servico_Tela = A1061Servico_Tela;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z156Servico_Descricao = A156Servico_Descricao;
            Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z2132ContratoServicos_UndCntNome = A2132ContratoServicos_UndCntNome;
            Z1712ContratoServicos_UndCntSgl = A1712ContratoServicos_UndCntSgl;
         }
      }

      protected void standaloneNotModal( )
      {
         AV36Pgmname = "ContratoServicos_BC";
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2094ContratoServicos_SolicitaGestorSistema) && ( Gx_BScreen == 0 ) )
         {
            A2094ContratoServicos_SolicitaGestorSistema = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A638ContratoServicos_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A638ContratoServicos_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1649ContratoServicos_PrazoInicio) && ( Gx_BScreen == 0 ) )
         {
            A1649ContratoServicos_PrazoInicio = 1;
            n1649ContratoServicos_PrazoInicio = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A607ServicoContrato_Faturamento)) && ( Gx_BScreen == 0 ) )
         {
            A607ServicoContrato_Faturamento = "B";
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) && ( Gx_BScreen == 0 ) )
         {
            A1454ContratoServicos_PrazoTpDias = "U";
            n1454ContratoServicos_PrazoTpDias = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A558Servico_Percentual) && ( Gx_BScreen == 0 ) )
         {
            A558Servico_Percentual = (decimal)(1);
            n558Servico_Percentual = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
         }
      }

      protected void Load0X34( )
      {
         /* Using cursor BC000X21 */
         pr_default.execute(14, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound34 = 1;
            A77Contrato_Numero = BC000X21_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000X21_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000X21_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000X21_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = BC000X21_A116Contrato_ValorUnidadeContratacao[0];
            A41Contratada_PessoaNom = BC000X21_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000X21_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000X21_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000X21_n42Contratada_PessoaCNPJ[0];
            A516Contratada_TipoFabrica = BC000X21_A516Contratada_TipoFabrica[0];
            A1858ContratoServicos_Alias = BC000X21_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = BC000X21_n1858ContratoServicos_Alias[0];
            A608Servico_Nome = BC000X21_A608Servico_Nome[0];
            A605Servico_Sigla = BC000X21_A605Servico_Sigla[0];
            A1061Servico_Tela = BC000X21_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000X21_n1061Servico_Tela[0];
            A632Servico_Ativo = BC000X21_A632Servico_Ativo[0];
            A156Servico_Descricao = BC000X21_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000X21_n156Servico_Descricao[0];
            A158ServicoGrupo_Descricao = BC000X21_A158ServicoGrupo_Descricao[0];
            A2092Servico_IsOrigemReferencia = BC000X21_A2092Servico_IsOrigemReferencia[0];
            A2132ContratoServicos_UndCntNome = BC000X21_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = BC000X21_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = BC000X21_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = BC000X21_n1712ContratoServicos_UndCntSgl[0];
            A555Servico_QtdContratada = BC000X21_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = BC000X21_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = BC000X21_A557Servico_VlrUnidadeContratada[0];
            A558Servico_Percentual = BC000X21_A558Servico_Percentual[0];
            n558Servico_Percentual = BC000X21_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = BC000X21_A607ServicoContrato_Faturamento[0];
            A639ContratoServicos_LocalExec = BC000X21_A639ContratoServicos_LocalExec[0];
            A868ContratoServicos_TipoVnc = BC000X21_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = BC000X21_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = BC000X21_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = BC000X21_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = BC000X21_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = BC000X21_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = BC000X21_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = BC000X21_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = BC000X21_A1153ContratoServicos_PrazoResposta[0];
            n1153ContratoServicos_PrazoResposta = BC000X21_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = BC000X21_A1181ContratoServicos_PrazoGarantia[0];
            n1181ContratoServicos_PrazoGarantia = BC000X21_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = BC000X21_A1182ContratoServicos_PrazoAtendeGarantia[0];
            n1182ContratoServicos_PrazoAtendeGarantia = BC000X21_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = BC000X21_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = BC000X21_n1224ContratoServicos_PrazoCorrecao[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = BC000X21_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = BC000X21_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1649ContratoServicos_PrazoInicio = BC000X21_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = BC000X21_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = BC000X21_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = BC000X21_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = BC000X21_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = BC000X21_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = BC000X21_A1217ContratoServicos_EspelhaAceite[0];
            n1217ContratoServicos_EspelhaAceite = BC000X21_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = BC000X21_A1266ContratoServicos_Momento[0];
            n1266ContratoServicos_Momento = BC000X21_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = BC000X21_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = BC000X21_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = BC000X21_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = BC000X21_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = BC000X21_A1341ContratoServicos_FatorCnvUndCnt[0];
            n1341ContratoServicos_FatorCnvUndCnt = BC000X21_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = BC000X21_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = BC000X21_n1397ContratoServicos_NaoRequerAtr[0];
            A453Contrato_IndiceDivergencia = BC000X21_A453Contrato_IndiceDivergencia[0];
            A1455ContratoServicos_IndiceDivergencia = BC000X21_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = BC000X21_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = BC000X21_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = BC000X21_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = BC000X21_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = BC000X21_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = BC000X21_A1502ContratoServicos_TmpEstCrr[0];
            n1502ContratoServicos_TmpEstCrr = BC000X21_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = BC000X21_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = BC000X21_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = BC000X21_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = BC000X21_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = BC000X21_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = BC000X21_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = BC000X21_A1539ContratoServicos_PercCnc[0];
            n1539ContratoServicos_PercCnc = BC000X21_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = BC000X21_A638ContratoServicos_Ativo[0];
            A1723ContratoServicos_CodigoFiscal = BC000X21_A1723ContratoServicos_CodigoFiscal[0];
            n1723ContratoServicos_CodigoFiscal = BC000X21_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = BC000X21_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = BC000X21_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = BC000X21_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = BC000X21_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = BC000X21_A2094ContratoServicos_SolicitaGestorSistema[0];
            A155Servico_Codigo = BC000X21_A155Servico_Codigo[0];
            A74Contrato_Codigo = BC000X21_A74Contrato_Codigo[0];
            A1212ContratoServicos_UnidadeContratada = BC000X21_A1212ContratoServicos_UnidadeContratada[0];
            A631Servico_Vinculado = BC000X21_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000X21_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = BC000X21_A157ServicoGrupo_Codigo[0];
            A39Contratada_Codigo = BC000X21_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = BC000X21_A40Contratada_PessoaCod[0];
            A75Contrato_AreaTrabalhoCod = BC000X21_A75Contrato_AreaTrabalhoCod[0];
            A913ContratoServicos_PrazoTipo = BC000X21_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = BC000X21_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = BC000X21_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = BC000X21_n914ContratoServicos_PrazoDias[0];
            A1377ContratoServicos_Indicadores = BC000X21_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = BC000X21_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = BC000X21_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = BC000X21_n2073ContratoServicos_QtdRmn[0];
            ZM0X34( -34) ;
         }
         pr_default.close(14);
         OnLoadActions0X34( ) ;
      }

      protected void OnLoadActions0X34( )
      {
         O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         /* Using cursor BC000X7 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A911ContratoServicos_Prazos = BC000X7_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = BC000X7_n911ContratoServicos_Prazos[0];
         }
         else
         {
            A911ContratoServicos_Prazos = 0;
            n911ContratoServicos_Prazos = false;
         }
         pr_default.close(4);
         AV32Servico_Nome = A608Servico_Nome;
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
         GXt_int2 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
         A1551Servico_Responsavel = GXt_int2;
         A827ContratoServicos_ServicoCod = A155Servico_Codigo;
         GXt_int3 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
         A640Servico_Vinculados = GXt_int3;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV30Alias = A1858ContratoServicos_Alias;
         }
         AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
         AV26CemPorCento = (bool)(((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P")==0)&&(A1224ContratoServicos_PrazoCorrecao==100)));
         if ( AV26CemPorCento )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = "I";
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
         }
         if ( ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I") == 0 ) || AV26CemPorCento )
         {
            A1224ContratoServicos_PrazoCorrecao = 0;
            n1224ContratoServicos_PrazoCorrecao = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1455ContratoServicos_IndiceDivergencia) && ( Gx_BScreen == 0 ) )
         {
            A1455ContratoServicos_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            n1455ContratoServicos_IndiceDivergencia = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A557Servico_VlrUnidadeContratada) && ( Gx_BScreen == 0 ) )
         {
            A557Servico_VlrUnidadeContratada = A116Contrato_ValorUnidadeContratacao;
         }
      }

      protected void CheckExtendedTable0X34( )
      {
         standaloneModal( ) ;
         /* Using cursor BC000X7 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A911ContratoServicos_Prazos = BC000X7_A911ContratoServicos_Prazos[0];
            n911ContratoServicos_Prazos = BC000X7_n911ContratoServicos_Prazos[0];
         }
         else
         {
            A911ContratoServicos_Prazos = 0;
            n911ContratoServicos_Prazos = false;
         }
         pr_default.close(4);
         /* Using cursor BC000X14 */
         pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            A913ContratoServicos_PrazoTipo = BC000X14_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = BC000X14_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = BC000X14_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = BC000X14_n914ContratoServicos_PrazoDias[0];
         }
         else
         {
            A914ContratoServicos_PrazoDias = 0;
            n914ContratoServicos_PrazoDias = false;
            A913ContratoServicos_PrazoTipo = "";
            n913ContratoServicos_PrazoTipo = false;
         }
         pr_default.close(11);
         /* Using cursor BC000X16 */
         pr_default.execute(12, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            A1377ContratoServicos_Indicadores = BC000X16_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = BC000X16_n1377ContratoServicos_Indicadores[0];
         }
         else
         {
            A1377ContratoServicos_Indicadores = 0;
            n1377ContratoServicos_Indicadores = false;
         }
         pr_default.close(12);
         /* Using cursor BC000X18 */
         pr_default.execute(13, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A2073ContratoServicos_QtdRmn = BC000X18_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = BC000X18_n2073ContratoServicos_QtdRmn[0];
         }
         else
         {
            A2073ContratoServicos_QtdRmn = 0;
            n2073ContratoServicos_QtdRmn = false;
         }
         pr_default.close(13);
         /* Using cursor BC000X9 */
         pr_default.execute(6, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "CONTRATO_CODIGO");
            AnyError = 1;
         }
         A77Contrato_Numero = BC000X9_A77Contrato_Numero[0];
         A78Contrato_NumeroAta = BC000X9_A78Contrato_NumeroAta[0];
         n78Contrato_NumeroAta = BC000X9_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = BC000X9_A79Contrato_Ano[0];
         A116Contrato_ValorUnidadeContratacao = BC000X9_A116Contrato_ValorUnidadeContratacao[0];
         A453Contrato_IndiceDivergencia = BC000X9_A453Contrato_IndiceDivergencia[0];
         A39Contratada_Codigo = BC000X9_A39Contratada_Codigo[0];
         A75Contrato_AreaTrabalhoCod = BC000X9_A75Contrato_AreaTrabalhoCod[0];
         pr_default.close(6);
         /* Using cursor BC000X12 */
         pr_default.execute(9, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A516Contratada_TipoFabrica = BC000X12_A516Contratada_TipoFabrica[0];
         A40Contratada_PessoaCod = BC000X12_A40Contratada_PessoaCod[0];
         pr_default.close(9);
         /* Using cursor BC000X13 */
         pr_default.execute(10, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = BC000X13_A41Contratada_PessoaNom[0];
         n41Contratada_PessoaNom = BC000X13_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = BC000X13_A42Contratada_PessoaCNPJ[0];
         n42Contratada_PessoaCNPJ = BC000X13_n42Contratada_PessoaCNPJ[0];
         pr_default.close(10);
         /* Using cursor BC000X8 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
         }
         A608Servico_Nome = BC000X8_A608Servico_Nome[0];
         A605Servico_Sigla = BC000X8_A605Servico_Sigla[0];
         A1061Servico_Tela = BC000X8_A1061Servico_Tela[0];
         n1061Servico_Tela = BC000X8_n1061Servico_Tela[0];
         A632Servico_Ativo = BC000X8_A632Servico_Ativo[0];
         A156Servico_Descricao = BC000X8_A156Servico_Descricao[0];
         n156Servico_Descricao = BC000X8_n156Servico_Descricao[0];
         A2092Servico_IsOrigemReferencia = BC000X8_A2092Servico_IsOrigemReferencia[0];
         A631Servico_Vinculado = BC000X8_A631Servico_Vinculado[0];
         n631Servico_Vinculado = BC000X8_n631Servico_Vinculado[0];
         A157ServicoGrupo_Codigo = BC000X8_A157ServicoGrupo_Codigo[0];
         pr_default.close(5);
         AV32Servico_Nome = A608Servico_Nome;
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
         /* Using cursor BC000X11 */
         pr_default.execute(8, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A158ServicoGrupo_Descricao = BC000X11_A158ServicoGrupo_Descricao[0];
         pr_default.close(8);
         GXt_int2 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
         A1551Servico_Responsavel = GXt_int2;
         A827ContratoServicos_ServicoCod = A155Servico_Codigo;
         GXt_int3 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
         A640Servico_Vinculados = GXt_int3;
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV30Alias = A1858ContratoServicos_Alias;
         }
         /* Using cursor BC000X10 */
         pr_default.execute(7, new Object[] {A1212ContratoServicos_UnidadeContratada});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos_Unidade Medicao'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_UNIDADECONTRATADA");
            AnyError = 1;
         }
         A2132ContratoServicos_UndCntNome = BC000X10_A2132ContratoServicos_UndCntNome[0];
         n2132ContratoServicos_UndCntNome = BC000X10_n2132ContratoServicos_UndCntNome[0];
         A1712ContratoServicos_UndCntSgl = BC000X10_A1712ContratoServicos_UndCntSgl[0];
         n1712ContratoServicos_UndCntSgl = BC000X10_n1712ContratoServicos_UndCntSgl[0];
         pr_default.close(7);
         AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
         if ( ! ( ( StringUtil.StrCmp(A639ContratoServicos_LocalExec, "A") == 0 ) || ( StringUtil.StrCmp(A639ContratoServicos_LocalExec, "E") == 0 ) || ( StringUtil.StrCmp(A639ContratoServicos_LocalExec, "O") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Local de Execu��o fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A1454ContratoServicos_PrazoTpDias, "U") == 0 ) || ( StringUtil.StrCmp(A1454ContratoServicos_PrazoTpDias, "C") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias)) ) )
         {
            GX_msglist.addItem("Campo Tipo de dias fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         AV26CemPorCento = (bool)(((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P")==0)&&(A1224ContratoServicos_PrazoCorrecao==100)));
         if ( AV26CemPorCento )
         {
            A1225ContratoServicos_PrazoCorrecaoTipo = "I";
            n1225ContratoServicos_PrazoCorrecaoTipo = false;
         }
         if ( ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I") == 0 ) || AV26CemPorCento )
         {
            A1224ContratoServicos_PrazoCorrecao = 0;
            n1224ContratoServicos_PrazoCorrecao = false;
         }
         if ( ! ( ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "B") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "S") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "E") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "A") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "R") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "C") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "D") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "H") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "O") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "P") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "L") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "X") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "N") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "J") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "I") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "T") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "Q") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "G") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "M") == 0 ) || ( StringUtil.StrCmp(A1325ContratoServicos_StatusPagFnc, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1325ContratoServicos_StatusPagFnc)) ) )
         {
            GX_msglist.addItem("Campo Pagar ao funcion�rio em fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A1531ContratoServicos_TipoHierarquia == 1 ) || ( A1531ContratoServicos_TipoHierarquia == 2 ) || (0==A1531ContratoServicos_TipoHierarquia) ) )
         {
            GX_msglist.addItem("Campo Tipo de Hierarquia fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1455ContratoServicos_IndiceDivergencia) && ( Gx_BScreen == 0 ) )
         {
            A1455ContratoServicos_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            n1455ContratoServicos_IndiceDivergencia = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A557Servico_VlrUnidadeContratada) && ( Gx_BScreen == 0 ) )
         {
            A557Servico_VlrUnidadeContratada = A116Contrato_ValorUnidadeContratacao;
         }
      }

      protected void CloseExtendedTableCursors0X34( )
      {
         pr_default.close(4);
         pr_default.close(11);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(6);
         pr_default.close(9);
         pr_default.close(10);
         pr_default.close(5);
         pr_default.close(8);
         pr_default.close(7);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0X34( )
      {
         /* Using cursor BC000X22 */
         pr_default.execute(15, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound34 = 1;
         }
         else
         {
            RcdFound34 = 0;
         }
         pr_default.close(15);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000X5 */
         pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            ZM0X34( 34) ;
            RcdFound34 = 1;
            A160ContratoServicos_Codigo = BC000X5_A160ContratoServicos_Codigo[0];
            A1858ContratoServicos_Alias = BC000X5_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = BC000X5_n1858ContratoServicos_Alias[0];
            A555Servico_QtdContratada = BC000X5_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = BC000X5_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = BC000X5_A557Servico_VlrUnidadeContratada[0];
            A558Servico_Percentual = BC000X5_A558Servico_Percentual[0];
            n558Servico_Percentual = BC000X5_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = BC000X5_A607ServicoContrato_Faturamento[0];
            A639ContratoServicos_LocalExec = BC000X5_A639ContratoServicos_LocalExec[0];
            A868ContratoServicos_TipoVnc = BC000X5_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = BC000X5_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = BC000X5_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = BC000X5_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = BC000X5_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = BC000X5_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = BC000X5_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = BC000X5_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = BC000X5_A1153ContratoServicos_PrazoResposta[0];
            n1153ContratoServicos_PrazoResposta = BC000X5_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = BC000X5_A1181ContratoServicos_PrazoGarantia[0];
            n1181ContratoServicos_PrazoGarantia = BC000X5_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = BC000X5_A1182ContratoServicos_PrazoAtendeGarantia[0];
            n1182ContratoServicos_PrazoAtendeGarantia = BC000X5_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = BC000X5_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = BC000X5_n1224ContratoServicos_PrazoCorrecao[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = BC000X5_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = BC000X5_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1649ContratoServicos_PrazoInicio = BC000X5_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = BC000X5_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = BC000X5_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = BC000X5_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = BC000X5_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = BC000X5_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = BC000X5_A1217ContratoServicos_EspelhaAceite[0];
            n1217ContratoServicos_EspelhaAceite = BC000X5_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = BC000X5_A1266ContratoServicos_Momento[0];
            n1266ContratoServicos_Momento = BC000X5_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = BC000X5_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = BC000X5_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = BC000X5_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = BC000X5_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = BC000X5_A1341ContratoServicos_FatorCnvUndCnt[0];
            n1341ContratoServicos_FatorCnvUndCnt = BC000X5_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = BC000X5_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = BC000X5_n1397ContratoServicos_NaoRequerAtr[0];
            A1455ContratoServicos_IndiceDivergencia = BC000X5_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = BC000X5_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = BC000X5_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = BC000X5_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = BC000X5_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = BC000X5_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = BC000X5_A1502ContratoServicos_TmpEstCrr[0];
            n1502ContratoServicos_TmpEstCrr = BC000X5_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = BC000X5_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = BC000X5_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = BC000X5_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = BC000X5_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = BC000X5_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = BC000X5_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = BC000X5_A1539ContratoServicos_PercCnc[0];
            n1539ContratoServicos_PercCnc = BC000X5_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = BC000X5_A638ContratoServicos_Ativo[0];
            A1723ContratoServicos_CodigoFiscal = BC000X5_A1723ContratoServicos_CodigoFiscal[0];
            n1723ContratoServicos_CodigoFiscal = BC000X5_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = BC000X5_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = BC000X5_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = BC000X5_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = BC000X5_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = BC000X5_A2094ContratoServicos_SolicitaGestorSistema[0];
            A155Servico_Codigo = BC000X5_A155Servico_Codigo[0];
            A74Contrato_Codigo = BC000X5_A74Contrato_Codigo[0];
            A1212ContratoServicos_UnidadeContratada = BC000X5_A1212ContratoServicos_UnidadeContratada[0];
            O557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            sMode34 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0X34( ) ;
            if ( AnyError == 1 )
            {
               RcdFound34 = 0;
               InitializeNonKey0X34( ) ;
            }
            Gx_mode = sMode34;
         }
         else
         {
            RcdFound34 = 0;
            InitializeNonKey0X34( ) ;
            sMode34 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode34;
         }
         pr_default.close(3);
      }

      protected void getEqualNoModal( )
      {
         GetKey0X34( ) ;
         if ( RcdFound34 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0X0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0X34( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000X4 */
            pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(2) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(2) == 101) || ( StringUtil.StrCmp(Z1858ContratoServicos_Alias, BC000X4_A1858ContratoServicos_Alias[0]) != 0 ) || ( Z555Servico_QtdContratada != BC000X4_A555Servico_QtdContratada[0] ) || ( Z557Servico_VlrUnidadeContratada != BC000X4_A557Servico_VlrUnidadeContratada[0] ) || ( Z558Servico_Percentual != BC000X4_A558Servico_Percentual[0] ) || ( StringUtil.StrCmp(Z607ServicoContrato_Faturamento, BC000X4_A607ServicoContrato_Faturamento[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z639ContratoServicos_LocalExec, BC000X4_A639ContratoServicos_LocalExec[0]) != 0 ) || ( StringUtil.StrCmp(Z868ContratoServicos_TipoVnc, BC000X4_A868ContratoServicos_TipoVnc[0]) != 0 ) || ( Z888ContratoServicos_HmlSemCnf != BC000X4_A888ContratoServicos_HmlSemCnf[0] ) || ( StringUtil.StrCmp(Z1454ContratoServicos_PrazoTpDias, BC000X4_A1454ContratoServicos_PrazoTpDias[0]) != 0 ) || ( Z1152ContratoServicos_PrazoAnalise != BC000X4_A1152ContratoServicos_PrazoAnalise[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1153ContratoServicos_PrazoResposta != BC000X4_A1153ContratoServicos_PrazoResposta[0] ) || ( Z1181ContratoServicos_PrazoGarantia != BC000X4_A1181ContratoServicos_PrazoGarantia[0] ) || ( Z1182ContratoServicos_PrazoAtendeGarantia != BC000X4_A1182ContratoServicos_PrazoAtendeGarantia[0] ) || ( Z1224ContratoServicos_PrazoCorrecao != BC000X4_A1224ContratoServicos_PrazoCorrecao[0] ) || ( StringUtil.StrCmp(Z1225ContratoServicos_PrazoCorrecaoTipo, BC000X4_A1225ContratoServicos_PrazoCorrecaoTipo[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1649ContratoServicos_PrazoInicio != BC000X4_A1649ContratoServicos_PrazoInicio[0] ) || ( Z1190ContratoServicos_PrazoImediato != BC000X4_A1190ContratoServicos_PrazoImediato[0] ) || ( Z1191ContratoServicos_Produtividade != BC000X4_A1191ContratoServicos_Produtividade[0] ) || ( Z1217ContratoServicos_EspelhaAceite != BC000X4_A1217ContratoServicos_EspelhaAceite[0] ) || ( StringUtil.StrCmp(Z1266ContratoServicos_Momento, BC000X4_A1266ContratoServicos_Momento[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1325ContratoServicos_StatusPagFnc, BC000X4_A1325ContratoServicos_StatusPagFnc[0]) != 0 ) || ( Z1340ContratoServicos_QntUntCns != BC000X4_A1340ContratoServicos_QntUntCns[0] ) || ( Z1341ContratoServicos_FatorCnvUndCnt != BC000X4_A1341ContratoServicos_FatorCnvUndCnt[0] ) || ( Z1397ContratoServicos_NaoRequerAtr != BC000X4_A1397ContratoServicos_NaoRequerAtr[0] ) || ( Z1455ContratoServicos_IndiceDivergencia != BC000X4_A1455ContratoServicos_IndiceDivergencia[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1516ContratoServicos_TmpEstAnl != BC000X4_A1516ContratoServicos_TmpEstAnl[0] ) || ( Z1501ContratoServicos_TmpEstExc != BC000X4_A1501ContratoServicos_TmpEstExc[0] ) || ( Z1502ContratoServicos_TmpEstCrr != BC000X4_A1502ContratoServicos_TmpEstCrr[0] ) || ( Z1531ContratoServicos_TipoHierarquia != BC000X4_A1531ContratoServicos_TipoHierarquia[0] ) || ( Z1537ContratoServicos_PercTmp != BC000X4_A1537ContratoServicos_PercTmp[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1538ContratoServicos_PercPgm != BC000X4_A1538ContratoServicos_PercPgm[0] ) || ( Z1539ContratoServicos_PercCnc != BC000X4_A1539ContratoServicos_PercCnc[0] ) || ( Z638ContratoServicos_Ativo != BC000X4_A638ContratoServicos_Ativo[0] ) || ( StringUtil.StrCmp(Z1723ContratoServicos_CodigoFiscal, BC000X4_A1723ContratoServicos_CodigoFiscal[0]) != 0 ) || ( Z1799ContratoServicos_LimiteProposta != BC000X4_A1799ContratoServicos_LimiteProposta[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2074ContratoServicos_CalculoRmn != BC000X4_A2074ContratoServicos_CalculoRmn[0] ) || ( Z2094ContratoServicos_SolicitaGestorSistema != BC000X4_A2094ContratoServicos_SolicitaGestorSistema[0] ) || ( Z155Servico_Codigo != BC000X4_A155Servico_Codigo[0] ) || ( Z74Contrato_Codigo != BC000X4_A74Contrato_Codigo[0] ) || ( Z1212ContratoServicos_UnidadeContratada != BC000X4_A1212ContratoServicos_UnidadeContratada[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0X34( )
      {
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0X34( 0) ;
            CheckOptimisticConcurrency0X34( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X34( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0X34( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000X23 */
                     pr_default.execute(16, new Object[] {n1858ContratoServicos_Alias, A1858ContratoServicos_Alias, n555Servico_QtdContratada, A555Servico_QtdContratada, A557Servico_VlrUnidadeContratada, n558Servico_Percentual, A558Servico_Percentual, A607ServicoContrato_Faturamento, A639ContratoServicos_LocalExec, n868ContratoServicos_TipoVnc, A868ContratoServicos_TipoVnc, n888ContratoServicos_HmlSemCnf, A888ContratoServicos_HmlSemCnf, n1454ContratoServicos_PrazoTpDias, A1454ContratoServicos_PrazoTpDias, n1152ContratoServicos_PrazoAnalise, A1152ContratoServicos_PrazoAnalise, n1153ContratoServicos_PrazoResposta, A1153ContratoServicos_PrazoResposta, n1181ContratoServicos_PrazoGarantia, A1181ContratoServicos_PrazoGarantia, n1182ContratoServicos_PrazoAtendeGarantia, A1182ContratoServicos_PrazoAtendeGarantia, n1224ContratoServicos_PrazoCorrecao, A1224ContratoServicos_PrazoCorrecao, n1225ContratoServicos_PrazoCorrecaoTipo, A1225ContratoServicos_PrazoCorrecaoTipo, n1649ContratoServicos_PrazoInicio, A1649ContratoServicos_PrazoInicio, n1190ContratoServicos_PrazoImediato, A1190ContratoServicos_PrazoImediato, n1191ContratoServicos_Produtividade, A1191ContratoServicos_Produtividade, n1217ContratoServicos_EspelhaAceite, A1217ContratoServicos_EspelhaAceite, n1266ContratoServicos_Momento, A1266ContratoServicos_Momento, n1325ContratoServicos_StatusPagFnc, A1325ContratoServicos_StatusPagFnc, n1340ContratoServicos_QntUntCns, A1340ContratoServicos_QntUntCns, n1341ContratoServicos_FatorCnvUndCnt, A1341ContratoServicos_FatorCnvUndCnt, n1397ContratoServicos_NaoRequerAtr, A1397ContratoServicos_NaoRequerAtr, n1455ContratoServicos_IndiceDivergencia, A1455ContratoServicos_IndiceDivergencia, n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, n1502ContratoServicos_TmpEstCrr, A1502ContratoServicos_TmpEstCrr, n1531ContratoServicos_TipoHierarquia, A1531ContratoServicos_TipoHierarquia, n1537ContratoServicos_PercTmp, A1537ContratoServicos_PercTmp, n1538ContratoServicos_PercPgm, A1538ContratoServicos_PercPgm, n1539ContratoServicos_PercCnc, A1539ContratoServicos_PercCnc, A638ContratoServicos_Ativo, n1723ContratoServicos_CodigoFiscal, A1723ContratoServicos_CodigoFiscal, n1799ContratoServicos_LimiteProposta, A1799ContratoServicos_LimiteProposta, n2074ContratoServicos_CalculoRmn, A2074ContratoServicos_CalculoRmn, A2094ContratoServicos_SolicitaGestorSistema, A155Servico_Codigo, A74Contrato_Codigo, A1212ContratoServicos_UnidadeContratada});
                     A160ContratoServicos_Codigo = BC000X23_A160ContratoServicos_Codigo[0];
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0X34( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0X34( ) ;
            }
            EndLevel0X34( ) ;
         }
         CloseExtendedTableCursors0X34( ) ;
      }

      protected void Update0X34( )
      {
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X34( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X34( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0X34( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000X24 */
                     pr_default.execute(17, new Object[] {n1858ContratoServicos_Alias, A1858ContratoServicos_Alias, n555Servico_QtdContratada, A555Servico_QtdContratada, A557Servico_VlrUnidadeContratada, n558Servico_Percentual, A558Servico_Percentual, A607ServicoContrato_Faturamento, A639ContratoServicos_LocalExec, n868ContratoServicos_TipoVnc, A868ContratoServicos_TipoVnc, n888ContratoServicos_HmlSemCnf, A888ContratoServicos_HmlSemCnf, n1454ContratoServicos_PrazoTpDias, A1454ContratoServicos_PrazoTpDias, n1152ContratoServicos_PrazoAnalise, A1152ContratoServicos_PrazoAnalise, n1153ContratoServicos_PrazoResposta, A1153ContratoServicos_PrazoResposta, n1181ContratoServicos_PrazoGarantia, A1181ContratoServicos_PrazoGarantia, n1182ContratoServicos_PrazoAtendeGarantia, A1182ContratoServicos_PrazoAtendeGarantia, n1224ContratoServicos_PrazoCorrecao, A1224ContratoServicos_PrazoCorrecao, n1225ContratoServicos_PrazoCorrecaoTipo, A1225ContratoServicos_PrazoCorrecaoTipo, n1649ContratoServicos_PrazoInicio, A1649ContratoServicos_PrazoInicio, n1190ContratoServicos_PrazoImediato, A1190ContratoServicos_PrazoImediato, n1191ContratoServicos_Produtividade, A1191ContratoServicos_Produtividade, n1217ContratoServicos_EspelhaAceite, A1217ContratoServicos_EspelhaAceite, n1266ContratoServicos_Momento, A1266ContratoServicos_Momento, n1325ContratoServicos_StatusPagFnc, A1325ContratoServicos_StatusPagFnc, n1340ContratoServicos_QntUntCns, A1340ContratoServicos_QntUntCns, n1341ContratoServicos_FatorCnvUndCnt, A1341ContratoServicos_FatorCnvUndCnt, n1397ContratoServicos_NaoRequerAtr, A1397ContratoServicos_NaoRequerAtr, n1455ContratoServicos_IndiceDivergencia, A1455ContratoServicos_IndiceDivergencia, n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, n1502ContratoServicos_TmpEstCrr, A1502ContratoServicos_TmpEstCrr, n1531ContratoServicos_TipoHierarquia, A1531ContratoServicos_TipoHierarquia, n1537ContratoServicos_PercTmp, A1537ContratoServicos_PercTmp, n1538ContratoServicos_PercPgm, A1538ContratoServicos_PercPgm, n1539ContratoServicos_PercCnc, A1539ContratoServicos_PercCnc, A638ContratoServicos_Ativo, n1723ContratoServicos_CodigoFiscal, A1723ContratoServicos_CodigoFiscal, n1799ContratoServicos_LimiteProposta, A1799ContratoServicos_LimiteProposta, n2074ContratoServicos_CalculoRmn, A2074ContratoServicos_CalculoRmn, A2094ContratoServicos_SolicitaGestorSistema, A155Servico_Codigo, A74Contrato_Codigo, A1212ContratoServicos_UnidadeContratada, A160ContratoServicos_Codigo});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0X34( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0X34( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0X34( ) ;
         }
         CloseExtendedTableCursors0X34( ) ;
      }

      protected void DeferredUpdate0X34( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0X34( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0X34( ) ;
            AfterConfirm0X34( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0X34( ) ;
               if ( AnyError == 0 )
               {
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  ScanKeyStart0X228( ) ;
                  while ( RcdFound228 != 0 )
                  {
                     getByPrimaryKey0X228( ) ;
                     Delete0X228( ) ;
                     ScanKeyNext0X228( ) ;
                     O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
                     n2073ContratoServicos_QtdRmn = false;
                  }
                  ScanKeyEnd0X228( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000X25 */
                     pr_default.execute(18, new Object[] {A160ContratoServicos_Codigo});
                     pr_default.close(18);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode34 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0X34( ) ;
         Gx_mode = sMode34;
      }

      protected void OnDeleteControls0X34( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000X27 */
            pr_default.execute(19, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               A911ContratoServicos_Prazos = BC000X27_A911ContratoServicos_Prazos[0];
               n911ContratoServicos_Prazos = BC000X27_n911ContratoServicos_Prazos[0];
            }
            else
            {
               A911ContratoServicos_Prazos = 0;
               n911ContratoServicos_Prazos = false;
            }
            pr_default.close(19);
            /* Using cursor BC000X28 */
            pr_default.execute(20, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               A913ContratoServicos_PrazoTipo = BC000X28_A913ContratoServicos_PrazoTipo[0];
               n913ContratoServicos_PrazoTipo = BC000X28_n913ContratoServicos_PrazoTipo[0];
               A914ContratoServicos_PrazoDias = BC000X28_A914ContratoServicos_PrazoDias[0];
               n914ContratoServicos_PrazoDias = BC000X28_n914ContratoServicos_PrazoDias[0];
            }
            else
            {
               A914ContratoServicos_PrazoDias = 0;
               n914ContratoServicos_PrazoDias = false;
               A913ContratoServicos_PrazoTipo = "";
               n913ContratoServicos_PrazoTipo = false;
            }
            pr_default.close(20);
            /* Using cursor BC000X30 */
            pr_default.execute(21, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               A1377ContratoServicos_Indicadores = BC000X30_A1377ContratoServicos_Indicadores[0];
               n1377ContratoServicos_Indicadores = BC000X30_n1377ContratoServicos_Indicadores[0];
            }
            else
            {
               A1377ContratoServicos_Indicadores = 0;
               n1377ContratoServicos_Indicadores = false;
            }
            pr_default.close(21);
            /* Using cursor BC000X32 */
            pr_default.execute(22, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               A2073ContratoServicos_QtdRmn = BC000X32_A2073ContratoServicos_QtdRmn[0];
               n2073ContratoServicos_QtdRmn = BC000X32_n2073ContratoServicos_QtdRmn[0];
            }
            else
            {
               A2073ContratoServicos_QtdRmn = 0;
               n2073ContratoServicos_QtdRmn = false;
            }
            pr_default.close(22);
            /* Using cursor BC000X33 */
            pr_default.execute(23, new Object[] {A74Contrato_Codigo});
            A77Contrato_Numero = BC000X33_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000X33_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000X33_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000X33_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = BC000X33_A116Contrato_ValorUnidadeContratacao[0];
            A453Contrato_IndiceDivergencia = BC000X33_A453Contrato_IndiceDivergencia[0];
            A39Contratada_Codigo = BC000X33_A39Contratada_Codigo[0];
            A75Contrato_AreaTrabalhoCod = BC000X33_A75Contrato_AreaTrabalhoCod[0];
            pr_default.close(23);
            /* Using cursor BC000X34 */
            pr_default.execute(24, new Object[] {A39Contratada_Codigo});
            A516Contratada_TipoFabrica = BC000X34_A516Contratada_TipoFabrica[0];
            A40Contratada_PessoaCod = BC000X34_A40Contratada_PessoaCod[0];
            pr_default.close(24);
            /* Using cursor BC000X35 */
            pr_default.execute(25, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = BC000X35_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000X35_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000X35_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000X35_n42Contratada_PessoaCNPJ[0];
            pr_default.close(25);
            /* Using cursor BC000X36 */
            pr_default.execute(26, new Object[] {A155Servico_Codigo});
            A608Servico_Nome = BC000X36_A608Servico_Nome[0];
            A605Servico_Sigla = BC000X36_A605Servico_Sigla[0];
            A1061Servico_Tela = BC000X36_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000X36_n1061Servico_Tela[0];
            A632Servico_Ativo = BC000X36_A632Servico_Ativo[0];
            A156Servico_Descricao = BC000X36_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000X36_n156Servico_Descricao[0];
            A2092Servico_IsOrigemReferencia = BC000X36_A2092Servico_IsOrigemReferencia[0];
            A631Servico_Vinculado = BC000X36_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000X36_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = BC000X36_A157ServicoGrupo_Codigo[0];
            pr_default.close(26);
            AV32Servico_Nome = A608Servico_Nome;
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            A826ContratoServicos_ServicoSigla = A605Servico_Sigla;
            /* Using cursor BC000X37 */
            pr_default.execute(27, new Object[] {A157ServicoGrupo_Codigo});
            A158ServicoGrupo_Descricao = BC000X37_A158ServicoGrupo_Descricao[0];
            pr_default.close(27);
            GXt_int2 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int2) ;
            A1551Servico_Responsavel = GXt_int2;
            A827ContratoServicos_ServicoCod = A155Servico_Codigo;
            GXt_int3 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int3) ;
            A640Servico_Vinculados = GXt_int3;
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV30Alias = A1858ContratoServicos_Alias;
            }
            /* Using cursor BC000X38 */
            pr_default.execute(28, new Object[] {A1212ContratoServicos_UnidadeContratada});
            A2132ContratoServicos_UndCntNome = BC000X38_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = BC000X38_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = BC000X38_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = BC000X38_n1712ContratoServicos_UndCntSgl[0];
            pr_default.close(28);
            AV21Servico_Percentual = (decimal)(A558Servico_Percentual*100);
            AV26CemPorCento = (bool)(((StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P")==0)&&(A1224ContratoServicos_PrazoCorrecao==100)));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000X39 */
            pr_default.execute(29, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor BC000X40 */
            pr_default.execute(30, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Custo do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor BC000X41 */
            pr_default.execute(31, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Prioridade"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor BC000X42 */
            pr_default.execute(32, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos Indicador"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor BC000X43 */
            pr_default.execute(33, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Agenda Atendimento"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor BC000X44 */
            pr_default.execute(34, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tela associada"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
            /* Using cursor BC000X45 */
            pr_default.execute(35, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Regra de Vinculo entre Servi�os"+" ("+"Contrato Servicos Vnc_Srv Vnc Contrato Servicos"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor BC000X46 */
            pr_default.execute(36, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Regra de Vinculo entre Servi�os"+" ("+"Contrato Servicos Vnc_ContratoServicos"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor BC000X47 */
            pr_default.execute(37, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Prazo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor BC000X48 */
            pr_default.execute(38, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Unidades de Convers�o do Servi�os do Contrato"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor BC000X49 */
            pr_default.execute(39, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor BC000X50 */
            pr_default.execute(40, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servicos (De Para)"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor BC000X51 */
            pr_default.execute(41, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
         }
      }

      protected void ProcessNestedLevel0X228( )
      {
         s2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         nGXsfl_228_idx = 0;
         while ( nGXsfl_228_idx < bcContratoServicos.gxTpr_Rmn.Count )
         {
            ReadRow0X228( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound228 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_228 != 0 ) )
            {
               standaloneNotModal0X228( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert0X228( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete0X228( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update0X228( ) ;
                  }
               }
               O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
            }
            KeyVarsToRow228( ((SdtContratoServicos_Rmn)bcContratoServicos.gxTpr_Rmn.Item(nGXsfl_228_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_228_idx = 0;
            while ( nGXsfl_228_idx < bcContratoServicos.gxTpr_Rmn.Count )
            {
               ReadRow0X228( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound228 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcContratoServicos.gxTpr_Rmn.RemoveElement(nGXsfl_228_idx);
                  nGXsfl_228_idx = (short)(nGXsfl_228_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey0X228( ) ;
                  VarsToRow228( ((SdtContratoServicos_Rmn)bcContratoServicos.gxTpr_Rmn.Item(nGXsfl_228_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0X228( ) ;
         if ( AnyError != 0 )
         {
            O2073ContratoServicos_QtdRmn = s2073ContratoServicos_QtdRmn;
            n2073ContratoServicos_QtdRmn = false;
         }
         nRcdExists_228 = 0;
         nIsMod_228 = 0;
         Gxremove228 = 0;
      }

      protected void ProcessLevel0X34( )
      {
         /* Save parent mode. */
         sMode34 = Gx_mode;
         ProcessNestedLevel0X228( ) ;
         if ( AnyError != 0 )
         {
            O2073ContratoServicos_QtdRmn = s2073ContratoServicos_QtdRmn;
            n2073ContratoServicos_QtdRmn = false;
         }
         /* Restore parent mode. */
         Gx_mode = sMode34;
         /* ' Update level parameters */
      }

      protected void EndLevel0X34( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(2);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0X34( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0X34( )
      {
         /* Scan By routine */
         /* Using cursor BC000X54 */
         pr_default.execute(42, new Object[] {A160ContratoServicos_Codigo});
         RcdFound34 = 0;
         if ( (pr_default.getStatus(42) != 101) )
         {
            RcdFound34 = 1;
            A160ContratoServicos_Codigo = BC000X54_A160ContratoServicos_Codigo[0];
            A77Contrato_Numero = BC000X54_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000X54_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000X54_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000X54_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = BC000X54_A116Contrato_ValorUnidadeContratacao[0];
            A41Contratada_PessoaNom = BC000X54_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000X54_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000X54_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000X54_n42Contratada_PessoaCNPJ[0];
            A516Contratada_TipoFabrica = BC000X54_A516Contratada_TipoFabrica[0];
            A1858ContratoServicos_Alias = BC000X54_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = BC000X54_n1858ContratoServicos_Alias[0];
            A608Servico_Nome = BC000X54_A608Servico_Nome[0];
            A605Servico_Sigla = BC000X54_A605Servico_Sigla[0];
            A1061Servico_Tela = BC000X54_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000X54_n1061Servico_Tela[0];
            A632Servico_Ativo = BC000X54_A632Servico_Ativo[0];
            A156Servico_Descricao = BC000X54_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000X54_n156Servico_Descricao[0];
            A158ServicoGrupo_Descricao = BC000X54_A158ServicoGrupo_Descricao[0];
            A2092Servico_IsOrigemReferencia = BC000X54_A2092Servico_IsOrigemReferencia[0];
            A2132ContratoServicos_UndCntNome = BC000X54_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = BC000X54_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = BC000X54_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = BC000X54_n1712ContratoServicos_UndCntSgl[0];
            A555Servico_QtdContratada = BC000X54_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = BC000X54_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = BC000X54_A557Servico_VlrUnidadeContratada[0];
            A558Servico_Percentual = BC000X54_A558Servico_Percentual[0];
            n558Servico_Percentual = BC000X54_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = BC000X54_A607ServicoContrato_Faturamento[0];
            A639ContratoServicos_LocalExec = BC000X54_A639ContratoServicos_LocalExec[0];
            A868ContratoServicos_TipoVnc = BC000X54_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = BC000X54_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = BC000X54_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = BC000X54_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = BC000X54_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = BC000X54_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = BC000X54_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = BC000X54_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = BC000X54_A1153ContratoServicos_PrazoResposta[0];
            n1153ContratoServicos_PrazoResposta = BC000X54_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = BC000X54_A1181ContratoServicos_PrazoGarantia[0];
            n1181ContratoServicos_PrazoGarantia = BC000X54_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = BC000X54_A1182ContratoServicos_PrazoAtendeGarantia[0];
            n1182ContratoServicos_PrazoAtendeGarantia = BC000X54_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = BC000X54_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = BC000X54_n1224ContratoServicos_PrazoCorrecao[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = BC000X54_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = BC000X54_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1649ContratoServicos_PrazoInicio = BC000X54_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = BC000X54_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = BC000X54_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = BC000X54_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = BC000X54_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = BC000X54_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = BC000X54_A1217ContratoServicos_EspelhaAceite[0];
            n1217ContratoServicos_EspelhaAceite = BC000X54_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = BC000X54_A1266ContratoServicos_Momento[0];
            n1266ContratoServicos_Momento = BC000X54_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = BC000X54_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = BC000X54_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = BC000X54_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = BC000X54_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = BC000X54_A1341ContratoServicos_FatorCnvUndCnt[0];
            n1341ContratoServicos_FatorCnvUndCnt = BC000X54_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = BC000X54_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = BC000X54_n1397ContratoServicos_NaoRequerAtr[0];
            A453Contrato_IndiceDivergencia = BC000X54_A453Contrato_IndiceDivergencia[0];
            A1455ContratoServicos_IndiceDivergencia = BC000X54_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = BC000X54_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = BC000X54_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = BC000X54_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = BC000X54_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = BC000X54_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = BC000X54_A1502ContratoServicos_TmpEstCrr[0];
            n1502ContratoServicos_TmpEstCrr = BC000X54_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = BC000X54_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = BC000X54_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = BC000X54_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = BC000X54_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = BC000X54_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = BC000X54_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = BC000X54_A1539ContratoServicos_PercCnc[0];
            n1539ContratoServicos_PercCnc = BC000X54_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = BC000X54_A638ContratoServicos_Ativo[0];
            A1723ContratoServicos_CodigoFiscal = BC000X54_A1723ContratoServicos_CodigoFiscal[0];
            n1723ContratoServicos_CodigoFiscal = BC000X54_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = BC000X54_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = BC000X54_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = BC000X54_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = BC000X54_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = BC000X54_A2094ContratoServicos_SolicitaGestorSistema[0];
            A155Servico_Codigo = BC000X54_A155Servico_Codigo[0];
            A74Contrato_Codigo = BC000X54_A74Contrato_Codigo[0];
            A1212ContratoServicos_UnidadeContratada = BC000X54_A1212ContratoServicos_UnidadeContratada[0];
            A631Servico_Vinculado = BC000X54_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000X54_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = BC000X54_A157ServicoGrupo_Codigo[0];
            A39Contratada_Codigo = BC000X54_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = BC000X54_A40Contratada_PessoaCod[0];
            A75Contrato_AreaTrabalhoCod = BC000X54_A75Contrato_AreaTrabalhoCod[0];
            A913ContratoServicos_PrazoTipo = BC000X54_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = BC000X54_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = BC000X54_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = BC000X54_n914ContratoServicos_PrazoDias[0];
            A1377ContratoServicos_Indicadores = BC000X54_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = BC000X54_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = BC000X54_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = BC000X54_n2073ContratoServicos_QtdRmn[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0X34( )
      {
         /* Scan next routine */
         pr_default.readNext(42);
         RcdFound34 = 0;
         ScanKeyLoad0X34( ) ;
      }

      protected void ScanKeyLoad0X34( )
      {
         sMode34 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(42) != 101) )
         {
            RcdFound34 = 1;
            A160ContratoServicos_Codigo = BC000X54_A160ContratoServicos_Codigo[0];
            A77Contrato_Numero = BC000X54_A77Contrato_Numero[0];
            A78Contrato_NumeroAta = BC000X54_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = BC000X54_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = BC000X54_A79Contrato_Ano[0];
            A116Contrato_ValorUnidadeContratacao = BC000X54_A116Contrato_ValorUnidadeContratacao[0];
            A41Contratada_PessoaNom = BC000X54_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = BC000X54_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = BC000X54_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = BC000X54_n42Contratada_PessoaCNPJ[0];
            A516Contratada_TipoFabrica = BC000X54_A516Contratada_TipoFabrica[0];
            A1858ContratoServicos_Alias = BC000X54_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = BC000X54_n1858ContratoServicos_Alias[0];
            A608Servico_Nome = BC000X54_A608Servico_Nome[0];
            A605Servico_Sigla = BC000X54_A605Servico_Sigla[0];
            A1061Servico_Tela = BC000X54_A1061Servico_Tela[0];
            n1061Servico_Tela = BC000X54_n1061Servico_Tela[0];
            A632Servico_Ativo = BC000X54_A632Servico_Ativo[0];
            A156Servico_Descricao = BC000X54_A156Servico_Descricao[0];
            n156Servico_Descricao = BC000X54_n156Servico_Descricao[0];
            A158ServicoGrupo_Descricao = BC000X54_A158ServicoGrupo_Descricao[0];
            A2092Servico_IsOrigemReferencia = BC000X54_A2092Servico_IsOrigemReferencia[0];
            A2132ContratoServicos_UndCntNome = BC000X54_A2132ContratoServicos_UndCntNome[0];
            n2132ContratoServicos_UndCntNome = BC000X54_n2132ContratoServicos_UndCntNome[0];
            A1712ContratoServicos_UndCntSgl = BC000X54_A1712ContratoServicos_UndCntSgl[0];
            n1712ContratoServicos_UndCntSgl = BC000X54_n1712ContratoServicos_UndCntSgl[0];
            A555Servico_QtdContratada = BC000X54_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = BC000X54_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = BC000X54_A557Servico_VlrUnidadeContratada[0];
            A558Servico_Percentual = BC000X54_A558Servico_Percentual[0];
            n558Servico_Percentual = BC000X54_n558Servico_Percentual[0];
            A607ServicoContrato_Faturamento = BC000X54_A607ServicoContrato_Faturamento[0];
            A639ContratoServicos_LocalExec = BC000X54_A639ContratoServicos_LocalExec[0];
            A868ContratoServicos_TipoVnc = BC000X54_A868ContratoServicos_TipoVnc[0];
            n868ContratoServicos_TipoVnc = BC000X54_n868ContratoServicos_TipoVnc[0];
            A888ContratoServicos_HmlSemCnf = BC000X54_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = BC000X54_n888ContratoServicos_HmlSemCnf[0];
            A1454ContratoServicos_PrazoTpDias = BC000X54_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = BC000X54_n1454ContratoServicos_PrazoTpDias[0];
            A1152ContratoServicos_PrazoAnalise = BC000X54_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = BC000X54_n1152ContratoServicos_PrazoAnalise[0];
            A1153ContratoServicos_PrazoResposta = BC000X54_A1153ContratoServicos_PrazoResposta[0];
            n1153ContratoServicos_PrazoResposta = BC000X54_n1153ContratoServicos_PrazoResposta[0];
            A1181ContratoServicos_PrazoGarantia = BC000X54_A1181ContratoServicos_PrazoGarantia[0];
            n1181ContratoServicos_PrazoGarantia = BC000X54_n1181ContratoServicos_PrazoGarantia[0];
            A1182ContratoServicos_PrazoAtendeGarantia = BC000X54_A1182ContratoServicos_PrazoAtendeGarantia[0];
            n1182ContratoServicos_PrazoAtendeGarantia = BC000X54_n1182ContratoServicos_PrazoAtendeGarantia[0];
            A1224ContratoServicos_PrazoCorrecao = BC000X54_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = BC000X54_n1224ContratoServicos_PrazoCorrecao[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = BC000X54_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = BC000X54_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1649ContratoServicos_PrazoInicio = BC000X54_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = BC000X54_n1649ContratoServicos_PrazoInicio[0];
            A1190ContratoServicos_PrazoImediato = BC000X54_A1190ContratoServicos_PrazoImediato[0];
            n1190ContratoServicos_PrazoImediato = BC000X54_n1190ContratoServicos_PrazoImediato[0];
            A1191ContratoServicos_Produtividade = BC000X54_A1191ContratoServicos_Produtividade[0];
            n1191ContratoServicos_Produtividade = BC000X54_n1191ContratoServicos_Produtividade[0];
            A1217ContratoServicos_EspelhaAceite = BC000X54_A1217ContratoServicos_EspelhaAceite[0];
            n1217ContratoServicos_EspelhaAceite = BC000X54_n1217ContratoServicos_EspelhaAceite[0];
            A1266ContratoServicos_Momento = BC000X54_A1266ContratoServicos_Momento[0];
            n1266ContratoServicos_Momento = BC000X54_n1266ContratoServicos_Momento[0];
            A1325ContratoServicos_StatusPagFnc = BC000X54_A1325ContratoServicos_StatusPagFnc[0];
            n1325ContratoServicos_StatusPagFnc = BC000X54_n1325ContratoServicos_StatusPagFnc[0];
            A1340ContratoServicos_QntUntCns = BC000X54_A1340ContratoServicos_QntUntCns[0];
            n1340ContratoServicos_QntUntCns = BC000X54_n1340ContratoServicos_QntUntCns[0];
            A1341ContratoServicos_FatorCnvUndCnt = BC000X54_A1341ContratoServicos_FatorCnvUndCnt[0];
            n1341ContratoServicos_FatorCnvUndCnt = BC000X54_n1341ContratoServicos_FatorCnvUndCnt[0];
            A1397ContratoServicos_NaoRequerAtr = BC000X54_A1397ContratoServicos_NaoRequerAtr[0];
            n1397ContratoServicos_NaoRequerAtr = BC000X54_n1397ContratoServicos_NaoRequerAtr[0];
            A453Contrato_IndiceDivergencia = BC000X54_A453Contrato_IndiceDivergencia[0];
            A1455ContratoServicos_IndiceDivergencia = BC000X54_A1455ContratoServicos_IndiceDivergencia[0];
            n1455ContratoServicos_IndiceDivergencia = BC000X54_n1455ContratoServicos_IndiceDivergencia[0];
            A1516ContratoServicos_TmpEstAnl = BC000X54_A1516ContratoServicos_TmpEstAnl[0];
            n1516ContratoServicos_TmpEstAnl = BC000X54_n1516ContratoServicos_TmpEstAnl[0];
            A1501ContratoServicos_TmpEstExc = BC000X54_A1501ContratoServicos_TmpEstExc[0];
            n1501ContratoServicos_TmpEstExc = BC000X54_n1501ContratoServicos_TmpEstExc[0];
            A1502ContratoServicos_TmpEstCrr = BC000X54_A1502ContratoServicos_TmpEstCrr[0];
            n1502ContratoServicos_TmpEstCrr = BC000X54_n1502ContratoServicos_TmpEstCrr[0];
            A1531ContratoServicos_TipoHierarquia = BC000X54_A1531ContratoServicos_TipoHierarquia[0];
            n1531ContratoServicos_TipoHierarquia = BC000X54_n1531ContratoServicos_TipoHierarquia[0];
            A1537ContratoServicos_PercTmp = BC000X54_A1537ContratoServicos_PercTmp[0];
            n1537ContratoServicos_PercTmp = BC000X54_n1537ContratoServicos_PercTmp[0];
            A1538ContratoServicos_PercPgm = BC000X54_A1538ContratoServicos_PercPgm[0];
            n1538ContratoServicos_PercPgm = BC000X54_n1538ContratoServicos_PercPgm[0];
            A1539ContratoServicos_PercCnc = BC000X54_A1539ContratoServicos_PercCnc[0];
            n1539ContratoServicos_PercCnc = BC000X54_n1539ContratoServicos_PercCnc[0];
            A638ContratoServicos_Ativo = BC000X54_A638ContratoServicos_Ativo[0];
            A1723ContratoServicos_CodigoFiscal = BC000X54_A1723ContratoServicos_CodigoFiscal[0];
            n1723ContratoServicos_CodigoFiscal = BC000X54_n1723ContratoServicos_CodigoFiscal[0];
            A1799ContratoServicos_LimiteProposta = BC000X54_A1799ContratoServicos_LimiteProposta[0];
            n1799ContratoServicos_LimiteProposta = BC000X54_n1799ContratoServicos_LimiteProposta[0];
            A2074ContratoServicos_CalculoRmn = BC000X54_A2074ContratoServicos_CalculoRmn[0];
            n2074ContratoServicos_CalculoRmn = BC000X54_n2074ContratoServicos_CalculoRmn[0];
            A2094ContratoServicos_SolicitaGestorSistema = BC000X54_A2094ContratoServicos_SolicitaGestorSistema[0];
            A155Servico_Codigo = BC000X54_A155Servico_Codigo[0];
            A74Contrato_Codigo = BC000X54_A74Contrato_Codigo[0];
            A1212ContratoServicos_UnidadeContratada = BC000X54_A1212ContratoServicos_UnidadeContratada[0];
            A631Servico_Vinculado = BC000X54_A631Servico_Vinculado[0];
            n631Servico_Vinculado = BC000X54_n631Servico_Vinculado[0];
            A157ServicoGrupo_Codigo = BC000X54_A157ServicoGrupo_Codigo[0];
            A39Contratada_Codigo = BC000X54_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = BC000X54_A40Contratada_PessoaCod[0];
            A75Contrato_AreaTrabalhoCod = BC000X54_A75Contrato_AreaTrabalhoCod[0];
            A913ContratoServicos_PrazoTipo = BC000X54_A913ContratoServicos_PrazoTipo[0];
            n913ContratoServicos_PrazoTipo = BC000X54_n913ContratoServicos_PrazoTipo[0];
            A914ContratoServicos_PrazoDias = BC000X54_A914ContratoServicos_PrazoDias[0];
            n914ContratoServicos_PrazoDias = BC000X54_n914ContratoServicos_PrazoDias[0];
            A1377ContratoServicos_Indicadores = BC000X54_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = BC000X54_n1377ContratoServicos_Indicadores[0];
            A2073ContratoServicos_QtdRmn = BC000X54_A2073ContratoServicos_QtdRmn[0];
            n2073ContratoServicos_QtdRmn = BC000X54_n2073ContratoServicos_QtdRmn[0];
         }
         Gx_mode = sMode34;
      }

      protected void ScanKeyEnd0X34( )
      {
         pr_default.close(42);
      }

      protected void AfterConfirm0X34( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0X34( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0X34( )
      {
         /* Before Update Rules */
         new loadauditcontratoservicos(context ).execute(  "Y", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
      }

      protected void BeforeDelete0X34( )
      {
         /* Before Delete Rules */
         new loadauditcontratoservicos(context ).execute(  "Y", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
      }

      protected void BeforeComplete0X34( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratoservicos(context ).execute(  "N", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratoservicos(context ).execute(  "N", ref  AV29AuditingObject,  A160ContratoServicos_Codigo,  Gx_mode) ;
         }
      }

      protected void BeforeValidate0X34( )
      {
         /* Before Validate Rules */
         A558Servico_Percentual = (decimal)(AV21Servico_Percentual/ (decimal)(100));
         n558Servico_Percentual = false;
      }

      protected void DisableAttributes0X34( )
      {
      }

      protected void ZM0X228( short GX_JID )
      {
         if ( ( GX_JID == 45 ) || ( GX_JID == 0 ) )
         {
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z116Contrato_ValorUnidadeContratacao = A116Contrato_ValorUnidadeContratacao;
            Z75Contrato_AreaTrabalhoCod = A75Contrato_AreaTrabalhoCod;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            Z516Contratada_TipoFabrica = A516Contratada_TipoFabrica;
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1858ContratoServicos_Alias = A1858ContratoServicos_Alias;
            Z827ContratoServicos_ServicoCod = A827ContratoServicos_ServicoCod;
            Z608Servico_Nome = A608Servico_Nome;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1061Servico_Tela = A1061Servico_Tela;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z2107Servico_Identificacao = A2107Servico_Identificacao;
            Z826ContratoServicos_ServicoSigla = A826ContratoServicos_ServicoSigla;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z640Servico_Vinculados = A640Servico_Vinculados;
            Z1551Servico_Responsavel = A1551Servico_Responsavel;
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            Z1212ContratoServicos_UnidadeContratada = A1212ContratoServicos_UnidadeContratada;
            Z2132ContratoServicos_UndCntNome = A2132ContratoServicos_UndCntNome;
            Z1712ContratoServicos_UndCntSgl = A1712ContratoServicos_UndCntSgl;
            Z555Servico_QtdContratada = A555Servico_QtdContratada;
            Z557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
            Z558Servico_Percentual = A558Servico_Percentual;
            Z607ServicoContrato_Faturamento = A607ServicoContrato_Faturamento;
            Z639ContratoServicos_LocalExec = A639ContratoServicos_LocalExec;
            Z868ContratoServicos_TipoVnc = A868ContratoServicos_TipoVnc;
            Z888ContratoServicos_HmlSemCnf = A888ContratoServicos_HmlSemCnf;
            Z913ContratoServicos_PrazoTipo = A913ContratoServicos_PrazoTipo;
            Z914ContratoServicos_PrazoDias = A914ContratoServicos_PrazoDias;
            Z1454ContratoServicos_PrazoTpDias = A1454ContratoServicos_PrazoTpDias;
            Z911ContratoServicos_Prazos = A911ContratoServicos_Prazos;
            Z1377ContratoServicos_Indicadores = A1377ContratoServicos_Indicadores;
            Z1152ContratoServicos_PrazoAnalise = A1152ContratoServicos_PrazoAnalise;
            Z1153ContratoServicos_PrazoResposta = A1153ContratoServicos_PrazoResposta;
            Z1181ContratoServicos_PrazoGarantia = A1181ContratoServicos_PrazoGarantia;
            Z1182ContratoServicos_PrazoAtendeGarantia = A1182ContratoServicos_PrazoAtendeGarantia;
            Z1224ContratoServicos_PrazoCorrecao = A1224ContratoServicos_PrazoCorrecao;
            Z1225ContratoServicos_PrazoCorrecaoTipo = A1225ContratoServicos_PrazoCorrecaoTipo;
            Z1649ContratoServicos_PrazoInicio = A1649ContratoServicos_PrazoInicio;
            Z1190ContratoServicos_PrazoImediato = A1190ContratoServicos_PrazoImediato;
            Z1191ContratoServicos_Produtividade = A1191ContratoServicos_Produtividade;
            Z1217ContratoServicos_EspelhaAceite = A1217ContratoServicos_EspelhaAceite;
            Z1266ContratoServicos_Momento = A1266ContratoServicos_Momento;
            Z1325ContratoServicos_StatusPagFnc = A1325ContratoServicos_StatusPagFnc;
            Z1340ContratoServicos_QntUntCns = A1340ContratoServicos_QntUntCns;
            Z1341ContratoServicos_FatorCnvUndCnt = A1341ContratoServicos_FatorCnvUndCnt;
            Z1397ContratoServicos_NaoRequerAtr = A1397ContratoServicos_NaoRequerAtr;
            Z453Contrato_IndiceDivergencia = A453Contrato_IndiceDivergencia;
            Z1455ContratoServicos_IndiceDivergencia = A1455ContratoServicos_IndiceDivergencia;
            Z1516ContratoServicos_TmpEstAnl = A1516ContratoServicos_TmpEstAnl;
            Z1501ContratoServicos_TmpEstExc = A1501ContratoServicos_TmpEstExc;
            Z1502ContratoServicos_TmpEstCrr = A1502ContratoServicos_TmpEstCrr;
            Z1531ContratoServicos_TipoHierarquia = A1531ContratoServicos_TipoHierarquia;
            Z1537ContratoServicos_PercTmp = A1537ContratoServicos_PercTmp;
            Z1538ContratoServicos_PercPgm = A1538ContratoServicos_PercPgm;
            Z1539ContratoServicos_PercCnc = A1539ContratoServicos_PercCnc;
            Z638ContratoServicos_Ativo = A638ContratoServicos_Ativo;
            Z1723ContratoServicos_CodigoFiscal = A1723ContratoServicos_CodigoFiscal;
            Z1799ContratoServicos_LimiteProposta = A1799ContratoServicos_LimiteProposta;
            Z2074ContratoServicos_CalculoRmn = A2074ContratoServicos_CalculoRmn;
            Z2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
            Z2094ContratoServicos_SolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
         }
         if ( GX_JID == -45 )
         {
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            Z2070ContratoServicosRmn_Inicio = A2070ContratoServicosRmn_Inicio;
            Z2071ContratoServicosRmn_Fim = A2071ContratoServicosRmn_Fim;
            Z2072ContratoServicosRmn_Valor = A2072ContratoServicosRmn_Valor;
         }
      }

      protected void standaloneNotModal0X228( )
      {
      }

      protected void standaloneModal0X228( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
            n2073ContratoServicos_QtdRmn = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2069ContratoServicosRmn_Sequencial = A2073ContratoServicos_QtdRmn;
         }
      }

      protected void Load0X228( )
      {
         /* Using cursor BC000X55 */
         pr_default.execute(43, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
         if ( (pr_default.getStatus(43) != 101) )
         {
            RcdFound228 = 1;
            A2070ContratoServicosRmn_Inicio = BC000X55_A2070ContratoServicosRmn_Inicio[0];
            A2071ContratoServicosRmn_Fim = BC000X55_A2071ContratoServicosRmn_Fim[0];
            A2072ContratoServicosRmn_Valor = BC000X55_A2072ContratoServicosRmn_Valor[0];
            ZM0X228( -45) ;
         }
         pr_default.close(43);
         OnLoadActions0X228( ) ;
      }

      protected void OnLoadActions0X228( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
            n2073ContratoServicos_QtdRmn = false;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn-1);
                  n2073ContratoServicos_QtdRmn = false;
               }
            }
         }
      }

      protected void CheckExtendedTable0X228( )
      {
         Gx_BScreen = 1;
         standaloneModal0X228( ) ;
         Gx_BScreen = 0;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
            n2073ContratoServicos_QtdRmn = false;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
               n2073ContratoServicos_QtdRmn = false;
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn-1);
                  n2073ContratoServicos_QtdRmn = false;
               }
            }
         }
      }

      protected void CloseExtendedTableCursors0X228( )
      {
      }

      protected void enableDisable0X228( )
      {
      }

      protected void GetKey0X228( )
      {
         /* Using cursor BC000X56 */
         pr_default.execute(44, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
         if ( (pr_default.getStatus(44) != 101) )
         {
            RcdFound228 = 1;
         }
         else
         {
            RcdFound228 = 0;
         }
         pr_default.close(44);
      }

      protected void getByPrimaryKey0X228( )
      {
         /* Using cursor BC000X3 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0X228( 45) ;
            RcdFound228 = 1;
            InitializeNonKey0X228( ) ;
            A2069ContratoServicosRmn_Sequencial = BC000X3_A2069ContratoServicosRmn_Sequencial[0];
            A2070ContratoServicosRmn_Inicio = BC000X3_A2070ContratoServicosRmn_Inicio[0];
            A2071ContratoServicosRmn_Fim = BC000X3_A2071ContratoServicosRmn_Fim[0];
            A2072ContratoServicosRmn_Valor = BC000X3_A2072ContratoServicosRmn_Valor[0];
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
            sMode228 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal0X228( ) ;
            Load0X228( ) ;
            Gx_mode = sMode228;
         }
         else
         {
            RcdFound228 = 0;
            InitializeNonKey0X228( ) ;
            sMode228 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal0X228( ) ;
            Gx_mode = sMode228;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0X228( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0X228( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000X2 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosRmn"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2070ContratoServicosRmn_Inicio != BC000X2_A2070ContratoServicosRmn_Inicio[0] ) || ( Z2071ContratoServicosRmn_Fim != BC000X2_A2071ContratoServicosRmn_Fim[0] ) || ( Z2072ContratoServicosRmn_Valor != BC000X2_A2072ContratoServicosRmn_Valor[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosRmn"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0X228( )
      {
         BeforeValidate0X228( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X228( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0X228( 0) ;
            CheckOptimisticConcurrency0X228( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X228( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0X228( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000X57 */
                     pr_default.execute(45, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial, A2070ContratoServicosRmn_Inicio, A2071ContratoServicosRmn_Fim, A2072ContratoServicosRmn_Valor});
                     pr_default.close(45);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
                     if ( (pr_default.getStatus(45) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0X228( ) ;
            }
            EndLevel0X228( ) ;
         }
         CloseExtendedTableCursors0X228( ) ;
      }

      protected void Update0X228( )
      {
         BeforeValidate0X228( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0X228( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X228( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0X228( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0X228( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000X58 */
                     pr_default.execute(46, new Object[] {A2070ContratoServicosRmn_Inicio, A2071ContratoServicosRmn_Fim, A2072ContratoServicosRmn_Valor, A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
                     pr_default.close(46);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
                     if ( (pr_default.getStatus(46) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosRmn"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0X228( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey0X228( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0X228( ) ;
         }
         CloseExtendedTableCursors0X228( ) ;
      }

      protected void DeferredUpdate0X228( )
      {
      }

      protected void Delete0X228( )
      {
         Gx_mode = "DLT";
         BeforeValidate0X228( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0X228( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0X228( ) ;
            AfterConfirm0X228( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0X228( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000X59 */
                  pr_default.execute(47, new Object[] {A160ContratoServicos_Codigo, A2069ContratoServicosRmn_Sequencial});
                  pr_default.close(47);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosRmn") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode228 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0X228( ) ;
         Gx_mode = sMode228;
      }

      protected void OnDeleteControls0X228( )
      {
         standaloneModal0X228( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn+1);
               n2073ContratoServicos_QtdRmn = false;
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A2073ContratoServicos_QtdRmn = (short)(O2073ContratoServicos_QtdRmn-1);
                     n2073ContratoServicos_QtdRmn = false;
                  }
               }
            }
         }
      }

      protected void EndLevel0X228( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0X228( )
      {
         /* Scan By routine */
         /* Using cursor BC000X60 */
         pr_default.execute(48, new Object[] {A160ContratoServicos_Codigo});
         RcdFound228 = 0;
         if ( (pr_default.getStatus(48) != 101) )
         {
            RcdFound228 = 1;
            A2069ContratoServicosRmn_Sequencial = BC000X60_A2069ContratoServicosRmn_Sequencial[0];
            A2070ContratoServicosRmn_Inicio = BC000X60_A2070ContratoServicosRmn_Inicio[0];
            A2071ContratoServicosRmn_Fim = BC000X60_A2071ContratoServicosRmn_Fim[0];
            A2072ContratoServicosRmn_Valor = BC000X60_A2072ContratoServicosRmn_Valor[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0X228( )
      {
         /* Scan next routine */
         pr_default.readNext(48);
         RcdFound228 = 0;
         ScanKeyLoad0X228( ) ;
      }

      protected void ScanKeyLoad0X228( )
      {
         sMode228 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(48) != 101) )
         {
            RcdFound228 = 1;
            A2069ContratoServicosRmn_Sequencial = BC000X60_A2069ContratoServicosRmn_Sequencial[0];
            A2070ContratoServicosRmn_Inicio = BC000X60_A2070ContratoServicosRmn_Inicio[0];
            A2071ContratoServicosRmn_Fim = BC000X60_A2071ContratoServicosRmn_Fim[0];
            A2072ContratoServicosRmn_Valor = BC000X60_A2072ContratoServicosRmn_Valor[0];
         }
         Gx_mode = sMode228;
      }

      protected void ScanKeyEnd0X228( )
      {
         pr_default.close(48);
      }

      protected void AfterConfirm0X228( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0X228( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0X228( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0X228( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0X228( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0X228( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0X228( )
      {
      }

      protected void AddRow0X34( )
      {
         VarsToRow34( bcContratoServicos) ;
      }

      protected void ReadRow0X34( )
      {
         RowToVars34( bcContratoServicos, 1) ;
      }

      protected void AddRow0X228( )
      {
         SdtContratoServicos_Rmn obj228 ;
         obj228 = new SdtContratoServicos_Rmn(context);
         VarsToRow228( obj228) ;
         bcContratoServicos.gxTpr_Rmn.Add(obj228, 0);
         obj228.gxTpr_Mode = "UPD";
         obj228.gxTpr_Modified = 0;
      }

      protected void ReadRow0X228( )
      {
         nGXsfl_228_idx = (short)(nGXsfl_228_idx+1);
         RowToVars228( ((SdtContratoServicos_Rmn)bcContratoServicos.gxTpr_Rmn.Item(nGXsfl_228_idx)), 1) ;
      }

      protected void InitializeNonKey0X34( )
      {
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV21Servico_Percentual = 0;
         AV26CemPorCento = false;
         AV30Alias = "";
         AV32Servico_Nome = "";
         A826ContratoServicos_ServicoSigla = "";
         A2107Servico_Identificacao = "";
         A640Servico_Vinculados = 0;
         A827ContratoServicos_ServicoCod = 0;
         A1551Servico_Responsavel = 0;
         A911ContratoServicos_Prazos = 0;
         n911ContratoServicos_Prazos = false;
         A913ContratoServicos_PrazoTipo = "";
         n913ContratoServicos_PrazoTipo = false;
         A914ContratoServicos_PrazoDias = 0;
         n914ContratoServicos_PrazoDias = false;
         A74Contrato_Codigo = 0;
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         n78Contrato_NumeroAta = false;
         A79Contrato_Ano = 0;
         A116Contrato_ValorUnidadeContratacao = 0;
         A75Contrato_AreaTrabalhoCod = 0;
         A39Contratada_Codigo = 0;
         A40Contratada_PessoaCod = 0;
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         A516Contratada_TipoFabrica = "";
         A155Servico_Codigo = 0;
         A1858ContratoServicos_Alias = "";
         n1858ContratoServicos_Alias = false;
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         A1061Servico_Tela = "";
         n1061Servico_Tela = false;
         A632Servico_Ativo = false;
         A156Servico_Descricao = "";
         n156Servico_Descricao = false;
         A631Servico_Vinculado = 0;
         n631Servico_Vinculado = false;
         A157ServicoGrupo_Codigo = 0;
         A158ServicoGrupo_Descricao = "";
         A2092Servico_IsOrigemReferencia = false;
         A1212ContratoServicos_UnidadeContratada = 0;
         A2132ContratoServicos_UndCntNome = "";
         n2132ContratoServicos_UndCntNome = false;
         A1712ContratoServicos_UndCntSgl = "";
         n1712ContratoServicos_UndCntSgl = false;
         A555Servico_QtdContratada = 0;
         n555Servico_QtdContratada = false;
         A639ContratoServicos_LocalExec = "";
         A868ContratoServicos_TipoVnc = "";
         n868ContratoServicos_TipoVnc = false;
         A888ContratoServicos_HmlSemCnf = false;
         n888ContratoServicos_HmlSemCnf = false;
         A1377ContratoServicos_Indicadores = 0;
         n1377ContratoServicos_Indicadores = false;
         A1152ContratoServicos_PrazoAnalise = 0;
         n1152ContratoServicos_PrazoAnalise = false;
         A1153ContratoServicos_PrazoResposta = 0;
         n1153ContratoServicos_PrazoResposta = false;
         A1181ContratoServicos_PrazoGarantia = 0;
         n1181ContratoServicos_PrazoGarantia = false;
         A1182ContratoServicos_PrazoAtendeGarantia = 0;
         n1182ContratoServicos_PrazoAtendeGarantia = false;
         A1224ContratoServicos_PrazoCorrecao = 0;
         n1224ContratoServicos_PrazoCorrecao = false;
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         n1225ContratoServicos_PrazoCorrecaoTipo = false;
         A1190ContratoServicos_PrazoImediato = false;
         n1190ContratoServicos_PrazoImediato = false;
         A1191ContratoServicos_Produtividade = 0;
         n1191ContratoServicos_Produtividade = false;
         A1217ContratoServicos_EspelhaAceite = false;
         n1217ContratoServicos_EspelhaAceite = false;
         A1266ContratoServicos_Momento = "";
         n1266ContratoServicos_Momento = false;
         A1325ContratoServicos_StatusPagFnc = "";
         n1325ContratoServicos_StatusPagFnc = false;
         A1340ContratoServicos_QntUntCns = 0;
         n1340ContratoServicos_QntUntCns = false;
         A1341ContratoServicos_FatorCnvUndCnt = 0;
         n1341ContratoServicos_FatorCnvUndCnt = false;
         A1397ContratoServicos_NaoRequerAtr = false;
         n1397ContratoServicos_NaoRequerAtr = false;
         A453Contrato_IndiceDivergencia = 0;
         A1516ContratoServicos_TmpEstAnl = 0;
         n1516ContratoServicos_TmpEstAnl = false;
         A1501ContratoServicos_TmpEstExc = 0;
         n1501ContratoServicos_TmpEstExc = false;
         A1502ContratoServicos_TmpEstCrr = 0;
         n1502ContratoServicos_TmpEstCrr = false;
         A1531ContratoServicos_TipoHierarquia = 0;
         n1531ContratoServicos_TipoHierarquia = false;
         A1537ContratoServicos_PercTmp = 0;
         n1537ContratoServicos_PercTmp = false;
         A1538ContratoServicos_PercPgm = 0;
         n1538ContratoServicos_PercPgm = false;
         A1539ContratoServicos_PercCnc = 0;
         n1539ContratoServicos_PercCnc = false;
         A1723ContratoServicos_CodigoFiscal = "";
         n1723ContratoServicos_CodigoFiscal = false;
         A1799ContratoServicos_LimiteProposta = 0;
         n1799ContratoServicos_LimiteProposta = false;
         A2074ContratoServicos_CalculoRmn = 0;
         n2074ContratoServicos_CalculoRmn = false;
         A2073ContratoServicos_QtdRmn = 0;
         n2073ContratoServicos_QtdRmn = false;
         A557Servico_VlrUnidadeContratada = 0;
         A558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         A607ServicoContrato_Faturamento = "B";
         A1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
         A1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         A1455ContratoServicos_IndiceDivergencia = 0;
         n1455ContratoServicos_IndiceDivergencia = false;
         A638ContratoServicos_Ativo = true;
         A2094ContratoServicos_SolicitaGestorSistema = false;
         O2073ContratoServicos_QtdRmn = A2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
         O557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
         Z1858ContratoServicos_Alias = "";
         Z555Servico_QtdContratada = 0;
         Z557Servico_VlrUnidadeContratada = 0;
         Z558Servico_Percentual = 0;
         Z607ServicoContrato_Faturamento = "";
         Z639ContratoServicos_LocalExec = "";
         Z868ContratoServicos_TipoVnc = "";
         Z888ContratoServicos_HmlSemCnf = false;
         Z1454ContratoServicos_PrazoTpDias = "";
         Z1152ContratoServicos_PrazoAnalise = 0;
         Z1153ContratoServicos_PrazoResposta = 0;
         Z1181ContratoServicos_PrazoGarantia = 0;
         Z1182ContratoServicos_PrazoAtendeGarantia = 0;
         Z1224ContratoServicos_PrazoCorrecao = 0;
         Z1225ContratoServicos_PrazoCorrecaoTipo = "";
         Z1649ContratoServicos_PrazoInicio = 0;
         Z1190ContratoServicos_PrazoImediato = false;
         Z1191ContratoServicos_Produtividade = 0;
         Z1217ContratoServicos_EspelhaAceite = false;
         Z1266ContratoServicos_Momento = "";
         Z1325ContratoServicos_StatusPagFnc = "";
         Z1340ContratoServicos_QntUntCns = 0;
         Z1341ContratoServicos_FatorCnvUndCnt = 0;
         Z1397ContratoServicos_NaoRequerAtr = false;
         Z1455ContratoServicos_IndiceDivergencia = 0;
         Z1516ContratoServicos_TmpEstAnl = 0;
         Z1501ContratoServicos_TmpEstExc = 0;
         Z1502ContratoServicos_TmpEstCrr = 0;
         Z1531ContratoServicos_TipoHierarquia = 0;
         Z1537ContratoServicos_PercTmp = 0;
         Z1538ContratoServicos_PercPgm = 0;
         Z1539ContratoServicos_PercCnc = 0;
         Z638ContratoServicos_Ativo = false;
         Z1723ContratoServicos_CodigoFiscal = "";
         Z1799ContratoServicos_LimiteProposta = 0;
         Z2074ContratoServicos_CalculoRmn = 0;
         Z2094ContratoServicos_SolicitaGestorSistema = false;
         Z155Servico_Codigo = 0;
         Z74Contrato_Codigo = 0;
         Z1212ContratoServicos_UnidadeContratada = 0;
      }

      protected void InitAll0X34( )
      {
         A160ContratoServicos_Codigo = 0;
         InitializeNonKey0X34( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2094ContratoServicos_SolicitaGestorSistema = i2094ContratoServicos_SolicitaGestorSistema;
         A638ContratoServicos_Ativo = i638ContratoServicos_Ativo;
         A1649ContratoServicos_PrazoInicio = i1649ContratoServicos_PrazoInicio;
         n1649ContratoServicos_PrazoInicio = false;
         A607ServicoContrato_Faturamento = i607ServicoContrato_Faturamento;
         A1454ContratoServicos_PrazoTpDias = i1454ContratoServicos_PrazoTpDias;
         n1454ContratoServicos_PrazoTpDias = false;
         A558Servico_Percentual = i558Servico_Percentual;
         n558Servico_Percentual = false;
      }

      protected void InitializeNonKey0X228( )
      {
         A2070ContratoServicosRmn_Inicio = 0;
         A2071ContratoServicosRmn_Fim = 0;
         A2072ContratoServicosRmn_Valor = 0;
         Z2070ContratoServicosRmn_Inicio = 0;
         Z2071ContratoServicosRmn_Fim = 0;
         Z2072ContratoServicosRmn_Valor = 0;
      }

      protected void InitAll0X228( )
      {
         A2069ContratoServicosRmn_Sequencial = 0;
         InitializeNonKey0X228( ) ;
      }

      protected void StandaloneModalInsert0X228( )
      {
         A2073ContratoServicos_QtdRmn = i2073ContratoServicos_QtdRmn;
         n2073ContratoServicos_QtdRmn = false;
      }

      public void VarsToRow34( SdtContratoServicos obj34 )
      {
         obj34.gxTpr_Mode = Gx_mode;
         obj34.gxTpr_Contratoservicos_servicosigla = A826ContratoServicos_ServicoSigla;
         obj34.gxTpr_Servico_identificacao = A2107Servico_Identificacao;
         obj34.gxTpr_Servico_vinculados = A640Servico_Vinculados;
         obj34.gxTpr_Contratoservicos_servicocod = A827ContratoServicos_ServicoCod;
         obj34.gxTpr_Servico_responsavel = A1551Servico_Responsavel;
         obj34.gxTpr_Contratoservicos_prazos = A911ContratoServicos_Prazos;
         obj34.gxTpr_Contratoservicos_prazotipo = A913ContratoServicos_PrazoTipo;
         obj34.gxTpr_Contratoservicos_prazodias = A914ContratoServicos_PrazoDias;
         obj34.gxTpr_Contrato_codigo = A74Contrato_Codigo;
         obj34.gxTpr_Contrato_numero = A77Contrato_Numero;
         obj34.gxTpr_Contrato_numeroata = A78Contrato_NumeroAta;
         obj34.gxTpr_Contrato_ano = A79Contrato_Ano;
         obj34.gxTpr_Contrato_valorunidadecontratacao = A116Contrato_ValorUnidadeContratacao;
         obj34.gxTpr_Contrato_areatrabalhocod = A75Contrato_AreaTrabalhoCod;
         obj34.gxTpr_Contratada_codigo = A39Contratada_Codigo;
         obj34.gxTpr_Contratada_pessoacod = A40Contratada_PessoaCod;
         obj34.gxTpr_Contratada_pessoanom = A41Contratada_PessoaNom;
         obj34.gxTpr_Contratada_pessoacnpj = A42Contratada_PessoaCNPJ;
         obj34.gxTpr_Contratada_tipofabrica = A516Contratada_TipoFabrica;
         obj34.gxTpr_Servico_codigo = A155Servico_Codigo;
         obj34.gxTpr_Contratoservicos_alias = A1858ContratoServicos_Alias;
         obj34.gxTpr_Servico_nome = A608Servico_Nome;
         obj34.gxTpr_Servico_sigla = A605Servico_Sigla;
         obj34.gxTpr_Servico_tela = A1061Servico_Tela;
         obj34.gxTpr_Servico_ativo = A632Servico_Ativo;
         obj34.gxTpr_Servico_descricao = A156Servico_Descricao;
         obj34.gxTpr_Servico_vinculado = A631Servico_Vinculado;
         obj34.gxTpr_Servicogrupo_codigo = A157ServicoGrupo_Codigo;
         obj34.gxTpr_Servicogrupo_descricao = A158ServicoGrupo_Descricao;
         obj34.gxTpr_Servico_isorigemreferencia = A2092Servico_IsOrigemReferencia;
         obj34.gxTpr_Contratoservicos_unidadecontratada = A1212ContratoServicos_UnidadeContratada;
         obj34.gxTpr_Contratoservicos_undcntnome = A2132ContratoServicos_UndCntNome;
         obj34.gxTpr_Contratoservicos_undcntsgl = A1712ContratoServicos_UndCntSgl;
         obj34.gxTpr_Servico_qtdcontratada = A555Servico_QtdContratada;
         obj34.gxTpr_Contratoservicos_localexec = A639ContratoServicos_LocalExec;
         obj34.gxTpr_Contratoservicos_tipovnc = A868ContratoServicos_TipoVnc;
         obj34.gxTpr_Contratoservicos_hmlsemcnf = A888ContratoServicos_HmlSemCnf;
         obj34.gxTpr_Contratoservicos_indicadores = A1377ContratoServicos_Indicadores;
         obj34.gxTpr_Contratoservicos_prazoanalise = A1152ContratoServicos_PrazoAnalise;
         obj34.gxTpr_Contratoservicos_prazoresposta = A1153ContratoServicos_PrazoResposta;
         obj34.gxTpr_Contratoservicos_prazogarantia = A1181ContratoServicos_PrazoGarantia;
         obj34.gxTpr_Contratoservicos_prazoatendegarantia = A1182ContratoServicos_PrazoAtendeGarantia;
         obj34.gxTpr_Contratoservicos_prazocorrecao = A1224ContratoServicos_PrazoCorrecao;
         obj34.gxTpr_Contratoservicos_prazocorrecaotipo = A1225ContratoServicos_PrazoCorrecaoTipo;
         obj34.gxTpr_Contratoservicos_prazoimediato = A1190ContratoServicos_PrazoImediato;
         obj34.gxTpr_Contratoservicos_produtividade = A1191ContratoServicos_Produtividade;
         obj34.gxTpr_Contratoservicos_espelhaaceite = A1217ContratoServicos_EspelhaAceite;
         obj34.gxTpr_Contratoservicos_momento = A1266ContratoServicos_Momento;
         obj34.gxTpr_Contratoservicos_statuspagfnc = A1325ContratoServicos_StatusPagFnc;
         obj34.gxTpr_Contratoservicos_qntuntcns = A1340ContratoServicos_QntUntCns;
         obj34.gxTpr_Contratoservicos_fatorcnvundcnt = A1341ContratoServicos_FatorCnvUndCnt;
         obj34.gxTpr_Contratoservicos_naorequeratr = A1397ContratoServicos_NaoRequerAtr;
         obj34.gxTpr_Contrato_indicedivergencia = A453Contrato_IndiceDivergencia;
         obj34.gxTpr_Contratoservicos_tmpestanl = A1516ContratoServicos_TmpEstAnl;
         obj34.gxTpr_Contratoservicos_tmpestexc = A1501ContratoServicos_TmpEstExc;
         obj34.gxTpr_Contratoservicos_tmpestcrr = A1502ContratoServicos_TmpEstCrr;
         obj34.gxTpr_Contratoservicos_tipohierarquia = A1531ContratoServicos_TipoHierarquia;
         obj34.gxTpr_Contratoservicos_perctmp = A1537ContratoServicos_PercTmp;
         obj34.gxTpr_Contratoservicos_percpgm = A1538ContratoServicos_PercPgm;
         obj34.gxTpr_Contratoservicos_perccnc = A1539ContratoServicos_PercCnc;
         obj34.gxTpr_Contratoservicos_codigofiscal = A1723ContratoServicos_CodigoFiscal;
         obj34.gxTpr_Contratoservicos_limiteproposta = A1799ContratoServicos_LimiteProposta;
         obj34.gxTpr_Contratoservicos_calculormn = A2074ContratoServicos_CalculoRmn;
         obj34.gxTpr_Contratoservicos_qtdrmn = A2073ContratoServicos_QtdRmn;
         obj34.gxTpr_Servico_vlrunidadecontratada = A557Servico_VlrUnidadeContratada;
         obj34.gxTpr_Servico_percentual = A558Servico_Percentual;
         obj34.gxTpr_Servicocontrato_faturamento = A607ServicoContrato_Faturamento;
         obj34.gxTpr_Contratoservicos_prazotpdias = A1454ContratoServicos_PrazoTpDias;
         obj34.gxTpr_Contratoservicos_prazoinicio = A1649ContratoServicos_PrazoInicio;
         obj34.gxTpr_Contratoservicos_indicedivergencia = A1455ContratoServicos_IndiceDivergencia;
         obj34.gxTpr_Contratoservicos_ativo = A638ContratoServicos_Ativo;
         obj34.gxTpr_Contratoservicos_solicitagestorsistema = A2094ContratoServicos_SolicitaGestorSistema;
         obj34.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         obj34.gxTpr_Contratoservicos_codigo_Z = Z160ContratoServicos_Codigo;
         obj34.gxTpr_Contrato_codigo_Z = Z74Contrato_Codigo;
         obj34.gxTpr_Contrato_numero_Z = Z77Contrato_Numero;
         obj34.gxTpr_Contrato_numeroata_Z = Z78Contrato_NumeroAta;
         obj34.gxTpr_Contrato_ano_Z = Z79Contrato_Ano;
         obj34.gxTpr_Contrato_valorunidadecontratacao_Z = Z116Contrato_ValorUnidadeContratacao;
         obj34.gxTpr_Contrato_areatrabalhocod_Z = Z75Contrato_AreaTrabalhoCod;
         obj34.gxTpr_Contratada_codigo_Z = Z39Contratada_Codigo;
         obj34.gxTpr_Contratada_pessoacod_Z = Z40Contratada_PessoaCod;
         obj34.gxTpr_Contratada_pessoanom_Z = Z41Contratada_PessoaNom;
         obj34.gxTpr_Contratada_pessoacnpj_Z = Z42Contratada_PessoaCNPJ;
         obj34.gxTpr_Contratada_tipofabrica_Z = Z516Contratada_TipoFabrica;
         obj34.gxTpr_Servico_codigo_Z = Z155Servico_Codigo;
         obj34.gxTpr_Contratoservicos_alias_Z = Z1858ContratoServicos_Alias;
         obj34.gxTpr_Contratoservicos_servicocod_Z = Z827ContratoServicos_ServicoCod;
         obj34.gxTpr_Servico_nome_Z = Z608Servico_Nome;
         obj34.gxTpr_Servico_sigla_Z = Z605Servico_Sigla;
         obj34.gxTpr_Servico_tela_Z = Z1061Servico_Tela;
         obj34.gxTpr_Servico_ativo_Z = Z632Servico_Ativo;
         obj34.gxTpr_Servico_identificacao_Z = Z2107Servico_Identificacao;
         obj34.gxTpr_Contratoservicos_servicosigla_Z = Z826ContratoServicos_ServicoSigla;
         obj34.gxTpr_Servico_vinculado_Z = Z631Servico_Vinculado;
         obj34.gxTpr_Servico_vinculados_Z = Z640Servico_Vinculados;
         obj34.gxTpr_Servico_responsavel_Z = Z1551Servico_Responsavel;
         obj34.gxTpr_Servicogrupo_codigo_Z = Z157ServicoGrupo_Codigo;
         obj34.gxTpr_Servicogrupo_descricao_Z = Z158ServicoGrupo_Descricao;
         obj34.gxTpr_Servico_isorigemreferencia_Z = Z2092Servico_IsOrigemReferencia;
         obj34.gxTpr_Contratoservicos_unidadecontratada_Z = Z1212ContratoServicos_UnidadeContratada;
         obj34.gxTpr_Contratoservicos_undcntnome_Z = Z2132ContratoServicos_UndCntNome;
         obj34.gxTpr_Contratoservicos_undcntsgl_Z = Z1712ContratoServicos_UndCntSgl;
         obj34.gxTpr_Servico_qtdcontratada_Z = Z555Servico_QtdContratada;
         obj34.gxTpr_Servico_vlrunidadecontratada_Z = Z557Servico_VlrUnidadeContratada;
         obj34.gxTpr_Servico_percentual_Z = Z558Servico_Percentual;
         obj34.gxTpr_Servicocontrato_faturamento_Z = Z607ServicoContrato_Faturamento;
         obj34.gxTpr_Contratoservicos_localexec_Z = Z639ContratoServicos_LocalExec;
         obj34.gxTpr_Contratoservicos_tipovnc_Z = Z868ContratoServicos_TipoVnc;
         obj34.gxTpr_Contratoservicos_hmlsemcnf_Z = Z888ContratoServicos_HmlSemCnf;
         obj34.gxTpr_Contratoservicos_prazotipo_Z = Z913ContratoServicos_PrazoTipo;
         obj34.gxTpr_Contratoservicos_prazodias_Z = Z914ContratoServicos_PrazoDias;
         obj34.gxTpr_Contratoservicos_prazotpdias_Z = Z1454ContratoServicos_PrazoTpDias;
         obj34.gxTpr_Contratoservicos_prazos_Z = Z911ContratoServicos_Prazos;
         obj34.gxTpr_Contratoservicos_indicadores_Z = Z1377ContratoServicos_Indicadores;
         obj34.gxTpr_Contratoservicos_prazoanalise_Z = Z1152ContratoServicos_PrazoAnalise;
         obj34.gxTpr_Contratoservicos_prazoresposta_Z = Z1153ContratoServicos_PrazoResposta;
         obj34.gxTpr_Contratoservicos_prazogarantia_Z = Z1181ContratoServicos_PrazoGarantia;
         obj34.gxTpr_Contratoservicos_prazoatendegarantia_Z = Z1182ContratoServicos_PrazoAtendeGarantia;
         obj34.gxTpr_Contratoservicos_prazocorrecao_Z = Z1224ContratoServicos_PrazoCorrecao;
         obj34.gxTpr_Contratoservicos_prazocorrecaotipo_Z = Z1225ContratoServicos_PrazoCorrecaoTipo;
         obj34.gxTpr_Contratoservicos_prazoinicio_Z = Z1649ContratoServicos_PrazoInicio;
         obj34.gxTpr_Contratoservicos_prazoimediato_Z = Z1190ContratoServicos_PrazoImediato;
         obj34.gxTpr_Contratoservicos_produtividade_Z = Z1191ContratoServicos_Produtividade;
         obj34.gxTpr_Contratoservicos_espelhaaceite_Z = Z1217ContratoServicos_EspelhaAceite;
         obj34.gxTpr_Contratoservicos_momento_Z = Z1266ContratoServicos_Momento;
         obj34.gxTpr_Contratoservicos_statuspagfnc_Z = Z1325ContratoServicos_StatusPagFnc;
         obj34.gxTpr_Contratoservicos_qntuntcns_Z = Z1340ContratoServicos_QntUntCns;
         obj34.gxTpr_Contratoservicos_fatorcnvundcnt_Z = Z1341ContratoServicos_FatorCnvUndCnt;
         obj34.gxTpr_Contratoservicos_naorequeratr_Z = Z1397ContratoServicos_NaoRequerAtr;
         obj34.gxTpr_Contrato_indicedivergencia_Z = Z453Contrato_IndiceDivergencia;
         obj34.gxTpr_Contratoservicos_indicedivergencia_Z = Z1455ContratoServicos_IndiceDivergencia;
         obj34.gxTpr_Contratoservicos_tmpestanl_Z = Z1516ContratoServicos_TmpEstAnl;
         obj34.gxTpr_Contratoservicos_tmpestexc_Z = Z1501ContratoServicos_TmpEstExc;
         obj34.gxTpr_Contratoservicos_tmpestcrr_Z = Z1502ContratoServicos_TmpEstCrr;
         obj34.gxTpr_Contratoservicos_tipohierarquia_Z = Z1531ContratoServicos_TipoHierarquia;
         obj34.gxTpr_Contratoservicos_perctmp_Z = Z1537ContratoServicos_PercTmp;
         obj34.gxTpr_Contratoservicos_percpgm_Z = Z1538ContratoServicos_PercPgm;
         obj34.gxTpr_Contratoservicos_perccnc_Z = Z1539ContratoServicos_PercCnc;
         obj34.gxTpr_Contratoservicos_ativo_Z = Z638ContratoServicos_Ativo;
         obj34.gxTpr_Contratoservicos_codigofiscal_Z = Z1723ContratoServicos_CodigoFiscal;
         obj34.gxTpr_Contratoservicos_limiteproposta_Z = Z1799ContratoServicos_LimiteProposta;
         obj34.gxTpr_Contratoservicos_calculormn_Z = Z2074ContratoServicos_CalculoRmn;
         obj34.gxTpr_Contratoservicos_qtdrmn_Z = Z2073ContratoServicos_QtdRmn;
         obj34.gxTpr_Contratoservicos_solicitagestorsistema_Z = Z2094ContratoServicos_SolicitaGestorSistema;
         obj34.gxTpr_Contrato_numeroata_N = (short)(Convert.ToInt16(n78Contrato_NumeroAta));
         obj34.gxTpr_Contratada_pessoanom_N = (short)(Convert.ToInt16(n41Contratada_PessoaNom));
         obj34.gxTpr_Contratada_pessoacnpj_N = (short)(Convert.ToInt16(n42Contratada_PessoaCNPJ));
         obj34.gxTpr_Contratoservicos_alias_N = (short)(Convert.ToInt16(n1858ContratoServicos_Alias));
         obj34.gxTpr_Servico_tela_N = (short)(Convert.ToInt16(n1061Servico_Tela));
         obj34.gxTpr_Servico_descricao_N = (short)(Convert.ToInt16(n156Servico_Descricao));
         obj34.gxTpr_Servico_vinculado_N = (short)(Convert.ToInt16(n631Servico_Vinculado));
         obj34.gxTpr_Contratoservicos_undcntnome_N = (short)(Convert.ToInt16(n2132ContratoServicos_UndCntNome));
         obj34.gxTpr_Contratoservicos_undcntsgl_N = (short)(Convert.ToInt16(n1712ContratoServicos_UndCntSgl));
         obj34.gxTpr_Servico_qtdcontratada_N = (short)(Convert.ToInt16(n555Servico_QtdContratada));
         obj34.gxTpr_Servico_percentual_N = (short)(Convert.ToInt16(n558Servico_Percentual));
         obj34.gxTpr_Contratoservicos_tipovnc_N = (short)(Convert.ToInt16(n868ContratoServicos_TipoVnc));
         obj34.gxTpr_Contratoservicos_hmlsemcnf_N = (short)(Convert.ToInt16(n888ContratoServicos_HmlSemCnf));
         obj34.gxTpr_Contratoservicos_prazotipo_N = (short)(Convert.ToInt16(n913ContratoServicos_PrazoTipo));
         obj34.gxTpr_Contratoservicos_prazodias_N = (short)(Convert.ToInt16(n914ContratoServicos_PrazoDias));
         obj34.gxTpr_Contratoservicos_prazotpdias_N = (short)(Convert.ToInt16(n1454ContratoServicos_PrazoTpDias));
         obj34.gxTpr_Contratoservicos_prazos_N = (short)(Convert.ToInt16(n911ContratoServicos_Prazos));
         obj34.gxTpr_Contratoservicos_indicadores_N = (short)(Convert.ToInt16(n1377ContratoServicos_Indicadores));
         obj34.gxTpr_Contratoservicos_prazoanalise_N = (short)(Convert.ToInt16(n1152ContratoServicos_PrazoAnalise));
         obj34.gxTpr_Contratoservicos_prazoresposta_N = (short)(Convert.ToInt16(n1153ContratoServicos_PrazoResposta));
         obj34.gxTpr_Contratoservicos_prazogarantia_N = (short)(Convert.ToInt16(n1181ContratoServicos_PrazoGarantia));
         obj34.gxTpr_Contratoservicos_prazoatendegarantia_N = (short)(Convert.ToInt16(n1182ContratoServicos_PrazoAtendeGarantia));
         obj34.gxTpr_Contratoservicos_prazocorrecao_N = (short)(Convert.ToInt16(n1224ContratoServicos_PrazoCorrecao));
         obj34.gxTpr_Contratoservicos_prazocorrecaotipo_N = (short)(Convert.ToInt16(n1225ContratoServicos_PrazoCorrecaoTipo));
         obj34.gxTpr_Contratoservicos_prazoinicio_N = (short)(Convert.ToInt16(n1649ContratoServicos_PrazoInicio));
         obj34.gxTpr_Contratoservicos_prazoimediato_N = (short)(Convert.ToInt16(n1190ContratoServicos_PrazoImediato));
         obj34.gxTpr_Contratoservicos_produtividade_N = (short)(Convert.ToInt16(n1191ContratoServicos_Produtividade));
         obj34.gxTpr_Contratoservicos_espelhaaceite_N = (short)(Convert.ToInt16(n1217ContratoServicos_EspelhaAceite));
         obj34.gxTpr_Contratoservicos_momento_N = (short)(Convert.ToInt16(n1266ContratoServicos_Momento));
         obj34.gxTpr_Contratoservicos_statuspagfnc_N = (short)(Convert.ToInt16(n1325ContratoServicos_StatusPagFnc));
         obj34.gxTpr_Contratoservicos_qntuntcns_N = (short)(Convert.ToInt16(n1340ContratoServicos_QntUntCns));
         obj34.gxTpr_Contratoservicos_fatorcnvundcnt_N = (short)(Convert.ToInt16(n1341ContratoServicos_FatorCnvUndCnt));
         obj34.gxTpr_Contratoservicos_naorequeratr_N = (short)(Convert.ToInt16(n1397ContratoServicos_NaoRequerAtr));
         obj34.gxTpr_Contratoservicos_indicedivergencia_N = (short)(Convert.ToInt16(n1455ContratoServicos_IndiceDivergencia));
         obj34.gxTpr_Contratoservicos_tmpestanl_N = (short)(Convert.ToInt16(n1516ContratoServicos_TmpEstAnl));
         obj34.gxTpr_Contratoservicos_tmpestexc_N = (short)(Convert.ToInt16(n1501ContratoServicos_TmpEstExc));
         obj34.gxTpr_Contratoservicos_tmpestcrr_N = (short)(Convert.ToInt16(n1502ContratoServicos_TmpEstCrr));
         obj34.gxTpr_Contratoservicos_tipohierarquia_N = (short)(Convert.ToInt16(n1531ContratoServicos_TipoHierarquia));
         obj34.gxTpr_Contratoservicos_perctmp_N = (short)(Convert.ToInt16(n1537ContratoServicos_PercTmp));
         obj34.gxTpr_Contratoservicos_percpgm_N = (short)(Convert.ToInt16(n1538ContratoServicos_PercPgm));
         obj34.gxTpr_Contratoservicos_perccnc_N = (short)(Convert.ToInt16(n1539ContratoServicos_PercCnc));
         obj34.gxTpr_Contratoservicos_codigofiscal_N = (short)(Convert.ToInt16(n1723ContratoServicos_CodigoFiscal));
         obj34.gxTpr_Contratoservicos_limiteproposta_N = (short)(Convert.ToInt16(n1799ContratoServicos_LimiteProposta));
         obj34.gxTpr_Contratoservicos_calculormn_N = (short)(Convert.ToInt16(n2074ContratoServicos_CalculoRmn));
         obj34.gxTpr_Contratoservicos_qtdrmn_N = (short)(Convert.ToInt16(n2073ContratoServicos_QtdRmn));
         obj34.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow34( SdtContratoServicos obj34 )
      {
         obj34.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         return  ;
      }

      public void RowToVars34( SdtContratoServicos obj34 ,
                               int forceLoad )
      {
         Gx_mode = obj34.gxTpr_Mode;
         A826ContratoServicos_ServicoSigla = obj34.gxTpr_Contratoservicos_servicosigla;
         A2107Servico_Identificacao = obj34.gxTpr_Servico_identificacao;
         A640Servico_Vinculados = obj34.gxTpr_Servico_vinculados;
         A827ContratoServicos_ServicoCod = obj34.gxTpr_Contratoservicos_servicocod;
         A1551Servico_Responsavel = obj34.gxTpr_Servico_responsavel;
         A911ContratoServicos_Prazos = obj34.gxTpr_Contratoservicos_prazos;
         n911ContratoServicos_Prazos = false;
         A913ContratoServicos_PrazoTipo = obj34.gxTpr_Contratoservicos_prazotipo;
         n913ContratoServicos_PrazoTipo = false;
         A914ContratoServicos_PrazoDias = obj34.gxTpr_Contratoservicos_prazodias;
         n914ContratoServicos_PrazoDias = false;
         A74Contrato_Codigo = obj34.gxTpr_Contrato_codigo;
         A77Contrato_Numero = obj34.gxTpr_Contrato_numero;
         A78Contrato_NumeroAta = obj34.gxTpr_Contrato_numeroata;
         n78Contrato_NumeroAta = false;
         A79Contrato_Ano = obj34.gxTpr_Contrato_ano;
         A116Contrato_ValorUnidadeContratacao = obj34.gxTpr_Contrato_valorunidadecontratacao;
         A75Contrato_AreaTrabalhoCod = obj34.gxTpr_Contrato_areatrabalhocod;
         A39Contratada_Codigo = obj34.gxTpr_Contratada_codigo;
         A40Contratada_PessoaCod = obj34.gxTpr_Contratada_pessoacod;
         A41Contratada_PessoaNom = obj34.gxTpr_Contratada_pessoanom;
         n41Contratada_PessoaNom = false;
         A42Contratada_PessoaCNPJ = obj34.gxTpr_Contratada_pessoacnpj;
         n42Contratada_PessoaCNPJ = false;
         A516Contratada_TipoFabrica = obj34.gxTpr_Contratada_tipofabrica;
         A155Servico_Codigo = obj34.gxTpr_Servico_codigo;
         A1858ContratoServicos_Alias = obj34.gxTpr_Contratoservicos_alias;
         n1858ContratoServicos_Alias = false;
         A608Servico_Nome = obj34.gxTpr_Servico_nome;
         A605Servico_Sigla = obj34.gxTpr_Servico_sigla;
         A1061Servico_Tela = obj34.gxTpr_Servico_tela;
         n1061Servico_Tela = false;
         A632Servico_Ativo = obj34.gxTpr_Servico_ativo;
         A156Servico_Descricao = obj34.gxTpr_Servico_descricao;
         n156Servico_Descricao = false;
         A631Servico_Vinculado = obj34.gxTpr_Servico_vinculado;
         n631Servico_Vinculado = false;
         A157ServicoGrupo_Codigo = obj34.gxTpr_Servicogrupo_codigo;
         A158ServicoGrupo_Descricao = obj34.gxTpr_Servicogrupo_descricao;
         A2092Servico_IsOrigemReferencia = obj34.gxTpr_Servico_isorigemreferencia;
         A1212ContratoServicos_UnidadeContratada = obj34.gxTpr_Contratoservicos_unidadecontratada;
         A2132ContratoServicos_UndCntNome = obj34.gxTpr_Contratoservicos_undcntnome;
         n2132ContratoServicos_UndCntNome = false;
         A1712ContratoServicos_UndCntSgl = obj34.gxTpr_Contratoservicos_undcntsgl;
         n1712ContratoServicos_UndCntSgl = false;
         A555Servico_QtdContratada = obj34.gxTpr_Servico_qtdcontratada;
         n555Servico_QtdContratada = false;
         A639ContratoServicos_LocalExec = obj34.gxTpr_Contratoservicos_localexec;
         A868ContratoServicos_TipoVnc = obj34.gxTpr_Contratoservicos_tipovnc;
         n868ContratoServicos_TipoVnc = false;
         A888ContratoServicos_HmlSemCnf = obj34.gxTpr_Contratoservicos_hmlsemcnf;
         n888ContratoServicos_HmlSemCnf = false;
         A1377ContratoServicos_Indicadores = obj34.gxTpr_Contratoservicos_indicadores;
         n1377ContratoServicos_Indicadores = false;
         A1152ContratoServicos_PrazoAnalise = obj34.gxTpr_Contratoservicos_prazoanalise;
         n1152ContratoServicos_PrazoAnalise = false;
         A1153ContratoServicos_PrazoResposta = obj34.gxTpr_Contratoservicos_prazoresposta;
         n1153ContratoServicos_PrazoResposta = false;
         A1181ContratoServicos_PrazoGarantia = obj34.gxTpr_Contratoservicos_prazogarantia;
         n1181ContratoServicos_PrazoGarantia = false;
         A1182ContratoServicos_PrazoAtendeGarantia = obj34.gxTpr_Contratoservicos_prazoatendegarantia;
         n1182ContratoServicos_PrazoAtendeGarantia = false;
         A1224ContratoServicos_PrazoCorrecao = obj34.gxTpr_Contratoservicos_prazocorrecao;
         n1224ContratoServicos_PrazoCorrecao = false;
         A1225ContratoServicos_PrazoCorrecaoTipo = obj34.gxTpr_Contratoservicos_prazocorrecaotipo;
         n1225ContratoServicos_PrazoCorrecaoTipo = false;
         A1190ContratoServicos_PrazoImediato = obj34.gxTpr_Contratoservicos_prazoimediato;
         n1190ContratoServicos_PrazoImediato = false;
         A1191ContratoServicos_Produtividade = obj34.gxTpr_Contratoservicos_produtividade;
         n1191ContratoServicos_Produtividade = false;
         A1217ContratoServicos_EspelhaAceite = obj34.gxTpr_Contratoservicos_espelhaaceite;
         n1217ContratoServicos_EspelhaAceite = false;
         A1266ContratoServicos_Momento = obj34.gxTpr_Contratoservicos_momento;
         n1266ContratoServicos_Momento = false;
         A1325ContratoServicos_StatusPagFnc = obj34.gxTpr_Contratoservicos_statuspagfnc;
         n1325ContratoServicos_StatusPagFnc = false;
         A1340ContratoServicos_QntUntCns = obj34.gxTpr_Contratoservicos_qntuntcns;
         n1340ContratoServicos_QntUntCns = false;
         A1341ContratoServicos_FatorCnvUndCnt = obj34.gxTpr_Contratoservicos_fatorcnvundcnt;
         n1341ContratoServicos_FatorCnvUndCnt = false;
         A1397ContratoServicos_NaoRequerAtr = obj34.gxTpr_Contratoservicos_naorequeratr;
         n1397ContratoServicos_NaoRequerAtr = false;
         A453Contrato_IndiceDivergencia = obj34.gxTpr_Contrato_indicedivergencia;
         A1516ContratoServicos_TmpEstAnl = obj34.gxTpr_Contratoservicos_tmpestanl;
         n1516ContratoServicos_TmpEstAnl = false;
         A1501ContratoServicos_TmpEstExc = obj34.gxTpr_Contratoservicos_tmpestexc;
         n1501ContratoServicos_TmpEstExc = false;
         A1502ContratoServicos_TmpEstCrr = obj34.gxTpr_Contratoservicos_tmpestcrr;
         n1502ContratoServicos_TmpEstCrr = false;
         A1531ContratoServicos_TipoHierarquia = obj34.gxTpr_Contratoservicos_tipohierarquia;
         n1531ContratoServicos_TipoHierarquia = false;
         A1537ContratoServicos_PercTmp = obj34.gxTpr_Contratoservicos_perctmp;
         n1537ContratoServicos_PercTmp = false;
         A1538ContratoServicos_PercPgm = obj34.gxTpr_Contratoservicos_percpgm;
         n1538ContratoServicos_PercPgm = false;
         A1539ContratoServicos_PercCnc = obj34.gxTpr_Contratoservicos_perccnc;
         n1539ContratoServicos_PercCnc = false;
         A1723ContratoServicos_CodigoFiscal = obj34.gxTpr_Contratoservicos_codigofiscal;
         n1723ContratoServicos_CodigoFiscal = false;
         A1799ContratoServicos_LimiteProposta = obj34.gxTpr_Contratoservicos_limiteproposta;
         n1799ContratoServicos_LimiteProposta = false;
         A2074ContratoServicos_CalculoRmn = obj34.gxTpr_Contratoservicos_calculormn;
         n2074ContratoServicos_CalculoRmn = false;
         if ( forceLoad == 1 )
         {
            A2073ContratoServicos_QtdRmn = obj34.gxTpr_Contratoservicos_qtdrmn;
            n2073ContratoServicos_QtdRmn = false;
         }
         A557Servico_VlrUnidadeContratada = obj34.gxTpr_Servico_vlrunidadecontratada;
         A558Servico_Percentual = obj34.gxTpr_Servico_percentual;
         n558Servico_Percentual = false;
         A607ServicoContrato_Faturamento = obj34.gxTpr_Servicocontrato_faturamento;
         A1454ContratoServicos_PrazoTpDias = obj34.gxTpr_Contratoservicos_prazotpdias;
         n1454ContratoServicos_PrazoTpDias = false;
         A1649ContratoServicos_PrazoInicio = obj34.gxTpr_Contratoservicos_prazoinicio;
         n1649ContratoServicos_PrazoInicio = false;
         A1455ContratoServicos_IndiceDivergencia = obj34.gxTpr_Contratoservicos_indicedivergencia;
         n1455ContratoServicos_IndiceDivergencia = false;
         A638ContratoServicos_Ativo = obj34.gxTpr_Contratoservicos_ativo;
         A2094ContratoServicos_SolicitaGestorSistema = obj34.gxTpr_Contratoservicos_solicitagestorsistema;
         A160ContratoServicos_Codigo = obj34.gxTpr_Contratoservicos_codigo;
         Z160ContratoServicos_Codigo = obj34.gxTpr_Contratoservicos_codigo_Z;
         Z74Contrato_Codigo = obj34.gxTpr_Contrato_codigo_Z;
         Z77Contrato_Numero = obj34.gxTpr_Contrato_numero_Z;
         Z78Contrato_NumeroAta = obj34.gxTpr_Contrato_numeroata_Z;
         Z79Contrato_Ano = obj34.gxTpr_Contrato_ano_Z;
         Z116Contrato_ValorUnidadeContratacao = obj34.gxTpr_Contrato_valorunidadecontratacao_Z;
         Z75Contrato_AreaTrabalhoCod = obj34.gxTpr_Contrato_areatrabalhocod_Z;
         Z39Contratada_Codigo = obj34.gxTpr_Contratada_codigo_Z;
         Z40Contratada_PessoaCod = obj34.gxTpr_Contratada_pessoacod_Z;
         Z41Contratada_PessoaNom = obj34.gxTpr_Contratada_pessoanom_Z;
         Z42Contratada_PessoaCNPJ = obj34.gxTpr_Contratada_pessoacnpj_Z;
         Z516Contratada_TipoFabrica = obj34.gxTpr_Contratada_tipofabrica_Z;
         Z155Servico_Codigo = obj34.gxTpr_Servico_codigo_Z;
         Z1858ContratoServicos_Alias = obj34.gxTpr_Contratoservicos_alias_Z;
         Z827ContratoServicos_ServicoCod = obj34.gxTpr_Contratoservicos_servicocod_Z;
         Z608Servico_Nome = obj34.gxTpr_Servico_nome_Z;
         Z605Servico_Sigla = obj34.gxTpr_Servico_sigla_Z;
         Z1061Servico_Tela = obj34.gxTpr_Servico_tela_Z;
         Z632Servico_Ativo = obj34.gxTpr_Servico_ativo_Z;
         Z2107Servico_Identificacao = obj34.gxTpr_Servico_identificacao_Z;
         Z826ContratoServicos_ServicoSigla = obj34.gxTpr_Contratoservicos_servicosigla_Z;
         Z631Servico_Vinculado = obj34.gxTpr_Servico_vinculado_Z;
         Z640Servico_Vinculados = obj34.gxTpr_Servico_vinculados_Z;
         Z1551Servico_Responsavel = obj34.gxTpr_Servico_responsavel_Z;
         Z157ServicoGrupo_Codigo = obj34.gxTpr_Servicogrupo_codigo_Z;
         Z158ServicoGrupo_Descricao = obj34.gxTpr_Servicogrupo_descricao_Z;
         Z2092Servico_IsOrigemReferencia = obj34.gxTpr_Servico_isorigemreferencia_Z;
         Z1212ContratoServicos_UnidadeContratada = obj34.gxTpr_Contratoservicos_unidadecontratada_Z;
         Z2132ContratoServicos_UndCntNome = obj34.gxTpr_Contratoservicos_undcntnome_Z;
         Z1712ContratoServicos_UndCntSgl = obj34.gxTpr_Contratoservicos_undcntsgl_Z;
         Z555Servico_QtdContratada = obj34.gxTpr_Servico_qtdcontratada_Z;
         Z557Servico_VlrUnidadeContratada = obj34.gxTpr_Servico_vlrunidadecontratada_Z;
         O557Servico_VlrUnidadeContratada = obj34.gxTpr_Servico_vlrunidadecontratada_Z;
         Z558Servico_Percentual = obj34.gxTpr_Servico_percentual_Z;
         Z607ServicoContrato_Faturamento = obj34.gxTpr_Servicocontrato_faturamento_Z;
         Z639ContratoServicos_LocalExec = obj34.gxTpr_Contratoservicos_localexec_Z;
         Z868ContratoServicos_TipoVnc = obj34.gxTpr_Contratoservicos_tipovnc_Z;
         Z888ContratoServicos_HmlSemCnf = obj34.gxTpr_Contratoservicos_hmlsemcnf_Z;
         Z913ContratoServicos_PrazoTipo = obj34.gxTpr_Contratoservicos_prazotipo_Z;
         Z914ContratoServicos_PrazoDias = obj34.gxTpr_Contratoservicos_prazodias_Z;
         Z1454ContratoServicos_PrazoTpDias = obj34.gxTpr_Contratoservicos_prazotpdias_Z;
         Z911ContratoServicos_Prazos = obj34.gxTpr_Contratoservicos_prazos_Z;
         Z1377ContratoServicos_Indicadores = obj34.gxTpr_Contratoservicos_indicadores_Z;
         Z1152ContratoServicos_PrazoAnalise = obj34.gxTpr_Contratoservicos_prazoanalise_Z;
         Z1153ContratoServicos_PrazoResposta = obj34.gxTpr_Contratoservicos_prazoresposta_Z;
         Z1181ContratoServicos_PrazoGarantia = obj34.gxTpr_Contratoservicos_prazogarantia_Z;
         Z1182ContratoServicos_PrazoAtendeGarantia = obj34.gxTpr_Contratoservicos_prazoatendegarantia_Z;
         Z1224ContratoServicos_PrazoCorrecao = obj34.gxTpr_Contratoservicos_prazocorrecao_Z;
         Z1225ContratoServicos_PrazoCorrecaoTipo = obj34.gxTpr_Contratoservicos_prazocorrecaotipo_Z;
         Z1649ContratoServicos_PrazoInicio = obj34.gxTpr_Contratoservicos_prazoinicio_Z;
         Z1190ContratoServicos_PrazoImediato = obj34.gxTpr_Contratoservicos_prazoimediato_Z;
         Z1191ContratoServicos_Produtividade = obj34.gxTpr_Contratoservicos_produtividade_Z;
         Z1217ContratoServicos_EspelhaAceite = obj34.gxTpr_Contratoservicos_espelhaaceite_Z;
         Z1266ContratoServicos_Momento = obj34.gxTpr_Contratoservicos_momento_Z;
         Z1325ContratoServicos_StatusPagFnc = obj34.gxTpr_Contratoservicos_statuspagfnc_Z;
         Z1340ContratoServicos_QntUntCns = obj34.gxTpr_Contratoservicos_qntuntcns_Z;
         Z1341ContratoServicos_FatorCnvUndCnt = obj34.gxTpr_Contratoservicos_fatorcnvundcnt_Z;
         Z1397ContratoServicos_NaoRequerAtr = obj34.gxTpr_Contratoservicos_naorequeratr_Z;
         Z453Contrato_IndiceDivergencia = obj34.gxTpr_Contrato_indicedivergencia_Z;
         Z1455ContratoServicos_IndiceDivergencia = obj34.gxTpr_Contratoservicos_indicedivergencia_Z;
         Z1516ContratoServicos_TmpEstAnl = obj34.gxTpr_Contratoservicos_tmpestanl_Z;
         Z1501ContratoServicos_TmpEstExc = obj34.gxTpr_Contratoservicos_tmpestexc_Z;
         Z1502ContratoServicos_TmpEstCrr = obj34.gxTpr_Contratoservicos_tmpestcrr_Z;
         Z1531ContratoServicos_TipoHierarquia = obj34.gxTpr_Contratoservicos_tipohierarquia_Z;
         Z1537ContratoServicos_PercTmp = obj34.gxTpr_Contratoservicos_perctmp_Z;
         Z1538ContratoServicos_PercPgm = obj34.gxTpr_Contratoservicos_percpgm_Z;
         Z1539ContratoServicos_PercCnc = obj34.gxTpr_Contratoservicos_perccnc_Z;
         Z638ContratoServicos_Ativo = obj34.gxTpr_Contratoservicos_ativo_Z;
         Z1723ContratoServicos_CodigoFiscal = obj34.gxTpr_Contratoservicos_codigofiscal_Z;
         Z1799ContratoServicos_LimiteProposta = obj34.gxTpr_Contratoservicos_limiteproposta_Z;
         Z2074ContratoServicos_CalculoRmn = obj34.gxTpr_Contratoservicos_calculormn_Z;
         Z2073ContratoServicos_QtdRmn = obj34.gxTpr_Contratoservicos_qtdrmn_Z;
         O2073ContratoServicos_QtdRmn = obj34.gxTpr_Contratoservicos_qtdrmn_Z;
         Z2094ContratoServicos_SolicitaGestorSistema = obj34.gxTpr_Contratoservicos_solicitagestorsistema_Z;
         n78Contrato_NumeroAta = (bool)(Convert.ToBoolean(obj34.gxTpr_Contrato_numeroata_N));
         n41Contratada_PessoaNom = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratada_pessoanom_N));
         n42Contratada_PessoaCNPJ = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratada_pessoacnpj_N));
         n1858ContratoServicos_Alias = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_alias_N));
         n1061Servico_Tela = (bool)(Convert.ToBoolean(obj34.gxTpr_Servico_tela_N));
         n156Servico_Descricao = (bool)(Convert.ToBoolean(obj34.gxTpr_Servico_descricao_N));
         n631Servico_Vinculado = (bool)(Convert.ToBoolean(obj34.gxTpr_Servico_vinculado_N));
         n2132ContratoServicos_UndCntNome = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_undcntnome_N));
         n1712ContratoServicos_UndCntSgl = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_undcntsgl_N));
         n555Servico_QtdContratada = (bool)(Convert.ToBoolean(obj34.gxTpr_Servico_qtdcontratada_N));
         n558Servico_Percentual = (bool)(Convert.ToBoolean(obj34.gxTpr_Servico_percentual_N));
         n868ContratoServicos_TipoVnc = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_tipovnc_N));
         n888ContratoServicos_HmlSemCnf = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_hmlsemcnf_N));
         n913ContratoServicos_PrazoTipo = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazotipo_N));
         n914ContratoServicos_PrazoDias = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazodias_N));
         n1454ContratoServicos_PrazoTpDias = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazotpdias_N));
         n911ContratoServicos_Prazos = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazos_N));
         n1377ContratoServicos_Indicadores = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_indicadores_N));
         n1152ContratoServicos_PrazoAnalise = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazoanalise_N));
         n1153ContratoServicos_PrazoResposta = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazoresposta_N));
         n1181ContratoServicos_PrazoGarantia = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazogarantia_N));
         n1182ContratoServicos_PrazoAtendeGarantia = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazoatendegarantia_N));
         n1224ContratoServicos_PrazoCorrecao = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazocorrecao_N));
         n1225ContratoServicos_PrazoCorrecaoTipo = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazocorrecaotipo_N));
         n1649ContratoServicos_PrazoInicio = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazoinicio_N));
         n1190ContratoServicos_PrazoImediato = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_prazoimediato_N));
         n1191ContratoServicos_Produtividade = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_produtividade_N));
         n1217ContratoServicos_EspelhaAceite = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_espelhaaceite_N));
         n1266ContratoServicos_Momento = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_momento_N));
         n1325ContratoServicos_StatusPagFnc = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_statuspagfnc_N));
         n1340ContratoServicos_QntUntCns = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_qntuntcns_N));
         n1341ContratoServicos_FatorCnvUndCnt = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_fatorcnvundcnt_N));
         n1397ContratoServicos_NaoRequerAtr = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_naorequeratr_N));
         n1455ContratoServicos_IndiceDivergencia = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_indicedivergencia_N));
         n1516ContratoServicos_TmpEstAnl = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_tmpestanl_N));
         n1501ContratoServicos_TmpEstExc = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_tmpestexc_N));
         n1502ContratoServicos_TmpEstCrr = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_tmpestcrr_N));
         n1531ContratoServicos_TipoHierarquia = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_tipohierarquia_N));
         n1537ContratoServicos_PercTmp = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_perctmp_N));
         n1538ContratoServicos_PercPgm = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_percpgm_N));
         n1539ContratoServicos_PercCnc = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_perccnc_N));
         n1723ContratoServicos_CodigoFiscal = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_codigofiscal_N));
         n1799ContratoServicos_LimiteProposta = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_limiteproposta_N));
         n2074ContratoServicos_CalculoRmn = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_calculormn_N));
         n2073ContratoServicos_QtdRmn = (bool)(Convert.ToBoolean(obj34.gxTpr_Contratoservicos_qtdrmn_N));
         Gx_mode = obj34.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow228( SdtContratoServicos_Rmn obj228 )
      {
         obj228.gxTpr_Mode = Gx_mode;
         obj228.gxTpr_Contratoservicosrmn_inicio = A2070ContratoServicosRmn_Inicio;
         obj228.gxTpr_Contratoservicosrmn_fim = A2071ContratoServicosRmn_Fim;
         obj228.gxTpr_Contratoservicosrmn_valor = A2072ContratoServicosRmn_Valor;
         obj228.gxTpr_Contratoservicosrmn_sequencial = A2069ContratoServicosRmn_Sequencial;
         obj228.gxTpr_Contratoservicosrmn_sequencial_Z = Z2069ContratoServicosRmn_Sequencial;
         obj228.gxTpr_Contratoservicosrmn_inicio_Z = Z2070ContratoServicosRmn_Inicio;
         obj228.gxTpr_Contratoservicosrmn_fim_Z = Z2071ContratoServicosRmn_Fim;
         obj228.gxTpr_Contratoservicosrmn_valor_Z = Z2072ContratoServicosRmn_Valor;
         obj228.gxTpr_Modified = nIsMod_228;
         return  ;
      }

      public void KeyVarsToRow228( SdtContratoServicos_Rmn obj228 )
      {
         obj228.gxTpr_Contratoservicosrmn_sequencial = A2069ContratoServicosRmn_Sequencial;
         return  ;
      }

      public void RowToVars228( SdtContratoServicos_Rmn obj228 ,
                                int forceLoad )
      {
         Gx_mode = obj228.gxTpr_Mode;
         A2070ContratoServicosRmn_Inicio = obj228.gxTpr_Contratoservicosrmn_inicio;
         A2071ContratoServicosRmn_Fim = obj228.gxTpr_Contratoservicosrmn_fim;
         A2072ContratoServicosRmn_Valor = obj228.gxTpr_Contratoservicosrmn_valor;
         A2069ContratoServicosRmn_Sequencial = obj228.gxTpr_Contratoservicosrmn_sequencial;
         Z2069ContratoServicosRmn_Sequencial = obj228.gxTpr_Contratoservicosrmn_sequencial_Z;
         Z2070ContratoServicosRmn_Inicio = obj228.gxTpr_Contratoservicosrmn_inicio_Z;
         Z2071ContratoServicosRmn_Fim = obj228.gxTpr_Contratoservicosrmn_fim_Z;
         Z2072ContratoServicosRmn_Valor = obj228.gxTpr_Contratoservicosrmn_valor_Z;
         nIsMod_228 = obj228.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A160ContratoServicos_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0X34( ) ;
         ScanKeyStart0X34( ) ;
         if ( RcdFound34 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            O557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
         }
         ZM0X34( -34) ;
         OnLoadActions0X34( ) ;
         AddRow0X34( ) ;
         bcContratoServicos.gxTpr_Rmn.ClearCollection();
         if ( RcdFound34 == 1 )
         {
            ScanKeyStart0X228( ) ;
            nGXsfl_228_idx = 1;
            while ( RcdFound228 != 0 )
            {
               Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
               ZM0X228( -45) ;
               OnLoadActions0X228( ) ;
               nRcdExists_228 = 1;
               nIsMod_228 = 0;
               AddRow0X228( ) ;
               nGXsfl_228_idx = (short)(nGXsfl_228_idx+1);
               ScanKeyNext0X228( ) ;
            }
            ScanKeyEnd0X228( ) ;
         }
         ScanKeyEnd0X34( ) ;
         if ( RcdFound34 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars34( bcContratoServicos, 0) ;
         ScanKeyStart0X34( ) ;
         if ( RcdFound34 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            O557Servico_VlrUnidadeContratada = A557Servico_VlrUnidadeContratada;
         }
         ZM0X34( -34) ;
         OnLoadActions0X34( ) ;
         AddRow0X34( ) ;
         bcContratoServicos.gxTpr_Rmn.ClearCollection();
         if ( RcdFound34 == 1 )
         {
            ScanKeyStart0X228( ) ;
            nGXsfl_228_idx = 1;
            while ( RcdFound228 != 0 )
            {
               Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               Z2069ContratoServicosRmn_Sequencial = A2069ContratoServicosRmn_Sequencial;
               ZM0X228( -45) ;
               OnLoadActions0X228( ) ;
               nRcdExists_228 = 1;
               nIsMod_228 = 0;
               AddRow0X228( ) ;
               nGXsfl_228_idx = (short)(nGXsfl_228_idx+1);
               ScanKeyNext0X228( ) ;
            }
            ScanKeyEnd0X228( ) ;
         }
         ScanKeyEnd0X34( ) ;
         if ( RcdFound34 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars34( bcContratoServicos, 0) ;
         nKeyPressed = 1;
         GetKey0X34( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
            n2073ContratoServicos_QtdRmn = false;
            Insert0X34( ) ;
         }
         else
         {
            if ( RcdFound34 == 1 )
            {
               if ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                  n2073ContratoServicos_QtdRmn = false;
                  Update0X34( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                        n2073ContratoServicos_QtdRmn = false;
                        Insert0X34( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        A2073ContratoServicos_QtdRmn = O2073ContratoServicos_QtdRmn;
                        n2073ContratoServicos_QtdRmn = false;
                        Insert0X34( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow34( bcContratoServicos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars34( bcContratoServicos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0X34( ) ;
         if ( RcdFound34 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo )
            {
               A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(3);
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(26);
         pr_default.close(23);
         pr_default.close(28);
         pr_default.close(27);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(22);
         context.RollbackDataStores( "ContratoServicos_BC");
         VarsToRow34( bcContratoServicos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoServicos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoServicos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoServicos )
         {
            bcContratoServicos = (SdtContratoServicos)(sdt);
            if ( StringUtil.StrCmp(bcContratoServicos.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow34( bcContratoServicos) ;
            }
            else
            {
               RowToVars34( bcContratoServicos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoServicos.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars34( bcContratoServicos, 1) ;
         return  ;
      }

      public SdtContratoServicos ContratoServicos_BC
      {
         get {
            return bcContratoServicos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(3);
         pr_default.close(26);
         pr_default.close(23);
         pr_default.close(28);
         pr_default.close(27);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(19);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(22);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         sMode34 = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV36Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV29AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV32Servico_Nome = "";
         GXt_char1 = "";
         Z1858ContratoServicos_Alias = "";
         A1858ContratoServicos_Alias = "";
         Z607ServicoContrato_Faturamento = "";
         A607ServicoContrato_Faturamento = "";
         Z639ContratoServicos_LocalExec = "";
         A639ContratoServicos_LocalExec = "";
         Z868ContratoServicos_TipoVnc = "";
         A868ContratoServicos_TipoVnc = "";
         Z1454ContratoServicos_PrazoTpDias = "";
         A1454ContratoServicos_PrazoTpDias = "";
         Z1225ContratoServicos_PrazoCorrecaoTipo = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         Z1266ContratoServicos_Momento = "";
         A1266ContratoServicos_Momento = "";
         Z1325ContratoServicos_StatusPagFnc = "";
         A1325ContratoServicos_StatusPagFnc = "";
         Z1723ContratoServicos_CodigoFiscal = "";
         A1723ContratoServicos_CodigoFiscal = "";
         Z2107Servico_Identificacao = "";
         A2107Servico_Identificacao = "";
         Z826ContratoServicos_ServicoSigla = "";
         A826ContratoServicos_ServicoSigla = "";
         Z913ContratoServicos_PrazoTipo = "";
         A913ContratoServicos_PrazoTipo = "";
         Z608Servico_Nome = "";
         A608Servico_Nome = "";
         Z605Servico_Sigla = "";
         A605Servico_Sigla = "";
         Z1061Servico_Tela = "";
         A1061Servico_Tela = "";
         Z77Contrato_Numero = "";
         A77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         A78Contrato_NumeroAta = "";
         Z2132ContratoServicos_UndCntNome = "";
         A2132ContratoServicos_UndCntNome = "";
         Z1712ContratoServicos_UndCntSgl = "";
         A1712ContratoServicos_UndCntSgl = "";
         Z158ServicoGrupo_Descricao = "";
         A158ServicoGrupo_Descricao = "";
         Z516Contratada_TipoFabrica = "";
         A516Contratada_TipoFabrica = "";
         Z41Contratada_PessoaNom = "";
         A41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         A42Contratada_PessoaCNPJ = "";
         Z156Servico_Descricao = "";
         A156Servico_Descricao = "";
         BC000X21_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         BC000X21_A160ContratoServicos_Codigo = new int[1] ;
         BC000X21_A77Contrato_Numero = new String[] {""} ;
         BC000X21_A78Contrato_NumeroAta = new String[] {""} ;
         BC000X21_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000X21_A79Contrato_Ano = new short[1] ;
         BC000X21_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000X21_A41Contratada_PessoaNom = new String[] {""} ;
         BC000X21_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000X21_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000X21_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000X21_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000X21_A1858ContratoServicos_Alias = new String[] {""} ;
         BC000X21_n1858ContratoServicos_Alias = new bool[] {false} ;
         BC000X21_A608Servico_Nome = new String[] {""} ;
         BC000X21_A605Servico_Sigla = new String[] {""} ;
         BC000X21_A1061Servico_Tela = new String[] {""} ;
         BC000X21_n1061Servico_Tela = new bool[] {false} ;
         BC000X21_A632Servico_Ativo = new bool[] {false} ;
         BC000X21_A156Servico_Descricao = new String[] {""} ;
         BC000X21_n156Servico_Descricao = new bool[] {false} ;
         BC000X21_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000X21_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000X21_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         BC000X21_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         BC000X21_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         BC000X21_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         BC000X21_A555Servico_QtdContratada = new long[1] ;
         BC000X21_n555Servico_QtdContratada = new bool[] {false} ;
         BC000X21_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         BC000X21_A558Servico_Percentual = new decimal[1] ;
         BC000X21_n558Servico_Percentual = new bool[] {false} ;
         BC000X21_A607ServicoContrato_Faturamento = new String[] {""} ;
         BC000X21_A639ContratoServicos_LocalExec = new String[] {""} ;
         BC000X21_A868ContratoServicos_TipoVnc = new String[] {""} ;
         BC000X21_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         BC000X21_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X21_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X21_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         BC000X21_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         BC000X21_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         BC000X21_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         BC000X21_A1153ContratoServicos_PrazoResposta = new short[1] ;
         BC000X21_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         BC000X21_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         BC000X21_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         BC000X21_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         BC000X21_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         BC000X21_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         BC000X21_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         BC000X21_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         BC000X21_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         BC000X21_A1649ContratoServicos_PrazoInicio = new short[1] ;
         BC000X21_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         BC000X21_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X21_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X21_A1191ContratoServicos_Produtividade = new decimal[1] ;
         BC000X21_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         BC000X21_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X21_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X21_A1266ContratoServicos_Momento = new String[] {""} ;
         BC000X21_n1266ContratoServicos_Momento = new bool[] {false} ;
         BC000X21_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         BC000X21_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         BC000X21_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         BC000X21_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         BC000X21_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         BC000X21_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         BC000X21_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X21_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X21_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000X21_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         BC000X21_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         BC000X21_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         BC000X21_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         BC000X21_A1501ContratoServicos_TmpEstExc = new int[1] ;
         BC000X21_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         BC000X21_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         BC000X21_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         BC000X21_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         BC000X21_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         BC000X21_A1537ContratoServicos_PercTmp = new short[1] ;
         BC000X21_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         BC000X21_A1538ContratoServicos_PercPgm = new short[1] ;
         BC000X21_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         BC000X21_A1539ContratoServicos_PercCnc = new short[1] ;
         BC000X21_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         BC000X21_A638ContratoServicos_Ativo = new bool[] {false} ;
         BC000X21_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         BC000X21_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         BC000X21_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         BC000X21_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         BC000X21_A2074ContratoServicos_CalculoRmn = new short[1] ;
         BC000X21_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         BC000X21_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         BC000X21_A155Servico_Codigo = new int[1] ;
         BC000X21_A74Contrato_Codigo = new int[1] ;
         BC000X21_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         BC000X21_A631Servico_Vinculado = new int[1] ;
         BC000X21_n631Servico_Vinculado = new bool[] {false} ;
         BC000X21_A157ServicoGrupo_Codigo = new int[1] ;
         BC000X21_A39Contratada_Codigo = new int[1] ;
         BC000X21_A40Contratada_PessoaCod = new int[1] ;
         BC000X21_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000X21_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         BC000X21_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         BC000X21_A914ContratoServicos_PrazoDias = new short[1] ;
         BC000X21_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         BC000X21_A1377ContratoServicos_Indicadores = new short[1] ;
         BC000X21_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         BC000X21_A2073ContratoServicos_QtdRmn = new short[1] ;
         BC000X21_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         BC000X7_A911ContratoServicos_Prazos = new short[1] ;
         BC000X7_n911ContratoServicos_Prazos = new bool[] {false} ;
         AV30Alias = "";
         BC000X14_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         BC000X14_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         BC000X14_A914ContratoServicos_PrazoDias = new short[1] ;
         BC000X14_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         BC000X16_A1377ContratoServicos_Indicadores = new short[1] ;
         BC000X16_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         BC000X18_A2073ContratoServicos_QtdRmn = new short[1] ;
         BC000X18_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         BC000X9_A77Contrato_Numero = new String[] {""} ;
         BC000X9_A78Contrato_NumeroAta = new String[] {""} ;
         BC000X9_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000X9_A79Contrato_Ano = new short[1] ;
         BC000X9_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000X9_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000X9_A39Contratada_Codigo = new int[1] ;
         BC000X9_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000X12_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000X12_A40Contratada_PessoaCod = new int[1] ;
         BC000X13_A41Contratada_PessoaNom = new String[] {""} ;
         BC000X13_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000X13_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000X13_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000X8_A608Servico_Nome = new String[] {""} ;
         BC000X8_A605Servico_Sigla = new String[] {""} ;
         BC000X8_A1061Servico_Tela = new String[] {""} ;
         BC000X8_n1061Servico_Tela = new bool[] {false} ;
         BC000X8_A632Servico_Ativo = new bool[] {false} ;
         BC000X8_A156Servico_Descricao = new String[] {""} ;
         BC000X8_n156Servico_Descricao = new bool[] {false} ;
         BC000X8_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000X8_A631Servico_Vinculado = new int[1] ;
         BC000X8_n631Servico_Vinculado = new bool[] {false} ;
         BC000X8_A157ServicoGrupo_Codigo = new int[1] ;
         BC000X11_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000X10_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         BC000X10_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         BC000X10_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         BC000X10_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         BC000X22_A160ContratoServicos_Codigo = new int[1] ;
         BC000X5_A160ContratoServicos_Codigo = new int[1] ;
         BC000X5_A1858ContratoServicos_Alias = new String[] {""} ;
         BC000X5_n1858ContratoServicos_Alias = new bool[] {false} ;
         BC000X5_A555Servico_QtdContratada = new long[1] ;
         BC000X5_n555Servico_QtdContratada = new bool[] {false} ;
         BC000X5_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         BC000X5_A558Servico_Percentual = new decimal[1] ;
         BC000X5_n558Servico_Percentual = new bool[] {false} ;
         BC000X5_A607ServicoContrato_Faturamento = new String[] {""} ;
         BC000X5_A639ContratoServicos_LocalExec = new String[] {""} ;
         BC000X5_A868ContratoServicos_TipoVnc = new String[] {""} ;
         BC000X5_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         BC000X5_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X5_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X5_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         BC000X5_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         BC000X5_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         BC000X5_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         BC000X5_A1153ContratoServicos_PrazoResposta = new short[1] ;
         BC000X5_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         BC000X5_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         BC000X5_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         BC000X5_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         BC000X5_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         BC000X5_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         BC000X5_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         BC000X5_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         BC000X5_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         BC000X5_A1649ContratoServicos_PrazoInicio = new short[1] ;
         BC000X5_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         BC000X5_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X5_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X5_A1191ContratoServicos_Produtividade = new decimal[1] ;
         BC000X5_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         BC000X5_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X5_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X5_A1266ContratoServicos_Momento = new String[] {""} ;
         BC000X5_n1266ContratoServicos_Momento = new bool[] {false} ;
         BC000X5_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         BC000X5_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         BC000X5_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         BC000X5_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         BC000X5_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         BC000X5_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         BC000X5_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X5_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X5_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         BC000X5_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         BC000X5_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         BC000X5_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         BC000X5_A1501ContratoServicos_TmpEstExc = new int[1] ;
         BC000X5_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         BC000X5_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         BC000X5_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         BC000X5_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         BC000X5_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         BC000X5_A1537ContratoServicos_PercTmp = new short[1] ;
         BC000X5_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         BC000X5_A1538ContratoServicos_PercPgm = new short[1] ;
         BC000X5_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         BC000X5_A1539ContratoServicos_PercCnc = new short[1] ;
         BC000X5_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         BC000X5_A638ContratoServicos_Ativo = new bool[] {false} ;
         BC000X5_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         BC000X5_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         BC000X5_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         BC000X5_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         BC000X5_A2074ContratoServicos_CalculoRmn = new short[1] ;
         BC000X5_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         BC000X5_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         BC000X5_A155Servico_Codigo = new int[1] ;
         BC000X5_A74Contrato_Codigo = new int[1] ;
         BC000X5_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         BC000X4_A160ContratoServicos_Codigo = new int[1] ;
         BC000X4_A1858ContratoServicos_Alias = new String[] {""} ;
         BC000X4_n1858ContratoServicos_Alias = new bool[] {false} ;
         BC000X4_A555Servico_QtdContratada = new long[1] ;
         BC000X4_n555Servico_QtdContratada = new bool[] {false} ;
         BC000X4_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         BC000X4_A558Servico_Percentual = new decimal[1] ;
         BC000X4_n558Servico_Percentual = new bool[] {false} ;
         BC000X4_A607ServicoContrato_Faturamento = new String[] {""} ;
         BC000X4_A639ContratoServicos_LocalExec = new String[] {""} ;
         BC000X4_A868ContratoServicos_TipoVnc = new String[] {""} ;
         BC000X4_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         BC000X4_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X4_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X4_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         BC000X4_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         BC000X4_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         BC000X4_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         BC000X4_A1153ContratoServicos_PrazoResposta = new short[1] ;
         BC000X4_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         BC000X4_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         BC000X4_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         BC000X4_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         BC000X4_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         BC000X4_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         BC000X4_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         BC000X4_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         BC000X4_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         BC000X4_A1649ContratoServicos_PrazoInicio = new short[1] ;
         BC000X4_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         BC000X4_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X4_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X4_A1191ContratoServicos_Produtividade = new decimal[1] ;
         BC000X4_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         BC000X4_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X4_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X4_A1266ContratoServicos_Momento = new String[] {""} ;
         BC000X4_n1266ContratoServicos_Momento = new bool[] {false} ;
         BC000X4_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         BC000X4_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         BC000X4_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         BC000X4_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         BC000X4_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         BC000X4_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         BC000X4_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X4_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X4_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         BC000X4_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         BC000X4_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         BC000X4_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         BC000X4_A1501ContratoServicos_TmpEstExc = new int[1] ;
         BC000X4_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         BC000X4_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         BC000X4_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         BC000X4_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         BC000X4_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         BC000X4_A1537ContratoServicos_PercTmp = new short[1] ;
         BC000X4_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         BC000X4_A1538ContratoServicos_PercPgm = new short[1] ;
         BC000X4_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         BC000X4_A1539ContratoServicos_PercCnc = new short[1] ;
         BC000X4_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         BC000X4_A638ContratoServicos_Ativo = new bool[] {false} ;
         BC000X4_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         BC000X4_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         BC000X4_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         BC000X4_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         BC000X4_A2074ContratoServicos_CalculoRmn = new short[1] ;
         BC000X4_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         BC000X4_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         BC000X4_A155Servico_Codigo = new int[1] ;
         BC000X4_A74Contrato_Codigo = new int[1] ;
         BC000X4_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         BC000X23_A160ContratoServicos_Codigo = new int[1] ;
         BC000X27_A911ContratoServicos_Prazos = new short[1] ;
         BC000X27_n911ContratoServicos_Prazos = new bool[] {false} ;
         BC000X28_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         BC000X28_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         BC000X28_A914ContratoServicos_PrazoDias = new short[1] ;
         BC000X28_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         BC000X30_A1377ContratoServicos_Indicadores = new short[1] ;
         BC000X30_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         BC000X32_A2073ContratoServicos_QtdRmn = new short[1] ;
         BC000X32_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         BC000X33_A77Contrato_Numero = new String[] {""} ;
         BC000X33_A78Contrato_NumeroAta = new String[] {""} ;
         BC000X33_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000X33_A79Contrato_Ano = new short[1] ;
         BC000X33_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000X33_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000X33_A39Contratada_Codigo = new int[1] ;
         BC000X33_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000X34_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000X34_A40Contratada_PessoaCod = new int[1] ;
         BC000X35_A41Contratada_PessoaNom = new String[] {""} ;
         BC000X35_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000X35_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000X35_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000X36_A608Servico_Nome = new String[] {""} ;
         BC000X36_A605Servico_Sigla = new String[] {""} ;
         BC000X36_A1061Servico_Tela = new String[] {""} ;
         BC000X36_n1061Servico_Tela = new bool[] {false} ;
         BC000X36_A632Servico_Ativo = new bool[] {false} ;
         BC000X36_A156Servico_Descricao = new String[] {""} ;
         BC000X36_n156Servico_Descricao = new bool[] {false} ;
         BC000X36_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000X36_A631Servico_Vinculado = new int[1] ;
         BC000X36_n631Servico_Vinculado = new bool[] {false} ;
         BC000X36_A157ServicoGrupo_Codigo = new int[1] ;
         BC000X37_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000X38_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         BC000X38_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         BC000X38_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         BC000X38_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         BC000X39_A456ContagemResultado_Codigo = new int[1] ;
         BC000X40_A1473ContratoServicosCusto_Codigo = new int[1] ;
         BC000X41_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         BC000X42_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         BC000X43_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         BC000X43_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         BC000X43_A1209AgendaAtendimento_CodDmn = new int[1] ;
         BC000X44_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         BC000X44_A938ContratoServicosTelas_Sequencial = new short[1] ;
         BC000X45_A917ContratoSrvVnc_Codigo = new int[1] ;
         BC000X46_A917ContratoSrvVnc_Codigo = new int[1] ;
         BC000X47_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         BC000X48_A160ContratoServicos_Codigo = new int[1] ;
         BC000X48_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         BC000X49_A160ContratoServicos_Codigo = new int[1] ;
         BC000X49_A1749Artefatos_Codigo = new int[1] ;
         BC000X50_A160ContratoServicos_Codigo = new int[1] ;
         BC000X50_A1465ContratoServicosDePara_Codigo = new int[1] ;
         BC000X51_A160ContratoServicos_Codigo = new int[1] ;
         BC000X51_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         BC000X51_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         BC000X54_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         BC000X54_A160ContratoServicos_Codigo = new int[1] ;
         BC000X54_A77Contrato_Numero = new String[] {""} ;
         BC000X54_A78Contrato_NumeroAta = new String[] {""} ;
         BC000X54_n78Contrato_NumeroAta = new bool[] {false} ;
         BC000X54_A79Contrato_Ano = new short[1] ;
         BC000X54_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         BC000X54_A41Contratada_PessoaNom = new String[] {""} ;
         BC000X54_n41Contratada_PessoaNom = new bool[] {false} ;
         BC000X54_A42Contratada_PessoaCNPJ = new String[] {""} ;
         BC000X54_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         BC000X54_A516Contratada_TipoFabrica = new String[] {""} ;
         BC000X54_A1858ContratoServicos_Alias = new String[] {""} ;
         BC000X54_n1858ContratoServicos_Alias = new bool[] {false} ;
         BC000X54_A608Servico_Nome = new String[] {""} ;
         BC000X54_A605Servico_Sigla = new String[] {""} ;
         BC000X54_A1061Servico_Tela = new String[] {""} ;
         BC000X54_n1061Servico_Tela = new bool[] {false} ;
         BC000X54_A632Servico_Ativo = new bool[] {false} ;
         BC000X54_A156Servico_Descricao = new String[] {""} ;
         BC000X54_n156Servico_Descricao = new bool[] {false} ;
         BC000X54_A158ServicoGrupo_Descricao = new String[] {""} ;
         BC000X54_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         BC000X54_A2132ContratoServicos_UndCntNome = new String[] {""} ;
         BC000X54_n2132ContratoServicos_UndCntNome = new bool[] {false} ;
         BC000X54_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         BC000X54_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         BC000X54_A555Servico_QtdContratada = new long[1] ;
         BC000X54_n555Servico_QtdContratada = new bool[] {false} ;
         BC000X54_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         BC000X54_A558Servico_Percentual = new decimal[1] ;
         BC000X54_n558Servico_Percentual = new bool[] {false} ;
         BC000X54_A607ServicoContrato_Faturamento = new String[] {""} ;
         BC000X54_A639ContratoServicos_LocalExec = new String[] {""} ;
         BC000X54_A868ContratoServicos_TipoVnc = new String[] {""} ;
         BC000X54_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         BC000X54_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X54_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         BC000X54_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         BC000X54_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         BC000X54_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         BC000X54_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         BC000X54_A1153ContratoServicos_PrazoResposta = new short[1] ;
         BC000X54_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         BC000X54_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         BC000X54_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         BC000X54_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         BC000X54_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         BC000X54_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         BC000X54_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         BC000X54_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         BC000X54_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         BC000X54_A1649ContratoServicos_PrazoInicio = new short[1] ;
         BC000X54_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         BC000X54_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X54_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         BC000X54_A1191ContratoServicos_Produtividade = new decimal[1] ;
         BC000X54_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         BC000X54_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X54_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         BC000X54_A1266ContratoServicos_Momento = new String[] {""} ;
         BC000X54_n1266ContratoServicos_Momento = new bool[] {false} ;
         BC000X54_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         BC000X54_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         BC000X54_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         BC000X54_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         BC000X54_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         BC000X54_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         BC000X54_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X54_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         BC000X54_A453Contrato_IndiceDivergencia = new decimal[1] ;
         BC000X54_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         BC000X54_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         BC000X54_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         BC000X54_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         BC000X54_A1501ContratoServicos_TmpEstExc = new int[1] ;
         BC000X54_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         BC000X54_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         BC000X54_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         BC000X54_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         BC000X54_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         BC000X54_A1537ContratoServicos_PercTmp = new short[1] ;
         BC000X54_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         BC000X54_A1538ContratoServicos_PercPgm = new short[1] ;
         BC000X54_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         BC000X54_A1539ContratoServicos_PercCnc = new short[1] ;
         BC000X54_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         BC000X54_A638ContratoServicos_Ativo = new bool[] {false} ;
         BC000X54_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         BC000X54_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         BC000X54_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         BC000X54_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         BC000X54_A2074ContratoServicos_CalculoRmn = new short[1] ;
         BC000X54_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         BC000X54_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         BC000X54_A155Servico_Codigo = new int[1] ;
         BC000X54_A74Contrato_Codigo = new int[1] ;
         BC000X54_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         BC000X54_A631Servico_Vinculado = new int[1] ;
         BC000X54_n631Servico_Vinculado = new bool[] {false} ;
         BC000X54_A157ServicoGrupo_Codigo = new int[1] ;
         BC000X54_A39Contratada_Codigo = new int[1] ;
         BC000X54_A40Contratada_PessoaCod = new int[1] ;
         BC000X54_A75Contrato_AreaTrabalhoCod = new int[1] ;
         BC000X54_A913ContratoServicos_PrazoTipo = new String[] {""} ;
         BC000X54_n913ContratoServicos_PrazoTipo = new bool[] {false} ;
         BC000X54_A914ContratoServicos_PrazoDias = new short[1] ;
         BC000X54_n914ContratoServicos_PrazoDias = new bool[] {false} ;
         BC000X54_A1377ContratoServicos_Indicadores = new short[1] ;
         BC000X54_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         BC000X54_A2073ContratoServicos_QtdRmn = new short[1] ;
         BC000X54_n2073ContratoServicos_QtdRmn = new bool[] {false} ;
         BC000X55_A160ContratoServicos_Codigo = new int[1] ;
         BC000X55_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         BC000X55_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         BC000X55_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         BC000X55_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         BC000X56_A160ContratoServicos_Codigo = new int[1] ;
         BC000X56_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         BC000X3_A160ContratoServicos_Codigo = new int[1] ;
         BC000X3_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         BC000X3_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         BC000X3_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         BC000X3_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         sMode228 = "";
         BC000X2_A160ContratoServicos_Codigo = new int[1] ;
         BC000X2_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         BC000X2_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         BC000X2_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         BC000X2_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         BC000X60_A160ContratoServicos_Codigo = new int[1] ;
         BC000X60_A2069ContratoServicosRmn_Sequencial = new short[1] ;
         BC000X60_A2070ContratoServicosRmn_Inicio = new decimal[1] ;
         BC000X60_A2071ContratoServicosRmn_Fim = new decimal[1] ;
         BC000X60_A2072ContratoServicosRmn_Valor = new decimal[1] ;
         i607ServicoContrato_Faturamento = "";
         i1454ContratoServicos_PrazoTpDias = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicos_bc__default(),
            new Object[][] {
                new Object[] {
               BC000X2_A160ContratoServicos_Codigo, BC000X2_A2069ContratoServicosRmn_Sequencial, BC000X2_A2070ContratoServicosRmn_Inicio, BC000X2_A2071ContratoServicosRmn_Fim, BC000X2_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               BC000X3_A160ContratoServicos_Codigo, BC000X3_A2069ContratoServicosRmn_Sequencial, BC000X3_A2070ContratoServicosRmn_Inicio, BC000X3_A2071ContratoServicosRmn_Fim, BC000X3_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               BC000X4_A160ContratoServicos_Codigo, BC000X4_A1858ContratoServicos_Alias, BC000X4_n1858ContratoServicos_Alias, BC000X4_A555Servico_QtdContratada, BC000X4_n555Servico_QtdContratada, BC000X4_A557Servico_VlrUnidadeContratada, BC000X4_A558Servico_Percentual, BC000X4_n558Servico_Percentual, BC000X4_A607ServicoContrato_Faturamento, BC000X4_A639ContratoServicos_LocalExec,
               BC000X4_A868ContratoServicos_TipoVnc, BC000X4_n868ContratoServicos_TipoVnc, BC000X4_A888ContratoServicos_HmlSemCnf, BC000X4_n888ContratoServicos_HmlSemCnf, BC000X4_A1454ContratoServicos_PrazoTpDias, BC000X4_n1454ContratoServicos_PrazoTpDias, BC000X4_A1152ContratoServicos_PrazoAnalise, BC000X4_n1152ContratoServicos_PrazoAnalise, BC000X4_A1153ContratoServicos_PrazoResposta, BC000X4_n1153ContratoServicos_PrazoResposta,
               BC000X4_A1181ContratoServicos_PrazoGarantia, BC000X4_n1181ContratoServicos_PrazoGarantia, BC000X4_A1182ContratoServicos_PrazoAtendeGarantia, BC000X4_n1182ContratoServicos_PrazoAtendeGarantia, BC000X4_A1224ContratoServicos_PrazoCorrecao, BC000X4_n1224ContratoServicos_PrazoCorrecao, BC000X4_A1225ContratoServicos_PrazoCorrecaoTipo, BC000X4_n1225ContratoServicos_PrazoCorrecaoTipo, BC000X4_A1649ContratoServicos_PrazoInicio, BC000X4_n1649ContratoServicos_PrazoInicio,
               BC000X4_A1190ContratoServicos_PrazoImediato, BC000X4_n1190ContratoServicos_PrazoImediato, BC000X4_A1191ContratoServicos_Produtividade, BC000X4_n1191ContratoServicos_Produtividade, BC000X4_A1217ContratoServicos_EspelhaAceite, BC000X4_n1217ContratoServicos_EspelhaAceite, BC000X4_A1266ContratoServicos_Momento, BC000X4_n1266ContratoServicos_Momento, BC000X4_A1325ContratoServicos_StatusPagFnc, BC000X4_n1325ContratoServicos_StatusPagFnc,
               BC000X4_A1340ContratoServicos_QntUntCns, BC000X4_n1340ContratoServicos_QntUntCns, BC000X4_A1341ContratoServicos_FatorCnvUndCnt, BC000X4_n1341ContratoServicos_FatorCnvUndCnt, BC000X4_A1397ContratoServicos_NaoRequerAtr, BC000X4_n1397ContratoServicos_NaoRequerAtr, BC000X4_A1455ContratoServicos_IndiceDivergencia, BC000X4_n1455ContratoServicos_IndiceDivergencia, BC000X4_A1516ContratoServicos_TmpEstAnl, BC000X4_n1516ContratoServicos_TmpEstAnl,
               BC000X4_A1501ContratoServicos_TmpEstExc, BC000X4_n1501ContratoServicos_TmpEstExc, BC000X4_A1502ContratoServicos_TmpEstCrr, BC000X4_n1502ContratoServicos_TmpEstCrr, BC000X4_A1531ContratoServicos_TipoHierarquia, BC000X4_n1531ContratoServicos_TipoHierarquia, BC000X4_A1537ContratoServicos_PercTmp, BC000X4_n1537ContratoServicos_PercTmp, BC000X4_A1538ContratoServicos_PercPgm, BC000X4_n1538ContratoServicos_PercPgm,
               BC000X4_A1539ContratoServicos_PercCnc, BC000X4_n1539ContratoServicos_PercCnc, BC000X4_A638ContratoServicos_Ativo, BC000X4_A1723ContratoServicos_CodigoFiscal, BC000X4_n1723ContratoServicos_CodigoFiscal, BC000X4_A1799ContratoServicos_LimiteProposta, BC000X4_n1799ContratoServicos_LimiteProposta, BC000X4_A2074ContratoServicos_CalculoRmn, BC000X4_n2074ContratoServicos_CalculoRmn, BC000X4_A2094ContratoServicos_SolicitaGestorSistema,
               BC000X4_A155Servico_Codigo, BC000X4_A74Contrato_Codigo, BC000X4_A1212ContratoServicos_UnidadeContratada
               }
               , new Object[] {
               BC000X5_A160ContratoServicos_Codigo, BC000X5_A1858ContratoServicos_Alias, BC000X5_n1858ContratoServicos_Alias, BC000X5_A555Servico_QtdContratada, BC000X5_n555Servico_QtdContratada, BC000X5_A557Servico_VlrUnidadeContratada, BC000X5_A558Servico_Percentual, BC000X5_n558Servico_Percentual, BC000X5_A607ServicoContrato_Faturamento, BC000X5_A639ContratoServicos_LocalExec,
               BC000X5_A868ContratoServicos_TipoVnc, BC000X5_n868ContratoServicos_TipoVnc, BC000X5_A888ContratoServicos_HmlSemCnf, BC000X5_n888ContratoServicos_HmlSemCnf, BC000X5_A1454ContratoServicos_PrazoTpDias, BC000X5_n1454ContratoServicos_PrazoTpDias, BC000X5_A1152ContratoServicos_PrazoAnalise, BC000X5_n1152ContratoServicos_PrazoAnalise, BC000X5_A1153ContratoServicos_PrazoResposta, BC000X5_n1153ContratoServicos_PrazoResposta,
               BC000X5_A1181ContratoServicos_PrazoGarantia, BC000X5_n1181ContratoServicos_PrazoGarantia, BC000X5_A1182ContratoServicos_PrazoAtendeGarantia, BC000X5_n1182ContratoServicos_PrazoAtendeGarantia, BC000X5_A1224ContratoServicos_PrazoCorrecao, BC000X5_n1224ContratoServicos_PrazoCorrecao, BC000X5_A1225ContratoServicos_PrazoCorrecaoTipo, BC000X5_n1225ContratoServicos_PrazoCorrecaoTipo, BC000X5_A1649ContratoServicos_PrazoInicio, BC000X5_n1649ContratoServicos_PrazoInicio,
               BC000X5_A1190ContratoServicos_PrazoImediato, BC000X5_n1190ContratoServicos_PrazoImediato, BC000X5_A1191ContratoServicos_Produtividade, BC000X5_n1191ContratoServicos_Produtividade, BC000X5_A1217ContratoServicos_EspelhaAceite, BC000X5_n1217ContratoServicos_EspelhaAceite, BC000X5_A1266ContratoServicos_Momento, BC000X5_n1266ContratoServicos_Momento, BC000X5_A1325ContratoServicos_StatusPagFnc, BC000X5_n1325ContratoServicos_StatusPagFnc,
               BC000X5_A1340ContratoServicos_QntUntCns, BC000X5_n1340ContratoServicos_QntUntCns, BC000X5_A1341ContratoServicos_FatorCnvUndCnt, BC000X5_n1341ContratoServicos_FatorCnvUndCnt, BC000X5_A1397ContratoServicos_NaoRequerAtr, BC000X5_n1397ContratoServicos_NaoRequerAtr, BC000X5_A1455ContratoServicos_IndiceDivergencia, BC000X5_n1455ContratoServicos_IndiceDivergencia, BC000X5_A1516ContratoServicos_TmpEstAnl, BC000X5_n1516ContratoServicos_TmpEstAnl,
               BC000X5_A1501ContratoServicos_TmpEstExc, BC000X5_n1501ContratoServicos_TmpEstExc, BC000X5_A1502ContratoServicos_TmpEstCrr, BC000X5_n1502ContratoServicos_TmpEstCrr, BC000X5_A1531ContratoServicos_TipoHierarquia, BC000X5_n1531ContratoServicos_TipoHierarquia, BC000X5_A1537ContratoServicos_PercTmp, BC000X5_n1537ContratoServicos_PercTmp, BC000X5_A1538ContratoServicos_PercPgm, BC000X5_n1538ContratoServicos_PercPgm,
               BC000X5_A1539ContratoServicos_PercCnc, BC000X5_n1539ContratoServicos_PercCnc, BC000X5_A638ContratoServicos_Ativo, BC000X5_A1723ContratoServicos_CodigoFiscal, BC000X5_n1723ContratoServicos_CodigoFiscal, BC000X5_A1799ContratoServicos_LimiteProposta, BC000X5_n1799ContratoServicos_LimiteProposta, BC000X5_A2074ContratoServicos_CalculoRmn, BC000X5_n2074ContratoServicos_CalculoRmn, BC000X5_A2094ContratoServicos_SolicitaGestorSistema,
               BC000X5_A155Servico_Codigo, BC000X5_A74Contrato_Codigo, BC000X5_A1212ContratoServicos_UnidadeContratada
               }
               , new Object[] {
               BC000X7_A911ContratoServicos_Prazos, BC000X7_n911ContratoServicos_Prazos
               }
               , new Object[] {
               BC000X8_A608Servico_Nome, BC000X8_A605Servico_Sigla, BC000X8_A1061Servico_Tela, BC000X8_n1061Servico_Tela, BC000X8_A632Servico_Ativo, BC000X8_A156Servico_Descricao, BC000X8_n156Servico_Descricao, BC000X8_A2092Servico_IsOrigemReferencia, BC000X8_A631Servico_Vinculado, BC000X8_n631Servico_Vinculado,
               BC000X8_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               BC000X9_A77Contrato_Numero, BC000X9_A78Contrato_NumeroAta, BC000X9_n78Contrato_NumeroAta, BC000X9_A79Contrato_Ano, BC000X9_A116Contrato_ValorUnidadeContratacao, BC000X9_A453Contrato_IndiceDivergencia, BC000X9_A39Contratada_Codigo, BC000X9_A75Contrato_AreaTrabalhoCod
               }
               , new Object[] {
               BC000X10_A2132ContratoServicos_UndCntNome, BC000X10_n2132ContratoServicos_UndCntNome, BC000X10_A1712ContratoServicos_UndCntSgl, BC000X10_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               BC000X11_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               BC000X12_A516Contratada_TipoFabrica, BC000X12_A40Contratada_PessoaCod
               }
               , new Object[] {
               BC000X13_A41Contratada_PessoaNom, BC000X13_n41Contratada_PessoaNom, BC000X13_A42Contratada_PessoaCNPJ, BC000X13_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               BC000X14_A913ContratoServicos_PrazoTipo, BC000X14_n913ContratoServicos_PrazoTipo, BC000X14_A914ContratoServicos_PrazoDias, BC000X14_n914ContratoServicos_PrazoDias
               }
               , new Object[] {
               BC000X16_A1377ContratoServicos_Indicadores, BC000X16_n1377ContratoServicos_Indicadores
               }
               , new Object[] {
               BC000X18_A2073ContratoServicos_QtdRmn, BC000X18_n2073ContratoServicos_QtdRmn
               }
               , new Object[] {
               BC000X21_A903ContratoServicosPrazo_CntSrvCod, BC000X21_A160ContratoServicos_Codigo, BC000X21_A77Contrato_Numero, BC000X21_A78Contrato_NumeroAta, BC000X21_n78Contrato_NumeroAta, BC000X21_A79Contrato_Ano, BC000X21_A116Contrato_ValorUnidadeContratacao, BC000X21_A41Contratada_PessoaNom, BC000X21_n41Contratada_PessoaNom, BC000X21_A42Contratada_PessoaCNPJ,
               BC000X21_n42Contratada_PessoaCNPJ, BC000X21_A516Contratada_TipoFabrica, BC000X21_A1858ContratoServicos_Alias, BC000X21_n1858ContratoServicos_Alias, BC000X21_A608Servico_Nome, BC000X21_A605Servico_Sigla, BC000X21_A1061Servico_Tela, BC000X21_n1061Servico_Tela, BC000X21_A632Servico_Ativo, BC000X21_A156Servico_Descricao,
               BC000X21_n156Servico_Descricao, BC000X21_A158ServicoGrupo_Descricao, BC000X21_A2092Servico_IsOrigemReferencia, BC000X21_A2132ContratoServicos_UndCntNome, BC000X21_n2132ContratoServicos_UndCntNome, BC000X21_A1712ContratoServicos_UndCntSgl, BC000X21_n1712ContratoServicos_UndCntSgl, BC000X21_A555Servico_QtdContratada, BC000X21_n555Servico_QtdContratada, BC000X21_A557Servico_VlrUnidadeContratada,
               BC000X21_A558Servico_Percentual, BC000X21_n558Servico_Percentual, BC000X21_A607ServicoContrato_Faturamento, BC000X21_A639ContratoServicos_LocalExec, BC000X21_A868ContratoServicos_TipoVnc, BC000X21_n868ContratoServicos_TipoVnc, BC000X21_A888ContratoServicos_HmlSemCnf, BC000X21_n888ContratoServicos_HmlSemCnf, BC000X21_A1454ContratoServicos_PrazoTpDias, BC000X21_n1454ContratoServicos_PrazoTpDias,
               BC000X21_A1152ContratoServicos_PrazoAnalise, BC000X21_n1152ContratoServicos_PrazoAnalise, BC000X21_A1153ContratoServicos_PrazoResposta, BC000X21_n1153ContratoServicos_PrazoResposta, BC000X21_A1181ContratoServicos_PrazoGarantia, BC000X21_n1181ContratoServicos_PrazoGarantia, BC000X21_A1182ContratoServicos_PrazoAtendeGarantia, BC000X21_n1182ContratoServicos_PrazoAtendeGarantia, BC000X21_A1224ContratoServicos_PrazoCorrecao, BC000X21_n1224ContratoServicos_PrazoCorrecao,
               BC000X21_A1225ContratoServicos_PrazoCorrecaoTipo, BC000X21_n1225ContratoServicos_PrazoCorrecaoTipo, BC000X21_A1649ContratoServicos_PrazoInicio, BC000X21_n1649ContratoServicos_PrazoInicio, BC000X21_A1190ContratoServicos_PrazoImediato, BC000X21_n1190ContratoServicos_PrazoImediato, BC000X21_A1191ContratoServicos_Produtividade, BC000X21_n1191ContratoServicos_Produtividade, BC000X21_A1217ContratoServicos_EspelhaAceite, BC000X21_n1217ContratoServicos_EspelhaAceite,
               BC000X21_A1266ContratoServicos_Momento, BC000X21_n1266ContratoServicos_Momento, BC000X21_A1325ContratoServicos_StatusPagFnc, BC000X21_n1325ContratoServicos_StatusPagFnc, BC000X21_A1340ContratoServicos_QntUntCns, BC000X21_n1340ContratoServicos_QntUntCns, BC000X21_A1341ContratoServicos_FatorCnvUndCnt, BC000X21_n1341ContratoServicos_FatorCnvUndCnt, BC000X21_A1397ContratoServicos_NaoRequerAtr, BC000X21_n1397ContratoServicos_NaoRequerAtr,
               BC000X21_A453Contrato_IndiceDivergencia, BC000X21_A1455ContratoServicos_IndiceDivergencia, BC000X21_n1455ContratoServicos_IndiceDivergencia, BC000X21_A1516ContratoServicos_TmpEstAnl, BC000X21_n1516ContratoServicos_TmpEstAnl, BC000X21_A1501ContratoServicos_TmpEstExc, BC000X21_n1501ContratoServicos_TmpEstExc, BC000X21_A1502ContratoServicos_TmpEstCrr, BC000X21_n1502ContratoServicos_TmpEstCrr, BC000X21_A1531ContratoServicos_TipoHierarquia,
               BC000X21_n1531ContratoServicos_TipoHierarquia, BC000X21_A1537ContratoServicos_PercTmp, BC000X21_n1537ContratoServicos_PercTmp, BC000X21_A1538ContratoServicos_PercPgm, BC000X21_n1538ContratoServicos_PercPgm, BC000X21_A1539ContratoServicos_PercCnc, BC000X21_n1539ContratoServicos_PercCnc, BC000X21_A638ContratoServicos_Ativo, BC000X21_A1723ContratoServicos_CodigoFiscal, BC000X21_n1723ContratoServicos_CodigoFiscal,
               BC000X21_A1799ContratoServicos_LimiteProposta, BC000X21_n1799ContratoServicos_LimiteProposta, BC000X21_A2074ContratoServicos_CalculoRmn, BC000X21_n2074ContratoServicos_CalculoRmn, BC000X21_A2094ContratoServicos_SolicitaGestorSistema, BC000X21_A155Servico_Codigo, BC000X21_A74Contrato_Codigo, BC000X21_A1212ContratoServicos_UnidadeContratada, BC000X21_A631Servico_Vinculado, BC000X21_n631Servico_Vinculado,
               BC000X21_A157ServicoGrupo_Codigo, BC000X21_A39Contratada_Codigo, BC000X21_A40Contratada_PessoaCod, BC000X21_A75Contrato_AreaTrabalhoCod, BC000X21_A913ContratoServicos_PrazoTipo, BC000X21_n913ContratoServicos_PrazoTipo, BC000X21_A914ContratoServicos_PrazoDias, BC000X21_n914ContratoServicos_PrazoDias, BC000X21_A1377ContratoServicos_Indicadores, BC000X21_n1377ContratoServicos_Indicadores,
               BC000X21_A2073ContratoServicos_QtdRmn, BC000X21_n2073ContratoServicos_QtdRmn
               }
               , new Object[] {
               BC000X22_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC000X23_A160ContratoServicos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000X27_A911ContratoServicos_Prazos, BC000X27_n911ContratoServicos_Prazos
               }
               , new Object[] {
               BC000X28_A913ContratoServicos_PrazoTipo, BC000X28_n913ContratoServicos_PrazoTipo, BC000X28_A914ContratoServicos_PrazoDias, BC000X28_n914ContratoServicos_PrazoDias
               }
               , new Object[] {
               BC000X30_A1377ContratoServicos_Indicadores, BC000X30_n1377ContratoServicos_Indicadores
               }
               , new Object[] {
               BC000X32_A2073ContratoServicos_QtdRmn, BC000X32_n2073ContratoServicos_QtdRmn
               }
               , new Object[] {
               BC000X33_A77Contrato_Numero, BC000X33_A78Contrato_NumeroAta, BC000X33_n78Contrato_NumeroAta, BC000X33_A79Contrato_Ano, BC000X33_A116Contrato_ValorUnidadeContratacao, BC000X33_A453Contrato_IndiceDivergencia, BC000X33_A39Contratada_Codigo, BC000X33_A75Contrato_AreaTrabalhoCod
               }
               , new Object[] {
               BC000X34_A516Contratada_TipoFabrica, BC000X34_A40Contratada_PessoaCod
               }
               , new Object[] {
               BC000X35_A41Contratada_PessoaNom, BC000X35_n41Contratada_PessoaNom, BC000X35_A42Contratada_PessoaCNPJ, BC000X35_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               BC000X36_A608Servico_Nome, BC000X36_A605Servico_Sigla, BC000X36_A1061Servico_Tela, BC000X36_n1061Servico_Tela, BC000X36_A632Servico_Ativo, BC000X36_A156Servico_Descricao, BC000X36_n156Servico_Descricao, BC000X36_A2092Servico_IsOrigemReferencia, BC000X36_A631Servico_Vinculado, BC000X36_n631Servico_Vinculado,
               BC000X36_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               BC000X37_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               BC000X38_A2132ContratoServicos_UndCntNome, BC000X38_n2132ContratoServicos_UndCntNome, BC000X38_A1712ContratoServicos_UndCntSgl, BC000X38_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               BC000X39_A456ContagemResultado_Codigo
               }
               , new Object[] {
               BC000X40_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               BC000X41_A1336ContratoServicosPrioridade_Codigo
               }
               , new Object[] {
               BC000X42_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               BC000X43_A1183AgendaAtendimento_CntSrcCod, BC000X43_A1184AgendaAtendimento_Data, BC000X43_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               BC000X44_A926ContratoServicosTelas_ContratoCod, BC000X44_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               BC000X45_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               BC000X46_A917ContratoSrvVnc_Codigo
               }
               , new Object[] {
               BC000X47_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               BC000X48_A160ContratoServicos_Codigo, BC000X48_A2110ContratoServicosUnidConversao_Codigo
               }
               , new Object[] {
               BC000X49_A160ContratoServicos_Codigo, BC000X49_A1749Artefatos_Codigo
               }
               , new Object[] {
               BC000X50_A160ContratoServicos_Codigo, BC000X50_A1465ContratoServicosDePara_Codigo
               }
               , new Object[] {
               BC000X51_A160ContratoServicos_Codigo, BC000X51_A1067ContratoServicosSistemas_ServicoCod, BC000X51_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               BC000X54_A903ContratoServicosPrazo_CntSrvCod, BC000X54_A160ContratoServicos_Codigo, BC000X54_A77Contrato_Numero, BC000X54_A78Contrato_NumeroAta, BC000X54_n78Contrato_NumeroAta, BC000X54_A79Contrato_Ano, BC000X54_A116Contrato_ValorUnidadeContratacao, BC000X54_A41Contratada_PessoaNom, BC000X54_n41Contratada_PessoaNom, BC000X54_A42Contratada_PessoaCNPJ,
               BC000X54_n42Contratada_PessoaCNPJ, BC000X54_A516Contratada_TipoFabrica, BC000X54_A1858ContratoServicos_Alias, BC000X54_n1858ContratoServicos_Alias, BC000X54_A608Servico_Nome, BC000X54_A605Servico_Sigla, BC000X54_A1061Servico_Tela, BC000X54_n1061Servico_Tela, BC000X54_A632Servico_Ativo, BC000X54_A156Servico_Descricao,
               BC000X54_n156Servico_Descricao, BC000X54_A158ServicoGrupo_Descricao, BC000X54_A2092Servico_IsOrigemReferencia, BC000X54_A2132ContratoServicos_UndCntNome, BC000X54_n2132ContratoServicos_UndCntNome, BC000X54_A1712ContratoServicos_UndCntSgl, BC000X54_n1712ContratoServicos_UndCntSgl, BC000X54_A555Servico_QtdContratada, BC000X54_n555Servico_QtdContratada, BC000X54_A557Servico_VlrUnidadeContratada,
               BC000X54_A558Servico_Percentual, BC000X54_n558Servico_Percentual, BC000X54_A607ServicoContrato_Faturamento, BC000X54_A639ContratoServicos_LocalExec, BC000X54_A868ContratoServicos_TipoVnc, BC000X54_n868ContratoServicos_TipoVnc, BC000X54_A888ContratoServicos_HmlSemCnf, BC000X54_n888ContratoServicos_HmlSemCnf, BC000X54_A1454ContratoServicos_PrazoTpDias, BC000X54_n1454ContratoServicos_PrazoTpDias,
               BC000X54_A1152ContratoServicos_PrazoAnalise, BC000X54_n1152ContratoServicos_PrazoAnalise, BC000X54_A1153ContratoServicos_PrazoResposta, BC000X54_n1153ContratoServicos_PrazoResposta, BC000X54_A1181ContratoServicos_PrazoGarantia, BC000X54_n1181ContratoServicos_PrazoGarantia, BC000X54_A1182ContratoServicos_PrazoAtendeGarantia, BC000X54_n1182ContratoServicos_PrazoAtendeGarantia, BC000X54_A1224ContratoServicos_PrazoCorrecao, BC000X54_n1224ContratoServicos_PrazoCorrecao,
               BC000X54_A1225ContratoServicos_PrazoCorrecaoTipo, BC000X54_n1225ContratoServicos_PrazoCorrecaoTipo, BC000X54_A1649ContratoServicos_PrazoInicio, BC000X54_n1649ContratoServicos_PrazoInicio, BC000X54_A1190ContratoServicos_PrazoImediato, BC000X54_n1190ContratoServicos_PrazoImediato, BC000X54_A1191ContratoServicos_Produtividade, BC000X54_n1191ContratoServicos_Produtividade, BC000X54_A1217ContratoServicos_EspelhaAceite, BC000X54_n1217ContratoServicos_EspelhaAceite,
               BC000X54_A1266ContratoServicos_Momento, BC000X54_n1266ContratoServicos_Momento, BC000X54_A1325ContratoServicos_StatusPagFnc, BC000X54_n1325ContratoServicos_StatusPagFnc, BC000X54_A1340ContratoServicos_QntUntCns, BC000X54_n1340ContratoServicos_QntUntCns, BC000X54_A1341ContratoServicos_FatorCnvUndCnt, BC000X54_n1341ContratoServicos_FatorCnvUndCnt, BC000X54_A1397ContratoServicos_NaoRequerAtr, BC000X54_n1397ContratoServicos_NaoRequerAtr,
               BC000X54_A453Contrato_IndiceDivergencia, BC000X54_A1455ContratoServicos_IndiceDivergencia, BC000X54_n1455ContratoServicos_IndiceDivergencia, BC000X54_A1516ContratoServicos_TmpEstAnl, BC000X54_n1516ContratoServicos_TmpEstAnl, BC000X54_A1501ContratoServicos_TmpEstExc, BC000X54_n1501ContratoServicos_TmpEstExc, BC000X54_A1502ContratoServicos_TmpEstCrr, BC000X54_n1502ContratoServicos_TmpEstCrr, BC000X54_A1531ContratoServicos_TipoHierarquia,
               BC000X54_n1531ContratoServicos_TipoHierarquia, BC000X54_A1537ContratoServicos_PercTmp, BC000X54_n1537ContratoServicos_PercTmp, BC000X54_A1538ContratoServicos_PercPgm, BC000X54_n1538ContratoServicos_PercPgm, BC000X54_A1539ContratoServicos_PercCnc, BC000X54_n1539ContratoServicos_PercCnc, BC000X54_A638ContratoServicos_Ativo, BC000X54_A1723ContratoServicos_CodigoFiscal, BC000X54_n1723ContratoServicos_CodigoFiscal,
               BC000X54_A1799ContratoServicos_LimiteProposta, BC000X54_n1799ContratoServicos_LimiteProposta, BC000X54_A2074ContratoServicos_CalculoRmn, BC000X54_n2074ContratoServicos_CalculoRmn, BC000X54_A2094ContratoServicos_SolicitaGestorSistema, BC000X54_A155Servico_Codigo, BC000X54_A74Contrato_Codigo, BC000X54_A1212ContratoServicos_UnidadeContratada, BC000X54_A631Servico_Vinculado, BC000X54_n631Servico_Vinculado,
               BC000X54_A157ServicoGrupo_Codigo, BC000X54_A39Contratada_Codigo, BC000X54_A40Contratada_PessoaCod, BC000X54_A75Contrato_AreaTrabalhoCod, BC000X54_A913ContratoServicos_PrazoTipo, BC000X54_n913ContratoServicos_PrazoTipo, BC000X54_A914ContratoServicos_PrazoDias, BC000X54_n914ContratoServicos_PrazoDias, BC000X54_A1377ContratoServicos_Indicadores, BC000X54_n1377ContratoServicos_Indicadores,
               BC000X54_A2073ContratoServicos_QtdRmn, BC000X54_n2073ContratoServicos_QtdRmn
               }
               , new Object[] {
               BC000X55_A160ContratoServicos_Codigo, BC000X55_A2069ContratoServicosRmn_Sequencial, BC000X55_A2070ContratoServicosRmn_Inicio, BC000X55_A2071ContratoServicosRmn_Fim, BC000X55_A2072ContratoServicosRmn_Valor
               }
               , new Object[] {
               BC000X56_A160ContratoServicos_Codigo, BC000X56_A2069ContratoServicosRmn_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000X60_A160ContratoServicos_Codigo, BC000X60_A2069ContratoServicosRmn_Sequencial, BC000X60_A2070ContratoServicosRmn_Inicio, BC000X60_A2071ContratoServicosRmn_Fim, BC000X60_A2072ContratoServicosRmn_Valor
               }
            }
         );
         Z2094ContratoServicos_SolicitaGestorSistema = false;
         A2094ContratoServicos_SolicitaGestorSistema = false;
         i2094ContratoServicos_SolicitaGestorSistema = false;
         Z638ContratoServicos_Ativo = true;
         A638ContratoServicos_Ativo = true;
         i638ContratoServicos_Ativo = true;
         Z1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         A1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         i1649ContratoServicos_PrazoInicio = 1;
         n1649ContratoServicos_PrazoInicio = false;
         Z607ServicoContrato_Faturamento = "B";
         A607ServicoContrato_Faturamento = "B";
         i607ServicoContrato_Faturamento = "B";
         AV36Pgmname = "ContratoServicos_BC";
         A557Servico_VlrUnidadeContratada = 0;
         O557Servico_VlrUnidadeContratada = 0;
         Z557Servico_VlrUnidadeContratada = 0;
         Z558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         A558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         i558Servico_Percentual = (decimal)(1);
         n558Servico_Percentual = false;
         Z1455ContratoServicos_IndiceDivergencia = 0;
         n1455ContratoServicos_IndiceDivergencia = false;
         A1455ContratoServicos_IndiceDivergencia = 0;
         n1455ContratoServicos_IndiceDivergencia = false;
         Z1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
         A1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
         i1454ContratoServicos_PrazoTpDias = "U";
         n1454ContratoServicos_PrazoTpDias = false;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E120X2 */
         E120X2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short s2073ContratoServicos_QtdRmn ;
      private short O2073ContratoServicos_QtdRmn ;
      private short A2073ContratoServicos_QtdRmn ;
      private short nGXsfl_228_idx=1 ;
      private short nIsMod_228 ;
      private short RcdFound228 ;
      private short A911ContratoServicos_Prazos ;
      private short GX_JID ;
      private short Z1152ContratoServicos_PrazoAnalise ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short Z1153ContratoServicos_PrazoResposta ;
      private short A1153ContratoServicos_PrazoResposta ;
      private short Z1181ContratoServicos_PrazoGarantia ;
      private short A1181ContratoServicos_PrazoGarantia ;
      private short Z1182ContratoServicos_PrazoAtendeGarantia ;
      private short A1182ContratoServicos_PrazoAtendeGarantia ;
      private short Z1224ContratoServicos_PrazoCorrecao ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short Z1649ContratoServicos_PrazoInicio ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short Z1531ContratoServicos_TipoHierarquia ;
      private short A1531ContratoServicos_TipoHierarquia ;
      private short Z1537ContratoServicos_PercTmp ;
      private short A1537ContratoServicos_PercTmp ;
      private short Z1538ContratoServicos_PercPgm ;
      private short A1538ContratoServicos_PercPgm ;
      private short Z1539ContratoServicos_PercCnc ;
      private short A1539ContratoServicos_PercCnc ;
      private short Z2074ContratoServicos_CalculoRmn ;
      private short A2074ContratoServicos_CalculoRmn ;
      private short Z640Servico_Vinculados ;
      private short A640Servico_Vinculados ;
      private short Z914ContratoServicos_PrazoDias ;
      private short A914ContratoServicos_PrazoDias ;
      private short Z911ContratoServicos_Prazos ;
      private short Z1377ContratoServicos_Indicadores ;
      private short A1377ContratoServicos_Indicadores ;
      private short Z2073ContratoServicos_QtdRmn ;
      private short Z2069ContratoServicosRmn_Sequencial ;
      private short A2069ContratoServicosRmn_Sequencial ;
      private short Z79Contrato_Ano ;
      private short A79Contrato_Ano ;
      private short Gx_BScreen ;
      private short RcdFound34 ;
      private short GXt_int3 ;
      private short nRcdExists_228 ;
      private short Gxremove228 ;
      private short i1649ContratoServicos_PrazoInicio ;
      private short i2073ContratoServicos_QtdRmn ;
      private int trnEnded ;
      private int Z160ContratoServicos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV37GXV1 ;
      private int AV11Insert_Contrato_Codigo ;
      private int AV12Insert_Servico_Codigo ;
      private int AV24Insert_ContratoServicos_UnidadeContratada ;
      private int A155Servico_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1516ContratoServicos_TmpEstAnl ;
      private int A1501ContratoServicos_TmpEstExc ;
      private int A1502ContratoServicos_TmpEstCrr ;
      private int AV7ContratoServicos_Codigo ;
      private int AV15SubServico_Codigo ;
      private int Z1516ContratoServicos_TmpEstAnl ;
      private int Z1501ContratoServicos_TmpEstExc ;
      private int Z1502ContratoServicos_TmpEstCrr ;
      private int Z155Servico_Codigo ;
      private int Z74Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int Z1212ContratoServicos_UnidadeContratada ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int Z827ContratoServicos_ServicoCod ;
      private int A827ContratoServicos_ServicoCod ;
      private int Z1551Servico_Responsavel ;
      private int A1551Servico_Responsavel ;
      private int Z631Servico_Vinculado ;
      private int A631Servico_Vinculado ;
      private int Z157ServicoGrupo_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int Z39Contratada_Codigo ;
      private int A39Contratada_Codigo ;
      private int Z75Contrato_AreaTrabalhoCod ;
      private int Z40Contratada_PessoaCod ;
      private int A40Contratada_PessoaCod ;
      private int GXt_int2 ;
      private long Z555Servico_QtdContratada ;
      private long A555Servico_QtdContratada ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal O557Servico_VlrUnidadeContratada ;
      private decimal Z557Servico_VlrUnidadeContratada ;
      private decimal Z558Servico_Percentual ;
      private decimal A558Servico_Percentual ;
      private decimal Z1191ContratoServicos_Produtividade ;
      private decimal A1191ContratoServicos_Produtividade ;
      private decimal Z1340ContratoServicos_QntUntCns ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal Z1341ContratoServicos_FatorCnvUndCnt ;
      private decimal A1341ContratoServicos_FatorCnvUndCnt ;
      private decimal Z1455ContratoServicos_IndiceDivergencia ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal Z1799ContratoServicos_LimiteProposta ;
      private decimal A1799ContratoServicos_LimiteProposta ;
      private decimal Z2070ContratoServicosRmn_Inicio ;
      private decimal A2070ContratoServicosRmn_Inicio ;
      private decimal Z2071ContratoServicosRmn_Fim ;
      private decimal A2071ContratoServicosRmn_Fim ;
      private decimal Z2072ContratoServicosRmn_Valor ;
      private decimal A2072ContratoServicosRmn_Valor ;
      private decimal Z116Contrato_ValorUnidadeContratacao ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal Z453Contrato_IndiceDivergencia ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal AV21Servico_Percentual ;
      private decimal i558Servico_Percentual ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode34 ;
      private String AV36Pgmname ;
      private String AV32Servico_Nome ;
      private String GXt_char1 ;
      private String Z1858ContratoServicos_Alias ;
      private String A1858ContratoServicos_Alias ;
      private String Z639ContratoServicos_LocalExec ;
      private String A639ContratoServicos_LocalExec ;
      private String Z868ContratoServicos_TipoVnc ;
      private String A868ContratoServicos_TipoVnc ;
      private String Z1454ContratoServicos_PrazoTpDias ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String Z1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String Z1266ContratoServicos_Momento ;
      private String A1266ContratoServicos_Momento ;
      private String Z1325ContratoServicos_StatusPagFnc ;
      private String A1325ContratoServicos_StatusPagFnc ;
      private String Z1723ContratoServicos_CodigoFiscal ;
      private String A1723ContratoServicos_CodigoFiscal ;
      private String Z826ContratoServicos_ServicoSigla ;
      private String A826ContratoServicos_ServicoSigla ;
      private String Z913ContratoServicos_PrazoTipo ;
      private String A913ContratoServicos_PrazoTipo ;
      private String Z608Servico_Nome ;
      private String A608Servico_Nome ;
      private String Z605Servico_Sigla ;
      private String A605Servico_Sigla ;
      private String Z1061Servico_Tela ;
      private String A1061Servico_Tela ;
      private String Z77Contrato_Numero ;
      private String A77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String A78Contrato_NumeroAta ;
      private String Z2132ContratoServicos_UndCntNome ;
      private String A2132ContratoServicos_UndCntNome ;
      private String Z1712ContratoServicos_UndCntSgl ;
      private String A1712ContratoServicos_UndCntSgl ;
      private String Z516Contratada_TipoFabrica ;
      private String A516Contratada_TipoFabrica ;
      private String Z41Contratada_PessoaNom ;
      private String A41Contratada_PessoaNom ;
      private String AV30Alias ;
      private String sMode228 ;
      private String i1454ContratoServicos_PrazoTpDias ;
      private bool n2073ContratoServicos_QtdRmn ;
      private bool returnInSub ;
      private bool Z888ContratoServicos_HmlSemCnf ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool Z1190ContratoServicos_PrazoImediato ;
      private bool A1190ContratoServicos_PrazoImediato ;
      private bool Z1217ContratoServicos_EspelhaAceite ;
      private bool A1217ContratoServicos_EspelhaAceite ;
      private bool Z1397ContratoServicos_NaoRequerAtr ;
      private bool A1397ContratoServicos_NaoRequerAtr ;
      private bool Z638ContratoServicos_Ativo ;
      private bool A638ContratoServicos_Ativo ;
      private bool Z2094ContratoServicos_SolicitaGestorSistema ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool Z632Servico_Ativo ;
      private bool A632Servico_Ativo ;
      private bool Z2092Servico_IsOrigemReferencia ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n558Servico_Percentual ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n1858ContratoServicos_Alias ;
      private bool n1061Servico_Tela ;
      private bool n156Servico_Descricao ;
      private bool n2132ContratoServicos_UndCntNome ;
      private bool n1712ContratoServicos_UndCntSgl ;
      private bool n555Servico_QtdContratada ;
      private bool n868ContratoServicos_TipoVnc ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1153ContratoServicos_PrazoResposta ;
      private bool n1181ContratoServicos_PrazoGarantia ;
      private bool n1182ContratoServicos_PrazoAtendeGarantia ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1190ContratoServicos_PrazoImediato ;
      private bool n1191ContratoServicos_Produtividade ;
      private bool n1217ContratoServicos_EspelhaAceite ;
      private bool n1266ContratoServicos_Momento ;
      private bool n1325ContratoServicos_StatusPagFnc ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1341ContratoServicos_FatorCnvUndCnt ;
      private bool n1397ContratoServicos_NaoRequerAtr ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private bool n1516ContratoServicos_TmpEstAnl ;
      private bool n1501ContratoServicos_TmpEstExc ;
      private bool n1502ContratoServicos_TmpEstCrr ;
      private bool n1531ContratoServicos_TipoHierarquia ;
      private bool n1537ContratoServicos_PercTmp ;
      private bool n1538ContratoServicos_PercPgm ;
      private bool n1539ContratoServicos_PercCnc ;
      private bool n1723ContratoServicos_CodigoFiscal ;
      private bool n1799ContratoServicos_LimiteProposta ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool n631Servico_Vinculado ;
      private bool n913ContratoServicos_PrazoTipo ;
      private bool n914ContratoServicos_PrazoDias ;
      private bool n1377ContratoServicos_Indicadores ;
      private bool n911ContratoServicos_Prazos ;
      private bool AV26CemPorCento ;
      private bool Gx_longc ;
      private bool i2094ContratoServicos_SolicitaGestorSistema ;
      private bool i638ContratoServicos_Ativo ;
      private String Z156Servico_Descricao ;
      private String A156Servico_Descricao ;
      private String Z607ServicoContrato_Faturamento ;
      private String A607ServicoContrato_Faturamento ;
      private String Z2107Servico_Identificacao ;
      private String A2107Servico_Identificacao ;
      private String Z158ServicoGrupo_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String Z42Contratada_PessoaCNPJ ;
      private String A42Contratada_PessoaCNPJ ;
      private String i607ServicoContrato_Faturamento ;
      private IGxSession AV10WebSession ;
      private SdtContratoServicos bcContratoServicos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC000X21_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] BC000X21_A160ContratoServicos_Codigo ;
      private String[] BC000X21_A77Contrato_Numero ;
      private String[] BC000X21_A78Contrato_NumeroAta ;
      private bool[] BC000X21_n78Contrato_NumeroAta ;
      private short[] BC000X21_A79Contrato_Ano ;
      private decimal[] BC000X21_A116Contrato_ValorUnidadeContratacao ;
      private String[] BC000X21_A41Contratada_PessoaNom ;
      private bool[] BC000X21_n41Contratada_PessoaNom ;
      private String[] BC000X21_A42Contratada_PessoaCNPJ ;
      private bool[] BC000X21_n42Contratada_PessoaCNPJ ;
      private String[] BC000X21_A516Contratada_TipoFabrica ;
      private String[] BC000X21_A1858ContratoServicos_Alias ;
      private bool[] BC000X21_n1858ContratoServicos_Alias ;
      private String[] BC000X21_A608Servico_Nome ;
      private String[] BC000X21_A605Servico_Sigla ;
      private String[] BC000X21_A1061Servico_Tela ;
      private bool[] BC000X21_n1061Servico_Tela ;
      private bool[] BC000X21_A632Servico_Ativo ;
      private String[] BC000X21_A156Servico_Descricao ;
      private bool[] BC000X21_n156Servico_Descricao ;
      private String[] BC000X21_A158ServicoGrupo_Descricao ;
      private bool[] BC000X21_A2092Servico_IsOrigemReferencia ;
      private String[] BC000X21_A2132ContratoServicos_UndCntNome ;
      private bool[] BC000X21_n2132ContratoServicos_UndCntNome ;
      private String[] BC000X21_A1712ContratoServicos_UndCntSgl ;
      private bool[] BC000X21_n1712ContratoServicos_UndCntSgl ;
      private long[] BC000X21_A555Servico_QtdContratada ;
      private bool[] BC000X21_n555Servico_QtdContratada ;
      private decimal[] BC000X21_A557Servico_VlrUnidadeContratada ;
      private decimal[] BC000X21_A558Servico_Percentual ;
      private bool[] BC000X21_n558Servico_Percentual ;
      private String[] BC000X21_A607ServicoContrato_Faturamento ;
      private String[] BC000X21_A639ContratoServicos_LocalExec ;
      private String[] BC000X21_A868ContratoServicos_TipoVnc ;
      private bool[] BC000X21_n868ContratoServicos_TipoVnc ;
      private bool[] BC000X21_A888ContratoServicos_HmlSemCnf ;
      private bool[] BC000X21_n888ContratoServicos_HmlSemCnf ;
      private String[] BC000X21_A1454ContratoServicos_PrazoTpDias ;
      private bool[] BC000X21_n1454ContratoServicos_PrazoTpDias ;
      private short[] BC000X21_A1152ContratoServicos_PrazoAnalise ;
      private bool[] BC000X21_n1152ContratoServicos_PrazoAnalise ;
      private short[] BC000X21_A1153ContratoServicos_PrazoResposta ;
      private bool[] BC000X21_n1153ContratoServicos_PrazoResposta ;
      private short[] BC000X21_A1181ContratoServicos_PrazoGarantia ;
      private bool[] BC000X21_n1181ContratoServicos_PrazoGarantia ;
      private short[] BC000X21_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] BC000X21_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] BC000X21_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] BC000X21_n1224ContratoServicos_PrazoCorrecao ;
      private String[] BC000X21_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] BC000X21_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] BC000X21_A1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X21_n1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X21_A1190ContratoServicos_PrazoImediato ;
      private bool[] BC000X21_n1190ContratoServicos_PrazoImediato ;
      private decimal[] BC000X21_A1191ContratoServicos_Produtividade ;
      private bool[] BC000X21_n1191ContratoServicos_Produtividade ;
      private bool[] BC000X21_A1217ContratoServicos_EspelhaAceite ;
      private bool[] BC000X21_n1217ContratoServicos_EspelhaAceite ;
      private String[] BC000X21_A1266ContratoServicos_Momento ;
      private bool[] BC000X21_n1266ContratoServicos_Momento ;
      private String[] BC000X21_A1325ContratoServicos_StatusPagFnc ;
      private bool[] BC000X21_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] BC000X21_A1340ContratoServicos_QntUntCns ;
      private bool[] BC000X21_n1340ContratoServicos_QntUntCns ;
      private decimal[] BC000X21_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X21_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X21_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] BC000X21_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] BC000X21_A453Contrato_IndiceDivergencia ;
      private decimal[] BC000X21_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] BC000X21_n1455ContratoServicos_IndiceDivergencia ;
      private int[] BC000X21_A1516ContratoServicos_TmpEstAnl ;
      private bool[] BC000X21_n1516ContratoServicos_TmpEstAnl ;
      private int[] BC000X21_A1501ContratoServicos_TmpEstExc ;
      private bool[] BC000X21_n1501ContratoServicos_TmpEstExc ;
      private int[] BC000X21_A1502ContratoServicos_TmpEstCrr ;
      private bool[] BC000X21_n1502ContratoServicos_TmpEstCrr ;
      private short[] BC000X21_A1531ContratoServicos_TipoHierarquia ;
      private bool[] BC000X21_n1531ContratoServicos_TipoHierarquia ;
      private short[] BC000X21_A1537ContratoServicos_PercTmp ;
      private bool[] BC000X21_n1537ContratoServicos_PercTmp ;
      private short[] BC000X21_A1538ContratoServicos_PercPgm ;
      private bool[] BC000X21_n1538ContratoServicos_PercPgm ;
      private short[] BC000X21_A1539ContratoServicos_PercCnc ;
      private bool[] BC000X21_n1539ContratoServicos_PercCnc ;
      private bool[] BC000X21_A638ContratoServicos_Ativo ;
      private String[] BC000X21_A1723ContratoServicos_CodigoFiscal ;
      private bool[] BC000X21_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] BC000X21_A1799ContratoServicos_LimiteProposta ;
      private bool[] BC000X21_n1799ContratoServicos_LimiteProposta ;
      private short[] BC000X21_A2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X21_n2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X21_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] BC000X21_A155Servico_Codigo ;
      private int[] BC000X21_A74Contrato_Codigo ;
      private int[] BC000X21_A1212ContratoServicos_UnidadeContratada ;
      private int[] BC000X21_A631Servico_Vinculado ;
      private bool[] BC000X21_n631Servico_Vinculado ;
      private int[] BC000X21_A157ServicoGrupo_Codigo ;
      private int[] BC000X21_A39Contratada_Codigo ;
      private int[] BC000X21_A40Contratada_PessoaCod ;
      private int[] BC000X21_A75Contrato_AreaTrabalhoCod ;
      private String[] BC000X21_A913ContratoServicos_PrazoTipo ;
      private bool[] BC000X21_n913ContratoServicos_PrazoTipo ;
      private short[] BC000X21_A914ContratoServicos_PrazoDias ;
      private bool[] BC000X21_n914ContratoServicos_PrazoDias ;
      private short[] BC000X21_A1377ContratoServicos_Indicadores ;
      private bool[] BC000X21_n1377ContratoServicos_Indicadores ;
      private short[] BC000X21_A2073ContratoServicos_QtdRmn ;
      private bool[] BC000X21_n2073ContratoServicos_QtdRmn ;
      private short[] BC000X7_A911ContratoServicos_Prazos ;
      private bool[] BC000X7_n911ContratoServicos_Prazos ;
      private String[] BC000X14_A913ContratoServicos_PrazoTipo ;
      private bool[] BC000X14_n913ContratoServicos_PrazoTipo ;
      private short[] BC000X14_A914ContratoServicos_PrazoDias ;
      private bool[] BC000X14_n914ContratoServicos_PrazoDias ;
      private short[] BC000X16_A1377ContratoServicos_Indicadores ;
      private bool[] BC000X16_n1377ContratoServicos_Indicadores ;
      private short[] BC000X18_A2073ContratoServicos_QtdRmn ;
      private bool[] BC000X18_n2073ContratoServicos_QtdRmn ;
      private String[] BC000X9_A77Contrato_Numero ;
      private String[] BC000X9_A78Contrato_NumeroAta ;
      private bool[] BC000X9_n78Contrato_NumeroAta ;
      private short[] BC000X9_A79Contrato_Ano ;
      private decimal[] BC000X9_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] BC000X9_A453Contrato_IndiceDivergencia ;
      private int[] BC000X9_A39Contratada_Codigo ;
      private int[] BC000X9_A75Contrato_AreaTrabalhoCod ;
      private String[] BC000X12_A516Contratada_TipoFabrica ;
      private int[] BC000X12_A40Contratada_PessoaCod ;
      private String[] BC000X13_A41Contratada_PessoaNom ;
      private bool[] BC000X13_n41Contratada_PessoaNom ;
      private String[] BC000X13_A42Contratada_PessoaCNPJ ;
      private bool[] BC000X13_n42Contratada_PessoaCNPJ ;
      private String[] BC000X8_A608Servico_Nome ;
      private String[] BC000X8_A605Servico_Sigla ;
      private String[] BC000X8_A1061Servico_Tela ;
      private bool[] BC000X8_n1061Servico_Tela ;
      private bool[] BC000X8_A632Servico_Ativo ;
      private String[] BC000X8_A156Servico_Descricao ;
      private bool[] BC000X8_n156Servico_Descricao ;
      private bool[] BC000X8_A2092Servico_IsOrigemReferencia ;
      private int[] BC000X8_A631Servico_Vinculado ;
      private bool[] BC000X8_n631Servico_Vinculado ;
      private int[] BC000X8_A157ServicoGrupo_Codigo ;
      private String[] BC000X11_A158ServicoGrupo_Descricao ;
      private String[] BC000X10_A2132ContratoServicos_UndCntNome ;
      private bool[] BC000X10_n2132ContratoServicos_UndCntNome ;
      private String[] BC000X10_A1712ContratoServicos_UndCntSgl ;
      private bool[] BC000X10_n1712ContratoServicos_UndCntSgl ;
      private int[] BC000X22_A160ContratoServicos_Codigo ;
      private int[] BC000X5_A160ContratoServicos_Codigo ;
      private String[] BC000X5_A1858ContratoServicos_Alias ;
      private bool[] BC000X5_n1858ContratoServicos_Alias ;
      private long[] BC000X5_A555Servico_QtdContratada ;
      private bool[] BC000X5_n555Servico_QtdContratada ;
      private decimal[] BC000X5_A557Servico_VlrUnidadeContratada ;
      private decimal[] BC000X5_A558Servico_Percentual ;
      private bool[] BC000X5_n558Servico_Percentual ;
      private String[] BC000X5_A607ServicoContrato_Faturamento ;
      private String[] BC000X5_A639ContratoServicos_LocalExec ;
      private String[] BC000X5_A868ContratoServicos_TipoVnc ;
      private bool[] BC000X5_n868ContratoServicos_TipoVnc ;
      private bool[] BC000X5_A888ContratoServicos_HmlSemCnf ;
      private bool[] BC000X5_n888ContratoServicos_HmlSemCnf ;
      private String[] BC000X5_A1454ContratoServicos_PrazoTpDias ;
      private bool[] BC000X5_n1454ContratoServicos_PrazoTpDias ;
      private short[] BC000X5_A1152ContratoServicos_PrazoAnalise ;
      private bool[] BC000X5_n1152ContratoServicos_PrazoAnalise ;
      private short[] BC000X5_A1153ContratoServicos_PrazoResposta ;
      private bool[] BC000X5_n1153ContratoServicos_PrazoResposta ;
      private short[] BC000X5_A1181ContratoServicos_PrazoGarantia ;
      private bool[] BC000X5_n1181ContratoServicos_PrazoGarantia ;
      private short[] BC000X5_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] BC000X5_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] BC000X5_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] BC000X5_n1224ContratoServicos_PrazoCorrecao ;
      private String[] BC000X5_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] BC000X5_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] BC000X5_A1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X5_n1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X5_A1190ContratoServicos_PrazoImediato ;
      private bool[] BC000X5_n1190ContratoServicos_PrazoImediato ;
      private decimal[] BC000X5_A1191ContratoServicos_Produtividade ;
      private bool[] BC000X5_n1191ContratoServicos_Produtividade ;
      private bool[] BC000X5_A1217ContratoServicos_EspelhaAceite ;
      private bool[] BC000X5_n1217ContratoServicos_EspelhaAceite ;
      private String[] BC000X5_A1266ContratoServicos_Momento ;
      private bool[] BC000X5_n1266ContratoServicos_Momento ;
      private String[] BC000X5_A1325ContratoServicos_StatusPagFnc ;
      private bool[] BC000X5_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] BC000X5_A1340ContratoServicos_QntUntCns ;
      private bool[] BC000X5_n1340ContratoServicos_QntUntCns ;
      private decimal[] BC000X5_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X5_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X5_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] BC000X5_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] BC000X5_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] BC000X5_n1455ContratoServicos_IndiceDivergencia ;
      private int[] BC000X5_A1516ContratoServicos_TmpEstAnl ;
      private bool[] BC000X5_n1516ContratoServicos_TmpEstAnl ;
      private int[] BC000X5_A1501ContratoServicos_TmpEstExc ;
      private bool[] BC000X5_n1501ContratoServicos_TmpEstExc ;
      private int[] BC000X5_A1502ContratoServicos_TmpEstCrr ;
      private bool[] BC000X5_n1502ContratoServicos_TmpEstCrr ;
      private short[] BC000X5_A1531ContratoServicos_TipoHierarquia ;
      private bool[] BC000X5_n1531ContratoServicos_TipoHierarquia ;
      private short[] BC000X5_A1537ContratoServicos_PercTmp ;
      private bool[] BC000X5_n1537ContratoServicos_PercTmp ;
      private short[] BC000X5_A1538ContratoServicos_PercPgm ;
      private bool[] BC000X5_n1538ContratoServicos_PercPgm ;
      private short[] BC000X5_A1539ContratoServicos_PercCnc ;
      private bool[] BC000X5_n1539ContratoServicos_PercCnc ;
      private bool[] BC000X5_A638ContratoServicos_Ativo ;
      private String[] BC000X5_A1723ContratoServicos_CodigoFiscal ;
      private bool[] BC000X5_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] BC000X5_A1799ContratoServicos_LimiteProposta ;
      private bool[] BC000X5_n1799ContratoServicos_LimiteProposta ;
      private short[] BC000X5_A2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X5_n2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X5_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] BC000X5_A155Servico_Codigo ;
      private int[] BC000X5_A74Contrato_Codigo ;
      private int[] BC000X5_A1212ContratoServicos_UnidadeContratada ;
      private int[] BC000X4_A160ContratoServicos_Codigo ;
      private String[] BC000X4_A1858ContratoServicos_Alias ;
      private bool[] BC000X4_n1858ContratoServicos_Alias ;
      private long[] BC000X4_A555Servico_QtdContratada ;
      private bool[] BC000X4_n555Servico_QtdContratada ;
      private decimal[] BC000X4_A557Servico_VlrUnidadeContratada ;
      private decimal[] BC000X4_A558Servico_Percentual ;
      private bool[] BC000X4_n558Servico_Percentual ;
      private String[] BC000X4_A607ServicoContrato_Faturamento ;
      private String[] BC000X4_A639ContratoServicos_LocalExec ;
      private String[] BC000X4_A868ContratoServicos_TipoVnc ;
      private bool[] BC000X4_n868ContratoServicos_TipoVnc ;
      private bool[] BC000X4_A888ContratoServicos_HmlSemCnf ;
      private bool[] BC000X4_n888ContratoServicos_HmlSemCnf ;
      private String[] BC000X4_A1454ContratoServicos_PrazoTpDias ;
      private bool[] BC000X4_n1454ContratoServicos_PrazoTpDias ;
      private short[] BC000X4_A1152ContratoServicos_PrazoAnalise ;
      private bool[] BC000X4_n1152ContratoServicos_PrazoAnalise ;
      private short[] BC000X4_A1153ContratoServicos_PrazoResposta ;
      private bool[] BC000X4_n1153ContratoServicos_PrazoResposta ;
      private short[] BC000X4_A1181ContratoServicos_PrazoGarantia ;
      private bool[] BC000X4_n1181ContratoServicos_PrazoGarantia ;
      private short[] BC000X4_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] BC000X4_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] BC000X4_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] BC000X4_n1224ContratoServicos_PrazoCorrecao ;
      private String[] BC000X4_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] BC000X4_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] BC000X4_A1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X4_n1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X4_A1190ContratoServicos_PrazoImediato ;
      private bool[] BC000X4_n1190ContratoServicos_PrazoImediato ;
      private decimal[] BC000X4_A1191ContratoServicos_Produtividade ;
      private bool[] BC000X4_n1191ContratoServicos_Produtividade ;
      private bool[] BC000X4_A1217ContratoServicos_EspelhaAceite ;
      private bool[] BC000X4_n1217ContratoServicos_EspelhaAceite ;
      private String[] BC000X4_A1266ContratoServicos_Momento ;
      private bool[] BC000X4_n1266ContratoServicos_Momento ;
      private String[] BC000X4_A1325ContratoServicos_StatusPagFnc ;
      private bool[] BC000X4_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] BC000X4_A1340ContratoServicos_QntUntCns ;
      private bool[] BC000X4_n1340ContratoServicos_QntUntCns ;
      private decimal[] BC000X4_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X4_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X4_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] BC000X4_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] BC000X4_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] BC000X4_n1455ContratoServicos_IndiceDivergencia ;
      private int[] BC000X4_A1516ContratoServicos_TmpEstAnl ;
      private bool[] BC000X4_n1516ContratoServicos_TmpEstAnl ;
      private int[] BC000X4_A1501ContratoServicos_TmpEstExc ;
      private bool[] BC000X4_n1501ContratoServicos_TmpEstExc ;
      private int[] BC000X4_A1502ContratoServicos_TmpEstCrr ;
      private bool[] BC000X4_n1502ContratoServicos_TmpEstCrr ;
      private short[] BC000X4_A1531ContratoServicos_TipoHierarquia ;
      private bool[] BC000X4_n1531ContratoServicos_TipoHierarquia ;
      private short[] BC000X4_A1537ContratoServicos_PercTmp ;
      private bool[] BC000X4_n1537ContratoServicos_PercTmp ;
      private short[] BC000X4_A1538ContratoServicos_PercPgm ;
      private bool[] BC000X4_n1538ContratoServicos_PercPgm ;
      private short[] BC000X4_A1539ContratoServicos_PercCnc ;
      private bool[] BC000X4_n1539ContratoServicos_PercCnc ;
      private bool[] BC000X4_A638ContratoServicos_Ativo ;
      private String[] BC000X4_A1723ContratoServicos_CodigoFiscal ;
      private bool[] BC000X4_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] BC000X4_A1799ContratoServicos_LimiteProposta ;
      private bool[] BC000X4_n1799ContratoServicos_LimiteProposta ;
      private short[] BC000X4_A2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X4_n2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X4_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] BC000X4_A155Servico_Codigo ;
      private int[] BC000X4_A74Contrato_Codigo ;
      private int[] BC000X4_A1212ContratoServicos_UnidadeContratada ;
      private int[] BC000X23_A160ContratoServicos_Codigo ;
      private short[] BC000X27_A911ContratoServicos_Prazos ;
      private bool[] BC000X27_n911ContratoServicos_Prazos ;
      private String[] BC000X28_A913ContratoServicos_PrazoTipo ;
      private bool[] BC000X28_n913ContratoServicos_PrazoTipo ;
      private short[] BC000X28_A914ContratoServicos_PrazoDias ;
      private bool[] BC000X28_n914ContratoServicos_PrazoDias ;
      private short[] BC000X30_A1377ContratoServicos_Indicadores ;
      private bool[] BC000X30_n1377ContratoServicos_Indicadores ;
      private short[] BC000X32_A2073ContratoServicos_QtdRmn ;
      private bool[] BC000X32_n2073ContratoServicos_QtdRmn ;
      private String[] BC000X33_A77Contrato_Numero ;
      private String[] BC000X33_A78Contrato_NumeroAta ;
      private bool[] BC000X33_n78Contrato_NumeroAta ;
      private short[] BC000X33_A79Contrato_Ano ;
      private decimal[] BC000X33_A116Contrato_ValorUnidadeContratacao ;
      private decimal[] BC000X33_A453Contrato_IndiceDivergencia ;
      private int[] BC000X33_A39Contratada_Codigo ;
      private int[] BC000X33_A75Contrato_AreaTrabalhoCod ;
      private String[] BC000X34_A516Contratada_TipoFabrica ;
      private int[] BC000X34_A40Contratada_PessoaCod ;
      private String[] BC000X35_A41Contratada_PessoaNom ;
      private bool[] BC000X35_n41Contratada_PessoaNom ;
      private String[] BC000X35_A42Contratada_PessoaCNPJ ;
      private bool[] BC000X35_n42Contratada_PessoaCNPJ ;
      private String[] BC000X36_A608Servico_Nome ;
      private String[] BC000X36_A605Servico_Sigla ;
      private String[] BC000X36_A1061Servico_Tela ;
      private bool[] BC000X36_n1061Servico_Tela ;
      private bool[] BC000X36_A632Servico_Ativo ;
      private String[] BC000X36_A156Servico_Descricao ;
      private bool[] BC000X36_n156Servico_Descricao ;
      private bool[] BC000X36_A2092Servico_IsOrigemReferencia ;
      private int[] BC000X36_A631Servico_Vinculado ;
      private bool[] BC000X36_n631Servico_Vinculado ;
      private int[] BC000X36_A157ServicoGrupo_Codigo ;
      private String[] BC000X37_A158ServicoGrupo_Descricao ;
      private String[] BC000X38_A2132ContratoServicos_UndCntNome ;
      private bool[] BC000X38_n2132ContratoServicos_UndCntNome ;
      private String[] BC000X38_A1712ContratoServicos_UndCntSgl ;
      private bool[] BC000X38_n1712ContratoServicos_UndCntSgl ;
      private int[] BC000X39_A456ContagemResultado_Codigo ;
      private int[] BC000X40_A1473ContratoServicosCusto_Codigo ;
      private int[] BC000X41_A1336ContratoServicosPrioridade_Codigo ;
      private int[] BC000X42_A1269ContratoServicosIndicador_Codigo ;
      private int[] BC000X43_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] BC000X43_A1184AgendaAtendimento_Data ;
      private int[] BC000X43_A1209AgendaAtendimento_CodDmn ;
      private int[] BC000X44_A926ContratoServicosTelas_ContratoCod ;
      private short[] BC000X44_A938ContratoServicosTelas_Sequencial ;
      private int[] BC000X45_A917ContratoSrvVnc_Codigo ;
      private int[] BC000X46_A917ContratoSrvVnc_Codigo ;
      private int[] BC000X47_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] BC000X48_A160ContratoServicos_Codigo ;
      private int[] BC000X48_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] BC000X49_A160ContratoServicos_Codigo ;
      private int[] BC000X49_A1749Artefatos_Codigo ;
      private int[] BC000X50_A160ContratoServicos_Codigo ;
      private int[] BC000X50_A1465ContratoServicosDePara_Codigo ;
      private int[] BC000X51_A160ContratoServicos_Codigo ;
      private int[] BC000X51_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] BC000X51_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] BC000X54_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] BC000X54_A160ContratoServicos_Codigo ;
      private String[] BC000X54_A77Contrato_Numero ;
      private String[] BC000X54_A78Contrato_NumeroAta ;
      private bool[] BC000X54_n78Contrato_NumeroAta ;
      private short[] BC000X54_A79Contrato_Ano ;
      private decimal[] BC000X54_A116Contrato_ValorUnidadeContratacao ;
      private String[] BC000X54_A41Contratada_PessoaNom ;
      private bool[] BC000X54_n41Contratada_PessoaNom ;
      private String[] BC000X54_A42Contratada_PessoaCNPJ ;
      private bool[] BC000X54_n42Contratada_PessoaCNPJ ;
      private String[] BC000X54_A516Contratada_TipoFabrica ;
      private String[] BC000X54_A1858ContratoServicos_Alias ;
      private bool[] BC000X54_n1858ContratoServicos_Alias ;
      private String[] BC000X54_A608Servico_Nome ;
      private String[] BC000X54_A605Servico_Sigla ;
      private String[] BC000X54_A1061Servico_Tela ;
      private bool[] BC000X54_n1061Servico_Tela ;
      private bool[] BC000X54_A632Servico_Ativo ;
      private String[] BC000X54_A156Servico_Descricao ;
      private bool[] BC000X54_n156Servico_Descricao ;
      private String[] BC000X54_A158ServicoGrupo_Descricao ;
      private bool[] BC000X54_A2092Servico_IsOrigemReferencia ;
      private String[] BC000X54_A2132ContratoServicos_UndCntNome ;
      private bool[] BC000X54_n2132ContratoServicos_UndCntNome ;
      private String[] BC000X54_A1712ContratoServicos_UndCntSgl ;
      private bool[] BC000X54_n1712ContratoServicos_UndCntSgl ;
      private long[] BC000X54_A555Servico_QtdContratada ;
      private bool[] BC000X54_n555Servico_QtdContratada ;
      private decimal[] BC000X54_A557Servico_VlrUnidadeContratada ;
      private decimal[] BC000X54_A558Servico_Percentual ;
      private bool[] BC000X54_n558Servico_Percentual ;
      private String[] BC000X54_A607ServicoContrato_Faturamento ;
      private String[] BC000X54_A639ContratoServicos_LocalExec ;
      private String[] BC000X54_A868ContratoServicos_TipoVnc ;
      private bool[] BC000X54_n868ContratoServicos_TipoVnc ;
      private bool[] BC000X54_A888ContratoServicos_HmlSemCnf ;
      private bool[] BC000X54_n888ContratoServicos_HmlSemCnf ;
      private String[] BC000X54_A1454ContratoServicos_PrazoTpDias ;
      private bool[] BC000X54_n1454ContratoServicos_PrazoTpDias ;
      private short[] BC000X54_A1152ContratoServicos_PrazoAnalise ;
      private bool[] BC000X54_n1152ContratoServicos_PrazoAnalise ;
      private short[] BC000X54_A1153ContratoServicos_PrazoResposta ;
      private bool[] BC000X54_n1153ContratoServicos_PrazoResposta ;
      private short[] BC000X54_A1181ContratoServicos_PrazoGarantia ;
      private bool[] BC000X54_n1181ContratoServicos_PrazoGarantia ;
      private short[] BC000X54_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] BC000X54_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] BC000X54_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] BC000X54_n1224ContratoServicos_PrazoCorrecao ;
      private String[] BC000X54_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] BC000X54_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] BC000X54_A1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X54_n1649ContratoServicos_PrazoInicio ;
      private bool[] BC000X54_A1190ContratoServicos_PrazoImediato ;
      private bool[] BC000X54_n1190ContratoServicos_PrazoImediato ;
      private decimal[] BC000X54_A1191ContratoServicos_Produtividade ;
      private bool[] BC000X54_n1191ContratoServicos_Produtividade ;
      private bool[] BC000X54_A1217ContratoServicos_EspelhaAceite ;
      private bool[] BC000X54_n1217ContratoServicos_EspelhaAceite ;
      private String[] BC000X54_A1266ContratoServicos_Momento ;
      private bool[] BC000X54_n1266ContratoServicos_Momento ;
      private String[] BC000X54_A1325ContratoServicos_StatusPagFnc ;
      private bool[] BC000X54_n1325ContratoServicos_StatusPagFnc ;
      private decimal[] BC000X54_A1340ContratoServicos_QntUntCns ;
      private bool[] BC000X54_n1340ContratoServicos_QntUntCns ;
      private decimal[] BC000X54_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X54_n1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] BC000X54_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] BC000X54_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] BC000X54_A453Contrato_IndiceDivergencia ;
      private decimal[] BC000X54_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] BC000X54_n1455ContratoServicos_IndiceDivergencia ;
      private int[] BC000X54_A1516ContratoServicos_TmpEstAnl ;
      private bool[] BC000X54_n1516ContratoServicos_TmpEstAnl ;
      private int[] BC000X54_A1501ContratoServicos_TmpEstExc ;
      private bool[] BC000X54_n1501ContratoServicos_TmpEstExc ;
      private int[] BC000X54_A1502ContratoServicos_TmpEstCrr ;
      private bool[] BC000X54_n1502ContratoServicos_TmpEstCrr ;
      private short[] BC000X54_A1531ContratoServicos_TipoHierarquia ;
      private bool[] BC000X54_n1531ContratoServicos_TipoHierarquia ;
      private short[] BC000X54_A1537ContratoServicos_PercTmp ;
      private bool[] BC000X54_n1537ContratoServicos_PercTmp ;
      private short[] BC000X54_A1538ContratoServicos_PercPgm ;
      private bool[] BC000X54_n1538ContratoServicos_PercPgm ;
      private short[] BC000X54_A1539ContratoServicos_PercCnc ;
      private bool[] BC000X54_n1539ContratoServicos_PercCnc ;
      private bool[] BC000X54_A638ContratoServicos_Ativo ;
      private String[] BC000X54_A1723ContratoServicos_CodigoFiscal ;
      private bool[] BC000X54_n1723ContratoServicos_CodigoFiscal ;
      private decimal[] BC000X54_A1799ContratoServicos_LimiteProposta ;
      private bool[] BC000X54_n1799ContratoServicos_LimiteProposta ;
      private short[] BC000X54_A2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X54_n2074ContratoServicos_CalculoRmn ;
      private bool[] BC000X54_A2094ContratoServicos_SolicitaGestorSistema ;
      private int[] BC000X54_A155Servico_Codigo ;
      private int[] BC000X54_A74Contrato_Codigo ;
      private int[] BC000X54_A1212ContratoServicos_UnidadeContratada ;
      private int[] BC000X54_A631Servico_Vinculado ;
      private bool[] BC000X54_n631Servico_Vinculado ;
      private int[] BC000X54_A157ServicoGrupo_Codigo ;
      private int[] BC000X54_A39Contratada_Codigo ;
      private int[] BC000X54_A40Contratada_PessoaCod ;
      private int[] BC000X54_A75Contrato_AreaTrabalhoCod ;
      private String[] BC000X54_A913ContratoServicos_PrazoTipo ;
      private bool[] BC000X54_n913ContratoServicos_PrazoTipo ;
      private short[] BC000X54_A914ContratoServicos_PrazoDias ;
      private bool[] BC000X54_n914ContratoServicos_PrazoDias ;
      private short[] BC000X54_A1377ContratoServicos_Indicadores ;
      private bool[] BC000X54_n1377ContratoServicos_Indicadores ;
      private short[] BC000X54_A2073ContratoServicos_QtdRmn ;
      private bool[] BC000X54_n2073ContratoServicos_QtdRmn ;
      private int[] BC000X55_A160ContratoServicos_Codigo ;
      private short[] BC000X55_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] BC000X55_A2070ContratoServicosRmn_Inicio ;
      private decimal[] BC000X55_A2071ContratoServicosRmn_Fim ;
      private decimal[] BC000X55_A2072ContratoServicosRmn_Valor ;
      private int[] BC000X56_A160ContratoServicos_Codigo ;
      private short[] BC000X56_A2069ContratoServicosRmn_Sequencial ;
      private int[] BC000X3_A160ContratoServicos_Codigo ;
      private short[] BC000X3_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] BC000X3_A2070ContratoServicosRmn_Inicio ;
      private decimal[] BC000X3_A2071ContratoServicosRmn_Fim ;
      private decimal[] BC000X3_A2072ContratoServicosRmn_Valor ;
      private int[] BC000X2_A160ContratoServicos_Codigo ;
      private short[] BC000X2_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] BC000X2_A2070ContratoServicosRmn_Inicio ;
      private decimal[] BC000X2_A2071ContratoServicosRmn_Fim ;
      private decimal[] BC000X2_A2072ContratoServicosRmn_Valor ;
      private int[] BC000X60_A160ContratoServicos_Codigo ;
      private short[] BC000X60_A2069ContratoServicosRmn_Sequencial ;
      private decimal[] BC000X60_A2070ContratoServicosRmn_Inicio ;
      private decimal[] BC000X60_A2071ContratoServicosRmn_Fim ;
      private decimal[] BC000X60_A2072ContratoServicosRmn_Valor ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtAuditingObject AV29AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratoservicos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new UpdateCursor(def[45])
         ,new UpdateCursor(def[46])
         ,new UpdateCursor(def[47])
         ,new ForEachCursor(def[48])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000X21 ;
          prmBC000X21 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000X21 ;
          cmdBufferBC000X21=" SELECT T2.[ContratoServicosPrazo_CntSrvCod], TM1.[ContratoServicos_Codigo], T5.[Contrato_Numero], T5.[Contrato_NumeroAta], T5.[Contrato_Ano], T5.[Contrato_ValorUnidadeContratacao], T7.[Pessoa_Nome] AS Contratada_PessoaNom, T7.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T6.[Contratada_TipoFabrica], TM1.[ContratoServicos_Alias], T8.[Servico_Nome], T8.[Servico_Sigla], T8.[Servico_Tela], T8.[Servico_Ativo], T8.[Servico_Descricao], T9.[ServicoGrupo_Descricao], T8.[Servico_IsOrigemReferencia], T10.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T10.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, TM1.[Servico_QtdContratada], TM1.[Servico_VlrUnidadeContratada], TM1.[Servico_Percentual], TM1.[ServicoContrato_Faturamento], TM1.[ContratoServicos_LocalExec], TM1.[ContratoServicos_TipoVnc], TM1.[ContratoServicos_HmlSemCnf], TM1.[ContratoServicos_PrazoTpDias], TM1.[ContratoServicos_PrazoAnalise], TM1.[ContratoServicos_PrazoResposta], TM1.[ContratoServicos_PrazoGarantia], TM1.[ContratoServicos_PrazoAtendeGarantia], TM1.[ContratoServicos_PrazoCorrecao], TM1.[ContratoServicos_PrazoCorrecaoTipo], TM1.[ContratoServicos_PrazoInicio], TM1.[ContratoServicos_PrazoImediato], TM1.[ContratoServicos_Produtividade], TM1.[ContratoServicos_EspelhaAceite], TM1.[ContratoServicos_Momento], TM1.[ContratoServicos_StatusPagFnc], TM1.[ContratoServicos_QntUntCns], TM1.[ContratoServicos_FatorCnvUndCnt], TM1.[ContratoServicos_NaoRequerAtr], T5.[Contrato_IndiceDivergencia], TM1.[ContratoServicos_IndiceDivergencia], TM1.[ContratoServicos_TmpEstAnl], TM1.[ContratoServicos_TmpEstExc], TM1.[ContratoServicos_TmpEstCrr], TM1.[ContratoServicos_TipoHierarquia], TM1.[ContratoServicos_PercTmp], TM1.[ContratoServicos_PercPgm], TM1.[ContratoServicos_PercCnc], TM1.[ContratoServicos_Ativo], TM1.[ContratoServicos_CodigoFiscal], "
          + " TM1.[ContratoServicos_LimiteProposta], TM1.[ContratoServicos_CalculoRmn], TM1.[ContratoServicos_SolicitaGestorSistema], TM1.[Servico_Codigo], TM1.[Contrato_Codigo], TM1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T8.[Servico_Vinculado], T8.[ServicoGrupo_Codigo], T5.[Contratada_Codigo], T6.[Contratada_PessoaCod] AS Contratada_PessoaCod, T5.[Contrato_AreaTrabalhoCod], COALESCE( T2.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( T2.[ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( T3.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores, COALESCE( T4.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn FROM ((((((((([ContratoServicos] TM1 WITH (NOLOCK) LEFT JOIN [ContratoServicosPrazo] T2 WITH (NOLOCK) ON T2.[ContratoServicosPrazo_CntSrvCod] = TM1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T3 ON T3.[ContratoServicosIndicador_CntSrvCod] = TM1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (NOLOCK) GROUP BY [ContratoServicos_Codigo] ) T4 ON T4.[ContratoServicos_Codigo] = TM1.[ContratoServicos_Codigo]) INNER JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T5.[Contratada_Codigo]) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Contratada_PessoaCod]) INNER JOIN [Servico] T8 WITH (NOLOCK) ON T8.[Servico_Codigo] = TM1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T9 WITH (NOLOCK) ON T9.[ServicoGrupo_Codigo]"
          + " = T8.[ServicoGrupo_Codigo]) INNER JOIN [UnidadeMedicao] T10 WITH (NOLOCK) ON T10.[UnidadeMedicao_Codigo] = TM1.[ContratoServicos_UnidadeContratada]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY TM1.[ContratoServicos_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC000X7 ;
          prmBC000X7 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X14 ;
          prmBC000X14 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X16 ;
          prmBC000X16 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X18 ;
          prmBC000X18 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X9 ;
          prmBC000X9 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X12 ;
          prmBC000X12 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X13 ;
          prmBC000X13 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X8 ;
          prmBC000X8 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X11 ;
          prmBC000X11 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X10 ;
          prmBC000X10 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X22 ;
          prmBC000X22 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X5 ;
          prmBC000X5 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X4 ;
          prmBC000X4 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X23 ;
          prmBC000X23 = new Object[] {
          new Object[] {"@ContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Servico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Servico_Percentual",SqlDbType.Decimal,7,3} ,
          new Object[] {"@ServicoContrato_Faturamento",SqlDbType.VarChar,5,0} ,
          new Object[] {"@ContratoServicos_LocalExec",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_TipoVnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_HmlSemCnf",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoTpDias",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoResposta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAtendeGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecaoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_PrazoInicio",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoImediato",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_EspelhaAceite",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Momento",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_StatusPagFnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_QntUntCns",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_FatorCnvUndCnt",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_NaoRequerAtr",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_CodigoFiscal",SqlDbType.Char,5,0} ,
          new Object[] {"@ContratoServicos_LimiteProposta",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_CalculoRmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_SolicitaGestorSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000X23 ;
          cmdBufferBC000X23=" INSERT INTO [ContratoServicos]([ContratoServicos_Alias], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoTpDias], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_PrazoInicio], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_EspelhaAceite], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercPgm], [ContratoServicos_PercCnc], [ContratoServicos_Ativo], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema], [Servico_Codigo], [Contrato_Codigo], [ContratoServicos_UnidadeContratada]) VALUES(@ContratoServicos_Alias, @Servico_QtdContratada, @Servico_VlrUnidadeContratada, @Servico_Percentual, @ServicoContrato_Faturamento, @ContratoServicos_LocalExec, @ContratoServicos_TipoVnc, @ContratoServicos_HmlSemCnf, @ContratoServicos_PrazoTpDias, @ContratoServicos_PrazoAnalise, @ContratoServicos_PrazoResposta, @ContratoServicos_PrazoGarantia, @ContratoServicos_PrazoAtendeGarantia, @ContratoServicos_PrazoCorrecao, @ContratoServicos_PrazoCorrecaoTipo, @ContratoServicos_PrazoInicio, @ContratoServicos_PrazoImediato, "
          + " @ContratoServicos_Produtividade, @ContratoServicos_EspelhaAceite, @ContratoServicos_Momento, @ContratoServicos_StatusPagFnc, @ContratoServicos_QntUntCns, @ContratoServicos_FatorCnvUndCnt, @ContratoServicos_NaoRequerAtr, @ContratoServicos_IndiceDivergencia, @ContratoServicos_TmpEstAnl, @ContratoServicos_TmpEstExc, @ContratoServicos_TmpEstCrr, @ContratoServicos_TipoHierarquia, @ContratoServicos_PercTmp, @ContratoServicos_PercPgm, @ContratoServicos_PercCnc, @ContratoServicos_Ativo, @ContratoServicos_CodigoFiscal, @ContratoServicos_LimiteProposta, @ContratoServicos_CalculoRmn, @ContratoServicos_SolicitaGestorSistema, @Servico_Codigo, @Contrato_Codigo, @ContratoServicos_UnidadeContratada); SELECT SCOPE_IDENTITY()" ;
          Object[] prmBC000X24 ;
          prmBC000X24 = new Object[] {
          new Object[] {"@ContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Servico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Servico_Percentual",SqlDbType.Decimal,7,3} ,
          new Object[] {"@ServicoContrato_Faturamento",SqlDbType.VarChar,5,0} ,
          new Object[] {"@ContratoServicos_LocalExec",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_TipoVnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_HmlSemCnf",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoTpDias",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoResposta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAtendeGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecaoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_PrazoInicio",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoImediato",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_EspelhaAceite",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Momento",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_StatusPagFnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_QntUntCns",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_FatorCnvUndCnt",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_NaoRequerAtr",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_CodigoFiscal",SqlDbType.Char,5,0} ,
          new Object[] {"@ContratoServicos_LimiteProposta",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_CalculoRmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_SolicitaGestorSistema",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000X24 ;
          cmdBufferBC000X24=" UPDATE [ContratoServicos] SET [ContratoServicos_Alias]=@ContratoServicos_Alias, [Servico_QtdContratada]=@Servico_QtdContratada, [Servico_VlrUnidadeContratada]=@Servico_VlrUnidadeContratada, [Servico_Percentual]=@Servico_Percentual, [ServicoContrato_Faturamento]=@ServicoContrato_Faturamento, [ContratoServicos_LocalExec]=@ContratoServicos_LocalExec, [ContratoServicos_TipoVnc]=@ContratoServicos_TipoVnc, [ContratoServicos_HmlSemCnf]=@ContratoServicos_HmlSemCnf, [ContratoServicos_PrazoTpDias]=@ContratoServicos_PrazoTpDias, [ContratoServicos_PrazoAnalise]=@ContratoServicos_PrazoAnalise, [ContratoServicos_PrazoResposta]=@ContratoServicos_PrazoResposta, [ContratoServicos_PrazoGarantia]=@ContratoServicos_PrazoGarantia, [ContratoServicos_PrazoAtendeGarantia]=@ContratoServicos_PrazoAtendeGarantia, [ContratoServicos_PrazoCorrecao]=@ContratoServicos_PrazoCorrecao, [ContratoServicos_PrazoCorrecaoTipo]=@ContratoServicos_PrazoCorrecaoTipo, [ContratoServicos_PrazoInicio]=@ContratoServicos_PrazoInicio, [ContratoServicos_PrazoImediato]=@ContratoServicos_PrazoImediato, [ContratoServicos_Produtividade]=@ContratoServicos_Produtividade, [ContratoServicos_EspelhaAceite]=@ContratoServicos_EspelhaAceite, [ContratoServicos_Momento]=@ContratoServicos_Momento, [ContratoServicos_StatusPagFnc]=@ContratoServicos_StatusPagFnc, [ContratoServicos_QntUntCns]=@ContratoServicos_QntUntCns, [ContratoServicos_FatorCnvUndCnt]=@ContratoServicos_FatorCnvUndCnt, [ContratoServicos_NaoRequerAtr]=@ContratoServicos_NaoRequerAtr, [ContratoServicos_IndiceDivergencia]=@ContratoServicos_IndiceDivergencia, [ContratoServicos_TmpEstAnl]=@ContratoServicos_TmpEstAnl, [ContratoServicos_TmpEstExc]=@ContratoServicos_TmpEstExc, [ContratoServicos_TmpEstCrr]=@ContratoServicos_TmpEstCrr, [ContratoServicos_TipoHierarquia]=@ContratoServicos_TipoHierarquia, "
          + " [ContratoServicos_PercTmp]=@ContratoServicos_PercTmp, [ContratoServicos_PercPgm]=@ContratoServicos_PercPgm, [ContratoServicos_PercCnc]=@ContratoServicos_PercCnc, [ContratoServicos_Ativo]=@ContratoServicos_Ativo, [ContratoServicos_CodigoFiscal]=@ContratoServicos_CodigoFiscal, [ContratoServicos_LimiteProposta]=@ContratoServicos_LimiteProposta, [ContratoServicos_CalculoRmn]=@ContratoServicos_CalculoRmn, [ContratoServicos_SolicitaGestorSistema]=@ContratoServicos_SolicitaGestorSistema, [Servico_Codigo]=@Servico_Codigo, [Contrato_Codigo]=@Contrato_Codigo, [ContratoServicos_UnidadeContratada]=@ContratoServicos_UnidadeContratada  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo" ;
          Object[] prmBC000X25 ;
          prmBC000X25 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X27 ;
          prmBC000X27 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X28 ;
          prmBC000X28 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X30 ;
          prmBC000X30 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X32 ;
          prmBC000X32 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X33 ;
          prmBC000X33 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X34 ;
          prmBC000X34 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X35 ;
          prmBC000X35 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X36 ;
          prmBC000X36 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X37 ;
          prmBC000X37 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X38 ;
          prmBC000X38 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X39 ;
          prmBC000X39 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X40 ;
          prmBC000X40 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X41 ;
          prmBC000X41 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X42 ;
          prmBC000X42 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X43 ;
          prmBC000X43 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X44 ;
          prmBC000X44 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X45 ;
          prmBC000X45 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X46 ;
          prmBC000X46 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X47 ;
          prmBC000X47 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X48 ;
          prmBC000X48 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X49 ;
          prmBC000X49 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X50 ;
          prmBC000X50 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X51 ;
          prmBC000X51 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000X54 ;
          prmBC000X54 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC000X54 ;
          cmdBufferBC000X54=" SELECT T2.[ContratoServicosPrazo_CntSrvCod], TM1.[ContratoServicos_Codigo], T5.[Contrato_Numero], T5.[Contrato_NumeroAta], T5.[Contrato_Ano], T5.[Contrato_ValorUnidadeContratacao], T7.[Pessoa_Nome] AS Contratada_PessoaNom, T7.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T6.[Contratada_TipoFabrica], TM1.[ContratoServicos_Alias], T8.[Servico_Nome], T8.[Servico_Sigla], T8.[Servico_Tela], T8.[Servico_Ativo], T8.[Servico_Descricao], T9.[ServicoGrupo_Descricao], T8.[Servico_IsOrigemReferencia], T10.[UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, T10.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, TM1.[Servico_QtdContratada], TM1.[Servico_VlrUnidadeContratada], TM1.[Servico_Percentual], TM1.[ServicoContrato_Faturamento], TM1.[ContratoServicos_LocalExec], TM1.[ContratoServicos_TipoVnc], TM1.[ContratoServicos_HmlSemCnf], TM1.[ContratoServicos_PrazoTpDias], TM1.[ContratoServicos_PrazoAnalise], TM1.[ContratoServicos_PrazoResposta], TM1.[ContratoServicos_PrazoGarantia], TM1.[ContratoServicos_PrazoAtendeGarantia], TM1.[ContratoServicos_PrazoCorrecao], TM1.[ContratoServicos_PrazoCorrecaoTipo], TM1.[ContratoServicos_PrazoInicio], TM1.[ContratoServicos_PrazoImediato], TM1.[ContratoServicos_Produtividade], TM1.[ContratoServicos_EspelhaAceite], TM1.[ContratoServicos_Momento], TM1.[ContratoServicos_StatusPagFnc], TM1.[ContratoServicos_QntUntCns], TM1.[ContratoServicos_FatorCnvUndCnt], TM1.[ContratoServicos_NaoRequerAtr], T5.[Contrato_IndiceDivergencia], TM1.[ContratoServicos_IndiceDivergencia], TM1.[ContratoServicos_TmpEstAnl], TM1.[ContratoServicos_TmpEstExc], TM1.[ContratoServicos_TmpEstCrr], TM1.[ContratoServicos_TipoHierarquia], TM1.[ContratoServicos_PercTmp], TM1.[ContratoServicos_PercPgm], TM1.[ContratoServicos_PercCnc], TM1.[ContratoServicos_Ativo], TM1.[ContratoServicos_CodigoFiscal], "
          + " TM1.[ContratoServicos_LimiteProposta], TM1.[ContratoServicos_CalculoRmn], TM1.[ContratoServicos_SolicitaGestorSistema], TM1.[Servico_Codigo], TM1.[Contrato_Codigo], TM1.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, T8.[Servico_Vinculado], T8.[ServicoGrupo_Codigo], T5.[Contratada_Codigo], T6.[Contratada_PessoaCod] AS Contratada_PessoaCod, T5.[Contrato_AreaTrabalhoCod], COALESCE( T2.[ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( T2.[ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias, COALESCE( T3.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores, COALESCE( T4.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn FROM ((((((((([ContratoServicos] TM1 WITH (NOLOCK) LEFT JOIN [ContratoServicosPrazo] T2 WITH (NOLOCK) ON T2.[ContratoServicosPrazo_CntSrvCod] = TM1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T3 ON T3.[ContratoServicosIndicador_CntSrvCod] = TM1.[ContratoServicos_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (NOLOCK) GROUP BY [ContratoServicos_Codigo] ) T4 ON T4.[ContratoServicos_Codigo] = TM1.[ContratoServicos_Codigo]) INNER JOIN [Contrato] T5 WITH (NOLOCK) ON T5.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T5.[Contratada_Codigo]) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Contratada_PessoaCod]) INNER JOIN [Servico] T8 WITH (NOLOCK) ON T8.[Servico_Codigo] = TM1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T9 WITH (NOLOCK) ON T9.[ServicoGrupo_Codigo]"
          + " = T8.[ServicoGrupo_Codigo]) INNER JOIN [UnidadeMedicao] T10 WITH (NOLOCK) ON T10.[UnidadeMedicao_Codigo] = TM1.[ContratoServicos_UnidadeContratada]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY TM1.[ContratoServicos_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC000X55 ;
          prmBC000X55 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000X56 ;
          prmBC000X56 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000X3 ;
          prmBC000X3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000X2 ;
          prmBC000X2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000X57 ;
          prmBC000X57 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosRmn_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Valor",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmBC000X58 ;
          prmBC000X58 = new Object[] {
          new Object[] {"@ContratoServicosRmn_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosRmn_Valor",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000X59 ;
          prmBC000X59 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosRmn_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000X60 ;
          prmBC000X60 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000X2", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X2,1,0,true,false )
             ,new CursorDef("BC000X3", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X3,1,0,true,false )
             ,new CursorDef("BC000X4", "SELECT [ContratoServicos_Codigo], [ContratoServicos_Alias], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoTpDias], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_PrazoInicio], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_EspelhaAceite], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercPgm], [ContratoServicos_PercCnc], [ContratoServicos_Ativo], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema], [Servico_Codigo], [Contrato_Codigo], [ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada FROM [ContratoServicos] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X4,1,0,true,false )
             ,new CursorDef("BC000X5", "SELECT [ContratoServicos_Codigo], [ContratoServicos_Alias], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoTpDias], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_PrazoInicio], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_EspelhaAceite], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercPgm], [ContratoServicos_PercCnc], [ContratoServicos_Ativo], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema], [Servico_Codigo], [Contrato_Codigo], [ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X5,1,0,true,false )
             ,new CursorDef("BC000X7", "SELECT COALESCE( T1.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos FROM (SELECT COUNT(*) AS ContratoServicos_Prazos FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X7,1,0,true,false )
             ,new CursorDef("BC000X8", "SELECT [Servico_Nome], [Servico_Sigla], [Servico_Tela], [Servico_Ativo], [Servico_Descricao], [Servico_IsOrigemReferencia], [Servico_Vinculado], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X8,1,0,true,false )
             ,new CursorDef("BC000X9", "SELECT [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_ValorUnidadeContratacao], [Contrato_IndiceDivergencia], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X9,1,0,true,false )
             ,new CursorDef("BC000X10", "SELECT [UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X10,1,0,true,false )
             ,new CursorDef("BC000X11", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X11,1,0,true,false )
             ,new CursorDef("BC000X12", "SELECT [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X12,1,0,true,false )
             ,new CursorDef("BC000X13", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X13,1,0,true,false )
             ,new CursorDef("BC000X14", "SELECT COALESCE( [ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( [ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X14,1,0,true,false )
             ,new CursorDef("BC000X16", "SELECT COALESCE( T1.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores FROM (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T1 WHERE T1.[ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X16,1,0,true,false )
             ,new CursorDef("BC000X18", "SELECT COALESCE( T1.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn FROM (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (UPDLOCK) GROUP BY [ContratoServicos_Codigo] ) T1 WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X18,1,0,true,false )
             ,new CursorDef("BC000X21", cmdBufferBC000X21,true, GxErrorMask.GX_NOMASK, false, this,prmBC000X21,100,0,true,false )
             ,new CursorDef("BC000X22", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X22,1,0,true,false )
             ,new CursorDef("BC000X23", cmdBufferBC000X23, GxErrorMask.GX_NOMASK,prmBC000X23)
             ,new CursorDef("BC000X24", cmdBufferBC000X24, GxErrorMask.GX_NOMASK,prmBC000X24)
             ,new CursorDef("BC000X25", "DELETE FROM [ContratoServicos]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo", GxErrorMask.GX_NOMASK,prmBC000X25)
             ,new CursorDef("BC000X27", "SELECT COALESCE( T1.[ContratoServicos_Prazos], 0) AS ContratoServicos_Prazos FROM (SELECT COUNT(*) AS ContratoServicos_Prazos FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X27,1,0,true,false )
             ,new CursorDef("BC000X28", "SELECT COALESCE( [ContratoServicosPrazo_Tipo], '') AS ContratoServicos_PrazoTipo, COALESCE( [ContratoServicosPrazo_Dias], 0) AS ContratoServicos_PrazoDias FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X28,1,0,true,false )
             ,new CursorDef("BC000X30", "SELECT COALESCE( T1.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores FROM (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T1 WHERE T1.[ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X30,1,0,true,false )
             ,new CursorDef("BC000X32", "SELECT COALESCE( T1.[ContratoServicos_QtdRmn], 0) AS ContratoServicos_QtdRmn FROM (SELECT COUNT(*) AS ContratoServicos_QtdRmn, [ContratoServicos_Codigo] FROM [ContratoServicosRmn] WITH (UPDLOCK) GROUP BY [ContratoServicos_Codigo] ) T1 WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X32,1,0,true,false )
             ,new CursorDef("BC000X33", "SELECT [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_ValorUnidadeContratacao], [Contrato_IndiceDivergencia], [Contratada_Codigo], [Contrato_AreaTrabalhoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X33,1,0,true,false )
             ,new CursorDef("BC000X34", "SELECT [Contratada_TipoFabrica], [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X34,1,0,true,false )
             ,new CursorDef("BC000X35", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X35,1,0,true,false )
             ,new CursorDef("BC000X36", "SELECT [Servico_Nome], [Servico_Sigla], [Servico_Tela], [Servico_Ativo], [Servico_Descricao], [Servico_IsOrigemReferencia], [Servico_Vinculado], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X36,1,0,true,false )
             ,new CursorDef("BC000X37", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X37,1,0,true,false )
             ,new CursorDef("BC000X38", "SELECT [UnidadeMedicao_Nome] AS ContratoServicos_UndCntNome, [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X38,1,0,true,false )
             ,new CursorDef("BC000X39", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X39,1,0,true,true )
             ,new CursorDef("BC000X40", "SELECT TOP 1 [ContratoServicosCusto_Codigo] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X40,1,0,true,true )
             ,new CursorDef("BC000X41", "SELECT TOP 1 [ContratoServicosPrioridade_Codigo] FROM [ContratoServicosPrioridade] WITH (NOLOCK) WHERE [ContratoServicosPrioridade_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X41,1,0,true,true )
             ,new CursorDef("BC000X42", "SELECT TOP 1 [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X42,1,0,true,true )
             ,new CursorDef("BC000X43", "SELECT TOP 1 [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] FROM [AgendaAtendimento] WITH (NOLOCK) WHERE [AgendaAtendimento_CntSrcCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X43,1,0,true,true )
             ,new CursorDef("BC000X44", "SELECT TOP 1 [ContratoServicosTelas_ContratoCod], [ContratoServicosTelas_Sequencial] FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X44,1,0,true,true )
             ,new CursorDef("BC000X45", "SELECT TOP 1 [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_SrvVncCntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X45,1,0,true,true )
             ,new CursorDef("BC000X46", "SELECT TOP 1 [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE [ContratoSrvVnc_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X46,1,0,true,true )
             ,new CursorDef("BC000X47", "SELECT TOP 1 [ContratoServicosPrazo_CntSrvCod] FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X47,1,0,true,true )
             ,new CursorDef("BC000X48", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosUnidConversao_Codigo] FROM [ContratoServicosUnidConversao] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X48,1,0,true,true )
             ,new CursorDef("BC000X49", "SELECT TOP 1 [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X49,1,0,true,true )
             ,new CursorDef("BC000X50", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X50,1,0,true,true )
             ,new CursorDef("BC000X51", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X51,1,0,true,true )
             ,new CursorDef("BC000X54", cmdBufferBC000X54,true, GxErrorMask.GX_NOMASK, false, this,prmBC000X54,100,0,true,false )
             ,new CursorDef("BC000X55", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X55,11,0,true,false )
             ,new CursorDef("BC000X56", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X56,1,0,true,false )
             ,new CursorDef("BC000X57", "INSERT INTO [ContratoServicosRmn]([ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor]) VALUES(@ContratoServicos_Codigo, @ContratoServicosRmn_Sequencial, @ContratoServicosRmn_Inicio, @ContratoServicosRmn_Fim, @ContratoServicosRmn_Valor)", GxErrorMask.GX_NOMASK,prmBC000X57)
             ,new CursorDef("BC000X58", "UPDATE [ContratoServicosRmn] SET [ContratoServicosRmn_Inicio]=@ContratoServicosRmn_Inicio, [ContratoServicosRmn_Fim]=@ContratoServicosRmn_Fim, [ContratoServicosRmn_Valor]=@ContratoServicosRmn_Valor  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial", GxErrorMask.GX_NOMASK,prmBC000X58)
             ,new CursorDef("BC000X59", "DELETE FROM [ContratoServicosRmn]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosRmn_Sequencial] = @ContratoServicosRmn_Sequencial", GxErrorMask.GX_NOMASK,prmBC000X59)
             ,new CursorDef("BC000X60", "SELECT [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial], [ContratoServicosRmn_Inicio], [ContratoServicosRmn_Fim], [ContratoServicosRmn_Valor] FROM [ContratoServicosRmn] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo], [ContratoServicosRmn_Sequencial]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000X60,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((long[]) buf[3])[0] = rslt.getLong(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((short[]) buf[28])[0] = rslt.getShort(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((bool[]) buf[34])[0] = rslt.getBool(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((String[]) buf[36])[0] = rslt.getString(21, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((String[]) buf[38])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((decimal[]) buf[40])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((decimal[]) buf[42])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((bool[]) buf[44])[0] = rslt.getBool(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((short[]) buf[54])[0] = rslt.getShort(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((short[]) buf[56])[0] = rslt.getShort(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((short[]) buf[58])[0] = rslt.getShort(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((short[]) buf[60])[0] = rslt.getShort(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((bool[]) buf[62])[0] = rslt.getBool(34) ;
                ((String[]) buf[63])[0] = rslt.getString(35, 5) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(35);
                ((decimal[]) buf[65])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(36);
                ((short[]) buf[67])[0] = rslt.getShort(37) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(37);
                ((bool[]) buf[69])[0] = rslt.getBool(38) ;
                ((int[]) buf[70])[0] = rslt.getInt(39) ;
                ((int[]) buf[71])[0] = rslt.getInt(40) ;
                ((int[]) buf[72])[0] = rslt.getInt(41) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((long[]) buf[3])[0] = rslt.getLong(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 1) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((String[]) buf[26])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((short[]) buf[28])[0] = rslt.getShort(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((bool[]) buf[30])[0] = rslt.getBool(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((bool[]) buf[34])[0] = rslt.getBool(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((String[]) buf[36])[0] = rslt.getString(21, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((String[]) buf[38])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((decimal[]) buf[40])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((decimal[]) buf[42])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((bool[]) buf[44])[0] = rslt.getBool(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((short[]) buf[54])[0] = rslt.getShort(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((short[]) buf[56])[0] = rslt.getShort(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((short[]) buf[58])[0] = rslt.getShort(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((short[]) buf[60])[0] = rslt.getShort(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((bool[]) buf[62])[0] = rslt.getBool(34) ;
                ((String[]) buf[63])[0] = rslt.getString(35, 5) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(35);
                ((decimal[]) buf[65])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(36);
                ((short[]) buf[67])[0] = rslt.getShort(37) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(37);
                ((bool[]) buf[69])[0] = rslt.getBool(38) ;
                ((int[]) buf[70])[0] = rslt.getInt(39) ;
                ((int[]) buf[71])[0] = rslt.getInt(40) ;
                ((int[]) buf[72])[0] = rslt.getInt(41) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 1) ;
                ((String[]) buf[12])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 50) ;
                ((String[]) buf[15])[0] = rslt.getString(12, 15) ;
                ((String[]) buf[16])[0] = rslt.getString(13, 3) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(13);
                ((bool[]) buf[18])[0] = rslt.getBool(14) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((String[]) buf[21])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[22])[0] = rslt.getBool(17) ;
                ((String[]) buf[23])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(18);
                ((String[]) buf[25])[0] = rslt.getString(19, 15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(19);
                ((long[]) buf[27])[0] = rslt.getLong(20) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(20);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(21) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(22);
                ((String[]) buf[32])[0] = rslt.getVarchar(23) ;
                ((String[]) buf[33])[0] = rslt.getString(24, 1) ;
                ((String[]) buf[34])[0] = rslt.getString(25, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(25);
                ((bool[]) buf[36])[0] = rslt.getBool(26) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(26);
                ((String[]) buf[38])[0] = rslt.getString(27, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(27);
                ((short[]) buf[40])[0] = rslt.getShort(28) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(28);
                ((short[]) buf[42])[0] = rslt.getShort(29) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(29);
                ((short[]) buf[44])[0] = rslt.getShort(30) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(30);
                ((short[]) buf[46])[0] = rslt.getShort(31) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(31);
                ((short[]) buf[48])[0] = rslt.getShort(32) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(32);
                ((String[]) buf[50])[0] = rslt.getString(33, 1) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(33);
                ((short[]) buf[52])[0] = rslt.getShort(34) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(34);
                ((bool[]) buf[54])[0] = rslt.getBool(35) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(35);
                ((decimal[]) buf[56])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(36);
                ((bool[]) buf[58])[0] = rslt.getBool(37) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(37);
                ((String[]) buf[60])[0] = rslt.getString(38, 1) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(38);
                ((String[]) buf[62])[0] = rslt.getString(39, 1) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(39);
                ((decimal[]) buf[64])[0] = rslt.getDecimal(40) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(40);
                ((decimal[]) buf[66])[0] = rslt.getDecimal(41) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(41);
                ((bool[]) buf[68])[0] = rslt.getBool(42) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(42);
                ((decimal[]) buf[70])[0] = rslt.getDecimal(43) ;
                ((decimal[]) buf[71])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(44);
                ((int[]) buf[73])[0] = rslt.getInt(45) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(45);
                ((int[]) buf[75])[0] = rslt.getInt(46) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(46);
                ((int[]) buf[77])[0] = rslt.getInt(47) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(47);
                ((short[]) buf[79])[0] = rslt.getShort(48) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(48);
                ((short[]) buf[81])[0] = rslt.getShort(49) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(49);
                ((short[]) buf[83])[0] = rslt.getShort(50) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(50);
                ((short[]) buf[85])[0] = rslt.getShort(51) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(51);
                ((bool[]) buf[87])[0] = rslt.getBool(52) ;
                ((String[]) buf[88])[0] = rslt.getString(53, 5) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(53);
                ((decimal[]) buf[90])[0] = rslt.getDecimal(54) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(54);
                ((short[]) buf[92])[0] = rslt.getShort(55) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(55);
                ((bool[]) buf[94])[0] = rslt.getBool(56) ;
                ((int[]) buf[95])[0] = rslt.getInt(57) ;
                ((int[]) buf[96])[0] = rslt.getInt(58) ;
                ((int[]) buf[97])[0] = rslt.getInt(59) ;
                ((int[]) buf[98])[0] = rslt.getInt(60) ;
                ((bool[]) buf[99])[0] = rslt.wasNull(60);
                ((int[]) buf[100])[0] = rslt.getInt(61) ;
                ((int[]) buf[101])[0] = rslt.getInt(62) ;
                ((int[]) buf[102])[0] = rslt.getInt(63) ;
                ((int[]) buf[103])[0] = rslt.getInt(64) ;
                ((String[]) buf[104])[0] = rslt.getString(65, 20) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(65);
                ((short[]) buf[106])[0] = rslt.getShort(66) ;
                ((bool[]) buf[107])[0] = rslt.wasNull(66);
                ((short[]) buf[108])[0] = rslt.getShort(67) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(67);
                ((short[]) buf[110])[0] = rslt.getShort(68) ;
                ((bool[]) buf[111])[0] = rslt.wasNull(68);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 21 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 1) ;
                ((String[]) buf[12])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((String[]) buf[14])[0] = rslt.getString(11, 50) ;
                ((String[]) buf[15])[0] = rslt.getString(12, 15) ;
                ((String[]) buf[16])[0] = rslt.getString(13, 3) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(13);
                ((bool[]) buf[18])[0] = rslt.getBool(14) ;
                ((String[]) buf[19])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((String[]) buf[21])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[22])[0] = rslt.getBool(17) ;
                ((String[]) buf[23])[0] = rslt.getString(18, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(18);
                ((String[]) buf[25])[0] = rslt.getString(19, 15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(19);
                ((long[]) buf[27])[0] = rslt.getLong(20) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(20);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(21) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(22);
                ((String[]) buf[32])[0] = rslt.getVarchar(23) ;
                ((String[]) buf[33])[0] = rslt.getString(24, 1) ;
                ((String[]) buf[34])[0] = rslt.getString(25, 1) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(25);
                ((bool[]) buf[36])[0] = rslt.getBool(26) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(26);
                ((String[]) buf[38])[0] = rslt.getString(27, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(27);
                ((short[]) buf[40])[0] = rslt.getShort(28) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(28);
                ((short[]) buf[42])[0] = rslt.getShort(29) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(29);
                ((short[]) buf[44])[0] = rslt.getShort(30) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(30);
                ((short[]) buf[46])[0] = rslt.getShort(31) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(31);
                ((short[]) buf[48])[0] = rslt.getShort(32) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(32);
                ((String[]) buf[50])[0] = rslt.getString(33, 1) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(33);
                ((short[]) buf[52])[0] = rslt.getShort(34) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(34);
                ((bool[]) buf[54])[0] = rslt.getBool(35) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(35);
                ((decimal[]) buf[56])[0] = rslt.getDecimal(36) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(36);
                ((bool[]) buf[58])[0] = rslt.getBool(37) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(37);
                ((String[]) buf[60])[0] = rslt.getString(38, 1) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(38);
                ((String[]) buf[62])[0] = rslt.getString(39, 1) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(39);
                ((decimal[]) buf[64])[0] = rslt.getDecimal(40) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(40);
                ((decimal[]) buf[66])[0] = rslt.getDecimal(41) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(41);
                ((bool[]) buf[68])[0] = rslt.getBool(42) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(42);
                ((decimal[]) buf[70])[0] = rslt.getDecimal(43) ;
                ((decimal[]) buf[71])[0] = rslt.getDecimal(44) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(44);
                ((int[]) buf[73])[0] = rslt.getInt(45) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(45);
                ((int[]) buf[75])[0] = rslt.getInt(46) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(46);
                ((int[]) buf[77])[0] = rslt.getInt(47) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(47);
                ((short[]) buf[79])[0] = rslt.getShort(48) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(48);
                ((short[]) buf[81])[0] = rslt.getShort(49) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(49);
                ((short[]) buf[83])[0] = rslt.getShort(50) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(50);
                ((short[]) buf[85])[0] = rslt.getShort(51) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(51);
                ((bool[]) buf[87])[0] = rslt.getBool(52) ;
                ((String[]) buf[88])[0] = rslt.getString(53, 5) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(53);
                ((decimal[]) buf[90])[0] = rslt.getDecimal(54) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(54);
                ((short[]) buf[92])[0] = rslt.getShort(55) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(55);
                ((bool[]) buf[94])[0] = rslt.getBool(56) ;
                ((int[]) buf[95])[0] = rslt.getInt(57) ;
                ((int[]) buf[96])[0] = rslt.getInt(58) ;
                ((int[]) buf[97])[0] = rslt.getInt(59) ;
                ((int[]) buf[98])[0] = rslt.getInt(60) ;
                ((bool[]) buf[99])[0] = rslt.wasNull(60);
                ((int[]) buf[100])[0] = rslt.getInt(61) ;
                ((int[]) buf[101])[0] = rslt.getInt(62) ;
                ((int[]) buf[102])[0] = rslt.getInt(63) ;
                ((int[]) buf[103])[0] = rslt.getInt(64) ;
                ((String[]) buf[104])[0] = rslt.getString(65, 20) ;
                ((bool[]) buf[105])[0] = rslt.wasNull(65);
                ((short[]) buf[106])[0] = rslt.getShort(66) ;
                ((bool[]) buf[107])[0] = rslt.wasNull(66);
                ((short[]) buf[108])[0] = rslt.getShort(67) ;
                ((bool[]) buf[109])[0] = rslt.wasNull(67);
                ((short[]) buf[110])[0] = rslt.getShort(68) ;
                ((bool[]) buf[111])[0] = rslt.wasNull(68);
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (long)parms[3]);
                }
                stmt.SetParameter(3, (decimal)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                stmt.SetParameter(5, (String)parms[7]);
                stmt.SetParameter(6, (String)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(16, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(18, (decimal)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(24, (bool)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(29, (short)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(30, (short)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(31, (short)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(32, (short)parms[60]);
                }
                stmt.SetParameter(33, (bool)parms[61]);
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 34 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(34, (String)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 35 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(35, (decimal)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 36 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(36, (short)parms[67]);
                }
                stmt.SetParameter(37, (bool)parms[68]);
                stmt.SetParameter(38, (int)parms[69]);
                stmt.SetParameter(39, (int)parms[70]);
                stmt.SetParameter(40, (int)parms[71]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (long)parms[3]);
                }
                stmt.SetParameter(3, (decimal)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                stmt.SetParameter(5, (String)parms[7]);
                stmt.SetParameter(6, (String)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 16 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(16, (short)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(18, (decimal)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(19, (bool)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(24, (bool)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(29, (short)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(30, (short)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(31, (short)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(32, (short)parms[60]);
                }
                stmt.SetParameter(33, (bool)parms[61]);
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 34 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(34, (String)parms[63]);
                }
                if ( (bool)parms[64] )
                {
                   stmt.setNull( 35 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(35, (decimal)parms[65]);
                }
                if ( (bool)parms[66] )
                {
                   stmt.setNull( 36 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(36, (short)parms[67]);
                }
                stmt.SetParameter(37, (bool)parms[68]);
                stmt.SetParameter(38, (int)parms[69]);
                stmt.SetParameter(39, (int)parms[70]);
                stmt.SetParameter(40, (int)parms[71]);
                stmt.SetParameter(41, (int)parms[72]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 45 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                return;
             case 46 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 48 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
