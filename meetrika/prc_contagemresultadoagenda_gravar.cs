/*
               File: PRC_ContagemResultadoAgenda_Gravar
        Description: PRC_Contagem Resultado Agenda_Gravar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:6:43.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contagemresultadoagenda_gravar : GXProcedure
   {
      public prc_contagemresultadoagenda_gravar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contagemresultadoagenda_gravar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Owner ,
                           int aP1_OriContagemResultado_Codigo ,
                           int aP2_ContagemResultado_Responsavel ,
                           String aP3_ContagemResultado_Descricao ,
                           String aP4_ContagemResultado_Observacao ,
                           ref bool aP5_isOK )
      {
         this.AV8ContagemResultado_Owner = aP0_ContagemResultado_Owner;
         this.AV9OriContagemResultado_Codigo = aP1_OriContagemResultado_Codigo;
         this.AV10ContagemResultado_Responsavel = aP2_ContagemResultado_Responsavel;
         this.AV11ContagemResultado_Descricao = aP3_ContagemResultado_Descricao;
         this.AV12ContagemResultado_Observacao = aP4_ContagemResultado_Observacao;
         this.AV13isOK = aP5_isOK;
         initialize();
         executePrivate();
         aP5_isOK=this.AV13isOK;
      }

      public bool executeUdp( int aP0_ContagemResultado_Owner ,
                              int aP1_OriContagemResultado_Codigo ,
                              int aP2_ContagemResultado_Responsavel ,
                              String aP3_ContagemResultado_Descricao ,
                              String aP4_ContagemResultado_Observacao )
      {
         this.AV8ContagemResultado_Owner = aP0_ContagemResultado_Owner;
         this.AV9OriContagemResultado_Codigo = aP1_OriContagemResultado_Codigo;
         this.AV10ContagemResultado_Responsavel = aP2_ContagemResultado_Responsavel;
         this.AV11ContagemResultado_Descricao = aP3_ContagemResultado_Descricao;
         this.AV12ContagemResultado_Observacao = aP4_ContagemResultado_Observacao;
         this.AV13isOK = aP5_isOK;
         initialize();
         executePrivate();
         aP5_isOK=this.AV13isOK;
         return AV13isOK ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Owner ,
                                 int aP1_OriContagemResultado_Codigo ,
                                 int aP2_ContagemResultado_Responsavel ,
                                 String aP3_ContagemResultado_Descricao ,
                                 String aP4_ContagemResultado_Observacao ,
                                 ref bool aP5_isOK )
      {
         prc_contagemresultadoagenda_gravar objprc_contagemresultadoagenda_gravar;
         objprc_contagemresultadoagenda_gravar = new prc_contagemresultadoagenda_gravar();
         objprc_contagemresultadoagenda_gravar.AV8ContagemResultado_Owner = aP0_ContagemResultado_Owner;
         objprc_contagemresultadoagenda_gravar.AV9OriContagemResultado_Codigo = aP1_OriContagemResultado_Codigo;
         objprc_contagemresultadoagenda_gravar.AV10ContagemResultado_Responsavel = aP2_ContagemResultado_Responsavel;
         objprc_contagemresultadoagenda_gravar.AV11ContagemResultado_Descricao = aP3_ContagemResultado_Descricao;
         objprc_contagemresultadoagenda_gravar.AV12ContagemResultado_Observacao = aP4_ContagemResultado_Observacao;
         objprc_contagemresultadoagenda_gravar.AV13isOK = aP5_isOK;
         objprc_contagemresultadoagenda_gravar.context.SetSubmitInitialConfig(context);
         objprc_contagemresultadoagenda_gravar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contagemresultadoagenda_gravar);
         aP5_isOK=this.AV13isOK;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contagemresultadoagenda_gravar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13isOK = false;
         /*
            INSERT RECORD ON TABLE ContagemResultado

         */
         A1515ContagemResultado_Evento = 4;
         n1515ContagemResultado_Evento = false;
         A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         A484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         n490ContagemResultado_ContratadaCod = true;
         A508ContagemResultado_Owner = AV8ContagemResultado_Owner;
         A602ContagemResultado_OSVinculada = AV9OriContagemResultado_Codigo;
         n602ContagemResultado_OSVinculada = false;
         A890ContagemResultado_Responsavel = AV10ContagemResultado_Responsavel;
         n890ContagemResultado_Responsavel = false;
         A1046ContagemResultado_Agrupador = "Reuni�o";
         n1046ContagemResultado_Agrupador = false;
         A1583ContagemResultado_TipoRegistro = 3;
         A457ContagemResultado_Demanda = StringUtil.Trim( StringUtil.Str( (decimal)(AV9OriContagemResultado_Codigo), 6, 0));
         n457ContagemResultado_Demanda = false;
         A494ContagemResultado_Descricao = AV11ContagemResultado_Descricao;
         n494ContagemResultado_Descricao = false;
         A514ContagemResultado_Observacao = AV12ContagemResultado_Observacao;
         n514ContagemResultado_Observacao = false;
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         n1553ContagemResultado_CntSrvCod = true;
         A1636ContagemResultado_ServicoSS = 0;
         n1636ContagemResultado_ServicoSS = false;
         n1636ContagemResultado_ServicoSS = true;
         A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(DateTimeUtil.ServerNow( context, "DEFAULT"));
         n912ContagemResultado_HoraEntrega = false;
         A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
         n472ContagemResultado_DataEntrega = false;
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         /* Using cursor P00BT2 */
         pr_default.execute(0, new Object[] {A471ContagemResultado_DataDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1046ContagemResultado_Agrupador, A1046ContagemResultado_Agrupador, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1583ContagemResultado_TipoRegistro, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
         A456ContagemResultado_Codigo = P00BT2_A456ContagemResultado_Codigo[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            AV13isOK = false;
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         if ( context.Gx_err != 0 )
         {
            AV13isOK = false;
         }
         else
         {
            AV13isOK = true;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A1046ContagemResultado_Agrupador = "";
         A457ContagemResultado_Demanda = "";
         A494ContagemResultado_Descricao = "";
         A514ContagemResultado_Observacao = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         Gx_emsg = "";
         P00BT2_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contagemresultadoagenda_gravar__default(),
            new Object[][] {
                new Object[] {
               P00BT2_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1515ContagemResultado_Evento ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV8ContagemResultado_Owner ;
      private int AV9OriContagemResultado_Codigo ;
      private int AV10ContagemResultado_Responsavel ;
      private int GX_INS69 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A508ContagemResultado_Owner ;
      private int A602ContagemResultado_OSVinculada ;
      private int A890ContagemResultado_Responsavel ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1452ContagemResultado_SS ;
      private int A456ContagemResultado_Codigo ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String Gx_emsg ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool AV13isOK ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n457ContagemResultado_Demanda ;
      private bool n494ContagemResultado_Descricao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1452ContagemResultado_SS ;
      private String AV12ContagemResultado_Observacao ;
      private String A514ContagemResultado_Observacao ;
      private String AV11ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private bool aP5_isOK ;
      private IDataStoreProvider pr_default ;
      private int[] P00BT2_A456ContagemResultado_Codigo ;
   }

   public class prc_contagemresultadoagenda_gravar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BT2 ;
          prmP00BT2 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00BT2 ;
          cmdBufferP00BT2=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_DataEntrega], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_Descricao], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_Agrupador], [ContagemResultado_DataCadastro], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_CntSrvCod], [ContagemResultado_TipoRegistro], [ContagemResultado_ServicoSS], [ContagemResultado_ContadorFSCod], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_DemandaFM], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_ValorPF], [ContagemResultado_Evidencia], [ContagemResultado_LoteAceiteCod], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_LiqLogCod], [ContagemResultado_FncUsrCod], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoInicialDias], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], "
          + " [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_DataEntrega, @ContagemResultado_Demanda, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_Descricao, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_Responsavel, @ContagemResultado_HoraEntrega, @ContagemResultado_Agrupador, @ContagemResultado_DataCadastro, @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_CntSrvCod, @ContagemResultado_TipoRegistro, @ContagemResultado_ServicoSS, convert(int, 0), '', convert(int, 0), '', convert(int, 0), convert(int, 0), convert(int, 0), '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert("
          + " DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          def= new CursorDef[] {
              new CursorDef("P00BT2", cmdBufferP00BT2, GxErrorMask.GX_NOMASK,prmP00BT2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                stmt.SetParameter(9, (int)parms[15]);
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(13, (DateTime)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(15, (DateTime)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(17, (short)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[33]);
                }
                stmt.SetParameter(19, (short)parms[34]);
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[36]);
                }
                return;
       }
    }

 }

}
