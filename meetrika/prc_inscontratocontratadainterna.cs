/*
               File: PRC_InsContratoContratadaInterna
        Description: Inserir Contrato Contratada Interna
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:17:9.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_inscontratocontratadainterna : GXProcedure
   {
      public prc_inscontratocontratadainterna( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_inscontratocontratadainterna( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_Contratada_Codigo ,
                           String aP2_Contratada_PessoaNom ,
                           out int aP3_Contrato_Codigo )
      {
         this.AV12AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV11Contratada_PessoaNom = aP2_Contratada_PessoaNom;
         this.AV10Contrato_Codigo = 0 ;
         initialize();
         executePrivate();
         aP3_Contrato_Codigo=this.AV10Contrato_Codigo;
      }

      public int executeUdp( int aP0_AreaTrabalho_Codigo ,
                             int aP1_Contratada_Codigo ,
                             String aP2_Contratada_PessoaNom )
      {
         this.AV12AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV9Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV11Contratada_PessoaNom = aP2_Contratada_PessoaNom;
         this.AV10Contrato_Codigo = 0 ;
         initialize();
         executePrivate();
         aP3_Contrato_Codigo=this.AV10Contrato_Codigo;
         return AV10Contrato_Codigo ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 int aP1_Contratada_Codigo ,
                                 String aP2_Contratada_PessoaNom ,
                                 out int aP3_Contrato_Codigo )
      {
         prc_inscontratocontratadainterna objprc_inscontratocontratadainterna;
         objprc_inscontratocontratadainterna = new prc_inscontratocontratadainterna();
         objprc_inscontratocontratadainterna.AV12AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_inscontratocontratadainterna.AV9Contratada_Codigo = aP1_Contratada_Codigo;
         objprc_inscontratocontratadainterna.AV11Contratada_PessoaNom = aP2_Contratada_PessoaNom;
         objprc_inscontratocontratadainterna.AV10Contrato_Codigo = 0 ;
         objprc_inscontratocontratadainterna.context.SetSubmitInitialConfig(context);
         objprc_inscontratocontratadainterna.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_inscontratocontratadainterna);
         aP3_Contrato_Codigo=this.AV10Contrato_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_inscontratocontratadainterna)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13EmptyDate = DateTime.MinValue;
         AV8Contrato = new SdtContrato(context);
         AV8Contrato.gxTpr_Contrato_areatrabalhocod = AV12AreaTrabalho_Codigo;
         AV8Contrato.gxTpr_Contratada_codigo = AV9Contratada_Codigo;
         AV8Contrato.gxTpr_Contrato_numero = "Interno";
         AV8Contrato.gxTpr_Contrato_numeroata = "Interno";
         AV8Contrato.gxTpr_Contrato_objeto = AV11Contratada_PessoaNom;
         AV8Contrato.gxTpr_Contrato_ano = (short)(DateTimeUtil.Year( Gx_date));
         AV8Contrato.gxTpr_Contrato_unidadecontratacao = 2;
         AV8Contrato.gxTpr_Contrato_quantidade = 0;
         AV8Contrato.gxTpr_Contrato_datavigenciainicio = AV13EmptyDate;
         AV8Contrato.gxTpr_Contrato_datavigenciatermino = AV13EmptyDate;
         AV8Contrato.gxTpr_Contrato_datapublicacaodou = AV13EmptyDate;
         AV8Contrato.gxTpr_Contrato_dataassinatura = AV13EmptyDate;
         AV8Contrato.gxTpr_Contrato_datapedidoreajuste = AV13EmptyDate;
         AV8Contrato.gxTpr_Contrato_dataterminoata = AV13EmptyDate;
         AV8Contrato.gxTpr_Contrato_datafimadaptacao = AV13EmptyDate;
         AV8Contrato.gxTpr_Contrato_valor = (decimal)(0);
         AV8Contrato.gxTpr_Contrato_valorunidadecontratacao = (decimal)(0);
         AV8Contrato.gxTpr_Contrato_regraspagto = "";
         AV8Contrato.gxTpr_Contrato_calculodivergencia = "B";
         AV8Contrato.gxTpr_Contrato_indicedivergencia = (decimal)(100);
         AV8Contrato.gxTv_SdtContrato_Contrato_prepostocod_SetNull();
         AV8Contrato.gxTpr_Contrato_ativo = true;
         AV8Contrato.Save();
         if ( AV8Contrato.Success() )
         {
            context.CommitDataStores( "PRC_InsContratoContratadaInterna");
            AV10Contrato_Codigo = AV8Contrato.gxTpr_Contrato_codigo;
         }
         else
         {
            context.RollbackDataStores( "PRC_InsContratoContratadaInterna");
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13EmptyDate = DateTime.MinValue;
         AV8Contrato = new SdtContrato(context);
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_inscontratocontratadainterna__default(),
            new Object[][] {
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private int AV12AreaTrabalho_Codigo ;
      private int AV9Contratada_Codigo ;
      private int AV10Contrato_Codigo ;
      private String AV11Contratada_PessoaNom ;
      private DateTime AV13EmptyDate ;
      private DateTime Gx_date ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP3_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private SdtContrato AV8Contrato ;
   }

   public class prc_inscontratocontratadainterna__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
