/*
               File: WWTipoDocumento
        Description:  Tipo de Documentos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:44:57.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwtipodocumento : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwtipodocumento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwtipodocumento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkTipoDocumento_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_73 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_73_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_73_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17TipoDocumento_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoDocumento_Nome1", AV17TipoDocumento_Nome1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21TipoDocumento_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoDocumento_Nome2", AV21TipoDocumento_Nome2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV25TipoDocumento_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDocumento_Nome3", AV25TipoDocumento_Nome3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFTipoDocumento_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFTipoDocumento_Nome", AV34TFTipoDocumento_Nome);
               AV35TFTipoDocumento_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoDocumento_Nome_Sel", AV35TFTipoDocumento_Nome_Sel);
               AV38TFTipoDocumento_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFTipoDocumento_Ativo_Sel", StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0));
               AV36ddo_TipoDocumento_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_TipoDocumento_NomeTitleControlIdToReplace", AV36ddo_TipoDocumento_NomeTitleControlIdToReplace);
               AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace", AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace);
               AV60Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A647TipoDocumento_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADB2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDB2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117445727");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwtipodocumento.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME1", StringUtil.RTrim( AV17TipoDocumento_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME2", StringUtil.RTrim( AV21TipoDocumento_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME3", StringUtil.RTrim( AV25TipoDocumento_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODOCUMENTO_NOME", StringUtil.RTrim( AV34TFTipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODOCUMENTO_NOME_SEL", StringUtil.RTrim( AV35TFTipoDocumento_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODOCUMENTO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_73", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_73), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV40DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODOCUMENTO_NOMETITLEFILTERDATA", AV33TipoDocumento_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODOCUMENTO_NOMETITLEFILTERDATA", AV33TipoDocumento_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODOCUMENTO_ATIVOTITLEFILTERDATA", AV37TipoDocumento_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODOCUMENTO_ATIVOTITLEFILTERDATA", AV37TipoDocumento_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV60Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Caption", StringUtil.RTrim( Ddo_tipodocumento_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Tooltip", StringUtil.RTrim( Ddo_tipodocumento_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Cls", StringUtil.RTrim( Ddo_tipodocumento_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tipodocumento_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodocumento_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodocumento_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodocumento_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filtertype", StringUtil.RTrim( Ddo_tipodocumento_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalisttype", StringUtil.RTrim( Ddo_tipodocumento_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalistproc", StringUtil.RTrim( Ddo_tipodocumento_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tipodocumento_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortasc", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortdsc", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Loadingdata", StringUtil.RTrim( Ddo_tipodocumento_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tipodocumento_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tipodocumento_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tipodocumento_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Caption", StringUtil.RTrim( Ddo_tipodocumento_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_tipodocumento_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Cls", StringUtil.RTrim( Ddo_tipodocumento_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodocumento_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodocumento_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodocumento_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_tipodocumento_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodocumento_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_tipodocumento_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_tipodocumento_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_tipodocumento_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_tipodocumento_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_tipodocumento_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_tipodocumento_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_tipodocumento_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_tipodocumento_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_tipodocumento_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tipodocumento_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tipodocumento_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodocumento_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_tipodocumento_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodocumento_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDB2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDB2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwtipodocumento.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWTipoDocumento" ;
      }

      public override String GetPgmdesc( )
      {
         return " Tipo de Documentos" ;
      }

      protected void WBDB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DB2( true) ;
         }
         else
         {
            wb_table1_2_DB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(83, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(84, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodocumento_nome_Internalname, StringUtil.RTrim( AV34TFTipoDocumento_Nome), StringUtil.RTrim( context.localUtil.Format( AV34TFTipoDocumento_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodocumento_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodocumento_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipoDocumento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodocumento_nome_sel_Internalname, StringUtil.RTrim( AV35TFTipoDocumento_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV35TFTipoDocumento_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodocumento_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodocumento_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipoDocumento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodocumento_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFTipoDocumento_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodocumento_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodocumento_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWTipoDocumento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODOCUMENTO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", 0, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipoDocumento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODOCUMENTO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_73_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Internalname, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", 0, edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTipoDocumento.htm");
         }
         wbLoad = true;
      }

      protected void STARTDB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Tipo de Documentos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDB0( ) ;
      }

      protected void WSDB2( )
      {
         STARTDB2( ) ;
         EVTDB2( ) ;
      }

      protected void EVTDB2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DB2 */
                              E11DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODOCUMENTO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DB2 */
                              E12DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODOCUMENTO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DB2 */
                              E13DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14DB2 */
                              E14DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15DB2 */
                              E15DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16DB2 */
                              E16DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17DB2 */
                              E17DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18DB2 */
                              E18DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19DB2 */
                              E19DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20DB2 */
                              E20DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21DB2 */
                              E21DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22DB2 */
                              E22DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23DB2 */
                              E23DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24DB2 */
                              E24DB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_73_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
                              SubsflControlProps_732( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV58Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV59Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoDocumento_Codigo_Internalname), ",", "."));
                              A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
                              A647TipoDocumento_Ativo = StringUtil.StrToBool( cgiGet( chkTipoDocumento_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25DB2 */
                                    E25DB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26DB2 */
                                    E26DB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27DB2 */
                                    E27DB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodocumento_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME1"), AV17TipoDocumento_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodocumento_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME2"), AV21TipoDocumento_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodocumento_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME3"), AV25TipoDocumento_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodocumento_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME"), AV34TFTipoDocumento_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodocumento_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME_SEL"), AV35TFTipoDocumento_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodocumento_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPODOCUMENTO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV38TFTipoDocumento_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            GXCCtl = "TIPODOCUMENTO_ATIVO_" + sGXsfl_73_idx;
            chkTipoDocumento_Ativo.Name = GXCCtl;
            chkTipoDocumento_Ativo.WebTags = "";
            chkTipoDocumento_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTipoDocumento_Ativo_Internalname, "TitleCaption", chkTipoDocumento_Ativo.Caption);
            chkTipoDocumento_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_732( ) ;
         while ( nGXsfl_73_idx <= nRC_GXsfl_73 )
         {
            sendrow_732( ) ;
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17TipoDocumento_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV21TipoDocumento_Nome2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV25TipoDocumento_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV34TFTipoDocumento_Nome ,
                                       String AV35TFTipoDocumento_Nome_Sel ,
                                       short AV38TFTipoDocumento_Ativo_Sel ,
                                       String AV36ddo_TipoDocumento_NomeTitleControlIdToReplace ,
                                       String AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace ,
                                       String AV60Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A645TipoDocumento_Codigo ,
                                       bool A647TipoDocumento_Ativo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDB2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_NOME", StringUtil.RTrim( A646TipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_ATIVO", GetSecureSignedToken( "", A647TipoDocumento_Ativo));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_ATIVO", StringUtil.BoolToStr( A647TipoDocumento_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV60Pgmname = "WWTipoDocumento";
         context.Gx_err = 0;
      }

      protected void RFDB2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 73;
         /* Execute user event: E26DB2 */
         E26DB2 ();
         nGXsfl_73_idx = 1;
         sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
         SubsflControlProps_732( ) ;
         nGXsfl_73_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_732( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 ,
                                                 AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 ,
                                                 AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 ,
                                                 AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 ,
                                                 AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 ,
                                                 AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 ,
                                                 AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 ,
                                                 AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 ,
                                                 AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel ,
                                                 AV55WWTipoDocumentoDS_9_Tftipodocumento_nome ,
                                                 AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel ,
                                                 A646TipoDocumento_Nome ,
                                                 A647TipoDocumento_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWTipoDocumentoDS_2_Tipodocumento_nome1), 50, "%");
            lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV51WWTipoDocumentoDS_5_Tipodocumento_nome2), 50, "%");
            lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV54WWTipoDocumentoDS_8_Tipodocumento_nome3), 50, "%");
            lV55WWTipoDocumentoDS_9_Tftipodocumento_nome = StringUtil.PadR( StringUtil.RTrim( AV55WWTipoDocumentoDS_9_Tftipodocumento_nome), 50, "%");
            /* Using cursor H00DB2 */
            pr_default.execute(0, new Object[] {lV48WWTipoDocumentoDS_2_Tipodocumento_nome1, lV51WWTipoDocumentoDS_5_Tipodocumento_nome2, lV54WWTipoDocumentoDS_8_Tipodocumento_nome3, lV55WWTipoDocumentoDS_9_Tftipodocumento_nome, AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_73_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A647TipoDocumento_Ativo = H00DB2_A647TipoDocumento_Ativo[0];
               A646TipoDocumento_Nome = H00DB2_A646TipoDocumento_Nome[0];
               A645TipoDocumento_Codigo = H00DB2_A645TipoDocumento_Codigo[0];
               /* Execute user event: E27DB2 */
               E27DB2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 73;
            WBDB0( ) ;
         }
         nGXsfl_73_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = AV17TipoDocumento_Nome1;
         AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = AV21TipoDocumento_Nome2;
         AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = AV25TipoDocumento_Nome3;
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = AV34TFTipoDocumento_Nome;
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = AV35TFTipoDocumento_Nome_Sel;
         AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel = AV38TFTipoDocumento_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 ,
                                              AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 ,
                                              AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 ,
                                              AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 ,
                                              AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 ,
                                              AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 ,
                                              AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 ,
                                              AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 ,
                                              AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel ,
                                              AV55WWTipoDocumentoDS_9_Tftipodocumento_nome ,
                                              AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel ,
                                              A646TipoDocumento_Nome ,
                                              A647TipoDocumento_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV48WWTipoDocumentoDS_2_Tipodocumento_nome1), 50, "%");
         lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV51WWTipoDocumentoDS_5_Tipodocumento_nome2), 50, "%");
         lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV54WWTipoDocumentoDS_8_Tipodocumento_nome3), 50, "%");
         lV55WWTipoDocumentoDS_9_Tftipodocumento_nome = StringUtil.PadR( StringUtil.RTrim( AV55WWTipoDocumentoDS_9_Tftipodocumento_nome), 50, "%");
         /* Using cursor H00DB3 */
         pr_default.execute(1, new Object[] {lV48WWTipoDocumentoDS_2_Tipodocumento_nome1, lV51WWTipoDocumentoDS_5_Tipodocumento_nome2, lV54WWTipoDocumentoDS_8_Tipodocumento_nome3, lV55WWTipoDocumentoDS_9_Tftipodocumento_nome, AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel});
         GRID_nRecordCount = H00DB3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = AV17TipoDocumento_Nome1;
         AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = AV21TipoDocumento_Nome2;
         AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = AV25TipoDocumento_Nome3;
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = AV34TFTipoDocumento_Nome;
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = AV35TFTipoDocumento_Nome_Sel;
         AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel = AV38TFTipoDocumento_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = AV17TipoDocumento_Nome1;
         AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = AV21TipoDocumento_Nome2;
         AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = AV25TipoDocumento_Nome3;
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = AV34TFTipoDocumento_Nome;
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = AV35TFTipoDocumento_Nome_Sel;
         AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel = AV38TFTipoDocumento_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = AV17TipoDocumento_Nome1;
         AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = AV21TipoDocumento_Nome2;
         AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = AV25TipoDocumento_Nome3;
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = AV34TFTipoDocumento_Nome;
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = AV35TFTipoDocumento_Nome_Sel;
         AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel = AV38TFTipoDocumento_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = AV17TipoDocumento_Nome1;
         AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = AV21TipoDocumento_Nome2;
         AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = AV25TipoDocumento_Nome3;
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = AV34TFTipoDocumento_Nome;
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = AV35TFTipoDocumento_Nome_Sel;
         AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel = AV38TFTipoDocumento_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = AV17TipoDocumento_Nome1;
         AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = AV21TipoDocumento_Nome2;
         AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = AV25TipoDocumento_Nome3;
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = AV34TFTipoDocumento_Nome;
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = AV35TFTipoDocumento_Nome_Sel;
         AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel = AV38TFTipoDocumento_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDB0( )
      {
         /* Before Start, stand alone formulas. */
         AV60Pgmname = "WWTipoDocumento";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25DB2 */
         E25DB2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV40DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODOCUMENTO_NOMETITLEFILTERDATA"), AV33TipoDocumento_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODOCUMENTO_ATIVOTITLEFILTERDATA"), AV37TipoDocumento_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17TipoDocumento_Nome1 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoDocumento_Nome1", AV17TipoDocumento_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV21TipoDocumento_Nome2 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoDocumento_Nome2", AV21TipoDocumento_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV25TipoDocumento_Nome3 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDocumento_Nome3", AV25TipoDocumento_Nome3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV34TFTipoDocumento_Nome = StringUtil.Upper( cgiGet( edtavTftipodocumento_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFTipoDocumento_Nome", AV34TFTipoDocumento_Nome);
            AV35TFTipoDocumento_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftipodocumento_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoDocumento_Nome_Sel", AV35TFTipoDocumento_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTftipodocumento_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTftipodocumento_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFTIPODOCUMENTO_ATIVO_SEL");
               GX_FocusControl = edtavTftipodocumento_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFTipoDocumento_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFTipoDocumento_Ativo_Sel", StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0));
            }
            else
            {
               AV38TFTipoDocumento_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTftipodocumento_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFTipoDocumento_Ativo_Sel", StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0));
            }
            AV36ddo_TipoDocumento_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_TipoDocumento_NomeTitleControlIdToReplace", AV36ddo_TipoDocumento_NomeTitleControlIdToReplace);
            AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace", AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_73 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_73"), ",", "."));
            AV42GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV43GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tipodocumento_nome_Caption = cgiGet( "DDO_TIPODOCUMENTO_NOME_Caption");
            Ddo_tipodocumento_nome_Tooltip = cgiGet( "DDO_TIPODOCUMENTO_NOME_Tooltip");
            Ddo_tipodocumento_nome_Cls = cgiGet( "DDO_TIPODOCUMENTO_NOME_Cls");
            Ddo_tipodocumento_nome_Filteredtext_set = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filteredtext_set");
            Ddo_tipodocumento_nome_Selectedvalue_set = cgiGet( "DDO_TIPODOCUMENTO_NOME_Selectedvalue_set");
            Ddo_tipodocumento_nome_Dropdownoptionstype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Dropdownoptionstype");
            Ddo_tipodocumento_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODOCUMENTO_NOME_Titlecontrolidtoreplace");
            Ddo_tipodocumento_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includesortasc"));
            Ddo_tipodocumento_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includesortdsc"));
            Ddo_tipodocumento_nome_Sortedstatus = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortedstatus");
            Ddo_tipodocumento_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includefilter"));
            Ddo_tipodocumento_nome_Filtertype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filtertype");
            Ddo_tipodocumento_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Filterisrange"));
            Ddo_tipodocumento_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includedatalist"));
            Ddo_tipodocumento_nome_Datalisttype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalisttype");
            Ddo_tipodocumento_nome_Datalistproc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalistproc");
            Ddo_tipodocumento_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tipodocumento_nome_Sortasc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortasc");
            Ddo_tipodocumento_nome_Sortdsc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortdsc");
            Ddo_tipodocumento_nome_Loadingdata = cgiGet( "DDO_TIPODOCUMENTO_NOME_Loadingdata");
            Ddo_tipodocumento_nome_Cleanfilter = cgiGet( "DDO_TIPODOCUMENTO_NOME_Cleanfilter");
            Ddo_tipodocumento_nome_Noresultsfound = cgiGet( "DDO_TIPODOCUMENTO_NOME_Noresultsfound");
            Ddo_tipodocumento_nome_Searchbuttontext = cgiGet( "DDO_TIPODOCUMENTO_NOME_Searchbuttontext");
            Ddo_tipodocumento_ativo_Caption = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Caption");
            Ddo_tipodocumento_ativo_Tooltip = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Tooltip");
            Ddo_tipodocumento_ativo_Cls = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Cls");
            Ddo_tipodocumento_ativo_Selectedvalue_set = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Selectedvalue_set");
            Ddo_tipodocumento_ativo_Dropdownoptionstype = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Dropdownoptionstype");
            Ddo_tipodocumento_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Titlecontrolidtoreplace");
            Ddo_tipodocumento_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Includesortasc"));
            Ddo_tipodocumento_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Includesortdsc"));
            Ddo_tipodocumento_ativo_Sortedstatus = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Sortedstatus");
            Ddo_tipodocumento_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Includefilter"));
            Ddo_tipodocumento_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Includedatalist"));
            Ddo_tipodocumento_ativo_Datalisttype = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Datalisttype");
            Ddo_tipodocumento_ativo_Datalistfixedvalues = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Datalistfixedvalues");
            Ddo_tipodocumento_ativo_Sortasc = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Sortasc");
            Ddo_tipodocumento_ativo_Sortdsc = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Sortdsc");
            Ddo_tipodocumento_ativo_Cleanfilter = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Cleanfilter");
            Ddo_tipodocumento_ativo_Searchbuttontext = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tipodocumento_nome_Activeeventkey = cgiGet( "DDO_TIPODOCUMENTO_NOME_Activeeventkey");
            Ddo_tipodocumento_nome_Filteredtext_get = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filteredtext_get");
            Ddo_tipodocumento_nome_Selectedvalue_get = cgiGet( "DDO_TIPODOCUMENTO_NOME_Selectedvalue_get");
            Ddo_tipodocumento_ativo_Activeeventkey = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Activeeventkey");
            Ddo_tipodocumento_ativo_Selectedvalue_get = cgiGet( "DDO_TIPODOCUMENTO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME1"), AV17TipoDocumento_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME2"), AV21TipoDocumento_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME3"), AV25TipoDocumento_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME"), AV34TFTipoDocumento_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME_SEL"), AV35TFTipoDocumento_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFTIPODOCUMENTO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV38TFTipoDocumento_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25DB2 */
         E25DB2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25DB2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "TIPODOCUMENTO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "TIPODOCUMENTO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "TIPODOCUMENTO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTftipodocumento_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodocumento_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodocumento_nome_Visible), 5, 0)));
         edtavTftipodocumento_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodocumento_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodocumento_nome_sel_Visible), 5, 0)));
         edtavTftipodocumento_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodocumento_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodocumento_ativo_sel_Visible), 5, 0)));
         Ddo_tipodocumento_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoDocumento_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "TitleControlIdToReplace", Ddo_tipodocumento_nome_Titlecontrolidtoreplace);
         AV36ddo_TipoDocumento_NomeTitleControlIdToReplace = Ddo_tipodocumento_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_TipoDocumento_NomeTitleControlIdToReplace", AV36ddo_TipoDocumento_NomeTitleControlIdToReplace);
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipodocumento_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoDocumento_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_ativo_Internalname, "TitleControlIdToReplace", Ddo_tipodocumento_ativo_Titlecontrolidtoreplace);
         AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace = Ddo_tipodocumento_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace", AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace);
         edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Tipo de Documentos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV40DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV40DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26DB2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33TipoDocumento_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37TipoDocumento_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTipoDocumento_Nome_Titleformat = 2;
         edtTipoDocumento_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Title", edtTipoDocumento_Nome_Title);
         chkTipoDocumento_Ativo_Titleformat = 2;
         chkTipoDocumento_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTipoDocumento_Ativo_Internalname, "Title", chkTipoDocumento_Ativo.Title.Text);
         AV42GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42GridCurrentPage), 10, 0)));
         AV43GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = AV17TipoDocumento_Nome1;
         AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = AV21TipoDocumento_Nome2;
         AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = AV25TipoDocumento_Nome3;
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = AV34TFTipoDocumento_Nome;
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = AV35TFTipoDocumento_Nome_Sel;
         AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel = AV38TFTipoDocumento_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33TipoDocumento_NomeTitleFilterData", AV33TipoDocumento_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37TipoDocumento_AtivoTitleFilterData", AV37TipoDocumento_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11DB2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV41PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV41PageToGo) ;
         }
      }

      protected void E12DB2( )
      {
         /* Ddo_tipodocumento_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodocumento_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodocumento_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFTipoDocumento_Nome = Ddo_tipodocumento_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFTipoDocumento_Nome", AV34TFTipoDocumento_Nome);
            AV35TFTipoDocumento_Nome_Sel = Ddo_tipodocumento_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoDocumento_Nome_Sel", AV35TFTipoDocumento_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13DB2( )
      {
         /* Ddo_tipodocumento_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodocumento_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodocumento_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_ativo_Internalname, "SortedStatus", Ddo_tipodocumento_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodocumento_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_ativo_Internalname, "SortedStatus", Ddo_tipodocumento_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFTipoDocumento_Ativo_Sel = (short)(NumberUtil.Val( Ddo_tipodocumento_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFTipoDocumento_Ativo_Sel", StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27DB2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV58Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("tipodocumento.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A645TipoDocumento_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV59Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("tipodocumento.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A645TipoDocumento_Codigo);
         edtTipoDocumento_Nome_Link = formatLink("viewtipodocumento.aspx") + "?" + UrlEncode("" +A645TipoDocumento_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( A647TipoDocumento_Ativo )
         {
            AV44Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV44Color = GXUtil.RGB( 255, 0, 0);
         }
         edtTipoDocumento_Nome_Forecolor = (int)(AV44Color);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 73;
         }
         sendrow_732( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_73_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(73, GridRow);
         }
      }

      protected void E14DB2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20DB2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15DB2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21DB2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22DB2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16DB2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23DB2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17DB2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17TipoDocumento_Nome1, AV19DynamicFiltersSelector2, AV21TipoDocumento_Nome2, AV23DynamicFiltersSelector3, AV25TipoDocumento_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFTipoDocumento_Nome, AV35TFTipoDocumento_Nome_Sel, AV38TFTipoDocumento_Ativo_Sel, AV36ddo_TipoDocumento_NomeTitleControlIdToReplace, AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A645TipoDocumento_Codigo, A647TipoDocumento_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24DB2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18DB2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E19DB2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("tipodocumento.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tipodocumento_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
         Ddo_tipodocumento_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_ativo_Internalname, "SortedStatus", Ddo_tipodocumento_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_tipodocumento_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tipodocumento_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_ativo_Internalname, "SortedStatus", Ddo_tipodocumento_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavTipodocumento_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavTipodocumento_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavTipodocumento_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "TIPODOCUMENTO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21TipoDocumento_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoDocumento_Nome2", AV21TipoDocumento_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "TIPODOCUMENTO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25TipoDocumento_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDocumento_Nome3", AV25TipoDocumento_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFTipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFTipoDocumento_Nome", AV34TFTipoDocumento_Nome);
         Ddo_tipodocumento_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "FilteredText_set", Ddo_tipodocumento_nome_Filteredtext_set);
         AV35TFTipoDocumento_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoDocumento_Nome_Sel", AV35TFTipoDocumento_Nome_Sel);
         Ddo_tipodocumento_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SelectedValue_set", Ddo_tipodocumento_nome_Selectedvalue_set);
         AV38TFTipoDocumento_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFTipoDocumento_Ativo_Sel", StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0));
         Ddo_tipodocumento_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_ativo_Internalname, "SelectedValue_set", Ddo_tipodocumento_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "TIPODOCUMENTO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17TipoDocumento_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoDocumento_Nome1", AV17TipoDocumento_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV60Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV60Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV60Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV61GXV1 = 1;
         while ( AV61GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV61GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME") == 0 )
            {
               AV34TFTipoDocumento_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFTipoDocumento_Nome", AV34TFTipoDocumento_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFTipoDocumento_Nome)) )
               {
                  Ddo_tipodocumento_nome_Filteredtext_set = AV34TFTipoDocumento_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "FilteredText_set", Ddo_tipodocumento_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME_SEL") == 0 )
            {
               AV35TFTipoDocumento_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFTipoDocumento_Nome_Sel", AV35TFTipoDocumento_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFTipoDocumento_Nome_Sel)) )
               {
                  Ddo_tipodocumento_nome_Selectedvalue_set = AV35TFTipoDocumento_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SelectedValue_set", Ddo_tipodocumento_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_ATIVO_SEL") == 0 )
            {
               AV38TFTipoDocumento_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFTipoDocumento_Ativo_Sel", StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0));
               if ( ! (0==AV38TFTipoDocumento_Ativo_Sel) )
               {
                  Ddo_tipodocumento_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_ativo_Internalname, "SelectedValue_set", Ddo_tipodocumento_ativo_Selectedvalue_set);
               }
            }
            AV61GXV1 = (int)(AV61GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
            {
               AV17TipoDocumento_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17TipoDocumento_Nome1", AV17TipoDocumento_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
               {
                  AV21TipoDocumento_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TipoDocumento_Nome2", AV21TipoDocumento_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
                  {
                     AV25TipoDocumento_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TipoDocumento_Nome3", AV25TipoDocumento_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV60Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFTipoDocumento_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODOCUMENTO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFTipoDocumento_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFTipoDocumento_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODOCUMENTO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFTipoDocumento_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV38TFTipoDocumento_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODOCUMENTO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV38TFTipoDocumento_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV60Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TipoDocumento_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17TipoDocumento_Nome1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TipoDocumento_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21TipoDocumento_Nome2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TipoDocumento_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25TipoDocumento_Nome3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV60Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "TipoDocumento";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_DB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_DB2( true) ;
         }
         else
         {
            wb_table2_8_DB2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_67_DB2( true) ;
         }
         else
         {
            wb_table3_67_DB2( false) ;
         }
         return  ;
      }

      protected void wb_table3_67_DB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DB2e( true) ;
         }
         else
         {
            wb_table1_2_DB2e( false) ;
         }
      }

      protected void wb_table3_67_DB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_70_DB2( true) ;
         }
         else
         {
            wb_table4_70_DB2( false) ;
         }
         return  ;
      }

      protected void wb_table4_70_DB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_67_DB2e( true) ;
         }
         else
         {
            wb_table3_67_DB2e( false) ;
         }
      }

      protected void wb_table4_70_DB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"73\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Documento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoDocumento_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoDocumento_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoDocumento_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkTipoDocumento_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkTipoDocumento_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkTipoDocumento_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A646TipoDocumento_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoDocumento_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTipoDocumento_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A647TipoDocumento_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkTipoDocumento_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkTipoDocumento_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 73 )
         {
            wbEnd = 0;
            nRC_GXsfl_73 = (short)(nGXsfl_73_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_70_DB2e( true) ;
         }
         else
         {
            wb_table4_70_DB2e( false) ;
         }
      }

      protected void wb_table2_8_DB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTipodocumentotitle_Internalname, "Tipos de Documento", "", "", lblTipodocumentotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_DB2( true) ;
         }
         else
         {
            wb_table5_13_DB2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_DB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWTipoDocumento.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_DB2( true) ;
         }
         else
         {
            wb_table6_23_DB2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_DB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DB2e( true) ;
         }
         else
         {
            wb_table2_8_DB2e( false) ;
         }
      }

      protected void wb_table6_23_DB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_DB2( true) ;
         }
         else
         {
            wb_table7_28_DB2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_DB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_DB2e( true) ;
         }
         else
         {
            wb_table6_23_DB2e( false) ;
         }
      }

      protected void wb_table7_28_DB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWTipoDocumento.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome1_Internalname, StringUtil.RTrim( AV17TipoDocumento_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17TipoDocumento_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoDocumento.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WWTipoDocumento.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome2_Internalname, StringUtil.RTrim( AV21TipoDocumento_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21TipoDocumento_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoDocumento.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_73_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWTipoDocumento.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_73_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome3_Internalname, StringUtil.RTrim( AV25TipoDocumento_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25TipoDocumento_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_DB2e( true) ;
         }
         else
         {
            wb_table7_28_DB2e( false) ;
         }
      }

      protected void wb_table5_13_DB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTipoDocumento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_DB2e( true) ;
         }
         else
         {
            wb_table5_13_DB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADB2( ) ;
         WSDB2( ) ;
         WEDB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311745079");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwtipodocumento.js", "?2020311745079");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_idx;
         edtTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO_"+sGXsfl_73_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_73_idx;
         chkTipoDocumento_Ativo_Internalname = "TIPODOCUMENTO_ATIVO_"+sGXsfl_73_idx;
      }

      protected void SubsflControlProps_fel_732( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_73_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_73_fel_idx;
         edtTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO_"+sGXsfl_73_fel_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_73_fel_idx;
         chkTipoDocumento_Ativo_Internalname = "TIPODOCUMENTO_ATIVO_"+sGXsfl_73_fel_idx;
      }

      protected void sendrow_732( )
      {
         SubsflControlProps_732( ) ;
         WBDB0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_73_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_73_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_73_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV58Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV58Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV59Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDocumento_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Nome_Internalname,StringUtil.RTrim( A646TipoDocumento_Nome),StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTipoDocumento_Nome_Link,(String)"",(String)"",(String)"",(String)edtTipoDocumento_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtTipoDocumento_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)73,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkTipoDocumento_Ativo_Internalname,StringUtil.BoolToStr( A647TipoDocumento_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_CODIGO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_NOME"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_ATIVO"+"_"+sGXsfl_73_idx, GetSecureSignedToken( sGXsfl_73_idx, A647TipoDocumento_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_73_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_73_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_73_idx+1));
            sGXsfl_73_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_73_idx), 4, 0)), 4, "0");
            SubsflControlProps_732( ) ;
         }
         /* End function sendrow_732 */
      }

      protected void init_default_properties( )
      {
         lblTipodocumentotitle_Internalname = "TIPODOCUMENTOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavTipodocumento_nome1_Internalname = "vTIPODOCUMENTO_NOME1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavTipodocumento_nome2_Internalname = "vTIPODOCUMENTO_NOME2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavTipodocumento_nome3_Internalname = "vTIPODOCUMENTO_NOME3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO";
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME";
         chkTipoDocumento_Ativo_Internalname = "TIPODOCUMENTO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTftipodocumento_nome_Internalname = "vTFTIPODOCUMENTO_NOME";
         edtavTftipodocumento_nome_sel_Internalname = "vTFTIPODOCUMENTO_NOME_SEL";
         edtavTftipodocumento_ativo_sel_Internalname = "vTFTIPODOCUMENTO_ATIVO_SEL";
         Ddo_tipodocumento_nome_Internalname = "DDO_TIPODOCUMENTO_NOME";
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname = "vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tipodocumento_ativo_Internalname = "DDO_TIPODOCUMENTO_ATIVO";
         edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Internalname = "vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtTipoDocumento_Nome_Jsonclick = "";
         edtTipoDocumento_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavTipodocumento_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavTipodocumento_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavTipodocumento_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTipoDocumento_Nome_Link = "";
         edtTipoDocumento_Nome_Forecolor = (int)(0xFFFFFF);
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkTipoDocumento_Ativo_Titleformat = 0;
         edtTipoDocumento_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavTipodocumento_nome3_Visible = 1;
         edtavTipodocumento_nome2_Visible = 1;
         edtavTipodocumento_nome1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         chkTipoDocumento_Ativo.Title.Text = "Ativo?";
         edtTipoDocumento_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkTipoDocumento_Ativo.Caption = "";
         edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible = 1;
         edtavTftipodocumento_ativo_sel_Jsonclick = "";
         edtavTftipodocumento_ativo_sel_Visible = 1;
         edtavTftipodocumento_nome_sel_Jsonclick = "";
         edtavTftipodocumento_nome_sel_Visible = 1;
         edtavTftipodocumento_nome_Jsonclick = "";
         edtavTftipodocumento_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_tipodocumento_ativo_Searchbuttontext = "Pesquisar";
         Ddo_tipodocumento_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodocumento_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodocumento_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_tipodocumento_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_tipodocumento_ativo_Datalisttype = "FixedValues";
         Ddo_tipodocumento_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodocumento_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_tipodocumento_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_ativo_Titlecontrolidtoreplace = "";
         Ddo_tipodocumento_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodocumento_ativo_Cls = "ColumnSettings";
         Ddo_tipodocumento_ativo_Tooltip = "Op��es";
         Ddo_tipodocumento_ativo_Caption = "";
         Ddo_tipodocumento_nome_Searchbuttontext = "Pesquisar";
         Ddo_tipodocumento_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tipodocumento_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodocumento_nome_Loadingdata = "Carregando dados...";
         Ddo_tipodocumento_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodocumento_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tipodocumento_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tipodocumento_nome_Datalistproc = "GetWWTipoDocumentoFilterData";
         Ddo_tipodocumento_nome_Datalisttype = "Dynamic";
         Ddo_tipodocumento_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tipodocumento_nome_Filtertype = "Character";
         Ddo_tipodocumento_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Titlecontrolidtoreplace = "";
         Ddo_tipodocumento_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodocumento_nome_Cls = "ColumnSettings";
         Ddo_tipodocumento_nome_Tooltip = "Op��es";
         Ddo_tipodocumento_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Tipo de Documentos";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33TipoDocumento_NomeTitleFilterData',fld:'vTIPODOCUMENTO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV37TipoDocumento_AtivoTitleFilterData',fld:'vTIPODOCUMENTO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'chkTipoDocumento_Ativo_Titleformat',ctrl:'TIPODOCUMENTO_ATIVO',prop:'Titleformat'},{av:'chkTipoDocumento_Ativo.Title.Text',ctrl:'TIPODOCUMENTO_ATIVO',prop:'Title'},{av:'AV42GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV43GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TIPODOCUMENTO_NOME.ONOPTIONCLICKED","{handler:'E12DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_tipodocumento_nome_Activeeventkey',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'ActiveEventKey'},{av:'Ddo_tipodocumento_nome_Filteredtext_get',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'FilteredText_get'},{av:'Ddo_tipodocumento_nome_Selectedvalue_get',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipodocumento_ativo_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPODOCUMENTO_ATIVO.ONOPTIONCLICKED","{handler:'E13DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_tipodocumento_ativo_Activeeventkey',ctrl:'DDO_TIPODOCUMENTO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_tipodocumento_ativo_Selectedvalue_get',ctrl:'DDO_TIPODOCUMENTO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodocumento_ativo_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_ATIVO',prop:'SortedStatus'},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27DB2',iparms:[{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtTipoDocumento_Nome_Link',ctrl:'TIPODOCUMENTO_NOME',prop:'Link'},{av:'edtTipoDocumento_Nome_Forecolor',ctrl:'TIPODOCUMENTO_NOME',prop:'Forecolor'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20DB2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21DB2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22DB2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23DB2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24DB2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18DB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A647TipoDocumento_Ativo',fld:'TIPODOCUMENTO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV34TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'Ddo_tipodocumento_nome_Filteredtext_set',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'FilteredText_set'},{av:'AV35TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipodocumento_nome_Selectedvalue_set',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SelectedValue_set'},{av:'AV38TFTipoDocumento_Ativo_Sel',fld:'vTFTIPODOCUMENTO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_tipodocumento_ativo_Selectedvalue_set',ctrl:'DDO_TIPODOCUMENTO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E19DB2',iparms:[{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_tipodocumento_nome_Activeeventkey = "";
         Ddo_tipodocumento_nome_Filteredtext_get = "";
         Ddo_tipodocumento_nome_Selectedvalue_get = "";
         Ddo_tipodocumento_ativo_Activeeventkey = "";
         Ddo_tipodocumento_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17TipoDocumento_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21TipoDocumento_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25TipoDocumento_Nome3 = "";
         AV34TFTipoDocumento_Nome = "";
         AV35TFTipoDocumento_Nome_Sel = "";
         AV36ddo_TipoDocumento_NomeTitleControlIdToReplace = "";
         AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace = "";
         AV60Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV40DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33TipoDocumento_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37TipoDocumento_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_tipodocumento_nome_Filteredtext_set = "";
         Ddo_tipodocumento_nome_Selectedvalue_set = "";
         Ddo_tipodocumento_nome_Sortedstatus = "";
         Ddo_tipodocumento_ativo_Selectedvalue_set = "";
         Ddo_tipodocumento_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV58Update_GXI = "";
         AV29Delete = "";
         AV59Delete_GXI = "";
         A646TipoDocumento_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = "";
         lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = "";
         lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = "";
         lV55WWTipoDocumentoDS_9_Tftipodocumento_nome = "";
         AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 = "";
         AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 = "";
         AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 = "";
         AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 = "";
         AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 = "";
         AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 = "";
         AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel = "";
         AV55WWTipoDocumentoDS_9_Tftipodocumento_nome = "";
         H00DB2_A647TipoDocumento_Ativo = new bool[] {false} ;
         H00DB2_A646TipoDocumento_Nome = new String[] {""} ;
         H00DB2_A645TipoDocumento_Codigo = new int[1] ;
         H00DB3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTipodocumentotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwtipodocumento__default(),
            new Object[][] {
                new Object[] {
               H00DB2_A647TipoDocumento_Ativo, H00DB2_A646TipoDocumento_Nome, H00DB2_A645TipoDocumento_Codigo
               }
               , new Object[] {
               H00DB3_AGRID_nRecordCount
               }
            }
         );
         AV60Pgmname = "WWTipoDocumento";
         /* GeneXus formulas. */
         AV60Pgmname = "WWTipoDocumento";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_73 ;
      private short nGXsfl_73_idx=1 ;
      private short AV13OrderedBy ;
      private short AV38TFTipoDocumento_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_73_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel ;
      private short edtTipoDocumento_Nome_Titleformat ;
      private short chkTipoDocumento_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A645TipoDocumento_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tipodocumento_nome_Datalistupdateminimumcharacters ;
      private int edtavTftipodocumento_nome_Visible ;
      private int edtavTftipodocumento_nome_sel_Visible ;
      private int edtavTftipodocumento_ativo_sel_Visible ;
      private int edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV41PageToGo ;
      private int edtTipoDocumento_Nome_Forecolor ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavTipodocumento_nome1_Visible ;
      private int edtavTipodocumento_nome2_Visible ;
      private int edtavTipodocumento_nome3_Visible ;
      private int AV61GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV42GridCurrentPage ;
      private long AV43GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV44Color ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tipodocumento_nome_Activeeventkey ;
      private String Ddo_tipodocumento_nome_Filteredtext_get ;
      private String Ddo_tipodocumento_nome_Selectedvalue_get ;
      private String Ddo_tipodocumento_ativo_Activeeventkey ;
      private String Ddo_tipodocumento_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_73_idx="0001" ;
      private String AV17TipoDocumento_Nome1 ;
      private String AV21TipoDocumento_Nome2 ;
      private String AV25TipoDocumento_Nome3 ;
      private String AV34TFTipoDocumento_Nome ;
      private String AV35TFTipoDocumento_Nome_Sel ;
      private String AV60Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tipodocumento_nome_Caption ;
      private String Ddo_tipodocumento_nome_Tooltip ;
      private String Ddo_tipodocumento_nome_Cls ;
      private String Ddo_tipodocumento_nome_Filteredtext_set ;
      private String Ddo_tipodocumento_nome_Selectedvalue_set ;
      private String Ddo_tipodocumento_nome_Dropdownoptionstype ;
      private String Ddo_tipodocumento_nome_Titlecontrolidtoreplace ;
      private String Ddo_tipodocumento_nome_Sortedstatus ;
      private String Ddo_tipodocumento_nome_Filtertype ;
      private String Ddo_tipodocumento_nome_Datalisttype ;
      private String Ddo_tipodocumento_nome_Datalistproc ;
      private String Ddo_tipodocumento_nome_Sortasc ;
      private String Ddo_tipodocumento_nome_Sortdsc ;
      private String Ddo_tipodocumento_nome_Loadingdata ;
      private String Ddo_tipodocumento_nome_Cleanfilter ;
      private String Ddo_tipodocumento_nome_Noresultsfound ;
      private String Ddo_tipodocumento_nome_Searchbuttontext ;
      private String Ddo_tipodocumento_ativo_Caption ;
      private String Ddo_tipodocumento_ativo_Tooltip ;
      private String Ddo_tipodocumento_ativo_Cls ;
      private String Ddo_tipodocumento_ativo_Selectedvalue_set ;
      private String Ddo_tipodocumento_ativo_Dropdownoptionstype ;
      private String Ddo_tipodocumento_ativo_Titlecontrolidtoreplace ;
      private String Ddo_tipodocumento_ativo_Sortedstatus ;
      private String Ddo_tipodocumento_ativo_Datalisttype ;
      private String Ddo_tipodocumento_ativo_Datalistfixedvalues ;
      private String Ddo_tipodocumento_ativo_Sortasc ;
      private String Ddo_tipodocumento_ativo_Sortdsc ;
      private String Ddo_tipodocumento_ativo_Cleanfilter ;
      private String Ddo_tipodocumento_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTftipodocumento_nome_Internalname ;
      private String edtavTftipodocumento_nome_Jsonclick ;
      private String edtavTftipodocumento_nome_sel_Internalname ;
      private String edtavTftipodocumento_nome_sel_Jsonclick ;
      private String edtavTftipodocumento_ativo_sel_Internalname ;
      private String edtavTftipodocumento_ativo_sel_Jsonclick ;
      private String edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipodocumento_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtTipoDocumento_Codigo_Internalname ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String chkTipoDocumento_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 ;
      private String lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 ;
      private String lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 ;
      private String lV55WWTipoDocumentoDS_9_Tftipodocumento_nome ;
      private String AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 ;
      private String AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 ;
      private String AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 ;
      private String AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel ;
      private String AV55WWTipoDocumentoDS_9_Tftipodocumento_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavTipodocumento_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavTipodocumento_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavTipodocumento_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tipodocumento_nome_Internalname ;
      private String Ddo_tipodocumento_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtTipoDocumento_Nome_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtTipoDocumento_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblTipodocumentotitle_Internalname ;
      private String lblTipodocumentotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavTipodocumento_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavTipodocumento_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavTipodocumento_nome3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_73_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTipoDocumento_Codigo_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool A647TipoDocumento_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tipodocumento_nome_Includesortasc ;
      private bool Ddo_tipodocumento_nome_Includesortdsc ;
      private bool Ddo_tipodocumento_nome_Includefilter ;
      private bool Ddo_tipodocumento_nome_Filterisrange ;
      private bool Ddo_tipodocumento_nome_Includedatalist ;
      private bool Ddo_tipodocumento_ativo_Includesortasc ;
      private bool Ddo_tipodocumento_ativo_Includesortdsc ;
      private bool Ddo_tipodocumento_ativo_Includefilter ;
      private bool Ddo_tipodocumento_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 ;
      private bool AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV36ddo_TipoDocumento_NomeTitleControlIdToReplace ;
      private String AV39ddo_TipoDocumento_AtivoTitleControlIdToReplace ;
      private String AV58Update_GXI ;
      private String AV59Delete_GXI ;
      private String AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 ;
      private String AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 ;
      private String AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkTipoDocumento_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00DB2_A647TipoDocumento_Ativo ;
      private String[] H00DB2_A646TipoDocumento_Nome ;
      private int[] H00DB2_A645TipoDocumento_Codigo ;
      private long[] H00DB3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33TipoDocumento_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37TipoDocumento_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV40DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwtipodocumento__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DB2( IGxContext context ,
                                             String AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 ,
                                             String AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 ,
                                             bool AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 ,
                                             String AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 ,
                                             String AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 ,
                                             bool AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 ,
                                             String AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 ,
                                             String AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 ,
                                             String AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel ,
                                             String AV55WWTipoDocumentoDS_9_Tftipodocumento_nome ,
                                             short AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel ,
                                             String A646TipoDocumento_Nome ,
                                             bool A647TipoDocumento_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [10] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [TipoDocumento_Ativo], [TipoDocumento_Nome], [TipoDocumento_Codigo]";
         sFromString = " FROM [TipoDocumento] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWTipoDocumentoDS_2_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like '%' + @lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWTipoDocumentoDS_5_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like '%' + @lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTipoDocumentoDS_8_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like '%' + @lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTipoDocumentoDS_9_Tftipodocumento_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like @lV55WWTipoDocumentoDS_9_Tftipodocumento_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like @lV55WWTipoDocumentoDS_9_Tftipodocumento_nome)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] = @AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] = @AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Ativo] = 1)";
            }
         }
         if ( AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoDocumento_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoDocumento_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TipoDocumento_Ativo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TipoDocumento_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [TipoDocumento_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00DB3( IGxContext context ,
                                             String AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1 ,
                                             String AV48WWTipoDocumentoDS_2_Tipodocumento_nome1 ,
                                             bool AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 ,
                                             String AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2 ,
                                             String AV51WWTipoDocumentoDS_5_Tipodocumento_nome2 ,
                                             bool AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 ,
                                             String AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3 ,
                                             String AV54WWTipoDocumentoDS_8_Tipodocumento_nome3 ,
                                             String AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel ,
                                             String AV55WWTipoDocumentoDS_9_Tftipodocumento_nome ,
                                             short AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel ,
                                             String A646TipoDocumento_Nome ,
                                             bool A647TipoDocumento_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [TipoDocumento] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV47WWTipoDocumentoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWTipoDocumentoDS_2_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like '%' + @lV48WWTipoDocumentoDS_2_Tipodocumento_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( AV49WWTipoDocumentoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV50WWTipoDocumentoDS_4_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWTipoDocumentoDS_5_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like '%' + @lV51WWTipoDocumentoDS_5_Tipodocumento_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV52WWTipoDocumentoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV53WWTipoDocumentoDS_7_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWTipoDocumentoDS_8_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like '%' + @lV54WWTipoDocumentoDS_8_Tipodocumento_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTipoDocumentoDS_9_Tftipodocumento_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] like @lV55WWTipoDocumentoDS_9_Tftipodocumento_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] like @lV55WWTipoDocumentoDS_9_Tftipodocumento_nome)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Nome] = @AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Nome] = @AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Ativo] = 1)";
            }
         }
         if ( AV57WWTipoDocumentoDS_11_Tftipodocumento_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDocumento_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDocumento_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00DB2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] );
               case 1 :
                     return conditional_H00DB3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (short)dynConstraints[13] , (bool)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DB2 ;
          prmH00DB2 = new Object[] {
          new Object[] {"@lV48WWTipoDocumentoDS_2_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV51WWTipoDocumentoDS_5_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTipoDocumentoDS_8_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTipoDocumentoDS_9_Tftipodocumento_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00DB3 ;
          prmH00DB3 = new Object[] {
          new Object[] {"@lV48WWTipoDocumentoDS_2_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV51WWTipoDocumentoDS_5_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWTipoDocumentoDS_8_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWTipoDocumentoDS_9_Tftipodocumento_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV56WWTipoDocumentoDS_10_Tftipodocumento_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DB2,11,0,true,false )
             ,new CursorDef("H00DB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DB3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

}
