/*
               File: PRC_CriarAnexosCompartilhados
        Description: Criar Anexos Compartilhados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:27.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_criaranexoscompartilhados : GXProcedure
   {
      public prc_criaranexoscompartilhados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_criaranexoscompartilhados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out bool aP1_TemAnexo )
      {
         this.AV16ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12TemAnexo = false ;
         initialize();
         executePrivate();
         aP1_TemAnexo=this.AV12TemAnexo;
      }

      public bool executeUdp( int aP0_ContagemResultado_Codigo )
      {
         this.AV16ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12TemAnexo = false ;
         initialize();
         executePrivate();
         aP1_TemAnexo=this.AV12TemAnexo;
         return AV12TemAnexo ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out bool aP1_TemAnexo )
      {
         prc_criaranexoscompartilhados objprc_criaranexoscompartilhados;
         objprc_criaranexoscompartilhados = new prc_criaranexoscompartilhados();
         objprc_criaranexoscompartilhados.AV16ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_criaranexoscompartilhados.AV12TemAnexo = false ;
         objprc_criaranexoscompartilhados.context.SetSubmitInitialConfig(context);
         objprc_criaranexoscompartilhados.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_criaranexoscompartilhados);
         aP1_TemAnexo=this.AV12TemAnexo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_criaranexoscompartilhados)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10SDT_ContagemResultadoEvidencias.FromXml(AV14WebSession.Get("ArquivosEvd"), "");
         AV12TemAnexo = (bool)((AV10SDT_ContagemResultadoEvidencias.Count>0));
         if ( AV12TemAnexo )
         {
            AV20GXV1 = 1;
            while ( AV20GXV1 <= AV10SDT_ContagemResultadoEvidencias.Count )
            {
               AV9Sdt_ArquivoEvidencia = ((SdtSDT_ContagemResultadoEvidencias_Arquivo)AV10SDT_ContagemResultadoEvidencias.Item(AV20GXV1));
               /*
                  INSERT RECORD ON TABLE Anexos

               */
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link)) )
               {
                  A1101Anexo_Arquivo = AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo;
                  n1101Anexo_Arquivo = false;
                  A1107Anexo_NomeArq = AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq;
                  n1107Anexo_NomeArq = false;
                  A1108Anexo_TipoArq = AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq;
                  n1108Anexo_TipoArq = false;
               }
               else
               {
                  A1450Anexo_Link = AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link;
                  n1450Anexo_Link = false;
               }
               A1103Anexo_Data = AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data;
               A1102Anexo_UserId = AV15WWPContext.gxTpr_Userid;
               A1123Anexo_Descricao = AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao;
               n1123Anexo_Descricao = false;
               if ( AV9Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo > 0 )
               {
                  A645TipoDocumento_Codigo = AV9Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo;
                  n645TipoDocumento_Codigo = false;
               }
               else
               {
                  A645TipoDocumento_Codigo = 0;
                  n645TipoDocumento_Codigo = false;
                  n645TipoDocumento_Codigo = true;
               }
               A1495Anexo_Owner = AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner;
               n1495Anexo_Owner = false;
               /* Using cursor P00AA2 */
               A1107Anexo_NomeArq = FileUtil.GetFileName( A1101Anexo_Arquivo);
               n1107Anexo_NomeArq = false;
               A1108Anexo_TipoArq = FileUtil.GetFileType( A1101Anexo_Arquivo);
               n1108Anexo_TipoArq = false;
               pr_default.execute(0, new Object[] {n1107Anexo_NomeArq, A1107Anexo_NomeArq, n1108Anexo_TipoArq, A1108Anexo_TipoArq, n1101Anexo_Arquivo, A1101Anexo_Arquivo, A1102Anexo_UserId, A1103Anexo_Data, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, n1123Anexo_Descricao, A1123Anexo_Descricao, n1450Anexo_Link, A1450Anexo_Link, n1495Anexo_Owner, A1495Anexo_Owner});
               A1106Anexo_Codigo = P00AA2_A1106Anexo_Codigo[0];
               pr_default.close(0);
               dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
               if ( (pr_default.getStatus(0) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
               AV11Anexo_Codigo = A1106Anexo_Codigo;
               AV13Codigos_Retorno.Add(AV11Anexo_Codigo, 0);
               /* Optimized UPDATE. */
               /* Using cursor P00AA3 */
               A1107Anexo_NomeArq = FileUtil.GetFileName( A1101Anexo_Arquivo);
               n1107Anexo_NomeArq = false;
               A1108Anexo_TipoArq = FileUtil.GetFileType( A1101Anexo_Arquivo);
               n1108Anexo_TipoArq = false;
               pr_default.execute(1, new Object[] {n1107Anexo_NomeArq, AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq, n1108Anexo_TipoArq, AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq, AV11Anexo_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
               dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
               /* End optimized UPDATE. */
               if ( AV9Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo > 0 )
               {
                  /* Using cursor P00AA4 */
                  pr_default.execute(2, new Object[] {AV9Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo, AV16ContagemResultado_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A1771ContagemResultadoArtefato_ArtefatoCod = P00AA4_A1771ContagemResultadoArtefato_ArtefatoCod[0];
                     A1772ContagemResultadoArtefato_OSCod = P00AA4_A1772ContagemResultadoArtefato_OSCod[0];
                     A1773ContagemResultadoArtefato_AnxCod = P00AA4_A1773ContagemResultadoArtefato_AnxCod[0];
                     n1773ContagemResultadoArtefato_AnxCod = P00AA4_n1773ContagemResultadoArtefato_AnxCod[0];
                     A1769ContagemResultadoArtefato_Codigo = P00AA4_A1769ContagemResultadoArtefato_Codigo[0];
                     A1773ContagemResultadoArtefato_AnxCod = AV11Anexo_Codigo;
                     n1773ContagemResultadoArtefato_AnxCod = false;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     /* Using cursor P00AA5 */
                     pr_default.execute(3, new Object[] {n1773ContagemResultadoArtefato_AnxCod, A1773ContagemResultadoArtefato_AnxCod, A1769ContagemResultadoArtefato_Codigo});
                     pr_default.close(3);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
                     if (true) break;
                     /* Using cursor P00AA6 */
                     pr_default.execute(4, new Object[] {n1773ContagemResultadoArtefato_AnxCod, A1773ContagemResultadoArtefato_AnxCod, A1769ContagemResultadoArtefato_Codigo});
                     pr_default.close(4);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoArtefato") ;
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link)) )
               {
                  AV8File.Source = "PublicTempStorage\\"+AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq+"."+AV9Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq;
                  if ( AV8File.Exists() )
                  {
                     AV8File.Delete();
                  }
               }
               AV20GXV1 = (int)(AV20GXV1+1);
            }
            AV14WebSession.Set("CodigosAnexos", AV13Codigos_Retorno.ToXml(false, true, "Collection", ""));
            AV14WebSession.Remove("ArquivosEvd");
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_CriarAnexosCompartilhados");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10SDT_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         AV14WebSession = context.GetSession();
         AV9Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         A1101Anexo_Arquivo = "";
         A1107Anexo_NomeArq = "";
         A1108Anexo_TipoArq = "";
         A1450Anexo_Link = "";
         A1103Anexo_Data = (DateTime)(DateTime.MinValue);
         AV15WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A1123Anexo_Descricao = "";
         P00AA2_A1106Anexo_Codigo = new int[1] ;
         Gx_emsg = "";
         AV13Codigos_Retorno = new GxSimpleCollection();
         scmdbuf = "";
         P00AA4_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         P00AA4_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         P00AA4_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         P00AA4_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         P00AA4_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         AV8File = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_criaranexoscompartilhados__default(),
            new Object[][] {
                new Object[] {
               P00AA2_A1106Anexo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               P00AA4_A1771ContagemResultadoArtefato_ArtefatoCod, P00AA4_A1772ContagemResultadoArtefato_OSCod, P00AA4_A1773ContagemResultadoArtefato_AnxCod, P00AA4_n1773ContagemResultadoArtefato_AnxCod, P00AA4_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV16ContagemResultado_Codigo ;
      private int AV20GXV1 ;
      private int GX_INS133 ;
      private int A1102Anexo_UserId ;
      private int A645TipoDocumento_Codigo ;
      private int A1495Anexo_Owner ;
      private int A1106Anexo_Codigo ;
      private int AV11Anexo_Codigo ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private int A1769ContagemResultadoArtefato_Codigo ;
      private String A1107Anexo_NomeArq ;
      private String A1108Anexo_TipoArq ;
      private String Gx_emsg ;
      private String scmdbuf ;
      private DateTime A1103Anexo_Data ;
      private bool AV12TemAnexo ;
      private bool n1101Anexo_Arquivo ;
      private bool n1107Anexo_NomeArq ;
      private bool n1108Anexo_TipoArq ;
      private bool n1450Anexo_Link ;
      private bool n1123Anexo_Descricao ;
      private bool n645TipoDocumento_Codigo ;
      private bool n1495Anexo_Owner ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private String A1450Anexo_Link ;
      private String A1123Anexo_Descricao ;
      private String A1101Anexo_Arquivo ;
      private IGxSession AV14WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00AA2_A1106Anexo_Codigo ;
      private int[] P00AA4_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int[] P00AA4_A1772ContagemResultadoArtefato_OSCod ;
      private int[] P00AA4_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] P00AA4_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] P00AA4_A1769ContagemResultadoArtefato_Codigo ;
      private bool aP1_TemAnexo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV13Codigos_Retorno ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV10SDT_ContagemResultadoEvidencias ;
      private GxFile AV8File ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV9Sdt_ArquivoEvidencia ;
      private wwpbaseobjects.SdtWWPContext AV15WWPContext ;
   }

   public class prc_criaranexoscompartilhados__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AA2 ;
          prmP00AA2 = new Object[] {
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Anexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Anexo_UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Anexo_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Anexo_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AA3 ;
          prmP00AA3 = new Object[] {
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV11Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AA4 ;
          prmP00AA4 = new Object[] {
          new Object[] {"@AV9Sdt_A_1Artefatos_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AA5 ;
          prmP00AA5 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_AnxCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AA6 ;
          prmP00AA6 = new Object[] {
          new Object[] {"@ContagemResultadoArtefato_AnxCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoArtefato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AA2", "INSERT INTO [Anexos]([Anexo_NomeArq], [Anexo_TipoArq], [Anexo_Arquivo], [Anexo_UserId], [Anexo_Data], [TipoDocumento_Codigo], [Anexo_Descricao], [Anexo_Link], [Anexo_Owner]) VALUES(@Anexo_NomeArq, @Anexo_TipoArq, @Anexo_Arquivo, @Anexo_UserId, @Anexo_Data, @TipoDocumento_Codigo, @Anexo_Descricao, @Anexo_Link, @Anexo_Owner); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00AA2)
             ,new CursorDef("P00AA3", "UPDATE [Anexos] SET [Anexo_NomeArq]=@Anexo_NomeArq, [Anexo_TipoArq]=@Anexo_TipoArq  WHERE [Anexo_Codigo] = @AV11Anexo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AA3)
             ,new CursorDef("P00AA4", "SELECT TOP 1 [ContagemResultadoArtefato_ArtefatoCod], [ContagemResultadoArtefato_OSCod], [ContagemResultadoArtefato_AnxCod], [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (UPDLOCK) WHERE ([ContagemResultadoArtefato_ArtefatoCod] = @AV9Sdt_A_1Artefatos_codigo) AND ([ContagemResultadoArtefato_OSCod] = @AV16ContagemResultado_Codigo) ORDER BY [ContagemResultadoArtefato_ArtefatoCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AA4,1,0,true,true )
             ,new CursorDef("P00AA5", "UPDATE [ContagemResultadoArtefato] SET [ContagemResultadoArtefato_AnxCod]=@ContagemResultadoArtefato_AnxCod  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AA5)
             ,new CursorDef("P00AA6", "UPDATE [ContagemResultadoArtefato] SET [ContagemResultadoArtefato_AnxCod]=@ContagemResultadoArtefato_AnxCod  WHERE [ContagemResultadoArtefato_Codigo] = @ContagemResultadoArtefato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00AA6)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameterDatetime(5, (DateTime)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
