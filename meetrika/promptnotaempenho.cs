/*
               File: PromptNotaEmpenho
        Description: Selecione Nota Empenho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:2:48.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptnotaempenho : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptnotaempenho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptnotaempenho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutNotaEmpenho_Codigo ,
                           ref int aP1_InOutSaldoContrato_Codigo )
      {
         this.AV7InOutNotaEmpenho_Codigo = aP0_InOutNotaEmpenho_Codigo;
         this.AV8InOutSaldoContrato_Codigo = aP1_InOutSaldoContrato_Codigo;
         executePrivate();
         aP0_InOutNotaEmpenho_Codigo=this.AV7InOutNotaEmpenho_Codigo;
         aP1_InOutSaldoContrato_Codigo=this.AV8InOutSaldoContrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkNotaEmpenho_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17SaldoContrato_Codigo1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17SaldoContrato_Codigo1), 6, 0)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21SaldoContrato_Codigo2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21SaldoContrato_Codigo2), 6, 0)));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25SaldoContrato_Codigo3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25SaldoContrato_Codigo3), 6, 0)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0)));
               AV32TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0)));
               AV35TFSaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0)));
               AV36TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0)));
               AV39TFNotaEmpenho_Itentificador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNotaEmpenho_Itentificador", AV39TFNotaEmpenho_Itentificador);
               AV40TFNotaEmpenho_Itentificador_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFNotaEmpenho_Itentificador_Sel", AV40TFNotaEmpenho_Itentificador_Sel);
               AV43TFNotaEmpenho_DEmissao = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
               AV44TFNotaEmpenho_DEmissao_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
               AV49TFNotaEmpenho_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_Valor, 18, 5)));
               AV50TFNotaEmpenho_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV50TFNotaEmpenho_Valor_To, 18, 5)));
               AV53TFNotaEmpenho_Qtd = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_Qtd, 14, 5)));
               AV54TFNotaEmpenho_Qtd_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV54TFNotaEmpenho_Qtd_To, 14, 5)));
               AV57TFNotaEmpenho_SaldoAnt = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV57TFNotaEmpenho_SaldoAnt, 18, 5)));
               AV58TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5)));
               AV61TFNotaEmpenho_SaldoPos = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV61TFNotaEmpenho_SaldoPos, 18, 5)));
               AV62TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV62TFNotaEmpenho_SaldoPos_To, 18, 5)));
               AV65TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0));
               AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace", AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace);
               AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace);
               AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace", AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace);
               AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace", AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace);
               AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace", AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace);
               AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace", AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace);
               AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace", AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace);
               AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace", AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace);
               AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace", AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace);
               AV74Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutNotaEmpenho_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutNotaEmpenho_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutSaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutSaldoContrato_Codigo), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAM92( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV74Pgmname = "PromptNotaEmpenho";
               context.Gx_err = 0;
               WSM92( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEM92( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311924849");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptnotaempenho.aspx") + "?" + UrlEncode("" +AV7InOutNotaEmpenho_Codigo) + "," + UrlEncode("" +AV8InOutSaldoContrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_CODIGO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17SaldoContrato_Codigo1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_CODIGO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21SaldoContrato_Codigo2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSALDOCONTRATO_CODIGO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25SaldoContrato_Codigo3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSALDOCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_ITENTIFICADOR", StringUtil.RTrim( AV39TFNotaEmpenho_Itentificador));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_ITENTIFICADOR_SEL", StringUtil.RTrim( AV40TFNotaEmpenho_Itentificador_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_DEMISSAO", context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_DEMISSAO_TO", context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_VALOR", StringUtil.LTrim( StringUtil.NToC( AV49TFNotaEmpenho_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV50TFNotaEmpenho_Valor_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_QTD", StringUtil.LTrim( StringUtil.NToC( AV53TFNotaEmpenho_Qtd, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_QTD_TO", StringUtil.LTrim( StringUtil.NToC( AV54TFNotaEmpenho_Qtd_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_SALDOANT", StringUtil.LTrim( StringUtil.NToC( AV57TFNotaEmpenho_SaldoAnt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_SALDOANT_TO", StringUtil.LTrim( StringUtil.NToC( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_SALDOPOS", StringUtil.LTrim( StringUtil.NToC( AV61TFNotaEmpenho_SaldoPos, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_SALDOPOS_TO", StringUtil.LTrim( StringUtil.NToC( AV62TFNotaEmpenho_SaldoPos_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFNOTAEMPENHO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV67DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_CODIGOTITLEFILTERDATA", AV30NotaEmpenho_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_CODIGOTITLEFILTERDATA", AV30NotaEmpenho_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSALDOCONTRATO_CODIGOTITLEFILTERDATA", AV34SaldoContrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSALDOCONTRATO_CODIGOTITLEFILTERDATA", AV34SaldoContrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA", AV38NotaEmpenho_ItentificadorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA", AV38NotaEmpenho_ItentificadorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA", AV42NotaEmpenho_DEmissaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA", AV42NotaEmpenho_DEmissaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_VALORTITLEFILTERDATA", AV48NotaEmpenho_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_VALORTITLEFILTERDATA", AV48NotaEmpenho_ValorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_QTDTITLEFILTERDATA", AV52NotaEmpenho_QtdTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_QTDTITLEFILTERDATA", AV52NotaEmpenho_QtdTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_SALDOANTTITLEFILTERDATA", AV56NotaEmpenho_SaldoAntTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_SALDOANTTITLEFILTERDATA", AV56NotaEmpenho_SaldoAntTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA", AV60NotaEmpenho_SaldoPosTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA", AV60NotaEmpenho_SaldoPosTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTAEMPENHO_ATIVOTITLEFILTERDATA", AV64NotaEmpenho_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTAEMPENHO_ATIVOTITLEFILTERDATA", AV64NotaEmpenho_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV74Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTNOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutNotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutSaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Caption", StringUtil.RTrim( Ddo_notaempenho_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_notaempenho_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Cls", StringUtil.RTrim( Ddo_notaempenho_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_notaempenho_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_notaempenho_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_saldocontrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_saldocontrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_saldocontrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_saldocontrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_saldocontrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_saldocontrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_saldocontrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_saldocontrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_saldocontrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_saldocontrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_saldocontrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Caption", StringUtil.RTrim( Ddo_notaempenho_itentificador_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Tooltip", StringUtil.RTrim( Ddo_notaempenho_itentificador_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Cls", StringUtil.RTrim( Ddo_notaempenho_itentificador_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_itentificador_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_set", StringUtil.RTrim( Ddo_notaempenho_itentificador_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_itentificador_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_itentificador_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_itentificador_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Filtertype", StringUtil.RTrim( Ddo_notaempenho_itentificador_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_itentificador_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Datalisttype", StringUtil.RTrim( Ddo_notaempenho_itentificador_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistproc", StringUtil.RTrim( Ddo_notaempenho_itentificador_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Sortasc", StringUtil.RTrim( Ddo_notaempenho_itentificador_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_itentificador_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Loadingdata", StringUtil.RTrim( Ddo_notaempenho_itentificador_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_itentificador_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Noresultsfound", StringUtil.RTrim( Ddo_notaempenho_itentificador_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_itentificador_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Caption", StringUtil.RTrim( Ddo_notaempenho_demissao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Tooltip", StringUtil.RTrim( Ddo_notaempenho_demissao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Cls", StringUtil.RTrim( Ddo_notaempenho_demissao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_demissao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_demissao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_demissao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Filtertype", StringUtil.RTrim( Ddo_notaempenho_demissao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_demissao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Sortasc", StringUtil.RTrim( Ddo_notaempenho_demissao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_demissao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_demissao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_demissao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_demissao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_demissao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Caption", StringUtil.RTrim( Ddo_notaempenho_valor_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Tooltip", StringUtil.RTrim( Ddo_notaempenho_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Cls", StringUtil.RTrim( Ddo_notaempenho_valor_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Filtertype", StringUtil.RTrim( Ddo_notaempenho_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Sortasc", StringUtil.RTrim( Ddo_notaempenho_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Caption", StringUtil.RTrim( Ddo_notaempenho_qtd_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Tooltip", StringUtil.RTrim( Ddo_notaempenho_qtd_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Cls", StringUtil.RTrim( Ddo_notaempenho_qtd_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_qtd_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_qtd_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_qtd_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Filtertype", StringUtil.RTrim( Ddo_notaempenho_qtd_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_qtd_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Sortasc", StringUtil.RTrim( Ddo_notaempenho_qtd_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_qtd_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_qtd_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_qtd_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_qtd_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_qtd_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Caption", StringUtil.RTrim( Ddo_notaempenho_saldoant_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Tooltip", StringUtil.RTrim( Ddo_notaempenho_saldoant_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Cls", StringUtil.RTrim( Ddo_notaempenho_saldoant_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_saldoant_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_saldoant_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_saldoant_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Filtertype", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_saldoant_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Sortasc", StringUtil.RTrim( Ddo_notaempenho_saldoant_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_saldoant_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_saldoant_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_saldoant_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_saldoant_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_saldoant_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Caption", StringUtil.RTrim( Ddo_notaempenho_saldopos_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Tooltip", StringUtil.RTrim( Ddo_notaempenho_saldopos_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Cls", StringUtil.RTrim( Ddo_notaempenho_saldopos_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_set", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_set", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_saldopos_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_saldopos_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_saldopos_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Filtertype", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Filterisrange", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_saldopos_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Sortasc", StringUtil.RTrim( Ddo_notaempenho_saldopos_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_saldopos_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_saldopos_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Rangefilterfrom", StringUtil.RTrim( Ddo_notaempenho_saldopos_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Rangefilterto", StringUtil.RTrim( Ddo_notaempenho_saldopos_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_saldopos_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Caption", StringUtil.RTrim( Ddo_notaempenho_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_notaempenho_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Cls", StringUtil.RTrim( Ddo_notaempenho_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_notaempenho_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_notaempenho_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_notaempenho_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_notaempenho_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_notaempenho_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_notaempenho_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_notaempenho_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_notaempenho_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_notaempenho_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_notaempenho_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_notaempenho_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_saldocontrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_saldocontrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_itentificador_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_itentificador_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_get", StringUtil.RTrim( Ddo_notaempenho_itentificador_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_demissao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_demissao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_valor_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_qtd_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_QTD_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_qtd_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_saldoant_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_saldoant_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_saldopos_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_get", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_get", StringUtil.RTrim( Ddo_notaempenho_saldopos_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_notaempenho_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_NOTAEMPENHO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_notaempenho_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormM92( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptNotaEmpenho" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Nota Empenho" ;
      }

      protected void WBM90( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_M92( true) ;
         }
         else
         {
            wb_table1_2_M92( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(95, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFNotaEmpenho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFNotaEmpenho_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFSaldoContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsaldocontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFSaldoContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsaldocontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsaldocontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_itentificador_Internalname, StringUtil.RTrim( AV39TFNotaEmpenho_Itentificador), StringUtil.RTrim( context.localUtil.Format( AV39TFNotaEmpenho_Itentificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_itentificador_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_itentificador_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_itentificador_sel_Internalname, StringUtil.RTrim( AV40TFNotaEmpenho_Itentificador_Sel), StringUtil.RTrim( context.localUtil.Format( AV40TFNotaEmpenho_Itentificador_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_itentificador_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_itentificador_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfnotaempenho_demissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_demissao_Internalname, context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV43TFNotaEmpenho_DEmissao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_demissao_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_demissao_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            GxWebStd.gx_bitmap( context, edtavTfnotaempenho_demissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfnotaempenho_demissao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfnotaempenho_demissao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_demissao_to_Internalname, context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV44TFNotaEmpenho_DEmissao_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_demissao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_demissao_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            GxWebStd.gx_bitmap( context, edtavTfnotaempenho_demissao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfnotaempenho_demissao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_notaempenho_demissaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_notaempenho_demissaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_notaempenho_demissaoauxdate_Internalname, context.localUtil.Format(AV45DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"), context.localUtil.Format( AV45DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_notaempenho_demissaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_notaempenho_demissaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_notaempenho_demissaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_notaempenho_demissaoauxdateto_Internalname, context.localUtil.Format(AV46DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV46DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_notaempenho_demissaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_notaempenho_demissaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV49TFNotaEmpenho_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV49TFNotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV50TFNotaEmpenho_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV50TFNotaEmpenho_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_qtd_Internalname, StringUtil.LTrim( StringUtil.NToC( AV53TFNotaEmpenho_Qtd, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV53TFNotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_qtd_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_qtd_Visible, 1, 0, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_qtd_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV54TFNotaEmpenho_Qtd_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV54TFNotaEmpenho_Qtd_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_qtd_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_qtd_to_Visible, 1, 0, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldoant_Internalname, StringUtil.LTrim( StringUtil.NToC( AV57TFNotaEmpenho_SaldoAnt, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV57TFNotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldoant_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldoant_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldoant_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV58TFNotaEmpenho_SaldoAnt_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldoant_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldoant_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldopos_Internalname, StringUtil.LTrim( StringUtil.NToC( AV61TFNotaEmpenho_SaldoPos, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV61TFNotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldopos_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldopos_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_saldopos_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV62TFNotaEmpenho_SaldoPos_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV62TFNotaEmpenho_SaldoPos_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_saldopos_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_saldopos_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfnotaempenho_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfnotaempenho_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfnotaempenho_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SALDOCONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_ITENTIFICADORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_DEMISSAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_QTDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_SALDOANTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_SALDOPOSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_NOTAEMPENHO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptNotaEmpenho.htm");
         }
         wbLoad = true;
      }

      protected void STARTM92( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Nota Empenho", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPM90( ) ;
      }

      protected void WSM92( )
      {
         STARTM92( ) ;
         EVTM92( ) ;
      }

      protected void EVTM92( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11M92 */
                           E11M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12M92 */
                           E12M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13M92 */
                           E13M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_ITENTIFICADOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14M92 */
                           E14M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_DEMISSAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15M92 */
                           E15M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_VALOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16M92 */
                           E16M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_QTD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17M92 */
                           E17M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_SALDOANT.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18M92 */
                           E18M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_SALDOPOS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19M92 */
                           E19M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_NOTAEMPENHO_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20M92 */
                           E20M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21M92 */
                           E21M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22M92 */
                           E22M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23M92 */
                           E23M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24M92 */
                           E24M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25M92 */
                           E25M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26M92 */
                           E26M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27M92 */
                           E27M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28M92 */
                           E28M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E29M92 */
                           E29M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E30M92 */
                           E30M92 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV73Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNotaEmpenho_Codigo_Internalname), ",", "."));
                           A1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSaldoContrato_Codigo_Internalname), ",", "."));
                           A1564NotaEmpenho_Itentificador = cgiGet( edtNotaEmpenho_Itentificador_Internalname);
                           n1564NotaEmpenho_Itentificador = false;
                           A1565NotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( edtNotaEmpenho_DEmissao_Internalname), 0);
                           n1565NotaEmpenho_DEmissao = false;
                           A1566NotaEmpenho_Valor = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Valor_Internalname), ",", ".");
                           n1566NotaEmpenho_Valor = false;
                           A1567NotaEmpenho_Qtd = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Qtd_Internalname), ",", ".");
                           n1567NotaEmpenho_Qtd = false;
                           A1568NotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( edtNotaEmpenho_SaldoAnt_Internalname), ",", ".");
                           n1568NotaEmpenho_SaldoAnt = false;
                           A1569NotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( edtNotaEmpenho_SaldoPos_Internalname), ",", ".");
                           n1569NotaEmpenho_SaldoPos = false;
                           A1570NotaEmpenho_Ativo = StringUtil.StrToBool( cgiGet( chkNotaEmpenho_Ativo_Internalname));
                           n1570NotaEmpenho_Ativo = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E31M92 */
                                 E31M92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E32M92 */
                                 E32M92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E33M92 */
                                 E33M92 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Saldocontrato_codigo1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vSALDOCONTRATO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV17SaldoContrato_Codigo1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Saldocontrato_codigo2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vSALDOCONTRATO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV21SaldoContrato_Codigo2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Saldocontrato_codigo3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vSALDOCONTRATO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV25SaldoContrato_Codigo3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFNotaEmpenho_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFNotaEmpenho_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsaldocontrato_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFSaldoContrato_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsaldocontrato_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFSaldoContrato_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_itentificador Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNOTAEMPENHO_ITENTIFICADOR"), AV39TFNotaEmpenho_Itentificador) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_itentificador_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNOTAEMPENHO_ITENTIFICADOR_SEL"), AV40TFNotaEmpenho_Itentificador_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_demissao Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFNOTAEMPENHO_DEMISSAO"), 0) != AV43TFNotaEmpenho_DEmissao )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_demissao_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFNOTAEMPENHO_DEMISSAO_TO"), 0) != AV44TFNotaEmpenho_DEmissao_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_valor Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_VALOR"), ",", ".") != AV49TFNotaEmpenho_Valor )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_valor_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_VALOR_TO"), ",", ".") != AV50TFNotaEmpenho_Valor_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_qtd Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_QTD"), ",", ".") != AV53TFNotaEmpenho_Qtd )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_qtd_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_QTD_TO"), ",", ".") != AV54TFNotaEmpenho_Qtd_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_saldoant Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOANT"), ",", ".") != AV57TFNotaEmpenho_SaldoAnt )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_saldoant_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOANT_TO"), ",", ".") != AV58TFNotaEmpenho_SaldoAnt_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_saldopos Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOPOS"), ",", ".") != AV61TFNotaEmpenho_SaldoPos )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_saldopos_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOPOS_TO"), ",", ".") != AV62TFNotaEmpenho_SaldoPos_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfnotaempenho_ativo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV65TFNotaEmpenho_Ativo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E34M92 */
                                       E34M92 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEM92( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormM92( ) ;
            }
         }
      }

      protected void PAM92( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SALDOCONTRATO_CODIGO", "Saldo Contrato", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SALDOCONTRATO_CODIGO", "Saldo Contrato", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SALDOCONTRATO_CODIGO", "Saldo Contrato", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "NOTAEMPENHO_ATIVO_" + sGXsfl_80_idx;
            chkNotaEmpenho_Ativo.Name = GXCCtl;
            chkNotaEmpenho_Ativo.WebTags = "";
            chkNotaEmpenho_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNotaEmpenho_Ativo_Internalname, "TitleCaption", chkNotaEmpenho_Ativo.Caption);
            chkNotaEmpenho_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       int AV17SaldoContrato_Codigo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       int AV21SaldoContrato_Codigo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       int AV25SaldoContrato_Codigo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFNotaEmpenho_Codigo ,
                                       int AV32TFNotaEmpenho_Codigo_To ,
                                       int AV35TFSaldoContrato_Codigo ,
                                       int AV36TFSaldoContrato_Codigo_To ,
                                       String AV39TFNotaEmpenho_Itentificador ,
                                       String AV40TFNotaEmpenho_Itentificador_Sel ,
                                       DateTime AV43TFNotaEmpenho_DEmissao ,
                                       DateTime AV44TFNotaEmpenho_DEmissao_To ,
                                       decimal AV49TFNotaEmpenho_Valor ,
                                       decimal AV50TFNotaEmpenho_Valor_To ,
                                       decimal AV53TFNotaEmpenho_Qtd ,
                                       decimal AV54TFNotaEmpenho_Qtd_To ,
                                       decimal AV57TFNotaEmpenho_SaldoAnt ,
                                       decimal AV58TFNotaEmpenho_SaldoAnt_To ,
                                       decimal AV61TFNotaEmpenho_SaldoPos ,
                                       decimal AV62TFNotaEmpenho_SaldoPos_To ,
                                       short AV65TFNotaEmpenho_Ativo_Sel ,
                                       String AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace ,
                                       String AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace ,
                                       String AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace ,
                                       String AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace ,
                                       String AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace ,
                                       String AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace ,
                                       String AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace ,
                                       String AV74Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFM92( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_ITENTIFICADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, ""))));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_ITENTIFICADOR", StringUtil.RTrim( A1564NotaEmpenho_Itentificador));
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_DEMISSAO", GetSecureSignedToken( "", context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_DEMISSAO", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_VALOR", StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_QTD", GetSecureSignedToken( "", context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_QTD", StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_SALDOANT", GetSecureSignedToken( "", context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_SALDOANT", StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_SALDOPOS", GetSecureSignedToken( "", context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_SALDOPOS", StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_ATIVO", GetSecureSignedToken( "", A1570NotaEmpenho_Ativo));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_ATIVO", StringUtil.BoolToStr( A1570NotaEmpenho_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFM92( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV74Pgmname = "PromptNotaEmpenho";
         context.Gx_err = 0;
      }

      protected void RFM92( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E32M92 */
         E32M92 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17SaldoContrato_Codigo1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21SaldoContrato_Codigo2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25SaldoContrato_Codigo3 ,
                                                 AV31TFNotaEmpenho_Codigo ,
                                                 AV32TFNotaEmpenho_Codigo_To ,
                                                 AV35TFSaldoContrato_Codigo ,
                                                 AV36TFSaldoContrato_Codigo_To ,
                                                 AV40TFNotaEmpenho_Itentificador_Sel ,
                                                 AV39TFNotaEmpenho_Itentificador ,
                                                 AV43TFNotaEmpenho_DEmissao ,
                                                 AV44TFNotaEmpenho_DEmissao_To ,
                                                 AV49TFNotaEmpenho_Valor ,
                                                 AV50TFNotaEmpenho_Valor_To ,
                                                 AV53TFNotaEmpenho_Qtd ,
                                                 AV54TFNotaEmpenho_Qtd_To ,
                                                 AV57TFNotaEmpenho_SaldoAnt ,
                                                 AV58TFNotaEmpenho_SaldoAnt_To ,
                                                 AV61TFNotaEmpenho_SaldoPos ,
                                                 AV62TFNotaEmpenho_SaldoPos_To ,
                                                 AV65TFNotaEmpenho_Ativo_Sel ,
                                                 A1561SaldoContrato_Codigo ,
                                                 A1560NotaEmpenho_Codigo ,
                                                 A1564NotaEmpenho_Itentificador ,
                                                 A1565NotaEmpenho_DEmissao ,
                                                 A1566NotaEmpenho_Valor ,
                                                 A1567NotaEmpenho_Qtd ,
                                                 A1568NotaEmpenho_SaldoAnt ,
                                                 A1569NotaEmpenho_SaldoPos ,
                                                 A1570NotaEmpenho_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                                 TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV39TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV39TFNotaEmpenho_Itentificador), 15, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNotaEmpenho_Itentificador", AV39TFNotaEmpenho_Itentificador);
            /* Using cursor H00M92 */
            pr_default.execute(0, new Object[] {AV17SaldoContrato_Codigo1, AV17SaldoContrato_Codigo1, AV17SaldoContrato_Codigo1, AV21SaldoContrato_Codigo2, AV21SaldoContrato_Codigo2, AV21SaldoContrato_Codigo2, AV25SaldoContrato_Codigo3, AV25SaldoContrato_Codigo3, AV25SaldoContrato_Codigo3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, lV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1570NotaEmpenho_Ativo = H00M92_A1570NotaEmpenho_Ativo[0];
               n1570NotaEmpenho_Ativo = H00M92_n1570NotaEmpenho_Ativo[0];
               A1569NotaEmpenho_SaldoPos = H00M92_A1569NotaEmpenho_SaldoPos[0];
               n1569NotaEmpenho_SaldoPos = H00M92_n1569NotaEmpenho_SaldoPos[0];
               A1568NotaEmpenho_SaldoAnt = H00M92_A1568NotaEmpenho_SaldoAnt[0];
               n1568NotaEmpenho_SaldoAnt = H00M92_n1568NotaEmpenho_SaldoAnt[0];
               A1567NotaEmpenho_Qtd = H00M92_A1567NotaEmpenho_Qtd[0];
               n1567NotaEmpenho_Qtd = H00M92_n1567NotaEmpenho_Qtd[0];
               A1566NotaEmpenho_Valor = H00M92_A1566NotaEmpenho_Valor[0];
               n1566NotaEmpenho_Valor = H00M92_n1566NotaEmpenho_Valor[0];
               A1565NotaEmpenho_DEmissao = H00M92_A1565NotaEmpenho_DEmissao[0];
               n1565NotaEmpenho_DEmissao = H00M92_n1565NotaEmpenho_DEmissao[0];
               A1564NotaEmpenho_Itentificador = H00M92_A1564NotaEmpenho_Itentificador[0];
               n1564NotaEmpenho_Itentificador = H00M92_n1564NotaEmpenho_Itentificador[0];
               A1561SaldoContrato_Codigo = H00M92_A1561SaldoContrato_Codigo[0];
               A1560NotaEmpenho_Codigo = H00M92_A1560NotaEmpenho_Codigo[0];
               /* Execute user event: E33M92 */
               E33M92 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBM90( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17SaldoContrato_Codigo1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21SaldoContrato_Codigo2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25SaldoContrato_Codigo3 ,
                                              AV31TFNotaEmpenho_Codigo ,
                                              AV32TFNotaEmpenho_Codigo_To ,
                                              AV35TFSaldoContrato_Codigo ,
                                              AV36TFSaldoContrato_Codigo_To ,
                                              AV40TFNotaEmpenho_Itentificador_Sel ,
                                              AV39TFNotaEmpenho_Itentificador ,
                                              AV43TFNotaEmpenho_DEmissao ,
                                              AV44TFNotaEmpenho_DEmissao_To ,
                                              AV49TFNotaEmpenho_Valor ,
                                              AV50TFNotaEmpenho_Valor_To ,
                                              AV53TFNotaEmpenho_Qtd ,
                                              AV54TFNotaEmpenho_Qtd_To ,
                                              AV57TFNotaEmpenho_SaldoAnt ,
                                              AV58TFNotaEmpenho_SaldoAnt_To ,
                                              AV61TFNotaEmpenho_SaldoPos ,
                                              AV62TFNotaEmpenho_SaldoPos_To ,
                                              AV65TFNotaEmpenho_Ativo_Sel ,
                                              A1561SaldoContrato_Codigo ,
                                              A1560NotaEmpenho_Codigo ,
                                              A1564NotaEmpenho_Itentificador ,
                                              A1565NotaEmpenho_DEmissao ,
                                              A1566NotaEmpenho_Valor ,
                                              A1567NotaEmpenho_Qtd ,
                                              A1568NotaEmpenho_SaldoAnt ,
                                              A1569NotaEmpenho_SaldoPos ,
                                              A1570NotaEmpenho_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV39TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV39TFNotaEmpenho_Itentificador), 15, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNotaEmpenho_Itentificador", AV39TFNotaEmpenho_Itentificador);
         /* Using cursor H00M93 */
         pr_default.execute(1, new Object[] {AV17SaldoContrato_Codigo1, AV17SaldoContrato_Codigo1, AV17SaldoContrato_Codigo1, AV21SaldoContrato_Codigo2, AV21SaldoContrato_Codigo2, AV21SaldoContrato_Codigo2, AV25SaldoContrato_Codigo3, AV25SaldoContrato_Codigo3, AV25SaldoContrato_Codigo3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, lV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To});
         GRID_nRecordCount = H00M93_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPM90( )
      {
         /* Before Start, stand alone formulas. */
         AV74Pgmname = "PromptNotaEmpenho";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E31M92 */
         E31M92 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV67DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_CODIGOTITLEFILTERDATA"), AV30NotaEmpenho_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSALDOCONTRATO_CODIGOTITLEFILTERDATA"), AV34SaldoContrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA"), AV38NotaEmpenho_ItentificadorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA"), AV42NotaEmpenho_DEmissaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_VALORTITLEFILTERDATA"), AV48NotaEmpenho_ValorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_QTDTITLEFILTERDATA"), AV52NotaEmpenho_QtdTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_SALDOANTTITLEFILTERDATA"), AV56NotaEmpenho_SaldoAntTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA"), AV60NotaEmpenho_SaldoPosTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTAEMPENHO_ATIVOTITLEFILTERDATA"), AV64NotaEmpenho_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo1_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO_CODIGO1");
               GX_FocusControl = edtavSaldocontrato_codigo1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17SaldoContrato_Codigo1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17SaldoContrato_Codigo1), 6, 0)));
            }
            else
            {
               AV17SaldoContrato_Codigo1 = (int)(context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17SaldoContrato_Codigo1), 6, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo2_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO_CODIGO2");
               GX_FocusControl = edtavSaldocontrato_codigo2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21SaldoContrato_Codigo2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21SaldoContrato_Codigo2), 6, 0)));
            }
            else
            {
               AV21SaldoContrato_Codigo2 = (int)(context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21SaldoContrato_Codigo2), 6, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo3_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO_CODIGO3");
               GX_FocusControl = edtavSaldocontrato_codigo3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25SaldoContrato_Codigo3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25SaldoContrato_Codigo3), 6, 0)));
            }
            else
            {
               AV25SaldoContrato_Codigo3 = (int)(context.localUtil.CToN( cgiGet( edtavSaldocontrato_codigo3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25SaldoContrato_Codigo3), 6, 0)));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_CODIGO");
               GX_FocusControl = edtavTfnotaempenho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFNotaEmpenho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0)));
            }
            else
            {
               AV31TFNotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_CODIGO_TO");
               GX_FocusControl = edtavTfnotaempenho_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFNotaEmpenho_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFNotaEmpenho_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfnotaempenho_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CODIGO");
               GX_FocusControl = edtavTfsaldocontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFSaldoContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0)));
            }
            else
            {
               AV35TFSaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSALDOCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfsaldocontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFSaldoContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFSaldoContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsaldocontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0)));
            }
            AV39TFNotaEmpenho_Itentificador = cgiGet( edtavTfnotaempenho_itentificador_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNotaEmpenho_Itentificador", AV39TFNotaEmpenho_Itentificador);
            AV40TFNotaEmpenho_Itentificador_Sel = cgiGet( edtavTfnotaempenho_itentificador_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFNotaEmpenho_Itentificador_Sel", AV40TFNotaEmpenho_Itentificador_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfnotaempenho_demissao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFNota Empenho_DEmissao"}), 1, "vTFNOTAEMPENHO_DEMISSAO");
               GX_FocusControl = edtavTfnotaempenho_demissao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV43TFNotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( edtavTfnotaempenho_demissao_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfnotaempenho_demissao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFNota Empenho_DEmissao_To"}), 1, "vTFNOTAEMPENHO_DEMISSAO_TO");
               GX_FocusControl = edtavTfnotaempenho_demissao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV44TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( cgiGet( edtavTfnotaempenho_demissao_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_notaempenho_demissaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Nota Empenho_DEmissao Aux Date"}), 1, "vDDO_NOTAEMPENHO_DEMISSAOAUXDATE");
               GX_FocusControl = edtavDdo_notaempenho_demissaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45DDO_NotaEmpenho_DEmissaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45DDO_NotaEmpenho_DEmissaoAuxDate", context.localUtil.Format(AV45DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"));
            }
            else
            {
               AV45DDO_NotaEmpenho_DEmissaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_notaempenho_demissaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45DDO_NotaEmpenho_DEmissaoAuxDate", context.localUtil.Format(AV45DDO_NotaEmpenho_DEmissaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_notaempenho_demissaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Nota Empenho_DEmissao Aux Date To"}), 1, "vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO");
               GX_FocusControl = edtavDdo_notaempenho_demissaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46DDO_NotaEmpenho_DEmissaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46DDO_NotaEmpenho_DEmissaoAuxDateTo", context.localUtil.Format(AV46DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV46DDO_NotaEmpenho_DEmissaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_notaempenho_demissaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46DDO_NotaEmpenho_DEmissaoAuxDateTo", context.localUtil.Format(AV46DDO_NotaEmpenho_DEmissaoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_VALOR");
               GX_FocusControl = edtavTfnotaempenho_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFNotaEmpenho_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_Valor, 18, 5)));
            }
            else
            {
               AV49TFNotaEmpenho_Valor = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_VALOR_TO");
               GX_FocusControl = edtavTfnotaempenho_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFNotaEmpenho_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV50TFNotaEmpenho_Valor_To, 18, 5)));
            }
            else
            {
               AV50TFNotaEmpenho_Valor_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV50TFNotaEmpenho_Valor_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_QTD");
               GX_FocusControl = edtavTfnotaempenho_qtd_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFNotaEmpenho_Qtd = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_Qtd, 14, 5)));
            }
            else
            {
               AV53TFNotaEmpenho_Qtd = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_Qtd, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_QTD_TO");
               GX_FocusControl = edtavTfnotaempenho_qtd_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFNotaEmpenho_Qtd_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV54TFNotaEmpenho_Qtd_To, 14, 5)));
            }
            else
            {
               AV54TFNotaEmpenho_Qtd_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_qtd_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV54TFNotaEmpenho_Qtd_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOANT");
               GX_FocusControl = edtavTfnotaempenho_saldoant_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57TFNotaEmpenho_SaldoAnt = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV57TFNotaEmpenho_SaldoAnt, 18, 5)));
            }
            else
            {
               AV57TFNotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV57TFNotaEmpenho_SaldoAnt, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOANT_TO");
               GX_FocusControl = edtavTfnotaempenho_saldoant_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFNotaEmpenho_SaldoAnt_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5)));
            }
            else
            {
               AV58TFNotaEmpenho_SaldoAnt_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldoant_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOPOS");
               GX_FocusControl = edtavTfnotaempenho_saldopos_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFNotaEmpenho_SaldoPos = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV61TFNotaEmpenho_SaldoPos, 18, 5)));
            }
            else
            {
               AV61TFNotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV61TFNotaEmpenho_SaldoPos, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_SALDOPOS_TO");
               GX_FocusControl = edtavTfnotaempenho_saldopos_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFNotaEmpenho_SaldoPos_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV62TFNotaEmpenho_SaldoPos_To, 18, 5)));
            }
            else
            {
               AV62TFNotaEmpenho_SaldoPos_To = context.localUtil.CToN( cgiGet( edtavTfnotaempenho_saldopos_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV62TFNotaEmpenho_SaldoPos_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfnotaempenho_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFNOTAEMPENHO_ATIVO_SEL");
               GX_FocusControl = edtavTfnotaempenho_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFNotaEmpenho_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0));
            }
            else
            {
               AV65TFNotaEmpenho_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfnotaempenho_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0));
            }
            AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace", AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace);
            AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace);
            AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace", AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace);
            AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace", AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace);
            AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace", AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace);
            AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace", AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace);
            AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace", AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace);
            AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace", AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace);
            AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace", AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV69GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV70GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_notaempenho_codigo_Caption = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Caption");
            Ddo_notaempenho_codigo_Tooltip = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Tooltip");
            Ddo_notaempenho_codigo_Cls = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Cls");
            Ddo_notaempenho_codigo_Filteredtext_set = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Filteredtext_set");
            Ddo_notaempenho_codigo_Filteredtextto_set = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Filteredtextto_set");
            Ddo_notaempenho_codigo_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Dropdownoptionstype");
            Ddo_notaempenho_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Titlecontrolidtoreplace");
            Ddo_notaempenho_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_CODIGO_Includesortasc"));
            Ddo_notaempenho_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_CODIGO_Includesortdsc"));
            Ddo_notaempenho_codigo_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Sortedstatus");
            Ddo_notaempenho_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_CODIGO_Includefilter"));
            Ddo_notaempenho_codigo_Filtertype = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Filtertype");
            Ddo_notaempenho_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_CODIGO_Filterisrange"));
            Ddo_notaempenho_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_CODIGO_Includedatalist"));
            Ddo_notaempenho_codigo_Sortasc = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Sortasc");
            Ddo_notaempenho_codigo_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Sortdsc");
            Ddo_notaempenho_codigo_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Cleanfilter");
            Ddo_notaempenho_codigo_Rangefilterfrom = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Rangefilterfrom");
            Ddo_notaempenho_codigo_Rangefilterto = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Rangefilterto");
            Ddo_notaempenho_codigo_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Searchbuttontext");
            Ddo_saldocontrato_codigo_Caption = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Caption");
            Ddo_saldocontrato_codigo_Tooltip = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Tooltip");
            Ddo_saldocontrato_codigo_Cls = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Cls");
            Ddo_saldocontrato_codigo_Filteredtext_set = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtext_set");
            Ddo_saldocontrato_codigo_Filteredtextto_set = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_set");
            Ddo_saldocontrato_codigo_Dropdownoptionstype = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_saldocontrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includesortasc"));
            Ddo_saldocontrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includesortdsc"));
            Ddo_saldocontrato_codigo_Sortedstatus = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Sortedstatus");
            Ddo_saldocontrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includefilter"));
            Ddo_saldocontrato_codigo_Filtertype = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filtertype");
            Ddo_saldocontrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filterisrange"));
            Ddo_saldocontrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SALDOCONTRATO_CODIGO_Includedatalist"));
            Ddo_saldocontrato_codigo_Sortasc = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Sortasc");
            Ddo_saldocontrato_codigo_Sortdsc = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Sortdsc");
            Ddo_saldocontrato_codigo_Cleanfilter = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Cleanfilter");
            Ddo_saldocontrato_codigo_Rangefilterfrom = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Rangefilterfrom");
            Ddo_saldocontrato_codigo_Rangefilterto = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Rangefilterto");
            Ddo_saldocontrato_codigo_Searchbuttontext = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Searchbuttontext");
            Ddo_notaempenho_itentificador_Caption = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Caption");
            Ddo_notaempenho_itentificador_Tooltip = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Tooltip");
            Ddo_notaempenho_itentificador_Cls = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Cls");
            Ddo_notaempenho_itentificador_Filteredtext_set = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_set");
            Ddo_notaempenho_itentificador_Selectedvalue_set = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_set");
            Ddo_notaempenho_itentificador_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Dropdownoptionstype");
            Ddo_notaempenho_itentificador_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Titlecontrolidtoreplace");
            Ddo_notaempenho_itentificador_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortasc"));
            Ddo_notaempenho_itentificador_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Includesortdsc"));
            Ddo_notaempenho_itentificador_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Sortedstatus");
            Ddo_notaempenho_itentificador_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Includefilter"));
            Ddo_notaempenho_itentificador_Filtertype = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Filtertype");
            Ddo_notaempenho_itentificador_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Filterisrange"));
            Ddo_notaempenho_itentificador_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Includedatalist"));
            Ddo_notaempenho_itentificador_Datalisttype = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Datalisttype");
            Ddo_notaempenho_itentificador_Datalistproc = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistproc");
            Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_notaempenho_itentificador_Sortasc = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Sortasc");
            Ddo_notaempenho_itentificador_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Sortdsc");
            Ddo_notaempenho_itentificador_Loadingdata = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Loadingdata");
            Ddo_notaempenho_itentificador_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Cleanfilter");
            Ddo_notaempenho_itentificador_Noresultsfound = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Noresultsfound");
            Ddo_notaempenho_itentificador_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Searchbuttontext");
            Ddo_notaempenho_demissao_Caption = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Caption");
            Ddo_notaempenho_demissao_Tooltip = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Tooltip");
            Ddo_notaempenho_demissao_Cls = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Cls");
            Ddo_notaempenho_demissao_Filteredtext_set = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_set");
            Ddo_notaempenho_demissao_Filteredtextto_set = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_set");
            Ddo_notaempenho_demissao_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Dropdownoptionstype");
            Ddo_notaempenho_demissao_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Titlecontrolidtoreplace");
            Ddo_notaempenho_demissao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Includesortasc"));
            Ddo_notaempenho_demissao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Includesortdsc"));
            Ddo_notaempenho_demissao_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Sortedstatus");
            Ddo_notaempenho_demissao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Includefilter"));
            Ddo_notaempenho_demissao_Filtertype = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Filtertype");
            Ddo_notaempenho_demissao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Filterisrange"));
            Ddo_notaempenho_demissao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Includedatalist"));
            Ddo_notaempenho_demissao_Sortasc = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Sortasc");
            Ddo_notaempenho_demissao_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Sortdsc");
            Ddo_notaempenho_demissao_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Cleanfilter");
            Ddo_notaempenho_demissao_Rangefilterfrom = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Rangefilterfrom");
            Ddo_notaempenho_demissao_Rangefilterto = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Rangefilterto");
            Ddo_notaempenho_demissao_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Searchbuttontext");
            Ddo_notaempenho_valor_Caption = cgiGet( "DDO_NOTAEMPENHO_VALOR_Caption");
            Ddo_notaempenho_valor_Tooltip = cgiGet( "DDO_NOTAEMPENHO_VALOR_Tooltip");
            Ddo_notaempenho_valor_Cls = cgiGet( "DDO_NOTAEMPENHO_VALOR_Cls");
            Ddo_notaempenho_valor_Filteredtext_set = cgiGet( "DDO_NOTAEMPENHO_VALOR_Filteredtext_set");
            Ddo_notaempenho_valor_Filteredtextto_set = cgiGet( "DDO_NOTAEMPENHO_VALOR_Filteredtextto_set");
            Ddo_notaempenho_valor_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_VALOR_Dropdownoptionstype");
            Ddo_notaempenho_valor_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_VALOR_Titlecontrolidtoreplace");
            Ddo_notaempenho_valor_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_VALOR_Includesortasc"));
            Ddo_notaempenho_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_VALOR_Includesortdsc"));
            Ddo_notaempenho_valor_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_VALOR_Sortedstatus");
            Ddo_notaempenho_valor_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_VALOR_Includefilter"));
            Ddo_notaempenho_valor_Filtertype = cgiGet( "DDO_NOTAEMPENHO_VALOR_Filtertype");
            Ddo_notaempenho_valor_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_VALOR_Filterisrange"));
            Ddo_notaempenho_valor_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_VALOR_Includedatalist"));
            Ddo_notaempenho_valor_Sortasc = cgiGet( "DDO_NOTAEMPENHO_VALOR_Sortasc");
            Ddo_notaempenho_valor_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_VALOR_Sortdsc");
            Ddo_notaempenho_valor_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_VALOR_Cleanfilter");
            Ddo_notaempenho_valor_Rangefilterfrom = cgiGet( "DDO_NOTAEMPENHO_VALOR_Rangefilterfrom");
            Ddo_notaempenho_valor_Rangefilterto = cgiGet( "DDO_NOTAEMPENHO_VALOR_Rangefilterto");
            Ddo_notaempenho_valor_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_VALOR_Searchbuttontext");
            Ddo_notaempenho_qtd_Caption = cgiGet( "DDO_NOTAEMPENHO_QTD_Caption");
            Ddo_notaempenho_qtd_Tooltip = cgiGet( "DDO_NOTAEMPENHO_QTD_Tooltip");
            Ddo_notaempenho_qtd_Cls = cgiGet( "DDO_NOTAEMPENHO_QTD_Cls");
            Ddo_notaempenho_qtd_Filteredtext_set = cgiGet( "DDO_NOTAEMPENHO_QTD_Filteredtext_set");
            Ddo_notaempenho_qtd_Filteredtextto_set = cgiGet( "DDO_NOTAEMPENHO_QTD_Filteredtextto_set");
            Ddo_notaempenho_qtd_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_QTD_Dropdownoptionstype");
            Ddo_notaempenho_qtd_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_QTD_Titlecontrolidtoreplace");
            Ddo_notaempenho_qtd_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_QTD_Includesortasc"));
            Ddo_notaempenho_qtd_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_QTD_Includesortdsc"));
            Ddo_notaempenho_qtd_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_QTD_Sortedstatus");
            Ddo_notaempenho_qtd_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_QTD_Includefilter"));
            Ddo_notaempenho_qtd_Filtertype = cgiGet( "DDO_NOTAEMPENHO_QTD_Filtertype");
            Ddo_notaempenho_qtd_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_QTD_Filterisrange"));
            Ddo_notaempenho_qtd_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_QTD_Includedatalist"));
            Ddo_notaempenho_qtd_Sortasc = cgiGet( "DDO_NOTAEMPENHO_QTD_Sortasc");
            Ddo_notaempenho_qtd_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_QTD_Sortdsc");
            Ddo_notaempenho_qtd_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_QTD_Cleanfilter");
            Ddo_notaempenho_qtd_Rangefilterfrom = cgiGet( "DDO_NOTAEMPENHO_QTD_Rangefilterfrom");
            Ddo_notaempenho_qtd_Rangefilterto = cgiGet( "DDO_NOTAEMPENHO_QTD_Rangefilterto");
            Ddo_notaempenho_qtd_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_QTD_Searchbuttontext");
            Ddo_notaempenho_saldoant_Caption = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Caption");
            Ddo_notaempenho_saldoant_Tooltip = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Tooltip");
            Ddo_notaempenho_saldoant_Cls = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Cls");
            Ddo_notaempenho_saldoant_Filteredtext_set = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Filteredtext_set");
            Ddo_notaempenho_saldoant_Filteredtextto_set = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_set");
            Ddo_notaempenho_saldoant_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Dropdownoptionstype");
            Ddo_notaempenho_saldoant_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Titlecontrolidtoreplace");
            Ddo_notaempenho_saldoant_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Includesortasc"));
            Ddo_notaempenho_saldoant_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Includesortdsc"));
            Ddo_notaempenho_saldoant_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Sortedstatus");
            Ddo_notaempenho_saldoant_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Includefilter"));
            Ddo_notaempenho_saldoant_Filtertype = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Filtertype");
            Ddo_notaempenho_saldoant_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Filterisrange"));
            Ddo_notaempenho_saldoant_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Includedatalist"));
            Ddo_notaempenho_saldoant_Sortasc = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Sortasc");
            Ddo_notaempenho_saldoant_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Sortdsc");
            Ddo_notaempenho_saldoant_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Cleanfilter");
            Ddo_notaempenho_saldoant_Rangefilterfrom = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Rangefilterfrom");
            Ddo_notaempenho_saldoant_Rangefilterto = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Rangefilterto");
            Ddo_notaempenho_saldoant_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Searchbuttontext");
            Ddo_notaempenho_saldopos_Caption = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Caption");
            Ddo_notaempenho_saldopos_Tooltip = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Tooltip");
            Ddo_notaempenho_saldopos_Cls = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Cls");
            Ddo_notaempenho_saldopos_Filteredtext_set = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_set");
            Ddo_notaempenho_saldopos_Filteredtextto_set = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_set");
            Ddo_notaempenho_saldopos_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Dropdownoptionstype");
            Ddo_notaempenho_saldopos_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Titlecontrolidtoreplace");
            Ddo_notaempenho_saldopos_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Includesortasc"));
            Ddo_notaempenho_saldopos_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Includesortdsc"));
            Ddo_notaempenho_saldopos_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Sortedstatus");
            Ddo_notaempenho_saldopos_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Includefilter"));
            Ddo_notaempenho_saldopos_Filtertype = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Filtertype");
            Ddo_notaempenho_saldopos_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Filterisrange"));
            Ddo_notaempenho_saldopos_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Includedatalist"));
            Ddo_notaempenho_saldopos_Sortasc = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Sortasc");
            Ddo_notaempenho_saldopos_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Sortdsc");
            Ddo_notaempenho_saldopos_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Cleanfilter");
            Ddo_notaempenho_saldopos_Rangefilterfrom = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Rangefilterfrom");
            Ddo_notaempenho_saldopos_Rangefilterto = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Rangefilterto");
            Ddo_notaempenho_saldopos_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Searchbuttontext");
            Ddo_notaempenho_ativo_Caption = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Caption");
            Ddo_notaempenho_ativo_Tooltip = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Tooltip");
            Ddo_notaempenho_ativo_Cls = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Cls");
            Ddo_notaempenho_ativo_Selectedvalue_set = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Selectedvalue_set");
            Ddo_notaempenho_ativo_Dropdownoptionstype = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Dropdownoptionstype");
            Ddo_notaempenho_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Titlecontrolidtoreplace");
            Ddo_notaempenho_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ATIVO_Includesortasc"));
            Ddo_notaempenho_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ATIVO_Includesortdsc"));
            Ddo_notaempenho_ativo_Sortedstatus = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Sortedstatus");
            Ddo_notaempenho_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ATIVO_Includefilter"));
            Ddo_notaempenho_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_NOTAEMPENHO_ATIVO_Includedatalist"));
            Ddo_notaempenho_ativo_Datalisttype = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Datalisttype");
            Ddo_notaempenho_ativo_Datalistfixedvalues = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Datalistfixedvalues");
            Ddo_notaempenho_ativo_Sortasc = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Sortasc");
            Ddo_notaempenho_ativo_Sortdsc = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Sortdsc");
            Ddo_notaempenho_ativo_Cleanfilter = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Cleanfilter");
            Ddo_notaempenho_ativo_Searchbuttontext = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_notaempenho_codigo_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Activeeventkey");
            Ddo_notaempenho_codigo_Filteredtext_get = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Filteredtext_get");
            Ddo_notaempenho_codigo_Filteredtextto_get = cgiGet( "DDO_NOTAEMPENHO_CODIGO_Filteredtextto_get");
            Ddo_saldocontrato_codigo_Activeeventkey = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Activeeventkey");
            Ddo_saldocontrato_codigo_Filteredtext_get = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtext_get");
            Ddo_saldocontrato_codigo_Filteredtextto_get = cgiGet( "DDO_SALDOCONTRATO_CODIGO_Filteredtextto_get");
            Ddo_notaempenho_itentificador_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Activeeventkey");
            Ddo_notaempenho_itentificador_Filteredtext_get = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Filteredtext_get");
            Ddo_notaempenho_itentificador_Selectedvalue_get = cgiGet( "DDO_NOTAEMPENHO_ITENTIFICADOR_Selectedvalue_get");
            Ddo_notaempenho_demissao_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Activeeventkey");
            Ddo_notaempenho_demissao_Filteredtext_get = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Filteredtext_get");
            Ddo_notaempenho_demissao_Filteredtextto_get = cgiGet( "DDO_NOTAEMPENHO_DEMISSAO_Filteredtextto_get");
            Ddo_notaempenho_valor_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_VALOR_Activeeventkey");
            Ddo_notaempenho_valor_Filteredtext_get = cgiGet( "DDO_NOTAEMPENHO_VALOR_Filteredtext_get");
            Ddo_notaempenho_valor_Filteredtextto_get = cgiGet( "DDO_NOTAEMPENHO_VALOR_Filteredtextto_get");
            Ddo_notaempenho_qtd_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_QTD_Activeeventkey");
            Ddo_notaempenho_qtd_Filteredtext_get = cgiGet( "DDO_NOTAEMPENHO_QTD_Filteredtext_get");
            Ddo_notaempenho_qtd_Filteredtextto_get = cgiGet( "DDO_NOTAEMPENHO_QTD_Filteredtextto_get");
            Ddo_notaempenho_saldoant_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Activeeventkey");
            Ddo_notaempenho_saldoant_Filteredtext_get = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Filteredtext_get");
            Ddo_notaempenho_saldoant_Filteredtextto_get = cgiGet( "DDO_NOTAEMPENHO_SALDOANT_Filteredtextto_get");
            Ddo_notaempenho_saldopos_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Activeeventkey");
            Ddo_notaempenho_saldopos_Filteredtext_get = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Filteredtext_get");
            Ddo_notaempenho_saldopos_Filteredtextto_get = cgiGet( "DDO_NOTAEMPENHO_SALDOPOS_Filteredtextto_get");
            Ddo_notaempenho_ativo_Activeeventkey = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Activeeventkey");
            Ddo_notaempenho_ativo_Selectedvalue_get = cgiGet( "DDO_NOTAEMPENHO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSALDOCONTRATO_CODIGO1"), ",", ".") != Convert.ToDecimal( AV17SaldoContrato_Codigo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSALDOCONTRATO_CODIGO2"), ",", ".") != Convert.ToDecimal( AV21SaldoContrato_Codigo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vSALDOCONTRATO_CODIGO3"), ",", ".") != Convert.ToDecimal( AV25SaldoContrato_Codigo3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFNotaEmpenho_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFNotaEmpenho_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFSaldoContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSALDOCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFSaldoContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNOTAEMPENHO_ITENTIFICADOR"), AV39TFNotaEmpenho_Itentificador) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFNOTAEMPENHO_ITENTIFICADOR_SEL"), AV40TFNotaEmpenho_Itentificador_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFNOTAEMPENHO_DEMISSAO"), 0) != AV43TFNotaEmpenho_DEmissao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFNOTAEMPENHO_DEMISSAO_TO"), 0) != AV44TFNotaEmpenho_DEmissao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_VALOR"), ",", ".") != AV49TFNotaEmpenho_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_VALOR_TO"), ",", ".") != AV50TFNotaEmpenho_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_QTD"), ",", ".") != AV53TFNotaEmpenho_Qtd )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_QTD_TO"), ",", ".") != AV54TFNotaEmpenho_Qtd_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOANT"), ",", ".") != AV57TFNotaEmpenho_SaldoAnt )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOANT_TO"), ",", ".") != AV58TFNotaEmpenho_SaldoAnt_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOPOS"), ",", ".") != AV61TFNotaEmpenho_SaldoPos )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_SALDOPOS_TO"), ",", ".") != AV62TFNotaEmpenho_SaldoPos_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFNOTAEMPENHO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV65TFNotaEmpenho_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E31M92 */
         E31M92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E31M92( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SALDOCONTRATO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SALDOCONTRATO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SALDOCONTRATO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfnotaempenho_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_codigo_Visible), 5, 0)));
         edtavTfnotaempenho_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_codigo_to_Visible), 5, 0)));
         edtavTfsaldocontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_codigo_Visible), 5, 0)));
         edtavTfsaldocontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsaldocontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsaldocontrato_codigo_to_Visible), 5, 0)));
         edtavTfnotaempenho_itentificador_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_itentificador_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_itentificador_Visible), 5, 0)));
         edtavTfnotaempenho_itentificador_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_itentificador_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_itentificador_sel_Visible), 5, 0)));
         edtavTfnotaempenho_demissao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_demissao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_demissao_Visible), 5, 0)));
         edtavTfnotaempenho_demissao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_demissao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_demissao_to_Visible), 5, 0)));
         edtavTfnotaempenho_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_valor_Visible), 5, 0)));
         edtavTfnotaempenho_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_valor_to_Visible), 5, 0)));
         edtavTfnotaempenho_qtd_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_qtd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_qtd_Visible), 5, 0)));
         edtavTfnotaempenho_qtd_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_qtd_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_qtd_to_Visible), 5, 0)));
         edtavTfnotaempenho_saldoant_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_saldoant_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldoant_Visible), 5, 0)));
         edtavTfnotaempenho_saldoant_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_saldoant_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldoant_to_Visible), 5, 0)));
         edtavTfnotaempenho_saldopos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_saldopos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldopos_Visible), 5, 0)));
         edtavTfnotaempenho_saldopos_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_saldopos_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_saldopos_to_Visible), 5, 0)));
         edtavTfnotaempenho_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfnotaempenho_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfnotaempenho_ativo_sel_Visible), 5, 0)));
         Ddo_notaempenho_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_codigo_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_codigo_Titlecontrolidtoreplace);
         AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace = Ddo_notaempenho_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace", AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace);
         edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_SaldoContrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_saldocontrato_codigo_Titlecontrolidtoreplace);
         AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace = Ddo_saldocontrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace", AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace);
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_itentificador_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Itentificador";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_itentificador_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_itentificador_Titlecontrolidtoreplace);
         AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = Ddo_notaempenho_itentificador_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace", AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace);
         edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_demissao_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_DEmissao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_demissao_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_demissao_Titlecontrolidtoreplace);
         AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = Ddo_notaempenho_demissao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace", AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace);
         edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_valor_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_valor_Titlecontrolidtoreplace);
         AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace = Ddo_notaempenho_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace", AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace);
         edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_qtd_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Qtd";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_qtd_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_qtd_Titlecontrolidtoreplace);
         AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace = Ddo_notaempenho_qtd_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace", AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace);
         edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_saldoant_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_SaldoAnt";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldoant_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_saldoant_Titlecontrolidtoreplace);
         AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = Ddo_notaempenho_saldoant_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace", AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace);
         edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_saldopos_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_SaldoPos";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldopos_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_saldopos_Titlecontrolidtoreplace);
         AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = Ddo_notaempenho_saldopos_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace", AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace);
         edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_notaempenho_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_NotaEmpenho_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_ativo_Internalname, "TitleControlIdToReplace", Ddo_notaempenho_ativo_Titlecontrolidtoreplace);
         AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace = Ddo_notaempenho_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace", AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace);
         edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Nota Empenho";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Saldo Contrato", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Identificador", 0);
         cmbavOrderedby.addItem("4", "Emiss�o", 0);
         cmbavOrderedby.addItem("5", "Valor", 0);
         cmbavOrderedby.addItem("6", "Quantidade", 0);
         cmbavOrderedby.addItem("7", "Anterior", 0);
         cmbavOrderedby.addItem("8", "Posterior", 0);
         cmbavOrderedby.addItem("9", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV67DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV67DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E32M92( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30NotaEmpenho_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34SaldoContrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38NotaEmpenho_ItentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42NotaEmpenho_DEmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48NotaEmpenho_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52NotaEmpenho_QtdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56NotaEmpenho_SaldoAntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60NotaEmpenho_SaldoPosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64NotaEmpenho_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtNotaEmpenho_Codigo_Titleformat = 2;
         edtNotaEmpenho_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Codigo_Internalname, "Title", edtNotaEmpenho_Codigo_Title);
         edtSaldoContrato_Codigo_Titleformat = 2;
         edtSaldoContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Saldo Contrato", AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSaldoContrato_Codigo_Internalname, "Title", edtSaldoContrato_Codigo_Title);
         edtNotaEmpenho_Itentificador_Titleformat = 2;
         edtNotaEmpenho_Itentificador_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Identificador", AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Itentificador_Internalname, "Title", edtNotaEmpenho_Itentificador_Title);
         edtNotaEmpenho_DEmissao_Titleformat = 2;
         edtNotaEmpenho_DEmissao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Emiss�o", AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_DEmissao_Internalname, "Title", edtNotaEmpenho_DEmissao_Title);
         edtNotaEmpenho_Valor_Titleformat = 2;
         edtNotaEmpenho_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Valor_Internalname, "Title", edtNotaEmpenho_Valor_Title);
         edtNotaEmpenho_Qtd_Titleformat = 2;
         edtNotaEmpenho_Qtd_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Quantidade", AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Qtd_Internalname, "Title", edtNotaEmpenho_Qtd_Title);
         edtNotaEmpenho_SaldoAnt_Titleformat = 2;
         edtNotaEmpenho_SaldoAnt_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Anterior", AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_SaldoAnt_Internalname, "Title", edtNotaEmpenho_SaldoAnt_Title);
         edtNotaEmpenho_SaldoPos_Titleformat = 2;
         edtNotaEmpenho_SaldoPos_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Posterior", AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_SaldoPos_Internalname, "Title", edtNotaEmpenho_SaldoPos_Title);
         chkNotaEmpenho_Ativo_Titleformat = 2;
         chkNotaEmpenho_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNotaEmpenho_Ativo_Internalname, "Title", chkNotaEmpenho_Ativo.Title.Text);
         AV69GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridCurrentPage), 10, 0)));
         AV70GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30NotaEmpenho_CodigoTitleFilterData", AV30NotaEmpenho_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34SaldoContrato_CodigoTitleFilterData", AV34SaldoContrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38NotaEmpenho_ItentificadorTitleFilterData", AV38NotaEmpenho_ItentificadorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42NotaEmpenho_DEmissaoTitleFilterData", AV42NotaEmpenho_DEmissaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48NotaEmpenho_ValorTitleFilterData", AV48NotaEmpenho_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52NotaEmpenho_QtdTitleFilterData", AV52NotaEmpenho_QtdTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56NotaEmpenho_SaldoAntTitleFilterData", AV56NotaEmpenho_SaldoAntTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60NotaEmpenho_SaldoPosTitleFilterData", AV60NotaEmpenho_SaldoPosTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64NotaEmpenho_AtivoTitleFilterData", AV64NotaEmpenho_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11M92( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV68PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV68PageToGo) ;
         }
      }

      protected void E12M92( )
      {
         /* Ddo_notaempenho_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( Ddo_notaempenho_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0)));
            AV32TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( Ddo_notaempenho_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13M92( )
      {
         /* Ddo_saldocontrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_saldocontrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_saldocontrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFSaldoContrato_Codigo = (int)(NumberUtil.Val( Ddo_saldocontrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0)));
            AV36TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_saldocontrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14M92( )
      {
         /* Ddo_notaempenho_itentificador_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_itentificador_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_itentificador_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_itentificador_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_itentificador_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_itentificador_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFNotaEmpenho_Itentificador = Ddo_notaempenho_itentificador_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNotaEmpenho_Itentificador", AV39TFNotaEmpenho_Itentificador);
            AV40TFNotaEmpenho_Itentificador_Sel = Ddo_notaempenho_itentificador_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFNotaEmpenho_Itentificador_Sel", AV40TFNotaEmpenho_Itentificador_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15M92( )
      {
         /* Ddo_notaempenho_demissao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_demissao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_demissao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_demissao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_demissao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_demissao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFNotaEmpenho_DEmissao = context.localUtil.CToT( Ddo_notaempenho_demissao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            AV44TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( Ddo_notaempenho_demissao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV44TFNotaEmpenho_DEmissao_To) )
            {
               AV44TFNotaEmpenho_DEmissao_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV44TFNotaEmpenho_DEmissao_To)), (short)(DateTimeUtil.Month( AV44TFNotaEmpenho_DEmissao_To)), (short)(DateTimeUtil.Day( AV44TFNotaEmpenho_DEmissao_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16M92( )
      {
         /* Ddo_notaempenho_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFNotaEmpenho_Valor = NumberUtil.Val( Ddo_notaempenho_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_Valor, 18, 5)));
            AV50TFNotaEmpenho_Valor_To = NumberUtil.Val( Ddo_notaempenho_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV50TFNotaEmpenho_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17M92( )
      {
         /* Ddo_notaempenho_qtd_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_qtd_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_qtd_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_qtd_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_qtd_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_qtd_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFNotaEmpenho_Qtd = NumberUtil.Val( Ddo_notaempenho_qtd_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_Qtd, 14, 5)));
            AV54TFNotaEmpenho_Qtd_To = NumberUtil.Val( Ddo_notaempenho_qtd_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV54TFNotaEmpenho_Qtd_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18M92( )
      {
         /* Ddo_notaempenho_saldoant_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_saldoant_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldoant_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldoant_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldoant_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldoant_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV57TFNotaEmpenho_SaldoAnt = NumberUtil.Val( Ddo_notaempenho_saldoant_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV57TFNotaEmpenho_SaldoAnt, 18, 5)));
            AV58TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( Ddo_notaempenho_saldoant_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19M92( )
      {
         /* Ddo_notaempenho_saldopos_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_saldopos_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldopos_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldopos_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_saldopos_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_saldopos_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFNotaEmpenho_SaldoPos = NumberUtil.Val( Ddo_notaempenho_saldopos_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV61TFNotaEmpenho_SaldoPos, 18, 5)));
            AV62TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( Ddo_notaempenho_saldopos_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV62TFNotaEmpenho_SaldoPos_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20M92( )
      {
         /* Ddo_notaempenho_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_notaempenho_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_notaempenho_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_notaempenho_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( Ddo_notaempenho_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E33M92( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV73Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E34M92 */
         E34M92 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E34M92( )
      {
         /* Enter Routine */
         AV7InOutNotaEmpenho_Codigo = A1560NotaEmpenho_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutNotaEmpenho_Codigo), 6, 0)));
         AV8InOutSaldoContrato_Codigo = A1561SaldoContrato_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutSaldoContrato_Codigo), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV7InOutNotaEmpenho_Codigo,(int)AV8InOutSaldoContrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E21M92( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E26M92( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E22M92( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E27M92( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28M92( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E23M92( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E29M92( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24M92( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17SaldoContrato_Codigo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21SaldoContrato_Codigo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25SaldoContrato_Codigo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFNotaEmpenho_Codigo, AV32TFNotaEmpenho_Codigo_To, AV35TFSaldoContrato_Codigo, AV36TFSaldoContrato_Codigo_To, AV39TFNotaEmpenho_Itentificador, AV40TFNotaEmpenho_Itentificador_Sel, AV43TFNotaEmpenho_DEmissao, AV44TFNotaEmpenho_DEmissao_To, AV49TFNotaEmpenho_Valor, AV50TFNotaEmpenho_Valor_To, AV53TFNotaEmpenho_Qtd, AV54TFNotaEmpenho_Qtd_To, AV57TFNotaEmpenho_SaldoAnt, AV58TFNotaEmpenho_SaldoAnt_To, AV61TFNotaEmpenho_SaldoPos, AV62TFNotaEmpenho_SaldoPos_To, AV65TFNotaEmpenho_Ativo_Sel, AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace, AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace, AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace, AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace, AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace, AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace, AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace, AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace, AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace, AV74Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E30M92( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25M92( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_notaempenho_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
         Ddo_saldocontrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
         Ddo_notaempenho_itentificador_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
         Ddo_notaempenho_demissao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
         Ddo_notaempenho_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
         Ddo_notaempenho_qtd_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
         Ddo_notaempenho_saldoant_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
         Ddo_notaempenho_saldopos_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
         Ddo_notaempenho_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_notaempenho_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_codigo_Internalname, "SortedStatus", Ddo_notaempenho_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_saldocontrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "SortedStatus", Ddo_saldocontrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_notaempenho_itentificador_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_itentificador_Internalname, "SortedStatus", Ddo_notaempenho_itentificador_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_notaempenho_demissao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_demissao_Internalname, "SortedStatus", Ddo_notaempenho_demissao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_notaempenho_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_valor_Internalname, "SortedStatus", Ddo_notaempenho_valor_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_notaempenho_qtd_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_qtd_Internalname, "SortedStatus", Ddo_notaempenho_qtd_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_notaempenho_saldoant_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldoant_Internalname, "SortedStatus", Ddo_notaempenho_saldoant_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_notaempenho_saldopos_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldopos_Internalname, "SortedStatus", Ddo_notaempenho_saldopos_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_notaempenho_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_ativo_Internalname, "SortedStatus", Ddo_notaempenho_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavSaldocontrato_codigo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_codigo1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 )
         {
            edtavSaldocontrato_codigo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_codigo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_codigo1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavSaldocontrato_codigo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_codigo2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 )
         {
            edtavSaldocontrato_codigo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_codigo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_codigo2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavSaldocontrato_codigo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_codigo3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 )
         {
            edtavSaldocontrato_codigo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_codigo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_codigo3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SALDOCONTRATO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21SaldoContrato_Codigo2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21SaldoContrato_Codigo2), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SALDOCONTRATO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25SaldoContrato_Codigo3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25SaldoContrato_Codigo3), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFNotaEmpenho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0)));
         Ddo_notaempenho_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_codigo_Internalname, "FilteredText_set", Ddo_notaempenho_codigo_Filteredtext_set);
         AV32TFNotaEmpenho_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFNotaEmpenho_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0)));
         Ddo_notaempenho_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_codigo_Internalname, "FilteredTextTo_set", Ddo_notaempenho_codigo_Filteredtextto_set);
         AV35TFSaldoContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0)));
         Ddo_saldocontrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "FilteredText_set", Ddo_saldocontrato_codigo_Filteredtext_set);
         AV36TFSaldoContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFSaldoContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0)));
         Ddo_saldocontrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_saldocontrato_codigo_Internalname, "FilteredTextTo_set", Ddo_saldocontrato_codigo_Filteredtextto_set);
         AV39TFNotaEmpenho_Itentificador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFNotaEmpenho_Itentificador", AV39TFNotaEmpenho_Itentificador);
         Ddo_notaempenho_itentificador_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_itentificador_Internalname, "FilteredText_set", Ddo_notaempenho_itentificador_Filteredtext_set);
         AV40TFNotaEmpenho_Itentificador_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFNotaEmpenho_Itentificador_Sel", AV40TFNotaEmpenho_Itentificador_Sel);
         Ddo_notaempenho_itentificador_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_itentificador_Internalname, "SelectedValue_set", Ddo_notaempenho_itentificador_Selectedvalue_set);
         AV43TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFNotaEmpenho_DEmissao", context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
         Ddo_notaempenho_demissao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_demissao_Internalname, "FilteredText_set", Ddo_notaempenho_demissao_Filteredtext_set);
         AV44TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFNotaEmpenho_DEmissao_To", context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_notaempenho_demissao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_demissao_Internalname, "FilteredTextTo_set", Ddo_notaempenho_demissao_Filteredtextto_set);
         AV49TFNotaEmpenho_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFNotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( AV49TFNotaEmpenho_Valor, 18, 5)));
         Ddo_notaempenho_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_valor_Internalname, "FilteredText_set", Ddo_notaempenho_valor_Filteredtext_set);
         AV50TFNotaEmpenho_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFNotaEmpenho_Valor_To", StringUtil.LTrim( StringUtil.Str( AV50TFNotaEmpenho_Valor_To, 18, 5)));
         Ddo_notaempenho_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_valor_Internalname, "FilteredTextTo_set", Ddo_notaempenho_valor_Filteredtextto_set);
         AV53TFNotaEmpenho_Qtd = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFNotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( AV53TFNotaEmpenho_Qtd, 14, 5)));
         Ddo_notaempenho_qtd_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_qtd_Internalname, "FilteredText_set", Ddo_notaempenho_qtd_Filteredtext_set);
         AV54TFNotaEmpenho_Qtd_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFNotaEmpenho_Qtd_To", StringUtil.LTrim( StringUtil.Str( AV54TFNotaEmpenho_Qtd_To, 14, 5)));
         Ddo_notaempenho_qtd_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_qtd_Internalname, "FilteredTextTo_set", Ddo_notaempenho_qtd_Filteredtextto_set);
         AV57TFNotaEmpenho_SaldoAnt = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFNotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV57TFNotaEmpenho_SaldoAnt, 18, 5)));
         Ddo_notaempenho_saldoant_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldoant_Internalname, "FilteredText_set", Ddo_notaempenho_saldoant_Filteredtext_set);
         AV58TFNotaEmpenho_SaldoAnt_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFNotaEmpenho_SaldoAnt_To", StringUtil.LTrim( StringUtil.Str( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5)));
         Ddo_notaempenho_saldoant_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldoant_Internalname, "FilteredTextTo_set", Ddo_notaempenho_saldoant_Filteredtextto_set);
         AV61TFNotaEmpenho_SaldoPos = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFNotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV61TFNotaEmpenho_SaldoPos, 18, 5)));
         Ddo_notaempenho_saldopos_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldopos_Internalname, "FilteredText_set", Ddo_notaempenho_saldopos_Filteredtext_set);
         AV62TFNotaEmpenho_SaldoPos_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFNotaEmpenho_SaldoPos_To", StringUtil.LTrim( StringUtil.Str( AV62TFNotaEmpenho_SaldoPos_To, 18, 5)));
         Ddo_notaempenho_saldopos_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_saldopos_Internalname, "FilteredTextTo_set", Ddo_notaempenho_saldopos_Filteredtextto_set);
         AV65TFNotaEmpenho_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFNotaEmpenho_Ativo_Sel", StringUtil.Str( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0));
         Ddo_notaempenho_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_notaempenho_ativo_Internalname, "SelectedValue_set", Ddo_notaempenho_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SALDOCONTRATO_CODIGO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17SaldoContrato_Codigo1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17SaldoContrato_Codigo1), 6, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17SaldoContrato_Codigo1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17SaldoContrato_Codigo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17SaldoContrato_Codigo1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21SaldoContrato_Codigo2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21SaldoContrato_Codigo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21SaldoContrato_Codigo2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25SaldoContrato_Codigo3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25SaldoContrato_Codigo3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25SaldoContrato_Codigo3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV31TFNotaEmpenho_Codigo) && (0==AV32TFNotaEmpenho_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFNotaEmpenho_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFNotaEmpenho_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV35TFSaldoContrato_Codigo) && (0==AV36TFSaldoContrato_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSALDOCONTRATO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFSaldoContrato_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFSaldoContrato_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFNotaEmpenho_Itentificador)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_ITENTIFICADOR";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFNotaEmpenho_Itentificador;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFNotaEmpenho_Itentificador_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_ITENTIFICADOR_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFNotaEmpenho_Itentificador_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV43TFNotaEmpenho_DEmissao) && (DateTime.MinValue==AV44TFNotaEmpenho_DEmissao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_DEMISSAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV43TFNotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV44TFNotaEmpenho_DEmissao_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV49TFNotaEmpenho_Valor) && (Convert.ToDecimal(0)==AV50TFNotaEmpenho_Valor_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_VALOR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV49TFNotaEmpenho_Valor, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV50TFNotaEmpenho_Valor_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV53TFNotaEmpenho_Qtd) && (Convert.ToDecimal(0)==AV54TFNotaEmpenho_Qtd_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_QTD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV53TFNotaEmpenho_Qtd, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV54TFNotaEmpenho_Qtd_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV57TFNotaEmpenho_SaldoAnt) && (Convert.ToDecimal(0)==AV58TFNotaEmpenho_SaldoAnt_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_SALDOANT";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV57TFNotaEmpenho_SaldoAnt, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV58TFNotaEmpenho_SaldoAnt_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV61TFNotaEmpenho_SaldoPos) && (Convert.ToDecimal(0)==AV62TFNotaEmpenho_SaldoPos_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_SALDOPOS";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV61TFNotaEmpenho_SaldoPos, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV62TFNotaEmpenho_SaldoPos_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV65TFNotaEmpenho_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFNOTAEMPENHO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV65TFNotaEmpenho_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV74Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ! (0==AV17SaldoContrato_Codigo1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV17SaldoContrato_Codigo1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ! (0==AV21SaldoContrato_Codigo2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV21SaldoContrato_Codigo2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ! (0==AV25SaldoContrato_Codigo3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV25SaldoContrato_Codigo3), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_M92( true) ;
         }
         else
         {
            wb_table2_5_M92( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_M92( true) ;
         }
         else
         {
            wb_table3_74_M92( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_M92e( true) ;
         }
         else
         {
            wb_table1_2_M92e( false) ;
         }
      }

      protected void wb_table3_74_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_M92( true) ;
         }
         else
         {
            wb_table4_77_M92( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_M92e( true) ;
         }
         else
         {
            wb_table3_74_M92e( false) ;
         }
      }

      protected void wb_table4_77_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSaldoContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSaldoContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSaldoContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Itentificador_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Itentificador_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Itentificador_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_DEmissao_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_DEmissao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_DEmissao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_Qtd_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_Qtd_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_Qtd_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_SaldoAnt_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_SaldoAnt_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_SaldoAnt_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtNotaEmpenho_SaldoPos_Titleformat == 0 )
               {
                  context.SendWebValue( edtNotaEmpenho_SaldoPos_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtNotaEmpenho_SaldoPos_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkNotaEmpenho_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkNotaEmpenho_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkNotaEmpenho_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSaldoContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSaldoContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1564NotaEmpenho_Itentificador));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Itentificador_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Itentificador_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_DEmissao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_DEmissao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_Qtd_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_Qtd_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_SaldoAnt_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_SaldoAnt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtNotaEmpenho_SaldoPos_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtNotaEmpenho_SaldoPos_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1570NotaEmpenho_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkNotaEmpenho_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkNotaEmpenho_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_M92e( true) ;
         }
         else
         {
            wb_table4_77_M92e( false) ;
         }
      }

      protected void wb_table2_5_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptNotaEmpenho.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_M92( true) ;
         }
         else
         {
            wb_table5_14_M92( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_M92e( true) ;
         }
         else
         {
            wb_table2_5_M92e( false) ;
         }
      }

      protected void wb_table5_14_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_M92( true) ;
         }
         else
         {
            wb_table6_19_M92( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_M92e( true) ;
         }
         else
         {
            wb_table5_14_M92e( false) ;
         }
      }

      protected void wb_table6_19_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptNotaEmpenho.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_M92( true) ;
         }
         else
         {
            wb_table7_28_M92( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptNotaEmpenho.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_M92( true) ;
         }
         else
         {
            wb_table8_45_M92( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptNotaEmpenho.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_M92( true) ;
         }
         else
         {
            wb_table9_62_M92( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_M92e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_M92e( true) ;
         }
         else
         {
            wb_table6_19_M92e( false) ;
         }
      }

      protected void wb_table9_62_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptNotaEmpenho.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_codigo3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25SaldoContrato_Codigo3), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25SaldoContrato_Codigo3), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_codigo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSaldocontrato_codigo3_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_M92e( true) ;
         }
         else
         {
            wb_table9_62_M92e( false) ;
         }
      }

      protected void wb_table8_45_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptNotaEmpenho.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_codigo2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21SaldoContrato_Codigo2), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21SaldoContrato_Codigo2), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_codigo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSaldocontrato_codigo2_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_M92e( true) ;
         }
         else
         {
            wb_table8_45_M92e( false) ;
         }
      }

      protected void wb_table7_28_M92( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptNotaEmpenho.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSaldocontrato_codigo1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17SaldoContrato_Codigo1), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17SaldoContrato_Codigo1), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSaldocontrato_codigo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSaldocontrato_codigo1_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptNotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_M92e( true) ;
         }
         else
         {
            wb_table7_28_M92e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutNotaEmpenho_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutNotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutNotaEmpenho_Codigo), 6, 0)));
         AV8InOutSaldoContrato_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutSaldoContrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAM92( ) ;
         WSM92( ) ;
         WEM92( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031193025");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptnotaempenho.js", "?202031193025");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtNotaEmpenho_Codigo_Internalname = "NOTAEMPENHO_CODIGO_"+sGXsfl_80_idx;
         edtSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO_"+sGXsfl_80_idx;
         edtNotaEmpenho_Itentificador_Internalname = "NOTAEMPENHO_ITENTIFICADOR_"+sGXsfl_80_idx;
         edtNotaEmpenho_DEmissao_Internalname = "NOTAEMPENHO_DEMISSAO_"+sGXsfl_80_idx;
         edtNotaEmpenho_Valor_Internalname = "NOTAEMPENHO_VALOR_"+sGXsfl_80_idx;
         edtNotaEmpenho_Qtd_Internalname = "NOTAEMPENHO_QTD_"+sGXsfl_80_idx;
         edtNotaEmpenho_SaldoAnt_Internalname = "NOTAEMPENHO_SALDOANT_"+sGXsfl_80_idx;
         edtNotaEmpenho_SaldoPos_Internalname = "NOTAEMPENHO_SALDOPOS_"+sGXsfl_80_idx;
         chkNotaEmpenho_Ativo_Internalname = "NOTAEMPENHO_ATIVO_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtNotaEmpenho_Codigo_Internalname = "NOTAEMPENHO_CODIGO_"+sGXsfl_80_fel_idx;
         edtSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO_"+sGXsfl_80_fel_idx;
         edtNotaEmpenho_Itentificador_Internalname = "NOTAEMPENHO_ITENTIFICADOR_"+sGXsfl_80_fel_idx;
         edtNotaEmpenho_DEmissao_Internalname = "NOTAEMPENHO_DEMISSAO_"+sGXsfl_80_fel_idx;
         edtNotaEmpenho_Valor_Internalname = "NOTAEMPENHO_VALOR_"+sGXsfl_80_fel_idx;
         edtNotaEmpenho_Qtd_Internalname = "NOTAEMPENHO_QTD_"+sGXsfl_80_fel_idx;
         edtNotaEmpenho_SaldoAnt_Internalname = "NOTAEMPENHO_SALDOANT_"+sGXsfl_80_fel_idx;
         edtNotaEmpenho_SaldoPos_Internalname = "NOTAEMPENHO_SALDOPOS_"+sGXsfl_80_fel_idx;
         chkNotaEmpenho_Ativo_Internalname = "NOTAEMPENHO_ATIVO_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBM90( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV73Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV73Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSaldoContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSaldoContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Itentificador_Internalname,StringUtil.RTrim( A1564NotaEmpenho_Itentificador),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Itentificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_DEmissao_Internalname,context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_DEmissao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ",", "")),context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_Qtd_Internalname,StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ",", "")),context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_Qtd_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_SaldoAnt_Internalname,StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ",", "")),context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_SaldoAnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtNotaEmpenho_SaldoPos_Internalname,StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ",", "")),context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtNotaEmpenho_SaldoPos_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkNotaEmpenho_Ativo_Internalname,StringUtil.BoolToStr( A1570NotaEmpenho_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SALDOCONTRATO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A1561SaldoContrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_ITENTIFICADOR"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_DEMISSAO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_VALOR"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_QTD"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_SALDOANT"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_SALDOPOS"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_NOTAEMPENHO_ATIVO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A1570NotaEmpenho_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavSaldocontrato_codigo1_Internalname = "vSALDOCONTRATO_CODIGO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavSaldocontrato_codigo2_Internalname = "vSALDOCONTRATO_CODIGO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavSaldocontrato_codigo3_Internalname = "vSALDOCONTRATO_CODIGO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtNotaEmpenho_Codigo_Internalname = "NOTAEMPENHO_CODIGO";
         edtSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO";
         edtNotaEmpenho_Itentificador_Internalname = "NOTAEMPENHO_ITENTIFICADOR";
         edtNotaEmpenho_DEmissao_Internalname = "NOTAEMPENHO_DEMISSAO";
         edtNotaEmpenho_Valor_Internalname = "NOTAEMPENHO_VALOR";
         edtNotaEmpenho_Qtd_Internalname = "NOTAEMPENHO_QTD";
         edtNotaEmpenho_SaldoAnt_Internalname = "NOTAEMPENHO_SALDOANT";
         edtNotaEmpenho_SaldoPos_Internalname = "NOTAEMPENHO_SALDOPOS";
         chkNotaEmpenho_Ativo_Internalname = "NOTAEMPENHO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfnotaempenho_codigo_Internalname = "vTFNOTAEMPENHO_CODIGO";
         edtavTfnotaempenho_codigo_to_Internalname = "vTFNOTAEMPENHO_CODIGO_TO";
         edtavTfsaldocontrato_codigo_Internalname = "vTFSALDOCONTRATO_CODIGO";
         edtavTfsaldocontrato_codigo_to_Internalname = "vTFSALDOCONTRATO_CODIGO_TO";
         edtavTfnotaempenho_itentificador_Internalname = "vTFNOTAEMPENHO_ITENTIFICADOR";
         edtavTfnotaempenho_itentificador_sel_Internalname = "vTFNOTAEMPENHO_ITENTIFICADOR_SEL";
         edtavTfnotaempenho_demissao_Internalname = "vTFNOTAEMPENHO_DEMISSAO";
         edtavTfnotaempenho_demissao_to_Internalname = "vTFNOTAEMPENHO_DEMISSAO_TO";
         edtavDdo_notaempenho_demissaoauxdate_Internalname = "vDDO_NOTAEMPENHO_DEMISSAOAUXDATE";
         edtavDdo_notaempenho_demissaoauxdateto_Internalname = "vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO";
         divDdo_notaempenho_demissaoauxdates_Internalname = "DDO_NOTAEMPENHO_DEMISSAOAUXDATES";
         edtavTfnotaempenho_valor_Internalname = "vTFNOTAEMPENHO_VALOR";
         edtavTfnotaempenho_valor_to_Internalname = "vTFNOTAEMPENHO_VALOR_TO";
         edtavTfnotaempenho_qtd_Internalname = "vTFNOTAEMPENHO_QTD";
         edtavTfnotaempenho_qtd_to_Internalname = "vTFNOTAEMPENHO_QTD_TO";
         edtavTfnotaempenho_saldoant_Internalname = "vTFNOTAEMPENHO_SALDOANT";
         edtavTfnotaempenho_saldoant_to_Internalname = "vTFNOTAEMPENHO_SALDOANT_TO";
         edtavTfnotaempenho_saldopos_Internalname = "vTFNOTAEMPENHO_SALDOPOS";
         edtavTfnotaempenho_saldopos_to_Internalname = "vTFNOTAEMPENHO_SALDOPOS_TO";
         edtavTfnotaempenho_ativo_sel_Internalname = "vTFNOTAEMPENHO_ATIVO_SEL";
         Ddo_notaempenho_codigo_Internalname = "DDO_NOTAEMPENHO_CODIGO";
         edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_saldocontrato_codigo_Internalname = "DDO_SALDOCONTRATO_CODIGO";
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_itentificador_Internalname = "DDO_NOTAEMPENHO_ITENTIFICADOR";
         edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_demissao_Internalname = "DDO_NOTAEMPENHO_DEMISSAO";
         edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_valor_Internalname = "DDO_NOTAEMPENHO_VALOR";
         edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_qtd_Internalname = "DDO_NOTAEMPENHO_QTD";
         edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_saldoant_Internalname = "DDO_NOTAEMPENHO_SALDOANT";
         edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_saldopos_Internalname = "DDO_NOTAEMPENHO_SALDOPOS";
         edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE";
         Ddo_notaempenho_ativo_Internalname = "DDO_NOTAEMPENHO_ATIVO";
         edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname = "vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtNotaEmpenho_SaldoPos_Jsonclick = "";
         edtNotaEmpenho_SaldoAnt_Jsonclick = "";
         edtNotaEmpenho_Qtd_Jsonclick = "";
         edtNotaEmpenho_Valor_Jsonclick = "";
         edtNotaEmpenho_DEmissao_Jsonclick = "";
         edtNotaEmpenho_Itentificador_Jsonclick = "";
         edtSaldoContrato_Codigo_Jsonclick = "";
         edtNotaEmpenho_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavSaldocontrato_codigo1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavSaldocontrato_codigo2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavSaldocontrato_codigo3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkNotaEmpenho_Ativo_Titleformat = 0;
         edtNotaEmpenho_SaldoPos_Titleformat = 0;
         edtNotaEmpenho_SaldoAnt_Titleformat = 0;
         edtNotaEmpenho_Qtd_Titleformat = 0;
         edtNotaEmpenho_Valor_Titleformat = 0;
         edtNotaEmpenho_DEmissao_Titleformat = 0;
         edtNotaEmpenho_Itentificador_Titleformat = 0;
         edtSaldoContrato_Codigo_Titleformat = 0;
         edtNotaEmpenho_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavSaldocontrato_codigo3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavSaldocontrato_codigo2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavSaldocontrato_codigo1_Visible = 1;
         chkNotaEmpenho_Ativo.Title.Text = "Ativo";
         edtNotaEmpenho_SaldoPos_Title = "Posterior";
         edtNotaEmpenho_SaldoAnt_Title = "Anterior";
         edtNotaEmpenho_Qtd_Title = "Quantidade";
         edtNotaEmpenho_Valor_Title = "Valor";
         edtNotaEmpenho_DEmissao_Title = "Emiss�o";
         edtNotaEmpenho_Itentificador_Title = "Identificador";
         edtSaldoContrato_Codigo_Title = "Saldo Contrato";
         edtNotaEmpenho_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkNotaEmpenho_Ativo.Caption = "";
         edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfnotaempenho_ativo_sel_Jsonclick = "";
         edtavTfnotaempenho_ativo_sel_Visible = 1;
         edtavTfnotaempenho_saldopos_to_Jsonclick = "";
         edtavTfnotaempenho_saldopos_to_Visible = 1;
         edtavTfnotaempenho_saldopos_Jsonclick = "";
         edtavTfnotaempenho_saldopos_Visible = 1;
         edtavTfnotaempenho_saldoant_to_Jsonclick = "";
         edtavTfnotaempenho_saldoant_to_Visible = 1;
         edtavTfnotaempenho_saldoant_Jsonclick = "";
         edtavTfnotaempenho_saldoant_Visible = 1;
         edtavTfnotaempenho_qtd_to_Jsonclick = "";
         edtavTfnotaempenho_qtd_to_Visible = 1;
         edtavTfnotaempenho_qtd_Jsonclick = "";
         edtavTfnotaempenho_qtd_Visible = 1;
         edtavTfnotaempenho_valor_to_Jsonclick = "";
         edtavTfnotaempenho_valor_to_Visible = 1;
         edtavTfnotaempenho_valor_Jsonclick = "";
         edtavTfnotaempenho_valor_Visible = 1;
         edtavDdo_notaempenho_demissaoauxdateto_Jsonclick = "";
         edtavDdo_notaempenho_demissaoauxdate_Jsonclick = "";
         edtavTfnotaempenho_demissao_to_Jsonclick = "";
         edtavTfnotaempenho_demissao_to_Visible = 1;
         edtavTfnotaempenho_demissao_Jsonclick = "";
         edtavTfnotaempenho_demissao_Visible = 1;
         edtavTfnotaempenho_itentificador_sel_Jsonclick = "";
         edtavTfnotaempenho_itentificador_sel_Visible = 1;
         edtavTfnotaempenho_itentificador_Jsonclick = "";
         edtavTfnotaempenho_itentificador_Visible = 1;
         edtavTfsaldocontrato_codigo_to_Jsonclick = "";
         edtavTfsaldocontrato_codigo_to_Visible = 1;
         edtavTfsaldocontrato_codigo_Jsonclick = "";
         edtavTfsaldocontrato_codigo_Visible = 1;
         edtavTfnotaempenho_codigo_to_Jsonclick = "";
         edtavTfnotaempenho_codigo_to_Visible = 1;
         edtavTfnotaempenho_codigo_Jsonclick = "";
         edtavTfnotaempenho_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_notaempenho_ativo_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_notaempenho_ativo_Datalisttype = "FixedValues";
         Ddo_notaempenho_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_notaempenho_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_notaempenho_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_ativo_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_ativo_Cls = "ColumnSettings";
         Ddo_notaempenho_ativo_Tooltip = "Op��es";
         Ddo_notaempenho_ativo_Caption = "";
         Ddo_notaempenho_saldopos_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_saldopos_Rangefilterto = "At�";
         Ddo_notaempenho_saldopos_Rangefilterfrom = "Desde";
         Ddo_notaempenho_saldopos_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_saldopos_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_saldopos_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_saldopos_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_saldopos_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Filtertype = "Numeric";
         Ddo_notaempenho_saldopos_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldopos_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_saldopos_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_saldopos_Cls = "ColumnSettings";
         Ddo_notaempenho_saldopos_Tooltip = "Op��es";
         Ddo_notaempenho_saldopos_Caption = "";
         Ddo_notaempenho_saldoant_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_saldoant_Rangefilterto = "At�";
         Ddo_notaempenho_saldoant_Rangefilterfrom = "Desde";
         Ddo_notaempenho_saldoant_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_saldoant_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_saldoant_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_saldoant_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_saldoant_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Filtertype = "Numeric";
         Ddo_notaempenho_saldoant_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_saldoant_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_saldoant_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_saldoant_Cls = "ColumnSettings";
         Ddo_notaempenho_saldoant_Tooltip = "Op��es";
         Ddo_notaempenho_saldoant_Caption = "";
         Ddo_notaempenho_qtd_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_qtd_Rangefilterto = "At�";
         Ddo_notaempenho_qtd_Rangefilterfrom = "Desde";
         Ddo_notaempenho_qtd_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_qtd_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_qtd_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_qtd_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_qtd_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Filtertype = "Numeric";
         Ddo_notaempenho_qtd_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_qtd_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_qtd_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_qtd_Cls = "ColumnSettings";
         Ddo_notaempenho_qtd_Tooltip = "Op��es";
         Ddo_notaempenho_qtd_Caption = "";
         Ddo_notaempenho_valor_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_valor_Rangefilterto = "At�";
         Ddo_notaempenho_valor_Rangefilterfrom = "Desde";
         Ddo_notaempenho_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_valor_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Filtertype = "Numeric";
         Ddo_notaempenho_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_valor_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_valor_Cls = "ColumnSettings";
         Ddo_notaempenho_valor_Tooltip = "Op��es";
         Ddo_notaempenho_valor_Caption = "";
         Ddo_notaempenho_demissao_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_demissao_Rangefilterto = "At�";
         Ddo_notaempenho_demissao_Rangefilterfrom = "Desde";
         Ddo_notaempenho_demissao_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_demissao_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_demissao_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_demissao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_demissao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Filtertype = "Date";
         Ddo_notaempenho_demissao_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_demissao_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_demissao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_demissao_Cls = "ColumnSettings";
         Ddo_notaempenho_demissao_Tooltip = "Op��es";
         Ddo_notaempenho_demissao_Caption = "";
         Ddo_notaempenho_itentificador_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_itentificador_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_notaempenho_itentificador_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_itentificador_Loadingdata = "Carregando dados...";
         Ddo_notaempenho_itentificador_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_itentificador_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters = 0;
         Ddo_notaempenho_itentificador_Datalistproc = "GetPromptNotaEmpenhoFilterData";
         Ddo_notaempenho_itentificador_Datalisttype = "Dynamic";
         Ddo_notaempenho_itentificador_Includedatalist = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Filterisrange = Convert.ToBoolean( 0);
         Ddo_notaempenho_itentificador_Filtertype = "Character";
         Ddo_notaempenho_itentificador_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_itentificador_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_itentificador_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_itentificador_Cls = "ColumnSettings";
         Ddo_notaempenho_itentificador_Tooltip = "Op��es";
         Ddo_notaempenho_itentificador_Caption = "";
         Ddo_saldocontrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_saldocontrato_codigo_Rangefilterto = "At�";
         Ddo_saldocontrato_codigo_Rangefilterfrom = "Desde";
         Ddo_saldocontrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_saldocontrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_saldocontrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_saldocontrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_saldocontrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Filtertype = "Numeric";
         Ddo_saldocontrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_saldocontrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_saldocontrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_saldocontrato_codigo_Cls = "ColumnSettings";
         Ddo_saldocontrato_codigo_Tooltip = "Op��es";
         Ddo_saldocontrato_codigo_Caption = "";
         Ddo_notaempenho_codigo_Searchbuttontext = "Pesquisar";
         Ddo_notaempenho_codigo_Rangefilterto = "At�";
         Ddo_notaempenho_codigo_Rangefilterfrom = "Desde";
         Ddo_notaempenho_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_notaempenho_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_notaempenho_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_notaempenho_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_notaempenho_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Filtertype = "Numeric";
         Ddo_notaempenho_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_notaempenho_codigo_Titlecontrolidtoreplace = "";
         Ddo_notaempenho_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_notaempenho_codigo_Cls = "ColumnSettings";
         Ddo_notaempenho_codigo_Tooltip = "Op��es";
         Ddo_notaempenho_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Nota Empenho";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV30NotaEmpenho_CodigoTitleFilterData',fld:'vNOTAEMPENHO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34SaldoContrato_CodigoTitleFilterData',fld:'vSALDOCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38NotaEmpenho_ItentificadorTitleFilterData',fld:'vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV42NotaEmpenho_DEmissaoTitleFilterData',fld:'vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48NotaEmpenho_ValorTitleFilterData',fld:'vNOTAEMPENHO_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV52NotaEmpenho_QtdTitleFilterData',fld:'vNOTAEMPENHO_QTDTITLEFILTERDATA',pic:'',nv:null},{av:'AV56NotaEmpenho_SaldoAntTitleFilterData',fld:'vNOTAEMPENHO_SALDOANTTITLEFILTERDATA',pic:'',nv:null},{av:'AV60NotaEmpenho_SaldoPosTitleFilterData',fld:'vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA',pic:'',nv:null},{av:'AV64NotaEmpenho_AtivoTitleFilterData',fld:'vNOTAEMPENHO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtNotaEmpenho_Codigo_Titleformat',ctrl:'NOTAEMPENHO_CODIGO',prop:'Titleformat'},{av:'edtNotaEmpenho_Codigo_Title',ctrl:'NOTAEMPENHO_CODIGO',prop:'Title'},{av:'edtSaldoContrato_Codigo_Titleformat',ctrl:'SALDOCONTRATO_CODIGO',prop:'Titleformat'},{av:'edtSaldoContrato_Codigo_Title',ctrl:'SALDOCONTRATO_CODIGO',prop:'Title'},{av:'edtNotaEmpenho_Itentificador_Titleformat',ctrl:'NOTAEMPENHO_ITENTIFICADOR',prop:'Titleformat'},{av:'edtNotaEmpenho_Itentificador_Title',ctrl:'NOTAEMPENHO_ITENTIFICADOR',prop:'Title'},{av:'edtNotaEmpenho_DEmissao_Titleformat',ctrl:'NOTAEMPENHO_DEMISSAO',prop:'Titleformat'},{av:'edtNotaEmpenho_DEmissao_Title',ctrl:'NOTAEMPENHO_DEMISSAO',prop:'Title'},{av:'edtNotaEmpenho_Valor_Titleformat',ctrl:'NOTAEMPENHO_VALOR',prop:'Titleformat'},{av:'edtNotaEmpenho_Valor_Title',ctrl:'NOTAEMPENHO_VALOR',prop:'Title'},{av:'edtNotaEmpenho_Qtd_Titleformat',ctrl:'NOTAEMPENHO_QTD',prop:'Titleformat'},{av:'edtNotaEmpenho_Qtd_Title',ctrl:'NOTAEMPENHO_QTD',prop:'Title'},{av:'edtNotaEmpenho_SaldoAnt_Titleformat',ctrl:'NOTAEMPENHO_SALDOANT',prop:'Titleformat'},{av:'edtNotaEmpenho_SaldoAnt_Title',ctrl:'NOTAEMPENHO_SALDOANT',prop:'Title'},{av:'edtNotaEmpenho_SaldoPos_Titleformat',ctrl:'NOTAEMPENHO_SALDOPOS',prop:'Titleformat'},{av:'edtNotaEmpenho_SaldoPos_Title',ctrl:'NOTAEMPENHO_SALDOPOS',prop:'Title'},{av:'chkNotaEmpenho_Ativo_Titleformat',ctrl:'NOTAEMPENHO_ATIVO',prop:'Titleformat'},{av:'chkNotaEmpenho_Ativo.Title.Text',ctrl:'NOTAEMPENHO_ATIVO',prop:'Title'},{av:'AV69GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV70GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_NOTAEMPENHO_CODIGO.ONOPTIONCLICKED","{handler:'E12M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_codigo_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_codigo_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_notaempenho_codigo_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E13M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_saldocontrato_codigo_Activeeventkey',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_saldocontrato_codigo_Filteredtext_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_saldocontrato_codigo_Filteredtextto_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_ITENTIFICADOR.ONOPTIONCLICKED","{handler:'E14M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_itentificador_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_itentificador_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'FilteredText_get'},{av:'Ddo_notaempenho_itentificador_Selectedvalue_get',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_DEMISSAO.ONOPTIONCLICKED","{handler:'E15M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_demissao_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_demissao_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredText_get'},{av:'Ddo_notaempenho_demissao_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_VALOR.ONOPTIONCLICKED","{handler:'E16M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_valor_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_valor_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredText_get'},{av:'Ddo_notaempenho_valor_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_QTD.ONOPTIONCLICKED","{handler:'E17M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_qtd_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_qtd_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredText_get'},{av:'Ddo_notaempenho_qtd_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_SALDOANT.ONOPTIONCLICKED","{handler:'E18M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_saldoant_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_saldoant_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredText_get'},{av:'Ddo_notaempenho_saldoant_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_SALDOPOS.ONOPTIONCLICKED","{handler:'E19M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_saldopos_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_saldopos_Filteredtext_get',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredText_get'},{av:'Ddo_notaempenho_saldopos_Filteredtextto_get',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_NOTAEMPENHO_ATIVO.ONOPTIONCLICKED","{handler:'E20M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_notaempenho_ativo_Activeeventkey',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_notaempenho_ativo_Selectedvalue_get',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_notaempenho_ativo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_notaempenho_codigo_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'Ddo_saldocontrato_codigo_Sortedstatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_notaempenho_itentificador_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_demissao_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'Ddo_notaempenho_valor_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'Ddo_notaempenho_qtd_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldoant_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'Ddo_notaempenho_saldopos_Sortedstatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E33M92',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E34M92',iparms:[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7InOutNotaEmpenho_Codigo',fld:'vINOUTNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutSaldoContrato_Codigo',fld:'vINOUTSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E21M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E26M92',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E22M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSaldocontrato_codigo2_Visible',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSaldocontrato_codigo3_Visible',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSaldocontrato_codigo1_Visible',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E27M92',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavSaldocontrato_codigo1_Visible',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E28M92',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E23M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSaldocontrato_codigo2_Visible',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSaldocontrato_codigo3_Visible',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSaldocontrato_codigo1_Visible',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E29M92',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavSaldocontrato_codigo2_Visible',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E24M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSaldocontrato_codigo2_Visible',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSaldocontrato_codigo3_Visible',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavSaldocontrato_codigo1_Visible',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E30M92',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavSaldocontrato_codigo3_Visible',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E25M92',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_notaempenho_codigo_Filteredtext_set',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredText_set'},{av:'AV32TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_notaempenho_codigo_Filteredtextto_set',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Filteredtext_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV36TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_saldocontrato_codigo_Filteredtextto_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'Ddo_notaempenho_itentificador_Filteredtext_set',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'FilteredText_set'},{av:'AV40TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'Ddo_notaempenho_itentificador_Selectedvalue_set',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SelectedValue_set'},{av:'AV43TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_notaempenho_demissao_Filteredtext_set',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredText_set'},{av:'AV44TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_notaempenho_demissao_Filteredtextto_set',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredTextTo_set'},{av:'AV49TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_valor_Filteredtext_set',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredText_set'},{av:'AV50TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_valor_Filteredtextto_set',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredTextTo_set'},{av:'AV53TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_notaempenho_qtd_Filteredtext_set',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredText_set'},{av:'AV54TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_notaempenho_qtd_Filteredtextto_set',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredTextTo_set'},{av:'AV57TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_saldoant_Filteredtext_set',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredText_set'},{av:'AV58TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_saldoant_Filteredtextto_set',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredTextTo_set'},{av:'AV61TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_saldopos_Filteredtext_set',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredText_set'},{av:'AV62TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_notaempenho_saldopos_Filteredtextto_set',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredTextTo_set'},{av:'AV65TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_notaempenho_ativo_Selectedvalue_set',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavSaldocontrato_codigo1_Visible',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavSaldocontrato_codigo2_Visible',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavSaldocontrato_codigo3_Visible',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_notaempenho_codigo_Activeeventkey = "";
         Ddo_notaempenho_codigo_Filteredtext_get = "";
         Ddo_notaempenho_codigo_Filteredtextto_get = "";
         Ddo_saldocontrato_codigo_Activeeventkey = "";
         Ddo_saldocontrato_codigo_Filteredtext_get = "";
         Ddo_saldocontrato_codigo_Filteredtextto_get = "";
         Ddo_notaempenho_itentificador_Activeeventkey = "";
         Ddo_notaempenho_itentificador_Filteredtext_get = "";
         Ddo_notaempenho_itentificador_Selectedvalue_get = "";
         Ddo_notaempenho_demissao_Activeeventkey = "";
         Ddo_notaempenho_demissao_Filteredtext_get = "";
         Ddo_notaempenho_demissao_Filteredtextto_get = "";
         Ddo_notaempenho_valor_Activeeventkey = "";
         Ddo_notaempenho_valor_Filteredtext_get = "";
         Ddo_notaempenho_valor_Filteredtextto_get = "";
         Ddo_notaempenho_qtd_Activeeventkey = "";
         Ddo_notaempenho_qtd_Filteredtext_get = "";
         Ddo_notaempenho_qtd_Filteredtextto_get = "";
         Ddo_notaempenho_saldoant_Activeeventkey = "";
         Ddo_notaempenho_saldoant_Filteredtext_get = "";
         Ddo_notaempenho_saldoant_Filteredtextto_get = "";
         Ddo_notaempenho_saldopos_Activeeventkey = "";
         Ddo_notaempenho_saldopos_Filteredtext_get = "";
         Ddo_notaempenho_saldopos_Filteredtextto_get = "";
         Ddo_notaempenho_ativo_Activeeventkey = "";
         Ddo_notaempenho_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV39TFNotaEmpenho_Itentificador = "";
         AV40TFNotaEmpenho_Itentificador_Sel = "";
         AV43TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         AV44TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
         AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace = "";
         AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace = "";
         AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = "";
         AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = "";
         AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace = "";
         AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace = "";
         AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = "";
         AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = "";
         AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace = "";
         AV74Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV67DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30NotaEmpenho_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34SaldoContrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38NotaEmpenho_ItentificadorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42NotaEmpenho_DEmissaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48NotaEmpenho_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52NotaEmpenho_QtdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56NotaEmpenho_SaldoAntTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60NotaEmpenho_SaldoPosTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64NotaEmpenho_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_notaempenho_codigo_Filteredtext_set = "";
         Ddo_notaempenho_codigo_Filteredtextto_set = "";
         Ddo_notaempenho_codigo_Sortedstatus = "";
         Ddo_saldocontrato_codigo_Filteredtext_set = "";
         Ddo_saldocontrato_codigo_Filteredtextto_set = "";
         Ddo_saldocontrato_codigo_Sortedstatus = "";
         Ddo_notaempenho_itentificador_Filteredtext_set = "";
         Ddo_notaempenho_itentificador_Selectedvalue_set = "";
         Ddo_notaempenho_itentificador_Sortedstatus = "";
         Ddo_notaempenho_demissao_Filteredtext_set = "";
         Ddo_notaempenho_demissao_Filteredtextto_set = "";
         Ddo_notaempenho_demissao_Sortedstatus = "";
         Ddo_notaempenho_valor_Filteredtext_set = "";
         Ddo_notaempenho_valor_Filteredtextto_set = "";
         Ddo_notaempenho_valor_Sortedstatus = "";
         Ddo_notaempenho_qtd_Filteredtext_set = "";
         Ddo_notaempenho_qtd_Filteredtextto_set = "";
         Ddo_notaempenho_qtd_Sortedstatus = "";
         Ddo_notaempenho_saldoant_Filteredtext_set = "";
         Ddo_notaempenho_saldoant_Filteredtextto_set = "";
         Ddo_notaempenho_saldoant_Sortedstatus = "";
         Ddo_notaempenho_saldopos_Filteredtext_set = "";
         Ddo_notaempenho_saldopos_Filteredtextto_set = "";
         Ddo_notaempenho_saldopos_Sortedstatus = "";
         Ddo_notaempenho_ativo_Selectedvalue_set = "";
         Ddo_notaempenho_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV45DDO_NotaEmpenho_DEmissaoAuxDate = DateTime.MinValue;
         AV46DDO_NotaEmpenho_DEmissaoAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV73Select_GXI = "";
         A1564NotaEmpenho_Itentificador = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV39TFNotaEmpenho_Itentificador = "";
         H00M92_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         H00M92_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         H00M92_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         H00M92_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         H00M92_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         H00M92_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         H00M92_A1567NotaEmpenho_Qtd = new decimal[1] ;
         H00M92_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         H00M92_A1566NotaEmpenho_Valor = new decimal[1] ;
         H00M92_n1566NotaEmpenho_Valor = new bool[] {false} ;
         H00M92_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         H00M92_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         H00M92_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         H00M92_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         H00M92_A1561SaldoContrato_Codigo = new int[1] ;
         H00M92_A1560NotaEmpenho_Codigo = new int[1] ;
         H00M93_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptnotaempenho__default(),
            new Object[][] {
                new Object[] {
               H00M92_A1570NotaEmpenho_Ativo, H00M92_n1570NotaEmpenho_Ativo, H00M92_A1569NotaEmpenho_SaldoPos, H00M92_n1569NotaEmpenho_SaldoPos, H00M92_A1568NotaEmpenho_SaldoAnt, H00M92_n1568NotaEmpenho_SaldoAnt, H00M92_A1567NotaEmpenho_Qtd, H00M92_n1567NotaEmpenho_Qtd, H00M92_A1566NotaEmpenho_Valor, H00M92_n1566NotaEmpenho_Valor,
               H00M92_A1565NotaEmpenho_DEmissao, H00M92_n1565NotaEmpenho_DEmissao, H00M92_A1564NotaEmpenho_Itentificador, H00M92_n1564NotaEmpenho_Itentificador, H00M92_A1561SaldoContrato_Codigo, H00M92_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               H00M93_AGRID_nRecordCount
               }
            }
         );
         AV74Pgmname = "PromptNotaEmpenho";
         /* GeneXus formulas. */
         AV74Pgmname = "PromptNotaEmpenho";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV65TFNotaEmpenho_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtNotaEmpenho_Codigo_Titleformat ;
      private short edtSaldoContrato_Codigo_Titleformat ;
      private short edtNotaEmpenho_Itentificador_Titleformat ;
      private short edtNotaEmpenho_DEmissao_Titleformat ;
      private short edtNotaEmpenho_Valor_Titleformat ;
      private short edtNotaEmpenho_Qtd_Titleformat ;
      private short edtNotaEmpenho_SaldoAnt_Titleformat ;
      private short edtNotaEmpenho_SaldoPos_Titleformat ;
      private short chkNotaEmpenho_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutNotaEmpenho_Codigo ;
      private int AV8InOutSaldoContrato_Codigo ;
      private int wcpOAV7InOutNotaEmpenho_Codigo ;
      private int wcpOAV8InOutSaldoContrato_Codigo ;
      private int subGrid_Rows ;
      private int AV17SaldoContrato_Codigo1 ;
      private int AV21SaldoContrato_Codigo2 ;
      private int AV25SaldoContrato_Codigo3 ;
      private int AV31TFNotaEmpenho_Codigo ;
      private int AV32TFNotaEmpenho_Codigo_To ;
      private int AV35TFSaldoContrato_Codigo ;
      private int AV36TFSaldoContrato_Codigo_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_notaempenho_itentificador_Datalistupdateminimumcharacters ;
      private int edtavTfnotaempenho_codigo_Visible ;
      private int edtavTfnotaempenho_codigo_to_Visible ;
      private int edtavTfsaldocontrato_codigo_Visible ;
      private int edtavTfsaldocontrato_codigo_to_Visible ;
      private int edtavTfnotaempenho_itentificador_Visible ;
      private int edtavTfnotaempenho_itentificador_sel_Visible ;
      private int edtavTfnotaempenho_demissao_Visible ;
      private int edtavTfnotaempenho_demissao_to_Visible ;
      private int edtavTfnotaempenho_valor_Visible ;
      private int edtavTfnotaempenho_valor_to_Visible ;
      private int edtavTfnotaempenho_qtd_Visible ;
      private int edtavTfnotaempenho_qtd_to_Visible ;
      private int edtavTfnotaempenho_saldoant_Visible ;
      private int edtavTfnotaempenho_saldoant_to_Visible ;
      private int edtavTfnotaempenho_saldopos_Visible ;
      private int edtavTfnotaempenho_saldopos_to_Visible ;
      private int edtavTfnotaempenho_ativo_sel_Visible ;
      private int edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_valortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Visible ;
      private int edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Visible ;
      private int A1560NotaEmpenho_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV68PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavSaldocontrato_codigo1_Visible ;
      private int edtavSaldocontrato_codigo2_Visible ;
      private int edtavSaldocontrato_codigo3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV69GridCurrentPage ;
      private long AV70GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV49TFNotaEmpenho_Valor ;
      private decimal AV50TFNotaEmpenho_Valor_To ;
      private decimal AV53TFNotaEmpenho_Qtd ;
      private decimal AV54TFNotaEmpenho_Qtd_To ;
      private decimal AV57TFNotaEmpenho_SaldoAnt ;
      private decimal AV58TFNotaEmpenho_SaldoAnt_To ;
      private decimal AV61TFNotaEmpenho_SaldoPos ;
      private decimal AV62TFNotaEmpenho_SaldoPos_To ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal A1567NotaEmpenho_Qtd ;
      private decimal A1568NotaEmpenho_SaldoAnt ;
      private decimal A1569NotaEmpenho_SaldoPos ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_notaempenho_codigo_Activeeventkey ;
      private String Ddo_notaempenho_codigo_Filteredtext_get ;
      private String Ddo_notaempenho_codigo_Filteredtextto_get ;
      private String Ddo_saldocontrato_codigo_Activeeventkey ;
      private String Ddo_saldocontrato_codigo_Filteredtext_get ;
      private String Ddo_saldocontrato_codigo_Filteredtextto_get ;
      private String Ddo_notaempenho_itentificador_Activeeventkey ;
      private String Ddo_notaempenho_itentificador_Filteredtext_get ;
      private String Ddo_notaempenho_itentificador_Selectedvalue_get ;
      private String Ddo_notaempenho_demissao_Activeeventkey ;
      private String Ddo_notaempenho_demissao_Filteredtext_get ;
      private String Ddo_notaempenho_demissao_Filteredtextto_get ;
      private String Ddo_notaempenho_valor_Activeeventkey ;
      private String Ddo_notaempenho_valor_Filteredtext_get ;
      private String Ddo_notaempenho_valor_Filteredtextto_get ;
      private String Ddo_notaempenho_qtd_Activeeventkey ;
      private String Ddo_notaempenho_qtd_Filteredtext_get ;
      private String Ddo_notaempenho_qtd_Filteredtextto_get ;
      private String Ddo_notaempenho_saldoant_Activeeventkey ;
      private String Ddo_notaempenho_saldoant_Filteredtext_get ;
      private String Ddo_notaempenho_saldoant_Filteredtextto_get ;
      private String Ddo_notaempenho_saldopos_Activeeventkey ;
      private String Ddo_notaempenho_saldopos_Filteredtext_get ;
      private String Ddo_notaempenho_saldopos_Filteredtextto_get ;
      private String Ddo_notaempenho_ativo_Activeeventkey ;
      private String Ddo_notaempenho_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV39TFNotaEmpenho_Itentificador ;
      private String AV40TFNotaEmpenho_Itentificador_Sel ;
      private String AV74Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_notaempenho_codigo_Caption ;
      private String Ddo_notaempenho_codigo_Tooltip ;
      private String Ddo_notaempenho_codigo_Cls ;
      private String Ddo_notaempenho_codigo_Filteredtext_set ;
      private String Ddo_notaempenho_codigo_Filteredtextto_set ;
      private String Ddo_notaempenho_codigo_Dropdownoptionstype ;
      private String Ddo_notaempenho_codigo_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_codigo_Sortedstatus ;
      private String Ddo_notaempenho_codigo_Filtertype ;
      private String Ddo_notaempenho_codigo_Sortasc ;
      private String Ddo_notaempenho_codigo_Sortdsc ;
      private String Ddo_notaempenho_codigo_Cleanfilter ;
      private String Ddo_notaempenho_codigo_Rangefilterfrom ;
      private String Ddo_notaempenho_codigo_Rangefilterto ;
      private String Ddo_notaempenho_codigo_Searchbuttontext ;
      private String Ddo_saldocontrato_codigo_Caption ;
      private String Ddo_saldocontrato_codigo_Tooltip ;
      private String Ddo_saldocontrato_codigo_Cls ;
      private String Ddo_saldocontrato_codigo_Filteredtext_set ;
      private String Ddo_saldocontrato_codigo_Filteredtextto_set ;
      private String Ddo_saldocontrato_codigo_Dropdownoptionstype ;
      private String Ddo_saldocontrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_saldocontrato_codigo_Sortedstatus ;
      private String Ddo_saldocontrato_codigo_Filtertype ;
      private String Ddo_saldocontrato_codigo_Sortasc ;
      private String Ddo_saldocontrato_codigo_Sortdsc ;
      private String Ddo_saldocontrato_codigo_Cleanfilter ;
      private String Ddo_saldocontrato_codigo_Rangefilterfrom ;
      private String Ddo_saldocontrato_codigo_Rangefilterto ;
      private String Ddo_saldocontrato_codigo_Searchbuttontext ;
      private String Ddo_notaempenho_itentificador_Caption ;
      private String Ddo_notaempenho_itentificador_Tooltip ;
      private String Ddo_notaempenho_itentificador_Cls ;
      private String Ddo_notaempenho_itentificador_Filteredtext_set ;
      private String Ddo_notaempenho_itentificador_Selectedvalue_set ;
      private String Ddo_notaempenho_itentificador_Dropdownoptionstype ;
      private String Ddo_notaempenho_itentificador_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_itentificador_Sortedstatus ;
      private String Ddo_notaempenho_itentificador_Filtertype ;
      private String Ddo_notaempenho_itentificador_Datalisttype ;
      private String Ddo_notaempenho_itentificador_Datalistproc ;
      private String Ddo_notaempenho_itentificador_Sortasc ;
      private String Ddo_notaempenho_itentificador_Sortdsc ;
      private String Ddo_notaempenho_itentificador_Loadingdata ;
      private String Ddo_notaempenho_itentificador_Cleanfilter ;
      private String Ddo_notaempenho_itentificador_Noresultsfound ;
      private String Ddo_notaempenho_itentificador_Searchbuttontext ;
      private String Ddo_notaempenho_demissao_Caption ;
      private String Ddo_notaempenho_demissao_Tooltip ;
      private String Ddo_notaempenho_demissao_Cls ;
      private String Ddo_notaempenho_demissao_Filteredtext_set ;
      private String Ddo_notaempenho_demissao_Filteredtextto_set ;
      private String Ddo_notaempenho_demissao_Dropdownoptionstype ;
      private String Ddo_notaempenho_demissao_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_demissao_Sortedstatus ;
      private String Ddo_notaempenho_demissao_Filtertype ;
      private String Ddo_notaempenho_demissao_Sortasc ;
      private String Ddo_notaempenho_demissao_Sortdsc ;
      private String Ddo_notaempenho_demissao_Cleanfilter ;
      private String Ddo_notaempenho_demissao_Rangefilterfrom ;
      private String Ddo_notaempenho_demissao_Rangefilterto ;
      private String Ddo_notaempenho_demissao_Searchbuttontext ;
      private String Ddo_notaempenho_valor_Caption ;
      private String Ddo_notaempenho_valor_Tooltip ;
      private String Ddo_notaempenho_valor_Cls ;
      private String Ddo_notaempenho_valor_Filteredtext_set ;
      private String Ddo_notaempenho_valor_Filteredtextto_set ;
      private String Ddo_notaempenho_valor_Dropdownoptionstype ;
      private String Ddo_notaempenho_valor_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_valor_Sortedstatus ;
      private String Ddo_notaempenho_valor_Filtertype ;
      private String Ddo_notaempenho_valor_Sortasc ;
      private String Ddo_notaempenho_valor_Sortdsc ;
      private String Ddo_notaempenho_valor_Cleanfilter ;
      private String Ddo_notaempenho_valor_Rangefilterfrom ;
      private String Ddo_notaempenho_valor_Rangefilterto ;
      private String Ddo_notaempenho_valor_Searchbuttontext ;
      private String Ddo_notaempenho_qtd_Caption ;
      private String Ddo_notaempenho_qtd_Tooltip ;
      private String Ddo_notaempenho_qtd_Cls ;
      private String Ddo_notaempenho_qtd_Filteredtext_set ;
      private String Ddo_notaempenho_qtd_Filteredtextto_set ;
      private String Ddo_notaempenho_qtd_Dropdownoptionstype ;
      private String Ddo_notaempenho_qtd_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_qtd_Sortedstatus ;
      private String Ddo_notaempenho_qtd_Filtertype ;
      private String Ddo_notaempenho_qtd_Sortasc ;
      private String Ddo_notaempenho_qtd_Sortdsc ;
      private String Ddo_notaempenho_qtd_Cleanfilter ;
      private String Ddo_notaempenho_qtd_Rangefilterfrom ;
      private String Ddo_notaempenho_qtd_Rangefilterto ;
      private String Ddo_notaempenho_qtd_Searchbuttontext ;
      private String Ddo_notaempenho_saldoant_Caption ;
      private String Ddo_notaempenho_saldoant_Tooltip ;
      private String Ddo_notaempenho_saldoant_Cls ;
      private String Ddo_notaempenho_saldoant_Filteredtext_set ;
      private String Ddo_notaempenho_saldoant_Filteredtextto_set ;
      private String Ddo_notaempenho_saldoant_Dropdownoptionstype ;
      private String Ddo_notaempenho_saldoant_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_saldoant_Sortedstatus ;
      private String Ddo_notaempenho_saldoant_Filtertype ;
      private String Ddo_notaempenho_saldoant_Sortasc ;
      private String Ddo_notaempenho_saldoant_Sortdsc ;
      private String Ddo_notaempenho_saldoant_Cleanfilter ;
      private String Ddo_notaempenho_saldoant_Rangefilterfrom ;
      private String Ddo_notaempenho_saldoant_Rangefilterto ;
      private String Ddo_notaempenho_saldoant_Searchbuttontext ;
      private String Ddo_notaempenho_saldopos_Caption ;
      private String Ddo_notaempenho_saldopos_Tooltip ;
      private String Ddo_notaempenho_saldopos_Cls ;
      private String Ddo_notaempenho_saldopos_Filteredtext_set ;
      private String Ddo_notaempenho_saldopos_Filteredtextto_set ;
      private String Ddo_notaempenho_saldopos_Dropdownoptionstype ;
      private String Ddo_notaempenho_saldopos_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_saldopos_Sortedstatus ;
      private String Ddo_notaempenho_saldopos_Filtertype ;
      private String Ddo_notaempenho_saldopos_Sortasc ;
      private String Ddo_notaempenho_saldopos_Sortdsc ;
      private String Ddo_notaempenho_saldopos_Cleanfilter ;
      private String Ddo_notaempenho_saldopos_Rangefilterfrom ;
      private String Ddo_notaempenho_saldopos_Rangefilterto ;
      private String Ddo_notaempenho_saldopos_Searchbuttontext ;
      private String Ddo_notaempenho_ativo_Caption ;
      private String Ddo_notaempenho_ativo_Tooltip ;
      private String Ddo_notaempenho_ativo_Cls ;
      private String Ddo_notaempenho_ativo_Selectedvalue_set ;
      private String Ddo_notaempenho_ativo_Dropdownoptionstype ;
      private String Ddo_notaempenho_ativo_Titlecontrolidtoreplace ;
      private String Ddo_notaempenho_ativo_Sortedstatus ;
      private String Ddo_notaempenho_ativo_Datalisttype ;
      private String Ddo_notaempenho_ativo_Datalistfixedvalues ;
      private String Ddo_notaempenho_ativo_Sortasc ;
      private String Ddo_notaempenho_ativo_Sortdsc ;
      private String Ddo_notaempenho_ativo_Cleanfilter ;
      private String Ddo_notaempenho_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfnotaempenho_codigo_Internalname ;
      private String edtavTfnotaempenho_codigo_Jsonclick ;
      private String edtavTfnotaempenho_codigo_to_Internalname ;
      private String edtavTfnotaempenho_codigo_to_Jsonclick ;
      private String edtavTfsaldocontrato_codigo_Internalname ;
      private String edtavTfsaldocontrato_codigo_Jsonclick ;
      private String edtavTfsaldocontrato_codigo_to_Internalname ;
      private String edtavTfsaldocontrato_codigo_to_Jsonclick ;
      private String edtavTfnotaempenho_itentificador_Internalname ;
      private String edtavTfnotaempenho_itentificador_Jsonclick ;
      private String edtavTfnotaempenho_itentificador_sel_Internalname ;
      private String edtavTfnotaempenho_itentificador_sel_Jsonclick ;
      private String edtavTfnotaempenho_demissao_Internalname ;
      private String edtavTfnotaempenho_demissao_Jsonclick ;
      private String edtavTfnotaempenho_demissao_to_Internalname ;
      private String edtavTfnotaempenho_demissao_to_Jsonclick ;
      private String divDdo_notaempenho_demissaoauxdates_Internalname ;
      private String edtavDdo_notaempenho_demissaoauxdate_Internalname ;
      private String edtavDdo_notaempenho_demissaoauxdate_Jsonclick ;
      private String edtavDdo_notaempenho_demissaoauxdateto_Internalname ;
      private String edtavDdo_notaempenho_demissaoauxdateto_Jsonclick ;
      private String edtavTfnotaempenho_valor_Internalname ;
      private String edtavTfnotaempenho_valor_Jsonclick ;
      private String edtavTfnotaempenho_valor_to_Internalname ;
      private String edtavTfnotaempenho_valor_to_Jsonclick ;
      private String edtavTfnotaempenho_qtd_Internalname ;
      private String edtavTfnotaempenho_qtd_Jsonclick ;
      private String edtavTfnotaempenho_qtd_to_Internalname ;
      private String edtavTfnotaempenho_qtd_to_Jsonclick ;
      private String edtavTfnotaempenho_saldoant_Internalname ;
      private String edtavTfnotaempenho_saldoant_Jsonclick ;
      private String edtavTfnotaempenho_saldoant_to_Internalname ;
      private String edtavTfnotaempenho_saldoant_to_Jsonclick ;
      private String edtavTfnotaempenho_saldopos_Internalname ;
      private String edtavTfnotaempenho_saldopos_Jsonclick ;
      private String edtavTfnotaempenho_saldopos_to_Internalname ;
      private String edtavTfnotaempenho_saldopos_to_Jsonclick ;
      private String edtavTfnotaempenho_ativo_sel_Internalname ;
      private String edtavTfnotaempenho_ativo_sel_Jsonclick ;
      private String edtavDdo_notaempenho_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_saldocontrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_itentificadortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_demissaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_valortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_qtdtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_saldoanttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_saldopostitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_notaempenho_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtNotaEmpenho_Codigo_Internalname ;
      private String edtSaldoContrato_Codigo_Internalname ;
      private String A1564NotaEmpenho_Itentificador ;
      private String edtNotaEmpenho_Itentificador_Internalname ;
      private String edtNotaEmpenho_DEmissao_Internalname ;
      private String edtNotaEmpenho_Valor_Internalname ;
      private String edtNotaEmpenho_Qtd_Internalname ;
      private String edtNotaEmpenho_SaldoAnt_Internalname ;
      private String edtNotaEmpenho_SaldoPos_Internalname ;
      private String chkNotaEmpenho_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV39TFNotaEmpenho_Itentificador ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavSaldocontrato_codigo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavSaldocontrato_codigo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavSaldocontrato_codigo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_notaempenho_codigo_Internalname ;
      private String Ddo_saldocontrato_codigo_Internalname ;
      private String Ddo_notaempenho_itentificador_Internalname ;
      private String Ddo_notaempenho_demissao_Internalname ;
      private String Ddo_notaempenho_valor_Internalname ;
      private String Ddo_notaempenho_qtd_Internalname ;
      private String Ddo_notaempenho_saldoant_Internalname ;
      private String Ddo_notaempenho_saldopos_Internalname ;
      private String Ddo_notaempenho_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtNotaEmpenho_Codigo_Title ;
      private String edtSaldoContrato_Codigo_Title ;
      private String edtNotaEmpenho_Itentificador_Title ;
      private String edtNotaEmpenho_DEmissao_Title ;
      private String edtNotaEmpenho_Valor_Title ;
      private String edtNotaEmpenho_Qtd_Title ;
      private String edtNotaEmpenho_SaldoAnt_Title ;
      private String edtNotaEmpenho_SaldoPos_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavSaldocontrato_codigo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavSaldocontrato_codigo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavSaldocontrato_codigo1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtNotaEmpenho_Codigo_Jsonclick ;
      private String edtSaldoContrato_Codigo_Jsonclick ;
      private String edtNotaEmpenho_Itentificador_Jsonclick ;
      private String edtNotaEmpenho_DEmissao_Jsonclick ;
      private String edtNotaEmpenho_Valor_Jsonclick ;
      private String edtNotaEmpenho_Qtd_Jsonclick ;
      private String edtNotaEmpenho_SaldoAnt_Jsonclick ;
      private String edtNotaEmpenho_SaldoPos_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV43TFNotaEmpenho_DEmissao ;
      private DateTime AV44TFNotaEmpenho_DEmissao_To ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private DateTime AV45DDO_NotaEmpenho_DEmissaoAuxDate ;
      private DateTime AV46DDO_NotaEmpenho_DEmissaoAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_notaempenho_codigo_Includesortasc ;
      private bool Ddo_notaempenho_codigo_Includesortdsc ;
      private bool Ddo_notaempenho_codigo_Includefilter ;
      private bool Ddo_notaempenho_codigo_Filterisrange ;
      private bool Ddo_notaempenho_codigo_Includedatalist ;
      private bool Ddo_saldocontrato_codigo_Includesortasc ;
      private bool Ddo_saldocontrato_codigo_Includesortdsc ;
      private bool Ddo_saldocontrato_codigo_Includefilter ;
      private bool Ddo_saldocontrato_codigo_Filterisrange ;
      private bool Ddo_saldocontrato_codigo_Includedatalist ;
      private bool Ddo_notaempenho_itentificador_Includesortasc ;
      private bool Ddo_notaempenho_itentificador_Includesortdsc ;
      private bool Ddo_notaempenho_itentificador_Includefilter ;
      private bool Ddo_notaempenho_itentificador_Filterisrange ;
      private bool Ddo_notaempenho_itentificador_Includedatalist ;
      private bool Ddo_notaempenho_demissao_Includesortasc ;
      private bool Ddo_notaempenho_demissao_Includesortdsc ;
      private bool Ddo_notaempenho_demissao_Includefilter ;
      private bool Ddo_notaempenho_demissao_Filterisrange ;
      private bool Ddo_notaempenho_demissao_Includedatalist ;
      private bool Ddo_notaempenho_valor_Includesortasc ;
      private bool Ddo_notaempenho_valor_Includesortdsc ;
      private bool Ddo_notaempenho_valor_Includefilter ;
      private bool Ddo_notaempenho_valor_Filterisrange ;
      private bool Ddo_notaempenho_valor_Includedatalist ;
      private bool Ddo_notaempenho_qtd_Includesortasc ;
      private bool Ddo_notaempenho_qtd_Includesortdsc ;
      private bool Ddo_notaempenho_qtd_Includefilter ;
      private bool Ddo_notaempenho_qtd_Filterisrange ;
      private bool Ddo_notaempenho_qtd_Includedatalist ;
      private bool Ddo_notaempenho_saldoant_Includesortasc ;
      private bool Ddo_notaempenho_saldoant_Includesortdsc ;
      private bool Ddo_notaempenho_saldoant_Includefilter ;
      private bool Ddo_notaempenho_saldoant_Filterisrange ;
      private bool Ddo_notaempenho_saldoant_Includedatalist ;
      private bool Ddo_notaempenho_saldopos_Includesortasc ;
      private bool Ddo_notaempenho_saldopos_Includesortdsc ;
      private bool Ddo_notaempenho_saldopos_Includefilter ;
      private bool Ddo_notaempenho_saldopos_Filterisrange ;
      private bool Ddo_notaempenho_saldopos_Includedatalist ;
      private bool Ddo_notaempenho_ativo_Includesortasc ;
      private bool Ddo_notaempenho_ativo_Includesortdsc ;
      private bool Ddo_notaempenho_ativo_Includefilter ;
      private bool Ddo_notaempenho_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool n1565NotaEmpenho_DEmissao ;
      private bool n1566NotaEmpenho_Valor ;
      private bool n1567NotaEmpenho_Qtd ;
      private bool n1568NotaEmpenho_SaldoAnt ;
      private bool n1569NotaEmpenho_SaldoPos ;
      private bool A1570NotaEmpenho_Ativo ;
      private bool n1570NotaEmpenho_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV33ddo_NotaEmpenho_CodigoTitleControlIdToReplace ;
      private String AV37ddo_SaldoContrato_CodigoTitleControlIdToReplace ;
      private String AV41ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace ;
      private String AV47ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace ;
      private String AV51ddo_NotaEmpenho_ValorTitleControlIdToReplace ;
      private String AV55ddo_NotaEmpenho_QtdTitleControlIdToReplace ;
      private String AV59ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace ;
      private String AV63ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace ;
      private String AV66ddo_NotaEmpenho_AtivoTitleControlIdToReplace ;
      private String AV73Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutNotaEmpenho_Codigo ;
      private int aP1_InOutSaldoContrato_Codigo ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkNotaEmpenho_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00M92_A1570NotaEmpenho_Ativo ;
      private bool[] H00M92_n1570NotaEmpenho_Ativo ;
      private decimal[] H00M92_A1569NotaEmpenho_SaldoPos ;
      private bool[] H00M92_n1569NotaEmpenho_SaldoPos ;
      private decimal[] H00M92_A1568NotaEmpenho_SaldoAnt ;
      private bool[] H00M92_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] H00M92_A1567NotaEmpenho_Qtd ;
      private bool[] H00M92_n1567NotaEmpenho_Qtd ;
      private decimal[] H00M92_A1566NotaEmpenho_Valor ;
      private bool[] H00M92_n1566NotaEmpenho_Valor ;
      private DateTime[] H00M92_A1565NotaEmpenho_DEmissao ;
      private bool[] H00M92_n1565NotaEmpenho_DEmissao ;
      private String[] H00M92_A1564NotaEmpenho_Itentificador ;
      private bool[] H00M92_n1564NotaEmpenho_Itentificador ;
      private int[] H00M92_A1561SaldoContrato_Codigo ;
      private int[] H00M92_A1560NotaEmpenho_Codigo ;
      private long[] H00M93_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30NotaEmpenho_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34SaldoContrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38NotaEmpenho_ItentificadorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42NotaEmpenho_DEmissaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48NotaEmpenho_ValorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52NotaEmpenho_QtdTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV56NotaEmpenho_SaldoAntTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60NotaEmpenho_SaldoPosTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64NotaEmpenho_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV67DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptnotaempenho__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00M92( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV17SaldoContrato_Codigo1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             int AV21SaldoContrato_Codigo2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             int AV25SaldoContrato_Codigo3 ,
                                             int AV31TFNotaEmpenho_Codigo ,
                                             int AV32TFNotaEmpenho_Codigo_To ,
                                             int AV35TFSaldoContrato_Codigo ,
                                             int AV36TFSaldoContrato_Codigo_To ,
                                             String AV40TFNotaEmpenho_Itentificador_Sel ,
                                             String AV39TFNotaEmpenho_Itentificador ,
                                             DateTime AV43TFNotaEmpenho_DEmissao ,
                                             DateTime AV44TFNotaEmpenho_DEmissao_To ,
                                             decimal AV49TFNotaEmpenho_Valor ,
                                             decimal AV50TFNotaEmpenho_Valor_To ,
                                             decimal AV53TFNotaEmpenho_Qtd ,
                                             decimal AV54TFNotaEmpenho_Qtd_To ,
                                             decimal AV57TFNotaEmpenho_SaldoAnt ,
                                             decimal AV58TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV61TFNotaEmpenho_SaldoPos ,
                                             decimal AV62TFNotaEmpenho_SaldoPos_To ,
                                             short AV65TFNotaEmpenho_Ativo_Sel ,
                                             int A1561SaldoContrato_Codigo ,
                                             int A1560NotaEmpenho_Codigo ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [30] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [NotaEmpenho_Ativo], [NotaEmpenho_SaldoPos], [NotaEmpenho_SaldoAnt], [NotaEmpenho_Qtd], [NotaEmpenho_Valor], [NotaEmpenho_DEmissao], [NotaEmpenho_Itentificador], [SaldoContrato_Codigo], [NotaEmpenho_Codigo]";
         sFromString = " FROM [NotaEmpenho] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV17SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV17SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV17SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV17SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV17SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV17SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV17SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV17SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV17SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! (0==AV21SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV21SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV21SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! (0==AV21SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV21SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV21SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! (0==AV21SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV21SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV21SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! (0==AV25SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV25SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV25SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! (0==AV25SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV25SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV25SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! (0==AV25SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV25SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV25SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV31TFNotaEmpenho_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] >= @AV31TFNotaEmpenho_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Codigo] >= @AV31TFNotaEmpenho_Codigo)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV32TFNotaEmpenho_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] <= @AV32TFNotaEmpenho_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Codigo] <= @AV32TFNotaEmpenho_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV35TFSaldoContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] >= @AV35TFSaldoContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] >= @AV35TFSaldoContrato_Codigo)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV36TFSaldoContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] <= @AV36TFSaldoContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] <= @AV36TFSaldoContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFNotaEmpenho_Itentificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like @lV39TFNotaEmpenho_Itentificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Itentificador] like @lV39TFNotaEmpenho_Itentificador)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFNotaEmpenho_Itentificador_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] = @AV40TFNotaEmpenho_Itentificador_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Itentificador] = @AV40TFNotaEmpenho_Itentificador_Sel)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV43TFNotaEmpenho_DEmissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] >= @AV43TFNotaEmpenho_DEmissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_DEmissao] >= @AV43TFNotaEmpenho_DEmissao)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV44TFNotaEmpenho_DEmissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] <= @AV44TFNotaEmpenho_DEmissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_DEmissao] <= @AV44TFNotaEmpenho_DEmissao_To)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV49TFNotaEmpenho_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Valor] >= @AV49TFNotaEmpenho_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Valor] >= @AV49TFNotaEmpenho_Valor)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV50TFNotaEmpenho_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Valor] <= @AV50TFNotaEmpenho_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Valor] <= @AV50TFNotaEmpenho_Valor_To)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV53TFNotaEmpenho_Qtd) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] >= @AV53TFNotaEmpenho_Qtd)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Qtd] >= @AV53TFNotaEmpenho_Qtd)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV54TFNotaEmpenho_Qtd_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] <= @AV54TFNotaEmpenho_Qtd_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Qtd] <= @AV54TFNotaEmpenho_Qtd_To)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV57TFNotaEmpenho_SaldoAnt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] >= @AV57TFNotaEmpenho_SaldoAnt)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoAnt] >= @AV57TFNotaEmpenho_SaldoAnt)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58TFNotaEmpenho_SaldoAnt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] <= @AV58TFNotaEmpenho_SaldoAnt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoAnt] <= @AV58TFNotaEmpenho_SaldoAnt_To)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFNotaEmpenho_SaldoPos) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] >= @AV61TFNotaEmpenho_SaldoPos)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoPos] >= @AV61TFNotaEmpenho_SaldoPos)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFNotaEmpenho_SaldoPos_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] <= @AV62TFNotaEmpenho_SaldoPos_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoPos] <= @AV62TFNotaEmpenho_SaldoPos_To)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV65TFNotaEmpenho_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Ativo] = 1)";
            }
         }
         if ( AV65TFNotaEmpenho_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SaldoContrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SaldoContrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Itentificador]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Itentificador] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_DEmissao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_DEmissao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Valor]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Valor] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Qtd]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Qtd] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_SaldoAnt]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_SaldoAnt] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_SaldoPos]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_SaldoPos] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Ativo]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [NotaEmpenho_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00M93( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             int AV17SaldoContrato_Codigo1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             int AV21SaldoContrato_Codigo2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             int AV25SaldoContrato_Codigo3 ,
                                             int AV31TFNotaEmpenho_Codigo ,
                                             int AV32TFNotaEmpenho_Codigo_To ,
                                             int AV35TFSaldoContrato_Codigo ,
                                             int AV36TFSaldoContrato_Codigo_To ,
                                             String AV40TFNotaEmpenho_Itentificador_Sel ,
                                             String AV39TFNotaEmpenho_Itentificador ,
                                             DateTime AV43TFNotaEmpenho_DEmissao ,
                                             DateTime AV44TFNotaEmpenho_DEmissao_To ,
                                             decimal AV49TFNotaEmpenho_Valor ,
                                             decimal AV50TFNotaEmpenho_Valor_To ,
                                             decimal AV53TFNotaEmpenho_Qtd ,
                                             decimal AV54TFNotaEmpenho_Qtd_To ,
                                             decimal AV57TFNotaEmpenho_SaldoAnt ,
                                             decimal AV58TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV61TFNotaEmpenho_SaldoPos ,
                                             decimal AV62TFNotaEmpenho_SaldoPos_To ,
                                             short AV65TFNotaEmpenho_Ativo_Sel ,
                                             int A1561SaldoContrato_Codigo ,
                                             int A1560NotaEmpenho_Codigo ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [NotaEmpenho] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! (0==AV17SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV17SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV17SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! (0==AV17SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV17SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV17SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV16DynamicFiltersOperator1 == 2 ) && ( ! (0==AV17SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV17SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV17SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! (0==AV21SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV21SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV21SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! (0==AV21SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV21SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV21SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV20DynamicFiltersOperator2 == 2 ) && ( ! (0==AV21SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV21SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV21SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! (0==AV25SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV25SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV25SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! (0==AV25SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV25SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV25SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV24DynamicFiltersOperator3 == 2 ) && ( ! (0==AV25SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV25SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV25SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV31TFNotaEmpenho_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] >= @AV31TFNotaEmpenho_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Codigo] >= @AV31TFNotaEmpenho_Codigo)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV32TFNotaEmpenho_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] <= @AV32TFNotaEmpenho_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Codigo] <= @AV32TFNotaEmpenho_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV35TFSaldoContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] >= @AV35TFSaldoContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] >= @AV35TFSaldoContrato_Codigo)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV36TFSaldoContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] <= @AV36TFSaldoContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] <= @AV36TFSaldoContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFNotaEmpenho_Itentificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like @lV39TFNotaEmpenho_Itentificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Itentificador] like @lV39TFNotaEmpenho_Itentificador)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFNotaEmpenho_Itentificador_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] = @AV40TFNotaEmpenho_Itentificador_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Itentificador] = @AV40TFNotaEmpenho_Itentificador_Sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV43TFNotaEmpenho_DEmissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] >= @AV43TFNotaEmpenho_DEmissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_DEmissao] >= @AV43TFNotaEmpenho_DEmissao)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV44TFNotaEmpenho_DEmissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] <= @AV44TFNotaEmpenho_DEmissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_DEmissao] <= @AV44TFNotaEmpenho_DEmissao_To)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV49TFNotaEmpenho_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Valor] >= @AV49TFNotaEmpenho_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Valor] >= @AV49TFNotaEmpenho_Valor)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV50TFNotaEmpenho_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Valor] <= @AV50TFNotaEmpenho_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Valor] <= @AV50TFNotaEmpenho_Valor_To)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV53TFNotaEmpenho_Qtd) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] >= @AV53TFNotaEmpenho_Qtd)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Qtd] >= @AV53TFNotaEmpenho_Qtd)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV54TFNotaEmpenho_Qtd_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] <= @AV54TFNotaEmpenho_Qtd_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Qtd] <= @AV54TFNotaEmpenho_Qtd_To)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV57TFNotaEmpenho_SaldoAnt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] >= @AV57TFNotaEmpenho_SaldoAnt)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoAnt] >= @AV57TFNotaEmpenho_SaldoAnt)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV58TFNotaEmpenho_SaldoAnt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] <= @AV58TFNotaEmpenho_SaldoAnt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoAnt] <= @AV58TFNotaEmpenho_SaldoAnt_To)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV61TFNotaEmpenho_SaldoPos) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] >= @AV61TFNotaEmpenho_SaldoPos)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoPos] >= @AV61TFNotaEmpenho_SaldoPos)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV62TFNotaEmpenho_SaldoPos_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] <= @AV62TFNotaEmpenho_SaldoPos_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoPos] <= @AV62TFNotaEmpenho_SaldoPos_To)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV65TFNotaEmpenho_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Ativo] = 1)";
            }
         }
         if ( AV65TFNotaEmpenho_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00M92(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (short)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (bool)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] );
               case 1 :
                     return conditional_H00M93(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (short)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (bool)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00M92 ;
          prmH00M92 = new Object[] {
          new Object[] {"@AV17SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV40TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV43TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV44TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV49TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV50TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV53TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV54TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV57TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV58TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV61TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV62TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00M93 ;
          prmH00M93 = new Object[] {
          new Object[] {"@AV17SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV40TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV43TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV44TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV49TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV50TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV53TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV54TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV57TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV58TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV61TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV62TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00M92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M92,11,0,true,false )
             ,new CursorDef("H00M93", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M93,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                return;
       }
    }

 }

}
