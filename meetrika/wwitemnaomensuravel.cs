/*
               File: WWItemNaoMensuravel
        Description:  Item N�o Mensuravel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:46:7.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwitemnaomensuravel : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwitemnaomensuravel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwitemnaomensuravel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbItemNaoMensuravel_Tipo = new GXCombobox();
         chkItemNaoMensuravel_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_91 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_91_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_91_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV43ItemNaoMensuravel_AreaTrabalhoDes1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ItemNaoMensuravel_AreaTrabalhoDes1", AV43ItemNaoMensuravel_AreaTrabalhoDes1);
               AV18ReferenciaINM_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ReferenciaINM_Descricao1", AV18ReferenciaINM_Descricao1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV44ItemNaoMensuravel_AreaTrabalhoDes2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ItemNaoMensuravel_AreaTrabalhoDes2", AV44ItemNaoMensuravel_AreaTrabalhoDes2);
               AV23ReferenciaINM_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ReferenciaINM_Descricao2", AV23ReferenciaINM_Descricao2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV45ItemNaoMensuravel_AreaTrabalhoDes3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ItemNaoMensuravel_AreaTrabalhoDes3", AV45ItemNaoMensuravel_AreaTrabalhoDes3);
               AV28ReferenciaINM_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ReferenciaINM_Descricao3", AV28ReferenciaINM_Descricao3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV49TFItemNaoMensuravel_Codigo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFItemNaoMensuravel_Codigo", AV49TFItemNaoMensuravel_Codigo);
               AV50TFItemNaoMensuravel_Codigo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFItemNaoMensuravel_Codigo_Sel", AV50TFItemNaoMensuravel_Codigo_Sel);
               AV53TFItemNaoMensuravel_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFItemNaoMensuravel_Descricao", AV53TFItemNaoMensuravel_Descricao);
               AV54TFItemNaoMensuravel_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFItemNaoMensuravel_Descricao_Sel", AV54TFItemNaoMensuravel_Descricao_Sel);
               AV57TFItemNaoMensuravel_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5)));
               AV58TFItemNaoMensuravel_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5)));
               AV61TFReferenciaINM_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFReferenciaINM_Descricao", AV61TFReferenciaINM_Descricao);
               AV62TFReferenciaINM_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFReferenciaINM_Descricao_Sel", AV62TFReferenciaINM_Descricao_Sel);
               AV69TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0));
               AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace", AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace);
               AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace", AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace);
               AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace", AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace);
               AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace", AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace);
               AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace", AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace);
               AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace", AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV66TFItemNaoMensuravel_Tipo_Sels);
               AV103Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A715ItemNaoMensuravel_Codigo = GetNextPar( );
               A709ReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAE72( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTE72( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311746759");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwitemnaomensuravel.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES1", AV43ItemNaoMensuravel_AreaTrabalhoDes1);
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIAINM_DESCRICAO1", AV18ReferenciaINM_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES2", AV44ItemNaoMensuravel_AreaTrabalhoDes2);
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIAINM_DESCRICAO2", AV23ReferenciaINM_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES3", AV45ItemNaoMensuravel_AreaTrabalhoDes3);
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIAINM_DESCRICAO3", AV28ReferenciaINM_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, "GXH_vTFITEMNAOMENSURAVEL_CODIGO_SEL", StringUtil.RTrim( AV50TFItemNaoMensuravel_Codigo_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFITEMNAOMENSURAVEL_DESCRICAO", AV53TFItemNaoMensuravel_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFITEMNAOMENSURAVEL_DESCRICAO_SEL", AV54TFItemNaoMensuravel_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFITEMNAOMENSURAVEL_VALOR", StringUtil.LTrim( StringUtil.NToC( AV57TFItemNaoMensuravel_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFITEMNAOMENSURAVEL_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV58TFItemNaoMensuravel_Valor_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_DESCRICAO", AV61TFReferenciaINM_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_DESCRICAO_SEL", AV62TFReferenciaINM_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFITEMNAOMENSURAVEL_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_91", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_91), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV71DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV71DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA", AV48ItemNaoMensuravel_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA", AV48ItemNaoMensuravel_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA", AV52ItemNaoMensuravel_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA", AV52ItemNaoMensuravel_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA", AV56ItemNaoMensuravel_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA", AV56ItemNaoMensuravel_ValorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIAINM_DESCRICAOTITLEFILTERDATA", AV60ReferenciaINM_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIAINM_DESCRICAOTITLEFILTERDATA", AV60ReferenciaINM_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA", AV64ItemNaoMensuravel_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA", AV64ItemNaoMensuravel_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA", AV68ItemNaoMensuravel_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA", AV68ItemNaoMensuravel_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFITEMNAOMENSURAVEL_TIPO_SELS", AV66TFItemNaoMensuravel_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFITEMNAOMENSURAVEL_TIPO_SELS", AV66TFItemNaoMensuravel_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV103Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Filtertype", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistproc", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Filtertype", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Caption", StringUtil.RTrim( Ddo_referenciainm_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_referenciainm_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Cls", StringUtil.RTrim( Ddo_referenciainm_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_referenciainm_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_referenciainm_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciainm_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciainm_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_referenciainm_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_referenciainm_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_referenciainm_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_referenciainm_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciainm_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_referenciainm_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_referenciainm_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_referenciainm_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_referenciainm_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_referenciainm_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_referenciainm_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_itemnaomensuravel_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Caption", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Tooltip", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Cls", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_itemnaomensuravel_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Sortasc", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_codigo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_itemnaomensuravel_valor_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_referenciainm_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_referenciainm_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_referenciainm_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_itemnaomensuravel_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEE72( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTE72( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwitemnaomensuravel.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWItemNaoMensuravel" ;
      }

      public override String GetPgmdesc( )
      {
         return " Item N�o Mensuravel" ;
      }

      protected void WBE70( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_E72( true) ;
         }
         else
         {
            wb_table1_2_E72( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(106, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(107, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_codigo_Internalname, StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo), StringUtil.RTrim( context.localUtil.Format( AV49TFItemNaoMensuravel_Codigo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_codigo_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_codigo_sel_Internalname, StringUtil.RTrim( AV50TFItemNaoMensuravel_Codigo_Sel), StringUtil.RTrim( context.localUtil.Format( AV50TFItemNaoMensuravel_Codigo_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_codigo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_codigo_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfitemnaomensuravel_descricao_Internalname, AV53TFItemNaoMensuravel_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavTfitemnaomensuravel_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfitemnaomensuravel_descricao_sel_Internalname, AV54TFItemNaoMensuravel_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", 0, edtavTfitemnaomensuravel_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV57TFItemNaoMensuravel_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV57TFItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV58TFItemNaoMensuravel_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV58TFItemNaoMensuravel_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_descricao_Internalname, AV61TFReferenciaINM_Descricao, StringUtil.RTrim( context.localUtil.Format( AV61TFReferenciaINM_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_descricao_sel_Internalname, AV62TFReferenciaINM_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV62TFReferenciaINM_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfitemnaomensuravel_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfitemnaomensuravel_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfitemnaomensuravel_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWItemNaoMensuravel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ITEMNAOMENSURAVEL_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", 0, edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ITEMNAOMENSURAVEL_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ITEMNAOMENSURAVEL_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIAINM_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ITEMNAOMENSURAVEL_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ITEMNAOMENSURAVEL_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWItemNaoMensuravel.htm");
         }
         wbLoad = true;
      }

      protected void STARTE72( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Item N�o Mensuravel", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPE70( ) ;
      }

      protected void WSE72( )
      {
         STARTE72( ) ;
         EVTE72( ) ;
      }

      protected void EVTE72( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11E72 */
                              E11E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12E72 */
                              E12E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13E72 */
                              E13E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_VALOR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14E72 */
                              E14E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIAINM_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15E72 */
                              E15E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16E72 */
                              E16E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ITEMNAOMENSURAVEL_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17E72 */
                              E17E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18E72 */
                              E18E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19E72 */
                              E19E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20E72 */
                              E20E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21E72 */
                              E21E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22E72 */
                              E22E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23E72 */
                              E23E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24E72 */
                              E24E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25E72 */
                              E25E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26E72 */
                              E26E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27E72 */
                              E27E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28E72 */
                              E28E72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_91_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
                              SubsflControlProps_912( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV101Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV102Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              A718ItemNaoMensuravel_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_AreaTrabalhoCod_Internalname), ",", "."));
                              A715ItemNaoMensuravel_Codigo = StringUtil.Upper( cgiGet( edtItemNaoMensuravel_Codigo_Internalname));
                              A714ItemNaoMensuravel_Descricao = cgiGet( edtItemNaoMensuravel_Descricao_Internalname);
                              A719ItemNaoMensuravel_Valor = context.localUtil.CToN( cgiGet( edtItemNaoMensuravel_Valor_Internalname), ",", ".");
                              A709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINM_Codigo_Internalname), ",", "."));
                              A710ReferenciaINM_Descricao = StringUtil.Upper( cgiGet( edtReferenciaINM_Descricao_Internalname));
                              cmbItemNaoMensuravel_Tipo.Name = cmbItemNaoMensuravel_Tipo_Internalname;
                              cmbItemNaoMensuravel_Tipo.CurrentValue = cgiGet( cmbItemNaoMensuravel_Tipo_Internalname);
                              A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cgiGet( cmbItemNaoMensuravel_Tipo_Internalname), "."));
                              A716ItemNaoMensuravel_Ativo = StringUtil.StrToBool( cgiGet( chkItemNaoMensuravel_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29E72 */
                                    E29E72 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30E72 */
                                    E30E72 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31E72 */
                                    E31E72 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Itemnaomensuravel_areatrabalhodes1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES1"), AV43ItemNaoMensuravel_AreaTrabalhoDes1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Referenciainm_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO1"), AV18ReferenciaINM_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Itemnaomensuravel_areatrabalhodes2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES2"), AV44ItemNaoMensuravel_AreaTrabalhoDes2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Referenciainm_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO2"), AV23ReferenciaINM_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Itemnaomensuravel_areatrabalhodes3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES3"), AV45ItemNaoMensuravel_AreaTrabalhoDes3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Referenciainm_descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO3"), AV28ReferenciaINM_Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfitemnaomensuravel_codigo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_CODIGO"), AV49TFItemNaoMensuravel_Codigo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfitemnaomensuravel_codigo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_CODIGO_SEL"), AV50TFItemNaoMensuravel_Codigo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfitemnaomensuravel_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_DESCRICAO"), AV53TFItemNaoMensuravel_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfitemnaomensuravel_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_DESCRICAO_SEL"), AV54TFItemNaoMensuravel_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfitemnaomensuravel_valor Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFITEMNAOMENSURAVEL_VALOR"), ",", ".") != AV57TFItemNaoMensuravel_Valor )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfitemnaomensuravel_valor_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFITEMNAOMENSURAVEL_VALOR_TO"), ",", ".") != AV58TFItemNaoMensuravel_Valor_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfreferenciainm_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO"), AV61TFReferenciaINM_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfreferenciainm_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO_SEL"), AV62TFReferenciaINM_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfitemnaomensuravel_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFITEMNAOMENSURAVEL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV69TFItemNaoMensuravel_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEE72( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAE72( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ITEMNAOMENSURAVEL_AREATRABALHODES", "Descri��o", 0);
            cmbavDynamicfiltersselector1.addItem("REFERENCIAINM_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ITEMNAOMENSURAVEL_AREATRABALHODES", "Descri��o", 0);
            cmbavDynamicfiltersselector2.addItem("REFERENCIAINM_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("ITEMNAOMENSURAVEL_AREATRABALHODES", "Descri��o", 0);
            cmbavDynamicfiltersselector3.addItem("REFERENCIAINM_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "ITEMNAOMENSURAVEL_TIPO_" + sGXsfl_91_idx;
            cmbItemNaoMensuravel_Tipo.Name = GXCCtl;
            cmbItemNaoMensuravel_Tipo.WebTags = "";
            cmbItemNaoMensuravel_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
            cmbItemNaoMensuravel_Tipo.addItem("1", "PC", 0);
            cmbItemNaoMensuravel_Tipo.addItem("2", "PF", 0);
            if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
            {
               A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
            }
            GXCCtl = "ITEMNAOMENSURAVEL_ATIVO_" + sGXsfl_91_idx;
            chkItemNaoMensuravel_Ativo.Name = GXCCtl;
            chkItemNaoMensuravel_Ativo.WebTags = "";
            chkItemNaoMensuravel_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkItemNaoMensuravel_Ativo_Internalname, "TitleCaption", chkItemNaoMensuravel_Ativo.Caption);
            chkItemNaoMensuravel_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_912( ) ;
         while ( nGXsfl_91_idx <= nRC_GXsfl_91 )
         {
            sendrow_912( ) ;
            nGXsfl_91_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_91_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV43ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                       String AV18ReferenciaINM_Descricao1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV44ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                       String AV23ReferenciaINM_Descricao2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV45ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                       String AV28ReferenciaINM_Descricao3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV49TFItemNaoMensuravel_Codigo ,
                                       String AV50TFItemNaoMensuravel_Codigo_Sel ,
                                       String AV53TFItemNaoMensuravel_Descricao ,
                                       String AV54TFItemNaoMensuravel_Descricao_Sel ,
                                       decimal AV57TFItemNaoMensuravel_Valor ,
                                       decimal AV58TFItemNaoMensuravel_Valor_To ,
                                       String AV61TFReferenciaINM_Descricao ,
                                       String AV62TFReferenciaINM_Descricao_Sel ,
                                       short AV69TFItemNaoMensuravel_Ativo_Sel ,
                                       String AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace ,
                                       String AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace ,
                                       String AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace ,
                                       String AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace ,
                                       String AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace ,
                                       String AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV66TFItemNaoMensuravel_Tipo_Sels ,
                                       String AV103Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                       String A715ItemNaoMensuravel_Codigo ,
                                       int A709ReferenciaINM_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFE72( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( A715ItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_DESCRICAO", GetSecureSignedToken( "", A714ItemNaoMensuravel_Descricao));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_DESCRICAO", A714ItemNaoMensuravel_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_VALOR", StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_TIPO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_ATIVO", GetSecureSignedToken( "", A716ItemNaoMensuravel_Ativo));
         GxWebStd.gx_hidden_field( context, "ITEMNAOMENSURAVEL_ATIVO", StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFE72( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV103Pgmname = "WWItemNaoMensuravel";
         context.Gx_err = 0;
      }

      protected void RFE72( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 91;
         /* Execute user event: E30E72 */
         E30E72 ();
         nGXsfl_91_idx = 1;
         sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
         SubsflControlProps_912( ) ;
         nGXsfl_91_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_912( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A717ItemNaoMensuravel_Tipo ,
                                                 AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                                 AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                                 AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                                 AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                                 AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                                 AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                                 AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                                 AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                                 AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                                 AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                                 AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                                 AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                                 AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                                 AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                                 AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                                 AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                                 AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                                 AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                                 AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                                 AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                                 AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                                 AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                                 AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                                 AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels.Count ,
                                                 AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                                 A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                                 A710ReferenciaINM_Descricao ,
                                                 A715ItemNaoMensuravel_Codigo ,
                                                 A714ItemNaoMensuravel_Descricao ,
                                                 A719ItemNaoMensuravel_Valor ,
                                                 A716ItemNaoMensuravel_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                                 AV38ItemNaoMensuravel_AreaTrabalhoCod },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
            lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
            lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
            lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
            lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
            lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
            lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
            lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
            lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
            lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
            lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
            lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
            lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = StringUtil.PadR( StringUtil.RTrim( AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo), 20, "%");
            lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = StringUtil.Concat( StringUtil.RTrim( AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao), "%", "");
            lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = StringUtil.Concat( StringUtil.RTrim( AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao), "%", "");
            /* Using cursor H00E72 */
            pr_default.execute(0, new Object[] {AV38ItemNaoMensuravel_AreaTrabalhoCod, lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo, AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel, lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao, AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel, AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor, AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to, lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao, AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_91_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A720ItemNaoMensuravel_AreaTrabalhoDes = H00E72_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
               n720ItemNaoMensuravel_AreaTrabalhoDes = H00E72_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
               A716ItemNaoMensuravel_Ativo = H00E72_A716ItemNaoMensuravel_Ativo[0];
               A717ItemNaoMensuravel_Tipo = H00E72_A717ItemNaoMensuravel_Tipo[0];
               A710ReferenciaINM_Descricao = H00E72_A710ReferenciaINM_Descricao[0];
               A709ReferenciaINM_Codigo = H00E72_A709ReferenciaINM_Codigo[0];
               A719ItemNaoMensuravel_Valor = H00E72_A719ItemNaoMensuravel_Valor[0];
               A714ItemNaoMensuravel_Descricao = H00E72_A714ItemNaoMensuravel_Descricao[0];
               A715ItemNaoMensuravel_Codigo = H00E72_A715ItemNaoMensuravel_Codigo[0];
               A718ItemNaoMensuravel_AreaTrabalhoCod = H00E72_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               A710ReferenciaINM_Descricao = H00E72_A710ReferenciaINM_Descricao[0];
               A720ItemNaoMensuravel_AreaTrabalhoDes = H00E72_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
               n720ItemNaoMensuravel_AreaTrabalhoDes = H00E72_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
               /* Execute user event: E31E72 */
               E31E72 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 91;
            WBE70( ) ;
         }
         nGXsfl_91_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV18ReferenciaINM_Descricao1;
         AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV23ReferenciaINM_Descricao2;
         AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV28ReferenciaINM_Descricao3;
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV49TFItemNaoMensuravel_Codigo;
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV50TFItemNaoMensuravel_Codigo_Sel;
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV53TFItemNaoMensuravel_Descricao;
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV54TFItemNaoMensuravel_Descricao_Sel;
         AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV57TFItemNaoMensuravel_Valor;
         AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV58TFItemNaoMensuravel_Valor_To;
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV61TFReferenciaINM_Descricao;
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV62TFReferenciaINM_Descricao_Sel;
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV66TFItemNaoMensuravel_Tipo_Sels;
         AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV69TFItemNaoMensuravel_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                              AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                              AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                              AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                              AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                              AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                              AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                              AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                              AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                              AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                              AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                              AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                              AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                              AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                              AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                              AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                              AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                              AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                              AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                              AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                              AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                              AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                              AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                              AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels.Count ,
                                              AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A716ItemNaoMensuravel_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                              AV38ItemNaoMensuravel_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1), "%", "");
         lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1), "%", "");
         lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2), "%", "");
         lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2), "%", "");
         lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3), "%", "");
         lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3), "%", "");
         lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = StringUtil.PadR( StringUtil.RTrim( AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo), 20, "%");
         lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = StringUtil.Concat( StringUtil.RTrim( AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao), "%", "");
         lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = StringUtil.Concat( StringUtil.RTrim( AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao), "%", "");
         /* Using cursor H00E73 */
         pr_default.execute(1, new Object[] {AV38ItemNaoMensuravel_AreaTrabalhoCod, lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1, lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1, lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2, lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2, lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3, lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3, lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo, AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel, lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao, AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel, AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor, AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to, lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao, AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel});
         GRID_nRecordCount = H00E73_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV18ReferenciaINM_Descricao1;
         AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV23ReferenciaINM_Descricao2;
         AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV28ReferenciaINM_Descricao3;
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV49TFItemNaoMensuravel_Codigo;
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV50TFItemNaoMensuravel_Codigo_Sel;
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV53TFItemNaoMensuravel_Descricao;
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV54TFItemNaoMensuravel_Descricao_Sel;
         AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV57TFItemNaoMensuravel_Valor;
         AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV58TFItemNaoMensuravel_Valor_To;
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV61TFReferenciaINM_Descricao;
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV62TFReferenciaINM_Descricao_Sel;
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV66TFItemNaoMensuravel_Tipo_Sels;
         AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV69TFItemNaoMensuravel_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV18ReferenciaINM_Descricao1;
         AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV23ReferenciaINM_Descricao2;
         AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV28ReferenciaINM_Descricao3;
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV49TFItemNaoMensuravel_Codigo;
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV50TFItemNaoMensuravel_Codigo_Sel;
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV53TFItemNaoMensuravel_Descricao;
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV54TFItemNaoMensuravel_Descricao_Sel;
         AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV57TFItemNaoMensuravel_Valor;
         AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV58TFItemNaoMensuravel_Valor_To;
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV61TFReferenciaINM_Descricao;
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV62TFReferenciaINM_Descricao_Sel;
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV66TFItemNaoMensuravel_Tipo_Sels;
         AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV69TFItemNaoMensuravel_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV18ReferenciaINM_Descricao1;
         AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV23ReferenciaINM_Descricao2;
         AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV28ReferenciaINM_Descricao3;
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV49TFItemNaoMensuravel_Codigo;
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV50TFItemNaoMensuravel_Codigo_Sel;
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV53TFItemNaoMensuravel_Descricao;
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV54TFItemNaoMensuravel_Descricao_Sel;
         AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV57TFItemNaoMensuravel_Valor;
         AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV58TFItemNaoMensuravel_Valor_To;
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV61TFReferenciaINM_Descricao;
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV62TFReferenciaINM_Descricao_Sel;
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV66TFItemNaoMensuravel_Tipo_Sels;
         AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV69TFItemNaoMensuravel_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV18ReferenciaINM_Descricao1;
         AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV23ReferenciaINM_Descricao2;
         AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV28ReferenciaINM_Descricao3;
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV49TFItemNaoMensuravel_Codigo;
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV50TFItemNaoMensuravel_Codigo_Sel;
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV53TFItemNaoMensuravel_Descricao;
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV54TFItemNaoMensuravel_Descricao_Sel;
         AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV57TFItemNaoMensuravel_Valor;
         AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV58TFItemNaoMensuravel_Valor_To;
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV61TFReferenciaINM_Descricao;
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV62TFReferenciaINM_Descricao_Sel;
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV66TFItemNaoMensuravel_Tipo_Sels;
         AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV69TFItemNaoMensuravel_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV18ReferenciaINM_Descricao1;
         AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV23ReferenciaINM_Descricao2;
         AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV28ReferenciaINM_Descricao3;
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV49TFItemNaoMensuravel_Codigo;
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV50TFItemNaoMensuravel_Codigo_Sel;
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV53TFItemNaoMensuravel_Descricao;
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV54TFItemNaoMensuravel_Descricao_Sel;
         AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV57TFItemNaoMensuravel_Valor;
         AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV58TFItemNaoMensuravel_Valor_To;
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV61TFReferenciaINM_Descricao;
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV62TFReferenciaINM_Descricao_Sel;
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV66TFItemNaoMensuravel_Tipo_Sels;
         AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV69TFItemNaoMensuravel_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPE70( )
      {
         /* Before Start, stand alone formulas. */
         AV103Pgmname = "WWItemNaoMensuravel";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E29E72 */
         E29E72 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV71DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA"), AV48ItemNaoMensuravel_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA"), AV52ItemNaoMensuravel_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA"), AV56ItemNaoMensuravel_ValorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIAINM_DESCRICAOTITLEFILTERDATA"), AV60ReferenciaINM_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA"), AV64ItemNaoMensuravel_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA"), AV68ItemNaoMensuravel_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV43ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Upper( cgiGet( edtavItemnaomensuravel_areatrabalhodes1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ItemNaoMensuravel_AreaTrabalhoDes1", AV43ItemNaoMensuravel_AreaTrabalhoDes1);
            AV18ReferenciaINM_Descricao1 = StringUtil.Upper( cgiGet( edtavReferenciainm_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ReferenciaINM_Descricao1", AV18ReferenciaINM_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV44ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Upper( cgiGet( edtavItemnaomensuravel_areatrabalhodes2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ItemNaoMensuravel_AreaTrabalhoDes2", AV44ItemNaoMensuravel_AreaTrabalhoDes2);
            AV23ReferenciaINM_Descricao2 = StringUtil.Upper( cgiGet( edtavReferenciainm_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ReferenciaINM_Descricao2", AV23ReferenciaINM_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV45ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Upper( cgiGet( edtavItemnaomensuravel_areatrabalhodes3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ItemNaoMensuravel_AreaTrabalhoDes3", AV45ItemNaoMensuravel_AreaTrabalhoDes3);
            AV28ReferenciaINM_Descricao3 = StringUtil.Upper( cgiGet( edtavReferenciainm_descricao3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ReferenciaINM_Descricao3", AV28ReferenciaINM_Descricao3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV49TFItemNaoMensuravel_Codigo = StringUtil.Upper( cgiGet( edtavTfitemnaomensuravel_codigo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFItemNaoMensuravel_Codigo", AV49TFItemNaoMensuravel_Codigo);
            AV50TFItemNaoMensuravel_Codigo_Sel = StringUtil.Upper( cgiGet( edtavTfitemnaomensuravel_codigo_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFItemNaoMensuravel_Codigo_Sel", AV50TFItemNaoMensuravel_Codigo_Sel);
            AV53TFItemNaoMensuravel_Descricao = cgiGet( edtavTfitemnaomensuravel_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFItemNaoMensuravel_Descricao", AV53TFItemNaoMensuravel_Descricao);
            AV54TFItemNaoMensuravel_Descricao_Sel = cgiGet( edtavTfitemnaomensuravel_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFItemNaoMensuravel_Descricao_Sel", AV54TFItemNaoMensuravel_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFITEMNAOMENSURAVEL_VALOR");
               GX_FocusControl = edtavTfitemnaomensuravel_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57TFItemNaoMensuravel_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5)));
            }
            else
            {
               AV57TFItemNaoMensuravel_Valor = context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFITEMNAOMENSURAVEL_VALOR_TO");
               GX_FocusControl = edtavTfitemnaomensuravel_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFItemNaoMensuravel_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5)));
            }
            else
            {
               AV58TFItemNaoMensuravel_Valor_To = context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5)));
            }
            AV61TFReferenciaINM_Descricao = StringUtil.Upper( cgiGet( edtavTfreferenciainm_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFReferenciaINM_Descricao", AV61TFReferenciaINM_Descricao);
            AV62TFReferenciaINM_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfreferenciainm_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFReferenciaINM_Descricao_Sel", AV62TFReferenciaINM_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFITEMNAOMENSURAVEL_ATIVO_SEL");
               GX_FocusControl = edtavTfitemnaomensuravel_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFItemNaoMensuravel_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0));
            }
            else
            {
               AV69TFItemNaoMensuravel_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfitemnaomensuravel_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0));
            }
            AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace", AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace);
            AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace", AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace);
            AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace", AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace);
            AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace", AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace);
            AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace", AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace);
            AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace", AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_91 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_91"), ",", "."));
            AV73GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV74GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_itemnaomensuravel_codigo_Caption = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Caption");
            Ddo_itemnaomensuravel_codigo_Tooltip = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Tooltip");
            Ddo_itemnaomensuravel_codigo_Cls = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Cls");
            Ddo_itemnaomensuravel_codigo_Filteredtext_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_set");
            Ddo_itemnaomensuravel_codigo_Selectedvalue_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_set");
            Ddo_itemnaomensuravel_codigo_Dropdownoptionstype = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortasc"));
            Ddo_itemnaomensuravel_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Includesortdsc"));
            Ddo_itemnaomensuravel_codigo_Sortedstatus = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Sortedstatus");
            Ddo_itemnaomensuravel_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Includefilter"));
            Ddo_itemnaomensuravel_codigo_Filtertype = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Filtertype");
            Ddo_itemnaomensuravel_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Filterisrange"));
            Ddo_itemnaomensuravel_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Includedatalist"));
            Ddo_itemnaomensuravel_codigo_Datalisttype = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Datalisttype");
            Ddo_itemnaomensuravel_codigo_Datalistproc = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistproc");
            Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_itemnaomensuravel_codigo_Sortasc = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Sortasc");
            Ddo_itemnaomensuravel_codigo_Sortdsc = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Sortdsc");
            Ddo_itemnaomensuravel_codigo_Loadingdata = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Loadingdata");
            Ddo_itemnaomensuravel_codigo_Cleanfilter = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Cleanfilter");
            Ddo_itemnaomensuravel_codigo_Noresultsfound = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Noresultsfound");
            Ddo_itemnaomensuravel_codigo_Searchbuttontext = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Searchbuttontext");
            Ddo_itemnaomensuravel_descricao_Caption = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Caption");
            Ddo_itemnaomensuravel_descricao_Tooltip = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Tooltip");
            Ddo_itemnaomensuravel_descricao_Cls = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cls");
            Ddo_itemnaomensuravel_descricao_Filteredtext_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_set");
            Ddo_itemnaomensuravel_descricao_Selectedvalue_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_set");
            Ddo_itemnaomensuravel_descricao_Dropdownoptionstype = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortasc"));
            Ddo_itemnaomensuravel_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includesortdsc"));
            Ddo_itemnaomensuravel_descricao_Sortedstatus = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortedstatus");
            Ddo_itemnaomensuravel_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includefilter"));
            Ddo_itemnaomensuravel_descricao_Filtertype = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filtertype");
            Ddo_itemnaomensuravel_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filterisrange"));
            Ddo_itemnaomensuravel_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Includedatalist"));
            Ddo_itemnaomensuravel_descricao_Datalisttype = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalisttype");
            Ddo_itemnaomensuravel_descricao_Datalistproc = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistproc");
            Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_itemnaomensuravel_descricao_Sortasc = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortasc");
            Ddo_itemnaomensuravel_descricao_Sortdsc = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Sortdsc");
            Ddo_itemnaomensuravel_descricao_Loadingdata = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Loadingdata");
            Ddo_itemnaomensuravel_descricao_Cleanfilter = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Cleanfilter");
            Ddo_itemnaomensuravel_descricao_Noresultsfound = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Noresultsfound");
            Ddo_itemnaomensuravel_descricao_Searchbuttontext = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Searchbuttontext");
            Ddo_itemnaomensuravel_valor_Caption = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Caption");
            Ddo_itemnaomensuravel_valor_Tooltip = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Tooltip");
            Ddo_itemnaomensuravel_valor_Cls = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Cls");
            Ddo_itemnaomensuravel_valor_Filteredtext_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_set");
            Ddo_itemnaomensuravel_valor_Filteredtextto_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_set");
            Ddo_itemnaomensuravel_valor_Dropdownoptionstype = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Dropdownoptionstype");
            Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_valor_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Includesortasc"));
            Ddo_itemnaomensuravel_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Includesortdsc"));
            Ddo_itemnaomensuravel_valor_Sortedstatus = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Sortedstatus");
            Ddo_itemnaomensuravel_valor_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Includefilter"));
            Ddo_itemnaomensuravel_valor_Filtertype = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Filtertype");
            Ddo_itemnaomensuravel_valor_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Filterisrange"));
            Ddo_itemnaomensuravel_valor_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Includedatalist"));
            Ddo_itemnaomensuravel_valor_Sortasc = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Sortasc");
            Ddo_itemnaomensuravel_valor_Sortdsc = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Sortdsc");
            Ddo_itemnaomensuravel_valor_Cleanfilter = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Cleanfilter");
            Ddo_itemnaomensuravel_valor_Rangefilterfrom = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterfrom");
            Ddo_itemnaomensuravel_valor_Rangefilterto = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Rangefilterto");
            Ddo_itemnaomensuravel_valor_Searchbuttontext = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Searchbuttontext");
            Ddo_referenciainm_descricao_Caption = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Caption");
            Ddo_referenciainm_descricao_Tooltip = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Tooltip");
            Ddo_referenciainm_descricao_Cls = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Cls");
            Ddo_referenciainm_descricao_Filteredtext_set = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_set");
            Ddo_referenciainm_descricao_Selectedvalue_set = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_set");
            Ddo_referenciainm_descricao_Dropdownoptionstype = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Dropdownoptionstype");
            Ddo_referenciainm_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_referenciainm_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includesortasc"));
            Ddo_referenciainm_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includesortdsc"));
            Ddo_referenciainm_descricao_Sortedstatus = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Sortedstatus");
            Ddo_referenciainm_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includefilter"));
            Ddo_referenciainm_descricao_Filtertype = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filtertype");
            Ddo_referenciainm_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filterisrange"));
            Ddo_referenciainm_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includedatalist"));
            Ddo_referenciainm_descricao_Datalisttype = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Datalisttype");
            Ddo_referenciainm_descricao_Datalistproc = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Datalistproc");
            Ddo_referenciainm_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciainm_descricao_Sortasc = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Sortasc");
            Ddo_referenciainm_descricao_Sortdsc = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Sortdsc");
            Ddo_referenciainm_descricao_Loadingdata = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Loadingdata");
            Ddo_referenciainm_descricao_Cleanfilter = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Cleanfilter");
            Ddo_referenciainm_descricao_Noresultsfound = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Noresultsfound");
            Ddo_referenciainm_descricao_Searchbuttontext = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Searchbuttontext");
            Ddo_itemnaomensuravel_tipo_Caption = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Caption");
            Ddo_itemnaomensuravel_tipo_Tooltip = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Tooltip");
            Ddo_itemnaomensuravel_tipo_Cls = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Cls");
            Ddo_itemnaomensuravel_tipo_Selectedvalue_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_set");
            Ddo_itemnaomensuravel_tipo_Dropdownoptionstype = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Includesortasc"));
            Ddo_itemnaomensuravel_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Includesortdsc"));
            Ddo_itemnaomensuravel_tipo_Sortedstatus = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Sortedstatus");
            Ddo_itemnaomensuravel_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Includefilter"));
            Ddo_itemnaomensuravel_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Includedatalist"));
            Ddo_itemnaomensuravel_tipo_Datalisttype = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Datalisttype");
            Ddo_itemnaomensuravel_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Allowmultipleselection"));
            Ddo_itemnaomensuravel_tipo_Datalistfixedvalues = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Datalistfixedvalues");
            Ddo_itemnaomensuravel_tipo_Sortasc = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Sortasc");
            Ddo_itemnaomensuravel_tipo_Sortdsc = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Sortdsc");
            Ddo_itemnaomensuravel_tipo_Cleanfilter = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Cleanfilter");
            Ddo_itemnaomensuravel_tipo_Searchbuttontext = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Searchbuttontext");
            Ddo_itemnaomensuravel_ativo_Caption = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Caption");
            Ddo_itemnaomensuravel_ativo_Tooltip = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Tooltip");
            Ddo_itemnaomensuravel_ativo_Cls = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Cls");
            Ddo_itemnaomensuravel_ativo_Selectedvalue_set = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_set");
            Ddo_itemnaomensuravel_ativo_Dropdownoptionstype = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Dropdownoptionstype");
            Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Titlecontrolidtoreplace");
            Ddo_itemnaomensuravel_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortasc"));
            Ddo_itemnaomensuravel_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Includesortdsc"));
            Ddo_itemnaomensuravel_ativo_Sortedstatus = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Sortedstatus");
            Ddo_itemnaomensuravel_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Includefilter"));
            Ddo_itemnaomensuravel_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Includedatalist"));
            Ddo_itemnaomensuravel_ativo_Datalisttype = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Datalisttype");
            Ddo_itemnaomensuravel_ativo_Datalistfixedvalues = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Datalistfixedvalues");
            Ddo_itemnaomensuravel_ativo_Sortasc = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Sortasc");
            Ddo_itemnaomensuravel_ativo_Sortdsc = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Sortdsc");
            Ddo_itemnaomensuravel_ativo_Cleanfilter = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Cleanfilter");
            Ddo_itemnaomensuravel_ativo_Searchbuttontext = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_itemnaomensuravel_codigo_Activeeventkey = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Activeeventkey");
            Ddo_itemnaomensuravel_codigo_Filteredtext_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Filteredtext_get");
            Ddo_itemnaomensuravel_codigo_Selectedvalue_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_CODIGO_Selectedvalue_get");
            Ddo_itemnaomensuravel_descricao_Activeeventkey = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Activeeventkey");
            Ddo_itemnaomensuravel_descricao_Filteredtext_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Filteredtext_get");
            Ddo_itemnaomensuravel_descricao_Selectedvalue_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_DESCRICAO_Selectedvalue_get");
            Ddo_itemnaomensuravel_valor_Activeeventkey = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Activeeventkey");
            Ddo_itemnaomensuravel_valor_Filteredtext_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtext_get");
            Ddo_itemnaomensuravel_valor_Filteredtextto_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_VALOR_Filteredtextto_get");
            Ddo_referenciainm_descricao_Activeeventkey = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Activeeventkey");
            Ddo_referenciainm_descricao_Filteredtext_get = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_get");
            Ddo_referenciainm_descricao_Selectedvalue_get = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_get");
            Ddo_itemnaomensuravel_tipo_Activeeventkey = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Activeeventkey");
            Ddo_itemnaomensuravel_tipo_Selectedvalue_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_TIPO_Selectedvalue_get");
            Ddo_itemnaomensuravel_ativo_Activeeventkey = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Activeeventkey");
            Ddo_itemnaomensuravel_ativo_Selectedvalue_get = cgiGet( "DDO_ITEMNAOMENSURAVEL_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES1"), AV43ItemNaoMensuravel_AreaTrabalhoDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO1"), AV18ReferenciaINM_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES2"), AV44ItemNaoMensuravel_AreaTrabalhoDes2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO2"), AV23ReferenciaINM_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vITEMNAOMENSURAVEL_AREATRABALHODES3"), AV45ItemNaoMensuravel_AreaTrabalhoDes3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO3"), AV28ReferenciaINM_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_CODIGO"), AV49TFItemNaoMensuravel_Codigo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_CODIGO_SEL"), AV50TFItemNaoMensuravel_Codigo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_DESCRICAO"), AV53TFItemNaoMensuravel_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFITEMNAOMENSURAVEL_DESCRICAO_SEL"), AV54TFItemNaoMensuravel_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFITEMNAOMENSURAVEL_VALOR"), ",", ".") != AV57TFItemNaoMensuravel_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFITEMNAOMENSURAVEL_VALOR_TO"), ",", ".") != AV58TFItemNaoMensuravel_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO"), AV61TFReferenciaINM_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO_SEL"), AV62TFReferenciaINM_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFITEMNAOMENSURAVEL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV69TFItemNaoMensuravel_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E29E72 */
         E29E72 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29E72( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "ITEMNAOMENSURAVEL_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "ITEMNAOMENSURAVEL_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "ITEMNAOMENSURAVEL_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfitemnaomensuravel_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfitemnaomensuravel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_codigo_Visible), 5, 0)));
         edtavTfitemnaomensuravel_codigo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfitemnaomensuravel_codigo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_codigo_sel_Visible), 5, 0)));
         edtavTfitemnaomensuravel_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfitemnaomensuravel_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_descricao_Visible), 5, 0)));
         edtavTfitemnaomensuravel_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfitemnaomensuravel_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_descricao_sel_Visible), 5, 0)));
         edtavTfitemnaomensuravel_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfitemnaomensuravel_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_valor_Visible), 5, 0)));
         edtavTfitemnaomensuravel_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfitemnaomensuravel_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_valor_to_Visible), 5, 0)));
         edtavTfreferenciainm_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_descricao_Visible), 5, 0)));
         edtavTfreferenciainm_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_descricao_sel_Visible), 5, 0)));
         edtavTfitemnaomensuravel_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfitemnaomensuravel_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfitemnaomensuravel_ativo_sel_Visible), 5, 0)));
         Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace);
         AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace", AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace);
         AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace", AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace);
         AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace", AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciainm_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaINM_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "TitleControlIdToReplace", Ddo_referenciainm_descricao_Titlecontrolidtoreplace);
         AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = Ddo_referenciainm_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace", AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace);
         edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_tipo_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace);
         AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace", AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ItemNaoMensuravel_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_ativo_Internalname, "TitleControlIdToReplace", Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace);
         AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace", AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace);
         edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Item N�o Mensuravel";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "C�digo", 0);
         cmbavOrderedby.addItem("2", "Descri��o", 0);
         cmbavOrderedby.addItem("3", "Valor", 0);
         cmbavOrderedby.addItem("4", "Descri��o", 0);
         cmbavOrderedby.addItem("5", "do Item", 0);
         cmbavOrderedby.addItem("6", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV71DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV71DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E30E72( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV48ItemNaoMensuravel_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ItemNaoMensuravel_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56ItemNaoMensuravel_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60ReferenciaINM_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ItemNaoMensuravel_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ItemNaoMensuravel_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtItemNaoMensuravel_Codigo_Titleformat = 2;
         edtItemNaoMensuravel_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Codigo_Internalname, "Title", edtItemNaoMensuravel_Codigo_Title);
         edtItemNaoMensuravel_Descricao_Titleformat = 2;
         edtItemNaoMensuravel_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Descricao_Internalname, "Title", edtItemNaoMensuravel_Descricao_Title);
         edtItemNaoMensuravel_Valor_Titleformat = 2;
         edtItemNaoMensuravel_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtItemNaoMensuravel_Valor_Internalname, "Title", edtItemNaoMensuravel_Valor_Title);
         edtReferenciaINM_Descricao_Titleformat = 2;
         edtReferenciaINM_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Descricao_Internalname, "Title", edtReferenciaINM_Descricao_Title);
         cmbItemNaoMensuravel_Tipo_Titleformat = 2;
         cmbItemNaoMensuravel_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Item", AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbItemNaoMensuravel_Tipo_Internalname, "Title", cmbItemNaoMensuravel_Tipo.Title.Text);
         chkItemNaoMensuravel_Ativo_Titleformat = 2;
         chkItemNaoMensuravel_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkItemNaoMensuravel_Ativo_Internalname, "Title", chkItemNaoMensuravel_Ativo.Title.Text);
         AV73GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV73GridCurrentPage), 10, 0)));
         AV74GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74GridPageCount), 10, 0)));
         AV38ItemNaoMensuravel_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = AV18ReferenciaINM_Descricao1;
         AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = AV23ReferenciaINM_Descricao2;
         AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = AV28ReferenciaINM_Descricao3;
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = AV49TFItemNaoMensuravel_Codigo;
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = AV50TFItemNaoMensuravel_Codigo_Sel;
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = AV53TFItemNaoMensuravel_Descricao;
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = AV54TFItemNaoMensuravel_Descricao_Sel;
         AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor = AV57TFItemNaoMensuravel_Valor;
         AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to = AV58TFItemNaoMensuravel_Valor_To;
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = AV61TFReferenciaINM_Descricao;
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = AV62TFReferenciaINM_Descricao_Sel;
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = AV66TFItemNaoMensuravel_Tipo_Sels;
         AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel = AV69TFItemNaoMensuravel_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48ItemNaoMensuravel_CodigoTitleFilterData", AV48ItemNaoMensuravel_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52ItemNaoMensuravel_DescricaoTitleFilterData", AV52ItemNaoMensuravel_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56ItemNaoMensuravel_ValorTitleFilterData", AV56ItemNaoMensuravel_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60ReferenciaINM_DescricaoTitleFilterData", AV60ReferenciaINM_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64ItemNaoMensuravel_TipoTitleFilterData", AV64ItemNaoMensuravel_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68ItemNaoMensuravel_AtivoTitleFilterData", AV68ItemNaoMensuravel_AtivoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11E72( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV72PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV72PageToGo) ;
         }
      }

      protected void E12E72( )
      {
         /* Ddo_itemnaomensuravel_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFItemNaoMensuravel_Codigo = Ddo_itemnaomensuravel_codigo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFItemNaoMensuravel_Codigo", AV49TFItemNaoMensuravel_Codigo);
            AV50TFItemNaoMensuravel_Codigo_Sel = Ddo_itemnaomensuravel_codigo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFItemNaoMensuravel_Codigo_Sel", AV50TFItemNaoMensuravel_Codigo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13E72( )
      {
         /* Ddo_itemnaomensuravel_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFItemNaoMensuravel_Descricao = Ddo_itemnaomensuravel_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFItemNaoMensuravel_Descricao", AV53TFItemNaoMensuravel_Descricao);
            AV54TFItemNaoMensuravel_Descricao_Sel = Ddo_itemnaomensuravel_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFItemNaoMensuravel_Descricao_Sel", AV54TFItemNaoMensuravel_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14E72( )
      {
         /* Ddo_itemnaomensuravel_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV57TFItemNaoMensuravel_Valor = NumberUtil.Val( Ddo_itemnaomensuravel_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5)));
            AV58TFItemNaoMensuravel_Valor_To = NumberUtil.Val( Ddo_itemnaomensuravel_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15E72( )
      {
         /* Ddo_referenciainm_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciainm_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFReferenciaINM_Descricao = Ddo_referenciainm_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFReferenciaINM_Descricao", AV61TFReferenciaINM_Descricao);
            AV62TFReferenciaINM_Descricao_Sel = Ddo_referenciainm_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFReferenciaINM_Descricao_Sel", AV62TFReferenciaINM_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16E72( )
      {
         /* Ddo_itemnaomensuravel_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFItemNaoMensuravel_Tipo_SelsJson = Ddo_itemnaomensuravel_tipo_Selectedvalue_get;
            AV66TFItemNaoMensuravel_Tipo_Sels.FromJSonString(StringUtil.StringReplace( AV65TFItemNaoMensuravel_Tipo_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66TFItemNaoMensuravel_Tipo_Sels", AV66TFItemNaoMensuravel_Tipo_Sels);
      }

      protected void E17E72( )
      {
         /* Ddo_itemnaomensuravel_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_itemnaomensuravel_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_itemnaomensuravel_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( Ddo_itemnaomensuravel_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E31E72( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV101Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo));
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV102Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo));
         edtItemNaoMensuravel_Descricao_Link = formatLink("viewitemnaomensuravel.aspx") + "?" + UrlEncode("" +A718ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(A715ItemNaoMensuravel_Codigo)) + "," + UrlEncode(StringUtil.RTrim(""));
         edtReferenciaINM_Descricao_Link = formatLink("viewreferenciainm.aspx") + "?" + UrlEncode("" +A709ReferenciaINM_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 91;
         }
         sendrow_912( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_91_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(91, GridRow);
         }
      }

      protected void E18E72( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E24E72( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
      }

      protected void E19E72( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25E72( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E26E72( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
      }

      protected void E20E72( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E27E72( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E21E72( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV43ItemNaoMensuravel_AreaTrabalhoDes1, AV18ReferenciaINM_Descricao1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV44ItemNaoMensuravel_AreaTrabalhoDes2, AV23ReferenciaINM_Descricao2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV45ItemNaoMensuravel_AreaTrabalhoDes3, AV28ReferenciaINM_Descricao3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV49TFItemNaoMensuravel_Codigo, AV50TFItemNaoMensuravel_Codigo_Sel, AV53TFItemNaoMensuravel_Descricao, AV54TFItemNaoMensuravel_Descricao_Sel, AV57TFItemNaoMensuravel_Valor, AV58TFItemNaoMensuravel_Valor_To, AV61TFReferenciaINM_Descricao, AV62TFReferenciaINM_Descricao_Sel, AV69TFItemNaoMensuravel_Ativo_Sel, AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace, AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace, AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace, AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace, AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace, AV66TFItemNaoMensuravel_Tipo_Sels, AV103Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A718ItemNaoMensuravel_AreaTrabalhoCod, A715ItemNaoMensuravel_Codigo, A709ReferenciaINM_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E28E72( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E22E72( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66TFItemNaoMensuravel_Tipo_Sels", AV66TFItemNaoMensuravel_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E23E72( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("itemnaomensuravel.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_itemnaomensuravel_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
         Ddo_itemnaomensuravel_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
         Ddo_itemnaomensuravel_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
         Ddo_referenciainm_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
         Ddo_itemnaomensuravel_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
         Ddo_itemnaomensuravel_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_itemnaomensuravel_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_itemnaomensuravel_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "SortedStatus", Ddo_itemnaomensuravel_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_itemnaomensuravel_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "SortedStatus", Ddo_itemnaomensuravel_valor_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_referenciainm_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_itemnaomensuravel_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_tipo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_itemnaomensuravel_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_ativo_Internalname, "SortedStatus", Ddo_itemnaomensuravel_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavItemnaomensuravel_areatrabalhodes1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavItemnaomensuravel_areatrabalhodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_areatrabalhodes1_Visible), 5, 0)));
         edtavReferenciainm_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
         {
            edtavItemnaomensuravel_areatrabalhodes1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavItemnaomensuravel_areatrabalhodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_areatrabalhodes1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
         {
            edtavReferenciainm_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavItemnaomensuravel_areatrabalhodes2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavItemnaomensuravel_areatrabalhodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_areatrabalhodes2_Visible), 5, 0)));
         edtavReferenciainm_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
         {
            edtavItemnaomensuravel_areatrabalhodes2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavItemnaomensuravel_areatrabalhodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_areatrabalhodes2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
         {
            edtavReferenciainm_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavItemnaomensuravel_areatrabalhodes3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavItemnaomensuravel_areatrabalhodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_areatrabalhodes3_Visible), 5, 0)));
         edtavReferenciainm_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
         {
            edtavItemnaomensuravel_areatrabalhodes3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavItemnaomensuravel_areatrabalhodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemnaomensuravel_areatrabalhodes3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
         {
            edtavReferenciainm_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "ITEMNAOMENSURAVEL_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV44ItemNaoMensuravel_AreaTrabalhoDes2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ItemNaoMensuravel_AreaTrabalhoDes2", AV44ItemNaoMensuravel_AreaTrabalhoDes2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "ITEMNAOMENSURAVEL_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV45ItemNaoMensuravel_AreaTrabalhoDes3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ItemNaoMensuravel_AreaTrabalhoDes3", AV45ItemNaoMensuravel_AreaTrabalhoDes3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV49TFItemNaoMensuravel_Codigo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFItemNaoMensuravel_Codigo", AV49TFItemNaoMensuravel_Codigo);
         Ddo_itemnaomensuravel_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_codigo_Filteredtext_set);
         AV50TFItemNaoMensuravel_Codigo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFItemNaoMensuravel_Codigo_Sel", AV50TFItemNaoMensuravel_Codigo_Sel);
         Ddo_itemnaomensuravel_codigo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_codigo_Selectedvalue_set);
         AV53TFItemNaoMensuravel_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFItemNaoMensuravel_Descricao", AV53TFItemNaoMensuravel_Descricao);
         Ddo_itemnaomensuravel_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_descricao_Filteredtext_set);
         AV54TFItemNaoMensuravel_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFItemNaoMensuravel_Descricao_Sel", AV54TFItemNaoMensuravel_Descricao_Sel);
         Ddo_itemnaomensuravel_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_descricao_Selectedvalue_set);
         AV57TFItemNaoMensuravel_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5)));
         Ddo_itemnaomensuravel_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_valor_Filteredtext_set);
         AV58TFItemNaoMensuravel_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5)));
         Ddo_itemnaomensuravel_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredTextTo_set", Ddo_itemnaomensuravel_valor_Filteredtextto_set);
         AV61TFReferenciaINM_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFReferenciaINM_Descricao", AV61TFReferenciaINM_Descricao);
         Ddo_referenciainm_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "FilteredText_set", Ddo_referenciainm_descricao_Filteredtext_set);
         AV62TFReferenciaINM_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFReferenciaINM_Descricao_Sel", AV62TFReferenciaINM_Descricao_Sel);
         Ddo_referenciainm_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SelectedValue_set", Ddo_referenciainm_descricao_Selectedvalue_set);
         AV66TFItemNaoMensuravel_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_itemnaomensuravel_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_tipo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_tipo_Selectedvalue_set);
         AV69TFItemNaoMensuravel_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0));
         Ddo_itemnaomensuravel_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_ativo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "ITEMNAOMENSURAVEL_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV43ItemNaoMensuravel_AreaTrabalhoDes1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ItemNaoMensuravel_AreaTrabalhoDes1", AV43ItemNaoMensuravel_AreaTrabalhoDes1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV103Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV103Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV103Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV104GXV1 = 1;
         while ( AV104GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV104GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               AV49TFItemNaoMensuravel_Codigo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFItemNaoMensuravel_Codigo", AV49TFItemNaoMensuravel_Codigo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo)) )
               {
                  Ddo_itemnaomensuravel_codigo_Filteredtext_set = AV49TFItemNaoMensuravel_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_codigo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO_SEL") == 0 )
            {
               AV50TFItemNaoMensuravel_Codigo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFItemNaoMensuravel_Codigo_Sel", AV50TFItemNaoMensuravel_Codigo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFItemNaoMensuravel_Codigo_Sel)) )
               {
                  Ddo_itemnaomensuravel_codigo_Selectedvalue_set = AV50TFItemNaoMensuravel_Codigo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_codigo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_codigo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV53TFItemNaoMensuravel_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFItemNaoMensuravel_Descricao", AV53TFItemNaoMensuravel_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao)) )
               {
                  Ddo_itemnaomensuravel_descricao_Filteredtext_set = AV53TFItemNaoMensuravel_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO_SEL") == 0 )
            {
               AV54TFItemNaoMensuravel_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFItemNaoMensuravel_Descricao_Sel", AV54TFItemNaoMensuravel_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFItemNaoMensuravel_Descricao_Sel)) )
               {
                  Ddo_itemnaomensuravel_descricao_Selectedvalue_set = AV54TFItemNaoMensuravel_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_descricao_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_VALOR") == 0 )
            {
               AV57TFItemNaoMensuravel_Valor = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFItemNaoMensuravel_Valor", StringUtil.LTrim( StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5)));
               AV58TFItemNaoMensuravel_Valor_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFItemNaoMensuravel_Valor_To", StringUtil.LTrim( StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV57TFItemNaoMensuravel_Valor) )
               {
                  Ddo_itemnaomensuravel_valor_Filteredtext_set = StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredText_set", Ddo_itemnaomensuravel_valor_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV58TFItemNaoMensuravel_Valor_To) )
               {
                  Ddo_itemnaomensuravel_valor_Filteredtextto_set = StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_valor_Internalname, "FilteredTextTo_set", Ddo_itemnaomensuravel_valor_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO") == 0 )
            {
               AV61TFReferenciaINM_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFReferenciaINM_Descricao", AV61TFReferenciaINM_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFReferenciaINM_Descricao)) )
               {
                  Ddo_referenciainm_descricao_Filteredtext_set = AV61TFReferenciaINM_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "FilteredText_set", Ddo_referenciainm_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO_SEL") == 0 )
            {
               AV62TFReferenciaINM_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFReferenciaINM_Descricao_Sel", AV62TFReferenciaINM_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFReferenciaINM_Descricao_Sel)) )
               {
                  Ddo_referenciainm_descricao_Selectedvalue_set = AV62TFReferenciaINM_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SelectedValue_set", Ddo_referenciainm_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_TIPO_SEL") == 0 )
            {
               AV65TFItemNaoMensuravel_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV66TFItemNaoMensuravel_Tipo_Sels.FromJSonString(AV65TFItemNaoMensuravel_Tipo_SelsJson);
               if ( ! ( AV66TFItemNaoMensuravel_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_itemnaomensuravel_tipo_Selectedvalue_set = AV65TFItemNaoMensuravel_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_tipo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_ATIVO_SEL") == 0 )
            {
               AV69TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFItemNaoMensuravel_Ativo_Sel", StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0));
               if ( ! (0==AV69TFItemNaoMensuravel_Ativo_Sel) )
               {
                  Ddo_itemnaomensuravel_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_itemnaomensuravel_ativo_Internalname, "SelectedValue_set", Ddo_itemnaomensuravel_ativo_Selectedvalue_set);
               }
            }
            AV104GXV1 = (int)(AV104GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV43ItemNaoMensuravel_AreaTrabalhoDes1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ItemNaoMensuravel_AreaTrabalhoDes1", AV43ItemNaoMensuravel_AreaTrabalhoDes1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18ReferenciaINM_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ReferenciaINM_Descricao1", AV18ReferenciaINM_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV44ItemNaoMensuravel_AreaTrabalhoDes2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ItemNaoMensuravel_AreaTrabalhoDes2", AV44ItemNaoMensuravel_AreaTrabalhoDes2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23ReferenciaINM_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ReferenciaINM_Descricao2", AV23ReferenciaINM_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV45ItemNaoMensuravel_AreaTrabalhoDes3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ItemNaoMensuravel_AreaTrabalhoDes3", AV45ItemNaoMensuravel_AreaTrabalhoDes3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV28ReferenciaINM_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ReferenciaINM_Descricao3", AV28ReferenciaINM_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV103Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFItemNaoMensuravel_Codigo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFItemNaoMensuravel_Codigo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFItemNaoMensuravel_Codigo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_CODIGO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFItemNaoMensuravel_Codigo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFItemNaoMensuravel_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFItemNaoMensuravel_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFItemNaoMensuravel_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV54TFItemNaoMensuravel_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV57TFItemNaoMensuravel_Valor) && (Convert.ToDecimal(0)==AV58TFItemNaoMensuravel_Valor_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_VALOR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV57TFItemNaoMensuravel_Valor, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV58TFItemNaoMensuravel_Valor_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFReferenciaINM_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFReferenciaINM_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFReferenciaINM_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV62TFReferenciaINM_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV66TFItemNaoMensuravel_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFItemNaoMensuravel_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV69TFItemNaoMensuravel_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFITEMNAOMENSURAVEL_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV69TFItemNaoMensuravel_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV103Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV43ItemNaoMensuravel_AreaTrabalhoDes1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV43ItemNaoMensuravel_AreaTrabalhoDes1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18ReferenciaINM_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18ReferenciaINM_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ItemNaoMensuravel_AreaTrabalhoDes2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV44ItemNaoMensuravel_AreaTrabalhoDes2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23ReferenciaINM_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23ReferenciaINM_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ItemNaoMensuravel_AreaTrabalhoDes3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV45ItemNaoMensuravel_AreaTrabalhoDes3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ReferenciaINM_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28ReferenciaINM_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV103Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ItemNaoMensuravel";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_E72( true) ;
         }
         else
         {
            wb_table2_8_E72( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_85_E72( true) ;
         }
         else
         {
            wb_table3_85_E72( false) ;
         }
         return  ;
      }

      protected void wb_table3_85_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_E72e( true) ;
         }
         else
         {
            wb_table1_2_E72e( false) ;
         }
      }

      protected void wb_table3_85_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_88_E72( true) ;
         }
         else
         {
            wb_table4_88_E72( false) ;
         }
         return  ;
      }

      protected void wb_table4_88_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_85_E72e( true) ;
         }
         else
         {
            wb_table3_85_E72e( false) ;
         }
      }

      protected void wb_table4_88_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"91\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtItemNaoMensuravel_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtItemNaoMensuravel_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtItemNaoMensuravel_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtItemNaoMensuravel_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtItemNaoMensuravel_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtItemNaoMensuravel_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtItemNaoMensuravel_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtItemNaoMensuravel_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtItemNaoMensuravel_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Guia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaINM_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaINM_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaINM_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbItemNaoMensuravel_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbItemNaoMensuravel_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbItemNaoMensuravel_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkItemNaoMensuravel_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkItemNaoMensuravel_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkItemNaoMensuravel_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A715ItemNaoMensuravel_Codigo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtItemNaoMensuravel_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtItemNaoMensuravel_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A714ItemNaoMensuravel_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtItemNaoMensuravel_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtItemNaoMensuravel_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtItemNaoMensuravel_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtItemNaoMensuravel_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtItemNaoMensuravel_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A710ReferenciaINM_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaINM_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINM_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtReferenciaINM_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbItemNaoMensuravel_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbItemNaoMensuravel_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkItemNaoMensuravel_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkItemNaoMensuravel_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 91 )
         {
            wbEnd = 0;
            nRC_GXsfl_91 = (short)(nGXsfl_91_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_88_E72e( true) ;
         }
         else
         {
            wb_table4_88_E72e( false) ;
         }
      }

      protected void wb_table2_8_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblItemnaomensuraveltitle_Internalname, "Item N�o Mensuravel", "", "", lblItemnaomensuraveltitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_E72( true) ;
         }
         else
         {
            wb_table5_13_E72( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWItemNaoMensuravel.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_E72( true) ;
         }
         else
         {
            wb_table6_23_E72( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_E72e( true) ;
         }
         else
         {
            wb_table2_8_E72e( false) ;
         }
      }

      protected void wb_table6_23_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_E72( true) ;
         }
         else
         {
            wb_table7_28_E72( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_E72e( true) ;
         }
         else
         {
            wb_table6_23_E72e( false) ;
         }
      }

      protected void wb_table7_28_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWItemNaoMensuravel.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_E72( true) ;
         }
         else
         {
            wb_table8_37_E72( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWItemNaoMensuravel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWItemNaoMensuravel.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_55_E72( true) ;
         }
         else
         {
            wb_table9_55_E72( false) ;
         }
         return  ;
      }

      protected void wb_table9_55_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWItemNaoMensuravel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WWItemNaoMensuravel.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_73_E72( true) ;
         }
         else
         {
            wb_table10_73_E72( false) ;
         }
         return  ;
      }

      protected void wb_table10_73_E72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_E72e( true) ;
         }
         else
         {
            wb_table7_28_E72e( false) ;
         }
      }

      protected void wb_table10_73_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_WWItemNaoMensuravel.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavItemnaomensuravel_areatrabalhodes3_Internalname, AV45ItemNaoMensuravel_AreaTrabalhoDes3, StringUtil.RTrim( context.localUtil.Format( AV45ItemNaoMensuravel_AreaTrabalhoDes3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavItemnaomensuravel_areatrabalhodes3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavItemnaomensuravel_areatrabalhodes3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciainm_descricao3_Internalname, AV28ReferenciaINM_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV28ReferenciaINM_Descricao3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciainm_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciainm_descricao3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_73_E72e( true) ;
         }
         else
         {
            wb_table10_73_E72e( false) ;
         }
      }

      protected void wb_table9_55_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWItemNaoMensuravel.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavItemnaomensuravel_areatrabalhodes2_Internalname, AV44ItemNaoMensuravel_AreaTrabalhoDes2, StringUtil.RTrim( context.localUtil.Format( AV44ItemNaoMensuravel_AreaTrabalhoDes2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavItemnaomensuravel_areatrabalhodes2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavItemnaomensuravel_areatrabalhodes2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciainm_descricao2_Internalname, AV23ReferenciaINM_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV23ReferenciaINM_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciainm_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciainm_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_55_E72e( true) ;
         }
         else
         {
            wb_table9_55_E72e( false) ;
         }
      }

      protected void wb_table8_37_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWItemNaoMensuravel.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavItemnaomensuravel_areatrabalhodes1_Internalname, AV43ItemNaoMensuravel_AreaTrabalhoDes1, StringUtil.RTrim( context.localUtil.Format( AV43ItemNaoMensuravel_AreaTrabalhoDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavItemnaomensuravel_areatrabalhodes1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavItemnaomensuravel_areatrabalhodes1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciainm_descricao1_Internalname, AV18ReferenciaINM_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV18ReferenciaINM_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciainm_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciainm_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_E72e( true) ;
         }
         else
         {
            wb_table8_37_E72e( false) ;
         }
      }

      protected void wb_table5_13_E72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_E72e( true) ;
         }
         else
         {
            wb_table5_13_E72e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAE72( ) ;
         WSE72( ) ;
         WEE72( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117461675");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwitemnaomensuravel.js", "?20203117461676");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_912( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_91_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_91_idx;
         edtItemNaoMensuravel_AreaTrabalhoCod_Internalname = "ITEMNAOMENSURAVEL_AREATRABALHOCOD_"+sGXsfl_91_idx;
         edtItemNaoMensuravel_Codigo_Internalname = "ITEMNAOMENSURAVEL_CODIGO_"+sGXsfl_91_idx;
         edtItemNaoMensuravel_Descricao_Internalname = "ITEMNAOMENSURAVEL_DESCRICAO_"+sGXsfl_91_idx;
         edtItemNaoMensuravel_Valor_Internalname = "ITEMNAOMENSURAVEL_VALOR_"+sGXsfl_91_idx;
         edtReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO_"+sGXsfl_91_idx;
         edtReferenciaINM_Descricao_Internalname = "REFERENCIAINM_DESCRICAO_"+sGXsfl_91_idx;
         cmbItemNaoMensuravel_Tipo_Internalname = "ITEMNAOMENSURAVEL_TIPO_"+sGXsfl_91_idx;
         chkItemNaoMensuravel_Ativo_Internalname = "ITEMNAOMENSURAVEL_ATIVO_"+sGXsfl_91_idx;
      }

      protected void SubsflControlProps_fel_912( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_91_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_91_fel_idx;
         edtItemNaoMensuravel_AreaTrabalhoCod_Internalname = "ITEMNAOMENSURAVEL_AREATRABALHOCOD_"+sGXsfl_91_fel_idx;
         edtItemNaoMensuravel_Codigo_Internalname = "ITEMNAOMENSURAVEL_CODIGO_"+sGXsfl_91_fel_idx;
         edtItemNaoMensuravel_Descricao_Internalname = "ITEMNAOMENSURAVEL_DESCRICAO_"+sGXsfl_91_fel_idx;
         edtItemNaoMensuravel_Valor_Internalname = "ITEMNAOMENSURAVEL_VALOR_"+sGXsfl_91_fel_idx;
         edtReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO_"+sGXsfl_91_fel_idx;
         edtReferenciaINM_Descricao_Internalname = "REFERENCIAINM_DESCRICAO_"+sGXsfl_91_fel_idx;
         cmbItemNaoMensuravel_Tipo_Internalname = "ITEMNAOMENSURAVEL_TIPO_"+sGXsfl_91_fel_idx;
         chkItemNaoMensuravel_Ativo_Internalname = "ITEMNAOMENSURAVEL_ATIVO_"+sGXsfl_91_fel_idx;
      }

      protected void sendrow_912( )
      {
         SubsflControlProps_912( ) ;
         WBE70( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_91_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_91_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_91_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV101Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV101Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV102Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV102Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_Codigo_Internalname,StringUtil.RTrim( A715ItemNaoMensuravel_Codigo),StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"CodigoINM20",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_Descricao_Internalname,(String)A714ItemNaoMensuravel_Descricao,(String)A714ItemNaoMensuravel_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtItemNaoMensuravel_Descricao_Link,(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)91,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga2M",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtItemNaoMensuravel_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A719ItemNaoMensuravel_Valor, 18, 5, ",", "")),context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtItemNaoMensuravel_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINM_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINM_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINM_Descricao_Internalname,(String)A710ReferenciaINM_Descricao,StringUtil.RTrim( context.localUtil.Format( A710ReferenciaINM_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtReferenciaINM_Descricao_Link,(String)"",(String)"",(String)"",(String)edtReferenciaINM_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_91_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ITEMNAOMENSURAVEL_TIPO_" + sGXsfl_91_idx;
               cmbItemNaoMensuravel_Tipo.Name = GXCCtl;
               cmbItemNaoMensuravel_Tipo.WebTags = "";
               cmbItemNaoMensuravel_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "(Nenhum)", 0);
               cmbItemNaoMensuravel_Tipo.addItem("1", "PC", 0);
               cmbItemNaoMensuravel_Tipo.addItem("2", "PF", 0);
               if ( cmbItemNaoMensuravel_Tipo.ItemCount > 0 )
               {
                  A717ItemNaoMensuravel_Tipo = (short)(NumberUtil.Val( cmbItemNaoMensuravel_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbItemNaoMensuravel_Tipo,(String)cmbItemNaoMensuravel_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0)),(short)1,(String)cmbItemNaoMensuravel_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbItemNaoMensuravel_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A717ItemNaoMensuravel_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbItemNaoMensuravel_Tipo_Internalname, "Values", (String)(cmbItemNaoMensuravel_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkItemNaoMensuravel_Ativo_Internalname,StringUtil.BoolToStr( A716ItemNaoMensuravel_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_AREATRABALHOCOD"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( (decimal)(A718ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_CODIGO"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_DESCRICAO"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, A714ItemNaoMensuravel_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_VALOR"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( A719ItemNaoMensuravel_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_CODIGO"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_TIPO"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( (decimal)(A717ItemNaoMensuravel_Tipo), "Z9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_ATIVO"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, A716ItemNaoMensuravel_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_91_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_91_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         /* End function sendrow_912 */
      }

      protected void init_default_properties( )
      {
         lblItemnaomensuraveltitle_Internalname = "ITEMNAOMENSURAVELTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavItemnaomensuravel_areatrabalhodes1_Internalname = "vITEMNAOMENSURAVEL_AREATRABALHODES1";
         edtavReferenciainm_descricao1_Internalname = "vREFERENCIAINM_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavItemnaomensuravel_areatrabalhodes2_Internalname = "vITEMNAOMENSURAVEL_AREATRABALHODES2";
         edtavReferenciainm_descricao2_Internalname = "vREFERENCIAINM_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavItemnaomensuravel_areatrabalhodes3_Internalname = "vITEMNAOMENSURAVEL_AREATRABALHODES3";
         edtavReferenciainm_descricao3_Internalname = "vREFERENCIAINM_DESCRICAO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtItemNaoMensuravel_AreaTrabalhoCod_Internalname = "ITEMNAOMENSURAVEL_AREATRABALHOCOD";
         edtItemNaoMensuravel_Codigo_Internalname = "ITEMNAOMENSURAVEL_CODIGO";
         edtItemNaoMensuravel_Descricao_Internalname = "ITEMNAOMENSURAVEL_DESCRICAO";
         edtItemNaoMensuravel_Valor_Internalname = "ITEMNAOMENSURAVEL_VALOR";
         edtReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO";
         edtReferenciaINM_Descricao_Internalname = "REFERENCIAINM_DESCRICAO";
         cmbItemNaoMensuravel_Tipo_Internalname = "ITEMNAOMENSURAVEL_TIPO";
         chkItemNaoMensuravel_Ativo_Internalname = "ITEMNAOMENSURAVEL_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfitemnaomensuravel_codigo_Internalname = "vTFITEMNAOMENSURAVEL_CODIGO";
         edtavTfitemnaomensuravel_codigo_sel_Internalname = "vTFITEMNAOMENSURAVEL_CODIGO_SEL";
         edtavTfitemnaomensuravel_descricao_Internalname = "vTFITEMNAOMENSURAVEL_DESCRICAO";
         edtavTfitemnaomensuravel_descricao_sel_Internalname = "vTFITEMNAOMENSURAVEL_DESCRICAO_SEL";
         edtavTfitemnaomensuravel_valor_Internalname = "vTFITEMNAOMENSURAVEL_VALOR";
         edtavTfitemnaomensuravel_valor_to_Internalname = "vTFITEMNAOMENSURAVEL_VALOR_TO";
         edtavTfreferenciainm_descricao_Internalname = "vTFREFERENCIAINM_DESCRICAO";
         edtavTfreferenciainm_descricao_sel_Internalname = "vTFREFERENCIAINM_DESCRICAO_SEL";
         edtavTfitemnaomensuravel_ativo_sel_Internalname = "vTFITEMNAOMENSURAVEL_ATIVO_SEL";
         Ddo_itemnaomensuravel_codigo_Internalname = "DDO_ITEMNAOMENSURAVEL_CODIGO";
         edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname = "vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_descricao_Internalname = "DDO_ITEMNAOMENSURAVEL_DESCRICAO";
         edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname = "vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_valor_Internalname = "DDO_ITEMNAOMENSURAVEL_VALOR";
         edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname = "vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE";
         Ddo_referenciainm_descricao_Internalname = "DDO_REFERENCIAINM_DESCRICAO";
         edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_tipo_Internalname = "DDO_ITEMNAOMENSURAVEL_TIPO";
         edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname = "vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_itemnaomensuravel_ativo_Internalname = "DDO_ITEMNAOMENSURAVEL_ATIVO";
         edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname = "vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbItemNaoMensuravel_Tipo_Jsonclick = "";
         edtReferenciaINM_Descricao_Jsonclick = "";
         edtReferenciaINM_Codigo_Jsonclick = "";
         edtItemNaoMensuravel_Valor_Jsonclick = "";
         edtItemNaoMensuravel_Descricao_Jsonclick = "";
         edtItemNaoMensuravel_Codigo_Jsonclick = "";
         edtItemNaoMensuravel_AreaTrabalhoCod_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavReferenciainm_descricao1_Jsonclick = "";
         edtavItemnaomensuravel_areatrabalhodes1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavReferenciainm_descricao2_Jsonclick = "";
         edtavItemnaomensuravel_areatrabalhodes2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavReferenciainm_descricao3_Jsonclick = "";
         edtavItemnaomensuravel_areatrabalhodes3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtReferenciaINM_Descricao_Link = "";
         edtItemNaoMensuravel_Descricao_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkItemNaoMensuravel_Ativo_Titleformat = 0;
         cmbItemNaoMensuravel_Tipo_Titleformat = 0;
         edtReferenciaINM_Descricao_Titleformat = 0;
         edtItemNaoMensuravel_Valor_Titleformat = 0;
         edtItemNaoMensuravel_Descricao_Titleformat = 0;
         edtItemNaoMensuravel_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavReferenciainm_descricao3_Visible = 1;
         edtavItemnaomensuravel_areatrabalhodes3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavReferenciainm_descricao2_Visible = 1;
         edtavItemnaomensuravel_areatrabalhodes2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavReferenciainm_descricao1_Visible = 1;
         edtavItemnaomensuravel_areatrabalhodes1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         chkItemNaoMensuravel_Ativo.Title.Text = "Ativo";
         cmbItemNaoMensuravel_Tipo.Title.Text = "do Item";
         edtReferenciaINM_Descricao_Title = "Descri��o";
         edtItemNaoMensuravel_Valor_Title = "Valor";
         edtItemNaoMensuravel_Descricao_Title = "Descri��o";
         edtItemNaoMensuravel_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkItemNaoMensuravel_Ativo.Caption = "";
         edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfitemnaomensuravel_ativo_sel_Jsonclick = "";
         edtavTfitemnaomensuravel_ativo_sel_Visible = 1;
         edtavTfreferenciainm_descricao_sel_Jsonclick = "";
         edtavTfreferenciainm_descricao_sel_Visible = 1;
         edtavTfreferenciainm_descricao_Jsonclick = "";
         edtavTfreferenciainm_descricao_Visible = 1;
         edtavTfitemnaomensuravel_valor_to_Jsonclick = "";
         edtavTfitemnaomensuravel_valor_to_Visible = 1;
         edtavTfitemnaomensuravel_valor_Jsonclick = "";
         edtavTfitemnaomensuravel_valor_Visible = 1;
         edtavTfitemnaomensuravel_descricao_sel_Visible = 1;
         edtavTfitemnaomensuravel_descricao_Visible = 1;
         edtavTfitemnaomensuravel_codigo_sel_Jsonclick = "";
         edtavTfitemnaomensuravel_codigo_sel_Visible = 1;
         edtavTfitemnaomensuravel_codigo_Jsonclick = "";
         edtavTfitemnaomensuravel_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_itemnaomensuravel_ativo_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_itemnaomensuravel_ativo_Datalisttype = "FixedValues";
         Ddo_itemnaomensuravel_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_ativo_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_ativo_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_ativo_Caption = "";
         Ddo_itemnaomensuravel_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_itemnaomensuravel_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_tipo_Datalistfixedvalues = "1:PC,2:PF";
         Ddo_itemnaomensuravel_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Datalisttype = "FixedValues";
         Ddo_itemnaomensuravel_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_tipo_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_tipo_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_tipo_Caption = "";
         Ddo_referenciainm_descricao_Searchbuttontext = "Pesquisar";
         Ddo_referenciainm_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciainm_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciainm_descricao_Loadingdata = "Carregando dados...";
         Ddo_referenciainm_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciainm_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_referenciainm_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_referenciainm_descricao_Datalistproc = "GetWWItemNaoMensuravelFilterData";
         Ddo_referenciainm_descricao_Datalisttype = "Dynamic";
         Ddo_referenciainm_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_referenciainm_descricao_Filtertype = "Character";
         Ddo_referenciainm_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Titlecontrolidtoreplace = "";
         Ddo_referenciainm_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciainm_descricao_Cls = "ColumnSettings";
         Ddo_referenciainm_descricao_Tooltip = "Op��es";
         Ddo_referenciainm_descricao_Caption = "";
         Ddo_itemnaomensuravel_valor_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_valor_Rangefilterto = "At�";
         Ddo_itemnaomensuravel_valor_Rangefilterfrom = "Desde";
         Ddo_itemnaomensuravel_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_valor_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Filtertype = "Numeric";
         Ddo_itemnaomensuravel_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_valor_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_valor_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_valor_Caption = "";
         Ddo_itemnaomensuravel_descricao_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_itemnaomensuravel_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_descricao_Loadingdata = "Carregando dados...";
         Ddo_itemnaomensuravel_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_itemnaomensuravel_descricao_Datalistproc = "GetWWItemNaoMensuravelFilterData";
         Ddo_itemnaomensuravel_descricao_Datalisttype = "Dynamic";
         Ddo_itemnaomensuravel_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_descricao_Filtertype = "Character";
         Ddo_itemnaomensuravel_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_descricao_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_descricao_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_descricao_Caption = "";
         Ddo_itemnaomensuravel_codigo_Searchbuttontext = "Pesquisar";
         Ddo_itemnaomensuravel_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_itemnaomensuravel_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_itemnaomensuravel_codigo_Loadingdata = "Carregando dados...";
         Ddo_itemnaomensuravel_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_itemnaomensuravel_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_itemnaomensuravel_codigo_Datalistproc = "GetWWItemNaoMensuravelFilterData";
         Ddo_itemnaomensuravel_codigo_Datalisttype = "Dynamic";
         Ddo_itemnaomensuravel_codigo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_itemnaomensuravel_codigo_Filtertype = "Character";
         Ddo_itemnaomensuravel_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace = "";
         Ddo_itemnaomensuravel_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_itemnaomensuravel_codigo_Cls = "ColumnSettings";
         Ddo_itemnaomensuravel_codigo_Tooltip = "Op��es";
         Ddo_itemnaomensuravel_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Item N�o Mensuravel";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV48ItemNaoMensuravel_CodigoTitleFilterData',fld:'vITEMNAOMENSURAVEL_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ItemNaoMensuravel_DescricaoTitleFilterData',fld:'vITEMNAOMENSURAVEL_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV56ItemNaoMensuravel_ValorTitleFilterData',fld:'vITEMNAOMENSURAVEL_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV60ReferenciaINM_DescricaoTitleFilterData',fld:'vREFERENCIAINM_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV64ItemNaoMensuravel_TipoTitleFilterData',fld:'vITEMNAOMENSURAVEL_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV68ItemNaoMensuravel_AtivoTitleFilterData',fld:'vITEMNAOMENSURAVEL_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtItemNaoMensuravel_Codigo_Titleformat',ctrl:'ITEMNAOMENSURAVEL_CODIGO',prop:'Titleformat'},{av:'edtItemNaoMensuravel_Codigo_Title',ctrl:'ITEMNAOMENSURAVEL_CODIGO',prop:'Title'},{av:'edtItemNaoMensuravel_Descricao_Titleformat',ctrl:'ITEMNAOMENSURAVEL_DESCRICAO',prop:'Titleformat'},{av:'edtItemNaoMensuravel_Descricao_Title',ctrl:'ITEMNAOMENSURAVEL_DESCRICAO',prop:'Title'},{av:'edtItemNaoMensuravel_Valor_Titleformat',ctrl:'ITEMNAOMENSURAVEL_VALOR',prop:'Titleformat'},{av:'edtItemNaoMensuravel_Valor_Title',ctrl:'ITEMNAOMENSURAVEL_VALOR',prop:'Title'},{av:'edtReferenciaINM_Descricao_Titleformat',ctrl:'REFERENCIAINM_DESCRICAO',prop:'Titleformat'},{av:'edtReferenciaINM_Descricao_Title',ctrl:'REFERENCIAINM_DESCRICAO',prop:'Title'},{av:'cmbItemNaoMensuravel_Tipo'},{av:'chkItemNaoMensuravel_Ativo_Titleformat',ctrl:'ITEMNAOMENSURAVEL_ATIVO',prop:'Titleformat'},{av:'chkItemNaoMensuravel_Ativo.Title.Text',ctrl:'ITEMNAOMENSURAVEL_ATIVO',prop:'Title'},{av:'AV73GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV74GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38ItemNaoMensuravel_AreaTrabalhoCod',fld:'vITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_CODIGO.ONOPTIONCLICKED","{handler:'E12E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_itemnaomensuravel_codigo_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_codigo_Filteredtext_get',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'FilteredText_get'},{av:'Ddo_itemnaomensuravel_codigo_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_DESCRICAO.ONOPTIONCLICKED","{handler:'E13E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_itemnaomensuravel_descricao_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_descricao_Filteredtext_get',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_itemnaomensuravel_descricao_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_VALOR.ONOPTIONCLICKED","{handler:'E14E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_itemnaomensuravel_valor_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_valor_Filteredtext_get',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredText_get'},{av:'Ddo_itemnaomensuravel_valor_Filteredtextto_get',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIAINM_DESCRICAO.ONOPTIONCLICKED","{handler:'E15E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_referenciainm_descricao_Activeeventkey',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_referenciainm_descricao_Filteredtext_get',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_referenciainm_descricao_Selectedvalue_get',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_TIPO.ONOPTIONCLICKED","{handler:'E16E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_itemnaomensuravel_tipo_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_tipo_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ITEMNAOMENSURAVEL_ATIVO.ONOPTIONCLICKED","{handler:'E17E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_itemnaomensuravel_ativo_Activeeventkey',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_itemnaomensuravel_ativo_Selectedvalue_get',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_itemnaomensuravel_ativo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SortedStatus'},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_itemnaomensuravel_codigo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_descricao_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_valor_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_itemnaomensuravel_tipo_Sortedstatus',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E31E72',iparms:[{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtItemNaoMensuravel_Descricao_Link',ctrl:'ITEMNAOMENSURAVEL_DESCRICAO',prop:'Link'},{av:'edtReferenciaINM_Descricao_Link',ctrl:'REFERENCIAINM_DESCRICAO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E18E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E24E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E19E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'edtavItemnaomensuravel_areatrabalhodes2_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES2',prop:'Visible'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_areatrabalhodes3_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES3',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavItemnaomensuravel_areatrabalhodes1_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES1',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E25E72',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavItemnaomensuravel_areatrabalhodes1_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES1',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E26E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E20E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'edtavItemnaomensuravel_areatrabalhodes2_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES2',prop:'Visible'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_areatrabalhodes3_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES3',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavItemnaomensuravel_areatrabalhodes1_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES1',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E27E72',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavItemnaomensuravel_areatrabalhodes2_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES2',prop:'Visible'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E21E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'edtavItemnaomensuravel_areatrabalhodes2_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES2',prop:'Visible'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_areatrabalhodes3_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES3',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavItemnaomensuravel_areatrabalhodes1_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES1',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E28E72',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavItemnaomensuravel_areatrabalhodes3_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES3',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E22E72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace',fld:'vDDO_ITEMNAOMENSURAVEL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''},{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV49TFItemNaoMensuravel_Codigo',fld:'vTFITEMNAOMENSURAVEL_CODIGO',pic:'@!',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Filteredtext_set',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'FilteredText_set'},{av:'AV50TFItemNaoMensuravel_Codigo_Sel',fld:'vTFITEMNAOMENSURAVEL_CODIGO_SEL',pic:'@!',nv:''},{av:'Ddo_itemnaomensuravel_codigo_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_CODIGO',prop:'SelectedValue_set'},{av:'AV53TFItemNaoMensuravel_Descricao',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_descricao_Filteredtext_set',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'FilteredText_set'},{av:'AV54TFItemNaoMensuravel_Descricao_Sel',fld:'vTFITEMNAOMENSURAVEL_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_itemnaomensuravel_descricao_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_DESCRICAO',prop:'SelectedValue_set'},{av:'AV57TFItemNaoMensuravel_Valor',fld:'vTFITEMNAOMENSURAVEL_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_itemnaomensuravel_valor_Filteredtext_set',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredText_set'},{av:'AV58TFItemNaoMensuravel_Valor_To',fld:'vTFITEMNAOMENSURAVEL_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_itemnaomensuravel_valor_Filteredtextto_set',ctrl:'DDO_ITEMNAOMENSURAVEL_VALOR',prop:'FilteredTextTo_set'},{av:'AV61TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_referenciainm_descricao_Filteredtext_set',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'FilteredText_set'},{av:'AV62TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_referenciainm_descricao_Selectedvalue_set',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SelectedValue_set'},{av:'AV66TFItemNaoMensuravel_Tipo_Sels',fld:'vTFITEMNAOMENSURAVEL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_itemnaomensuravel_tipo_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_TIPO',prop:'SelectedValue_set'},{av:'AV69TFItemNaoMensuravel_Ativo_Sel',fld:'vTFITEMNAOMENSURAVEL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_itemnaomensuravel_ativo_Selectedvalue_set',ctrl:'DDO_ITEMNAOMENSURAVEL_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV43ItemNaoMensuravel_AreaTrabalhoDes1',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavItemnaomensuravel_areatrabalhodes1_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES1',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV44ItemNaoMensuravel_AreaTrabalhoDes2',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV45ItemNaoMensuravel_AreaTrabalhoDes3',fld:'vITEMNAOMENSURAVEL_AREATRABALHODES3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV28ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'edtavItemnaomensuravel_areatrabalhodes2_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES2',prop:'Visible'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavItemnaomensuravel_areatrabalhodes3_Visible',ctrl:'vITEMNAOMENSURAVEL_AREATRABALHODES3',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E23E72',iparms:[{av:'A718ItemNaoMensuravel_AreaTrabalhoCod',fld:'ITEMNAOMENSURAVEL_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A715ItemNaoMensuravel_Codigo',fld:'ITEMNAOMENSURAVEL_CODIGO',pic:'@!',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_itemnaomensuravel_codigo_Activeeventkey = "";
         Ddo_itemnaomensuravel_codigo_Filteredtext_get = "";
         Ddo_itemnaomensuravel_codigo_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_descricao_Activeeventkey = "";
         Ddo_itemnaomensuravel_descricao_Filteredtext_get = "";
         Ddo_itemnaomensuravel_descricao_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_valor_Activeeventkey = "";
         Ddo_itemnaomensuravel_valor_Filteredtext_get = "";
         Ddo_itemnaomensuravel_valor_Filteredtextto_get = "";
         Ddo_referenciainm_descricao_Activeeventkey = "";
         Ddo_referenciainm_descricao_Filteredtext_get = "";
         Ddo_referenciainm_descricao_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_tipo_Activeeventkey = "";
         Ddo_itemnaomensuravel_tipo_Selectedvalue_get = "";
         Ddo_itemnaomensuravel_ativo_Activeeventkey = "";
         Ddo_itemnaomensuravel_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV43ItemNaoMensuravel_AreaTrabalhoDes1 = "";
         AV18ReferenciaINM_Descricao1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV44ItemNaoMensuravel_AreaTrabalhoDes2 = "";
         AV23ReferenciaINM_Descricao2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV45ItemNaoMensuravel_AreaTrabalhoDes3 = "";
         AV28ReferenciaINM_Descricao3 = "";
         AV49TFItemNaoMensuravel_Codigo = "";
         AV50TFItemNaoMensuravel_Codigo_Sel = "";
         AV53TFItemNaoMensuravel_Descricao = "";
         AV54TFItemNaoMensuravel_Descricao_Sel = "";
         AV61TFReferenciaINM_Descricao = "";
         AV62TFReferenciaINM_Descricao_Sel = "";
         AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace = "";
         AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace = "";
         AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace = "";
         AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = "";
         AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace = "";
         AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace = "";
         AV66TFItemNaoMensuravel_Tipo_Sels = new GxSimpleCollection();
         AV103Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A715ItemNaoMensuravel_Codigo = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV71DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV48ItemNaoMensuravel_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ItemNaoMensuravel_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56ItemNaoMensuravel_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60ReferenciaINM_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64ItemNaoMensuravel_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68ItemNaoMensuravel_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_itemnaomensuravel_codigo_Filteredtext_set = "";
         Ddo_itemnaomensuravel_codigo_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_codigo_Sortedstatus = "";
         Ddo_itemnaomensuravel_descricao_Filteredtext_set = "";
         Ddo_itemnaomensuravel_descricao_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_descricao_Sortedstatus = "";
         Ddo_itemnaomensuravel_valor_Filteredtext_set = "";
         Ddo_itemnaomensuravel_valor_Filteredtextto_set = "";
         Ddo_itemnaomensuravel_valor_Sortedstatus = "";
         Ddo_referenciainm_descricao_Filteredtext_set = "";
         Ddo_referenciainm_descricao_Selectedvalue_set = "";
         Ddo_referenciainm_descricao_Sortedstatus = "";
         Ddo_itemnaomensuravel_tipo_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_tipo_Sortedstatus = "";
         Ddo_itemnaomensuravel_ativo_Selectedvalue_set = "";
         Ddo_itemnaomensuravel_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV101Update_GXI = "";
         AV32Delete = "";
         AV102Delete_GXI = "";
         A714ItemNaoMensuravel_Descricao = "";
         A710ReferenciaINM_Descricao = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = "";
         lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = "";
         lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = "";
         lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = "";
         lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = "";
         lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = "";
         lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = "";
         lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = "";
         lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = "";
         AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 = "";
         AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 = "";
         AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 = "";
         AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 = "";
         AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 = "";
         AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 = "";
         AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 = "";
         AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 = "";
         AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 = "";
         AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel = "";
         AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo = "";
         AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel = "";
         AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao = "";
         AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel = "";
         AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao = "";
         A720ItemNaoMensuravel_AreaTrabalhoDes = "";
         H00E72_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         H00E72_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         H00E72_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         H00E72_A717ItemNaoMensuravel_Tipo = new short[1] ;
         H00E72_A710ReferenciaINM_Descricao = new String[] {""} ;
         H00E72_A709ReferenciaINM_Codigo = new int[1] ;
         H00E72_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         H00E72_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         H00E72_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         H00E72_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         H00E73_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV65TFItemNaoMensuravel_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblItemnaomensuraveltitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwitemnaomensuravel__default(),
            new Object[][] {
                new Object[] {
               H00E72_A720ItemNaoMensuravel_AreaTrabalhoDes, H00E72_n720ItemNaoMensuravel_AreaTrabalhoDes, H00E72_A716ItemNaoMensuravel_Ativo, H00E72_A717ItemNaoMensuravel_Tipo, H00E72_A710ReferenciaINM_Descricao, H00E72_A709ReferenciaINM_Codigo, H00E72_A719ItemNaoMensuravel_Valor, H00E72_A714ItemNaoMensuravel_Descricao, H00E72_A715ItemNaoMensuravel_Codigo, H00E72_A718ItemNaoMensuravel_AreaTrabalhoCod
               }
               , new Object[] {
               H00E73_AGRID_nRecordCount
               }
            }
         );
         AV103Pgmname = "WWItemNaoMensuravel";
         /* GeneXus formulas. */
         AV103Pgmname = "WWItemNaoMensuravel";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_91 ;
      private short nGXsfl_91_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV69TFItemNaoMensuravel_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A717ItemNaoMensuravel_Tipo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_91_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ;
      private short AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ;
      private short AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ;
      private short AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ;
      private short edtItemNaoMensuravel_Codigo_Titleformat ;
      private short edtItemNaoMensuravel_Descricao_Titleformat ;
      private short edtItemNaoMensuravel_Valor_Titleformat ;
      private short edtReferenciaINM_Descricao_Titleformat ;
      private short cmbItemNaoMensuravel_Tipo_Titleformat ;
      private short chkItemNaoMensuravel_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int A709ReferenciaINM_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_itemnaomensuravel_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_itemnaomensuravel_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_referenciainm_descricao_Datalistupdateminimumcharacters ;
      private int edtavTfitemnaomensuravel_codigo_Visible ;
      private int edtavTfitemnaomensuravel_codigo_sel_Visible ;
      private int edtavTfitemnaomensuravel_descricao_Visible ;
      private int edtavTfitemnaomensuravel_descricao_sel_Visible ;
      private int edtavTfitemnaomensuravel_valor_Visible ;
      private int edtavTfitemnaomensuravel_valor_to_Visible ;
      private int edtavTfreferenciainm_descricao_Visible ;
      private int edtavTfreferenciainm_descricao_sel_Visible ;
      private int edtavTfitemnaomensuravel_ativo_sel_Visible ;
      private int edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count ;
      private int AV38ItemNaoMensuravel_AreaTrabalhoCod ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV72PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavItemnaomensuravel_areatrabalhodes1_Visible ;
      private int edtavReferenciainm_descricao1_Visible ;
      private int edtavItemnaomensuravel_areatrabalhodes2_Visible ;
      private int edtavReferenciainm_descricao2_Visible ;
      private int edtavItemnaomensuravel_areatrabalhodes3_Visible ;
      private int edtavReferenciainm_descricao3_Visible ;
      private int AV104GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV73GridCurrentPage ;
      private long AV74GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV57TFItemNaoMensuravel_Valor ;
      private decimal AV58TFItemNaoMensuravel_Valor_To ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private decimal AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ;
      private decimal AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_itemnaomensuravel_codigo_Activeeventkey ;
      private String Ddo_itemnaomensuravel_codigo_Filteredtext_get ;
      private String Ddo_itemnaomensuravel_codigo_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_descricao_Activeeventkey ;
      private String Ddo_itemnaomensuravel_descricao_Filteredtext_get ;
      private String Ddo_itemnaomensuravel_descricao_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_valor_Activeeventkey ;
      private String Ddo_itemnaomensuravel_valor_Filteredtext_get ;
      private String Ddo_itemnaomensuravel_valor_Filteredtextto_get ;
      private String Ddo_referenciainm_descricao_Activeeventkey ;
      private String Ddo_referenciainm_descricao_Filteredtext_get ;
      private String Ddo_referenciainm_descricao_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_tipo_Activeeventkey ;
      private String Ddo_itemnaomensuravel_tipo_Selectedvalue_get ;
      private String Ddo_itemnaomensuravel_ativo_Activeeventkey ;
      private String Ddo_itemnaomensuravel_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_91_idx="0001" ;
      private String AV49TFItemNaoMensuravel_Codigo ;
      private String AV50TFItemNaoMensuravel_Codigo_Sel ;
      private String AV103Pgmname ;
      private String A715ItemNaoMensuravel_Codigo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_itemnaomensuravel_codigo_Caption ;
      private String Ddo_itemnaomensuravel_codigo_Tooltip ;
      private String Ddo_itemnaomensuravel_codigo_Cls ;
      private String Ddo_itemnaomensuravel_codigo_Filteredtext_set ;
      private String Ddo_itemnaomensuravel_codigo_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_codigo_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_codigo_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_codigo_Sortedstatus ;
      private String Ddo_itemnaomensuravel_codigo_Filtertype ;
      private String Ddo_itemnaomensuravel_codigo_Datalisttype ;
      private String Ddo_itemnaomensuravel_codigo_Datalistproc ;
      private String Ddo_itemnaomensuravel_codigo_Sortasc ;
      private String Ddo_itemnaomensuravel_codigo_Sortdsc ;
      private String Ddo_itemnaomensuravel_codigo_Loadingdata ;
      private String Ddo_itemnaomensuravel_codigo_Cleanfilter ;
      private String Ddo_itemnaomensuravel_codigo_Noresultsfound ;
      private String Ddo_itemnaomensuravel_codigo_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_descricao_Caption ;
      private String Ddo_itemnaomensuravel_descricao_Tooltip ;
      private String Ddo_itemnaomensuravel_descricao_Cls ;
      private String Ddo_itemnaomensuravel_descricao_Filteredtext_set ;
      private String Ddo_itemnaomensuravel_descricao_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_descricao_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_descricao_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_descricao_Sortedstatus ;
      private String Ddo_itemnaomensuravel_descricao_Filtertype ;
      private String Ddo_itemnaomensuravel_descricao_Datalisttype ;
      private String Ddo_itemnaomensuravel_descricao_Datalistproc ;
      private String Ddo_itemnaomensuravel_descricao_Sortasc ;
      private String Ddo_itemnaomensuravel_descricao_Sortdsc ;
      private String Ddo_itemnaomensuravel_descricao_Loadingdata ;
      private String Ddo_itemnaomensuravel_descricao_Cleanfilter ;
      private String Ddo_itemnaomensuravel_descricao_Noresultsfound ;
      private String Ddo_itemnaomensuravel_descricao_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_valor_Caption ;
      private String Ddo_itemnaomensuravel_valor_Tooltip ;
      private String Ddo_itemnaomensuravel_valor_Cls ;
      private String Ddo_itemnaomensuravel_valor_Filteredtext_set ;
      private String Ddo_itemnaomensuravel_valor_Filteredtextto_set ;
      private String Ddo_itemnaomensuravel_valor_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_valor_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_valor_Sortedstatus ;
      private String Ddo_itemnaomensuravel_valor_Filtertype ;
      private String Ddo_itemnaomensuravel_valor_Sortasc ;
      private String Ddo_itemnaomensuravel_valor_Sortdsc ;
      private String Ddo_itemnaomensuravel_valor_Cleanfilter ;
      private String Ddo_itemnaomensuravel_valor_Rangefilterfrom ;
      private String Ddo_itemnaomensuravel_valor_Rangefilterto ;
      private String Ddo_itemnaomensuravel_valor_Searchbuttontext ;
      private String Ddo_referenciainm_descricao_Caption ;
      private String Ddo_referenciainm_descricao_Tooltip ;
      private String Ddo_referenciainm_descricao_Cls ;
      private String Ddo_referenciainm_descricao_Filteredtext_set ;
      private String Ddo_referenciainm_descricao_Selectedvalue_set ;
      private String Ddo_referenciainm_descricao_Dropdownoptionstype ;
      private String Ddo_referenciainm_descricao_Titlecontrolidtoreplace ;
      private String Ddo_referenciainm_descricao_Sortedstatus ;
      private String Ddo_referenciainm_descricao_Filtertype ;
      private String Ddo_referenciainm_descricao_Datalisttype ;
      private String Ddo_referenciainm_descricao_Datalistproc ;
      private String Ddo_referenciainm_descricao_Sortasc ;
      private String Ddo_referenciainm_descricao_Sortdsc ;
      private String Ddo_referenciainm_descricao_Loadingdata ;
      private String Ddo_referenciainm_descricao_Cleanfilter ;
      private String Ddo_referenciainm_descricao_Noresultsfound ;
      private String Ddo_referenciainm_descricao_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_tipo_Caption ;
      private String Ddo_itemnaomensuravel_tipo_Tooltip ;
      private String Ddo_itemnaomensuravel_tipo_Cls ;
      private String Ddo_itemnaomensuravel_tipo_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_tipo_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_tipo_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_tipo_Sortedstatus ;
      private String Ddo_itemnaomensuravel_tipo_Datalisttype ;
      private String Ddo_itemnaomensuravel_tipo_Datalistfixedvalues ;
      private String Ddo_itemnaomensuravel_tipo_Sortasc ;
      private String Ddo_itemnaomensuravel_tipo_Sortdsc ;
      private String Ddo_itemnaomensuravel_tipo_Cleanfilter ;
      private String Ddo_itemnaomensuravel_tipo_Searchbuttontext ;
      private String Ddo_itemnaomensuravel_ativo_Caption ;
      private String Ddo_itemnaomensuravel_ativo_Tooltip ;
      private String Ddo_itemnaomensuravel_ativo_Cls ;
      private String Ddo_itemnaomensuravel_ativo_Selectedvalue_set ;
      private String Ddo_itemnaomensuravel_ativo_Dropdownoptionstype ;
      private String Ddo_itemnaomensuravel_ativo_Titlecontrolidtoreplace ;
      private String Ddo_itemnaomensuravel_ativo_Sortedstatus ;
      private String Ddo_itemnaomensuravel_ativo_Datalisttype ;
      private String Ddo_itemnaomensuravel_ativo_Datalistfixedvalues ;
      private String Ddo_itemnaomensuravel_ativo_Sortasc ;
      private String Ddo_itemnaomensuravel_ativo_Sortdsc ;
      private String Ddo_itemnaomensuravel_ativo_Cleanfilter ;
      private String Ddo_itemnaomensuravel_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfitemnaomensuravel_codigo_Internalname ;
      private String edtavTfitemnaomensuravel_codigo_Jsonclick ;
      private String edtavTfitemnaomensuravel_codigo_sel_Internalname ;
      private String edtavTfitemnaomensuravel_codigo_sel_Jsonclick ;
      private String edtavTfitemnaomensuravel_descricao_Internalname ;
      private String edtavTfitemnaomensuravel_descricao_sel_Internalname ;
      private String edtavTfitemnaomensuravel_valor_Internalname ;
      private String edtavTfitemnaomensuravel_valor_Jsonclick ;
      private String edtavTfitemnaomensuravel_valor_to_Internalname ;
      private String edtavTfitemnaomensuravel_valor_to_Jsonclick ;
      private String edtavTfreferenciainm_descricao_Internalname ;
      private String edtavTfreferenciainm_descricao_Jsonclick ;
      private String edtavTfreferenciainm_descricao_sel_Internalname ;
      private String edtavTfreferenciainm_descricao_sel_Jsonclick ;
      private String edtavTfitemnaomensuravel_ativo_sel_Internalname ;
      private String edtavTfitemnaomensuravel_ativo_sel_Jsonclick ;
      private String edtavDdo_itemnaomensuravel_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_valortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_itemnaomensuravel_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtItemNaoMensuravel_AreaTrabalhoCod_Internalname ;
      private String edtItemNaoMensuravel_Codigo_Internalname ;
      private String edtItemNaoMensuravel_Descricao_Internalname ;
      private String edtItemNaoMensuravel_Valor_Internalname ;
      private String edtReferenciaINM_Codigo_Internalname ;
      private String edtReferenciaINM_Descricao_Internalname ;
      private String cmbItemNaoMensuravel_Tipo_Internalname ;
      private String chkItemNaoMensuravel_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ;
      private String AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ;
      private String AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavItemnaomensuravel_areatrabalhodes1_Internalname ;
      private String edtavReferenciainm_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavItemnaomensuravel_areatrabalhodes2_Internalname ;
      private String edtavReferenciainm_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavItemnaomensuravel_areatrabalhodes3_Internalname ;
      private String edtavReferenciainm_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_itemnaomensuravel_codigo_Internalname ;
      private String Ddo_itemnaomensuravel_descricao_Internalname ;
      private String Ddo_itemnaomensuravel_valor_Internalname ;
      private String Ddo_referenciainm_descricao_Internalname ;
      private String Ddo_itemnaomensuravel_tipo_Internalname ;
      private String Ddo_itemnaomensuravel_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtItemNaoMensuravel_Codigo_Title ;
      private String edtItemNaoMensuravel_Descricao_Title ;
      private String edtItemNaoMensuravel_Valor_Title ;
      private String edtReferenciaINM_Descricao_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtItemNaoMensuravel_Descricao_Link ;
      private String edtReferenciaINM_Descricao_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblItemnaomensuraveltitle_Internalname ;
      private String lblItemnaomensuraveltitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavItemnaomensuravel_areatrabalhodes3_Jsonclick ;
      private String edtavReferenciainm_descricao3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavItemnaomensuravel_areatrabalhodes2_Jsonclick ;
      private String edtavReferenciainm_descricao2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavItemnaomensuravel_areatrabalhodes1_Jsonclick ;
      private String edtavReferenciainm_descricao1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_91_fel_idx="0001" ;
      private String ROClassString ;
      private String edtItemNaoMensuravel_AreaTrabalhoCod_Jsonclick ;
      private String edtItemNaoMensuravel_Codigo_Jsonclick ;
      private String edtItemNaoMensuravel_Descricao_Jsonclick ;
      private String edtItemNaoMensuravel_Valor_Jsonclick ;
      private String edtReferenciaINM_Codigo_Jsonclick ;
      private String edtReferenciaINM_Descricao_Jsonclick ;
      private String cmbItemNaoMensuravel_Tipo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_itemnaomensuravel_codigo_Includesortasc ;
      private bool Ddo_itemnaomensuravel_codigo_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_codigo_Includefilter ;
      private bool Ddo_itemnaomensuravel_codigo_Filterisrange ;
      private bool Ddo_itemnaomensuravel_codigo_Includedatalist ;
      private bool Ddo_itemnaomensuravel_descricao_Includesortasc ;
      private bool Ddo_itemnaomensuravel_descricao_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_descricao_Includefilter ;
      private bool Ddo_itemnaomensuravel_descricao_Filterisrange ;
      private bool Ddo_itemnaomensuravel_descricao_Includedatalist ;
      private bool Ddo_itemnaomensuravel_valor_Includesortasc ;
      private bool Ddo_itemnaomensuravel_valor_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_valor_Includefilter ;
      private bool Ddo_itemnaomensuravel_valor_Filterisrange ;
      private bool Ddo_itemnaomensuravel_valor_Includedatalist ;
      private bool Ddo_referenciainm_descricao_Includesortasc ;
      private bool Ddo_referenciainm_descricao_Includesortdsc ;
      private bool Ddo_referenciainm_descricao_Includefilter ;
      private bool Ddo_referenciainm_descricao_Filterisrange ;
      private bool Ddo_referenciainm_descricao_Includedatalist ;
      private bool Ddo_itemnaomensuravel_tipo_Includesortasc ;
      private bool Ddo_itemnaomensuravel_tipo_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_tipo_Includefilter ;
      private bool Ddo_itemnaomensuravel_tipo_Includedatalist ;
      private bool Ddo_itemnaomensuravel_tipo_Allowmultipleselection ;
      private bool Ddo_itemnaomensuravel_ativo_Includesortasc ;
      private bool Ddo_itemnaomensuravel_ativo_Includesortdsc ;
      private bool Ddo_itemnaomensuravel_ativo_Includefilter ;
      private bool Ddo_itemnaomensuravel_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A716ItemNaoMensuravel_Ativo ;
      private bool AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ;
      private bool AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ;
      private bool n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private String A714ItemNaoMensuravel_Descricao ;
      private String AV65TFItemNaoMensuravel_Tipo_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV43ItemNaoMensuravel_AreaTrabalhoDes1 ;
      private String AV18ReferenciaINM_Descricao1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV44ItemNaoMensuravel_AreaTrabalhoDes2 ;
      private String AV23ReferenciaINM_Descricao2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV45ItemNaoMensuravel_AreaTrabalhoDes3 ;
      private String AV28ReferenciaINM_Descricao3 ;
      private String AV53TFItemNaoMensuravel_Descricao ;
      private String AV54TFItemNaoMensuravel_Descricao_Sel ;
      private String AV61TFReferenciaINM_Descricao ;
      private String AV62TFReferenciaINM_Descricao_Sel ;
      private String AV51ddo_ItemNaoMensuravel_CodigoTitleControlIdToReplace ;
      private String AV55ddo_ItemNaoMensuravel_DescricaoTitleControlIdToReplace ;
      private String AV59ddo_ItemNaoMensuravel_ValorTitleControlIdToReplace ;
      private String AV63ddo_ReferenciaINM_DescricaoTitleControlIdToReplace ;
      private String AV67ddo_ItemNaoMensuravel_TipoTitleControlIdToReplace ;
      private String AV70ddo_ItemNaoMensuravel_AtivoTitleControlIdToReplace ;
      private String AV101Update_GXI ;
      private String AV102Delete_GXI ;
      private String A710ReferenciaINM_Descricao ;
      private String lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ;
      private String lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ;
      private String lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ;
      private String lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ;
      private String lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ;
      private String lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ;
      private String lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ;
      private String lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ;
      private String AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ;
      private String AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ;
      private String AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ;
      private String AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ;
      private String AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ;
      private String AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ;
      private String AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ;
      private String AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ;
      private String AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ;
      private String AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ;
      private String AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ;
      private String AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ;
      private String AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ;
      private String A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String AV31Update ;
      private String AV32Delete ;
      private IGxSession AV33Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbItemNaoMensuravel_Tipo ;
      private GXCheckbox chkItemNaoMensuravel_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00E72_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] H00E72_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] H00E72_A716ItemNaoMensuravel_Ativo ;
      private short[] H00E72_A717ItemNaoMensuravel_Tipo ;
      private String[] H00E72_A710ReferenciaINM_Descricao ;
      private int[] H00E72_A709ReferenciaINM_Codigo ;
      private decimal[] H00E72_A719ItemNaoMensuravel_Valor ;
      private String[] H00E72_A714ItemNaoMensuravel_Descricao ;
      private String[] H00E72_A715ItemNaoMensuravel_Codigo ;
      private int[] H00E72_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private long[] H00E73_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV66TFItemNaoMensuravel_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48ItemNaoMensuravel_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ItemNaoMensuravel_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV56ItemNaoMensuravel_ValorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60ReferenciaINM_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64ItemNaoMensuravel_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68ItemNaoMensuravel_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV71DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwitemnaomensuravel__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00E72( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                             String AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                             short AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                             String AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                             String AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                             bool AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                             String AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                             short AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                             String AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                             String AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                             bool AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                             String AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                             short AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                             String AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                             String AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                             String AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                             String AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                             String AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                             String AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                             decimal AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                             decimal AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                             String AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                             String AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                             int AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count ,
                                             short AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV38ItemNaoMensuravel_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [26] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes, T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T2.[ReferenciaINM_Descricao], T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_Descricao], T1.[ItemNaoMensuravel_Codigo], T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod";
         sFromString = " FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ItemNaoMensuravel_AreaTrabalhoCod] = @AV38ItemNaoMensuravel_AreaTrabalhoCod)";
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Descricao]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Valor]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Valor] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ReferenciaINM_Descricao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ReferenciaINM_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Tipo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Ativo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ItemNaoMensuravel_AreaTrabalhoCod], T1.[ItemNaoMensuravel_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00E73( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels ,
                                             String AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1 ,
                                             short AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 ,
                                             String AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1 ,
                                             String AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1 ,
                                             bool AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 ,
                                             String AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2 ,
                                             short AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 ,
                                             String AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2 ,
                                             String AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2 ,
                                             bool AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 ,
                                             String AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3 ,
                                             short AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 ,
                                             String AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3 ,
                                             String AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3 ,
                                             String AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel ,
                                             String AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo ,
                                             String AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel ,
                                             String AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao ,
                                             decimal AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor ,
                                             decimal AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to ,
                                             String AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel ,
                                             String AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao ,
                                             int AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count ,
                                             short AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             bool A716ItemNaoMensuravel_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV38ItemNaoMensuravel_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [21] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ItemNaoMensuravel_AreaTrabalhoCod] = @AV38ItemNaoMensuravel_AreaTrabalhoCod)";
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV77WWItemNaoMensuravelDS_1_Dynamicfiltersselector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV78WWItemNaoMensuravelDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV81WWItemNaoMensuravelDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV82WWItemNaoMensuravelDS_6_Dynamicfiltersselector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV83WWItemNaoMensuravelDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV86WWItemNaoMensuravelDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV87WWItemNaoMensuravelDS_11_Dynamicfiltersselector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV88WWItemNaoMensuravelDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to) )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV99WWItemNaoMensuravelDS_23_Tfitemnaomensuravel_tipo_sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
         }
         if ( AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
         }
         if ( AV100WWItemNaoMensuravelDS_24_Tfitemnaomensuravel_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00E72(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (decimal)dynConstraints[30] , (bool)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
               case 1 :
                     return conditional_H00E73(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (decimal)dynConstraints[30] , (bool)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00E72 ;
          prmH00E72 = new Object[] {
          new Object[] {"@AV38ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00E73 ;
          prmH00E73 = new Object[] {
          new Object[] {"@AV38ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV79WWItemNaoMensuravelDS_3_Itemnaomensuravel_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV80WWItemNaoMensuravelDS_4_Referenciainm_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV84WWItemNaoMensuravelDS_8_Itemnaomensuravel_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV85WWItemNaoMensuravelDS_9_Referenciainm_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV89WWItemNaoMensuravelDS_13_Itemnaomensuravel_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV90WWItemNaoMensuravelDS_14_Referenciainm_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WWItemNaoMensuravelDS_15_Tfitemnaomensuravel_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV92WWItemNaoMensuravelDS_16_Tfitemnaomensuravel_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV93WWItemNaoMensuravelDS_17_Tfitemnaomensuravel_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV94WWItemNaoMensuravelDS_18_Tfitemnaomensuravel_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95WWItemNaoMensuravelDS_19_Tfitemnaomensuravel_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV96WWItemNaoMensuravelDS_20_Tfitemnaomensuravel_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV97WWItemNaoMensuravelDS_21_Tfreferenciainm_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV98WWItemNaoMensuravelDS_22_Tfreferenciainm_descricao_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00E72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E72,11,0,true,false )
             ,new CursorDef("H00E73", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E73,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
       }
    }

 }

}
