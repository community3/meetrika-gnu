/*
               File: GetWWContagemItemFilterData
        Description: Get WWContagem Item Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:44.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemitemfilterdata : GXProcedure
   {
      public getwwcontagemitemfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemitemfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemitemfilterdata objgetwwcontagemitemfilterdata;
         objgetwwcontagemitemfilterdata = new getwwcontagemitemfilterdata();
         objgetwwcontagemitemfilterdata.AV22DDOName = aP0_DDOName;
         objgetwwcontagemitemfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetwwcontagemitemfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemitemfilterdata.AV26OptionsJson = "" ;
         objgetwwcontagemitemfilterdata.AV29OptionsDescJson = "" ;
         objgetwwcontagemitemfilterdata.AV31OptionIndexesJson = "" ;
         objgetwwcontagemitemfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemitemfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemitemfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemitemfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTAGEMITEM_EVIDENCIAS") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMITEM_EVIDENCIASOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("WWContagemItemGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContagemItemGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("WWContagemItemGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_CODIGO") == 0 )
            {
               AV10TFContagem_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContagem_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_PFB") == 0 )
            {
               AV12TFContagemItem_PFB = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, ".");
               AV13TFContagemItem_PFB_To = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_PFL") == 0 )
            {
               AV14TFContagemItem_PFL = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, ".");
               AV15TFContagemItem_PFL_To = NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_TIPOUNIDADE_SEL") == 0 )
            {
               AV16TFContagemItem_TipoUnidade_SelsJson = AV36GridStateFilterValue.gxTpr_Value;
               AV17TFContagemItem_TipoUnidade_Sels.FromJSonString(AV16TFContagemItem_TipoUnidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_EVIDENCIAS") == 0 )
            {
               AV18TFContagemItem_Evidencias = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMITEM_EVIDENCIAS_SEL") == 0 )
            {
               AV19TFContagemItem_Evidencias_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "PARM_&STATUSITEM") == 0 )
            {
               AV38StatusItem = (short)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMITEM_EVIDENCIASOPTIONS' Routine */
         AV18TFContagemItem_Evidencias = AV20SearchTxt;
         AV19TFContagemItem_Evidencias_Sel = "";
         AV43WWContagemItemDS_1_Tfcontagem_codigo = AV10TFContagem_Codigo;
         AV44WWContagemItemDS_2_Tfcontagem_codigo_to = AV11TFContagem_Codigo_To;
         AV45WWContagemItemDS_3_Tfcontagemitem_pfb = AV12TFContagemItem_PFB;
         AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to = AV13TFContagemItem_PFB_To;
         AV47WWContagemItemDS_5_Tfcontagemitem_pfl = AV14TFContagemItem_PFL;
         AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to = AV15TFContagemItem_PFL_To;
         AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = AV17TFContagemItem_TipoUnidade_Sels;
         AV50WWContagemItemDS_8_Tfcontagemitem_evidencias = AV18TFContagemItem_Evidencias;
         AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = AV19TFContagemItem_Evidencias_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A952ContagemItem_TipoUnidade ,
                                              AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ,
                                              AV43WWContagemItemDS_1_Tfcontagem_codigo ,
                                              AV44WWContagemItemDS_2_Tfcontagem_codigo_to ,
                                              AV45WWContagemItemDS_3_Tfcontagemitem_pfb ,
                                              AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to ,
                                              AV47WWContagemItemDS_5_Tfcontagemitem_pfl ,
                                              AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to ,
                                              AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels.Count ,
                                              AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ,
                                              AV50WWContagemItemDS_8_Tfcontagemitem_evidencias ,
                                              A192Contagem_Codigo ,
                                              A950ContagemItem_PFB ,
                                              A951ContagemItem_PFL ,
                                              A953ContagemItem_Evidencias },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV50WWContagemItemDS_8_Tfcontagemitem_evidencias = StringUtil.Concat( StringUtil.RTrim( AV50WWContagemItemDS_8_Tfcontagemitem_evidencias), "%", "");
         /* Using cursor P00U62 */
         pr_default.execute(0, new Object[] {AV43WWContagemItemDS_1_Tfcontagem_codigo, AV44WWContagemItemDS_2_Tfcontagem_codigo_to, AV45WWContagemItemDS_3_Tfcontagemitem_pfb, AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to, AV47WWContagemItemDS_5_Tfcontagemitem_pfl, AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to, lV50WWContagemItemDS_8_Tfcontagemitem_evidencias, AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKU62 = false;
            A953ContagemItem_Evidencias = P00U62_A953ContagemItem_Evidencias[0];
            n953ContagemItem_Evidencias = P00U62_n953ContagemItem_Evidencias[0];
            A952ContagemItem_TipoUnidade = P00U62_A952ContagemItem_TipoUnidade[0];
            A951ContagemItem_PFL = P00U62_A951ContagemItem_PFL[0];
            n951ContagemItem_PFL = P00U62_n951ContagemItem_PFL[0];
            A950ContagemItem_PFB = P00U62_A950ContagemItem_PFB[0];
            n950ContagemItem_PFB = P00U62_n950ContagemItem_PFB[0];
            A192Contagem_Codigo = P00U62_A192Contagem_Codigo[0];
            A224ContagemItem_Lancamento = P00U62_A224ContagemItem_Lancamento[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00U62_A953ContagemItem_Evidencias[0], A953ContagemItem_Evidencias) == 0 ) )
            {
               BRKU62 = false;
               A224ContagemItem_Lancamento = P00U62_A224ContagemItem_Lancamento[0];
               AV32count = (long)(AV32count+1);
               BRKU62 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A953ContagemItem_Evidencias)) )
            {
               AV24Option = A953ContagemItem_Evidencias;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU62 )
            {
               BRKU62 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV16TFContagemItem_TipoUnidade_SelsJson = "";
         AV17TFContagemItem_TipoUnidade_Sels = new GxSimpleCollection();
         AV18TFContagemItem_Evidencias = "";
         AV19TFContagemItem_Evidencias_Sel = "";
         AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels = new GxSimpleCollection();
         AV50WWContagemItemDS_8_Tfcontagemitem_evidencias = "";
         AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel = "";
         scmdbuf = "";
         lV50WWContagemItemDS_8_Tfcontagemitem_evidencias = "";
         A952ContagemItem_TipoUnidade = "";
         A953ContagemItem_Evidencias = "";
         P00U62_A953ContagemItem_Evidencias = new String[] {""} ;
         P00U62_n953ContagemItem_Evidencias = new bool[] {false} ;
         P00U62_A952ContagemItem_TipoUnidade = new String[] {""} ;
         P00U62_A951ContagemItem_PFL = new decimal[1] ;
         P00U62_n951ContagemItem_PFL = new bool[] {false} ;
         P00U62_A950ContagemItem_PFB = new decimal[1] ;
         P00U62_n950ContagemItem_PFB = new bool[] {false} ;
         P00U62_A192Contagem_Codigo = new int[1] ;
         P00U62_A224ContagemItem_Lancamento = new int[1] ;
         AV24Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontagemitemfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00U62_A953ContagemItem_Evidencias, P00U62_n953ContagemItem_Evidencias, P00U62_A952ContagemItem_TipoUnidade, P00U62_A951ContagemItem_PFL, P00U62_n951ContagemItem_PFL, P00U62_A950ContagemItem_PFB, P00U62_n950ContagemItem_PFB, P00U62_A192Contagem_Codigo, P00U62_A224ContagemItem_Lancamento
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV38StatusItem ;
      private int AV41GXV1 ;
      private int AV10TFContagem_Codigo ;
      private int AV11TFContagem_Codigo_To ;
      private int AV43WWContagemItemDS_1_Tfcontagem_codigo ;
      private int AV44WWContagemItemDS_2_Tfcontagem_codigo_to ;
      private int AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count ;
      private int A192Contagem_Codigo ;
      private int A224ContagemItem_Lancamento ;
      private long AV32count ;
      private decimal AV12TFContagemItem_PFB ;
      private decimal AV13TFContagemItem_PFB_To ;
      private decimal AV14TFContagemItem_PFL ;
      private decimal AV15TFContagemItem_PFL_To ;
      private decimal AV45WWContagemItemDS_3_Tfcontagemitem_pfb ;
      private decimal AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to ;
      private decimal AV47WWContagemItemDS_5_Tfcontagemitem_pfl ;
      private decimal AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to ;
      private decimal A950ContagemItem_PFB ;
      private decimal A951ContagemItem_PFL ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKU62 ;
      private bool n953ContagemItem_Evidencias ;
      private bool n951ContagemItem_PFL ;
      private bool n950ContagemItem_PFB ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV16TFContagemItem_TipoUnidade_SelsJson ;
      private String A953ContagemItem_Evidencias ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV18TFContagemItem_Evidencias ;
      private String AV19TFContagemItem_Evidencias_Sel ;
      private String AV50WWContagemItemDS_8_Tfcontagemitem_evidencias ;
      private String AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ;
      private String lV50WWContagemItemDS_8_Tfcontagemitem_evidencias ;
      private String A952ContagemItem_TipoUnidade ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00U62_A953ContagemItem_Evidencias ;
      private bool[] P00U62_n953ContagemItem_Evidencias ;
      private String[] P00U62_A952ContagemItem_TipoUnidade ;
      private decimal[] P00U62_A951ContagemItem_PFL ;
      private bool[] P00U62_n951ContagemItem_PFL ;
      private decimal[] P00U62_A950ContagemItem_PFB ;
      private bool[] P00U62_n950ContagemItem_PFB ;
      private int[] P00U62_A192Contagem_Codigo ;
      private int[] P00U62_A224ContagemItem_Lancamento ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17TFContagemItem_TipoUnidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
   }

   public class getwwcontagemitemfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00U62( IGxContext context ,
                                             String A952ContagemItem_TipoUnidade ,
                                             IGxCollection AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels ,
                                             int AV43WWContagemItemDS_1_Tfcontagem_codigo ,
                                             int AV44WWContagemItemDS_2_Tfcontagem_codigo_to ,
                                             decimal AV45WWContagemItemDS_3_Tfcontagemitem_pfb ,
                                             decimal AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to ,
                                             decimal AV47WWContagemItemDS_5_Tfcontagemitem_pfl ,
                                             decimal AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to ,
                                             int AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count ,
                                             String AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel ,
                                             String AV50WWContagemItemDS_8_Tfcontagemitem_evidencias ,
                                             int A192Contagem_Codigo ,
                                             decimal A950ContagemItem_PFB ,
                                             decimal A951ContagemItem_PFL ,
                                             String A953ContagemItem_Evidencias )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ContagemItem_Evidencias], [ContagemItem_TipoUnidade], [ContagemItem_PFL], [ContagemItem_PFB], [Contagem_Codigo], [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK)";
         if ( ! (0==AV43WWContagemItemDS_1_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contagem_Codigo] >= @AV43WWContagemItemDS_1_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contagem_Codigo] >= @AV43WWContagemItemDS_1_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ! (0==AV44WWContagemItemDS_2_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contagem_Codigo] <= @AV44WWContagemItemDS_2_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contagem_Codigo] <= @AV44WWContagemItemDS_2_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV45WWContagemItemDS_3_Tfcontagemitem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFB] >= @AV45WWContagemItemDS_3_Tfcontagemitem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFB] >= @AV45WWContagemItemDS_3_Tfcontagemitem_pfb)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFB] <= @AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFB] <= @AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV47WWContagemItemDS_5_Tfcontagemitem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFL] >= @AV47WWContagemItemDS_5_Tfcontagemitem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFL] >= @AV47WWContagemItemDS_5_Tfcontagemitem_pfl)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_PFL] <= @AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_PFL] <= @AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels, "[ContagemItem_TipoUnidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49WWContagemItemDS_7_Tfcontagemitem_tipounidade_sels, "[ContagemItem_TipoUnidade] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContagemItemDS_8_Tfcontagemitem_evidencias)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_Evidencias] like @lV50WWContagemItemDS_8_Tfcontagemitem_evidencias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_Evidencias] like @lV50WWContagemItemDS_8_Tfcontagemitem_evidencias)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemItem_Evidencias] = @AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemItem_Evidencias] = @AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContagemItem_Evidencias]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00U62(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (String)dynConstraints[14] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00U62 ;
          prmP00U62 = new Object[] {
          new Object[] {"@AV43WWContagemItemDS_1_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44WWContagemItemDS_2_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45WWContagemItemDS_3_Tfcontagemitem_pfb",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV46WWContagemItemDS_4_Tfcontagemitem_pfb_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV47WWContagemItemDS_5_Tfcontagemitem_pfl",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV48WWContagemItemDS_6_Tfcontagemitem_pfl_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@lV50WWContagemItemDS_8_Tfcontagemitem_evidencias",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV51WWContagemItemDS_9_Tfcontagemitem_evidencias_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00U62", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U62,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemitemfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontagemitemfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontagemitemfilterdata") )
          {
             return  ;
          }
          getwwcontagemitemfilterdata worker = new getwwcontagemitemfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
