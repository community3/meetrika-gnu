/*
               File: Tabela_BC
        Description: Tabela
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:41.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tabela_bc : GXHttpHandler, IGxSilentTrn
   {
      public tabela_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tabela_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1239( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1239( ) ;
         standaloneModal( ) ;
         AddRow1239( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11122 */
            E11122 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z172Tabela_Codigo = A172Tabela_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_120( )
      {
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1239( ) ;
            }
            else
            {
               CheckExtendedTable1239( ) ;
               if ( AnyError == 0 )
               {
                  ZM1239( 11) ;
                  ZM1239( 12) ;
                  ZM1239( 13) ;
                  ZM1239( 14) ;
               }
               CloseExtendedTableCursors1239( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12122( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV19Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV20GXV1 = 1;
            while ( AV20GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV20GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_SistemaCod") == 0 )
               {
                  AV11Insert_Tabela_SistemaCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_ModuloCod") == 0 )
               {
                  AV12Insert_Tabela_ModuloCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_PaiCod") == 0 )
               {
                  AV13Insert_Tabela_PaiCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_MelhoraCod") == 0 )
               {
                  AV18Insert_Tabela_MelhoraCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV20GXV1 = (int)(AV20GXV1+1);
            }
         }
      }

      protected void E11122( )
      {
         /* After Trn Routine */
      }

      protected void E13122( )
      {
         /* 'DoEliminar' Routine */
         new prc_desativarregistro(context ).execute(  "Tbl",  AV7Tabela_Codigo,  0) ;
      }

      protected void E14122( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void E15122( )
      {
         /* 'DoCopiarColar' Routine */
         context.wjLoc = formatLink("wp_copiarcolar.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode(StringUtil.RTrim("Tab"));
         context.wjLocDisableFrm = 1;
      }

      protected void ZM1239( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z173Tabela_Nome = A173Tabela_Nome;
            Z174Tabela_Ativo = A174Tabela_Ativo;
            Z188Tabela_ModuloCod = A188Tabela_ModuloCod;
            Z190Tabela_SistemaCod = A190Tabela_SistemaCod;
            Z181Tabela_PaiCod = A181Tabela_PaiCod;
            Z746Tabela_MelhoraCod = A746Tabela_MelhoraCod;
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z189Tabela_ModuloDes = A189Tabela_ModuloDes;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z191Tabela_SistemaDes = A191Tabela_SistemaDes;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z182Tabela_PaiNom = A182Tabela_PaiNom;
         }
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -10 )
         {
            Z172Tabela_Codigo = A172Tabela_Codigo;
            Z173Tabela_Nome = A173Tabela_Nome;
            Z175Tabela_Descricao = A175Tabela_Descricao;
            Z174Tabela_Ativo = A174Tabela_Ativo;
            Z188Tabela_ModuloCod = A188Tabela_ModuloCod;
            Z190Tabela_SistemaCod = A190Tabela_SistemaCod;
            Z181Tabela_PaiCod = A181Tabela_PaiCod;
            Z746Tabela_MelhoraCod = A746Tabela_MelhoraCod;
            Z191Tabela_SistemaDes = A191Tabela_SistemaDes;
            Z189Tabela_ModuloDes = A189Tabela_ModuloDes;
            Z182Tabela_PaiNom = A182Tabela_PaiNom;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV19Pgmname = "Tabela_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A174Tabela_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A174Tabela_Ativo = true;
         }
      }

      protected void Load1239( )
      {
         /* Using cursor BC00128 */
         pr_default.execute(6, new Object[] {A172Tabela_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound39 = 1;
            A173Tabela_Nome = BC00128_A173Tabela_Nome[0];
            A175Tabela_Descricao = BC00128_A175Tabela_Descricao[0];
            n175Tabela_Descricao = BC00128_n175Tabela_Descricao[0];
            A191Tabela_SistemaDes = BC00128_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = BC00128_n191Tabela_SistemaDes[0];
            A189Tabela_ModuloDes = BC00128_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = BC00128_n189Tabela_ModuloDes[0];
            A182Tabela_PaiNom = BC00128_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = BC00128_n182Tabela_PaiNom[0];
            A174Tabela_Ativo = BC00128_A174Tabela_Ativo[0];
            A188Tabela_ModuloCod = BC00128_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = BC00128_n188Tabela_ModuloCod[0];
            A190Tabela_SistemaCod = BC00128_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = BC00128_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = BC00128_n181Tabela_PaiCod[0];
            A746Tabela_MelhoraCod = BC00128_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = BC00128_n746Tabela_MelhoraCod[0];
            ZM1239( -10) ;
         }
         pr_default.close(6);
         OnLoadActions1239( ) ;
      }

      protected void OnLoadActions1239( )
      {
         AV17Sistema_Codigo = A190Tabela_SistemaCod;
         AV16Tabela_SistemaCod = A190Tabela_SistemaCod;
         AV10WebSession.Set("Sistema_Codigo", StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A188Tabela_ModuloCod) )
         {
            A188Tabela_ModuloCod = 0;
            n188Tabela_ModuloCod = false;
            n188Tabela_ModuloCod = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A181Tabela_PaiCod) )
         {
            A181Tabela_PaiCod = 0;
            n181Tabela_PaiCod = false;
            n181Tabela_PaiCod = true;
         }
      }

      protected void CheckExtendedTable1239( )
      {
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A173Tabela_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00125 */
         pr_default.execute(3, new Object[] {A190Tabela_SistemaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Tabela_Sistema'.", "ForeignKeyNotFound", 1, "TABELA_SISTEMACOD");
            AnyError = 1;
         }
         A191Tabela_SistemaDes = BC00125_A191Tabela_SistemaDes[0];
         n191Tabela_SistemaDes = BC00125_n191Tabela_SistemaDes[0];
         pr_default.close(3);
         AV17Sistema_Codigo = A190Tabela_SistemaCod;
         AV16Tabela_SistemaCod = A190Tabela_SistemaCod;
         AV10WebSession.Set("Sistema_Codigo", StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A188Tabela_ModuloCod) )
         {
            A188Tabela_ModuloCod = 0;
            n188Tabela_ModuloCod = false;
            n188Tabela_ModuloCod = true;
         }
         /* Using cursor BC00124 */
         pr_default.execute(2, new Object[] {n188Tabela_ModuloCod, A188Tabela_ModuloCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A188Tabela_ModuloCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Modulo'.", "ForeignKeyNotFound", 1, "TABELA_MODULOCOD");
               AnyError = 1;
            }
         }
         A189Tabela_ModuloDes = BC00124_A189Tabela_ModuloDes[0];
         n189Tabela_ModuloDes = BC00124_n189Tabela_ModuloDes[0];
         pr_default.close(2);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A181Tabela_PaiCod) )
         {
            A181Tabela_PaiCod = 0;
            n181Tabela_PaiCod = false;
            n181Tabela_PaiCod = true;
         }
         /* Using cursor BC00126 */
         pr_default.execute(4, new Object[] {n181Tabela_PaiCod, A181Tabela_PaiCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A181Tabela_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Pai'.", "ForeignKeyNotFound", 1, "TABELA_PAICOD");
               AnyError = 1;
            }
         }
         A182Tabela_PaiNom = BC00126_A182Tabela_PaiNom[0];
         n182Tabela_PaiNom = BC00126_n182Tabela_PaiNom[0];
         pr_default.close(4);
         /* Using cursor BC00127 */
         pr_default.execute(5, new Object[] {n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A746Tabela_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Tabela_Tabela Melhora'.", "ForeignKeyNotFound", 1, "TABELA_MELHORACOD");
               AnyError = 1;
            }
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors1239( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1239( )
      {
         /* Using cursor BC00129 */
         pr_default.execute(7, new Object[] {A172Tabela_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound39 = 1;
         }
         else
         {
            RcdFound39 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00123 */
         pr_default.execute(1, new Object[] {A172Tabela_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1239( 10) ;
            RcdFound39 = 1;
            A172Tabela_Codigo = BC00123_A172Tabela_Codigo[0];
            A173Tabela_Nome = BC00123_A173Tabela_Nome[0];
            A175Tabela_Descricao = BC00123_A175Tabela_Descricao[0];
            n175Tabela_Descricao = BC00123_n175Tabela_Descricao[0];
            A174Tabela_Ativo = BC00123_A174Tabela_Ativo[0];
            A188Tabela_ModuloCod = BC00123_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = BC00123_n188Tabela_ModuloCod[0];
            A190Tabela_SistemaCod = BC00123_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = BC00123_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = BC00123_n181Tabela_PaiCod[0];
            A746Tabela_MelhoraCod = BC00123_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = BC00123_n746Tabela_MelhoraCod[0];
            Z172Tabela_Codigo = A172Tabela_Codigo;
            sMode39 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1239( ) ;
            if ( AnyError == 1 )
            {
               RcdFound39 = 0;
               InitializeNonKey1239( ) ;
            }
            Gx_mode = sMode39;
         }
         else
         {
            RcdFound39 = 0;
            InitializeNonKey1239( ) ;
            sMode39 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode39;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1239( ) ;
         if ( RcdFound39 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_120( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1239( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00122 */
            pr_default.execute(0, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tabela"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z173Tabela_Nome, BC00122_A173Tabela_Nome[0]) != 0 ) || ( Z174Tabela_Ativo != BC00122_A174Tabela_Ativo[0] ) || ( Z188Tabela_ModuloCod != BC00122_A188Tabela_ModuloCod[0] ) || ( Z190Tabela_SistemaCod != BC00122_A190Tabela_SistemaCod[0] ) || ( Z181Tabela_PaiCod != BC00122_A181Tabela_PaiCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z746Tabela_MelhoraCod != BC00122_A746Tabela_MelhoraCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Tabela"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1239( )
      {
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1239( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1239( 0) ;
            CheckOptimisticConcurrency1239( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1239( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1239( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001210 */
                     pr_default.execute(8, new Object[] {A173Tabela_Nome, n175Tabela_Descricao, A175Tabela_Descricao, A174Tabela_Ativo, n188Tabela_ModuloCod, A188Tabela_ModuloCod, A190Tabela_SistemaCod, n181Tabela_PaiCod, A181Tabela_PaiCod, n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
                     A172Tabela_Codigo = BC001210_A172Tabela_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1239( ) ;
            }
            EndLevel1239( ) ;
         }
         CloseExtendedTableCursors1239( ) ;
      }

      protected void Update1239( )
      {
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1239( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1239( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1239( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1239( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001211 */
                     pr_default.execute(9, new Object[] {A173Tabela_Nome, n175Tabela_Descricao, A175Tabela_Descricao, A174Tabela_Ativo, n188Tabela_ModuloCod, A188Tabela_ModuloCod, A190Tabela_SistemaCod, n181Tabela_PaiCod, A181Tabela_PaiCod, n746Tabela_MelhoraCod, A746Tabela_MelhoraCod, A172Tabela_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tabela"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1239( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1239( ) ;
         }
         CloseExtendedTableCursors1239( ) ;
      }

      protected void DeferredUpdate1239( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1239( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1239( ) ;
            AfterConfirm1239( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1239( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001212 */
                  pr_default.execute(10, new Object[] {A172Tabela_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode39 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1239( ) ;
         Gx_mode = sMode39;
      }

      protected void OnDeleteControls1239( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001213 */
            pr_default.execute(11, new Object[] {A190Tabela_SistemaCod});
            A191Tabela_SistemaDes = BC001213_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = BC001213_n191Tabela_SistemaDes[0];
            pr_default.close(11);
            AV17Sistema_Codigo = A190Tabela_SistemaCod;
            AV16Tabela_SistemaCod = A190Tabela_SistemaCod;
            AV10WebSession.Set("Sistema_Codigo", StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
            /* Using cursor BC001214 */
            pr_default.execute(12, new Object[] {n188Tabela_ModuloCod, A188Tabela_ModuloCod});
            A189Tabela_ModuloDes = BC001214_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = BC001214_n189Tabela_ModuloDes[0];
            pr_default.close(12);
            /* Using cursor BC001215 */
            pr_default.execute(13, new Object[] {n181Tabela_PaiCod, A181Tabela_PaiCod});
            A182Tabela_PaiNom = BC001215_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = BC001215_n182Tabela_PaiNom[0];
            pr_default.close(13);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC001216 */
            pr_default.execute(14, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor BC001217 */
            pr_default.execute(15, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor BC001218 */
            pr_default.execute(16, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor BC001219 */
            pr_default.execute(17, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao Dados Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
         }
      }

      protected void EndLevel1239( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1239( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1239( )
      {
         /* Scan By routine */
         /* Using cursor BC001220 */
         pr_default.execute(18, new Object[] {A172Tabela_Codigo});
         RcdFound39 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound39 = 1;
            A172Tabela_Codigo = BC001220_A172Tabela_Codigo[0];
            A173Tabela_Nome = BC001220_A173Tabela_Nome[0];
            A175Tabela_Descricao = BC001220_A175Tabela_Descricao[0];
            n175Tabela_Descricao = BC001220_n175Tabela_Descricao[0];
            A191Tabela_SistemaDes = BC001220_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = BC001220_n191Tabela_SistemaDes[0];
            A189Tabela_ModuloDes = BC001220_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = BC001220_n189Tabela_ModuloDes[0];
            A182Tabela_PaiNom = BC001220_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = BC001220_n182Tabela_PaiNom[0];
            A174Tabela_Ativo = BC001220_A174Tabela_Ativo[0];
            A188Tabela_ModuloCod = BC001220_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = BC001220_n188Tabela_ModuloCod[0];
            A190Tabela_SistemaCod = BC001220_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = BC001220_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = BC001220_n181Tabela_PaiCod[0];
            A746Tabela_MelhoraCod = BC001220_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = BC001220_n746Tabela_MelhoraCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1239( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound39 = 0;
         ScanKeyLoad1239( ) ;
      }

      protected void ScanKeyLoad1239( )
      {
         sMode39 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound39 = 1;
            A172Tabela_Codigo = BC001220_A172Tabela_Codigo[0];
            A173Tabela_Nome = BC001220_A173Tabela_Nome[0];
            A175Tabela_Descricao = BC001220_A175Tabela_Descricao[0];
            n175Tabela_Descricao = BC001220_n175Tabela_Descricao[0];
            A191Tabela_SistemaDes = BC001220_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = BC001220_n191Tabela_SistemaDes[0];
            A189Tabela_ModuloDes = BC001220_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = BC001220_n189Tabela_ModuloDes[0];
            A182Tabela_PaiNom = BC001220_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = BC001220_n182Tabela_PaiNom[0];
            A174Tabela_Ativo = BC001220_A174Tabela_Ativo[0];
            A188Tabela_ModuloCod = BC001220_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = BC001220_n188Tabela_ModuloCod[0];
            A190Tabela_SistemaCod = BC001220_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = BC001220_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = BC001220_n181Tabela_PaiCod[0];
            A746Tabela_MelhoraCod = BC001220_A746Tabela_MelhoraCod[0];
            n746Tabela_MelhoraCod = BC001220_n746Tabela_MelhoraCod[0];
         }
         Gx_mode = sMode39;
      }

      protected void ScanKeyEnd1239( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm1239( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1239( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1239( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1239( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1239( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1239( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1239( )
      {
      }

      protected void AddRow1239( )
      {
         VarsToRow39( bcTabela) ;
      }

      protected void ReadRow1239( )
      {
         RowToVars39( bcTabela, 1) ;
      }

      protected void InitializeNonKey1239( )
      {
         AV17Sistema_Codigo = 0;
         AV16Tabela_SistemaCod = 0;
         A173Tabela_Nome = "";
         A175Tabela_Descricao = "";
         n175Tabela_Descricao = false;
         A190Tabela_SistemaCod = 0;
         A191Tabela_SistemaDes = "";
         n191Tabela_SistemaDes = false;
         A188Tabela_ModuloCod = 0;
         n188Tabela_ModuloCod = false;
         A189Tabela_ModuloDes = "";
         n189Tabela_ModuloDes = false;
         A181Tabela_PaiCod = 0;
         n181Tabela_PaiCod = false;
         A182Tabela_PaiNom = "";
         n182Tabela_PaiNom = false;
         A746Tabela_MelhoraCod = 0;
         n746Tabela_MelhoraCod = false;
         A174Tabela_Ativo = true;
         Z173Tabela_Nome = "";
         Z174Tabela_Ativo = false;
         Z188Tabela_ModuloCod = 0;
         Z190Tabela_SistemaCod = 0;
         Z181Tabela_PaiCod = 0;
         Z746Tabela_MelhoraCod = 0;
      }

      protected void InitAll1239( )
      {
         A172Tabela_Codigo = 0;
         InitializeNonKey1239( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A174Tabela_Ativo = i174Tabela_Ativo;
      }

      public void VarsToRow39( SdtTabela obj39 )
      {
         obj39.gxTpr_Mode = Gx_mode;
         obj39.gxTpr_Tabela_nome = A173Tabela_Nome;
         obj39.gxTpr_Tabela_descricao = A175Tabela_Descricao;
         obj39.gxTpr_Tabela_sistemacod = A190Tabela_SistemaCod;
         obj39.gxTpr_Tabela_sistemades = A191Tabela_SistemaDes;
         obj39.gxTpr_Tabela_modulocod = A188Tabela_ModuloCod;
         obj39.gxTpr_Tabela_modulodes = A189Tabela_ModuloDes;
         obj39.gxTpr_Tabela_paicod = A181Tabela_PaiCod;
         obj39.gxTpr_Tabela_painom = A182Tabela_PaiNom;
         obj39.gxTpr_Tabela_melhoracod = A746Tabela_MelhoraCod;
         obj39.gxTpr_Tabela_ativo = A174Tabela_Ativo;
         obj39.gxTpr_Tabela_codigo = A172Tabela_Codigo;
         obj39.gxTpr_Tabela_codigo_Z = Z172Tabela_Codigo;
         obj39.gxTpr_Tabela_nome_Z = Z173Tabela_Nome;
         obj39.gxTpr_Tabela_sistemacod_Z = Z190Tabela_SistemaCod;
         obj39.gxTpr_Tabela_sistemades_Z = Z191Tabela_SistemaDes;
         obj39.gxTpr_Tabela_modulocod_Z = Z188Tabela_ModuloCod;
         obj39.gxTpr_Tabela_modulodes_Z = Z189Tabela_ModuloDes;
         obj39.gxTpr_Tabela_paicod_Z = Z181Tabela_PaiCod;
         obj39.gxTpr_Tabela_painom_Z = Z182Tabela_PaiNom;
         obj39.gxTpr_Tabela_melhoracod_Z = Z746Tabela_MelhoraCod;
         obj39.gxTpr_Tabela_ativo_Z = Z174Tabela_Ativo;
         obj39.gxTpr_Tabela_descricao_N = (short)(Convert.ToInt16(n175Tabela_Descricao));
         obj39.gxTpr_Tabela_sistemades_N = (short)(Convert.ToInt16(n191Tabela_SistemaDes));
         obj39.gxTpr_Tabela_modulocod_N = (short)(Convert.ToInt16(n188Tabela_ModuloCod));
         obj39.gxTpr_Tabela_modulodes_N = (short)(Convert.ToInt16(n189Tabela_ModuloDes));
         obj39.gxTpr_Tabela_paicod_N = (short)(Convert.ToInt16(n181Tabela_PaiCod));
         obj39.gxTpr_Tabela_painom_N = (short)(Convert.ToInt16(n182Tabela_PaiNom));
         obj39.gxTpr_Tabela_melhoracod_N = (short)(Convert.ToInt16(n746Tabela_MelhoraCod));
         obj39.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow39( SdtTabela obj39 )
      {
         obj39.gxTpr_Tabela_codigo = A172Tabela_Codigo;
         return  ;
      }

      public void RowToVars39( SdtTabela obj39 ,
                               int forceLoad )
      {
         Gx_mode = obj39.gxTpr_Mode;
         A173Tabela_Nome = obj39.gxTpr_Tabela_nome;
         A175Tabela_Descricao = obj39.gxTpr_Tabela_descricao;
         n175Tabela_Descricao = false;
         A190Tabela_SistemaCod = obj39.gxTpr_Tabela_sistemacod;
         A191Tabela_SistemaDes = obj39.gxTpr_Tabela_sistemades;
         n191Tabela_SistemaDes = false;
         A188Tabela_ModuloCod = obj39.gxTpr_Tabela_modulocod;
         n188Tabela_ModuloCod = false;
         A189Tabela_ModuloDes = obj39.gxTpr_Tabela_modulodes;
         n189Tabela_ModuloDes = false;
         A181Tabela_PaiCod = obj39.gxTpr_Tabela_paicod;
         n181Tabela_PaiCod = false;
         A182Tabela_PaiNom = obj39.gxTpr_Tabela_painom;
         n182Tabela_PaiNom = false;
         A746Tabela_MelhoraCod = obj39.gxTpr_Tabela_melhoracod;
         n746Tabela_MelhoraCod = false;
         A174Tabela_Ativo = obj39.gxTpr_Tabela_ativo;
         A172Tabela_Codigo = obj39.gxTpr_Tabela_codigo;
         Z172Tabela_Codigo = obj39.gxTpr_Tabela_codigo_Z;
         Z173Tabela_Nome = obj39.gxTpr_Tabela_nome_Z;
         Z190Tabela_SistemaCod = obj39.gxTpr_Tabela_sistemacod_Z;
         Z191Tabela_SistemaDes = obj39.gxTpr_Tabela_sistemades_Z;
         Z188Tabela_ModuloCod = obj39.gxTpr_Tabela_modulocod_Z;
         Z189Tabela_ModuloDes = obj39.gxTpr_Tabela_modulodes_Z;
         Z181Tabela_PaiCod = obj39.gxTpr_Tabela_paicod_Z;
         Z182Tabela_PaiNom = obj39.gxTpr_Tabela_painom_Z;
         Z746Tabela_MelhoraCod = obj39.gxTpr_Tabela_melhoracod_Z;
         Z174Tabela_Ativo = obj39.gxTpr_Tabela_ativo_Z;
         n175Tabela_Descricao = (bool)(Convert.ToBoolean(obj39.gxTpr_Tabela_descricao_N));
         n191Tabela_SistemaDes = (bool)(Convert.ToBoolean(obj39.gxTpr_Tabela_sistemades_N));
         n188Tabela_ModuloCod = (bool)(Convert.ToBoolean(obj39.gxTpr_Tabela_modulocod_N));
         n189Tabela_ModuloDes = (bool)(Convert.ToBoolean(obj39.gxTpr_Tabela_modulodes_N));
         n181Tabela_PaiCod = (bool)(Convert.ToBoolean(obj39.gxTpr_Tabela_paicod_N));
         n182Tabela_PaiNom = (bool)(Convert.ToBoolean(obj39.gxTpr_Tabela_painom_N));
         n746Tabela_MelhoraCod = (bool)(Convert.ToBoolean(obj39.gxTpr_Tabela_melhoracod_N));
         Gx_mode = obj39.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A172Tabela_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1239( ) ;
         ScanKeyStart1239( ) ;
         if ( RcdFound39 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z172Tabela_Codigo = A172Tabela_Codigo;
         }
         ZM1239( -10) ;
         OnLoadActions1239( ) ;
         AddRow1239( ) ;
         ScanKeyEnd1239( ) ;
         if ( RcdFound39 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars39( bcTabela, 0) ;
         ScanKeyStart1239( ) ;
         if ( RcdFound39 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z172Tabela_Codigo = A172Tabela_Codigo;
         }
         ZM1239( -10) ;
         OnLoadActions1239( ) ;
         AddRow1239( ) ;
         ScanKeyEnd1239( ) ;
         if ( RcdFound39 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars39( bcTabela, 0) ;
         nKeyPressed = 1;
         GetKey1239( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1239( ) ;
         }
         else
         {
            if ( RcdFound39 == 1 )
            {
               if ( A172Tabela_Codigo != Z172Tabela_Codigo )
               {
                  A172Tabela_Codigo = Z172Tabela_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1239( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A172Tabela_Codigo != Z172Tabela_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1239( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1239( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow39( bcTabela) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars39( bcTabela, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1239( ) ;
         if ( RcdFound39 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A172Tabela_Codigo != Z172Tabela_Codigo )
            {
               A172Tabela_Codigo = Z172Tabela_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A172Tabela_Codigo != Z172Tabela_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(11);
         pr_default.close(13);
         context.RollbackDataStores( "Tabela_BC");
         VarsToRow39( bcTabela) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcTabela.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcTabela.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcTabela )
         {
            bcTabela = (SdtTabela)(sdt);
            if ( StringUtil.StrCmp(bcTabela.gxTpr_Mode, "") == 0 )
            {
               bcTabela.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow39( bcTabela) ;
            }
            else
            {
               RowToVars39( bcTabela, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcTabela.gxTpr_Mode, "") == 0 )
            {
               bcTabela.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars39( bcTabela, 1) ;
         return  ;
      }

      public SdtTabela Tabela_BC
      {
         get {
            return bcTabela ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(11);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV19Pgmname = "";
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z173Tabela_Nome = "";
         A173Tabela_Nome = "";
         Z189Tabela_ModuloDes = "";
         A189Tabela_ModuloDes = "";
         Z191Tabela_SistemaDes = "";
         A191Tabela_SistemaDes = "";
         Z182Tabela_PaiNom = "";
         A182Tabela_PaiNom = "";
         Z175Tabela_Descricao = "";
         A175Tabela_Descricao = "";
         BC00128_A172Tabela_Codigo = new int[1] ;
         BC00128_A173Tabela_Nome = new String[] {""} ;
         BC00128_A175Tabela_Descricao = new String[] {""} ;
         BC00128_n175Tabela_Descricao = new bool[] {false} ;
         BC00128_A191Tabela_SistemaDes = new String[] {""} ;
         BC00128_n191Tabela_SistemaDes = new bool[] {false} ;
         BC00128_A189Tabela_ModuloDes = new String[] {""} ;
         BC00128_n189Tabela_ModuloDes = new bool[] {false} ;
         BC00128_A182Tabela_PaiNom = new String[] {""} ;
         BC00128_n182Tabela_PaiNom = new bool[] {false} ;
         BC00128_A174Tabela_Ativo = new bool[] {false} ;
         BC00128_A188Tabela_ModuloCod = new int[1] ;
         BC00128_n188Tabela_ModuloCod = new bool[] {false} ;
         BC00128_A190Tabela_SistemaCod = new int[1] ;
         BC00128_A181Tabela_PaiCod = new int[1] ;
         BC00128_n181Tabela_PaiCod = new bool[] {false} ;
         BC00128_A746Tabela_MelhoraCod = new int[1] ;
         BC00128_n746Tabela_MelhoraCod = new bool[] {false} ;
         BC00125_A191Tabela_SistemaDes = new String[] {""} ;
         BC00125_n191Tabela_SistemaDes = new bool[] {false} ;
         BC00124_A189Tabela_ModuloDes = new String[] {""} ;
         BC00124_n189Tabela_ModuloDes = new bool[] {false} ;
         BC00126_A182Tabela_PaiNom = new String[] {""} ;
         BC00126_n182Tabela_PaiNom = new bool[] {false} ;
         BC00127_A746Tabela_MelhoraCod = new int[1] ;
         BC00127_n746Tabela_MelhoraCod = new bool[] {false} ;
         BC00129_A172Tabela_Codigo = new int[1] ;
         BC00123_A172Tabela_Codigo = new int[1] ;
         BC00123_A173Tabela_Nome = new String[] {""} ;
         BC00123_A175Tabela_Descricao = new String[] {""} ;
         BC00123_n175Tabela_Descricao = new bool[] {false} ;
         BC00123_A174Tabela_Ativo = new bool[] {false} ;
         BC00123_A188Tabela_ModuloCod = new int[1] ;
         BC00123_n188Tabela_ModuloCod = new bool[] {false} ;
         BC00123_A190Tabela_SistemaCod = new int[1] ;
         BC00123_A181Tabela_PaiCod = new int[1] ;
         BC00123_n181Tabela_PaiCod = new bool[] {false} ;
         BC00123_A746Tabela_MelhoraCod = new int[1] ;
         BC00123_n746Tabela_MelhoraCod = new bool[] {false} ;
         sMode39 = "";
         BC00122_A172Tabela_Codigo = new int[1] ;
         BC00122_A173Tabela_Nome = new String[] {""} ;
         BC00122_A175Tabela_Descricao = new String[] {""} ;
         BC00122_n175Tabela_Descricao = new bool[] {false} ;
         BC00122_A174Tabela_Ativo = new bool[] {false} ;
         BC00122_A188Tabela_ModuloCod = new int[1] ;
         BC00122_n188Tabela_ModuloCod = new bool[] {false} ;
         BC00122_A190Tabela_SistemaCod = new int[1] ;
         BC00122_A181Tabela_PaiCod = new int[1] ;
         BC00122_n181Tabela_PaiCod = new bool[] {false} ;
         BC00122_A746Tabela_MelhoraCod = new int[1] ;
         BC00122_n746Tabela_MelhoraCod = new bool[] {false} ;
         BC001210_A172Tabela_Codigo = new int[1] ;
         BC001213_A191Tabela_SistemaDes = new String[] {""} ;
         BC001213_n191Tabela_SistemaDes = new bool[] {false} ;
         BC001214_A189Tabela_ModuloDes = new String[] {""} ;
         BC001214_n189Tabela_ModuloDes = new bool[] {false} ;
         BC001215_A182Tabela_PaiNom = new String[] {""} ;
         BC001215_n182Tabela_PaiNom = new bool[] {false} ;
         BC001216_A746Tabela_MelhoraCod = new int[1] ;
         BC001216_n746Tabela_MelhoraCod = new bool[] {false} ;
         BC001217_A181Tabela_PaiCod = new int[1] ;
         BC001217_n181Tabela_PaiCod = new bool[] {false} ;
         BC001218_A176Atributos_Codigo = new int[1] ;
         BC001219_A368FuncaoDados_Codigo = new int[1] ;
         BC001219_A172Tabela_Codigo = new int[1] ;
         BC001220_A172Tabela_Codigo = new int[1] ;
         BC001220_A173Tabela_Nome = new String[] {""} ;
         BC001220_A175Tabela_Descricao = new String[] {""} ;
         BC001220_n175Tabela_Descricao = new bool[] {false} ;
         BC001220_A191Tabela_SistemaDes = new String[] {""} ;
         BC001220_n191Tabela_SistemaDes = new bool[] {false} ;
         BC001220_A189Tabela_ModuloDes = new String[] {""} ;
         BC001220_n189Tabela_ModuloDes = new bool[] {false} ;
         BC001220_A182Tabela_PaiNom = new String[] {""} ;
         BC001220_n182Tabela_PaiNom = new bool[] {false} ;
         BC001220_A174Tabela_Ativo = new bool[] {false} ;
         BC001220_A188Tabela_ModuloCod = new int[1] ;
         BC001220_n188Tabela_ModuloCod = new bool[] {false} ;
         BC001220_A190Tabela_SistemaCod = new int[1] ;
         BC001220_A181Tabela_PaiCod = new int[1] ;
         BC001220_n181Tabela_PaiCod = new bool[] {false} ;
         BC001220_A746Tabela_MelhoraCod = new int[1] ;
         BC001220_n746Tabela_MelhoraCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tabela_bc__default(),
            new Object[][] {
                new Object[] {
               BC00122_A172Tabela_Codigo, BC00122_A173Tabela_Nome, BC00122_A175Tabela_Descricao, BC00122_n175Tabela_Descricao, BC00122_A174Tabela_Ativo, BC00122_A188Tabela_ModuloCod, BC00122_n188Tabela_ModuloCod, BC00122_A190Tabela_SistemaCod, BC00122_A181Tabela_PaiCod, BC00122_n181Tabela_PaiCod,
               BC00122_A746Tabela_MelhoraCod, BC00122_n746Tabela_MelhoraCod
               }
               , new Object[] {
               BC00123_A172Tabela_Codigo, BC00123_A173Tabela_Nome, BC00123_A175Tabela_Descricao, BC00123_n175Tabela_Descricao, BC00123_A174Tabela_Ativo, BC00123_A188Tabela_ModuloCod, BC00123_n188Tabela_ModuloCod, BC00123_A190Tabela_SistemaCod, BC00123_A181Tabela_PaiCod, BC00123_n181Tabela_PaiCod,
               BC00123_A746Tabela_MelhoraCod, BC00123_n746Tabela_MelhoraCod
               }
               , new Object[] {
               BC00124_A189Tabela_ModuloDes, BC00124_n189Tabela_ModuloDes
               }
               , new Object[] {
               BC00125_A191Tabela_SistemaDes, BC00125_n191Tabela_SistemaDes
               }
               , new Object[] {
               BC00126_A182Tabela_PaiNom, BC00126_n182Tabela_PaiNom
               }
               , new Object[] {
               BC00127_A746Tabela_MelhoraCod
               }
               , new Object[] {
               BC00128_A172Tabela_Codigo, BC00128_A173Tabela_Nome, BC00128_A175Tabela_Descricao, BC00128_n175Tabela_Descricao, BC00128_A191Tabela_SistemaDes, BC00128_n191Tabela_SistemaDes, BC00128_A189Tabela_ModuloDes, BC00128_n189Tabela_ModuloDes, BC00128_A182Tabela_PaiNom, BC00128_n182Tabela_PaiNom,
               BC00128_A174Tabela_Ativo, BC00128_A188Tabela_ModuloCod, BC00128_n188Tabela_ModuloCod, BC00128_A190Tabela_SistemaCod, BC00128_A181Tabela_PaiCod, BC00128_n181Tabela_PaiCod, BC00128_A746Tabela_MelhoraCod, BC00128_n746Tabela_MelhoraCod
               }
               , new Object[] {
               BC00129_A172Tabela_Codigo
               }
               , new Object[] {
               BC001210_A172Tabela_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001213_A191Tabela_SistemaDes, BC001213_n191Tabela_SistemaDes
               }
               , new Object[] {
               BC001214_A189Tabela_ModuloDes, BC001214_n189Tabela_ModuloDes
               }
               , new Object[] {
               BC001215_A182Tabela_PaiNom, BC001215_n182Tabela_PaiNom
               }
               , new Object[] {
               BC001216_A746Tabela_MelhoraCod
               }
               , new Object[] {
               BC001217_A181Tabela_PaiCod
               }
               , new Object[] {
               BC001218_A176Atributos_Codigo
               }
               , new Object[] {
               BC001219_A368FuncaoDados_Codigo, BC001219_A172Tabela_Codigo
               }
               , new Object[] {
               BC001220_A172Tabela_Codigo, BC001220_A173Tabela_Nome, BC001220_A175Tabela_Descricao, BC001220_n175Tabela_Descricao, BC001220_A191Tabela_SistemaDes, BC001220_n191Tabela_SistemaDes, BC001220_A189Tabela_ModuloDes, BC001220_n189Tabela_ModuloDes, BC001220_A182Tabela_PaiNom, BC001220_n182Tabela_PaiNom,
               BC001220_A174Tabela_Ativo, BC001220_A188Tabela_ModuloCod, BC001220_n188Tabela_ModuloCod, BC001220_A190Tabela_SistemaCod, BC001220_A181Tabela_PaiCod, BC001220_n181Tabela_PaiCod, BC001220_A746Tabela_MelhoraCod, BC001220_n746Tabela_MelhoraCod
               }
            }
         );
         Z174Tabela_Ativo = true;
         A174Tabela_Ativo = true;
         i174Tabela_Ativo = true;
         AV19Pgmname = "Tabela_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12122 */
         E12122 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound39 ;
      private int trnEnded ;
      private int Z172Tabela_Codigo ;
      private int A172Tabela_Codigo ;
      private int AV20GXV1 ;
      private int AV11Insert_Tabela_SistemaCod ;
      private int AV12Insert_Tabela_ModuloCod ;
      private int AV13Insert_Tabela_PaiCod ;
      private int AV18Insert_Tabela_MelhoraCod ;
      private int AV7Tabela_Codigo ;
      private int A190Tabela_SistemaCod ;
      private int Z188Tabela_ModuloCod ;
      private int A188Tabela_ModuloCod ;
      private int Z190Tabela_SistemaCod ;
      private int Z181Tabela_PaiCod ;
      private int A181Tabela_PaiCod ;
      private int Z746Tabela_MelhoraCod ;
      private int A746Tabela_MelhoraCod ;
      private int AV17Sistema_Codigo ;
      private int AV16Tabela_SistemaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV19Pgmname ;
      private String Z173Tabela_Nome ;
      private String A173Tabela_Nome ;
      private String Z189Tabela_ModuloDes ;
      private String A189Tabela_ModuloDes ;
      private String Z182Tabela_PaiNom ;
      private String A182Tabela_PaiNom ;
      private String sMode39 ;
      private bool Z174Tabela_Ativo ;
      private bool A174Tabela_Ativo ;
      private bool n175Tabela_Descricao ;
      private bool n191Tabela_SistemaDes ;
      private bool n189Tabela_ModuloDes ;
      private bool n182Tabela_PaiNom ;
      private bool n188Tabela_ModuloCod ;
      private bool n181Tabela_PaiCod ;
      private bool n746Tabela_MelhoraCod ;
      private bool Gx_longc ;
      private bool i174Tabela_Ativo ;
      private String Z175Tabela_Descricao ;
      private String A175Tabela_Descricao ;
      private String Z191Tabela_SistemaDes ;
      private String A191Tabela_SistemaDes ;
      private IGxSession AV10WebSession ;
      private SdtTabela bcTabela ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00128_A172Tabela_Codigo ;
      private String[] BC00128_A173Tabela_Nome ;
      private String[] BC00128_A175Tabela_Descricao ;
      private bool[] BC00128_n175Tabela_Descricao ;
      private String[] BC00128_A191Tabela_SistemaDes ;
      private bool[] BC00128_n191Tabela_SistemaDes ;
      private String[] BC00128_A189Tabela_ModuloDes ;
      private bool[] BC00128_n189Tabela_ModuloDes ;
      private String[] BC00128_A182Tabela_PaiNom ;
      private bool[] BC00128_n182Tabela_PaiNom ;
      private bool[] BC00128_A174Tabela_Ativo ;
      private int[] BC00128_A188Tabela_ModuloCod ;
      private bool[] BC00128_n188Tabela_ModuloCod ;
      private int[] BC00128_A190Tabela_SistemaCod ;
      private int[] BC00128_A181Tabela_PaiCod ;
      private bool[] BC00128_n181Tabela_PaiCod ;
      private int[] BC00128_A746Tabela_MelhoraCod ;
      private bool[] BC00128_n746Tabela_MelhoraCod ;
      private String[] BC00125_A191Tabela_SistemaDes ;
      private bool[] BC00125_n191Tabela_SistemaDes ;
      private String[] BC00124_A189Tabela_ModuloDes ;
      private bool[] BC00124_n189Tabela_ModuloDes ;
      private String[] BC00126_A182Tabela_PaiNom ;
      private bool[] BC00126_n182Tabela_PaiNom ;
      private int[] BC00127_A746Tabela_MelhoraCod ;
      private bool[] BC00127_n746Tabela_MelhoraCod ;
      private int[] BC00129_A172Tabela_Codigo ;
      private int[] BC00123_A172Tabela_Codigo ;
      private String[] BC00123_A173Tabela_Nome ;
      private String[] BC00123_A175Tabela_Descricao ;
      private bool[] BC00123_n175Tabela_Descricao ;
      private bool[] BC00123_A174Tabela_Ativo ;
      private int[] BC00123_A188Tabela_ModuloCod ;
      private bool[] BC00123_n188Tabela_ModuloCod ;
      private int[] BC00123_A190Tabela_SistemaCod ;
      private int[] BC00123_A181Tabela_PaiCod ;
      private bool[] BC00123_n181Tabela_PaiCod ;
      private int[] BC00123_A746Tabela_MelhoraCod ;
      private bool[] BC00123_n746Tabela_MelhoraCod ;
      private int[] BC00122_A172Tabela_Codigo ;
      private String[] BC00122_A173Tabela_Nome ;
      private String[] BC00122_A175Tabela_Descricao ;
      private bool[] BC00122_n175Tabela_Descricao ;
      private bool[] BC00122_A174Tabela_Ativo ;
      private int[] BC00122_A188Tabela_ModuloCod ;
      private bool[] BC00122_n188Tabela_ModuloCod ;
      private int[] BC00122_A190Tabela_SistemaCod ;
      private int[] BC00122_A181Tabela_PaiCod ;
      private bool[] BC00122_n181Tabela_PaiCod ;
      private int[] BC00122_A746Tabela_MelhoraCod ;
      private bool[] BC00122_n746Tabela_MelhoraCod ;
      private int[] BC001210_A172Tabela_Codigo ;
      private String[] BC001213_A191Tabela_SistemaDes ;
      private bool[] BC001213_n191Tabela_SistemaDes ;
      private String[] BC001214_A189Tabela_ModuloDes ;
      private bool[] BC001214_n189Tabela_ModuloDes ;
      private String[] BC001215_A182Tabela_PaiNom ;
      private bool[] BC001215_n182Tabela_PaiNom ;
      private int[] BC001216_A746Tabela_MelhoraCod ;
      private bool[] BC001216_n746Tabela_MelhoraCod ;
      private int[] BC001217_A181Tabela_PaiCod ;
      private bool[] BC001217_n181Tabela_PaiCod ;
      private int[] BC001218_A176Atributos_Codigo ;
      private int[] BC001219_A368FuncaoDados_Codigo ;
      private int[] BC001219_A172Tabela_Codigo ;
      private int[] BC001220_A172Tabela_Codigo ;
      private String[] BC001220_A173Tabela_Nome ;
      private String[] BC001220_A175Tabela_Descricao ;
      private bool[] BC001220_n175Tabela_Descricao ;
      private String[] BC001220_A191Tabela_SistemaDes ;
      private bool[] BC001220_n191Tabela_SistemaDes ;
      private String[] BC001220_A189Tabela_ModuloDes ;
      private bool[] BC001220_n189Tabela_ModuloDes ;
      private String[] BC001220_A182Tabela_PaiNom ;
      private bool[] BC001220_n182Tabela_PaiNom ;
      private bool[] BC001220_A174Tabela_Ativo ;
      private int[] BC001220_A188Tabela_ModuloCod ;
      private bool[] BC001220_n188Tabela_ModuloCod ;
      private int[] BC001220_A190Tabela_SistemaCod ;
      private int[] BC001220_A181Tabela_PaiCod ;
      private bool[] BC001220_n181Tabela_PaiCod ;
      private int[] BC001220_A746Tabela_MelhoraCod ;
      private bool[] BC001220_n746Tabela_MelhoraCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
   }

   public class tabela_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00128 ;
          prmBC00128 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00125 ;
          prmBC00125 = new Object[] {
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00124 ;
          prmBC00124 = new Object[] {
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00126 ;
          prmBC00126 = new Object[] {
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00127 ;
          prmBC00127 = new Object[] {
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00129 ;
          prmBC00129 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00123 ;
          prmBC00123 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00122 ;
          prmBC00122 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001210 ;
          prmBC001210 = new Object[] {
          new Object[] {"@Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tabela_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Tabela_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001211 ;
          prmBC001211 = new Object[] {
          new Object[] {"@Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tabela_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Tabela_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001212 ;
          prmBC001212 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001213 ;
          prmBC001213 = new Object[] {
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001214 ;
          prmBC001214 = new Object[] {
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001215 ;
          prmBC001215 = new Object[] {
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001216 ;
          prmBC001216 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001217 ;
          prmBC001217 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001218 ;
          prmBC001218 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001219 ;
          prmBC001219 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001220 ;
          prmBC001220 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00122", "SELECT [Tabela_Codigo], [Tabela_Nome], [Tabela_Descricao], [Tabela_Ativo], [Tabela_ModuloCod] AS Tabela_ModuloCod, [Tabela_SistemaCod] AS Tabela_SistemaCod, [Tabela_PaiCod] AS Tabela_PaiCod, [Tabela_MelhoraCod] AS Tabela_MelhoraCod FROM [Tabela] WITH (UPDLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00122,1,0,true,false )
             ,new CursorDef("BC00123", "SELECT [Tabela_Codigo], [Tabela_Nome], [Tabela_Descricao], [Tabela_Ativo], [Tabela_ModuloCod] AS Tabela_ModuloCod, [Tabela_SistemaCod] AS Tabela_SistemaCod, [Tabela_PaiCod] AS Tabela_PaiCod, [Tabela_MelhoraCod] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00123,1,0,true,false )
             ,new CursorDef("BC00124", "SELECT [Modulo_Nome] AS Tabela_ModuloDes FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Tabela_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00124,1,0,true,false )
             ,new CursorDef("BC00125", "SELECT [Sistema_Nome] AS Tabela_SistemaDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Tabela_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00125,1,0,true,false )
             ,new CursorDef("BC00126", "SELECT [Tabela_Nome] AS Tabela_PaiNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00126,1,0,true,false )
             ,new CursorDef("BC00127", "SELECT [Tabela_Codigo] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00127,1,0,true,false )
             ,new CursorDef("BC00128", "SELECT TM1.[Tabela_Codigo], TM1.[Tabela_Nome], TM1.[Tabela_Descricao], T2.[Sistema_Nome] AS Tabela_SistemaDes, T3.[Modulo_Nome] AS Tabela_ModuloDes, T4.[Tabela_Nome] AS Tabela_PaiNom, TM1.[Tabela_Ativo], TM1.[Tabela_ModuloCod] AS Tabela_ModuloCod, TM1.[Tabela_SistemaCod] AS Tabela_SistemaCod, TM1.[Tabela_PaiCod] AS Tabela_PaiCod, TM1.[Tabela_MelhoraCod] AS Tabela_MelhoraCod FROM ((([Tabela] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[Tabela_SistemaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = TM1.[Tabela_ModuloCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = TM1.[Tabela_PaiCod]) WHERE TM1.[Tabela_Codigo] = @Tabela_Codigo ORDER BY TM1.[Tabela_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00128,100,0,true,false )
             ,new CursorDef("BC00129", "SELECT [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00129,1,0,true,false )
             ,new CursorDef("BC001210", "INSERT INTO [Tabela]([Tabela_Nome], [Tabela_Descricao], [Tabela_Ativo], [Tabela_ModuloCod], [Tabela_SistemaCod], [Tabela_PaiCod], [Tabela_MelhoraCod]) VALUES(@Tabela_Nome, @Tabela_Descricao, @Tabela_Ativo, @Tabela_ModuloCod, @Tabela_SistemaCod, @Tabela_PaiCod, @Tabela_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC001210)
             ,new CursorDef("BC001211", "UPDATE [Tabela] SET [Tabela_Nome]=@Tabela_Nome, [Tabela_Descricao]=@Tabela_Descricao, [Tabela_Ativo]=@Tabela_Ativo, [Tabela_ModuloCod]=@Tabela_ModuloCod, [Tabela_SistemaCod]=@Tabela_SistemaCod, [Tabela_PaiCod]=@Tabela_PaiCod, [Tabela_MelhoraCod]=@Tabela_MelhoraCod  WHERE [Tabela_Codigo] = @Tabela_Codigo", GxErrorMask.GX_NOMASK,prmBC001211)
             ,new CursorDef("BC001212", "DELETE FROM [Tabela]  WHERE [Tabela_Codigo] = @Tabela_Codigo", GxErrorMask.GX_NOMASK,prmBC001212)
             ,new CursorDef("BC001213", "SELECT [Sistema_Nome] AS Tabela_SistemaDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Tabela_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001213,1,0,true,false )
             ,new CursorDef("BC001214", "SELECT [Modulo_Nome] AS Tabela_ModuloDes FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Tabela_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001214,1,0,true,false )
             ,new CursorDef("BC001215", "SELECT [Tabela_Nome] AS Tabela_PaiNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001215,1,0,true,false )
             ,new CursorDef("BC001216", "SELECT TOP 1 [Tabela_Codigo] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_MelhoraCod] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001216,1,0,true,true )
             ,new CursorDef("BC001217", "SELECT TOP 1 [Tabela_Codigo] AS Tabela_PaiCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_PaiCod] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001217,1,0,true,true )
             ,new CursorDef("BC001218", "SELECT TOP 1 [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_TabelaCod] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001218,1,0,true,true )
             ,new CursorDef("BC001219", "SELECT TOP 1 [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001219,1,0,true,true )
             ,new CursorDef("BC001220", "SELECT TM1.[Tabela_Codigo], TM1.[Tabela_Nome], TM1.[Tabela_Descricao], T2.[Sistema_Nome] AS Tabela_SistemaDes, T3.[Modulo_Nome] AS Tabela_ModuloDes, T4.[Tabela_Nome] AS Tabela_PaiNom, TM1.[Tabela_Ativo], TM1.[Tabela_ModuloCod] AS Tabela_ModuloCod, TM1.[Tabela_SistemaCod] AS Tabela_SistemaCod, TM1.[Tabela_PaiCod] AS Tabela_PaiCod, TM1.[Tabela_MelhoraCod] AS Tabela_MelhoraCod FROM ((([Tabela] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[Tabela_SistemaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = TM1.[Tabela_ModuloCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = TM1.[Tabela_PaiCod]) WHERE TM1.[Tabela_Codigo] = @Tabela_Codigo ORDER BY TM1.[Tabela_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001220,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[10]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (bool)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[10]);
                }
                stmt.SetParameter(8, (int)parms[11]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
