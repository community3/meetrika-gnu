/*
               File: type_SdtSDT_MonitorDmn
        Description: SDT_MonitorDmn
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:43.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_MonitorDmn" )]
   [XmlType(TypeName =  "SDT_MonitorDmn" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_MonitorDmn : GxUserType
   {
      public SdtSDT_MonitorDmn( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_MonitorDmn_Status = "";
         gxTv_SdtSDT_MonitorDmn_Statuscnt = "";
         gxTv_SdtSDT_MonitorDmn_Statusdmnvnc = "";
         gxTv_SdtSDT_MonitorDmn_Prazo = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_MonitorDmn_Prazoentrega = "";
         gxTv_SdtSDT_MonitorDmn_Horaentrega = "";
         gxTv_SdtSDT_MonitorDmn_Datadmn = "";
         gxTv_SdtSDT_MonitorDmn_Dataentregareal = DateTime.MinValue;
         gxTv_SdtSDT_MonitorDmn_Demanda = "";
         gxTv_SdtSDT_MonitorDmn_Osfmosfs = "";
         gxTv_SdtSDT_MonitorDmn_Referencia = "";
         gxTv_SdtSDT_MonitorDmn_Descricao = "";
         gxTv_SdtSDT_MonitorDmn_Descricaocomplementar = "";
         gxTv_SdtSDT_MonitorDmn_Sistema = "";
         gxTv_SdtSDT_MonitorDmn_Modulo = "";
         gxTv_SdtSDT_MonitorDmn_Servico = "";
         gxTv_SdtSDT_MonitorDmn_Prestadora_sigla = "";
         gxTv_SdtSDT_MonitorDmn_Agrupador = "";
         gxTv_SdtSDT_MonitorDmn_Responsavel_nome = "";
         gxTv_SdtSDT_MonitorDmn_Tela = "";
         gxTv_SdtSDT_MonitorDmn_Owner_nome = "";
         gxTv_SdtSDT_MonitorDmn_Tipodeprazo = "";
         gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao = "";
         gxTv_SdtSDT_MonitorDmn_Prioridade_nome = "";
      }

      public SdtSDT_MonitorDmn( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_MonitorDmn deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_MonitorDmn)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_MonitorDmn obj ;
         obj = this;
         obj.gxTpr_Codigo = deserialized.gxTpr_Codigo;
         obj.gxTpr_Linha = deserialized.gxTpr_Linha;
         obj.gxTpr_Status = deserialized.gxTpr_Status;
         obj.gxTpr_Statuscnt = deserialized.gxTpr_Statuscnt;
         obj.gxTpr_Statusdmnvnc = deserialized.gxTpr_Statusdmnvnc;
         obj.gxTpr_Prazo = deserialized.gxTpr_Prazo;
         obj.gxTpr_Prazoentrega = deserialized.gxTpr_Prazoentrega;
         obj.gxTpr_Horaentrega = deserialized.gxTpr_Horaentrega;
         obj.gxTpr_Datadmn = deserialized.gxTpr_Datadmn;
         obj.gxTpr_Dataentregareal = deserialized.gxTpr_Dataentregareal;
         obj.gxTpr_Atraso = deserialized.gxTpr_Atraso;
         obj.gxTpr_Demanda = deserialized.gxTpr_Demanda;
         obj.gxTpr_Osfmosfs = deserialized.gxTpr_Osfmosfs;
         obj.gxTpr_Referencia = deserialized.gxTpr_Referencia;
         obj.gxTpr_Descricao = deserialized.gxTpr_Descricao;
         obj.gxTpr_Descricaocomplementar = deserialized.gxTpr_Descricaocomplementar;
         obj.gxTpr_Sistema = deserialized.gxTpr_Sistema;
         obj.gxTpr_Modulo = deserialized.gxTpr_Modulo;
         obj.gxTpr_Servico = deserialized.gxTpr_Servico;
         obj.gxTpr_Servicoss_codigo = deserialized.gxTpr_Servicoss_codigo;
         obj.gxTpr_Prestadora = deserialized.gxTpr_Prestadora;
         obj.gxTpr_Prestadora_sigla = deserialized.gxTpr_Prestadora_sigla;
         obj.gxTpr_Agrupador = deserialized.gxTpr_Agrupador;
         obj.gxTpr_Responsavel = deserialized.gxTpr_Responsavel;
         obj.gxTpr_Responsavel_nome = deserialized.gxTpr_Responsavel_nome;
         obj.gxTpr_Responsavelexterno = deserialized.gxTpr_Responsavelexterno;
         obj.gxTpr_Osvinculada = deserialized.gxTpr_Osvinculada;
         obj.gxTpr_Tela = deserialized.gxTpr_Tela;
         obj.gxTpr_Unidades = deserialized.gxTpr_Unidades;
         obj.gxTpr_Selecionada = deserialized.gxTpr_Selecionada;
         obj.gxTpr_Visible = deserialized.gxTpr_Visible;
         obj.gxTpr_Owner = deserialized.gxTpr_Owner;
         obj.gxTpr_Owner_nome = deserialized.gxTpr_Owner_nome;
         obj.gxTpr_Temproposta = deserialized.gxTpr_Temproposta;
         obj.gxTpr_Tipodeprazo = deserialized.gxTpr_Tipodeprazo;
         obj.gxTpr_Logresponsavel_observacao = deserialized.gxTpr_Logresponsavel_observacao;
         obj.gxTpr_Warning = deserialized.gxTpr_Warning;
         obj.gxTpr_Exibepf = deserialized.gxTpr_Exibepf;
         obj.gxTpr_Pfbfsimp = deserialized.gxTpr_Pfbfsimp;
         obj.gxTpr_Pflfsimp = deserialized.gxTpr_Pflfsimp;
         obj.gxTpr_Contratadaorigemcod = deserialized.gxTpr_Contratadaorigemcod;
         obj.gxTpr_Contratadaorigemusasistema = deserialized.gxTpr_Contratadaorigemusasistema;
         obj.gxTpr_Prioridade_nome = deserialized.gxTpr_Prioridade_nome;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Codigo") )
               {
                  gxTv_SdtSDT_MonitorDmn_Codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Linha") )
               {
                  gxTv_SdtSDT_MonitorDmn_Linha = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Status") )
               {
                  gxTv_SdtSDT_MonitorDmn_Status = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "StatusCnt") )
               {
                  gxTv_SdtSDT_MonitorDmn_Statuscnt = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "StatusDmnVnc") )
               {
                  gxTv_SdtSDT_MonitorDmn_Statusdmnvnc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Prazo") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_MonitorDmn_Prazo = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSDT_MonitorDmn_Prazo = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PrazoEntrega") )
               {
                  gxTv_SdtSDT_MonitorDmn_Prazoentrega = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "HoraEntrega") )
               {
                  gxTv_SdtSDT_MonitorDmn_Horaentrega = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataDmn") )
               {
                  gxTv_SdtSDT_MonitorDmn_Datadmn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DataEntregaReal") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_MonitorDmn_Dataentregareal = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_MonitorDmn_Dataentregareal = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Atraso") )
               {
                  gxTv_SdtSDT_MonitorDmn_Atraso = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Demanda") )
               {
                  gxTv_SdtSDT_MonitorDmn_Demanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OsFmOsFs") )
               {
                  gxTv_SdtSDT_MonitorDmn_Osfmosfs = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Referencia") )
               {
                  gxTv_SdtSDT_MonitorDmn_Referencia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Descricao") )
               {
                  gxTv_SdtSDT_MonitorDmn_Descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "DescricaoComplementar") )
               {
                  gxTv_SdtSDT_MonitorDmn_Descricaocomplementar = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema") )
               {
                  gxTv_SdtSDT_MonitorDmn_Sistema = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo") )
               {
                  gxTv_SdtSDT_MonitorDmn_Modulo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico") )
               {
                  gxTv_SdtSDT_MonitorDmn_Servico = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoSS_Codigo") )
               {
                  gxTv_SdtSDT_MonitorDmn_Servicoss_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Prestadora") )
               {
                  gxTv_SdtSDT_MonitorDmn_Prestadora = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Prestadora_Sigla") )
               {
                  gxTv_SdtSDT_MonitorDmn_Prestadora_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Agrupador") )
               {
                  gxTv_SdtSDT_MonitorDmn_Agrupador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Responsavel") )
               {
                  gxTv_SdtSDT_MonitorDmn_Responsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Responsavel_Nome") )
               {
                  gxTv_SdtSDT_MonitorDmn_Responsavel_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ResponsavelExterno") )
               {
                  gxTv_SdtSDT_MonitorDmn_Responsavelexterno = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OSVinculada") )
               {
                  gxTv_SdtSDT_MonitorDmn_Osvinculada = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tela") )
               {
                  gxTv_SdtSDT_MonitorDmn_Tela = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Unidades") )
               {
                  gxTv_SdtSDT_MonitorDmn_Unidades = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Selecionada") )
               {
                  gxTv_SdtSDT_MonitorDmn_Selecionada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Visible") )
               {
                  gxTv_SdtSDT_MonitorDmn_Visible = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Owner") )
               {
                  gxTv_SdtSDT_MonitorDmn_Owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Owner_Nome") )
               {
                  gxTv_SdtSDT_MonitorDmn_Owner_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TemProposta") )
               {
                  gxTv_SdtSDT_MonitorDmn_Temproposta = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDePrazo") )
               {
                  gxTv_SdtSDT_MonitorDmn_Tipodeprazo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LogResponsavel_Observacao") )
               {
                  gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Warning") )
               {
                  gxTv_SdtSDT_MonitorDmn_Warning = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ExibePF") )
               {
                  gxTv_SdtSDT_MonitorDmn_Exibepf = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PFBFSImp") )
               {
                  gxTv_SdtSDT_MonitorDmn_Pfbfsimp = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "PFLFSImp") )
               {
                  gxTv_SdtSDT_MonitorDmn_Pflfsimp = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaOrigemCod") )
               {
                  gxTv_SdtSDT_MonitorDmn_Contratadaorigemcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratadaOrigemUsaSistema") )
               {
                  gxTv_SdtSDT_MonitorDmn_Contratadaorigemusasistema = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Prioridade_Nome") )
               {
                  gxTv_SdtSDT_MonitorDmn_Prioridade_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_MonitorDmn";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Linha", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Linha), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Status", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Status));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("StatusCnt", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Statuscnt));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("StatusDmnVnc", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Statusdmnvnc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_MonitorDmn_Prazo) )
         {
            oWriter.WriteStartElement("Prazo");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_MonitorDmn_Prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_MonitorDmn_Prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_MonitorDmn_Prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSDT_MonitorDmn_Prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSDT_MonitorDmn_Prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSDT_MonitorDmn_Prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Prazo", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("PrazoEntrega", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Prazoentrega));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("HoraEntrega", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Horaentrega));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("DataDmn", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Datadmn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_MonitorDmn_Dataentregareal) )
         {
            oWriter.WriteStartElement("DataEntregaReal");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_MonitorDmn_Dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_MonitorDmn_Dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_MonitorDmn_Dataentregareal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("DataEntregaReal", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Atraso", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Atraso), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Demanda", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Demanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("OsFmOsFs", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Osfmosfs));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Referencia", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Referencia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Descricao", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("DescricaoComplementar", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Descricaocomplementar));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Sistema", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Sistema));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Modulo", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Modulo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Servico", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Servico));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ServicoSS_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Servicoss_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Prestadora", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Prestadora), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Prestadora_Sigla", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Prestadora_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Agrupador", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Agrupador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Responsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Responsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Responsavel_Nome", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Responsavel_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ResponsavelExterno", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_MonitorDmn_Responsavelexterno)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("OSVinculada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Osvinculada), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Tela", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Tela));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Unidades", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_MonitorDmn_Unidades, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Selecionada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_MonitorDmn_Selecionada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Visible", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_MonitorDmn_Visible)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Owner_Nome", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Owner_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TemProposta", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_MonitorDmn_Temproposta)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TipoDePrazo", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Tipodeprazo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("LogResponsavel_Observacao", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Warning", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Warning), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ExibePF", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_MonitorDmn_Exibepf)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PFBFSImp", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_MonitorDmn_Pfbfsimp, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("PFLFSImp", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_MonitorDmn_Pflfsimp, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaOrigemCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MonitorDmn_Contratadaorigemcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratadaOrigemUsaSistema", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_MonitorDmn_Contratadaorigemusasistema)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Prioridade_Nome", StringUtil.RTrim( gxTv_SdtSDT_MonitorDmn_Prioridade_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Codigo", gxTv_SdtSDT_MonitorDmn_Codigo, false);
         AddObjectProperty("Linha", gxTv_SdtSDT_MonitorDmn_Linha, false);
         AddObjectProperty("Status", gxTv_SdtSDT_MonitorDmn_Status, false);
         AddObjectProperty("StatusCnt", gxTv_SdtSDT_MonitorDmn_Statuscnt, false);
         AddObjectProperty("StatusDmnVnc", gxTv_SdtSDT_MonitorDmn_Statusdmnvnc, false);
         datetime_STZ = gxTv_SdtSDT_MonitorDmn_Prazo;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Prazo", sDateCnv, false);
         AddObjectProperty("PrazoEntrega", gxTv_SdtSDT_MonitorDmn_Prazoentrega, false);
         AddObjectProperty("HoraEntrega", gxTv_SdtSDT_MonitorDmn_Horaentrega, false);
         AddObjectProperty("DataDmn", gxTv_SdtSDT_MonitorDmn_Datadmn, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_MonitorDmn_Dataentregareal)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_MonitorDmn_Dataentregareal)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_MonitorDmn_Dataentregareal)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("DataEntregaReal", sDateCnv, false);
         AddObjectProperty("Atraso", gxTv_SdtSDT_MonitorDmn_Atraso, false);
         AddObjectProperty("Demanda", gxTv_SdtSDT_MonitorDmn_Demanda, false);
         AddObjectProperty("OsFmOsFs", gxTv_SdtSDT_MonitorDmn_Osfmosfs, false);
         AddObjectProperty("Referencia", gxTv_SdtSDT_MonitorDmn_Referencia, false);
         AddObjectProperty("Descricao", gxTv_SdtSDT_MonitorDmn_Descricao, false);
         AddObjectProperty("DescricaoComplementar", gxTv_SdtSDT_MonitorDmn_Descricaocomplementar, false);
         AddObjectProperty("Sistema", gxTv_SdtSDT_MonitorDmn_Sistema, false);
         AddObjectProperty("Modulo", gxTv_SdtSDT_MonitorDmn_Modulo, false);
         AddObjectProperty("Servico", gxTv_SdtSDT_MonitorDmn_Servico, false);
         AddObjectProperty("ServicoSS_Codigo", gxTv_SdtSDT_MonitorDmn_Servicoss_codigo, false);
         AddObjectProperty("Prestadora", gxTv_SdtSDT_MonitorDmn_Prestadora, false);
         AddObjectProperty("Prestadora_Sigla", gxTv_SdtSDT_MonitorDmn_Prestadora_sigla, false);
         AddObjectProperty("Agrupador", gxTv_SdtSDT_MonitorDmn_Agrupador, false);
         AddObjectProperty("Responsavel", gxTv_SdtSDT_MonitorDmn_Responsavel, false);
         AddObjectProperty("Responsavel_Nome", gxTv_SdtSDT_MonitorDmn_Responsavel_nome, false);
         AddObjectProperty("ResponsavelExterno", gxTv_SdtSDT_MonitorDmn_Responsavelexterno, false);
         AddObjectProperty("OSVinculada", gxTv_SdtSDT_MonitorDmn_Osvinculada, false);
         AddObjectProperty("Tela", gxTv_SdtSDT_MonitorDmn_Tela, false);
         AddObjectProperty("Unidades", gxTv_SdtSDT_MonitorDmn_Unidades, false);
         AddObjectProperty("Selecionada", gxTv_SdtSDT_MonitorDmn_Selecionada, false);
         AddObjectProperty("Visible", gxTv_SdtSDT_MonitorDmn_Visible, false);
         AddObjectProperty("Owner", gxTv_SdtSDT_MonitorDmn_Owner, false);
         AddObjectProperty("Owner_Nome", gxTv_SdtSDT_MonitorDmn_Owner_nome, false);
         AddObjectProperty("TemProposta", gxTv_SdtSDT_MonitorDmn_Temproposta, false);
         AddObjectProperty("TipoDePrazo", gxTv_SdtSDT_MonitorDmn_Tipodeprazo, false);
         AddObjectProperty("LogResponsavel_Observacao", gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao, false);
         AddObjectProperty("Warning", gxTv_SdtSDT_MonitorDmn_Warning, false);
         AddObjectProperty("ExibePF", gxTv_SdtSDT_MonitorDmn_Exibepf, false);
         AddObjectProperty("PFBFSImp", gxTv_SdtSDT_MonitorDmn_Pfbfsimp, false);
         AddObjectProperty("PFLFSImp", gxTv_SdtSDT_MonitorDmn_Pflfsimp, false);
         AddObjectProperty("ContratadaOrigemCod", gxTv_SdtSDT_MonitorDmn_Contratadaorigemcod, false);
         AddObjectProperty("ContratadaOrigemUsaSistema", gxTv_SdtSDT_MonitorDmn_Contratadaorigemusasistema, false);
         AddObjectProperty("Prioridade_Nome", gxTv_SdtSDT_MonitorDmn_Prioridade_nome, false);
         return  ;
      }

      [  SoapElement( ElementName = "Codigo" )]
      [  XmlElement( ElementName = "Codigo"   )]
      public int gxTpr_Codigo
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Codigo ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Linha" )]
      [  XmlElement( ElementName = "Linha"   )]
      public short gxTpr_Linha
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Linha ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Linha = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Status" )]
      [  XmlElement( ElementName = "Status"   )]
      public String gxTpr_Status
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Status ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Status = (String)(value);
         }

      }

      [  SoapElement( ElementName = "StatusCnt" )]
      [  XmlElement( ElementName = "StatusCnt"   )]
      public String gxTpr_Statuscnt
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Statuscnt ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Statuscnt = (String)(value);
         }

      }

      [  SoapElement( ElementName = "StatusDmnVnc" )]
      [  XmlElement( ElementName = "StatusDmnVnc"   )]
      public String gxTpr_Statusdmnvnc
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Statusdmnvnc ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Statusdmnvnc = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Prazo" )]
      [  XmlElement( ElementName = "Prazo"  , IsNullable=true )]
      public string gxTpr_Prazo_Nullable
      {
         get {
            if ( gxTv_SdtSDT_MonitorDmn_Prazo == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSDT_MonitorDmn_Prazo).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSDT_MonitorDmn_Prazo = DateTime.MinValue;
            else
               gxTv_SdtSDT_MonitorDmn_Prazo = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Prazo
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Prazo ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Prazo = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "PrazoEntrega" )]
      [  XmlElement( ElementName = "PrazoEntrega"   )]
      public String gxTpr_Prazoentrega
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Prazoentrega ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Prazoentrega = (String)(value);
         }

      }

      [  SoapElement( ElementName = "HoraEntrega" )]
      [  XmlElement( ElementName = "HoraEntrega"   )]
      public String gxTpr_Horaentrega
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Horaentrega ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Horaentrega = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DataDmn" )]
      [  XmlElement( ElementName = "DataDmn"   )]
      public String gxTpr_Datadmn
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Datadmn ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Datadmn = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DataEntregaReal" )]
      [  XmlElement( ElementName = "DataEntregaReal"  , IsNullable=true )]
      public string gxTpr_Dataentregareal_Nullable
      {
         get {
            if ( gxTv_SdtSDT_MonitorDmn_Dataentregareal == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_MonitorDmn_Dataentregareal).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_MonitorDmn_Dataentregareal = DateTime.MinValue;
            else
               gxTv_SdtSDT_MonitorDmn_Dataentregareal = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Dataentregareal
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Dataentregareal ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Dataentregareal = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Atraso" )]
      [  XmlElement( ElementName = "Atraso"   )]
      public short gxTpr_Atraso
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Atraso ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Atraso = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Demanda" )]
      [  XmlElement( ElementName = "Demanda"   )]
      public String gxTpr_Demanda
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Demanda ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Demanda = (String)(value);
         }

      }

      [  SoapElement( ElementName = "OsFmOsFs" )]
      [  XmlElement( ElementName = "OsFmOsFs"   )]
      public String gxTpr_Osfmosfs
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Osfmosfs ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Osfmosfs = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Referencia" )]
      [  XmlElement( ElementName = "Referencia"   )]
      public String gxTpr_Referencia
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Referencia ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Referencia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Descricao" )]
      [  XmlElement( ElementName = "Descricao"   )]
      public String gxTpr_Descricao
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Descricao ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "DescricaoComplementar" )]
      [  XmlElement( ElementName = "DescricaoComplementar"   )]
      public String gxTpr_Descricaocomplementar
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Descricaocomplementar ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Descricaocomplementar = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema" )]
      [  XmlElement( ElementName = "Sistema"   )]
      public String gxTpr_Sistema
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Sistema ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Sistema = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Modulo" )]
      [  XmlElement( ElementName = "Modulo"   )]
      public String gxTpr_Modulo
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Modulo ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Modulo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico" )]
      [  XmlElement( ElementName = "Servico"   )]
      public String gxTpr_Servico
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Servico ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Servico = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoSS_Codigo" )]
      [  XmlElement( ElementName = "ServicoSS_Codigo"   )]
      public int gxTpr_Servicoss_codigo
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Servicoss_codigo ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Servicoss_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Prestadora" )]
      [  XmlElement( ElementName = "Prestadora"   )]
      public int gxTpr_Prestadora
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Prestadora ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Prestadora = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Prestadora_Sigla" )]
      [  XmlElement( ElementName = "Prestadora_Sigla"   )]
      public String gxTpr_Prestadora_sigla
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Prestadora_sigla ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Prestadora_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Agrupador" )]
      [  XmlElement( ElementName = "Agrupador"   )]
      public String gxTpr_Agrupador
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Agrupador ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Agrupador = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Responsavel" )]
      [  XmlElement( ElementName = "Responsavel"   )]
      public int gxTpr_Responsavel
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Responsavel ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Responsavel = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Responsavel_Nome" )]
      [  XmlElement( ElementName = "Responsavel_Nome"   )]
      public String gxTpr_Responsavel_nome
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Responsavel_nome ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Responsavel_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ResponsavelExterno" )]
      [  XmlElement( ElementName = "ResponsavelExterno"   )]
      public bool gxTpr_Responsavelexterno
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Responsavelexterno ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Responsavelexterno = value;
         }

      }

      [  SoapElement( ElementName = "OSVinculada" )]
      [  XmlElement( ElementName = "OSVinculada"   )]
      public int gxTpr_Osvinculada
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Osvinculada ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Osvinculada = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Tela" )]
      [  XmlElement( ElementName = "Tela"   )]
      public String gxTpr_Tela
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Tela ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Tela = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Unidades" )]
      [  XmlElement( ElementName = "Unidades"   )]
      public double gxTpr_Unidades_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_MonitorDmn_Unidades) ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Unidades = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Unidades
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Unidades ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Unidades = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Selecionada" )]
      [  XmlElement( ElementName = "Selecionada"   )]
      public bool gxTpr_Selecionada
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Selecionada ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Selecionada = value;
         }

      }

      [  SoapElement( ElementName = "Visible" )]
      [  XmlElement( ElementName = "Visible"   )]
      public bool gxTpr_Visible
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Visible ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Visible = value;
         }

      }

      [  SoapElement( ElementName = "Owner" )]
      [  XmlElement( ElementName = "Owner"   )]
      public int gxTpr_Owner
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Owner ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Owner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Owner_Nome" )]
      [  XmlElement( ElementName = "Owner_Nome"   )]
      public String gxTpr_Owner_nome
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Owner_nome ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Owner_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "TemProposta" )]
      [  XmlElement( ElementName = "TemProposta"   )]
      public bool gxTpr_Temproposta
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Temproposta ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Temproposta = value;
         }

      }

      [  SoapElement( ElementName = "TipoDePrazo" )]
      [  XmlElement( ElementName = "TipoDePrazo"   )]
      public String gxTpr_Tipodeprazo
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Tipodeprazo ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Tipodeprazo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "LogResponsavel_Observacao" )]
      [  XmlElement( ElementName = "LogResponsavel_Observacao"   )]
      public String gxTpr_Logresponsavel_observacao
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Warning" )]
      [  XmlElement( ElementName = "Warning"   )]
      public short gxTpr_Warning
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Warning ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Warning = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ExibePF" )]
      [  XmlElement( ElementName = "ExibePF"   )]
      public bool gxTpr_Exibepf
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Exibepf ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Exibepf = value;
         }

      }

      [  SoapElement( ElementName = "PFBFSImp" )]
      [  XmlElement( ElementName = "PFBFSImp"   )]
      public double gxTpr_Pfbfsimp_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_MonitorDmn_Pfbfsimp) ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Pfbfsimp = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Pfbfsimp
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Pfbfsimp ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Pfbfsimp = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "PFLFSImp" )]
      [  XmlElement( ElementName = "PFLFSImp"   )]
      public double gxTpr_Pflfsimp_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_MonitorDmn_Pflfsimp) ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Pflfsimp = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Pflfsimp
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Pflfsimp ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Pflfsimp = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContratadaOrigemCod" )]
      [  XmlElement( ElementName = "ContratadaOrigemCod"   )]
      public int gxTpr_Contratadaorigemcod
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Contratadaorigemcod ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Contratadaorigemcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratadaOrigemUsaSistema" )]
      [  XmlElement( ElementName = "ContratadaOrigemUsaSistema"   )]
      public bool gxTpr_Contratadaorigemusasistema
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Contratadaorigemusasistema ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Contratadaorigemusasistema = value;
         }

      }

      [  SoapElement( ElementName = "Prioridade_Nome" )]
      [  XmlElement( ElementName = "Prioridade_Nome"   )]
      public String gxTpr_Prioridade_nome
      {
         get {
            return gxTv_SdtSDT_MonitorDmn_Prioridade_nome ;
         }

         set {
            gxTv_SdtSDT_MonitorDmn_Prioridade_nome = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_MonitorDmn_Status = "";
         gxTv_SdtSDT_MonitorDmn_Statuscnt = "";
         gxTv_SdtSDT_MonitorDmn_Statusdmnvnc = "";
         gxTv_SdtSDT_MonitorDmn_Prazo = (DateTime)(DateTime.MinValue);
         gxTv_SdtSDT_MonitorDmn_Prazoentrega = "";
         gxTv_SdtSDT_MonitorDmn_Horaentrega = "";
         gxTv_SdtSDT_MonitorDmn_Datadmn = "";
         gxTv_SdtSDT_MonitorDmn_Dataentregareal = DateTime.MinValue;
         gxTv_SdtSDT_MonitorDmn_Demanda = "";
         gxTv_SdtSDT_MonitorDmn_Osfmosfs = "";
         gxTv_SdtSDT_MonitorDmn_Referencia = "";
         gxTv_SdtSDT_MonitorDmn_Descricao = "";
         gxTv_SdtSDT_MonitorDmn_Descricaocomplementar = "";
         gxTv_SdtSDT_MonitorDmn_Sistema = "";
         gxTv_SdtSDT_MonitorDmn_Modulo = "";
         gxTv_SdtSDT_MonitorDmn_Servico = "";
         gxTv_SdtSDT_MonitorDmn_Prestadora_sigla = "";
         gxTv_SdtSDT_MonitorDmn_Agrupador = "";
         gxTv_SdtSDT_MonitorDmn_Responsavel_nome = "";
         gxTv_SdtSDT_MonitorDmn_Tela = "";
         gxTv_SdtSDT_MonitorDmn_Owner_nome = "";
         gxTv_SdtSDT_MonitorDmn_Tipodeprazo = "";
         gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao = "";
         gxTv_SdtSDT_MonitorDmn_Prioridade_nome = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtSDT_MonitorDmn_Linha ;
      protected short gxTv_SdtSDT_MonitorDmn_Atraso ;
      protected short gxTv_SdtSDT_MonitorDmn_Warning ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_MonitorDmn_Codigo ;
      protected int gxTv_SdtSDT_MonitorDmn_Servicoss_codigo ;
      protected int gxTv_SdtSDT_MonitorDmn_Prestadora ;
      protected int gxTv_SdtSDT_MonitorDmn_Responsavel ;
      protected int gxTv_SdtSDT_MonitorDmn_Osvinculada ;
      protected int gxTv_SdtSDT_MonitorDmn_Owner ;
      protected int gxTv_SdtSDT_MonitorDmn_Contratadaorigemcod ;
      protected decimal gxTv_SdtSDT_MonitorDmn_Unidades ;
      protected decimal gxTv_SdtSDT_MonitorDmn_Pfbfsimp ;
      protected decimal gxTv_SdtSDT_MonitorDmn_Pflfsimp ;
      protected String gxTv_SdtSDT_MonitorDmn_Status ;
      protected String gxTv_SdtSDT_MonitorDmn_Statuscnt ;
      protected String gxTv_SdtSDT_MonitorDmn_Statusdmnvnc ;
      protected String gxTv_SdtSDT_MonitorDmn_Prazoentrega ;
      protected String gxTv_SdtSDT_MonitorDmn_Horaentrega ;
      protected String gxTv_SdtSDT_MonitorDmn_Datadmn ;
      protected String gxTv_SdtSDT_MonitorDmn_Sistema ;
      protected String gxTv_SdtSDT_MonitorDmn_Modulo ;
      protected String gxTv_SdtSDT_MonitorDmn_Servico ;
      protected String gxTv_SdtSDT_MonitorDmn_Prestadora_sigla ;
      protected String gxTv_SdtSDT_MonitorDmn_Agrupador ;
      protected String gxTv_SdtSDT_MonitorDmn_Responsavel_nome ;
      protected String gxTv_SdtSDT_MonitorDmn_Tela ;
      protected String gxTv_SdtSDT_MonitorDmn_Owner_nome ;
      protected String gxTv_SdtSDT_MonitorDmn_Tipodeprazo ;
      protected String gxTv_SdtSDT_MonitorDmn_Prioridade_nome ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_MonitorDmn_Prazo ;
      protected DateTime datetime_STZ ;
      protected DateTime gxTv_SdtSDT_MonitorDmn_Dataentregareal ;
      protected bool gxTv_SdtSDT_MonitorDmn_Responsavelexterno ;
      protected bool gxTv_SdtSDT_MonitorDmn_Selecionada ;
      protected bool gxTv_SdtSDT_MonitorDmn_Visible ;
      protected bool gxTv_SdtSDT_MonitorDmn_Temproposta ;
      protected bool gxTv_SdtSDT_MonitorDmn_Exibepf ;
      protected bool gxTv_SdtSDT_MonitorDmn_Contratadaorigemusasistema ;
      protected String gxTv_SdtSDT_MonitorDmn_Descricaocomplementar ;
      protected String gxTv_SdtSDT_MonitorDmn_Logresponsavel_observacao ;
      protected String gxTv_SdtSDT_MonitorDmn_Demanda ;
      protected String gxTv_SdtSDT_MonitorDmn_Osfmosfs ;
      protected String gxTv_SdtSDT_MonitorDmn_Referencia ;
      protected String gxTv_SdtSDT_MonitorDmn_Descricao ;
   }

   [DataContract(Name = @"SDT_MonitorDmn", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_MonitorDmn_RESTInterface : GxGenericCollectionItem<SdtSDT_MonitorDmn>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_MonitorDmn_RESTInterface( ) : base()
      {
      }

      public SdtSDT_MonitorDmn_RESTInterface( SdtSDT_MonitorDmn psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Codigo
      {
         get {
            return sdt.gxTpr_Codigo ;
         }

         set {
            sdt.gxTpr_Codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Linha" , Order = 1 )]
      public Nullable<short> gxTpr_Linha
      {
         get {
            return sdt.gxTpr_Linha ;
         }

         set {
            sdt.gxTpr_Linha = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Status" , Order = 2 )]
      public String gxTpr_Status
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Status) ;
         }

         set {
            sdt.gxTpr_Status = (String)(value);
         }

      }

      [DataMember( Name = "StatusCnt" , Order = 3 )]
      public String gxTpr_Statuscnt
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Statuscnt) ;
         }

         set {
            sdt.gxTpr_Statuscnt = (String)(value);
         }

      }

      [DataMember( Name = "StatusDmnVnc" , Order = 4 )]
      public String gxTpr_Statusdmnvnc
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Statusdmnvnc) ;
         }

         set {
            sdt.gxTpr_Statusdmnvnc = (String)(value);
         }

      }

      [DataMember( Name = "Prazo" , Order = 5 )]
      public String gxTpr_Prazo
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Prazo) ;
         }

         set {
            sdt.gxTpr_Prazo = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "PrazoEntrega" , Order = 6 )]
      public String gxTpr_Prazoentrega
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Prazoentrega) ;
         }

         set {
            sdt.gxTpr_Prazoentrega = (String)(value);
         }

      }

      [DataMember( Name = "HoraEntrega" , Order = 7 )]
      public String gxTpr_Horaentrega
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Horaentrega) ;
         }

         set {
            sdt.gxTpr_Horaentrega = (String)(value);
         }

      }

      [DataMember( Name = "DataDmn" , Order = 8 )]
      public String gxTpr_Datadmn
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Datadmn) ;
         }

         set {
            sdt.gxTpr_Datadmn = (String)(value);
         }

      }

      [DataMember( Name = "DataEntregaReal" , Order = 9 )]
      public String gxTpr_Dataentregareal
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Dataentregareal) ;
         }

         set {
            sdt.gxTpr_Dataentregareal = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Atraso" , Order = 10 )]
      public Nullable<short> gxTpr_Atraso
      {
         get {
            return sdt.gxTpr_Atraso ;
         }

         set {
            sdt.gxTpr_Atraso = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Demanda" , Order = 11 )]
      public String gxTpr_Demanda
      {
         get {
            return sdt.gxTpr_Demanda ;
         }

         set {
            sdt.gxTpr_Demanda = (String)(value);
         }

      }

      [DataMember( Name = "OsFmOsFs" , Order = 12 )]
      public String gxTpr_Osfmosfs
      {
         get {
            return sdt.gxTpr_Osfmosfs ;
         }

         set {
            sdt.gxTpr_Osfmosfs = (String)(value);
         }

      }

      [DataMember( Name = "Referencia" , Order = 13 )]
      public String gxTpr_Referencia
      {
         get {
            return sdt.gxTpr_Referencia ;
         }

         set {
            sdt.gxTpr_Referencia = (String)(value);
         }

      }

      [DataMember( Name = "Descricao" , Order = 14 )]
      public String gxTpr_Descricao
      {
         get {
            return sdt.gxTpr_Descricao ;
         }

         set {
            sdt.gxTpr_Descricao = (String)(value);
         }

      }

      [DataMember( Name = "DescricaoComplementar" , Order = 15 )]
      public String gxTpr_Descricaocomplementar
      {
         get {
            return sdt.gxTpr_Descricaocomplementar ;
         }

         set {
            sdt.gxTpr_Descricaocomplementar = (String)(value);
         }

      }

      [DataMember( Name = "Sistema" , Order = 16 )]
      public String gxTpr_Sistema
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistema) ;
         }

         set {
            sdt.gxTpr_Sistema = (String)(value);
         }

      }

      [DataMember( Name = "Modulo" , Order = 17 )]
      public String gxTpr_Modulo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Modulo) ;
         }

         set {
            sdt.gxTpr_Modulo = (String)(value);
         }

      }

      [DataMember( Name = "Servico" , Order = 18 )]
      public String gxTpr_Servico
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico) ;
         }

         set {
            sdt.gxTpr_Servico = (String)(value);
         }

      }

      [DataMember( Name = "ServicoSS_Codigo" , Order = 19 )]
      public Nullable<int> gxTpr_Servicoss_codigo
      {
         get {
            return sdt.gxTpr_Servicoss_codigo ;
         }

         set {
            sdt.gxTpr_Servicoss_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Prestadora" , Order = 20 )]
      public Nullable<int> gxTpr_Prestadora
      {
         get {
            return sdt.gxTpr_Prestadora ;
         }

         set {
            sdt.gxTpr_Prestadora = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Prestadora_Sigla" , Order = 21 )]
      public String gxTpr_Prestadora_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Prestadora_sigla) ;
         }

         set {
            sdt.gxTpr_Prestadora_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Agrupador" , Order = 22 )]
      public String gxTpr_Agrupador
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Agrupador) ;
         }

         set {
            sdt.gxTpr_Agrupador = (String)(value);
         }

      }

      [DataMember( Name = "Responsavel" , Order = 23 )]
      public Nullable<int> gxTpr_Responsavel
      {
         get {
            return sdt.gxTpr_Responsavel ;
         }

         set {
            sdt.gxTpr_Responsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Responsavel_Nome" , Order = 24 )]
      public String gxTpr_Responsavel_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Responsavel_nome) ;
         }

         set {
            sdt.gxTpr_Responsavel_nome = (String)(value);
         }

      }

      [DataMember( Name = "ResponsavelExterno" , Order = 25 )]
      public bool gxTpr_Responsavelexterno
      {
         get {
            return sdt.gxTpr_Responsavelexterno ;
         }

         set {
            sdt.gxTpr_Responsavelexterno = value;
         }

      }

      [DataMember( Name = "OSVinculada" , Order = 26 )]
      public Nullable<int> gxTpr_Osvinculada
      {
         get {
            return sdt.gxTpr_Osvinculada ;
         }

         set {
            sdt.gxTpr_Osvinculada = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tela" , Order = 27 )]
      public String gxTpr_Tela
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tela) ;
         }

         set {
            sdt.gxTpr_Tela = (String)(value);
         }

      }

      [DataMember( Name = "Unidades" , Order = 28 )]
      public String gxTpr_Unidades
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Unidades, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Unidades = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Selecionada" , Order = 29 )]
      public bool gxTpr_Selecionada
      {
         get {
            return sdt.gxTpr_Selecionada ;
         }

         set {
            sdt.gxTpr_Selecionada = value;
         }

      }

      [DataMember( Name = "Visible" , Order = 30 )]
      public bool gxTpr_Visible
      {
         get {
            return sdt.gxTpr_Visible ;
         }

         set {
            sdt.gxTpr_Visible = value;
         }

      }

      [DataMember( Name = "Owner" , Order = 31 )]
      public Nullable<int> gxTpr_Owner
      {
         get {
            return sdt.gxTpr_Owner ;
         }

         set {
            sdt.gxTpr_Owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Owner_Nome" , Order = 32 )]
      public String gxTpr_Owner_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Owner_nome) ;
         }

         set {
            sdt.gxTpr_Owner_nome = (String)(value);
         }

      }

      [DataMember( Name = "TemProposta" , Order = 33 )]
      public bool gxTpr_Temproposta
      {
         get {
            return sdt.gxTpr_Temproposta ;
         }

         set {
            sdt.gxTpr_Temproposta = value;
         }

      }

      [DataMember( Name = "TipoDePrazo" , Order = 34 )]
      public String gxTpr_Tipodeprazo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodeprazo) ;
         }

         set {
            sdt.gxTpr_Tipodeprazo = (String)(value);
         }

      }

      [DataMember( Name = "LogResponsavel_Observacao" , Order = 35 )]
      public String gxTpr_Logresponsavel_observacao
      {
         get {
            return sdt.gxTpr_Logresponsavel_observacao ;
         }

         set {
            sdt.gxTpr_Logresponsavel_observacao = (String)(value);
         }

      }

      [DataMember( Name = "Warning" , Order = 36 )]
      public Nullable<short> gxTpr_Warning
      {
         get {
            return sdt.gxTpr_Warning ;
         }

         set {
            sdt.gxTpr_Warning = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ExibePF" , Order = 37 )]
      public bool gxTpr_Exibepf
      {
         get {
            return sdt.gxTpr_Exibepf ;
         }

         set {
            sdt.gxTpr_Exibepf = value;
         }

      }

      [DataMember( Name = "PFBFSImp" , Order = 38 )]
      public String gxTpr_Pfbfsimp
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Pfbfsimp, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Pfbfsimp = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "PFLFSImp" , Order = 39 )]
      public String gxTpr_Pflfsimp
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Pflfsimp, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Pflfsimp = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContratadaOrigemCod" , Order = 40 )]
      public Nullable<int> gxTpr_Contratadaorigemcod
      {
         get {
            return sdt.gxTpr_Contratadaorigemcod ;
         }

         set {
            sdt.gxTpr_Contratadaorigemcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratadaOrigemUsaSistema" , Order = 41 )]
      public bool gxTpr_Contratadaorigemusasistema
      {
         get {
            return sdt.gxTpr_Contratadaorigemusasistema ;
         }

         set {
            sdt.gxTpr_Contratadaorigemusasistema = value;
         }

      }

      [DataMember( Name = "Prioridade_Nome" , Order = 42 )]
      public String gxTpr_Prioridade_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Prioridade_nome) ;
         }

         set {
            sdt.gxTpr_Prioridade_nome = (String)(value);
         }

      }

      public SdtSDT_MonitorDmn sdt
      {
         get {
            return (SdtSDT_MonitorDmn)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_MonitorDmn() ;
         }
      }

   }

}
