/*
               File: GetContratoContratoArquivosAnexosWCFilterData
        Description: Get Contrato Contrato Arquivos Anexos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/28/2019 16:48:33.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratoarquivosanexoswcfilterdata : GXProcedure
   {
      public getcontratocontratoarquivosanexoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratoarquivosanexoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratoarquivosanexoswcfilterdata objgetcontratocontratoarquivosanexoswcfilterdata;
         objgetcontratocontratoarquivosanexoswcfilterdata = new getcontratocontratoarquivosanexoswcfilterdata();
         objgetcontratocontratoarquivosanexoswcfilterdata.AV18DDOName = aP0_DDOName;
         objgetcontratocontratoarquivosanexoswcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetcontratocontratoarquivosanexoswcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratoarquivosanexoswcfilterdata.AV22OptionsJson = "" ;
         objgetcontratocontratoarquivosanexoswcfilterdata.AV25OptionsDescJson = "" ;
         objgetcontratocontratoarquivosanexoswcfilterdata.AV27OptionIndexesJson = "" ;
         objgetcontratocontratoarquivosanexoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratoarquivosanexoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratoarquivosanexoswcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratoarquivosanexoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOARQUIVOSANEXOS_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOARQUIVOSANEXOS_NOMEARQOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOARQUIVOSANEXOS_TIPOARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOARQUIVOSANEXOS_TIPOARQOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("ContratoContratoArquivosAnexosWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoArquivosAnexosWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("ContratoContratoArquivosAnexosWCGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_NOMEARQ") == 0 )
            {
               AV10TFContratoArquivosAnexos_NomeArq = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_NOMEARQ_SEL") == 0 )
            {
               AV11TFContratoArquivosAnexos_NomeArq_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_TIPOARQ") == 0 )
            {
               AV12TFContratoArquivosAnexos_TipoArq = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_TIPOARQ_SEL") == 0 )
            {
               AV13TFContratoArquivosAnexos_TipoArq_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOARQUIVOSANEXOS_DATA") == 0 )
            {
               AV14TFContratoArquivosAnexos_Data = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( AV32GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV34Contrato_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOARQUIVOSANEXOS_NOMEARQOPTIONS' Routine */
         AV10TFContratoArquivosAnexos_NomeArq = AV16SearchTxt;
         AV11TFContratoArquivosAnexos_NomeArq_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFContratoArquivosAnexos_NomeArq_Sel ,
                                              AV10TFContratoArquivosAnexos_NomeArq ,
                                              AV13TFContratoArquivosAnexos_TipoArq_Sel ,
                                              AV12TFContratoArquivosAnexos_TipoArq ,
                                              AV14TFContratoArquivosAnexos_Data ,
                                              AV15TFContratoArquivosAnexos_Data_To ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq ,
                                              A113ContratoArquivosAnexos_Data ,
                                              AV34Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV10TFContratoArquivosAnexos_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoArquivosAnexos_NomeArq), 50, "%");
         lV12TFContratoArquivosAnexos_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoArquivosAnexos_TipoArq), 10, "%");
         /* Using cursor P00UE2 */
         pr_default.execute(0, new Object[] {AV34Contrato_Codigo, lV10TFContratoArquivosAnexos_NomeArq, AV11TFContratoArquivosAnexos_NomeArq_Sel, lV12TFContratoArquivosAnexos_TipoArq, AV13TFContratoArquivosAnexos_TipoArq_Sel, AV14TFContratoArquivosAnexos_Data, AV15TFContratoArquivosAnexos_Data_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUE2 = false;
            A74Contrato_Codigo = P00UE2_A74Contrato_Codigo[0];
            A112ContratoArquivosAnexos_NomeArq = P00UE2_A112ContratoArquivosAnexos_NomeArq[0];
            A113ContratoArquivosAnexos_Data = P00UE2_A113ContratoArquivosAnexos_Data[0];
            A109ContratoArquivosAnexos_TipoArq = P00UE2_A109ContratoArquivosAnexos_TipoArq[0];
            A108ContratoArquivosAnexos_Codigo = P00UE2_A108ContratoArquivosAnexos_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UE2_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UE2_A112ContratoArquivosAnexos_NomeArq[0], A112ContratoArquivosAnexos_NomeArq) == 0 ) )
            {
               BRKUE2 = false;
               A108ContratoArquivosAnexos_Codigo = P00UE2_A108ContratoArquivosAnexos_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUE2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq)) )
            {
               AV20Option = A112ContratoArquivosAnexos_NomeArq;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUE2 )
            {
               BRKUE2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOARQUIVOSANEXOS_TIPOARQOPTIONS' Routine */
         AV12TFContratoArquivosAnexos_TipoArq = AV16SearchTxt;
         AV13TFContratoArquivosAnexos_TipoArq_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV11TFContratoArquivosAnexos_NomeArq_Sel ,
                                              AV10TFContratoArquivosAnexos_NomeArq ,
                                              AV13TFContratoArquivosAnexos_TipoArq_Sel ,
                                              AV12TFContratoArquivosAnexos_TipoArq ,
                                              AV14TFContratoArquivosAnexos_Data ,
                                              AV15TFContratoArquivosAnexos_Data_To ,
                                              A112ContratoArquivosAnexos_NomeArq ,
                                              A109ContratoArquivosAnexos_TipoArq ,
                                              A113ContratoArquivosAnexos_Data ,
                                              AV34Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV10TFContratoArquivosAnexos_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoArquivosAnexos_NomeArq), 50, "%");
         lV12TFContratoArquivosAnexos_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoArquivosAnexos_TipoArq), 10, "%");
         /* Using cursor P00UE3 */
         pr_default.execute(1, new Object[] {AV34Contrato_Codigo, lV10TFContratoArquivosAnexos_NomeArq, AV11TFContratoArquivosAnexos_NomeArq_Sel, lV12TFContratoArquivosAnexos_TipoArq, AV13TFContratoArquivosAnexos_TipoArq_Sel, AV14TFContratoArquivosAnexos_Data, AV15TFContratoArquivosAnexos_Data_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUE4 = false;
            A74Contrato_Codigo = P00UE3_A74Contrato_Codigo[0];
            A109ContratoArquivosAnexos_TipoArq = P00UE3_A109ContratoArquivosAnexos_TipoArq[0];
            A113ContratoArquivosAnexos_Data = P00UE3_A113ContratoArquivosAnexos_Data[0];
            A112ContratoArquivosAnexos_NomeArq = P00UE3_A112ContratoArquivosAnexos_NomeArq[0];
            A108ContratoArquivosAnexos_Codigo = P00UE3_A108ContratoArquivosAnexos_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00UE3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UE3_A109ContratoArquivosAnexos_TipoArq[0], A109ContratoArquivosAnexos_TipoArq) == 0 ) )
            {
               BRKUE4 = false;
               A108ContratoArquivosAnexos_Codigo = P00UE3_A108ContratoArquivosAnexos_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUE4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq)) )
            {
               AV20Option = A109ContratoArquivosAnexos_TipoArq;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUE4 )
            {
               BRKUE4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoArquivosAnexos_NomeArq = "";
         AV11TFContratoArquivosAnexos_NomeArq_Sel = "";
         AV12TFContratoArquivosAnexos_TipoArq = "";
         AV13TFContratoArquivosAnexos_TipoArq_Sel = "";
         AV14TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         AV15TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV10TFContratoArquivosAnexos_NomeArq = "";
         lV12TFContratoArquivosAnexos_TipoArq = "";
         A112ContratoArquivosAnexos_NomeArq = "";
         A109ContratoArquivosAnexos_TipoArq = "";
         A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         P00UE2_A74Contrato_Codigo = new int[1] ;
         P00UE2_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00UE2_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00UE2_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00UE2_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         AV20Option = "";
         P00UE3_A74Contrato_Codigo = new int[1] ;
         P00UE3_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P00UE3_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P00UE3_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P00UE3_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratoarquivosanexoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UE2_A74Contrato_Codigo, P00UE2_A112ContratoArquivosAnexos_NomeArq, P00UE2_A113ContratoArquivosAnexos_Data, P00UE2_A109ContratoArquivosAnexos_TipoArq, P00UE2_A108ContratoArquivosAnexos_Codigo
               }
               , new Object[] {
               P00UE3_A74Contrato_Codigo, P00UE3_A109ContratoArquivosAnexos_TipoArq, P00UE3_A113ContratoArquivosAnexos_Data, P00UE3_A112ContratoArquivosAnexos_NomeArq, P00UE3_A108ContratoArquivosAnexos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV37GXV1 ;
      private int AV34Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A108ContratoArquivosAnexos_Codigo ;
      private long AV28count ;
      private String AV10TFContratoArquivosAnexos_NomeArq ;
      private String AV11TFContratoArquivosAnexos_NomeArq_Sel ;
      private String AV12TFContratoArquivosAnexos_TipoArq ;
      private String AV13TFContratoArquivosAnexos_TipoArq_Sel ;
      private String scmdbuf ;
      private String lV10TFContratoArquivosAnexos_NomeArq ;
      private String lV12TFContratoArquivosAnexos_TipoArq ;
      private String A112ContratoArquivosAnexos_NomeArq ;
      private String A109ContratoArquivosAnexos_TipoArq ;
      private DateTime AV14TFContratoArquivosAnexos_Data ;
      private DateTime AV15TFContratoArquivosAnexos_Data_To ;
      private DateTime A113ContratoArquivosAnexos_Data ;
      private bool returnInSub ;
      private bool BRKUE2 ;
      private bool BRKUE4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UE2_A74Contrato_Codigo ;
      private String[] P00UE2_A112ContratoArquivosAnexos_NomeArq ;
      private DateTime[] P00UE2_A113ContratoArquivosAnexos_Data ;
      private String[] P00UE2_A109ContratoArquivosAnexos_TipoArq ;
      private int[] P00UE2_A108ContratoArquivosAnexos_Codigo ;
      private int[] P00UE3_A74Contrato_Codigo ;
      private String[] P00UE3_A109ContratoArquivosAnexos_TipoArq ;
      private DateTime[] P00UE3_A113ContratoArquivosAnexos_Data ;
      private String[] P00UE3_A112ContratoArquivosAnexos_NomeArq ;
      private int[] P00UE3_A108ContratoArquivosAnexos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getcontratocontratoarquivosanexoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UE2( IGxContext context ,
                                             String AV11TFContratoArquivosAnexos_NomeArq_Sel ,
                                             String AV10TFContratoArquivosAnexos_NomeArq ,
                                             String AV13TFContratoArquivosAnexos_TipoArq_Sel ,
                                             String AV12TFContratoArquivosAnexos_TipoArq ,
                                             DateTime AV14TFContratoArquivosAnexos_Data ,
                                             DateTime AV15TFContratoArquivosAnexos_Data_To ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             int AV34Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Contrato_Codigo], [ContratoArquivosAnexos_NomeArq], [ContratoArquivosAnexos_Data], [ContratoArquivosAnexos_TipoArq], [ContratoArquivosAnexos_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV34Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoArquivosAnexos_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoArquivosAnexos_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_NomeArq] like @lV10TFContratoArquivosAnexos_NomeArq)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoArquivosAnexos_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_NomeArq] = @AV11TFContratoArquivosAnexos_NomeArq_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoArquivosAnexos_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoArquivosAnexos_TipoArq)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_TipoArq] like @lV12TFContratoArquivosAnexos_TipoArq)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoArquivosAnexos_TipoArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_TipoArq] = @AV13TFContratoArquivosAnexos_TipoArq_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContratoArquivosAnexos_Data) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_Data] >= @AV14TFContratoArquivosAnexos_Data)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContratoArquivosAnexos_Data_To) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_Data] <= @AV15TFContratoArquivosAnexos_Data_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo], [ContratoArquivosAnexos_NomeArq]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UE3( IGxContext context ,
                                             String AV11TFContratoArquivosAnexos_NomeArq_Sel ,
                                             String AV10TFContratoArquivosAnexos_NomeArq ,
                                             String AV13TFContratoArquivosAnexos_TipoArq_Sel ,
                                             String AV12TFContratoArquivosAnexos_TipoArq ,
                                             DateTime AV14TFContratoArquivosAnexos_Data ,
                                             DateTime AV15TFContratoArquivosAnexos_Data_To ,
                                             String A112ContratoArquivosAnexos_NomeArq ,
                                             String A109ContratoArquivosAnexos_TipoArq ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             int AV34Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Contrato_Codigo], [ContratoArquivosAnexos_TipoArq], [ContratoArquivosAnexos_Data], [ContratoArquivosAnexos_NomeArq], [ContratoArquivosAnexos_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Contrato_Codigo] = @AV34Contrato_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoArquivosAnexos_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoArquivosAnexos_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_NomeArq] like @lV10TFContratoArquivosAnexos_NomeArq)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoArquivosAnexos_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_NomeArq] = @AV11TFContratoArquivosAnexos_NomeArq_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoArquivosAnexos_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoArquivosAnexos_TipoArq)) ) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_TipoArq] like @lV12TFContratoArquivosAnexos_TipoArq)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoArquivosAnexos_TipoArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_TipoArq] = @AV13TFContratoArquivosAnexos_TipoArq_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContratoArquivosAnexos_Data) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_Data] >= @AV14TFContratoArquivosAnexos_Data)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContratoArquivosAnexos_Data_To) )
         {
            sWhereString = sWhereString + " and ([ContratoArquivosAnexos_Data] <= @AV15TFContratoArquivosAnexos_Data_To)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Contrato_Codigo], [ContratoArquivosAnexos_TipoArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UE2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
               case 1 :
                     return conditional_P00UE3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UE2 ;
          prmP00UE2 = new Object[] {
          new Object[] {"@AV34Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoArquivosAnexos_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFContratoArquivosAnexos_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratoArquivosAnexos_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContratoArquivosAnexos_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV14TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV15TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00UE3 ;
          prmP00UE3 = new Object[] {
          new Object[] {"@AV34Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoArquivosAnexos_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFContratoArquivosAnexos_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratoArquivosAnexos_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV13TFContratoArquivosAnexos_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV14TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV15TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UE2,100,0,true,false )
             ,new CursorDef("P00UE3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UE3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[13]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratoarquivosanexoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratoarquivosanexoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratoarquivosanexoswcfilterdata") )
          {
             return  ;
          }
          getcontratocontratoarquivosanexoswcfilterdata worker = new getcontratocontratoarquivosanexoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
