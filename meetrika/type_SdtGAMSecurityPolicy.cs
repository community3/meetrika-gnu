/*
               File: type_SdtGAMSecurityPolicy
        Description: GAMSecurityPolicy
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:12.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMSecurityPolicy : GxUserType, IGxExternalObject
   {
      public SdtGAMSecurityPolicy( )
      {
         initialize();
      }

      public SdtGAMSecurityPolicy( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public bool saveas( out SdtGAMSecurityPolicy gxTp_SecurityPolicy ,
                          out IGxCollection gxTp_Errors )
      {
         bool returnsaveas ;
         gxTp_SecurityPolicy = new SdtGAMSecurityPolicy(context);
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         returnsaveas = false;
         Artech.Security.GAMSecurityPolicy externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         returnsaveas = (bool)(GAMSecurityPolicy_externalReference.SaveAs(out externalParm0, out externalParm1));
         gxTp_SecurityPolicy.ExternalInstance = externalParm0;
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returnsaveas ;
      }

      public void load( int gxTp_Id )
      {
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         GAMSecurityPolicy_externalReference.Load(gxTp_Id);
         return  ;
      }

      public void save( )
      {
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         GAMSecurityPolicy_externalReference.Save();
         return  ;
      }

      public void delete( )
      {
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         GAMSecurityPolicy_externalReference.Delete();
         return  ;
      }

      public bool success( )
      {
         bool returnsuccess ;
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         returnsuccess = false;
         returnsuccess = (bool)(GAMSecurityPolicy_externalReference.Success());
         return returnsuccess ;
      }

      public bool fail( )
      {
         bool returnfail ;
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         returnfail = false;
         returnfail = (bool)(GAMSecurityPolicy_externalReference.Fail());
         return returnfail ;
      }

      public IGxCollection geterrors( )
      {
         IGxCollection returngeterrors ;
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         returngeterrors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         externalParm0 = GAMSecurityPolicy_externalReference.GetErrors();
         returngeterrors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returngeterrors ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMSecurityPolicy_externalReference == null )
         {
            GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
         }
         returntostring = "";
         returntostring = (String)(GAMSecurityPolicy_externalReference.ToString());
         return returntostring ;
      }

      public int gxTpr_Id
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.Id ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.Id = value;
         }

      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.GUID ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.GUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.Name ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.Name = value;
         }

      }

      public short gxTpr_Allowmultipleconcurrentwebsessions
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.AllowMultipleConcurrentWebSessions ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.AllowMultipleConcurrentWebSessions = value;
         }

      }

      public short gxTpr_Websessiontimeout
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.WebSessionTimeout ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.WebSessionTimeout = value;
         }

      }

      public int gxTpr_Oauthtokenexpire
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.OauthTokenExpire ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.OauthTokenExpire = value;
         }

      }

      public short gxTpr_Oauthtokenmaximumrenovations
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.OauthTokenMaximumRenovations ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.OauthTokenMaximumRenovations = value;
         }

      }

      public short gxTpr_Periodchangepassword
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.PeriodChangePassword ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.PeriodChangePassword = value;
         }

      }

      public short gxTpr_Minimumtimetochangepasswords
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.MinimumTimeToChangePasswords ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.MinimumTimeToChangePasswords = value;
         }

      }

      public short gxTpr_Minimumlengthpassword
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.MinimumLengthPassword ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.MinimumLengthPassword = value;
         }

      }

      public short gxTpr_Minimumnumericcharacterspassword
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.MinimumNumericCharactersPassword ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.MinimumNumericCharactersPassword = value;
         }

      }

      public short gxTpr_Minimumuppercasecharacterspassword
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.MinimumUpperCaseCharactersPassword ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.MinimumUpperCaseCharactersPassword = value;
         }

      }

      public short gxTpr_Minimumspecialcharacterspassword
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.MinimumSpecialCharactersPassword ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.MinimumSpecialCharactersPassword = value;
         }

      }

      public short gxTpr_Maximumpasswordhistoryentries
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.MaximumPasswordHistoryEntries ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.MaximumPasswordHistoryEntries = value;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.DateCreated ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.UserCreated ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.DateUpdated ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference.UserUpdated ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            GAMSecurityPolicy_externalReference.UserUpdated = value;
         }

      }

      public IGxCollection gxTpr_Descriptions
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMDescription", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm0 ;
            externalParm0 = GAMSecurityPolicy_externalReference.Descriptions;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMDescription>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), intValue.ExternalInstance);
            GAMSecurityPolicy_externalReference.Descriptions = externalParm1;
         }

      }

      public IGxCollection gxTpr_Properties
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMProperty", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm2 ;
            externalParm2 = GAMSecurityPolicy_externalReference.Properties;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), externalParm2);
            return intValue ;
         }

         set {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm3 ;
            intValue = value;
            externalParm3 = (System.Collections.Generic.List<Artech.Security.GAMProperty>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), intValue.ExternalInstance);
            GAMSecurityPolicy_externalReference.Properties = externalParm3;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMSecurityPolicy_externalReference == null )
            {
               GAMSecurityPolicy_externalReference = new Artech.Security.GAMSecurityPolicy(context);
            }
            return GAMSecurityPolicy_externalReference ;
         }

         set {
            GAMSecurityPolicy_externalReference = (Artech.Security.GAMSecurityPolicy)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMSecurityPolicy GAMSecurityPolicy_externalReference=null ;
   }

}
