/*
               File: ContratoGeneral
        Description: Contrato General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:14.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContrato_UnidadeContratacao = new GXCombobox();
         cmbContrato_PrdFtrCada = new GXCombobox();
         cmbavUnidadedemedicao = new GXCombobox();
         cmbContrato_CalculoDivergencia = new GXCombobox();
         chkContrato_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n74Contrato_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A74Contrato_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA672( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "ContratoGeneral";
               context.Gx_err = 0;
               cmbavUnidadedemedicao.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavUnidadedemedicao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUnidadedemedicao.Enabled), 5, 0)));
               /* Using cursor H00672 */
               pr_default.execute(0, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
               A39Contratada_Codigo = H00672_A39Contratada_Codigo[0];
               A1013Contrato_PrepostoCod = H00672_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00672_n1013Contrato_PrepostoCod[0];
               A453Contrato_IndiceDivergencia = H00672_A453Contrato_IndiceDivergencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
               A452Contrato_CalculoDivergencia = H00672_A452Contrato_CalculoDivergencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
               A90Contrato_RegrasPagto = H00672_A90Contrato_RegrasPagto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A90Contrato_RegrasPagto", A90Contrato_RegrasPagto);
               A91Contrato_DiasPagto = H00672_A91Contrato_DiasPagto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
               A2086Contrato_LmtFtr = H00672_A2086Contrato_LmtFtr[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
               n2086Contrato_LmtFtr = H00672_n2086Contrato_LmtFtr[0];
               A1358Contrato_PrdFtrFim = H00672_A1358Contrato_PrdFtrFim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
               n1358Contrato_PrdFtrFim = H00672_n1358Contrato_PrdFtrFim[0];
               A1357Contrato_PrdFtrIni = H00672_A1357Contrato_PrdFtrIni[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
               n1357Contrato_PrdFtrIni = H00672_n1357Contrato_PrdFtrIni[0];
               A1354Contrato_PrdFtrCada = H00672_A1354Contrato_PrdFtrCada[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
               n1354Contrato_PrdFtrCada = H00672_n1354Contrato_PrdFtrCada[0];
               A116Contrato_ValorUnidadeContratacao = H00672_A116Contrato_ValorUnidadeContratacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
               A81Contrato_Quantidade = H00672_A81Contrato_Quantidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
               A115Contrato_UnidadeContratacao = H00672_A115Contrato_UnidadeContratacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
               A89Contrato_Valor = H00672_A89Contrato_Valor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
               A88Contrato_DataFimAdaptacao = H00672_A88Contrato_DataFimAdaptacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
               A87Contrato_DataTerminoAta = H00672_A87Contrato_DataTerminoAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
               A86Contrato_DataPedidoReajuste = H00672_A86Contrato_DataPedidoReajuste[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
               A85Contrato_DataAssinatura = H00672_A85Contrato_DataAssinatura[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
               A84Contrato_DataPublicacaoDOU = H00672_A84Contrato_DataPublicacaoDOU[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
               A83Contrato_DataVigenciaTermino = H00672_A83Contrato_DataVigenciaTermino[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               A82Contrato_DataVigenciaInicio = H00672_A82Contrato_DataVigenciaInicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
               A80Contrato_Objeto = H00672_A80Contrato_Objeto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A80Contrato_Objeto", A80Contrato_Objeto);
               A79Contrato_Ano = H00672_A79Contrato_Ano[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A78Contrato_NumeroAta = H00672_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = H00672_n78Contrato_NumeroAta[0];
               A92Contrato_Ativo = H00672_A92Contrato_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A92Contrato_Ativo", A92Contrato_Ativo);
               A77Contrato_Numero = H00672_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               pr_default.close(0);
               /* Using cursor H00673 */
               pr_default.execute(1, new Object[] {A39Contratada_Codigo});
               A40Contratada_PessoaCod = H00673_A40Contratada_PessoaCod[0];
               A516Contratada_TipoFabrica = H00673_A516Contratada_TipoFabrica[0];
               pr_default.close(1);
               /* Using cursor H00674 */
               pr_default.execute(2, new Object[] {A40Contratada_PessoaCod});
               A42Contratada_PessoaCNPJ = H00674_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H00674_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00674_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H00674_n41Contratada_PessoaNom[0];
               pr_default.close(2);
               /* Using cursor H00675 */
               pr_default.execute(3, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
               A1016Contrato_PrepostoPesCod = H00675_A1016Contrato_PrepostoPesCod[0];
               n1016Contrato_PrepostoPesCod = H00675_n1016Contrato_PrepostoPesCod[0];
               pr_default.close(3);
               /* Using cursor H00676 */
               pr_default.execute(4, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
               A1015Contrato_PrepostoNom = H00676_A1015Contrato_PrepostoNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
               n1015Contrato_PrepostoNom = H00676_n1015Contrato_PrepostoNom[0];
               pr_default.close(4);
               A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATO_IDENTIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2096Contrato_Identificacao, ""))));
               /* Using cursor H00679 */
               pr_default.execute(5, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
               if ( (pr_default.getStatus(5) != 101) )
               {
                  A843Contrato_DataFimTA = H00679_A843Contrato_DataFimTA[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
                  n843Contrato_DataFimTA = H00679_n843Contrato_DataFimTA[0];
               }
               else
               {
                  A843Contrato_DataFimTA = DateTime.MinValue;
                  n843Contrato_DataFimTA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
               }
               pr_default.close(5);
               /* Using cursor H006712 */
               pr_default.execute(6, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
               if ( (pr_default.getStatus(6) != 101) )
               {
                  A842Contrato_DataInicioTA = H006712_A842Contrato_DataInicioTA[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
                  n842Contrato_DataInicioTA = H006712_n842Contrato_DataInicioTA[0];
               }
               else
               {
                  A842Contrato_DataInicioTA = DateTime.MinValue;
                  n842Contrato_DataInicioTA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
               }
               pr_default.close(6);
               WS672( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122191479");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratogeneral.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vUNIDADEDEMEDICAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV13UnidadeDeMedicao), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATO_IDENTIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2096Contrato_Identificacao, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm672( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratogeneral.js", "?20203122191483");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato General" ;
      }

      protected void WB670( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratogeneral.aspx");
            }
            wb_table1_2_672( true) ;
         }
         else
         {
            wb_table1_2_672( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_672e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContrato_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START672( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP670( ) ;
            }
         }
      }

      protected void WS672( )
      {
         START672( ) ;
         EVT672( ) ;
      }

      protected void EVT672( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11672 */
                                    E11672 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12672 */
                                    E12672 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13672 */
                                    E13672 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14672 */
                                    E14672 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15672 */
                                    E15672 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP670( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavUnidadedemedicao_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE672( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm672( ) ;
            }
         }
      }

      protected void PA672( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContrato_UnidadeContratacao.Name = "CONTRATO_UNIDADECONTRATACAO";
            cmbContrato_UnidadeContratacao.WebTags = "";
            cmbContrato_UnidadeContratacao.addItem("1", "Ponto Fun��o", 0);
            cmbContrato_UnidadeContratacao.addItem("2", "Horas", 0);
            cmbContrato_UnidadeContratacao.addItem("3", "UST", 0);
            if ( cmbContrato_UnidadeContratacao.ItemCount > 0 )
            {
               A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( cmbContrato_UnidadeContratacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
            }
            cmbContrato_PrdFtrCada.Name = "CONTRATO_PRDFTRCADA";
            cmbContrato_PrdFtrCada.WebTags = "";
            cmbContrato_PrdFtrCada.addItem("", "(Nenhum)", 0);
            cmbContrato_PrdFtrCada.addItem("M", "Mensal", 0);
            cmbContrato_PrdFtrCada.addItem("S", "Semanal", 0);
            cmbContrato_PrdFtrCada.addItem("P", "Periodo", 0);
            cmbContrato_PrdFtrCada.addItem("D", "Diario", 0);
            if ( cmbContrato_PrdFtrCada.ItemCount > 0 )
            {
               A1354Contrato_PrdFtrCada = cmbContrato_PrdFtrCada.getValidValue(A1354Contrato_PrdFtrCada);
               n1354Contrato_PrdFtrCada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
            }
            cmbavUnidadedemedicao.Name = "vUNIDADEDEMEDICAO";
            cmbavUnidadedemedicao.WebTags = "";
            cmbavUnidadedemedicao.addItem("0", "--", 0);
            cmbavUnidadedemedicao.addItem("1", "PF", 0);
            cmbavUnidadedemedicao.addItem("2", "Hs.", 0);
            cmbavUnidadedemedicao.addItem("3", "UST", 0);
            if ( cmbavUnidadedemedicao.ItemCount > 0 )
            {
               AV13UnidadeDeMedicao = (short)(NumberUtil.Val( cmbavUnidadedemedicao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13UnidadeDeMedicao), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13UnidadeDeMedicao), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vUNIDADEDEMEDICAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV13UnidadeDeMedicao), "ZZZ9")));
            }
            cmbContrato_CalculoDivergencia.Name = "CONTRATO_CALCULODIVERGENCIA";
            cmbContrato_CalculoDivergencia.WebTags = "";
            cmbContrato_CalculoDivergencia.addItem("B", "Sob PF Bruto", 0);
            cmbContrato_CalculoDivergencia.addItem("L", "Sob PF Liquido", 0);
            cmbContrato_CalculoDivergencia.addItem("A", "Maior diverg�ncia entre ambos", 0);
            if ( cmbContrato_CalculoDivergencia.ItemCount > 0 )
            {
               A452Contrato_CalculoDivergencia = cmbContrato_CalculoDivergencia.getValidValue(A452Contrato_CalculoDivergencia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
            }
            chkContrato_Ativo.Name = "CONTRATO_ATIVO";
            chkContrato_Ativo.WebTags = "";
            chkContrato_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkContrato_Ativo_Internalname, "TitleCaption", chkContrato_Ativo.Caption);
            chkContrato_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavUnidadedemedicao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContrato_UnidadeContratacao.ItemCount > 0 )
         {
            A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( cmbContrato_UnidadeContratacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
         }
         if ( cmbContrato_PrdFtrCada.ItemCount > 0 )
         {
            A1354Contrato_PrdFtrCada = cmbContrato_PrdFtrCada.getValidValue(A1354Contrato_PrdFtrCada);
            n1354Contrato_PrdFtrCada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
         }
         if ( cmbavUnidadedemedicao.ItemCount > 0 )
         {
            AV13UnidadeDeMedicao = (short)(NumberUtil.Val( cmbavUnidadedemedicao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13UnidadeDeMedicao), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13UnidadeDeMedicao), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vUNIDADEDEMEDICAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV13UnidadeDeMedicao), "ZZZ9")));
         }
         if ( cmbContrato_CalculoDivergencia.ItemCount > 0 )
         {
            A452Contrato_CalculoDivergencia = cmbContrato_CalculoDivergencia.getValidValue(A452Contrato_CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF672( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "ContratoGeneral";
         context.Gx_err = 0;
         cmbavUnidadedemedicao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavUnidadedemedicao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUnidadedemedicao.Enabled), 5, 0)));
      }

      protected void RF672( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H006717 */
            pr_default.execute(7, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A2098ContratoCaixaEntrada_Codigo = H006717_A2098ContratoCaixaEntrada_Codigo[0];
               A843Contrato_DataFimTA = H006717_A843Contrato_DataFimTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
               n843Contrato_DataFimTA = H006717_n843Contrato_DataFimTA[0];
               A842Contrato_DataInicioTA = H006717_A842Contrato_DataInicioTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
               n842Contrato_DataInicioTA = H006717_n842Contrato_DataInicioTA[0];
               A843Contrato_DataFimTA = H006717_A843Contrato_DataFimTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
               n843Contrato_DataFimTA = H006717_n843Contrato_DataFimTA[0];
               A842Contrato_DataInicioTA = H006717_A842Contrato_DataInicioTA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
               n842Contrato_DataInicioTA = H006717_n842Contrato_DataInicioTA[0];
               /* Execute user event: E12672 */
               E12672 ();
               pr_default.readNext(7);
            }
            pr_default.close(7);
            WB670( ) ;
         }
      }

      protected void STRUP670( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "ContratoGeneral";
         context.Gx_err = 0;
         cmbavUnidadedemedicao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavUnidadedemedicao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavUnidadedemedicao.Enabled), 5, 0)));
         /* Using cursor H006718 */
         pr_default.execute(8, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         A39Contratada_Codigo = H006718_A39Contratada_Codigo[0];
         A1013Contrato_PrepostoCod = H006718_A1013Contrato_PrepostoCod[0];
         n1013Contrato_PrepostoCod = H006718_n1013Contrato_PrepostoCod[0];
         A453Contrato_IndiceDivergencia = H006718_A453Contrato_IndiceDivergencia[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
         A452Contrato_CalculoDivergencia = H006718_A452Contrato_CalculoDivergencia[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
         A90Contrato_RegrasPagto = H006718_A90Contrato_RegrasPagto[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A90Contrato_RegrasPagto", A90Contrato_RegrasPagto);
         A91Contrato_DiasPagto = H006718_A91Contrato_DiasPagto[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
         A2086Contrato_LmtFtr = H006718_A2086Contrato_LmtFtr[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
         n2086Contrato_LmtFtr = H006718_n2086Contrato_LmtFtr[0];
         A1358Contrato_PrdFtrFim = H006718_A1358Contrato_PrdFtrFim[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
         n1358Contrato_PrdFtrFim = H006718_n1358Contrato_PrdFtrFim[0];
         A1357Contrato_PrdFtrIni = H006718_A1357Contrato_PrdFtrIni[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
         n1357Contrato_PrdFtrIni = H006718_n1357Contrato_PrdFtrIni[0];
         A1354Contrato_PrdFtrCada = H006718_A1354Contrato_PrdFtrCada[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
         n1354Contrato_PrdFtrCada = H006718_n1354Contrato_PrdFtrCada[0];
         A116Contrato_ValorUnidadeContratacao = H006718_A116Contrato_ValorUnidadeContratacao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
         A81Contrato_Quantidade = H006718_A81Contrato_Quantidade[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
         A115Contrato_UnidadeContratacao = H006718_A115Contrato_UnidadeContratacao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
         A89Contrato_Valor = H006718_A89Contrato_Valor[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
         A88Contrato_DataFimAdaptacao = H006718_A88Contrato_DataFimAdaptacao[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
         A87Contrato_DataTerminoAta = H006718_A87Contrato_DataTerminoAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
         A86Contrato_DataPedidoReajuste = H006718_A86Contrato_DataPedidoReajuste[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
         A85Contrato_DataAssinatura = H006718_A85Contrato_DataAssinatura[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
         A84Contrato_DataPublicacaoDOU = H006718_A84Contrato_DataPublicacaoDOU[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
         A83Contrato_DataVigenciaTermino = H006718_A83Contrato_DataVigenciaTermino[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
         A82Contrato_DataVigenciaInicio = H006718_A82Contrato_DataVigenciaInicio[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
         A80Contrato_Objeto = H006718_A80Contrato_Objeto[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A80Contrato_Objeto", A80Contrato_Objeto);
         A79Contrato_Ano = H006718_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A78Contrato_NumeroAta = H006718_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = H006718_n78Contrato_NumeroAta[0];
         A92Contrato_Ativo = H006718_A92Contrato_Ativo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A92Contrato_Ativo", A92Contrato_Ativo);
         A77Contrato_Numero = H006718_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
         pr_default.close(8);
         /* Using cursor H006719 */
         pr_default.execute(9, new Object[] {A39Contratada_Codigo});
         A40Contratada_PessoaCod = H006719_A40Contratada_PessoaCod[0];
         A516Contratada_TipoFabrica = H006719_A516Contratada_TipoFabrica[0];
         pr_default.close(9);
         /* Using cursor H006720 */
         pr_default.execute(10, new Object[] {A40Contratada_PessoaCod});
         A42Contratada_PessoaCNPJ = H006720_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = H006720_n42Contratada_PessoaCNPJ[0];
         A41Contratada_PessoaNom = H006720_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = H006720_n41Contratada_PessoaNom[0];
         pr_default.close(10);
         /* Using cursor H006721 */
         pr_default.execute(11, new Object[] {n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod});
         A1016Contrato_PrepostoPesCod = H006721_A1016Contrato_PrepostoPesCod[0];
         n1016Contrato_PrepostoPesCod = H006721_n1016Contrato_PrepostoPesCod[0];
         pr_default.close(11);
         /* Using cursor H006722 */
         pr_default.execute(12, new Object[] {n1016Contrato_PrepostoPesCod, A1016Contrato_PrepostoPesCod});
         A1015Contrato_PrepostoNom = H006722_A1015Contrato_PrepostoNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
         n1015Contrato_PrepostoNom = H006722_n1015Contrato_PrepostoNom[0];
         pr_default.close(12);
         A2096Contrato_Identificacao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATO_IDENTIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2096Contrato_Identificacao, ""))));
         /* Using cursor H006725 */
         pr_default.execute(13, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A843Contrato_DataFimTA = H006725_A843Contrato_DataFimTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
            n843Contrato_DataFimTA = H006725_n843Contrato_DataFimTA[0];
         }
         else
         {
            A843Contrato_DataFimTA = DateTime.MinValue;
            n843Contrato_DataFimTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
         }
         pr_default.close(13);
         /* Using cursor H006728 */
         pr_default.execute(14, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            A842Contrato_DataInicioTA = H006728_A842Contrato_DataInicioTA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
            n842Contrato_DataInicioTA = H006728_n842Contrato_DataInicioTA[0];
         }
         else
         {
            A842Contrato_DataInicioTA = DateTime.MinValue;
            n842Contrato_DataInicioTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
         }
         pr_default.close(14);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11672 */
         E11672 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
            n41Contratada_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
            n42Contratada_PessoaCNPJ = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            A1015Contrato_PrepostoNom = StringUtil.Upper( cgiGet( edtContrato_PrepostoNom_Internalname));
            n1015Contrato_PrepostoNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1015Contrato_PrepostoNom", A1015Contrato_PrepostoNom);
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
            n78Contrato_NumeroAta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A80Contrato_Objeto = cgiGet( edtContrato_Objeto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A80Contrato_Objeto", A80Contrato_Objeto);
            A82Contrato_DataVigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataVigenciaInicio_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A82Contrato_DataVigenciaInicio", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
            A83Contrato_DataVigenciaTermino = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataVigenciaTermino_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A83Contrato_DataVigenciaTermino", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
            A842Contrato_DataInicioTA = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataInicioTA_Internalname), 0));
            n842Contrato_DataInicioTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A842Contrato_DataInicioTA", context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"));
            A843Contrato_DataFimTA = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataFimTA_Internalname), 0));
            n843Contrato_DataFimTA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A843Contrato_DataFimTA", context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"));
            A84Contrato_DataPublicacaoDOU = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataPublicacaoDOU_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A84Contrato_DataPublicacaoDOU", context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"));
            A85Contrato_DataAssinatura = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataAssinatura_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A85Contrato_DataAssinatura", context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"));
            A86Contrato_DataPedidoReajuste = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataPedidoReajuste_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A86Contrato_DataPedidoReajuste", context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"));
            A87Contrato_DataTerminoAta = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataTerminoAta_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A87Contrato_DataTerminoAta", context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"));
            A88Contrato_DataFimAdaptacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataFimAdaptacao_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A88Contrato_DataFimAdaptacao", context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"));
            A89Contrato_Valor = context.localUtil.CToN( cgiGet( edtContrato_Valor_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A89Contrato_Valor", StringUtil.LTrim( StringUtil.Str( A89Contrato_Valor, 18, 5)));
            cmbContrato_UnidadeContratacao.CurrentValue = cgiGet( cmbContrato_UnidadeContratacao_Internalname);
            A115Contrato_UnidadeContratacao = (short)(NumberUtil.Val( cgiGet( cmbContrato_UnidadeContratacao_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A115Contrato_UnidadeContratacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)));
            A81Contrato_Quantidade = (int)(context.localUtil.CToN( cgiGet( edtContrato_Quantidade_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A81Contrato_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(A81Contrato_Quantidade), 9, 0)));
            A116Contrato_ValorUnidadeContratacao = context.localUtil.CToN( cgiGet( edtContrato_ValorUnidadeContratacao_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A116Contrato_ValorUnidadeContratacao", StringUtil.LTrim( StringUtil.Str( A116Contrato_ValorUnidadeContratacao, 18, 5)));
            cmbContrato_PrdFtrCada.CurrentValue = cgiGet( cmbContrato_PrdFtrCada_Internalname);
            A1354Contrato_PrdFtrCada = cgiGet( cmbContrato_PrdFtrCada_Internalname);
            n1354Contrato_PrdFtrCada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1354Contrato_PrdFtrCada", A1354Contrato_PrdFtrCada);
            A1357Contrato_PrdFtrIni = (short)(context.localUtil.CToN( cgiGet( edtContrato_PrdFtrIni_Internalname), ",", "."));
            n1357Contrato_PrdFtrIni = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1357Contrato_PrdFtrIni", StringUtil.LTrim( StringUtil.Str( (decimal)(A1357Contrato_PrdFtrIni), 4, 0)));
            A1358Contrato_PrdFtrFim = (short)(context.localUtil.CToN( cgiGet( edtContrato_PrdFtrFim_Internalname), ",", "."));
            n1358Contrato_PrdFtrFim = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1358Contrato_PrdFtrFim", StringUtil.LTrim( StringUtil.Str( (decimal)(A1358Contrato_PrdFtrFim), 4, 0)));
            A2086Contrato_LmtFtr = context.localUtil.CToN( cgiGet( edtContrato_LmtFtr_Internalname), ",", ".");
            n2086Contrato_LmtFtr = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2086Contrato_LmtFtr", StringUtil.LTrim( StringUtil.Str( A2086Contrato_LmtFtr, 14, 5)));
            cmbavUnidadedemedicao.CurrentValue = cgiGet( cmbavUnidadedemedicao_Internalname);
            AV13UnidadeDeMedicao = (short)(NumberUtil.Val( cgiGet( cmbavUnidadedemedicao_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13UnidadeDeMedicao", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13UnidadeDeMedicao), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vUNIDADEDEMEDICAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(AV13UnidadeDeMedicao), "ZZZ9")));
            A91Contrato_DiasPagto = (short)(context.localUtil.CToN( cgiGet( edtContrato_DiasPagto_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A91Contrato_DiasPagto", StringUtil.LTrim( StringUtil.Str( (decimal)(A91Contrato_DiasPagto), 4, 0)));
            A90Contrato_RegrasPagto = cgiGet( edtContrato_RegrasPagto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A90Contrato_RegrasPagto", A90Contrato_RegrasPagto);
            cmbContrato_CalculoDivergencia.CurrentValue = cgiGet( cmbContrato_CalculoDivergencia_Internalname);
            A452Contrato_CalculoDivergencia = cgiGet( cmbContrato_CalculoDivergencia_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A452Contrato_CalculoDivergencia", A452Contrato_CalculoDivergencia);
            A453Contrato_IndiceDivergencia = context.localUtil.CToN( cgiGet( edtContrato_IndiceDivergencia_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A453Contrato_IndiceDivergencia", StringUtil.LTrim( StringUtil.Str( A453Contrato_IndiceDivergencia, 6, 2)));
            A92Contrato_Ativo = StringUtil.StrToBool( cgiGet( chkContrato_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A92Contrato_Ativo", A92Contrato_Ativo);
            A2096Contrato_Identificacao = cgiGet( edtContrato_Identificacao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2096Contrato_Identificacao", A2096Contrato_Identificacao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATO_IDENTIFICACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2096Contrato_Identificacao, ""))));
            /* Read saved values. */
            wcpOA74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA74Contrato_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11672 */
         E11672 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11672( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12672( )
      {
         /* Load Routine */
         edtContratada_PessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaNom_Internalname, "Link", edtContratada_PessoaNom_Link);
         edtContrato_PrepostoNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1013Contrato_PrepostoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_PrepostoNom_Internalname, "Link", edtContrato_PrepostoNom_Link);
         edtContrato_Numero_Link = formatLink("viewcontratocaixaentrada.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +A2098ContratoCaixaEntrada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_Numero_Internalname, "Link", edtContrato_Numero_Link);
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
         AV12Contratada_Codigo = A39Contratada_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Contratada_Codigo), 6, 0)));
         if ( StringUtil.StrCmp(A516Contratada_TipoFabrica, "I") == 0 )
         {
            lblTextblockcontrato_prepostonom_Caption = "Respons�vel";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTextblockcontrato_prepostonom_Internalname, "Caption", lblTextblockcontrato_prepostonom_Caption);
         }
         else
         {
            lblTextblockcontrato_prepostonom_Caption = "Preposto";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTextblockcontrato_prepostonom_Internalname, "Caption", lblTextblockcontrato_prepostonom_Caption);
         }
      }

      protected void E13672( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +AV12Contratada_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14672( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +AV12Contratada_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15672( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwcontrato.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contrato";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Contrato_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_672( true) ;
         }
         else
         {
            wb_table2_8_672( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_672e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_177_672( true) ;
         }
         else
         {
            wb_table3_177_672( false) ;
         }
         return  ;
      }

      protected void wb_table3_177_672e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_672e( true) ;
         }
         else
         {
            wb_table1_2_672e( false) ;
         }
      }

      protected void wb_table3_177_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 180,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 182,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_177_672e( true) ;
         }
         else
         {
            wb_table3_177_672e( false) ;
         }
      }

      protected void wb_table2_8_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratada_PessoaNom_Link, "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prepostonom_Internalname, lblTextblockcontrato_prepostonom_Caption, "", "", lblTextblockcontrato_prepostonom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_PrepostoNom_Internalname, StringUtil.RTrim( A1015Contrato_PrepostoNom), StringUtil.RTrim( context.localUtil.Format( A1015Contrato_PrepostoNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContrato_PrepostoNom_Link, "", "", "", edtContrato_PrepostoNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContrato_Numero_Link, "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_objeto_Internalname, "Objeto do Contrato", "", "", lblTextblockcontrato_objeto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContrato_Objeto_Internalname, A80Contrato_Objeto, "", "", 0, 1, 0, 0, 100, "%", 2, "row", StyleString, ClassString, "", "10000", 1, "", "", -1, true, "TextoObjeto", "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            wb_table4_42_672( true) ;
         }
         else
         {
            wb_table4_42_672( false) ;
         }
         return  ;
      }

      protected void wb_table4_42_672e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_valor_Internalname, "Valor do Contrato", "", "", lblTextblockcontrato_valor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A89Contrato_Valor, 18, 5, ",", "")), context.localUtil.Format( A89Contrato_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_unidadecontratacao_Internalname, "Unidade de Contrata��o", "", "", lblTextblockcontrato_unidadecontratacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContrato_UnidadeContratacao, cmbContrato_UnidadeContratacao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0)), 1, cmbContrato_UnidadeContratacao_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoGeneral.htm");
            cmbContrato_UnidadeContratacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A115Contrato_UnidadeContratacao), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContrato_UnidadeContratacao_Internalname, "Values", (String)(cmbContrato_UnidadeContratacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_quantidade_Internalname, "Quantidade Contratada", "", "", lblTextblockcontrato_quantidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Quantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A81Contrato_Quantidade), 9, 0, ",", "")), context.localUtil.Format( (decimal)(A81Contrato_Quantidade), "ZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Quantidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "QuantidadeContrato", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_valorunidadecontratacao_Internalname, "Valor Unidade Contrata��o", "", "", lblTextblockcontrato_valorunidadecontratacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_ValorUnidadeContratacao_Internalname, StringUtil.LTrim( StringUtil.NToC( A116Contrato_ValorUnidadeContratacao, 18, 5, ",", "")), context.localUtil.Format( A116Contrato_ValorUnidadeContratacao, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_ValorUnidadeContratacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prdftrcada_Internalname, "Faturamento", "", "", lblTextblockcontrato_prdftrcada_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContrato_PrdFtrCada, cmbContrato_PrdFtrCada_Internalname, StringUtil.RTrim( A1354Contrato_PrdFtrCada), 1, cmbContrato_PrdFtrCada_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratoGeneral.htm");
            cmbContrato_PrdFtrCada.CurrentValue = StringUtil.RTrim( A1354Contrato_PrdFtrCada);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContrato_PrdFtrCada_Internalname, "Values", (String)(cmbContrato_PrdFtrCada.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prdftrini_Internalname, "Inicio", "", "", lblTextblockcontrato_prdftrini_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_PrdFtrIni_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1357Contrato_PrdFtrIni), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1357Contrato_PrdFtrIni), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_PrdFtrIni_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_prdftrfim_Internalname, "Fim", "", "", lblTextblockcontrato_prdftrfim_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_PrdFtrFim_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1358Contrato_PrdFtrFim), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1358Contrato_PrdFtrFim), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_PrdFtrFim_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_lmtftr_Internalname, "Limite de Faturamento", "", "", lblTextblockcontrato_lmtftr_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table5_131_672( true) ;
         }
         else
         {
            wb_table5_131_672( false) ;
         }
         return  ;
      }

      protected void wb_table5_131_672e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_diaspagto_Internalname, "Dias para Pagto", "", "", lblTextblockcontrato_diaspagto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_DiasPagto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A91Contrato_DiasPagto), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A91Contrato_DiasPagto), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DiasPagto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_regraspagto_Internalname, "Regras para pagamento", "", "", lblTextblockcontrato_regraspagto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContrato_RegrasPagto_Internalname, A90Contrato_RegrasPagto, "", "", 0, 1, 0, 0, 100, "%", 2, "row", StyleString, ClassString, "", "500", 1, "", "", 0, true, "RegrasPagto", "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_calculodivergencia_Internalname, "C�lculo da Diverg�ncia:", "", "", lblTextblockcontrato_calculodivergencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContrato_CalculoDivergencia, cmbContrato_CalculoDivergencia_Internalname, StringUtil.RTrim( A452Contrato_CalculoDivergencia), 1, cmbContrato_CalculoDivergencia_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratoGeneral.htm");
            cmbContrato_CalculoDivergencia.CurrentValue = StringUtil.RTrim( A452Contrato_CalculoDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContrato_CalculoDivergencia_Internalname, "Values", (String)(cmbContrato_CalculoDivergencia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_indicedivergencia_Internalname, "Indice de Aceita��o", "", "", lblTextblockcontrato_indicedivergencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table6_156_672( true) ;
         }
         else
         {
            wb_table6_156_672( false) ;
         }
         return  ;
      }

      protected void wb_table6_156_672e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ativo_Internalname, "Ativo", "", "", lblTextblockcontrato_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContrato_Ativo_Internalname, StringUtil.BoolToStr( A92Contrato_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_identificacao_Internalname, "Contrato ", "", "", lblTextblockcontrato_identificacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Identificacao_Internalname, A2096Contrato_Identificacao, StringUtil.RTrim( context.localUtil.Format( A2096Contrato_Identificacao, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Identificacao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_672e( true) ;
         }
         else
         {
            wb_table2_8_672e( false) ;
         }
      }

      protected void wb_table6_156_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_indicedivergencia_Internalname, tblTablemergedcontrato_indicedivergencia_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_IndiceDivergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( A453Contrato_IndiceDivergencia, 6, 2, ",", "")), context.localUtil.Format( A453Contrato_IndiceDivergencia, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_IndiceDivergencia_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContrato_indicedivergencia_righttext_Internalname, "%", "", "", lblContrato_indicedivergencia_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_156_672e( true) ;
         }
         else
         {
            wb_table6_156_672e( false) ;
         }
      }

      protected void wb_table5_131_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_lmtftr_Internalname, tblTablemergedcontrato_lmtftr_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_LmtFtr_Internalname, StringUtil.LTrim( StringUtil.NToC( A2086Contrato_LmtFtr, 14, 5, ",", "")), context.localUtil.Format( A2086Contrato_LmtFtr, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_LmtFtr_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'" + sPrefix + "',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUnidadedemedicao, cmbavUnidadedemedicao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13UnidadeDeMedicao), 4, 0)), 1, cmbavUnidadedemedicao_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavUnidadedemedicao.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", "", true, "HLP_ContratoGeneral.htm");
            cmbavUnidadedemedicao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13UnidadeDeMedicao), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavUnidadedemedicao_Internalname, "Values", (String)(cmbavUnidadedemedicao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_131_672e( true) ;
         }
         else
         {
            wb_table5_131_672e( false) ;
         }
      }

      protected void wb_table4_42_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciainicio_Internalname, "Data Vig�ncia", "", "", lblTextblockcontrato_datavigenciainicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table7_47_672( true) ;
         }
         else
         {
            wb_table7_47_672( false) ;
         }
         return  ;
      }

      protected void wb_table7_47_672e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datainiciota_Internalname, "Termo Aditivo", "", "", lblTextblockcontrato_datainiciota_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table8_58_672( true) ;
         }
         else
         {
            wb_table8_58_672( false) ;
         }
         return  ;
      }

      protected void wb_table8_58_672e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datapublicacaodou_Internalname, "Data Publicacao DOU", "", "", lblTextblockcontrato_datapublicacaodou_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataPublicacaoDOU_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataPublicacaoDOU_Internalname, context.localUtil.Format(A84Contrato_DataPublicacaoDOU, "99/99/99"), context.localUtil.Format( A84Contrato_DataPublicacaoDOU, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataPublicacaoDOU_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataPublicacaoDOU_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_dataassinatura_Internalname, "Data Assinatura", "", "", lblTextblockcontrato_dataassinatura_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataAssinatura_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataAssinatura_Internalname, context.localUtil.Format(A85Contrato_DataAssinatura, "99/99/99"), context.localUtil.Format( A85Contrato_DataAssinatura, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataAssinatura_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataAssinatura_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datapedidoreajuste_Internalname, "Data Pedido Reajuste", "", "", lblTextblockcontrato_datapedidoreajuste_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataPedidoReajuste_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataPedidoReajuste_Internalname, context.localUtil.Format(A86Contrato_DataPedidoReajuste, "99/99/99"), context.localUtil.Format( A86Contrato_DataPedidoReajuste, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataPedidoReajuste_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataPedidoReajuste_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_dataterminoata_Internalname, "Data Termino Ata", "", "", lblTextblockcontrato_dataterminoata_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataTerminoAta_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataTerminoAta_Internalname, context.localUtil.Format(A87Contrato_DataTerminoAta, "99/99/99"), context.localUtil.Format( A87Contrato_DataTerminoAta, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataTerminoAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataTerminoAta_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datafimadaptacao_Internalname, "Data Fim Adapta��o", "", "", lblTextblockcontrato_datafimadaptacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataFimAdaptacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataFimAdaptacao_Internalname, context.localUtil.Format(A88Contrato_DataFimAdaptacao, "99/99/99"), context.localUtil.Format( A88Contrato_DataFimAdaptacao, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataFimAdaptacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataFimAdaptacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_42_672e( true) ;
         }
         else
         {
            wb_table4_42_672e( false) ;
         }
      }

      protected void wb_table8_58_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_datainiciota_Internalname, tblTablemergedcontrato_datainiciota_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataInicioTA_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataInicioTA_Internalname, context.localUtil.Format(A842Contrato_DataInicioTA, "99/99/99"), context.localUtil.Format( A842Contrato_DataInicioTA, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataInicioTA_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataInicioTA_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datafimta_Internalname, "-", "", "", lblTextblockcontrato_datafimta_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataFimTA_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataFimTA_Internalname, context.localUtil.Format(A843Contrato_DataFimTA, "99/99/99"), context.localUtil.Format( A843Contrato_DataFimTA, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataFimTA_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataFimTA_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_58_672e( true) ;
         }
         else
         {
            wb_table8_58_672e( false) ;
         }
      }

      protected void wb_table7_47_672( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_datavigenciainicio_Internalname, tblTablemergedcontrato_datavigenciainicio_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaInicio_Internalname, context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"), context.localUtil.Format( A82Contrato_DataVigenciaInicio, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaInicio_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_datavigenciatermino_Internalname, "-", "", "", lblTextblockcontrato_datavigenciatermino_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContrato_DataVigenciaTermino_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContrato_DataVigenciaTermino_Internalname, context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"), context.localUtil.Format( A83Contrato_DataVigenciaTermino, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_DataVigenciaTermino_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContrato_DataVigenciaTermino_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_47_672e( true) ;
         }
         else
         {
            wb_table7_47_672e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A74Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         n74Contrato_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA672( ) ;
         WS672( ) ;
         WE672( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA74Contrato_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA672( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA672( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A74Contrato_Codigo = Convert.ToInt32(getParm(obj,2));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         wcpOA74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA74Contrato_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A74Contrato_Codigo != wcpOA74Contrato_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA74Contrato_Codigo = A74Contrato_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA74Contrato_Codigo = cgiGet( sPrefix+"A74Contrato_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA74Contrato_Codigo) > 0 )
         {
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA74Contrato_Codigo), ",", "."));
            n74Contrato_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         else
         {
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A74Contrato_Codigo_PARM"), ",", "."));
            n74Contrato_Codigo = false;
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA672( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS672( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS672( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A74Contrato_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA74Contrato_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A74Contrato_Codigo_CTRL", StringUtil.RTrim( sCtrlA74Contrato_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE672( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122191735");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratogeneral.js", "?20203122191735");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratada_pessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ";
         lblTextblockcontrato_prepostonom_Internalname = sPrefix+"TEXTBLOCKCONTRATO_PREPOSTONOM";
         edtContrato_PrepostoNom_Internalname = sPrefix+"CONTRATO_PREPOSTONOM";
         lblTextblockcontrato_numero_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = sPrefix+"CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = sPrefix+"TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = sPrefix+"CONTRATO_ANO";
         lblTextblockcontrato_objeto_Internalname = sPrefix+"TEXTBLOCKCONTRATO_OBJETO";
         edtContrato_Objeto_Internalname = sPrefix+"CONTRATO_OBJETO";
         lblTextblockcontrato_datavigenciainicio_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAVIGENCIAINICIO";
         edtContrato_DataVigenciaInicio_Internalname = sPrefix+"CONTRATO_DATAVIGENCIAINICIO";
         lblTextblockcontrato_datavigenciatermino_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAVIGENCIATERMINO";
         edtContrato_DataVigenciaTermino_Internalname = sPrefix+"CONTRATO_DATAVIGENCIATERMINO";
         tblTablemergedcontrato_datavigenciainicio_Internalname = sPrefix+"TABLEMERGEDCONTRATO_DATAVIGENCIAINICIO";
         lblTextblockcontrato_datainiciota_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAINICIOTA";
         edtContrato_DataInicioTA_Internalname = sPrefix+"CONTRATO_DATAINICIOTA";
         lblTextblockcontrato_datafimta_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAFIMTA";
         edtContrato_DataFimTA_Internalname = sPrefix+"CONTRATO_DATAFIMTA";
         tblTablemergedcontrato_datainiciota_Internalname = sPrefix+"TABLEMERGEDCONTRATO_DATAINICIOTA";
         lblTextblockcontrato_datapublicacaodou_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAPUBLICACAODOU";
         edtContrato_DataPublicacaoDOU_Internalname = sPrefix+"CONTRATO_DATAPUBLICACAODOU";
         lblTextblockcontrato_dataassinatura_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAASSINATURA";
         edtContrato_DataAssinatura_Internalname = sPrefix+"CONTRATO_DATAASSINATURA";
         lblTextblockcontrato_datapedidoreajuste_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAPEDIDOREAJUSTE";
         edtContrato_DataPedidoReajuste_Internalname = sPrefix+"CONTRATO_DATAPEDIDOREAJUSTE";
         lblTextblockcontrato_dataterminoata_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATATERMINOATA";
         edtContrato_DataTerminoAta_Internalname = sPrefix+"CONTRATO_DATATERMINOATA";
         lblTextblockcontrato_datafimadaptacao_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DATAFIMADAPTACAO";
         edtContrato_DataFimAdaptacao_Internalname = sPrefix+"CONTRATO_DATAFIMADAPTACAO";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         lblTextblockcontrato_valor_Internalname = sPrefix+"TEXTBLOCKCONTRATO_VALOR";
         edtContrato_Valor_Internalname = sPrefix+"CONTRATO_VALOR";
         lblTextblockcontrato_unidadecontratacao_Internalname = sPrefix+"TEXTBLOCKCONTRATO_UNIDADECONTRATACAO";
         cmbContrato_UnidadeContratacao_Internalname = sPrefix+"CONTRATO_UNIDADECONTRATACAO";
         lblTextblockcontrato_quantidade_Internalname = sPrefix+"TEXTBLOCKCONTRATO_QUANTIDADE";
         edtContrato_Quantidade_Internalname = sPrefix+"CONTRATO_QUANTIDADE";
         lblTextblockcontrato_valorunidadecontratacao_Internalname = sPrefix+"TEXTBLOCKCONTRATO_VALORUNIDADECONTRATACAO";
         edtContrato_ValorUnidadeContratacao_Internalname = sPrefix+"CONTRATO_VALORUNIDADECONTRATACAO";
         lblTextblockcontrato_prdftrcada_Internalname = sPrefix+"TEXTBLOCKCONTRATO_PRDFTRCADA";
         cmbContrato_PrdFtrCada_Internalname = sPrefix+"CONTRATO_PRDFTRCADA";
         lblTextblockcontrato_prdftrini_Internalname = sPrefix+"TEXTBLOCKCONTRATO_PRDFTRINI";
         edtContrato_PrdFtrIni_Internalname = sPrefix+"CONTRATO_PRDFTRINI";
         lblTextblockcontrato_prdftrfim_Internalname = sPrefix+"TEXTBLOCKCONTRATO_PRDFTRFIM";
         edtContrato_PrdFtrFim_Internalname = sPrefix+"CONTRATO_PRDFTRFIM";
         lblTextblockcontrato_lmtftr_Internalname = sPrefix+"TEXTBLOCKCONTRATO_LMTFTR";
         edtContrato_LmtFtr_Internalname = sPrefix+"CONTRATO_LMTFTR";
         cmbavUnidadedemedicao_Internalname = sPrefix+"vUNIDADEDEMEDICAO";
         tblTablemergedcontrato_lmtftr_Internalname = sPrefix+"TABLEMERGEDCONTRATO_LMTFTR";
         lblTextblockcontrato_diaspagto_Internalname = sPrefix+"TEXTBLOCKCONTRATO_DIASPAGTO";
         edtContrato_DiasPagto_Internalname = sPrefix+"CONTRATO_DIASPAGTO";
         lblTextblockcontrato_regraspagto_Internalname = sPrefix+"TEXTBLOCKCONTRATO_REGRASPAGTO";
         edtContrato_RegrasPagto_Internalname = sPrefix+"CONTRATO_REGRASPAGTO";
         lblTextblockcontrato_calculodivergencia_Internalname = sPrefix+"TEXTBLOCKCONTRATO_CALCULODIVERGENCIA";
         cmbContrato_CalculoDivergencia_Internalname = sPrefix+"CONTRATO_CALCULODIVERGENCIA";
         lblTextblockcontrato_indicedivergencia_Internalname = sPrefix+"TEXTBLOCKCONTRATO_INDICEDIVERGENCIA";
         edtContrato_IndiceDivergencia_Internalname = sPrefix+"CONTRATO_INDICEDIVERGENCIA";
         lblContrato_indicedivergencia_righttext_Internalname = sPrefix+"CONTRATO_INDICEDIVERGENCIA_RIGHTTEXT";
         tblTablemergedcontrato_indicedivergencia_Internalname = sPrefix+"TABLEMERGEDCONTRATO_INDICEDIVERGENCIA";
         lblTextblockcontrato_ativo_Internalname = sPrefix+"TEXTBLOCKCONTRATO_ATIVO";
         chkContrato_Ativo_Internalname = sPrefix+"CONTRATO_ATIVO";
         lblTextblockcontrato_identificacao_Internalname = sPrefix+"TEXTBLOCKCONTRATO_IDENTIFICACAO";
         edtContrato_Identificacao_Internalname = sPrefix+"CONTRATO_IDENTIFICACAO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContrato_DataVigenciaTermino_Jsonclick = "";
         edtContrato_DataVigenciaInicio_Jsonclick = "";
         edtContrato_DataFimTA_Jsonclick = "";
         edtContrato_DataInicioTA_Jsonclick = "";
         edtContrato_DataFimAdaptacao_Jsonclick = "";
         edtContrato_DataTerminoAta_Jsonclick = "";
         edtContrato_DataPedidoReajuste_Jsonclick = "";
         edtContrato_DataAssinatura_Jsonclick = "";
         edtContrato_DataPublicacaoDOU_Jsonclick = "";
         cmbavUnidadedemedicao_Jsonclick = "";
         cmbavUnidadedemedicao.Enabled = 1;
         edtContrato_LmtFtr_Jsonclick = "";
         edtContrato_IndiceDivergencia_Jsonclick = "";
         edtContrato_Identificacao_Jsonclick = "";
         cmbContrato_CalculoDivergencia_Jsonclick = "";
         edtContrato_DiasPagto_Jsonclick = "";
         edtContrato_PrdFtrFim_Jsonclick = "";
         edtContrato_PrdFtrIni_Jsonclick = "";
         cmbContrato_PrdFtrCada_Jsonclick = "";
         edtContrato_ValorUnidadeContratacao_Jsonclick = "";
         edtContrato_Quantidade_Jsonclick = "";
         cmbContrato_UnidadeContratacao_Jsonclick = "";
         edtContrato_Valor_Jsonclick = "";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_PrepostoNom_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtndelete_Visible = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnupdate_Visible = 1;
         lblTextblockcontrato_prepostonom_Caption = "Preposto";
         edtContrato_Numero_Link = "";
         edtContrato_PrepostoNom_Link = "";
         edtContratada_PessoaNom_Link = "";
         chkContrato_Ativo.Caption = "";
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13672',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14672',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15672',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         scmdbuf = "";
         H00672_A39Contratada_Codigo = new int[1] ;
         H00672_A1013Contrato_PrepostoCod = new int[1] ;
         H00672_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00672_A453Contrato_IndiceDivergencia = new decimal[1] ;
         H00672_A452Contrato_CalculoDivergencia = new String[] {""} ;
         H00672_A90Contrato_RegrasPagto = new String[] {""} ;
         H00672_A91Contrato_DiasPagto = new short[1] ;
         H00672_A2086Contrato_LmtFtr = new decimal[1] ;
         H00672_n2086Contrato_LmtFtr = new bool[] {false} ;
         H00672_A1358Contrato_PrdFtrFim = new short[1] ;
         H00672_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         H00672_A1357Contrato_PrdFtrIni = new short[1] ;
         H00672_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         H00672_A1354Contrato_PrdFtrCada = new String[] {""} ;
         H00672_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         H00672_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H00672_A81Contrato_Quantidade = new int[1] ;
         H00672_A115Contrato_UnidadeContratacao = new short[1] ;
         H00672_A89Contrato_Valor = new decimal[1] ;
         H00672_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         H00672_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         H00672_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         H00672_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         H00672_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         H00672_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00672_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00672_A80Contrato_Objeto = new String[] {""} ;
         H00672_A79Contrato_Ano = new short[1] ;
         H00672_A78Contrato_NumeroAta = new String[] {""} ;
         H00672_n78Contrato_NumeroAta = new bool[] {false} ;
         H00672_A92Contrato_Ativo = new bool[] {false} ;
         H00672_A77Contrato_Numero = new String[] {""} ;
         A452Contrato_CalculoDivergencia = "";
         A90Contrato_RegrasPagto = "";
         A1354Contrato_PrdFtrCada = "";
         A88Contrato_DataFimAdaptacao = DateTime.MinValue;
         A87Contrato_DataTerminoAta = DateTime.MinValue;
         A86Contrato_DataPedidoReajuste = DateTime.MinValue;
         A85Contrato_DataAssinatura = DateTime.MinValue;
         A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A80Contrato_Objeto = "";
         A78Contrato_NumeroAta = "";
         A77Contrato_Numero = "";
         H00673_A40Contratada_PessoaCod = new int[1] ;
         H00673_A516Contratada_TipoFabrica = new String[] {""} ;
         A516Contratada_TipoFabrica = "";
         H00674_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00674_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00674_A41Contratada_PessoaNom = new String[] {""} ;
         H00674_n41Contratada_PessoaNom = new bool[] {false} ;
         A42Contratada_PessoaCNPJ = "";
         A41Contratada_PessoaNom = "";
         H00675_A1016Contrato_PrepostoPesCod = new int[1] ;
         H00675_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         H00676_A1015Contrato_PrepostoNom = new String[] {""} ;
         H00676_n1015Contrato_PrepostoNom = new bool[] {false} ;
         A1015Contrato_PrepostoNom = "";
         A2096Contrato_Identificacao = "";
         H00679_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H00679_n843Contrato_DataFimTA = new bool[] {false} ;
         A843Contrato_DataFimTA = DateTime.MinValue;
         H006712_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         H006712_n842Contrato_DataInicioTA = new bool[] {false} ;
         A842Contrato_DataInicioTA = DateTime.MinValue;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H006717_A40Contratada_PessoaCod = new int[1] ;
         H006717_A1016Contrato_PrepostoPesCod = new int[1] ;
         H006717_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         H006717_A74Contrato_Codigo = new int[1] ;
         H006717_n74Contrato_Codigo = new bool[] {false} ;
         H006717_A39Contratada_Codigo = new int[1] ;
         H006717_A1013Contrato_PrepostoCod = new int[1] ;
         H006717_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H006717_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         H006717_A516Contratada_TipoFabrica = new String[] {""} ;
         H006717_A453Contrato_IndiceDivergencia = new decimal[1] ;
         H006717_A452Contrato_CalculoDivergencia = new String[] {""} ;
         H006717_A90Contrato_RegrasPagto = new String[] {""} ;
         H006717_A91Contrato_DiasPagto = new short[1] ;
         H006717_A2086Contrato_LmtFtr = new decimal[1] ;
         H006717_n2086Contrato_LmtFtr = new bool[] {false} ;
         H006717_A1358Contrato_PrdFtrFim = new short[1] ;
         H006717_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         H006717_A1357Contrato_PrdFtrIni = new short[1] ;
         H006717_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         H006717_A1354Contrato_PrdFtrCada = new String[] {""} ;
         H006717_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         H006717_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H006717_A81Contrato_Quantidade = new int[1] ;
         H006717_A115Contrato_UnidadeContratacao = new short[1] ;
         H006717_A89Contrato_Valor = new decimal[1] ;
         H006717_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         H006717_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         H006717_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         H006717_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         H006717_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         H006717_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H006717_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H006717_A80Contrato_Objeto = new String[] {""} ;
         H006717_A79Contrato_Ano = new short[1] ;
         H006717_A78Contrato_NumeroAta = new String[] {""} ;
         H006717_n78Contrato_NumeroAta = new bool[] {false} ;
         H006717_A1015Contrato_PrepostoNom = new String[] {""} ;
         H006717_n1015Contrato_PrepostoNom = new bool[] {false} ;
         H006717_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006717_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006717_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H006717_n843Contrato_DataFimTA = new bool[] {false} ;
         H006717_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         H006717_n842Contrato_DataInicioTA = new bool[] {false} ;
         H006717_A41Contratada_PessoaNom = new String[] {""} ;
         H006717_n41Contratada_PessoaNom = new bool[] {false} ;
         H006717_A92Contrato_Ativo = new bool[] {false} ;
         H006717_A77Contrato_Numero = new String[] {""} ;
         H006718_A39Contratada_Codigo = new int[1] ;
         H006718_A1013Contrato_PrepostoCod = new int[1] ;
         H006718_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H006718_A453Contrato_IndiceDivergencia = new decimal[1] ;
         H006718_A452Contrato_CalculoDivergencia = new String[] {""} ;
         H006718_A90Contrato_RegrasPagto = new String[] {""} ;
         H006718_A91Contrato_DiasPagto = new short[1] ;
         H006718_A2086Contrato_LmtFtr = new decimal[1] ;
         H006718_n2086Contrato_LmtFtr = new bool[] {false} ;
         H006718_A1358Contrato_PrdFtrFim = new short[1] ;
         H006718_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         H006718_A1357Contrato_PrdFtrIni = new short[1] ;
         H006718_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         H006718_A1354Contrato_PrdFtrCada = new String[] {""} ;
         H006718_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         H006718_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         H006718_A81Contrato_Quantidade = new int[1] ;
         H006718_A115Contrato_UnidadeContratacao = new short[1] ;
         H006718_A89Contrato_Valor = new decimal[1] ;
         H006718_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         H006718_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         H006718_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         H006718_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         H006718_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         H006718_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H006718_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H006718_A80Contrato_Objeto = new String[] {""} ;
         H006718_A79Contrato_Ano = new short[1] ;
         H006718_A78Contrato_NumeroAta = new String[] {""} ;
         H006718_n78Contrato_NumeroAta = new bool[] {false} ;
         H006718_A92Contrato_Ativo = new bool[] {false} ;
         H006718_A77Contrato_Numero = new String[] {""} ;
         H006719_A40Contratada_PessoaCod = new int[1] ;
         H006719_A516Contratada_TipoFabrica = new String[] {""} ;
         H006720_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006720_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006720_A41Contratada_PessoaNom = new String[] {""} ;
         H006720_n41Contratada_PessoaNom = new bool[] {false} ;
         H006721_A1016Contrato_PrepostoPesCod = new int[1] ;
         H006721_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         H006722_A1015Contrato_PrepostoNom = new String[] {""} ;
         H006722_n1015Contrato_PrepostoNom = new bool[] {false} ;
         H006725_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H006725_n843Contrato_DataFimTA = new bool[] {false} ;
         H006728_A842Contrato_DataInicioTA = new DateTime[] {DateTime.MinValue} ;
         H006728_n842Contrato_DataInicioTA = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         lblTextblockcontrato_prepostonom_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         lblTextblockcontrato_objeto_Jsonclick = "";
         lblTextblockcontrato_valor_Jsonclick = "";
         lblTextblockcontrato_unidadecontratacao_Jsonclick = "";
         lblTextblockcontrato_quantidade_Jsonclick = "";
         lblTextblockcontrato_valorunidadecontratacao_Jsonclick = "";
         lblTextblockcontrato_prdftrcada_Jsonclick = "";
         lblTextblockcontrato_prdftrini_Jsonclick = "";
         lblTextblockcontrato_prdftrfim_Jsonclick = "";
         lblTextblockcontrato_lmtftr_Jsonclick = "";
         lblTextblockcontrato_diaspagto_Jsonclick = "";
         lblTextblockcontrato_regraspagto_Jsonclick = "";
         lblTextblockcontrato_calculodivergencia_Jsonclick = "";
         lblTextblockcontrato_indicedivergencia_Jsonclick = "";
         lblTextblockcontrato_ativo_Jsonclick = "";
         lblTextblockcontrato_identificacao_Jsonclick = "";
         lblContrato_indicedivergencia_righttext_Jsonclick = "";
         lblTextblockcontrato_datavigenciainicio_Jsonclick = "";
         lblTextblockcontrato_datainiciota_Jsonclick = "";
         lblTextblockcontrato_datapublicacaodou_Jsonclick = "";
         lblTextblockcontrato_dataassinatura_Jsonclick = "";
         lblTextblockcontrato_datapedidoreajuste_Jsonclick = "";
         lblTextblockcontrato_dataterminoata_Jsonclick = "";
         lblTextblockcontrato_datafimadaptacao_Jsonclick = "";
         lblTextblockcontrato_datafimta_Jsonclick = "";
         lblTextblockcontrato_datavigenciatermino_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA74Contrato_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratogeneral__default(),
            new Object[][] {
                new Object[] {
               H00672_A39Contratada_Codigo, H00672_A1013Contrato_PrepostoCod, H00672_n1013Contrato_PrepostoCod, H00672_A453Contrato_IndiceDivergencia, H00672_A452Contrato_CalculoDivergencia, H00672_A90Contrato_RegrasPagto, H00672_A91Contrato_DiasPagto, H00672_A2086Contrato_LmtFtr, H00672_n2086Contrato_LmtFtr, H00672_A1358Contrato_PrdFtrFim,
               H00672_n1358Contrato_PrdFtrFim, H00672_A1357Contrato_PrdFtrIni, H00672_n1357Contrato_PrdFtrIni, H00672_A1354Contrato_PrdFtrCada, H00672_n1354Contrato_PrdFtrCada, H00672_A116Contrato_ValorUnidadeContratacao, H00672_A81Contrato_Quantidade, H00672_A115Contrato_UnidadeContratacao, H00672_A89Contrato_Valor, H00672_A88Contrato_DataFimAdaptacao,
               H00672_A87Contrato_DataTerminoAta, H00672_A86Contrato_DataPedidoReajuste, H00672_A85Contrato_DataAssinatura, H00672_A84Contrato_DataPublicacaoDOU, H00672_A83Contrato_DataVigenciaTermino, H00672_A82Contrato_DataVigenciaInicio, H00672_A80Contrato_Objeto, H00672_A79Contrato_Ano, H00672_A78Contrato_NumeroAta, H00672_n78Contrato_NumeroAta,
               H00672_A92Contrato_Ativo, H00672_A77Contrato_Numero
               }
               , new Object[] {
               H00673_A40Contratada_PessoaCod, H00673_A516Contratada_TipoFabrica
               }
               , new Object[] {
               H00674_A42Contratada_PessoaCNPJ, H00674_n42Contratada_PessoaCNPJ, H00674_A41Contratada_PessoaNom, H00674_n41Contratada_PessoaNom
               }
               , new Object[] {
               H00675_A1016Contrato_PrepostoPesCod, H00675_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               H00676_A1015Contrato_PrepostoNom, H00676_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               H00679_A843Contrato_DataFimTA, H00679_n843Contrato_DataFimTA
               }
               , new Object[] {
               H006712_A842Contrato_DataInicioTA, H006712_n842Contrato_DataInicioTA
               }
               , new Object[] {
               H006717_A40Contratada_PessoaCod, H006717_A1016Contrato_PrepostoPesCod, H006717_n1016Contrato_PrepostoPesCod, H006717_A74Contrato_Codigo, H006717_A39Contratada_Codigo, H006717_A1013Contrato_PrepostoCod, H006717_n1013Contrato_PrepostoCod, H006717_A2098ContratoCaixaEntrada_Codigo, H006717_A516Contratada_TipoFabrica, H006717_A453Contrato_IndiceDivergencia,
               H006717_A452Contrato_CalculoDivergencia, H006717_A90Contrato_RegrasPagto, H006717_A91Contrato_DiasPagto, H006717_A2086Contrato_LmtFtr, H006717_n2086Contrato_LmtFtr, H006717_A1358Contrato_PrdFtrFim, H006717_n1358Contrato_PrdFtrFim, H006717_A1357Contrato_PrdFtrIni, H006717_n1357Contrato_PrdFtrIni, H006717_A1354Contrato_PrdFtrCada,
               H006717_n1354Contrato_PrdFtrCada, H006717_A116Contrato_ValorUnidadeContratacao, H006717_A81Contrato_Quantidade, H006717_A115Contrato_UnidadeContratacao, H006717_A89Contrato_Valor, H006717_A88Contrato_DataFimAdaptacao, H006717_A87Contrato_DataTerminoAta, H006717_A86Contrato_DataPedidoReajuste, H006717_A85Contrato_DataAssinatura, H006717_A84Contrato_DataPublicacaoDOU,
               H006717_A83Contrato_DataVigenciaTermino, H006717_A82Contrato_DataVigenciaInicio, H006717_A80Contrato_Objeto, H006717_A79Contrato_Ano, H006717_A78Contrato_NumeroAta, H006717_n78Contrato_NumeroAta, H006717_A1015Contrato_PrepostoNom, H006717_n1015Contrato_PrepostoNom, H006717_A42Contratada_PessoaCNPJ, H006717_n42Contratada_PessoaCNPJ,
               H006717_A843Contrato_DataFimTA, H006717_n843Contrato_DataFimTA, H006717_A842Contrato_DataInicioTA, H006717_n842Contrato_DataInicioTA, H006717_A41Contratada_PessoaNom, H006717_n41Contratada_PessoaNom, H006717_A92Contrato_Ativo, H006717_A77Contrato_Numero
               }
               , new Object[] {
               H006718_A39Contratada_Codigo, H006718_A1013Contrato_PrepostoCod, H006718_n1013Contrato_PrepostoCod, H006718_A453Contrato_IndiceDivergencia, H006718_A452Contrato_CalculoDivergencia, H006718_A90Contrato_RegrasPagto, H006718_A91Contrato_DiasPagto, H006718_A2086Contrato_LmtFtr, H006718_n2086Contrato_LmtFtr, H006718_A1358Contrato_PrdFtrFim,
               H006718_n1358Contrato_PrdFtrFim, H006718_A1357Contrato_PrdFtrIni, H006718_n1357Contrato_PrdFtrIni, H006718_A1354Contrato_PrdFtrCada, H006718_n1354Contrato_PrdFtrCada, H006718_A116Contrato_ValorUnidadeContratacao, H006718_A81Contrato_Quantidade, H006718_A115Contrato_UnidadeContratacao, H006718_A89Contrato_Valor, H006718_A88Contrato_DataFimAdaptacao,
               H006718_A87Contrato_DataTerminoAta, H006718_A86Contrato_DataPedidoReajuste, H006718_A85Contrato_DataAssinatura, H006718_A84Contrato_DataPublicacaoDOU, H006718_A83Contrato_DataVigenciaTermino, H006718_A82Contrato_DataVigenciaInicio, H006718_A80Contrato_Objeto, H006718_A79Contrato_Ano, H006718_A78Contrato_NumeroAta, H006718_n78Contrato_NumeroAta,
               H006718_A92Contrato_Ativo, H006718_A77Contrato_Numero
               }
               , new Object[] {
               H006719_A40Contratada_PessoaCod, H006719_A516Contratada_TipoFabrica
               }
               , new Object[] {
               H006720_A42Contratada_PessoaCNPJ, H006720_n42Contratada_PessoaCNPJ, H006720_A41Contratada_PessoaNom, H006720_n41Contratada_PessoaNom
               }
               , new Object[] {
               H006721_A1016Contrato_PrepostoPesCod, H006721_n1016Contrato_PrepostoPesCod
               }
               , new Object[] {
               H006722_A1015Contrato_PrepostoNom, H006722_n1015Contrato_PrepostoNom
               }
               , new Object[] {
               H006725_A843Contrato_DataFimTA, H006725_n843Contrato_DataFimTA
               }
               , new Object[] {
               H006728_A842Contrato_DataInicioTA, H006728_n842Contrato_DataInicioTA
               }
            }
         );
         AV16Pgmname = "ContratoGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "ContratoGeneral";
         context.Gx_err = 0;
         cmbavUnidadedemedicao.Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A91Contrato_DiasPagto ;
      private short A1358Contrato_PrdFtrFim ;
      private short A1357Contrato_PrdFtrIni ;
      private short A115Contrato_UnidadeContratacao ;
      private short A79Contrato_Ano ;
      private short AV13UnidadeDeMedicao ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A74Contrato_Codigo ;
      private int wcpOA74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A81Contrato_Quantidade ;
      private int A40Contratada_PessoaCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int AV12Contratada_Codigo ;
      private int edtContrato_Codigo_Visible ;
      private int A2098ContratoCaixaEntrada_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Contrato_Codigo ;
      private int idxLst ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal A2086Contrato_LmtFtr ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A89Contrato_Valor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String cmbavUnidadedemedicao_Internalname ;
      private String scmdbuf ;
      private String A452Contrato_CalculoDivergencia ;
      private String A1354Contrato_PrdFtrCada ;
      private String A78Contrato_NumeroAta ;
      private String A77Contrato_Numero ;
      private String A516Contratada_TipoFabrica ;
      private String A41Contratada_PessoaNom ;
      private String A1015Contrato_PrepostoNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkContrato_Ativo_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContrato_PrepostoNom_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String edtContrato_NumeroAta_Internalname ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Objeto_Internalname ;
      private String edtContrato_DataVigenciaInicio_Internalname ;
      private String edtContrato_DataVigenciaTermino_Internalname ;
      private String edtContrato_DataInicioTA_Internalname ;
      private String edtContrato_DataFimTA_Internalname ;
      private String edtContrato_DataPublicacaoDOU_Internalname ;
      private String edtContrato_DataAssinatura_Internalname ;
      private String edtContrato_DataPedidoReajuste_Internalname ;
      private String edtContrato_DataTerminoAta_Internalname ;
      private String edtContrato_DataFimAdaptacao_Internalname ;
      private String edtContrato_Valor_Internalname ;
      private String cmbContrato_UnidadeContratacao_Internalname ;
      private String edtContrato_Quantidade_Internalname ;
      private String edtContrato_ValorUnidadeContratacao_Internalname ;
      private String cmbContrato_PrdFtrCada_Internalname ;
      private String edtContrato_PrdFtrIni_Internalname ;
      private String edtContrato_PrdFtrFim_Internalname ;
      private String edtContrato_LmtFtr_Internalname ;
      private String edtContrato_DiasPagto_Internalname ;
      private String edtContrato_RegrasPagto_Internalname ;
      private String cmbContrato_CalculoDivergencia_Internalname ;
      private String edtContrato_IndiceDivergencia_Internalname ;
      private String edtContrato_Identificacao_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContratada_PessoaNom_Link ;
      private String edtContrato_PrepostoNom_Link ;
      private String edtContrato_Numero_Link ;
      private String lblTextblockcontrato_prepostonom_Caption ;
      private String lblTextblockcontrato_prepostonom_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String lblTextblockcontrato_prepostonom_Jsonclick ;
      private String edtContrato_PrepostoNom_Jsonclick ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Jsonclick ;
      private String lblTextblockcontrato_objeto_Internalname ;
      private String lblTextblockcontrato_objeto_Jsonclick ;
      private String lblTextblockcontrato_valor_Internalname ;
      private String lblTextblockcontrato_valor_Jsonclick ;
      private String edtContrato_Valor_Jsonclick ;
      private String lblTextblockcontrato_unidadecontratacao_Internalname ;
      private String lblTextblockcontrato_unidadecontratacao_Jsonclick ;
      private String cmbContrato_UnidadeContratacao_Jsonclick ;
      private String lblTextblockcontrato_quantidade_Internalname ;
      private String lblTextblockcontrato_quantidade_Jsonclick ;
      private String edtContrato_Quantidade_Jsonclick ;
      private String lblTextblockcontrato_valorunidadecontratacao_Internalname ;
      private String lblTextblockcontrato_valorunidadecontratacao_Jsonclick ;
      private String edtContrato_ValorUnidadeContratacao_Jsonclick ;
      private String lblTextblockcontrato_prdftrcada_Internalname ;
      private String lblTextblockcontrato_prdftrcada_Jsonclick ;
      private String cmbContrato_PrdFtrCada_Jsonclick ;
      private String lblTextblockcontrato_prdftrini_Internalname ;
      private String lblTextblockcontrato_prdftrini_Jsonclick ;
      private String edtContrato_PrdFtrIni_Jsonclick ;
      private String lblTextblockcontrato_prdftrfim_Internalname ;
      private String lblTextblockcontrato_prdftrfim_Jsonclick ;
      private String edtContrato_PrdFtrFim_Jsonclick ;
      private String lblTextblockcontrato_lmtftr_Internalname ;
      private String lblTextblockcontrato_lmtftr_Jsonclick ;
      private String lblTextblockcontrato_diaspagto_Internalname ;
      private String lblTextblockcontrato_diaspagto_Jsonclick ;
      private String edtContrato_DiasPagto_Jsonclick ;
      private String lblTextblockcontrato_regraspagto_Internalname ;
      private String lblTextblockcontrato_regraspagto_Jsonclick ;
      private String lblTextblockcontrato_calculodivergencia_Internalname ;
      private String lblTextblockcontrato_calculodivergencia_Jsonclick ;
      private String cmbContrato_CalculoDivergencia_Jsonclick ;
      private String lblTextblockcontrato_indicedivergencia_Internalname ;
      private String lblTextblockcontrato_indicedivergencia_Jsonclick ;
      private String lblTextblockcontrato_ativo_Internalname ;
      private String lblTextblockcontrato_ativo_Jsonclick ;
      private String lblTextblockcontrato_identificacao_Internalname ;
      private String lblTextblockcontrato_identificacao_Jsonclick ;
      private String edtContrato_Identificacao_Jsonclick ;
      private String tblTablemergedcontrato_indicedivergencia_Internalname ;
      private String edtContrato_IndiceDivergencia_Jsonclick ;
      private String lblContrato_indicedivergencia_righttext_Internalname ;
      private String lblContrato_indicedivergencia_righttext_Jsonclick ;
      private String tblTablemergedcontrato_lmtftr_Internalname ;
      private String edtContrato_LmtFtr_Jsonclick ;
      private String cmbavUnidadedemedicao_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontrato_datavigenciainicio_Internalname ;
      private String lblTextblockcontrato_datavigenciainicio_Jsonclick ;
      private String lblTextblockcontrato_datainiciota_Internalname ;
      private String lblTextblockcontrato_datainiciota_Jsonclick ;
      private String lblTextblockcontrato_datapublicacaodou_Internalname ;
      private String lblTextblockcontrato_datapublicacaodou_Jsonclick ;
      private String edtContrato_DataPublicacaoDOU_Jsonclick ;
      private String lblTextblockcontrato_dataassinatura_Internalname ;
      private String lblTextblockcontrato_dataassinatura_Jsonclick ;
      private String edtContrato_DataAssinatura_Jsonclick ;
      private String lblTextblockcontrato_datapedidoreajuste_Internalname ;
      private String lblTextblockcontrato_datapedidoreajuste_Jsonclick ;
      private String edtContrato_DataPedidoReajuste_Jsonclick ;
      private String lblTextblockcontrato_dataterminoata_Internalname ;
      private String lblTextblockcontrato_dataterminoata_Jsonclick ;
      private String edtContrato_DataTerminoAta_Jsonclick ;
      private String lblTextblockcontrato_datafimadaptacao_Internalname ;
      private String lblTextblockcontrato_datafimadaptacao_Jsonclick ;
      private String edtContrato_DataFimAdaptacao_Jsonclick ;
      private String tblTablemergedcontrato_datainiciota_Internalname ;
      private String edtContrato_DataInicioTA_Jsonclick ;
      private String lblTextblockcontrato_datafimta_Internalname ;
      private String lblTextblockcontrato_datafimta_Jsonclick ;
      private String edtContrato_DataFimTA_Jsonclick ;
      private String tblTablemergedcontrato_datavigenciainicio_Internalname ;
      private String edtContrato_DataVigenciaInicio_Jsonclick ;
      private String lblTextblockcontrato_datavigenciatermino_Internalname ;
      private String lblTextblockcontrato_datavigenciatermino_Jsonclick ;
      private String edtContrato_DataVigenciaTermino_Jsonclick ;
      private String sCtrlA74Contrato_Codigo ;
      private DateTime A88Contrato_DataFimAdaptacao ;
      private DateTime A87Contrato_DataTerminoAta ;
      private DateTime A86Contrato_DataPedidoReajuste ;
      private DateTime A85Contrato_DataAssinatura ;
      private DateTime A84Contrato_DataPublicacaoDOU ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A842Contrato_DataInicioTA ;
      private bool entryPointCalled ;
      private bool n74Contrato_Codigo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n2086Contrato_LmtFtr ;
      private bool n1358Contrato_PrdFtrFim ;
      private bool n1357Contrato_PrdFtrIni ;
      private bool n1354Contrato_PrdFtrCada ;
      private bool n78Contrato_NumeroAta ;
      private bool A92Contrato_Ativo ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n843Contrato_DataFimTA ;
      private bool n842Contrato_DataInicioTA ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String A90Contrato_RegrasPagto ;
      private String A80Contrato_Objeto ;
      private String A42Contratada_PessoaCNPJ ;
      private String A2096Contrato_Identificacao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContrato_UnidadeContratacao ;
      private GXCombobox cmbContrato_PrdFtrCada ;
      private GXCombobox cmbavUnidadedemedicao ;
      private GXCombobox cmbContrato_CalculoDivergencia ;
      private GXCheckbox chkContrato_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00672_A39Contratada_Codigo ;
      private int[] H00672_A1013Contrato_PrepostoCod ;
      private bool[] H00672_n1013Contrato_PrepostoCod ;
      private decimal[] H00672_A453Contrato_IndiceDivergencia ;
      private String[] H00672_A452Contrato_CalculoDivergencia ;
      private String[] H00672_A90Contrato_RegrasPagto ;
      private short[] H00672_A91Contrato_DiasPagto ;
      private decimal[] H00672_A2086Contrato_LmtFtr ;
      private bool[] H00672_n2086Contrato_LmtFtr ;
      private short[] H00672_A1358Contrato_PrdFtrFim ;
      private bool[] H00672_n1358Contrato_PrdFtrFim ;
      private short[] H00672_A1357Contrato_PrdFtrIni ;
      private bool[] H00672_n1357Contrato_PrdFtrIni ;
      private String[] H00672_A1354Contrato_PrdFtrCada ;
      private bool[] H00672_n1354Contrato_PrdFtrCada ;
      private decimal[] H00672_A116Contrato_ValorUnidadeContratacao ;
      private int[] H00672_A81Contrato_Quantidade ;
      private short[] H00672_A115Contrato_UnidadeContratacao ;
      private decimal[] H00672_A89Contrato_Valor ;
      private DateTime[] H00672_A88Contrato_DataFimAdaptacao ;
      private DateTime[] H00672_A87Contrato_DataTerminoAta ;
      private DateTime[] H00672_A86Contrato_DataPedidoReajuste ;
      private DateTime[] H00672_A85Contrato_DataAssinatura ;
      private DateTime[] H00672_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] H00672_A83Contrato_DataVigenciaTermino ;
      private DateTime[] H00672_A82Contrato_DataVigenciaInicio ;
      private String[] H00672_A80Contrato_Objeto ;
      private short[] H00672_A79Contrato_Ano ;
      private String[] H00672_A78Contrato_NumeroAta ;
      private bool[] H00672_n78Contrato_NumeroAta ;
      private bool[] H00672_A92Contrato_Ativo ;
      private String[] H00672_A77Contrato_Numero ;
      private int[] H00673_A40Contratada_PessoaCod ;
      private String[] H00673_A516Contratada_TipoFabrica ;
      private String[] H00674_A42Contratada_PessoaCNPJ ;
      private bool[] H00674_n42Contratada_PessoaCNPJ ;
      private String[] H00674_A41Contratada_PessoaNom ;
      private bool[] H00674_n41Contratada_PessoaNom ;
      private int[] H00675_A1016Contrato_PrepostoPesCod ;
      private bool[] H00675_n1016Contrato_PrepostoPesCod ;
      private String[] H00676_A1015Contrato_PrepostoNom ;
      private bool[] H00676_n1015Contrato_PrepostoNom ;
      private DateTime[] H00679_A843Contrato_DataFimTA ;
      private bool[] H00679_n843Contrato_DataFimTA ;
      private DateTime[] H006712_A842Contrato_DataInicioTA ;
      private bool[] H006712_n842Contrato_DataInicioTA ;
      private int[] H006717_A40Contratada_PessoaCod ;
      private int[] H006717_A1016Contrato_PrepostoPesCod ;
      private bool[] H006717_n1016Contrato_PrepostoPesCod ;
      private int[] H006717_A74Contrato_Codigo ;
      private bool[] H006717_n74Contrato_Codigo ;
      private int[] H006717_A39Contratada_Codigo ;
      private int[] H006717_A1013Contrato_PrepostoCod ;
      private bool[] H006717_n1013Contrato_PrepostoCod ;
      private int[] H006717_A2098ContratoCaixaEntrada_Codigo ;
      private String[] H006717_A516Contratada_TipoFabrica ;
      private decimal[] H006717_A453Contrato_IndiceDivergencia ;
      private String[] H006717_A452Contrato_CalculoDivergencia ;
      private String[] H006717_A90Contrato_RegrasPagto ;
      private short[] H006717_A91Contrato_DiasPagto ;
      private decimal[] H006717_A2086Contrato_LmtFtr ;
      private bool[] H006717_n2086Contrato_LmtFtr ;
      private short[] H006717_A1358Contrato_PrdFtrFim ;
      private bool[] H006717_n1358Contrato_PrdFtrFim ;
      private short[] H006717_A1357Contrato_PrdFtrIni ;
      private bool[] H006717_n1357Contrato_PrdFtrIni ;
      private String[] H006717_A1354Contrato_PrdFtrCada ;
      private bool[] H006717_n1354Contrato_PrdFtrCada ;
      private decimal[] H006717_A116Contrato_ValorUnidadeContratacao ;
      private int[] H006717_A81Contrato_Quantidade ;
      private short[] H006717_A115Contrato_UnidadeContratacao ;
      private decimal[] H006717_A89Contrato_Valor ;
      private DateTime[] H006717_A88Contrato_DataFimAdaptacao ;
      private DateTime[] H006717_A87Contrato_DataTerminoAta ;
      private DateTime[] H006717_A86Contrato_DataPedidoReajuste ;
      private DateTime[] H006717_A85Contrato_DataAssinatura ;
      private DateTime[] H006717_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] H006717_A83Contrato_DataVigenciaTermino ;
      private DateTime[] H006717_A82Contrato_DataVigenciaInicio ;
      private String[] H006717_A80Contrato_Objeto ;
      private short[] H006717_A79Contrato_Ano ;
      private String[] H006717_A78Contrato_NumeroAta ;
      private bool[] H006717_n78Contrato_NumeroAta ;
      private String[] H006717_A1015Contrato_PrepostoNom ;
      private bool[] H006717_n1015Contrato_PrepostoNom ;
      private String[] H006717_A42Contratada_PessoaCNPJ ;
      private bool[] H006717_n42Contratada_PessoaCNPJ ;
      private DateTime[] H006717_A843Contrato_DataFimTA ;
      private bool[] H006717_n843Contrato_DataFimTA ;
      private DateTime[] H006717_A842Contrato_DataInicioTA ;
      private bool[] H006717_n842Contrato_DataInicioTA ;
      private String[] H006717_A41Contratada_PessoaNom ;
      private bool[] H006717_n41Contratada_PessoaNom ;
      private bool[] H006717_A92Contrato_Ativo ;
      private String[] H006717_A77Contrato_Numero ;
      private int[] H006718_A39Contratada_Codigo ;
      private int[] H006718_A1013Contrato_PrepostoCod ;
      private bool[] H006718_n1013Contrato_PrepostoCod ;
      private decimal[] H006718_A453Contrato_IndiceDivergencia ;
      private String[] H006718_A452Contrato_CalculoDivergencia ;
      private String[] H006718_A90Contrato_RegrasPagto ;
      private short[] H006718_A91Contrato_DiasPagto ;
      private decimal[] H006718_A2086Contrato_LmtFtr ;
      private bool[] H006718_n2086Contrato_LmtFtr ;
      private short[] H006718_A1358Contrato_PrdFtrFim ;
      private bool[] H006718_n1358Contrato_PrdFtrFim ;
      private short[] H006718_A1357Contrato_PrdFtrIni ;
      private bool[] H006718_n1357Contrato_PrdFtrIni ;
      private String[] H006718_A1354Contrato_PrdFtrCada ;
      private bool[] H006718_n1354Contrato_PrdFtrCada ;
      private decimal[] H006718_A116Contrato_ValorUnidadeContratacao ;
      private int[] H006718_A81Contrato_Quantidade ;
      private short[] H006718_A115Contrato_UnidadeContratacao ;
      private decimal[] H006718_A89Contrato_Valor ;
      private DateTime[] H006718_A88Contrato_DataFimAdaptacao ;
      private DateTime[] H006718_A87Contrato_DataTerminoAta ;
      private DateTime[] H006718_A86Contrato_DataPedidoReajuste ;
      private DateTime[] H006718_A85Contrato_DataAssinatura ;
      private DateTime[] H006718_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] H006718_A83Contrato_DataVigenciaTermino ;
      private DateTime[] H006718_A82Contrato_DataVigenciaInicio ;
      private String[] H006718_A80Contrato_Objeto ;
      private short[] H006718_A79Contrato_Ano ;
      private String[] H006718_A78Contrato_NumeroAta ;
      private bool[] H006718_n78Contrato_NumeroAta ;
      private bool[] H006718_A92Contrato_Ativo ;
      private String[] H006718_A77Contrato_Numero ;
      private int[] H006719_A40Contratada_PessoaCod ;
      private String[] H006719_A516Contratada_TipoFabrica ;
      private String[] H006720_A42Contratada_PessoaCNPJ ;
      private bool[] H006720_n42Contratada_PessoaCNPJ ;
      private String[] H006720_A41Contratada_PessoaNom ;
      private bool[] H006720_n41Contratada_PessoaNom ;
      private int[] H006721_A1016Contrato_PrepostoPesCod ;
      private bool[] H006721_n1016Contrato_PrepostoPesCod ;
      private String[] H006722_A1015Contrato_PrepostoNom ;
      private bool[] H006722_n1015Contrato_PrepostoNom ;
      private DateTime[] H006725_A843Contrato_DataFimTA ;
      private bool[] H006725_n843Contrato_DataFimTA ;
      private DateTime[] H006728_A842Contrato_DataInicioTA ;
      private bool[] H006728_n842Contrato_DataInicioTA ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contratogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00672 ;
          prmH00672 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00673 ;
          prmH00673 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00674 ;
          prmH00674 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00675 ;
          prmH00675 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00676 ;
          prmH00676 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00679 ;
          prmH00679 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006712 ;
          prmH006712 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006717 ;
          prmH006717 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH006717 ;
          cmdBufferH006717=" SELECT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T5.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T1.[Contrato_Codigo], T2.[Contratada_Codigo], T2.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[ContratoCaixaEntrada_Codigo], T3.[Contratada_TipoFabrica], T2.[Contrato_IndiceDivergencia], T2.[Contrato_CalculoDivergencia], T2.[Contrato_RegrasPagto], T2.[Contrato_DiasPagto], T2.[Contrato_LmtFtr], T2.[Contrato_PrdFtrFim], T2.[Contrato_PrdFtrIni], T2.[Contrato_PrdFtrCada], T2.[Contrato_ValorUnidadeContratacao], T2.[Contrato_Quantidade], T2.[Contrato_UnidadeContratacao], T2.[Contrato_Valor], T2.[Contrato_DataFimAdaptacao], T2.[Contrato_DataTerminoAta], T2.[Contrato_DataPedidoReajuste], T2.[Contrato_DataAssinatura], T2.[Contrato_DataPublicacaoDOU], T2.[Contrato_DataVigenciaTermino], T2.[Contrato_DataVigenciaInicio], T2.[Contrato_Objeto], T2.[Contrato_Ano], T2.[Contrato_NumeroAta], T6.[Pessoa_Nome] AS Contrato_PrepostoNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, COALESCE( T7.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, COALESCE( T8.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Ativo], T2.[Contrato_Numero] FROM ((((((([ContratoCaixaEntrada] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T2.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataFim], "
          + " T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC4] ) T7 ON T7.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataInicio], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC4] ) T8 ON T8.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY T1.[Contrato_Codigo]" ;
          Object[] prmH006718 ;
          prmH006718 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006719 ;
          prmH006719 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006720 ;
          prmH006720 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006721 ;
          prmH006721 = new Object[] {
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006722 ;
          prmH006722 = new Object[] {
          new Object[] {"@Contrato_PrepostoPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006725 ;
          prmH006725 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006728 ;
          prmH006728 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00672", "SELECT [Contratada_Codigo], [Contrato_PrepostoCod] AS Contrato_PrepostoCod, [Contrato_IndiceDivergencia], [Contrato_CalculoDivergencia], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_LmtFtr], [Contrato_PrdFtrFim], [Contrato_PrdFtrIni], [Contrato_PrdFtrCada], [Contrato_ValorUnidadeContratacao], [Contrato_Quantidade], [Contrato_UnidadeContratacao], [Contrato_Valor], [Contrato_DataFimAdaptacao], [Contrato_DataTerminoAta], [Contrato_DataPedidoReajuste], [Contrato_DataAssinatura], [Contrato_DataPublicacaoDOU], [Contrato_DataVigenciaTermino], [Contrato_DataVigenciaInicio], [Contrato_Objeto], [Contrato_Ano], [Contrato_NumeroAta], [Contrato_Ativo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00672,1,0,true,false )
             ,new CursorDef("H00673", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod, [Contratada_TipoFabrica] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00673,1,0,true,false )
             ,new CursorDef("H00674", "SELECT [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_Nome] AS Contratada_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00674,1,0,true,false )
             ,new CursorDef("H00675", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00675,1,0,true,false )
             ,new CursorDef("H00676", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00676,1,0,true,false )
             ,new CursorDef("H00679", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC4] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00679,1,0,true,false )
             ,new CursorDef("H006712", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC4] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006712,1,0,true,false )
             ,new CursorDef("H006717", cmdBufferH006717,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006717,100,0,true,false )
             ,new CursorDef("H006718", "SELECT [Contratada_Codigo], [Contrato_PrepostoCod] AS Contrato_PrepostoCod, [Contrato_IndiceDivergencia], [Contrato_CalculoDivergencia], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_LmtFtr], [Contrato_PrdFtrFim], [Contrato_PrdFtrIni], [Contrato_PrdFtrCada], [Contrato_ValorUnidadeContratacao], [Contrato_Quantidade], [Contrato_UnidadeContratacao], [Contrato_Valor], [Contrato_DataFimAdaptacao], [Contrato_DataTerminoAta], [Contrato_DataPedidoReajuste], [Contrato_DataAssinatura], [Contrato_DataPublicacaoDOU], [Contrato_DataVigenciaTermino], [Contrato_DataVigenciaInicio], [Contrato_Objeto], [Contrato_Ano], [Contrato_NumeroAta], [Contrato_Ativo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006718,1,0,true,false )
             ,new CursorDef("H006719", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod, [Contratada_TipoFabrica] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006719,1,0,true,false )
             ,new CursorDef("H006720", "SELECT [Pessoa_Docto] AS Contratada_PessoaCNPJ, [Pessoa_Nome] AS Contratada_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006720,1,0,true,false )
             ,new CursorDef("H006721", "SELECT [Usuario_PessoaCod] AS Contrato_PrepostoPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contrato_PrepostoCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006721,1,0,true,false )
             ,new CursorDef("H006722", "SELECT [Pessoa_Nome] AS Contrato_PrepostoNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contrato_PrepostoPesCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006722,1,0,true,false )
             ,new CursorDef("H006725", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA FROM (SELECT T2.[ContratoTermoAditivo_DataFim], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC4] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006725,1,0,true,false )
             ,new CursorDef("H006728", "SELECT COALESCE( T1.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS Contrato_DataInicioTA FROM (SELECT T2.[ContratoTermoAditivo_DataInicio], T2.[Contrato_Codigo], T2.[ContratoTermoAditivo_Codigo], T3.[GXC4] AS GXC4 FROM ([ContratoTermoAditivo] T2 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC4, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T3 ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T2.[ContratoTermoAditivo_Codigo] = T3.[GXC4] ) T1 WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006728,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((short[]) buf[9])[0] = rslt.getShort(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((short[]) buf[11])[0] = rslt.getShort(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((short[]) buf[17])[0] = rslt.getShort(13) ;
                ((decimal[]) buf[18])[0] = rslt.getDecimal(14) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(16) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(17) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(19) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(20) ;
                ((DateTime[]) buf[25])[0] = rslt.getGXDate(21) ;
                ((String[]) buf[26])[0] = rslt.getLongVarchar(22) ;
                ((short[]) buf[27])[0] = rslt.getShort(23) ;
                ((String[]) buf[28])[0] = rslt.getString(24, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(24);
                ((bool[]) buf[30])[0] = rslt.getBool(25) ;
                ((String[]) buf[31])[0] = rslt.getString(26, 20) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(8) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 1) ;
                ((String[]) buf[11])[0] = rslt.getLongVarchar(10) ;
                ((short[]) buf[12])[0] = rslt.getShort(11) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(12);
                ((short[]) buf[15])[0] = rslt.getShort(13) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((short[]) buf[17])[0] = rslt.getShort(14) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((String[]) buf[19])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(16) ;
                ((int[]) buf[22])[0] = rslt.getInt(17) ;
                ((short[]) buf[23])[0] = rslt.getShort(18) ;
                ((decimal[]) buf[24])[0] = rslt.getDecimal(19) ;
                ((DateTime[]) buf[25])[0] = rslt.getGXDate(20) ;
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(21) ;
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(22) ;
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(23) ;
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(24) ;
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(25) ;
                ((DateTime[]) buf[31])[0] = rslt.getGXDate(26) ;
                ((String[]) buf[32])[0] = rslt.getLongVarchar(27) ;
                ((short[]) buf[33])[0] = rslt.getShort(28) ;
                ((String[]) buf[34])[0] = rslt.getString(29, 10) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(29);
                ((String[]) buf[36])[0] = rslt.getString(30, 100) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(30);
                ((String[]) buf[38])[0] = rslt.getVarchar(31) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(31);
                ((DateTime[]) buf[40])[0] = rslt.getGXDate(32) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(32);
                ((DateTime[]) buf[42])[0] = rslt.getGXDate(33) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(33);
                ((String[]) buf[44])[0] = rslt.getString(34, 100) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(34);
                ((bool[]) buf[46])[0] = rslt.getBool(35) ;
                ((String[]) buf[47])[0] = rslt.getString(36, 20) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((short[]) buf[9])[0] = rslt.getShort(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((short[]) buf[11])[0] = rslt.getShort(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((short[]) buf[17])[0] = rslt.getShort(13) ;
                ((decimal[]) buf[18])[0] = rslt.getDecimal(14) ;
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(15) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(16) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(17) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(19) ;
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(20) ;
                ((DateTime[]) buf[25])[0] = rslt.getGXDate(21) ;
                ((String[]) buf[26])[0] = rslt.getLongVarchar(22) ;
                ((short[]) buf[27])[0] = rslt.getShort(23) ;
                ((String[]) buf[28])[0] = rslt.getString(24, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(24);
                ((bool[]) buf[30])[0] = rslt.getBool(25) ;
                ((String[]) buf[31])[0] = rslt.getString(26, 20) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
