/*
               File: GetPromptContagemResultadoIncidentesFilterData
        Description: Get Prompt Contagem Resultado Incidentes Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:7:38.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontagemresultadoincidentesfilterdata : GXProcedure
   {
      public getpromptcontagemresultadoincidentesfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontagemresultadoincidentesfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontagemresultadoincidentesfilterdata objgetpromptcontagemresultadoincidentesfilterdata;
         objgetpromptcontagemresultadoincidentesfilterdata = new getpromptcontagemresultadoincidentesfilterdata();
         objgetpromptcontagemresultadoincidentesfilterdata.AV22DDOName = aP0_DDOName;
         objgetpromptcontagemresultadoincidentesfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetpromptcontagemresultadoincidentesfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontagemresultadoincidentesfilterdata.AV26OptionsJson = "" ;
         objgetpromptcontagemresultadoincidentesfilterdata.AV29OptionsDescJson = "" ;
         objgetpromptcontagemresultadoincidentesfilterdata.AV31OptionIndexesJson = "" ;
         objgetpromptcontagemresultadoincidentesfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontagemresultadoincidentesfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontagemresultadoincidentesfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontagemresultadoincidentesfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_INCIDENTES_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADINCIDENTES_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_INCIDENTES_DEMANDANUM") == 0 )
         {
            /* Execute user subroutine: 'LOADINCIDENTES_DEMANDANUMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("PromptContagemResultadoIncidentesGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContagemResultadoIncidentesGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("PromptContagemResultadoIncidentesGridState"), "");
         }
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_CODIGO") == 0 )
            {
               AV10TFIncidentes_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFIncidentes_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DATA") == 0 )
            {
               AV12TFIncidentes_Data = context.localUtil.CToT( AV36GridStateFilterValue.gxTpr_Value, 2);
               AV13TFIncidentes_Data_To = context.localUtil.CToT( AV36GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DESCRICAO") == 0 )
            {
               AV14TFIncidentes_Descricao = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DESCRICAO_SEL") == 0 )
            {
               AV15TFIncidentes_Descricao_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DEMANDACOD") == 0 )
            {
               AV16TFIncidentes_DemandaCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV17TFIncidentes_DemandaCod_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DEMANDANUM") == 0 )
            {
               AV18TFIncidentes_DemandaNum = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DEMANDANUM_SEL") == 0 )
            {
               AV19TFIncidentes_DemandaNum_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 )
            {
               AV39Incidentes_Data1 = context.localUtil.CToT( AV37GridStateDynamicFilter.gxTpr_Value, 2);
               AV40Incidentes_Data_To1 = context.localUtil.CToT( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV41DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV42DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 )
               {
                  AV43Incidentes_Data2 = context.localUtil.CToT( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                  AV44Incidentes_Data_To2 = context.localUtil.CToT( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 )
                  {
                     AV47Incidentes_Data3 = context.localUtil.CToT( AV37GridStateDynamicFilter.gxTpr_Value, 2);
                     AV48Incidentes_Data_To3 = context.localUtil.CToT( AV37GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADINCIDENTES_DESCRICAOOPTIONS' Routine */
         AV14TFIncidentes_Descricao = AV20SearchTxt;
         AV15TFIncidentes_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39Incidentes_Data1 ,
                                              AV40Incidentes_Data_To1 ,
                                              AV41DynamicFiltersEnabled2 ,
                                              AV42DynamicFiltersSelector2 ,
                                              AV43Incidentes_Data2 ,
                                              AV44Incidentes_Data_To2 ,
                                              AV45DynamicFiltersEnabled3 ,
                                              AV46DynamicFiltersSelector3 ,
                                              AV47Incidentes_Data3 ,
                                              AV48Incidentes_Data_To3 ,
                                              AV10TFIncidentes_Codigo ,
                                              AV11TFIncidentes_Codigo_To ,
                                              AV12TFIncidentes_Data ,
                                              AV13TFIncidentes_Data_To ,
                                              AV15TFIncidentes_Descricao_Sel ,
                                              AV14TFIncidentes_Descricao ,
                                              AV16TFIncidentes_DemandaCod ,
                                              AV17TFIncidentes_DemandaCod_To ,
                                              AV19TFIncidentes_DemandaNum_Sel ,
                                              AV18TFIncidentes_DemandaNum ,
                                              A1242Incidentes_Data ,
                                              A1241Incidentes_Codigo ,
                                              A1243Incidentes_Descricao ,
                                              A1239Incidentes_DemandaCod ,
                                              A1240Incidentes_DemandaNum },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV14TFIncidentes_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFIncidentes_Descricao), "%", "");
         lV18TFIncidentes_DemandaNum = StringUtil.Concat( StringUtil.RTrim( AV18TFIncidentes_DemandaNum), "%", "");
         /* Using cursor P00QH2 */
         pr_default.execute(0, new Object[] {AV39Incidentes_Data1, AV40Incidentes_Data_To1, AV43Incidentes_Data2, AV44Incidentes_Data_To2, AV47Incidentes_Data3, AV48Incidentes_Data_To3, AV10TFIncidentes_Codigo, AV11TFIncidentes_Codigo_To, AV12TFIncidentes_Data, AV13TFIncidentes_Data_To, lV14TFIncidentes_Descricao, AV15TFIncidentes_Descricao_Sel, AV16TFIncidentes_DemandaCod, AV17TFIncidentes_DemandaCod_To, lV18TFIncidentes_DemandaNum, AV19TFIncidentes_DemandaNum_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQH2 = false;
            A1243Incidentes_Descricao = P00QH2_A1243Incidentes_Descricao[0];
            A1240Incidentes_DemandaNum = P00QH2_A1240Incidentes_DemandaNum[0];
            n1240Incidentes_DemandaNum = P00QH2_n1240Incidentes_DemandaNum[0];
            A1239Incidentes_DemandaCod = P00QH2_A1239Incidentes_DemandaCod[0];
            A1241Incidentes_Codigo = P00QH2_A1241Incidentes_Codigo[0];
            A1242Incidentes_Data = P00QH2_A1242Incidentes_Data[0];
            A1240Incidentes_DemandaNum = P00QH2_A1240Incidentes_DemandaNum[0];
            n1240Incidentes_DemandaNum = P00QH2_n1240Incidentes_DemandaNum[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00QH2_A1243Incidentes_Descricao[0], A1243Incidentes_Descricao) == 0 ) )
            {
               BRKQH2 = false;
               A1241Incidentes_Codigo = P00QH2_A1241Incidentes_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKQH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1243Incidentes_Descricao)) )
            {
               AV24Option = A1243Incidentes_Descricao;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQH2 )
            {
               BRKQH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADINCIDENTES_DEMANDANUMOPTIONS' Routine */
         AV18TFIncidentes_DemandaNum = AV20SearchTxt;
         AV19TFIncidentes_DemandaNum_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39Incidentes_Data1 ,
                                              AV40Incidentes_Data_To1 ,
                                              AV41DynamicFiltersEnabled2 ,
                                              AV42DynamicFiltersSelector2 ,
                                              AV43Incidentes_Data2 ,
                                              AV44Incidentes_Data_To2 ,
                                              AV45DynamicFiltersEnabled3 ,
                                              AV46DynamicFiltersSelector3 ,
                                              AV47Incidentes_Data3 ,
                                              AV48Incidentes_Data_To3 ,
                                              AV10TFIncidentes_Codigo ,
                                              AV11TFIncidentes_Codigo_To ,
                                              AV12TFIncidentes_Data ,
                                              AV13TFIncidentes_Data_To ,
                                              AV15TFIncidentes_Descricao_Sel ,
                                              AV14TFIncidentes_Descricao ,
                                              AV16TFIncidentes_DemandaCod ,
                                              AV17TFIncidentes_DemandaCod_To ,
                                              AV19TFIncidentes_DemandaNum_Sel ,
                                              AV18TFIncidentes_DemandaNum ,
                                              A1242Incidentes_Data ,
                                              A1241Incidentes_Codigo ,
                                              A1243Incidentes_Descricao ,
                                              A1239Incidentes_DemandaCod ,
                                              A1240Incidentes_DemandaNum },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV14TFIncidentes_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFIncidentes_Descricao), "%", "");
         lV18TFIncidentes_DemandaNum = StringUtil.Concat( StringUtil.RTrim( AV18TFIncidentes_DemandaNum), "%", "");
         /* Using cursor P00QH3 */
         pr_default.execute(1, new Object[] {AV39Incidentes_Data1, AV40Incidentes_Data_To1, AV43Incidentes_Data2, AV44Incidentes_Data_To2, AV47Incidentes_Data3, AV48Incidentes_Data_To3, AV10TFIncidentes_Codigo, AV11TFIncidentes_Codigo_To, AV12TFIncidentes_Data, AV13TFIncidentes_Data_To, lV14TFIncidentes_Descricao, AV15TFIncidentes_Descricao_Sel, AV16TFIncidentes_DemandaCod, AV17TFIncidentes_DemandaCod_To, lV18TFIncidentes_DemandaNum, AV19TFIncidentes_DemandaNum_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQH4 = false;
            A1240Incidentes_DemandaNum = P00QH3_A1240Incidentes_DemandaNum[0];
            n1240Incidentes_DemandaNum = P00QH3_n1240Incidentes_DemandaNum[0];
            A1239Incidentes_DemandaCod = P00QH3_A1239Incidentes_DemandaCod[0];
            A1243Incidentes_Descricao = P00QH3_A1243Incidentes_Descricao[0];
            A1241Incidentes_Codigo = P00QH3_A1241Incidentes_Codigo[0];
            A1242Incidentes_Data = P00QH3_A1242Incidentes_Data[0];
            A1240Incidentes_DemandaNum = P00QH3_A1240Incidentes_DemandaNum[0];
            n1240Incidentes_DemandaNum = P00QH3_n1240Incidentes_DemandaNum[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00QH3_A1240Incidentes_DemandaNum[0], A1240Incidentes_DemandaNum) == 0 ) )
            {
               BRKQH4 = false;
               A1239Incidentes_DemandaCod = P00QH3_A1239Incidentes_DemandaCod[0];
               A1241Incidentes_Codigo = P00QH3_A1241Incidentes_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKQH4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1240Incidentes_DemandaNum)) )
            {
               AV24Option = A1240Incidentes_DemandaNum;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQH4 )
            {
               BRKQH4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFIncidentes_Data = (DateTime)(DateTime.MinValue);
         AV13TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
         AV14TFIncidentes_Descricao = "";
         AV15TFIncidentes_Descricao_Sel = "";
         AV18TFIncidentes_DemandaNum = "";
         AV19TFIncidentes_DemandaNum_Sel = "";
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV39Incidentes_Data1 = (DateTime)(DateTime.MinValue);
         AV40Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
         AV42DynamicFiltersSelector2 = "";
         AV43Incidentes_Data2 = (DateTime)(DateTime.MinValue);
         AV44Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
         AV46DynamicFiltersSelector3 = "";
         AV47Incidentes_Data3 = (DateTime)(DateTime.MinValue);
         AV48Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         lV14TFIncidentes_Descricao = "";
         lV18TFIncidentes_DemandaNum = "";
         A1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         A1243Incidentes_Descricao = "";
         A1240Incidentes_DemandaNum = "";
         P00QH2_A1243Incidentes_Descricao = new String[] {""} ;
         P00QH2_A1240Incidentes_DemandaNum = new String[] {""} ;
         P00QH2_n1240Incidentes_DemandaNum = new bool[] {false} ;
         P00QH2_A1239Incidentes_DemandaCod = new int[1] ;
         P00QH2_A1241Incidentes_Codigo = new int[1] ;
         P00QH2_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         AV24Option = "";
         P00QH3_A1240Incidentes_DemandaNum = new String[] {""} ;
         P00QH3_n1240Incidentes_DemandaNum = new bool[] {false} ;
         P00QH3_A1239Incidentes_DemandaCod = new int[1] ;
         P00QH3_A1243Incidentes_Descricao = new String[] {""} ;
         P00QH3_A1241Incidentes_Codigo = new int[1] ;
         P00QH3_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontagemresultadoincidentesfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QH2_A1243Incidentes_Descricao, P00QH2_A1240Incidentes_DemandaNum, P00QH2_n1240Incidentes_DemandaNum, P00QH2_A1239Incidentes_DemandaCod, P00QH2_A1241Incidentes_Codigo, P00QH2_A1242Incidentes_Data
               }
               , new Object[] {
               P00QH3_A1240Incidentes_DemandaNum, P00QH3_n1240Incidentes_DemandaNum, P00QH3_A1239Incidentes_DemandaCod, P00QH3_A1243Incidentes_Descricao, P00QH3_A1241Incidentes_Codigo, P00QH3_A1242Incidentes_Data
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV51GXV1 ;
      private int AV10TFIncidentes_Codigo ;
      private int AV11TFIncidentes_Codigo_To ;
      private int AV16TFIncidentes_DemandaCod ;
      private int AV17TFIncidentes_DemandaCod_To ;
      private int A1241Incidentes_Codigo ;
      private int A1239Incidentes_DemandaCod ;
      private long AV32count ;
      private String scmdbuf ;
      private DateTime AV12TFIncidentes_Data ;
      private DateTime AV13TFIncidentes_Data_To ;
      private DateTime AV39Incidentes_Data1 ;
      private DateTime AV40Incidentes_Data_To1 ;
      private DateTime AV43Incidentes_Data2 ;
      private DateTime AV44Incidentes_Data_To2 ;
      private DateTime AV47Incidentes_Data3 ;
      private DateTime AV48Incidentes_Data_To3 ;
      private DateTime A1242Incidentes_Data ;
      private bool returnInSub ;
      private bool AV41DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool BRKQH2 ;
      private bool n1240Incidentes_DemandaNum ;
      private bool BRKQH4 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String A1243Incidentes_Descricao ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV14TFIncidentes_Descricao ;
      private String AV15TFIncidentes_Descricao_Sel ;
      private String AV18TFIncidentes_DemandaNum ;
      private String AV19TFIncidentes_DemandaNum_Sel ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV42DynamicFiltersSelector2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String lV14TFIncidentes_Descricao ;
      private String lV18TFIncidentes_DemandaNum ;
      private String A1240Incidentes_DemandaNum ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00QH2_A1243Incidentes_Descricao ;
      private String[] P00QH2_A1240Incidentes_DemandaNum ;
      private bool[] P00QH2_n1240Incidentes_DemandaNum ;
      private int[] P00QH2_A1239Incidentes_DemandaCod ;
      private int[] P00QH2_A1241Incidentes_Codigo ;
      private DateTime[] P00QH2_A1242Incidentes_Data ;
      private String[] P00QH3_A1240Incidentes_DemandaNum ;
      private bool[] P00QH3_n1240Incidentes_DemandaNum ;
      private int[] P00QH3_A1239Incidentes_DemandaCod ;
      private String[] P00QH3_A1243Incidentes_Descricao ;
      private int[] P00QH3_A1241Incidentes_Codigo ;
      private DateTime[] P00QH3_A1242Incidentes_Data ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getpromptcontagemresultadoincidentesfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QH2( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             DateTime AV39Incidentes_Data1 ,
                                             DateTime AV40Incidentes_Data_To1 ,
                                             bool AV41DynamicFiltersEnabled2 ,
                                             String AV42DynamicFiltersSelector2 ,
                                             DateTime AV43Incidentes_Data2 ,
                                             DateTime AV44Incidentes_Data_To2 ,
                                             bool AV45DynamicFiltersEnabled3 ,
                                             String AV46DynamicFiltersSelector3 ,
                                             DateTime AV47Incidentes_Data3 ,
                                             DateTime AV48Incidentes_Data_To3 ,
                                             int AV10TFIncidentes_Codigo ,
                                             int AV11TFIncidentes_Codigo_To ,
                                             DateTime AV12TFIncidentes_Data ,
                                             DateTime AV13TFIncidentes_Data_To ,
                                             String AV15TFIncidentes_Descricao_Sel ,
                                             String AV14TFIncidentes_Descricao ,
                                             int AV16TFIncidentes_DemandaCod ,
                                             int AV17TFIncidentes_DemandaCod_To ,
                                             String AV19TFIncidentes_DemandaNum_Sel ,
                                             String AV18TFIncidentes_DemandaNum ,
                                             DateTime A1242Incidentes_Data ,
                                             int A1241Incidentes_Codigo ,
                                             String A1243Incidentes_Descricao ,
                                             int A1239Incidentes_DemandaCod ,
                                             String A1240Incidentes_DemandaNum )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [16] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Incidentes_Descricao], T2.[ContagemResultado_Demanda] AS Incidentes_DemandaNum, T1.[Incidentes_DemandaCod] AS Incidentes_DemandaCod, T1.[Incidentes_Codigo], T1.[Incidentes_Data] FROM ([ContagemResultadoIncidentes] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Incidentes_DemandaCod])";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV39Incidentes_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV39Incidentes_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV39Incidentes_Data1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV40Incidentes_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV40Incidentes_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV40Incidentes_Data_To1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV41DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV43Incidentes_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV43Incidentes_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV43Incidentes_Data2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV41DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV44Incidentes_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV44Incidentes_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV44Incidentes_Data_To2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV47Incidentes_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV47Incidentes_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV47Incidentes_Data3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV48Incidentes_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV48Incidentes_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV48Incidentes_Data_To3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFIncidentes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] >= @AV10TFIncidentes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] >= @AV10TFIncidentes_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFIncidentes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] <= @AV11TFIncidentes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] <= @AV11TFIncidentes_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFIncidentes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV12TFIncidentes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV12TFIncidentes_Data)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFIncidentes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV13TFIncidentes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV13TFIncidentes_Data_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFIncidentes_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFIncidentes_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] like @lV14TFIncidentes_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] like @lV14TFIncidentes_Descricao)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFIncidentes_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] = @AV15TFIncidentes_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] = @AV15TFIncidentes_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV16TFIncidentes_DemandaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] >= @AV16TFIncidentes_DemandaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] >= @AV16TFIncidentes_DemandaCod)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV17TFIncidentes_DemandaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] <= @AV17TFIncidentes_DemandaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] <= @AV17TFIncidentes_DemandaCod_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFIncidentes_DemandaNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFIncidentes_DemandaNum)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV18TFIncidentes_DemandaNum)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV18TFIncidentes_DemandaNum)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFIncidentes_DemandaNum_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV19TFIncidentes_DemandaNum_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] = @AV19TFIncidentes_DemandaNum_Sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Incidentes_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00QH3( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             DateTime AV39Incidentes_Data1 ,
                                             DateTime AV40Incidentes_Data_To1 ,
                                             bool AV41DynamicFiltersEnabled2 ,
                                             String AV42DynamicFiltersSelector2 ,
                                             DateTime AV43Incidentes_Data2 ,
                                             DateTime AV44Incidentes_Data_To2 ,
                                             bool AV45DynamicFiltersEnabled3 ,
                                             String AV46DynamicFiltersSelector3 ,
                                             DateTime AV47Incidentes_Data3 ,
                                             DateTime AV48Incidentes_Data_To3 ,
                                             int AV10TFIncidentes_Codigo ,
                                             int AV11TFIncidentes_Codigo_To ,
                                             DateTime AV12TFIncidentes_Data ,
                                             DateTime AV13TFIncidentes_Data_To ,
                                             String AV15TFIncidentes_Descricao_Sel ,
                                             String AV14TFIncidentes_Descricao ,
                                             int AV16TFIncidentes_DemandaCod ,
                                             int AV17TFIncidentes_DemandaCod_To ,
                                             String AV19TFIncidentes_DemandaNum_Sel ,
                                             String AV18TFIncidentes_DemandaNum ,
                                             DateTime A1242Incidentes_Data ,
                                             int A1241Incidentes_Codigo ,
                                             String A1243Incidentes_Descricao ,
                                             int A1239Incidentes_DemandaCod ,
                                             String A1240Incidentes_DemandaNum )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_Demanda] AS Incidentes_DemandaNum, T1.[Incidentes_DemandaCod] AS Incidentes_DemandaCod, T1.[Incidentes_Descricao], T1.[Incidentes_Codigo], T1.[Incidentes_Data] FROM ([ContagemResultadoIncidentes] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Incidentes_DemandaCod])";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV39Incidentes_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV39Incidentes_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV39Incidentes_Data1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV40Incidentes_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV40Incidentes_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV40Incidentes_Data_To1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV41DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV43Incidentes_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV43Incidentes_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV43Incidentes_Data2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV41DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV44Incidentes_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV44Incidentes_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV44Incidentes_Data_To2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV47Incidentes_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV47Incidentes_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV47Incidentes_Data3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV45DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV48Incidentes_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV48Incidentes_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV48Incidentes_Data_To3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFIncidentes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] >= @AV10TFIncidentes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] >= @AV10TFIncidentes_Codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFIncidentes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] <= @AV11TFIncidentes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] <= @AV11TFIncidentes_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFIncidentes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV12TFIncidentes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV12TFIncidentes_Data)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFIncidentes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV13TFIncidentes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV13TFIncidentes_Data_To)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFIncidentes_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFIncidentes_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] like @lV14TFIncidentes_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] like @lV14TFIncidentes_Descricao)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFIncidentes_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] = @AV15TFIncidentes_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] = @AV15TFIncidentes_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV16TFIncidentes_DemandaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] >= @AV16TFIncidentes_DemandaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] >= @AV16TFIncidentes_DemandaCod)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV17TFIncidentes_DemandaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] <= @AV17TFIncidentes_DemandaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] <= @AV17TFIncidentes_DemandaCod_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFIncidentes_DemandaNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFIncidentes_DemandaNum)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV18TFIncidentes_DemandaNum)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV18TFIncidentes_DemandaNum)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFIncidentes_DemandaNum_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV19TFIncidentes_DemandaNum_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] = @AV19TFIncidentes_DemandaNum_Sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_Demanda]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QH2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] );
               case 1 :
                     return conditional_P00QH3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QH2 ;
          prmP00QH2 = new Object[] {
          new Object[] {"@AV39Incidentes_Data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40Incidentes_Data_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV43Incidentes_Data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV44Incidentes_Data_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV47Incidentes_Data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV48Incidentes_Data_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV10TFIncidentes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFIncidentes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFIncidentes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV13TFIncidentes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV14TFIncidentes_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFIncidentes_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV16TFIncidentes_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFIncidentes_DemandaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18TFIncidentes_DemandaNum",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV19TFIncidentes_DemandaNum_Sel",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP00QH3 ;
          prmP00QH3 = new Object[] {
          new Object[] {"@AV39Incidentes_Data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40Incidentes_Data_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV43Incidentes_Data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV44Incidentes_Data_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV47Incidentes_Data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV48Incidentes_Data_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV10TFIncidentes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFIncidentes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFIncidentes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV13TFIncidentes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV14TFIncidentes_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV15TFIncidentes_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV16TFIncidentes_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFIncidentes_DemandaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18TFIncidentes_DemandaNum",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV19TFIncidentes_DemandaNum_Sel",SqlDbType.VarChar,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QH2,100,0,true,false )
             ,new CursorDef("P00QH3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QH3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontagemresultadoincidentesfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontagemresultadoincidentesfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontagemresultadoincidentesfilterdata") )
          {
             return  ;
          }
          getpromptcontagemresultadoincidentesfilterdata worker = new getpromptcontagemresultadoincidentesfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
