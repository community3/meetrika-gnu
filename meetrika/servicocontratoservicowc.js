/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:13:53.99
*/
gx.evt.autoSkip = false;
gx.define('servicocontratoservicowc', true, function (CmpContext) {
   this.ServerClass =  "servicocontratoservicowc" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7Servico_Codigo=gx.fn.getIntegerValue("vSERVICO_CODIGO",'.') ;
      this.AV41Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV11GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV25DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV24DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
      this.A74Contrato_Codigo=gx.fn.getIntegerValue("CONTRATO_CODIGO",'.') ;
   };
   this.Valid_Contratada_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(69) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTRATADA_CODIGO", gx.fn.currentGridRowImpl(69));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM1","Visible", false );
      if ( this.AV16DynamicFiltersSelector1 == "CONTRATADA_PESSOANOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM1","Visible", true );
      }
   };
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM2","Visible", false );
      if ( this.AV19DynamicFiltersSelector2 == "CONTRATADA_PESSOANOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM2","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM3","Visible", false );
      if ( this.AV22DynamicFiltersSelector3 == "CONTRATADA_PESSOANOM" )
      {
         gx.fn.setCtrlProperty("vCONTRATADA_PESSOANOM3","Visible", true );
      }
   };
   this.s162_client=function()
   {
      this.s182_client();
      if ( this.AV14OrderedBy == 2 )
      {
         this.DDO_CONTRATADA_PESSOANOMContainer.SortedStatus =  (this.AV15OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s182_client=function()
   {
      this.DDO_CONTRATADA_PESSOANOMContainer.SortedStatus =  ""  ;
   };
   this.s202_client=function()
   {
      this.AV18DynamicFiltersEnabled2 =  false  ;
      this.AV19DynamicFiltersSelector2 =  "CONTRATADA_PESSOANOM"  ;
      this.AV20Contratada_PessoaNom2 =  ''  ;
      this.s122_client();
      this.AV21DynamicFiltersEnabled3 =  false  ;
      this.AV22DynamicFiltersSelector3 =  "CONTRATADA_PESSOANOM"  ;
      this.AV23Contratada_PessoaNom3 =  ''  ;
      this.s132_client();
   };
   this.e11fx2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12fx2_client=function()
   {
      this.executeServerEvent("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13fx2_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e14fx2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e15fx2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e16fx2_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e17fx2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e18fx2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e19fx2_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e20fx2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e21fx2_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e25fx2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e26fx2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,15,17,18,20,23,25,27,30,32,34,36,38,39,42,44,46,48,50,51,54,56,58,60,62,63,66,70,71,72,73,74,79,80,81,82,84];
   this.GXLastCtrlId =84;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",69,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"servicocontratoservicowc",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Display","vDISPLAY",70,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(155,71,"SERVICO_CODIGO","Serviço","","Servico_Codigo","int",0,"px",6,6,"right",null,[],155,"Servico_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(160,72,"CONTRATOSERVICOS_CODIGO","Código do Serviço","","ContratoServicos_Codigo","int",0,"px",6,6,"right",null,[],160,"ContratoServicos_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(39,73,"CONTRATADA_CODIGO","Contratada","","Contratada_Codigo","int",0,"px",6,6,"right",null,[],39,"Contratada_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(41,74,"CONTRATADA_PESSOANOM","","","Contratada_PessoaNom","char",0,"px",100,80,"left",null,[],41,"Contratada_PessoaNom",true,0,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 78, 17, "DVelop_WorkWithPlusUtilities", this.CmpContext + "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_CONTRATADA_PESSOANOMContainer = gx.uc.getNew(this, 83, 17, "BootstrapDropDownOptions", this.CmpContext + "DDO_CONTRATADA_PESSOANOMContainer", "Ddo_contratada_pessoanom");
   var DDO_CONTRATADA_PESSOANOMContainer = this.DDO_CONTRATADA_PESSOANOMContainer;
   DDO_CONTRATADA_PESSOANOMContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_CONTRATADA_PESSOANOMContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("DataListProc", "Datalistproc", "GetServicoContratoServicoWCFilterData", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTRATADA_PESSOANOMContainer.addV2CFunction('AV34DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTRATADA_PESSOANOMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV34DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV34DDO_TitleSettingsIcons); });
   DDO_CONTRATADA_PESSOANOMContainer.addV2CFunction('AV30Contratada_PessoaNomTitleFilterData', "vCONTRATADA_PESSOANOMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTRATADA_PESSOANOMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV30Contratada_PessoaNomTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTRATADA_PESSOANOMTITLEFILTERDATA",UC.ParentObject.AV30Contratada_PessoaNomTitleFilterData); });
   DDO_CONTRATADA_PESSOANOMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTRATADA_PESSOANOMContainer.setProp("Class", "Class", "", "char");
   DDO_CONTRATADA_PESSOANOMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTRATADA_PESSOANOMContainer.addEventHandler("OnOptionClicked", this.e12fx2_client);
   this.setUserControl(DDO_CONTRATADA_PESSOANOMContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 77, 17, "DVelop_DVPaginationBar", this.CmpContext + "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV36GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV36GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV36GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV37GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV37GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV37GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11fx2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[8]={fld:"TABLESEARCH",grid:0};
   GXValidFnc[11]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[15]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[17]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV14OrderedBy",gxold:"OV14OrderedBy",gxvar:"AV14OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV14OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[18]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV15OrderedDsc",gxold:"OV15OrderedDsc",gxvar:"AV15OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV15OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV15OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV15OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV15OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[20]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[23]={fld:"FILTERTEXTCONTRATADA_AREATRABALHOCOD", format:0,grid:0};
   GXValidFnc[25]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATADA_AREATRABALHOCOD",gxz:"ZV29Contratada_AreaTrabalhoCod",gxold:"OV29Contratada_AreaTrabalhoCod",gxvar:"AV29Contratada_AreaTrabalhoCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV29Contratada_AreaTrabalhoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV29Contratada_AreaTrabalhoCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vCONTRATADA_AREATRABALHOCOD",gx.O.AV29Contratada_AreaTrabalhoCod,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV29Contratada_AreaTrabalhoCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vCONTRATADA_AREATRABALHOCOD",'.')},nac:gx.falseFn};
   GXValidFnc[27]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[30]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[32]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV16DynamicFiltersSelector1",gxold:"OV16DynamicFiltersSelector1",gxvar:"AV16DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV16DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV16DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV16DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV16DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[34]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[36]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATADA_PESSOANOM1",gxz:"ZV17Contratada_PessoaNom1",gxold:"OV17Contratada_PessoaNom1",gxvar:"AV17Contratada_PessoaNom1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17Contratada_PessoaNom1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV17Contratada_PessoaNom1=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_PESSOANOM1",gx.O.AV17Contratada_PessoaNom1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17Contratada_PessoaNom1=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_PESSOANOM1")},nac:gx.falseFn};
   GXValidFnc[38]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[39]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[42]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[44]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV19DynamicFiltersSelector2",gxold:"OV19DynamicFiltersSelector2",gxvar:"AV19DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV19DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV19DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV19DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[46]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[48]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATADA_PESSOANOM2",gxz:"ZV20Contratada_PessoaNom2",gxold:"OV20Contratada_PessoaNom2",gxvar:"AV20Contratada_PessoaNom2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20Contratada_PessoaNom2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV20Contratada_PessoaNom2=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_PESSOANOM2",gx.O.AV20Contratada_PessoaNom2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20Contratada_PessoaNom2=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_PESSOANOM2")},nac:gx.falseFn};
   GXValidFnc[50]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[51]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[54]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[56]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV22DynamicFiltersSelector3",gxold:"OV22DynamicFiltersSelector3",gxvar:"AV22DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV22DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV22DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV22DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV22DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[58]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[60]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vCONTRATADA_PESSOANOM3",gxz:"ZV23Contratada_PessoaNom3",gxold:"OV23Contratada_PessoaNom3",gxvar:"AV23Contratada_PessoaNom3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23Contratada_PessoaNom3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23Contratada_PessoaNom3=Value},v2c:function(){gx.fn.setControlValue("vCONTRATADA_PESSOANOM3",gx.O.AV23Contratada_PessoaNom3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23Contratada_PessoaNom3=this.val()},val:function(){return gx.fn.getControlValue("vCONTRATADA_PESSOANOM3")},nac:gx.falseFn};
   GXValidFnc[62]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[63]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[66]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[70]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:69,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDISPLAY",gxz:"ZV26Display",gxold:"OV26Display",gxvar:"AV26Display",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV26Display=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV26Display=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDISPLAY",row || gx.fn.currentGridRowImpl(69),gx.O.AV26Display,gx.O.AV40Display_GXI)},c2v:function(){gx.O.AV40Display_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV26Display=this.val()},val:function(row){return gx.fn.getGridControlValue("vDISPLAY",row || gx.fn.currentGridRowImpl(69))},val_GXI:function(row){return gx.fn.getGridControlValue("vDISPLAY_GXI",row || gx.fn.currentGridRowImpl(69))}, gxvar_GXI:'AV40Display_GXI',nac:gx.falseFn};
   GXValidFnc[71]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:69,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"SERVICO_CODIGO",gxz:"Z155Servico_Codigo",gxold:"O155Servico_Codigo",gxvar:"A155Servico_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A155Servico_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z155Servico_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("SERVICO_CODIGO",row || gx.fn.currentGridRowImpl(69),gx.O.A155Servico_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A155Servico_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SERVICO_CODIGO",row || gx.fn.currentGridRowImpl(69),'.')},nac:gx.falseFn};
   GXValidFnc[72]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:69,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOS_CODIGO",gxz:"Z160ContratoServicos_Codigo",gxold:"O160ContratoServicos_Codigo",gxvar:"A160ContratoServicos_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A160ContratoServicos_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z160ContratoServicos_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATOSERVICOS_CODIGO",row || gx.fn.currentGridRowImpl(69),gx.O.A160ContratoServicos_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A160ContratoServicos_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATOSERVICOS_CODIGO",row || gx.fn.currentGridRowImpl(69),'.')},nac:gx.falseFn};
   GXValidFnc[73]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:69,gxgrid:this.GridContainer,fnc:this.Valid_Contratada_codigo,isvalid:null,rgrid:[],fld:"CONTRATADA_CODIGO",gxz:"Z39Contratada_Codigo",gxold:"O39Contratada_Codigo",gxvar:"A39Contratada_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A39Contratada_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z39Contratada_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTRATADA_CODIGO",row || gx.fn.currentGridRowImpl(69),gx.O.A39Contratada_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.A39Contratada_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTRATADA_CODIGO",row || gx.fn.currentGridRowImpl(69),'.')},nac:gx.falseFn};
   GXValidFnc[74]={lvl:2,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:69,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATADA_PESSOANOM",gxz:"Z41Contratada_PessoaNom",gxold:"O41Contratada_PessoaNom",gxvar:"A41Contratada_PessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A41Contratada_PessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z41Contratada_PessoaNom=Value},v2c:function(row){gx.fn.setGridControlValue("CONTRATADA_PESSOANOM",row || gx.fn.currentGridRowImpl(69),gx.O.A41Contratada_PessoaNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A41Contratada_PessoaNom=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTRATADA_PESSOANOM",row || gx.fn.currentGridRowImpl(69))},nac:gx.falseFn};
   GXValidFnc[79]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV18DynamicFiltersEnabled2",gxold:"OV18DynamicFiltersEnabled2",gxvar:"AV18DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV18DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[80]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV21DynamicFiltersEnabled3",gxold:"OV21DynamicFiltersEnabled3",gxvar:"AV21DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV21DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV21DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV21DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[81]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTRATADA_PESSOANOM",gxz:"ZV31TFContratada_PessoaNom",gxold:"OV31TFContratada_PessoaNom",gxvar:"AV31TFContratada_PessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV31TFContratada_PessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV31TFContratada_PessoaNom=Value},v2c:function(){gx.fn.setControlValue("vTFCONTRATADA_PESSOANOM",gx.O.AV31TFContratada_PessoaNom,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV31TFContratada_PessoaNom=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTRATADA_PESSOANOM")},nac:gx.falseFn};
   GXValidFnc[82]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTRATADA_PESSOANOM_SEL",gxz:"ZV32TFContratada_PessoaNom_Sel",gxold:"OV32TFContratada_PessoaNom_Sel",gxvar:"AV32TFContratada_PessoaNom_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV32TFContratada_PessoaNom_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV32TFContratada_PessoaNom_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFCONTRATADA_PESSOANOM_SEL",gx.O.AV32TFContratada_PessoaNom_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV32TFContratada_PessoaNom_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTRATADA_PESSOANOM_SEL")},nac:gx.falseFn};
   GXValidFnc[84]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE",gxz:"ZV33ddo_Contratada_PessoaNomTitleControlIdToReplace",gxold:"OV33ddo_Contratada_PessoaNomTitleControlIdToReplace",gxvar:"AV33ddo_Contratada_PessoaNomTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV33ddo_Contratada_PessoaNomTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV33ddo_Contratada_PessoaNomTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE",gx.O.AV33ddo_Contratada_PessoaNomTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV33ddo_Contratada_PessoaNomTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV14OrderedBy = 0 ;
   this.ZV14OrderedBy = 0 ;
   this.OV14OrderedBy = 0 ;
   this.AV15OrderedDsc = false ;
   this.ZV15OrderedDsc = false ;
   this.OV15OrderedDsc = false ;
   this.AV29Contratada_AreaTrabalhoCod = 0 ;
   this.ZV29Contratada_AreaTrabalhoCod = 0 ;
   this.OV29Contratada_AreaTrabalhoCod = 0 ;
   this.AV16DynamicFiltersSelector1 = "" ;
   this.ZV16DynamicFiltersSelector1 = "" ;
   this.OV16DynamicFiltersSelector1 = "" ;
   this.AV17Contratada_PessoaNom1 = "" ;
   this.ZV17Contratada_PessoaNom1 = "" ;
   this.OV17Contratada_PessoaNom1 = "" ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.ZV19DynamicFiltersSelector2 = "" ;
   this.OV19DynamicFiltersSelector2 = "" ;
   this.AV20Contratada_PessoaNom2 = "" ;
   this.ZV20Contratada_PessoaNom2 = "" ;
   this.OV20Contratada_PessoaNom2 = "" ;
   this.AV22DynamicFiltersSelector3 = "" ;
   this.ZV22DynamicFiltersSelector3 = "" ;
   this.OV22DynamicFiltersSelector3 = "" ;
   this.AV23Contratada_PessoaNom3 = "" ;
   this.ZV23Contratada_PessoaNom3 = "" ;
   this.OV23Contratada_PessoaNom3 = "" ;
   this.ZV26Display = "" ;
   this.OV26Display = "" ;
   this.Z155Servico_Codigo = 0 ;
   this.O155Servico_Codigo = 0 ;
   this.Z160ContratoServicos_Codigo = 0 ;
   this.O160ContratoServicos_Codigo = 0 ;
   this.Z39Contratada_Codigo = 0 ;
   this.O39Contratada_Codigo = 0 ;
   this.Z41Contratada_PessoaNom = "" ;
   this.O41Contratada_PessoaNom = "" ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.ZV18DynamicFiltersEnabled2 = false ;
   this.OV18DynamicFiltersEnabled2 = false ;
   this.AV21DynamicFiltersEnabled3 = false ;
   this.ZV21DynamicFiltersEnabled3 = false ;
   this.OV21DynamicFiltersEnabled3 = false ;
   this.AV31TFContratada_PessoaNom = "" ;
   this.ZV31TFContratada_PessoaNom = "" ;
   this.OV31TFContratada_PessoaNom = "" ;
   this.AV32TFContratada_PessoaNom_Sel = "" ;
   this.ZV32TFContratada_PessoaNom_Sel = "" ;
   this.OV32TFContratada_PessoaNom_Sel = "" ;
   this.AV33ddo_Contratada_PessoaNomTitleControlIdToReplace = "" ;
   this.ZV33ddo_Contratada_PessoaNomTitleControlIdToReplace = "" ;
   this.OV33ddo_Contratada_PessoaNomTitleControlIdToReplace = "" ;
   this.AV14OrderedBy = 0 ;
   this.AV15OrderedDsc = false ;
   this.AV29Contratada_AreaTrabalhoCod = 0 ;
   this.AV16DynamicFiltersSelector1 = "" ;
   this.AV17Contratada_PessoaNom1 = "" ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.AV20Contratada_PessoaNom2 = "" ;
   this.AV22DynamicFiltersSelector3 = "" ;
   this.AV23Contratada_PessoaNom3 = "" ;
   this.AV36GridCurrentPage = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.AV21DynamicFiltersEnabled3 = false ;
   this.AV31TFContratada_PessoaNom = "" ;
   this.AV32TFContratada_PessoaNom_Sel = "" ;
   this.AV34DDO_TitleSettingsIcons = {} ;
   this.AV33ddo_Contratada_PessoaNomTitleControlIdToReplace = "" ;
   this.AV7Servico_Codigo = 0 ;
   this.A52Contratada_AreaTrabalhoCod = 0 ;
   this.A74Contrato_Codigo = 0 ;
   this.A40Contratada_PessoaCod = 0 ;
   this.AV26Display = "" ;
   this.A155Servico_Codigo = 0 ;
   this.A160ContratoServicos_Codigo = 0 ;
   this.A39Contratada_Codigo = 0 ;
   this.A41Contratada_PessoaNom = "" ;
   this.AV41Pgmname = "" ;
   this.AV11GridState = {} ;
   this.AV25DynamicFiltersIgnoreFirst = false ;
   this.AV24DynamicFiltersRemoving = false ;
   this.Events = {"e11fx2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12fx2_client": ["DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED", true] ,"e13fx2_client": ["VORDEREDBY.CLICK", true] ,"e14fx2_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e15fx2_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e16fx2_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e17fx2_client": ["'ADDDYNAMICFILTERS1'", true] ,"e18fx2_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e19fx2_client": ["'ADDDYNAMICFILTERS2'", true] ,"e20fx2_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e21fx2_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e25fx2_client": ["ENTER", true] ,"e26fx2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV33ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV29Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''}],[{av:'AV30Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTRATADA_PESSOANOM","Title")',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'AV36GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV37GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'this.DDO_CONTRATADA_PESSOANOMContainer.ActiveEventKey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'this.DDO_CONTRATADA_PESSOANOMContainer.FilteredText_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'this.DDO_CONTRATADA_PESSOANOMContainer.SelectedValue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_CONTRATADA_PESSOANOMContainer.SortedStatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26Display',fld:'vDISPLAY',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDISPLAY","Tooltiptext")',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDISPLAY","Link")',ctrl:'vDISPLAY',prop:'Link'}]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV32TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV33ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM2","Visible")',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM1","Visible")',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vCONTRATADA_PESSOANOM3","Visible")',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'}]];
   this.setVCMap("AV7Servico_Codigo", "vSERVICO_CODIGO", 0, "int");
   this.setVCMap("AV41Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV11GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV25DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV24DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A74Contrato_Codigo", "CONTRATO_CODIGO", 0, "int");
   this.setVCMap("AV7Servico_Codigo", "vSERVICO_CODIGO", 0, "int");
   this.setVCMap("AV41Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV11GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV25DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV24DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A74Contrato_Codigo", "CONTRATO_CODIGO", 0, "int");
   GridContainer.addRefreshingVar(this.GXValidFnc[17]);
   GridContainer.addRefreshingVar(this.GXValidFnc[18]);
   GridContainer.addRefreshingVar(this.GXValidFnc[32]);
   GridContainer.addRefreshingVar(this.GXValidFnc[36]);
   GridContainer.addRefreshingVar(this.GXValidFnc[44]);
   GridContainer.addRefreshingVar(this.GXValidFnc[48]);
   GridContainer.addRefreshingVar(this.GXValidFnc[56]);
   GridContainer.addRefreshingVar(this.GXValidFnc[60]);
   GridContainer.addRefreshingVar(this.GXValidFnc[79]);
   GridContainer.addRefreshingVar(this.GXValidFnc[80]);
   GridContainer.addRefreshingVar(this.GXValidFnc[81]);
   GridContainer.addRefreshingVar(this.GXValidFnc[82]);
   GridContainer.addRefreshingVar({rfrVar:"AV7Servico_Codigo"});
   GridContainer.addRefreshingVar(this.GXValidFnc[84]);
   GridContainer.addRefreshingVar({rfrVar:"AV41Pgmname"});
   GridContainer.addRefreshingVar(this.GXValidFnc[25]);
   GridContainer.addRefreshingVar({rfrVar:"AV11GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV25DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV24DynamicFiltersRemoving"});
   GridContainer.addRefreshingVar({rfrVar:"A74Contrato_Codigo"});
   this.InitStandaloneVars( );
});
