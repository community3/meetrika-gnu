/*
               File: type_SdtSDT_RedmineTrackers
        Description: SDT_RedmineTrackers
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:5.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "trackers" )]
   [XmlType(TypeName =  "trackers" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineTrackers_tracker ))]
   [Serializable]
   public class SdtSDT_RedmineTrackers : GxUserType
   {
      public SdtSDT_RedmineTrackers( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineTrackers_Type = "";
      }

      public SdtSDT_RedmineTrackers( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineTrackers deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineTrackers)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineTrackers obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Trackers = deserialized.gxTpr_Trackers;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_RedmineTrackers_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            if ( gxTv_SdtSDT_RedmineTrackers_Trackers != null )
            {
               gxTv_SdtSDT_RedmineTrackers_Trackers.ClearCollection();
            }
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp(oReader.LocalName, "tracker") == 0 )
               {
                  if ( gxTv_SdtSDT_RedmineTrackers_Trackers == null )
                  {
                     gxTv_SdtSDT_RedmineTrackers_Trackers = new GxObjectCollection( context, "SDT_RedmineTrackers.tracker", "", "SdtSDT_RedmineTrackers_tracker", "GeneXus.Programs");
                  }
                  GXSoapError = gxTv_SdtSDT_RedmineTrackers_Trackers.readxmlcollection(oReader, "", "tracker");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "trackers";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_RedmineTrackers_Type));
         if ( gxTv_SdtSDT_RedmineTrackers_Trackers != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineTrackers_Trackers.writexmlcollection(oWriter, "", sNameSpace1, "tracker", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("type", gxTv_SdtSDT_RedmineTrackers_Type, false);
         if ( gxTv_SdtSDT_RedmineTrackers_Trackers != null )
         {
            AddObjectProperty("trackers", gxTv_SdtSDT_RedmineTrackers_Trackers, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_RedmineTrackers_Type ;
         }

         set {
            gxTv_SdtSDT_RedmineTrackers_Type = (String)(value);
         }

      }

      public class gxTv_SdtSDT_RedmineTrackers_Trackers_SdtSDT_RedmineTrackers_tracker_80compatibility:SdtSDT_RedmineTrackers_tracker {}
      [  SoapElement( ElementName = "tracker" )]
      [  XmlElement( ElementName = "tracker" , Namespace = "" , Type= typeof( SdtSDT_RedmineTrackers_tracker ))]
      public GxObjectCollection gxTpr_Trackers_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_RedmineTrackers_Trackers == null )
            {
               gxTv_SdtSDT_RedmineTrackers_Trackers = new GxObjectCollection( context, "SDT_RedmineTrackers.tracker", "", "SdtSDT_RedmineTrackers_tracker", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_RedmineTrackers_Trackers ;
         }

         set {
            if ( gxTv_SdtSDT_RedmineTrackers_Trackers == null )
            {
               gxTv_SdtSDT_RedmineTrackers_Trackers = new GxObjectCollection( context, "SDT_RedmineTrackers.tracker", "", "SdtSDT_RedmineTrackers_tracker", "GeneXus.Programs");
            }
            gxTv_SdtSDT_RedmineTrackers_Trackers = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Trackers
      {
         get {
            if ( gxTv_SdtSDT_RedmineTrackers_Trackers == null )
            {
               gxTv_SdtSDT_RedmineTrackers_Trackers = new GxObjectCollection( context, "SDT_RedmineTrackers.tracker", "", "SdtSDT_RedmineTrackers_tracker", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_RedmineTrackers_Trackers ;
         }

         set {
            gxTv_SdtSDT_RedmineTrackers_Trackers = value;
         }

      }

      public void gxTv_SdtSDT_RedmineTrackers_Trackers_SetNull( )
      {
         gxTv_SdtSDT_RedmineTrackers_Trackers = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineTrackers_Trackers_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineTrackers_Trackers == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineTrackers_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineTrackers_Type ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RedmineTrackers_tracker ))]
      protected IGxCollection gxTv_SdtSDT_RedmineTrackers_Trackers=null ;
   }

   [DataContract(Name = @"SDT_RedmineTrackers", Namespace = "")]
   public class SdtSDT_RedmineTrackers_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineTrackers>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineTrackers_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineTrackers_RESTInterface( SdtSDT_RedmineTrackers psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "trackers" , Order = 1 )]
      public GxGenericCollection<SdtSDT_RedmineTrackers_tracker_RESTInterface> gxTpr_Trackers
      {
         get {
            return new GxGenericCollection<SdtSDT_RedmineTrackers_tracker_RESTInterface>(sdt.gxTpr_Trackers) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Trackers);
         }

      }

      public SdtSDT_RedmineTrackers sdt
      {
         get {
            return (SdtSDT_RedmineTrackers)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineTrackers() ;
         }
      }

   }

}
