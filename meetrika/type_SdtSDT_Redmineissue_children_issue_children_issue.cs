/*
               File: type_SdtSDT_Redmineissue_children_issue_children_issue
        Description: SDT_Redmineissue
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 21:37:14.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineissue.children.issue.children.issue" )]
   [XmlType(TypeName =  "SDT_Redmineissue.children.issue.children.issue" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_Redmineissue_children_issue_children_issue_tracker ))]
   [Serializable]
   public class SdtSDT_Redmineissue_children_issue_children_issue : GxUserType
   {
      public SdtSDT_Redmineissue_children_issue_children_issue( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject = "";
      }

      public SdtSDT_Redmineissue_children_issue_children_issue( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineissue_children_issue_children_issue deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineissue_children_issue_children_issue)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineissue_children_issue_children_issue obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Tracker = deserialized.gxTpr_Tracker;
         obj.gxTpr_Subject = deserialized.gxTpr_Subject;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("id") == 1 )
         {
            gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Id = (short)(NumberUtil.Val( oReader.GetAttributeByName("id"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "tracker") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker == null )
                  {
                     gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker = new SdtSDT_Redmineissue_children_issue_children_issue_tracker(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker.readxml(oReader, "tracker");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "subject") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineissue.children.issue.children.issue";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Id), 4, 0)));
         if ( gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker.writexml(oWriter, "tracker", sNameSpace1);
         }
         oWriter.WriteElement("subject", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Id, false);
         if ( gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker != null )
         {
            AddObjectProperty("tracker", gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker, false);
         }
         AddObjectProperty("subject", gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject, false);
         return  ;
      }

      [SoapAttribute( AttributeName = "id" )]
      [XmlAttribute( AttributeName = "id" )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Id ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "tracker" )]
      [  XmlElement( ElementName = "tracker"   )]
      public SdtSDT_Redmineissue_children_issue_children_issue_tracker gxTpr_Tracker
      {
         get {
            if ( gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker == null )
            {
               gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker = new SdtSDT_Redmineissue_children_issue_children_issue_tracker(context);
            }
            return gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker = value;
         }

      }

      public void gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker_SetNull( )
      {
         gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker = null;
         return  ;
      }

      public bool gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker_IsNull( )
      {
         if ( gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "subject" )]
      [  XmlElement( ElementName = "subject" , Namespace = ""  )]
      public String gxTpr_Subject
      {
         get {
            return gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Subject ;
      protected String sTagName ;
      protected SdtSDT_Redmineissue_children_issue_children_issue_tracker gxTv_SdtSDT_Redmineissue_children_issue_children_issue_Tracker=null ;
   }

   [DataContract(Name = @"SDT_Redmineissue.children.issue.children.issue", Namespace = "")]
   public class SdtSDT_Redmineissue_children_issue_children_issue_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineissue_children_issue_children_issue>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineissue_children_issue_children_issue_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineissue_children_issue_children_issue_RESTInterface( SdtSDT_Redmineissue_children_issue_children_issue psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "tracker" , Order = 1 )]
      public SdtSDT_Redmineissue_children_issue_children_issue_tracker_RESTInterface gxTpr_Tracker
      {
         get {
            return new SdtSDT_Redmineissue_children_issue_children_issue_tracker_RESTInterface(sdt.gxTpr_Tracker) ;
         }

         set {
            sdt.gxTpr_Tracker = value.sdt;
         }

      }

      [DataMember( Name = "subject" , Order = 2 )]
      public String gxTpr_Subject
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Subject) ;
         }

         set {
            sdt.gxTpr_Subject = (String)(value);
         }

      }

      public SdtSDT_Redmineissue_children_issue_children_issue sdt
      {
         get {
            return (SdtSDT_Redmineissue_children_issue_children_issue)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineissue_children_issue_children_issue() ;
         }
      }

   }

}
