﻿/* START OF FILE - ..\QueryViewerRender.src.js - */
/* START OF FILE - ..\QueryViewerRender.src.js - */
var CSSStyles = {};
var QueryViewerCollection = {};
//Pìvot Version 75
//Higcharts Version 4.1.4
//Pivot Js Version 1.2.0.8
function QueryViewer() {
    this.debugServices = false;
    this.chartSeriesCount = 0;
    this.chartXmlData = "";
    this.oat_element = "";
    this.PreferredRendererForQueryViewerPivotsAndTables = "htmlandjs";
    this.PreferredRendererForQueryViewerCharts = "htmlandjs";
    this.IsExternalQuery = false;
    this.ExternalQueryResult = "[]";
    this.ServerPagingForTable = true;
    this.ServerPagingForPivotTable = true;
    this.UseRecordsetCache = true;
    this.MaximumCacheSize = 100;
    this.MinutesToKeepInRecordsetCache = 5;
	
    var ACTIVITY_INDICATOR_CLASS = 'gx-qv-loading';
    this.showActivityIndicator = function () {
        gx.dom.addClass(this.getContainerControl(), ACTIVITY_INDICATOR_CLASS);
    };

    this.hideActivityIndicator = function () {
        gx.dom.removeClass(this.getContainerControl(), ACTIVITY_INDICATOR_CLASS);
    };

    var queryself = this;
    this.ConvertXmlAxisTypeToSdtAxisType = function (hidden, isdimiennsion, type) {
        if (hidden == true) return "NotShow";
        if (!isdimiennsion) return "Data";
        if (type == "rows") return "Row";
        if (type == "columns") return "Column";
        return "Page";
    }

    this.getSingleElementByTagName = function (parent, name) {
        var nodes;
        var node;
        nodes = this.getMultipleElementsByTagName(parent, name);
        node = (nodes.length > 0 ? nodes[0] : null);
        return node;
    }

    this.getMultipleElementsByTagName = function (parent, name) {
        var nodes;
        var node;
        if (!gx.util.browser.isIE() || gx.util.browser.ieVersion() >= 12) 
		nodes = parent.getElementsByTagName(name);
        else // Internet Explorer
		nodes = parent.selectNodes(name);
        return nodes;
    }

    this.getValueNode = function (node) {
        if ((node == null) || (node == undefined)) return null;
        if ((node.firstChild != null) && (node.firstChild != undefined)) return node.firstChild.nodeValue;
        else return null;
    }

    this.GetXAxisFromChart = function (XmlDoc) {
        var axes_RESULT = [];
        var atts = this.GetMetadataFromChart(XmlDoc);
        var Xaxes = atts["xAxisName"];
        var Xaxes_Array = Xaxes.split(",");
        var h = 0;
        var Xaxis = "";
        for (h = 0; h < Xaxes_Array.length; h++) {
            Xaxis = this.trim(Xaxes_Array[h]);
            var axis_SDT = {};
            axis_SDT = {
                Name: "",
                Title: "",
                Type: "",
                AxisOrder: null,
                Format: null
            };
            //axis_SDT.Name = Xaxis;    // Imposible determinar a partir del xml de la gráfica
            axis_SDT.Title = Xaxis;
            axis_SDT.Type = "Row";
            axes_RESULT.push(axis_SDT);
        }
        return axes_RESULT;
    }

    this.GetMetadataChartOneSerie = function (XmlDoc) {
        var axes_RESULT = [];
        var X_Axes = this.GetXAxisFromChart(XmlDoc);
        var j = 0;
        var num = 1;
        var sdtAxis = {};
        for (j = 0; j < X_Axes.length; j++) {
            sdtAxis = X_Axes[j];
            sdtAxis.DataField = "F" + num;
            axes_RESULT.push(X_Axes[j]);
            num++;
        }
        var atts = this.GetMetadataFromChart(XmlDoc);
        var Yaxis = atts["yAxisName"];
        var axis_SDT = {};
        axis_SDT = {
            Name: "",
            Title: "",
            Type: "",
            AxisOrder: null,
            Format: null
        };
        //axis_SDT.Name = Yaxis;    // Imposible determinar a partir del xml de la gráfica
        axis_SDT.Title = Yaxis;
        axis_SDT.Type = "Data";
        axis_SDT.DataField = "F" + num;
        axes_RESULT.push(axis_SDT);
        return axes_RESULT;
    }

    this.GetDataChartOneSerie = function (XmlDoc) {
        var XML_Result = "<OLAPData format=" + '"' + "adonet" + '"' + ">" + "<Table>";
        var i = 0;
        var setnodes = this.getMultipleElementsByTagName(XmlDoc.documentElement, "set");

        for (i = 0; i < setnodes.length; i++) {
            var XAxesValues = setnodes[i].getAttribute("hoverText");
            var value = setnodes[i].getAttribute("value");

            var XAxesValues_Array = XAxesValues.split(",");
            var g = 0;
            var f = 1;

            XML_Result = XML_Result + "<Record>";
            for (g = 0; g < XAxesValues_Array.length; g++) {
                XML_Result = XML_Result + "<F" + f + ">" + XAxesValues_Array[g] + "</F" + f + ">";
                f++;
            }
            XML_Result = XML_Result + "<F" + f + ">" + value + "</F" + f + ">";
            XML_Result = XML_Result + "</Record>";
        }
        XML_Result = XML_Result + " </Table></OLAPData>";
        return XML_Result;
    }

    this.GetDataChartMultiSerie = function (XmlDoc) {
        var i = 0;
        var j = 0;
        var value = "";
        var n = 0;
        var setsnodes;
        var xmlresult = "<OLAPData format=" + '"' + "adonet" + '"' + ">" + "<Table>";
        var categoriesNode = this.getSingleElementByTagName(XmlDoc.documentElement, "categories");
        var categories = this.getMultipleElementsByTagName(categoriesNode, "category");
        var datasets = this.getMultipleElementsByTagName(XmlDoc.documentElement, "dataset");
        var table = new Array(datasets.length + 1);
        table[i] = new Array(categories.length);

        for (j = 0; j < categories.length; j++) {
            value = categories[j].attributes[0].value;
            table[i][j] = value;
        }
        for (n = 0; n < datasets.length; n++) {
            setsnodes = this.getMultipleElementsByTagName(datasets[n], "set");
            i++;
            j = 0;
            table[i] = new Array(categories.length);
            for (j = 0; j < setsnodes.length; j++) {
                value = setsnodes[j].attributes[0].value;
                table[i][j] = value;
            }
        }

        var f = 1;
        n = datasets.length + 1;
        for (j = 0; j < categories.length; j++) {
            xmlresult = xmlresult + "<Record>";
            f = 1;
            for (i = 0; i < n; i++) {
                xmlresult = xmlresult + "<F" + f + ">" + table[i][j] + "</F" + f + ">";
                f++;
            }
            xmlresult = xmlresult + "</Record>";
        }
        xmlresult = xmlresult + "</Table></OLAPData>";
        return xmlresult;
    }

    this.GetMetadataChartMultiSerie = function (XmlDoc) {
        var axes_RESULT = [];
        var X_Axes = this.GetXAxisFromChart(XmlDoc);
        var j = 0;
        var Y_Axis = "";
        var num = 1;
        var sdtAxis = {};

        for (j = 0; j < X_Axes.length; j++) {
            sdtAxis = X_Axes[j];
            sdtAxis.DataField = "F" + num;
            num++;
            axes_RESULT.push(sdtAxis);
        }

        var datasets = this.getMultipleElementsByTagName(XmlDoc.documentElement, "dataset");

        for (j = 0; j < datasets.length; j++) {
            Y_Axis = datasets[j].attributes[0].value;
            var axis_SDT = {};
            axis_SDT = {
                Name: "",
                Title: "",
                Type: "",
                AxisOrder: null,
                Format: null
            };
            //axis_SDT.Name = Y_Axis;     // Imposible determinar a partir del xml de la gráfica
            axis_SDT.Title = Y_Axis;
            axis_SDT.Type = "Data";
            axis_SDT.DataField = "F" + num;
            num++;
            axes_RESULT.push(axis_SDT);
        }
        return axes_RESULT;
    }

    this.createCookie = function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    this.readCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    this.eraseCookie = function (name) {
        createCookie(name, "", -1);
    }

    this.GetMetadataChart = function () {

        xmlDoc = this.xmlDocument(this.chartXmlData);
        if (this.chartSeriesCount == 1) return this.GetMetadataChartOneSerie(xmlDoc);
        else return this.GetMetadataChartMultiSerie(xmlDoc);
    }

    this.GetMetadataPivotStringXML = function () {
        var UCId = this.userControlId();
        var ctl = document.getElementById(UCId);
        var result = ctl.getMetadata();
        return result;
    }

    this.GetDataPivotStringXML = function () {
        var UCId = this.userControlId();
        var ctl = document.getElementById(UCId);
        var result = ctl.getData();
        return result;
    }

    this.SetMetadataPivotStringXML = function (xmlmetadata) {
        var UCId = this.userControlId();
        var ctl = document.getElementById(UCId);
        ctl.setMetadata(xmlmetadata);
    }

    this.SetMetadataPivotStringXMLState = function (xmlmetadata) {
        var UCId = this.userControlId();
        var ctl = document.getElementById(UCId);
        if ((ctl != undefined) && (ctl.setMetadata != undefined)){
            ctl.setMetadata(xmlmetadata, false, true);
        }
    }

    this.SetDataPivotStringXML = function (xmldata) {
        var UCId = this.userControlId();
        var ctl = document.getElementById(UCId);
        ctl.setData(xmldata);
    }

    this.GetMetadataPivot = function () {
        var UCId = this.userControlId();
        var ctl = document.getElementById(UCId);
        var result;
        if (this.RenderHtmlAndJs()) {
        	result = this.oat_element.getMetadataXML();
		} else {
			result = ctl.getMetadata();
		}
        xmlDoc = this.xmlDocument(result);

        var axes = [];
        var axistype = "";

        for (i = 0; i < xmlDoc.documentElement.childNodes.length; i++) {

            var xmlqueryelement = xmlDoc.documentElement.childNodes[i];
            if ((xmlqueryelement.nodeName == "OLAPDimension") || (xmlqueryelement.nodeName == "OLAPMeasure")) {

                var name = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "name"));
                var displayName = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "displayName"));
                var dataField = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "dataField"));
                var hidden = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "hidden"));
                var picture = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "picture"));
				
				var axisPosition = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "order"));
				if (axisPosition == 0){
					axisPosition = null;
				}
				
                var includevalues = [];

                if (xmlqueryelement.nodeName == "OLAPDimension") {

                    var defaultPosition = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "defaultPosition"));
                    axistype = this.ConvertXmlAxisTypeToSdtAxisType(hidden, true, defaultPosition);

                    var include = this.getSingleElementByTagName(xmlqueryelement, "include");

                    for (j = 0; j < include.childNodes.length; j++) {
                        if (include.childNodes[j].firstChild != null) {
                            if (include.childNodes[j].firstChild.nodeValue.toUpperCase() != "TOTAL") includevalues.push(include.childNodes[j].firstChild.nodeValue);
                        }
                    }
                } else {
                    axistype = this.ConvertXmlAxisTypeToSdtAxisType(hidden, false, "");
                }

                var Filter = {
					Type: this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "filterType")),
                    Values: null
                };
                Filter.Values = includevalues;

                var Format = {
                    Picture: "",
                    Color: -1
                };
                Format.Picture = picture;
				
				var colType = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "collapseType"))
				var ExpandCollapse = {
					Type: colType
				}
				ExpandCollapse.Values = [];
				if (colType == 'ExpandSomeValues'){
					var expand = this.getSingleElementByTagName(xmlqueryelement, "includeExpand");
					 for (j = 0; j < expand.childNodes.length; j++) {
                        if (expand.childNodes[j].firstChild != null) {
                            ExpandCollapse.Values.push(expand.childNodes[j].firstChild.nodeValue);
                        }
                    }
				}
				
                var axis = {};
                axis = {
                    Name: "",
                    Title: "",
                    Type: "",
                    AxisOrder: axisPosition,
                    Format: null
                };
                axis.Name = name;
                axis.Title = displayName;
                axis.DataField = dataField;
                axis.Type = axistype;
                axis.Filter = Filter;
				if (xmlqueryelement.nodeName == "OLAPDimension") {
					axis.ExpandCollapse = ExpandCollapse; 
				}
                axis.Format = Format;
                axes.push(axis);
            }
        }

        return axes;
    }

    this.GetMetadataPivot_JS = function (data) {

        var result = data;
        xmlDoc = this.xmlDocument(result);

        var axes = [];
        var axistype = "";

        for (i = 0; i < xmlDoc.documentElement.childNodes.length; i++) {

            var xmlqueryelement = xmlDoc.documentElement.childNodes[i];
            if ((xmlqueryelement.nodeName == "OLAPDimension") || (xmlqueryelement.nodeName == "OLAPMeasure")) {

                var name = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "name"));
                var displayName = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "displayName"));
                var dataField = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "dataField"));
                var hidden = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "hidden"));
                var picture = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "picture"));

                var includevalues = [];

                if (xmlqueryelement.nodeName == "OLAPDimension") {

                    var defaultPosition = this.getValueNode(this.getSingleElementByTagName(xmlqueryelement, "defaultPosition"));
                    axistype = this.ConvertXmlAxisTypeToSdtAxisType(hidden, true, defaultPosition);

                    var include = this.getSingleElementByTagName(xmlqueryelement, "include");

                    for (j = 0; j < include.childNodes.length; j++) {
                        if (include.childNodes[j].firstChild != null) {
                            if (include.childNodes[j].firstChild.nodeValue.toUpperCase() != "TOTAL") includevalues.push(include.childNodes[j].firstChild.nodeValue);
                        }
                    }
                } else {
                    axistype = this.ConvertXmlAxisTypeToSdtAxisType(hidden, false, "");
                }

                var Filter = {
                    Values: null
                };
                Filter.Values = includevalues;

                var Format = {
                    Picture: "",
                    Color: -1
                };
                Format.Picture = picture;

                var axis = {};
                axis = {
                    Name: "",
                    Title: "",
                    Type: "",
                    AxisOrder: null,
                    Format: null
                };
                axis.Name = name;
                axis.Title = displayName;
                axis.DataField = dataField;
                axis.Type = axistype;
                axis.Filter = Filter;
                axis.Format = Format;
                axes.push(axis);
            }
        }

        return axes;
    }

    this.GetMetadata_JS = function (data) {
        var ischart = (this.RealType == "Chart");
        if (!ischart) return this.GetMetadataPivot_JS(data);
        else return this.GetMetadataChart();
    }

    this.GetMetadata = function () {
    	var ischart = (this.RealType == "Chart");
        
        if (!ischart) {
        	return this.GetMetadataPivot();
        } else return this.GetMetadataChart();
    }

    this.GetMetadataFromChart = function (xmlDoc) {
        var result = [];
        for (i = 0; i < xmlDoc.documentElement.attributes.length; i++)
        result[xmlDoc.documentElement.attributes[i].nodeName] = xmlDoc.documentElement.attributes[i].nodeValue;
        return result;
    }

    this.SetMetadataExecutingQuery = function (axes) {
        this.Axes = axes;
        this.realShow();
    }

    this.GetParameterinCollection = function (parameteroraxis, parametercollection) {

        var found = false;
        var i = 0;

        while ((!found) && (i < parametercollection.length)) {
            if (parametercollection[i].Name == parameteroraxis.Name) found = true;
            else i++;
        }

        if (found) return parametercollection[i];
        else return null;

    }

    this.GetIncludedValuesForAxis = function (Axis) {
        var result = "[";
        var i = 0;
        for (i = 0; i < Axis.Filter.Values.length - 1; i++) {
            result = result + '"' + Axis.Filter.Values[i] + '"' + ",";
        }

        if (Axis.Filter.Values.length - 1 >= 0) result = result + '"' + Axis.Filter.Values[Axis.Filter.Values.length - 1] + '"';

        result = result + "]";
        return result;

    }

    this.GetParameterValues = function (parameters, metadata) {
        // Mergea los valores de los paramtros deacuerdo a 3 variables
        // parameters ==> Con los valores por defecto de los parametros (ultimo nivel de prioridad)
        // this.Parameters ==> Con los valores que le pasa el usuario para ciertos parametros (nivel medio de prioridad)
        //metadata ==> Metadata de la Query asociada al autorefresgroup

        var parameterresult = [];
        var i = 0;
        var item = null;
        for (i = 0; i < parameters.length; i++) {
            // Busco el parametro en la collection de Axis de la Query asociada
            item = this.GetParameterinCollection(parameters[i], metadata);
            if (item != null) {
                // Existe un Eje con este nombre tiene la prioridad
                var value = this.GetIncludedValuesForAxis(item);
                var param = parameters[i];
                param.Value = value;
                parameterresult.push(param);
            } else {
                // NO existe un Eje con este nombre busco a ver si hay un parametro que le pasa el usuario
                item = this.GetParameterinCollection(parameters[i], this.Parameters);
                if (item != null) {
                    var param = parameters[i];
                    param.Value = item.Value;
                    parameterresult.push(param);

                } else {
                    // NO existe un parametro pasado por el usuario , se ejecuta con el valor por defecto
                    parameterresult.push(parameters[i]);
                }
            }

        } // for
        return parameterresult;
    }

    this.SetMetadataForPivotTable_JS = function (UC, crude, metadata, isSameQuery) {
        if (UC.ControlName != this.ControlName) {
            if (typeof (this.oat_element.refreshPivot) == 'function') {
                this.oat_element.refreshPivot(crude, metadata, isSameQuery);
            }
        }
    }
    this.SetMetadataExecutingDifferentQuery = function (metadata) {
        this.getQueryParameters((function (parametersXML) {
            var parameters = this.GetParametersFromXML(parametersXML);
            var realparameters = this.GetParameterValues(parameters, metadata);
            this.Refresh(realparameters);
        }).closure(this));
    }

    this.SetMetadataExecutingDifferentQuery2 = function (metadata) {
        this.getQueryParameters2((function (parametersXML) {
            var parameters = this.GetParametersFromXML(parametersXML);
            var realparameters = this.GetParameterValues(parameters, metadata);
            this.Refresh(realparameters);
        }).closure(this));
    }

    this.GetParametersFromXML = function (XMLparameters) {

        xmlDoc = this.xmlDocument(XMLparameters);

        var parameters = [];
        var i = 0;
        var parameter = {};
        parameter = {
            Name: "",
            Description: "",
            Type: "",
            IsCollection: true,
            Value: ""
        };


        for (i = 0; i < xmlDoc.documentElement.childNodes.length; i++) {


            if (((this.GenType != "Ruby") && (xmlDoc.documentElement.childNodes[i].nodeName == "gxpl_Parameter")) || ((this.GenType == "Ruby") && (xmlDoc.documentElement.childNodes[i].nodeName == "item"))) {
                parameter = {};
                parameter.Name = this.getValueNode(this.getSingleElementByTagName(xmlDoc.documentElement.childNodes[i], "Name"));
                parameter.Description = this.getValueNode(this.getSingleElementByTagName(xmlDoc.documentElement.childNodes[i], "Description"));
                parameter.Type = this.getValueNode(this.getSingleElementByTagName(xmlDoc.documentElement.childNodes[i], "Type"));
                parameter.IsCollection = this.getValueNode(this.getSingleElementByTagName(xmlDoc.documentElement.childNodes[i], "IsCollection"));
                parameter.Value = this.getValueNode(this.getSingleElementByTagName(xmlDoc.documentElement.childNodes[i], "Value"));
                parameters.push(parameter);
            }
        }
        return parameters;
    }

    this.SetMetadataForPivotTable = function (UC) {
        var newmetadataXML = UC.GetMetadataPivotStringXML();
        var dataXML = UC.GetDataPivotStringXML();
        this.SetMetadataPivotStringXMLState(newmetadataXML);
        this.SetDataPivotStringXML(dataXML);
        this.RefreshPivot();
    }


    this.NotifyData = function () {
        var data = this.GetData();
        var sdtdata = {};
        sdtdata.XmlData = data;

        this.ExecuteTracker("QueryViewerData", sdtdata);
    }

    this.NotifyFilteredData = function () {
        var data = this.GetFilteredData();
        var sdtdata = {};
        sdtdata.XmlData = data;
        this.ExecuteTracker("QueryViewerFilteredData", sdtdata);
    }

    this.NotifyMetadata = function () {
        var metadata = this.GetMetadata();
        this.ExecuteTracker("CollQueryViewerAxes.Axis", metadata);
    }

    this.ExecuteTracker = function (trackerId, trackerData) {
        var trackers = Array();
        for (var i = 0; i < gx.fx.ctx.trackers.length; i++)
        if (gx.fx.ctx.trackers[i].types[0] == trackerId) trackers[trackers.length] = gx.fx.ctx.trackers[i];
        for (var i = 0; i < trackers.length; i++) {
            var tgxO = gx.O;
            gx.O = trackers[i].obj;
            var tCmp = gx.csv.cmpCtx;
            gx.csv.cmpCtx = trackers[i].obj.CmpContext;
            gx.evt.setProcessing(false);
            trackers[i].hdl.call(trackers[i].obj, null, null, trackerData);
            gx.O = tgxO;
            gx.csv.cmpCtx = tCmp;
        }
    }

    this.GetData = function () {
        var ischart = (this.RealType == "Chart");
        if (!ischart) return this.GetDataPivot();
        else return this.GetDataChart();
    }

    this.GetDataPivot = function () {
    	var result;
    	if (this.RenderHtmlAndJs()) {
    		result = this.oat_element.getDataXML()	
    	} else {
        	var UCId = this.userControlId();
        	var ctl = document.getElementById(UCId);
        	result = ctl.getData();
        }
        return result;
    }

    this.GetDataChart = function () {
        xmlDoc = this.xmlDocument(this.chartXmlData);
        if (this.chartSeriesCount == 1) return this.GetDataChartOneSerie(xmlDoc);
        else return this.GetDataChartMultiSerie(xmlDoc);
    }

    this.GetFilteredDataPivot = function () {
    	var result;
    	if (this.RenderHtmlAndJs()) {
    		result = this.oat_element.getFilteredDataXML()
    	} else {
        	var UCId = this.userControlId();
        	var ctl = document.getElementById(UCId);
        	result = ctl.getFilteredData();
       }
        return result;
    }

    this.GetFilteredData = function () {
        var ischart = (this.RealType == "Chart");
        if (!ischart) return this.GetFilteredDataPivot();
        else return this.GetDataChart(); // en las Charts coincide el GetData con el GetFilteredData
    }
    
    this.NextPage = function(){
    	queryself.oat_element.moveToNextPage()
    }
    
    this.FirstPage = function(){
		queryself.oat_element.moveToFirstPage()
	}
	
	this.PreviousPage = function(){
		queryself.oat_element.moveToPreviousPage()
	}
	
	this.LastPage = function(){
		queryself.oat_element.moveToLastPage()
	}
	
    this.SetDragAndDropData = function (data) {
        this.DragAndDropData = data;
    }

    this.GetDragAndDropData = function () {
        return this.DragAndDropData;
    }

    this.SetItemExpandData = function (data) {
        this.ItemExpandData = data;
    }

    this.GetItemExpandData = function () {
        return this.ItemExpandData;
    }

    this.SetItemCollapseData = function (data) {
        this.ItemCollapseData = data;
    }

    this.GetItemCollapseData = function () {
        return this.ItemCollapseData;
    }

    this.SetFilterChangedData = function (data) {
        this.FilterChangedData = data;
    }

    this.GetFilterChangedData = function () {
        return this.FilterChangedData;
    }

    this.SetAggregationChangedData = function (data) {
        this.AggregationChangedData = data;
    }

    this.GetAggregationChangedData = function () {
        return this.AggregationChangedData;
    }

    this.SetItemClickData = function (data) {
        this.ItemClickData = data;
    }

    this.GetItemClickData = function () {
        return this.ItemClickData;
    }

    this.SetItemDoubleClickData = function (data) {
        this.ItemDoubleClickData = data;
    }

    this.GetItemDoubleClickData = function () {
        return this.ItemDoubleClickData;
    }

    this.SetParameters = function (data) {
        this.Parameters = data;
    }

    this.GetParameters = function () {
        return this.Parameters;
    }

    this.SetAxes = function (data) {
        this.Axes = data;
    }

    this.GetAxes = function () {
        return this.Axes;
    }

	this.Refresh = function (parameters) {
        this.Parameters = parameters;
        this.realShow();
        var ischart = (this.RealType == "Chart");
        if (!ischart) this.RefreshPivot();
    }

    this.RefreshPivot = function () {
        var UCId = this.userControlId();
        var ctl = document.getElementById(UCId);
        ctl.refresh();
    }

    this.show = function () {
			
        if (!gx.lang.gxBoolean(this.AvoidAutomaticShow) || gx.lang.gxBoolean(this.ExecuteShow)) {
            QueryViewerCollection[this.userControlId()] = this;
            this.isdynamicrefresh = false;
            this.realShow();
        }
        if (gx.lang.gxBoolean(this.AvoidAutomaticShow)) this.ExecuteShow = false;
        
    }


    this.calculateRealOutputType = function (callback) {

        if (gx.lang.gxBoolean(this.Visible)) {

            // Calculate this.GenType
            this.GenType = this.getGenerator();

            if ((this.Type != undefined) && (this.Type != "")) this.RealType = this.Type;
            else this.RealType = "Table";

            if ((this.ChartType != undefined) && (this.ChartType != "")) this.RealChartType = this.ChartType;
            else this.RealChartType = "";

            if (this.GenType == "C#" || this.GenType == "Java" || this.GenType == "Ruby") {
                if (this.Type == "Default") {
                    this.getDefaultOutput((function (output) {
                        if ((output != "Table") && (output != "PivotTable")) {
                            this.RealType = "Chart";
                            this.RealChartType = output;
                        } else
                            this.RealType = output;
                        callback();
                    }).closure(this));
                    return;
                } //if (this.Type == "Default")
            } //(this.GenType == "C#" || this.GenType == "Java" || this.GenType == "Ruby")
        } //if (gx.lang.gxBoolean(this.Visible))
        callback();
    } // end function
	
    this.realShowforRenderTypeHtmlandJs = function () {

        if (gx.lang.gxBoolean(this.Visible)) {

            this.getContainerControl().style.display = "";

            var buff = "<div id=\"" + this.userControlId() + "\" style=\"width: " + this.fixSize(this.Width) + "px; height:" + this.fixSize(this.Height) + "px;\"></div>";
            document.getElementById(this.ContainerName).innerHTML = buff.toString();

            // Calculate this.GenType
            this.GenType = this.getGenerator();

            if ((this.Type != undefined) && (this.Type != "")) this.RealType = this.Type;
            else this.RealType = "Table";

            if ((this.ChartType != undefined) && (this.ChartType != "")) this.RealChartType = this.ChartType;
            else this.RealChartType = "";

            if (this.GenType == "C#" || this.GenType == "Java" || this.GenType == "Ruby") {
                if (this.Type == "Table") this.tryToRenderTable();
                else {
                    if (this.Type == "PivotTable") this.tryToRenderPivotTable();
                    else {
                        if (this.Type == "Default") {
                            this.getDefaultOutput((function (output) {
                                if ((output != "Table") && (output != "PivotTable")) {
                                    this.RealType = "Chart";
                                    this.RealChartType = output;
                                } else this.RealType = output;

                                if (this.RealType == "Table") {
                                    /*this.Paging = true;
                                    if (this.PageSize == undefined) {
                                        this.PageSize = 10;
                                    }*/
                                    this.tryToRenderTable();
                                } else {
                                    if (this.RealType == "PivotTable") {
                                        /*this.Paging = true;
                                        if (this.PageSize == undefined) {
                                            this.PageSize = 10;
                                        }*/
                                        this.tryToRenderPivotTable();
                                    } else this.tryToRenderChart();
                                }
                            }).closure(this));
                        } else this.tryToRenderChart();
                    }
                }
            } else //if (this.Type == "Table")
            this.renderError(gx.getMessage("GXPL_QViewerGenNotImplemented"));
        } else //if (this.GenType == "C#" || this.GenType == "Java" || this.GenType == "Ruby") 
        this.getContainerControl().style.display = "none";

    }

    this.realShowforRenderTypeFlash = function () {
        if (gx.lang.gxBoolean(this.Visible)) {

            this.getContainerControl().style.display = "";

            FlashInstalled = this.DetectFlashPlayer();
            if (FlashInstalled) {
                // Calculate this.GenType
                this.GenType = this.getGenerator();

                if ((this.Type != undefined) && (this.Type != "")) this.RealType = this.Type;
                else this.RealType = "Table";

                if ((this.ChartType != undefined) && (this.ChartType != "")) this.RealChartType = this.ChartType;
                else this.RealChartType = "";

                if (this.GenType == "C#" || this.GenType == "Java" || this.GenType == "Ruby") {
                    if (this.Type == "Table") this.tryToRenderTable();
                    else {
                        if (this.Type == "PivotTable") this.tryToRenderPivotTable();
                        else {
                            if (this.Type == "Default") {

                                this.getDefaultOutput((function (output) {
                                    if ((output != "Table") && (output != "PivotTable")) {
                                        this.RealType = "Chart";
                                        this.RealChartType = output;
                                    } else this.RealType = output;

                                    if (this.RealType == "Table") {
                                        this.Paging = true;
                                        if (this.PageSize == undefined) {
                                            this.PageSize = 10;
                                        }
                                        this.tryToRenderTable();
                                    } else {
                                        if (this.RealType == "PivotTable") {
                                            this.Paging = true;
                                            if (this.PageSize == undefined) {
                                                this.PageSize = 10;
                                            }
                                            this.tryToRenderPivotTable();
                                        } else this.tryToRenderChart();
                                    }
                                }).closure(this));
                            } else this.tryToRenderChart();
                        }
                    }
                } else this.renderError(gx.getMessage("GXPL_QViewerGenNotImplemented"));
            } else this.renderFlashError();
        } else this.getContainerControl().style.display = "none";
    }

    this.RenderHtmlAndJs = function()
    {
        return (this.PreferredRendererForQueryViewerCharts == "htmlandjs" && this.RealType == "Chart" && (!gx.util.browser.isIE() || 8<gx.util.browser.ieVersion())) 
                || (this.PreferredRendererForQueryViewerPivotsAndTables == "htmlandjs" && (this.RealType == "PivotTable" || this.RealType == "Table") && (!gx.util.browser.isIE() || 8<gx.util.browser.ieVersion())) 
                || gx.util.browser.isIPad() || gx.util.browser.isIPhone(); 
    }

    this.realShow = function () {
        var controlClassName,
            container = document.getElementById(this.ContainerName);

        if (!gx.lang.gxBoolean(this.IsExternalQuery)) {
            this.showActivityIndicator();
        }
        this.calculateRealOutputType((function () {
            if (this.RealChartType) {
                controlClassName = "gx-qv-" + this.RealChartType.toLowerCase() + "-chart";
                if (this.currentControlClassName) {
                    gx.dom.removeClass(container, this.currentControlClassName);
                }
                gx.dom.addClass(container, controlClassName);
                this.currentControlClassName = controlClassName;
            }
            if (this.RenderHtmlAndJs())
                this.realShowforRenderTypeHtmlandJs();
            else
                this.realShowforRenderTypeFlash();
        }).closure(this));
    }

    this.baseUrl = function () {
        var url = new gx.util.Url(location.href);

        var path = url.path;
        var pathResult = path.substr(0, path.lastIndexOf('/') + 1);

        if (url.port == "") urlresult = url.protocol + "://" + url.host + pathResult;
        else urlresult = url.protocol + "://" + url.host + ":" + url.port + pathResult;

        return urlresult;

    }

    this.isGeneXusPreview = function () {
        return (window.external.Text != undefined);
    }

    this.GetJavaBaseUrl = function (staticContent) {
        var baseUrl = gx.util.resourceUrl(gx.basePath, true);
        if (staticContent) {
            baseUrl += this.staticDirectory();
        } else baseUrl += "servlet/";

        return baseUrl;
    }

    this.staticDirectory = function () {
        if (gx.staticDirectory != "") return gx.staticDirectory;
        else return this.getCookie('StaticContent');
    }

    this.getHtmlColor = function (color) {
        var htmlColor;
        if ((typeof (color) == "string") || (typeof (color) == "number")) {
            if (gx.color.toHex) {
                if (color == -1) // -1 = null color
                htmlColor = "";
                else htmlColor = "#" + gx.color.toHex(color); // GeneXus X Ev. 1
            } else htmlColor = "#" + gxToHex(color) // GeneXus X
        } else htmlColor = (color == undefined ? "#000000" : color.Html);
        return htmlColor;
    }

    this.parseCSSColor = function (color) {
        if (color.substring(0, 3) == "rgb") {
            var values = color.replace("rgb", "").replace("(", "").replace(")", "").split(",");
            var numColor = gx.color.rgb(parseInt(values[0]), parseInt(values[1]), parseInt(values[2]));
            return this.getHtmlColor(numColor);
        } else return color;
    }

    this.foolCache = function () {
        var currentTime = new Date();
        var miliSecs = currentTime.getTime();
        return miliSecs;
    }

    this.DoDynamicRefresh_JS = function (data) {
        this.isdynamicrefresh = true;
        var my_refreshgroup = this.AutoRefreshGroup;

        if ((my_refreshgroup != undefined) && (my_refreshgroup != null) && (my_refreshgroup != "") && (this.trim(my_refreshgroup) != "")) {

            for (index in QueryViewerCollection) {
                ucqueryviewer = QueryViewerCollection[index];
                if (ucqueryviewer.userControlId() != this.userControlId()) {

                    if (my_refreshgroup == ucqueryviewer.AutoRefreshGroup) {
                        // In same Refresh Group
                        ucqueryviewer.isdynamicrefresh = true;
                        var metadata = this.GetMetadata_JS(data);
                        if ((this.ObjectId == ucqueryviewer.ObjectId) || (this.ObjectName == ucqueryviewer.ObjectName)) {
                            if (((this.RealType == ucqueryviewer.RealType) && (this.RealType == "PivotTable")) || ((this.RealType == ucqueryviewer.RealType) && (this.RealType == "Table")) || ((this.RealType != ucqueryviewer.RealType) && ((ucqueryviewer.RealType == "PivotTable") || (ucqueryviewer.RealType == "Table"))))
                                ucqueryviewer.SetMetadataForPivotTable_JS(this, data, metadata, true);
                            else
                                ucqueryviewer.SetMetadataExecutingQuery(metadata);
                        }
                        else {
                            if (ucqueryviewer.RealType == "Chart") {
                                ucqueryviewer.SetMetadataExecutingDifferentQuery2(metadata);
                            } else {
                                ucqueryviewer.SetMetadataForPivotTable_JS(this, data, metadata, false);
                            }
                        }

                    } //  if (my_refreshgroup == ucqueryviewer.AutoRefreshGroup)
                } //  if (ucqueryviewer.ContainerName + "_" + ucqueryviewer.ControlName != this.ContainerName + "_" + this.ControlName

            } // for
        }
    }

    this.DoDynamicRefresh = function () {
        this.isdynamicrefresh = true;
        var my_refreshgroup = this.AutoRefreshGroup;

        if ((my_refreshgroup != undefined) && (my_refreshgroup != null) && (my_refreshgroup != "") && (this.trim(my_refreshgroup) != "")) {

            for (index in QueryViewerCollection) {
                ucqueryviewer = QueryViewerCollection[index];
                if (ucqueryviewer.userControlId() != this.userControlId()) {

                    if (my_refreshgroup == ucqueryviewer.AutoRefreshGroup) {
                        // In same Refresh Group
                        ucqueryviewer.isdynamicrefresh = true;
                        var metadata = this.GetMetadata();
                        if ((this.ObjectId == ucqueryviewer.ObjectId) || (this.ObjectName == ucqueryviewer.ObjectName)) {
                            if (((this.RealType == ucqueryviewer.RealType) && (this.RealType == "PivotTable")) || ((this.RealType == ucqueryviewer.RealType) && (this.RealType == "Table")) || ((this.RealType != ucqueryviewer.RealType) && ((ucqueryviewer.RealType == "PivotTable") || (ucqueryviewer.RealType == "Table"))))
                                ucqueryviewer.SetMetadataForPivotTable(this);
                            else
                                ucqueryviewer.SetMetadataExecutingQuery(metadata);
                        }
                        else {
                            ucqueryviewer.SetMetadataExecutingDifferentQuery(metadata); 
                        }

                    } //  if (my_refreshgroup == ucqueryviewer.AutoRefreshGroup)
                } //  if (ucqueryviewer.ContainerName + "_" + ucqueryviewer.ControlName != this.ContainerName + "_" + this.ControlName

            } // for
        } //  if ((my_refreshgroup != undefined) && (my_refreshgroup != null) && (my_refreshgroup != "") && (this.trim(my_refreshgroup) != ""))

    } // method


    this.selectXPathNode = function (xmlDoc, xpath) {
        var nodes;
        var node;
        if (xmlDoc.evaluate) { // Firefox, Chrome, Opera and Safari
            nodes = xmlDoc.evaluate(xpath, xmlDoc, null, XPathResult.ANY_TYPE, null);
            node = nodes.iterateNext();
        } else { // Internet Explorer
            nodes = xmlDoc.selectNodes(xpath);
            node = (nodes.length > 0 ? nodes[0] : null);
        }
        return node;
    }


    this.onRestoreDefaultView = function (data) {

        this.DoDynamicRefresh();

        var xml_doc = this.xmlDocument(data);
        var Node = this.selectXPathNode(xml_doc, "/DATA");

        this.DoDynamicRefresh();
    }

    this.onDragAndDropEvent = function (data) {
        var position;
        if (this.RealType == "PivotTable") {
            // Dynamic Refresh
            this.DoDynamicRefresh();
            if (this.isGeneXusPreview()) window.external.Text = data;
            if (this.DragAndDrop) {
                var xml_doc = this.xmlDocument(data);
                var Node = this.selectXPathNode(xml_doc, "/DATA");
                this.DragAndDropData.Name = Node.getAttribute("name");
                position = Node.getAttribute("position");
                this.DragAndDropData.Axis = (position == "rows" ? "Row" : (position == "columns" ? "Column" : "Page"));
                this.DragAndDrop();
            }
        }
    }

    this.onOAT_PIVOTDragAndDropEvent = function (data) {
        var position;
        if (this.RealType == "PivotTable") {
            if (this.isGeneXusPreview()) window.external.Text = data;
            if (this.DragAndDrop) {
                var xml_doc = this.xmlDocument(data);
                var Node = this.selectXPathNode(xml_doc, "/DATA");
                this.DragAndDropData.Name = Node.getAttribute("name");
                position = Node.getAttribute("position");
                this.DragAndDropData.Axis = (position == "rows" ? "Row" : (position == "columns" ? "Column" : "Page"));
                this.DragAndDrop();
            }
        }
    }
    
    this.onAggregatorChangedEvent = function (data) {
        var aggregator;
        if (this.RealType == "PivotTable") {
            if (this.AggregationChanged) {
                var xml_doc = this.xmlDocument(data);
                var Node = this.selectXPathNode(xml_doc, "/DATA");
                this.AggregationChangedData.Name = Node.getAttribute("name");
                aggregator = Node.getAttribute("aggregator");
                this.AggregationChangedData.Aggregation = (aggregator == "sum" ? "Sum" : (aggregator == "average" ? "Average" : (aggregator == "count" ? "Count" : (aggregator == "maximum" ? "Max" : "Min"))));
                this.AggregationChanged();
            }
        }
    }

    this.onValuesChangedEvent = function (data) {
        if (this.RenderHtmlAndJs()) {
            this.DoDynamicRefresh_JS(data);
        } else {
            this.DoDynamicRefresh();

            if (this.FilterChanged) {
                var xml_doc = this.xmlDocument(data);
                var Node = this.selectXPathNode(xml_doc, "/DATA");
                this.FilterChangedData.Name = Node.getAttribute("name");
                this.FilterChangedData.SelectedValues = [];
                var valueIndex = -1;
                for (var i = 0; i < Node.childNodes.length; i++)
                if (Node.childNodes[i].nodeName == "VALUE") {
                    valueIndex++;
                    this.FilterChangedData.SelectedValues[valueIndex] = Node.childNodes[i].firstChild.nodeValue;
                }
                this.FilterChanged();
            }
        }
    }

    this.onItemClickEvent = function (data, isDoubleClick) {
        var location;
        var eventData;
        if ((this.ItemClick && !isDoubleClick) || (this.ItemDoubleClick && isDoubleClick)) {
            eventData = (isDoubleClick ? eventData = this.ItemDoubleClickData : this.ItemClickData);
            var xml_doc = this.xmlDocument(data);
            var Node1 = this.selectXPathNode(xml_doc, "/DATA/ITEM");
            var Node2 = this.selectXPathNode(xml_doc, "/DATA/CONTEXT/RELATED");
            var Node3 = this.selectXPathNode(xml_doc, "/DATA/CONTEXT/FILTERS");
            eventData.Name = Node1.getAttribute("name");
            location = Node1.getAttribute("location");
            eventData.Axis = (location == "rows" ? "Row" : (location == "columns" ? "Column" : (location == "pages" ? "Page" : "Data")));
            eventData.Value = (Node1.firstChild != null ? Node1.firstChild.nodeValue : "");
            eventData.Context = this.loadItems(Node2);
            eventData.Filters = this.loadItems(Node3);
            if (isDoubleClick) this.ItemDoubleClick();
            else this.ItemClick();
        }
    }

    // Manejador para el evento Item Click sobre las series de las graficas Highcharts
    this.onHighchartsItemClickEventHandler = function (event) {
        var eventData;
        var ctrl = QueryViewerCollection[this.chart.options.viewerId];
        if (ctrl.ItemClick) {
            eventData = ctrl.ItemClickData;
            var isMultiserie = event.point.series.chart.series.length && event.point.series.chart.series.length > 1;
            var isTimeAxis = this.chart.axes[0].isDatetimeAxis;
            if (event.point.series.chart.options.yAxis.title != undefined){
                eventData.Name = "" + (isMultiserie ? event.point.series.name : event.point.series.chart.options.yAxis.title.text);
            } else {
                eventData.Name = "" + (isMultiserie ? event.point.series.name : event.point.series.chart.options.yAxis[0].title.text);
            }
            eventData.Axis = "Data";
            eventData.Value = "" + event.point.y;
            eventData.Context = [];
            eventData.Context[0] = {};
            if (event.point.series.chart.options.yAxis.title != undefined){
                eventData.Context[0].Name = "" + (isMultiserie ? event.point.series.xAxis.userOptions.title.text : event.point.series.chart.options.yAxis.title.text);
            } else {
                eventData.Context[0].Name = "" + (isMultiserie ? event.point.series.xAxis.userOptions.title.text : event.point.series.chart.options.yAxis[0].title.text);
            }
            eventData.Context[0].Values = [];
            eventData.Context[0].Values[0] = "" + (isTimeAxis ? new Date(event.point.x) : event.point.id);
            eventData.Context[1] = {};
            if (event.point.series.chart.options.yAxis.title != undefined){
                eventData.Context[1].Name = "" + (isMultiserie ? event.point.series.name : event.point.series.chart.options.yAxis.title.text);
            } else {
                eventData.Context[0].Name = "" + (isMultiserie ? event.point.series.xAxis.userOptions.title.text : event.point.series.chart.options.yAxis[0].title.text);
            }
            eventData.Context[1].Values = [];
            eventData.Context[1].Values[0] = "" + event.point.y;
            eventData.Filters = [];
            ctrl.ItemClick();
        }
    }

    this.onHighchartsXAxisClickEventHandler = function (event, tickInd, tick, chart) {
        var eventData;
        var ctrl = QueryViewerCollection[chart.options.viewerId];
        if (ctrl.ItemClick) {
            eventData = ctrl.ItemClickData;
            var isMultiserie = chart.series.length && chart.series.length > 1;
            var isTimeAxis = chart.xAxis[0].isDatetimeAxis;
            var timezoneOffset = new gx.date.gxdate(new Date()).Value.getTimezoneOffset() * 60000;
            var time = tick.pos + timezoneOffset;
            eventData.Name = "" + (isMultiserie ? chart.xAxis[0].userOptions.title.text : chart.series[0].name);
            eventData.Axis = "Row";
            eventData.Value = "" + (isTimeAxis ? new Date(time) : chart.xAxis[0].userOptions.fullCategoriesValues[tickInd]);
            eventData.Context = [];
            eventData.Context[0] = {};
            eventData.Context[0].Name = "" + eventData.Name;
            eventData.Context[0].Values = [];
            eventData.Context[0].Values[0] = "" + (isTimeAxis ? new Date(time) : chart.xAxis[0].userOptions.fullCategoriesValues[tickInd]);
            for (var serieInd = 0; chart.series[serieInd] != undefined; serieInd++) {
                eventData.Context[serieInd + 1] = {};
                eventData.Context[serieInd + 1].Name = "" + (isMultiserie ? chart.series[serieInd].name : chart.yAxis[0].userOptions.title.text);
                eventData.Context[serieInd + 1].Values = [];
                var count = 0;
                if (isTimeAxis) {
                    for (var _i = 0; chart.series[serieInd].data[_i] != undefined; _i++) {
                        if ((chart.series[serieInd].data[_i].x - timezoneOffset) == time) {
                            eventData.Context[serieInd + 1].Values[count] = "" + chart.series[serieInd].data[_i].y;
                            count++;
                        }
                    }
                } else {
                    for (var _i = 0; chart.series[serieInd].data[_i] != undefined; _i++) {
                        if (chart.series[serieInd].data[_i].x == tickInd) {
                            eventData.Context[serieInd + 1].Values[count] = "" + (isMultiserie ? chart.series[serieInd].data[tickInd].y : chart.series[0].data[tickInd].y);
                            count++;
                        }
                    }
                }
            }
            eventData.Filters = [];
            ctrl.ItemClick();
        }
    }

    this.loadItems = function (Node) {
        var Items = [];
        if (Node != null) {
            var itemIndex = -1;
            for (var i = 0; i < Node.childNodes.length; i++) {
                if (Node.childNodes[i].nodeName == "ITEM") {
                    itemIndex++;
                    Items[itemIndex] = {};
                    Items[itemIndex].Name = Node.childNodes[i].getAttribute("name");
                    Items[itemIndex].Values = [];
                    var valueIndex = -1;
                    // Seek VALUES node
                    var valuesNode = Node.childNodes[i]; // ITEM
                    for (var j = 0; j < Node.childNodes[i].childNodes.length; j++)
                    if (Node.childNodes[i].childNodes[j].nodeName == "VALUES") {
                        valuesNode = Node.childNodes[i].childNodes[j];
                        break;
                    }
                    // Seek VALUE nodes
                    for (var j = 0; j < valuesNode.childNodes.length; j++)
                    if (valuesNode.childNodes[j].nodeName == "VALUE") {
                        valueIndex++;
                        Items[itemIndex].Values[valueIndex] = (valuesNode.childNodes[j].firstChild != null ? valuesNode.childNodes[j].firstChild.nodeValue : "");
                    }
                }
            }
        }
        return Items;
    }

    this.onItemExpandCollapseEvent = function (data, isCollapse) {
        var location;
        var eventData;
        if ((this.ItemExpand && !isCollapse) || (this.ItemCollapse && isCollapse)) {
            eventData = (isCollapse ? eventData = this.ItemCollapseData : this.ItemExpandData);
            var xml_doc = this.xmlDocument(data);
            var Node1 = this.selectXPathNode(xml_doc, "/DATA/ITEM");
            var Node2 = this.selectXPathNode(xml_doc, "/DATA/CONTEXT/EXPANDEDVALUES");
            eventData.Name = Node1.getAttribute("name");
            eventData.Value = Node1.firstChild.nodeValue;
            eventData.ExpandedValues = [];
            var valueIndex = -1;
            for (var i = 0; i < Node2.childNodes.length; i++)
            if (Node2.childNodes[i].nodeName == "VALUE") {
                valueIndex++;
                eventData.ExpandedValues[valueIndex] = Node2.childNodes[i].firstChild.nodeValue;
            }
            if (isCollapse) this.ItemCollapse();
            else this.ItemExpand();
        }
    }

    this.onResizePivotEvent = function (ctl, info) {
        if (gx.lang.gxBoolean(this.AutoResize)) {
            switch (this.AutoResizeType) {

                case "Horizontal":
                    ctl.setScrollPolicy('off', 'auto');
                    var dim = info.data.split(';');
                    ctl.width = dim[0];
                    break;

                case "Vertical":
                    ctl.setScrollPolicy('auto', 'off');
                    var dim = info.data.split(';');
                    ctl.height = dim[1];
                    break;
                default:
                    var dim = info.data.split(';');
                    ctl.width = dim[0];
                    ctl.height = dim[1];
                    break;
            }
        }
    }

    this.AdjustSize = function () // Para las salidas que no soportan AutoResize
    {
        if (gx.lang.gxBoolean(this.AutoResize)) {
            this.Width = "580";
            this.Height = "470";
        }
    }

    this.getSessionKey = function () {
        var sessionKey;
        try {
            if (this.getGXVersion() == "10.0") sessionKey = GXGetHidden("GX_AJAX_KEY").toLowerCase(); // GeneXus X
            else sessionKey = gx.sec.key.toLowerCase(); // GeneXus X Ev. 
        } catch (e) {
            sessionKey = ""; // En Ruby no hay encriptación
        };
        return sessionKey;
    }

    function getmetaContents(mn) {
        var m = document.getElementsByTagName('meta');
        for (i = 0; i < m.length; i++)
        if (m[i].name == mn) return m[i].content;
    }

    this.getGXVersion = function () {
        var gxVersion = getmetaContents("Version");
        gxVersion = gxVersion.substring(0, 4).replace("_", ".");
        return gxVersion;
    }

    this.getGenerator = function () {
        var gen;
        if (gx.gen.isDotNet()) gen = "C#";
        else if (gx.gen.ruby) gen = "Ruby";
        else gen = "Java";
        return gen;
    }

    this.fixSize = function (size) {
        return size.replace("px", "");
    }

    this.userControlId = function () {
        return this.ContainerName.replace("Container", "") + "_" + this.ControlName; // Nombre único y diferente al contenedor
    }

    // ---------------------------------------- Error ----------------------------------------

    this.DetectFlashPlayer = function () {
        var hasFlashRequestedVersion;
        if (this.HasFlashRequestedVersion == undefined) {
            // Versión mínima requerida por la PivotTable (FusionCharts requiere una versión menor)
            var requiredMajorVersion = 10;
            var requiredMinorVersion = 0;
            var requiredRevision = 0;
            hasFlashRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
            this.HasFlashRequestedVersion = hasFlashRequestedVersion;
        } else hasFlashRequestedVersion = this.HasFlashRequestedVersion;
        return hasFlashRequestedVersion;
    }

    this.renderFlashError = function () {
        var errMsg;
        if (this.isGeneXusPreview()) errMsg = gx.getMessage("GXPL_QViewerFlashRequiredGX");
        else errMsg = gx.getMessage("GXPL_QViewerFlashRequiredRuntime");
        this.renderError(errMsg);
    }

    this.renderError = function (errMsg) {
        if (this.isGeneXusPreview()) // Preview en GeneXus (se muestra en el output)
        window.external.Text = errMsg;
        else // Aplicación generada
        {
            this.AdjustSize();
            var buffer = "<table height=" + this.fixSize(this.Height) + " width =" + this.fixSize(this.Width) + " style=\"border-right: silver thin solid; border-top: silver thin solid; border-left: silver thin solid; border-bottom: silver thin solid; font-size: " + this.FontSize + "px; color: " + this.getHtmlColor(this.FontColor) + "; font-family: " + this.FontFamily + "\"><tbody><tr><td align=center>" + gx.getMessage("GXPL_QViewerError") + ": " + errMsg + "</td></tr></tbody></table>";
            this.setHtml(buffer);
        }
    }

    this.getErrorFromText = function (resultText) {
        if (resultText == "<Result OK=\"True\"></Result>") return ""; // No hubo error
        else return resultText.replace("<Result OK=\"False\"><Dsc>", "").replace("</Dsc></Result>", "");
    }

    this.anyError = function (resultText) {
        return (resultText.indexOf("<Result OK=\"False\"><Dsc>") == 0);
    }

    // --------------------------------------- General ---------------------------------------

    this.ExternalQueryResultObj = function() {
        if (this._ExternalQueryResultObj == undefined || this.ExternalQueryResult != this._LastExternalQueryResult) {
            this._ExternalQueryResultObj = eval('(' + this.ExternalQueryResult + ')');
            this._LastExternalQueryResult = this.ExternalQueryResult;
        }
        return this._ExternalQueryResultObj;
    }

    var STATE_DONE = 4,
        STATUS_OK = 200;
    this.createServerCallFn = function (outputUrlFn, postInfoFn, externalQueryMember, responsePreProcessFn) {
        return function(callback, postInfoParms) {
            postInfoParms = postInfoParms || [];
            var responseFn = (function (response) {
                if (responsePreProcessFn) {
                    response = responsePreProcessFn.call(this, response);
                }
                callback(response);
            }).closure(this);

            if (!gx.lang.gxBoolean(this.IsExternalQuery)) {
                var src = outputUrlFn.call(this),
                    postInfo = postInfoFn.apply(this, postInfoParms),
                    xmlHttp = this.getXMLHttpRequest(),
                    responseHandler = function() {
                        if (xmlHttp.readyState == STATE_DONE && xmlHttp.status == STATUS_OK) {
                            responseFn(xmlHttp.responseText);
                        }
                    };
                if (gx.util.browser.isIE()) {
                    xmlHttp.onreadystatechange = responseHandler;
                }
                else {
                    xmlHttp.onload = responseHandler;
                }
                xmlHttp.open("POST", src); // async
                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xmlHttp.send(postInfo);
            }
            else {
                responseFn(this.ExternalQueryResultObj()[externalQueryMember]);
            }
        };
    };

    this.CallServerSync = function (outputUrlFn, postInfoFn, postInfoParms) {
        postInfoParms = postInfoParms || [];
        var src = outputUrlFn.call(this);
        var postInfo = postInfoFn.apply(this, postInfoParms);
        var xmlHttp = this.getXMLHttpRequest();
        xmlHttp.open("POST", src, false); // sync
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlHttp.send(postInfo);
        return xmlHttp.responseText;
    };

    this.getServiceURL = function (serviceName) {
        var urlParam;
        if (this.GenType == "") this.GenType = "C#";
        var foolCache = this.foolCache(); // Para que el explorador no cachee los resultados de los servicios

        switch (this.GenType) {
            case "C#":
                return this.baseUrl() + "agxpl_" + serviceName + ".aspx?" + foolCache;
            case "Java":
                return this.GetJavaBaseUrl(false) + "agxpl_" + serviceName + "?" + foolCache;
            case "Ruby":
                return this.baseUrl() + "gxruby/GXplorerServices.gxpl_" + serviceName + "?" + foolCache;
            default:
                return "";
        }
    }
	
    this.getDefaultOutputURL = function () {
		return this.getServiceURL("getdefaultoutput");
    }

    this.replaceCssClasses = function (xml) {
        var replaceMarker = "gxpl_cssReplace(";
        while ((posIni = xml.indexOf(replaceMarker)) >= 0) {
            posFin = xml.indexOf(")", posIni);
            className = xml.substring(posIni + replaceMarker.length, posFin);
            style = this.getCSSStyle(className);
            toReplace = xml.substring(posIni, posFin + 1);
            xml = xml.replace(toReplace, style);
        }
        return xml;
    }

    this.getCSSStyle = function (className) {
        if (CSSStyles[className] != undefined) return CSSStyles[className];
        else {
            styleObj = this.loadCSSStyle(className);
            if (styleObj != undefined) {
                styleTransformed = this.transformCSSStyle(styleObj)
                CSSStyles[className] = styleTransformed;
                return styleTransformed;
            } else return "";
        }
    }

    this.loadCSSStyle = function (className) {
        var css;
        var cssName = gx.theme + ".css";
        var cssFound = false;
        var styleObj;
        for (var i = 0; i < document.styleSheets.length; i++)
        if (document.styleSheets[i].href.indexOf(cssName) >= 0) {
            cssFound = true;
            css = document.styleSheets[i];
            break;
        }
        if (cssFound) if (css.cssRules) crossRules = css.cssRules;
        else if (css.rules) crossRules = css.rules;
        for (var i = 0; i < crossRules.length; i++) {
            var rule = crossRules[i];
            if (rule.selectorText != undefined)
                if (rule.selectorText.toLowerCase().indexOf("." + className.toLowerCase()) == 0) 
                    styleObj = rule.style;
        }
        return styleObj;
    }

    this.transformCSSStyle = function (styleObj) {
        var strAux = "";
        if (styleObj.color != "") strAux += (strAux == "" ? "" : ";") + "color:" + this.parseCSSColor(styleObj.color);
        if (styleObj.backgroundColor != "") strAux += (strAux == "" ? "" : ";") + "backgroundColor:" + this.parseCSSColor(styleObj.backgroundColor);
        if (styleObj.borderStyle != "") strAux += (strAux == "" ? "" : ";") + "borderStyle:" + styleObj.borderStyle;
        if (styleObj.borderWidth != "") strAux += (strAux == "" ? "" : ";") + "borderThickness:" + styleObj.borderWidth.replace("pt", "").replace("px", "");
        if (styleObj.borderColor != "") strAux += (strAux == "" ? "" : ";") + "borderColor:" + this.parseCSSColor(styleObj.borderColor);
        //if (styleObj.paddingLeft != "")        strAux += (strAux == "" ? "" : ";") + "paddingLeft:"      + styleObj.paddingLeft.replace("pt", "").replace("px", "");
        //if (styleObj.paddingRight != "")       strAux += (strAux == "" ? "" : ";") + "paddingRight:"     + styleObj.paddingRight.replace("pt", "").replace("px", "");
        //if (styleObj.paddingTop != "")         strAux += (strAux == "" ? "" : ";") + "paddingTop:"       + styleObj.paddingTop.replace("pt", "").replace("px", "");
        //if (styleObj.paddingBottom != "")      strAux += (strAux == "" ? "" : ";") + "paddingBottom:"    + styleObj.paddingBottom.replace("pt", "").replace("px", "");
        if (styleObj.fontFamily != "") strAux += (strAux == "" ? "" : ";") + "fontFamily:" + styleObj.fontFamily.replace(/"/g, "").replace(/'/g, "");
        if (styleObj.fontSize != "") strAux += (strAux == "" ? "" : ";") + "fontSize:" + styleObj.fontSize.replace("pt", "").replace("px", "");
        if (styleObj.fontWeight != "") strAux += (strAux == "" ? "" : ";") + "fontWeight:" + styleObj.fontWeight;
        if (styleObj.fontStyle != "") strAux += (strAux == "" ? "" : ";") + "fontStyle:" + styleObj.fontStyle;
        if (styleObj.textDecoration != "") strAux += (strAux == "" ? "" : ";") + "textDecoration:" + styleObj.textDecoration;
        //if (styleObj.textAlign != "")          strAux += (strAux == "" ? "" : ";") + "textAlign:"        + styleObj.textAlign;

        // Faltan
        //    backgroundAlpha   = "1"     // 0 .. 1
        //    cornerRadius    = "0"       // 0 .. n
        //    dropShadowEnabled = "true"    // true | false
        //    shadowDirection   = "right"   // left | right | center
        return strAux;
    }

    // ---------------------------------------- Table ----------------------------------------

    this.tryToRenderTable = function () {
        this.tryToRenderPivotTable(); // same render
    }

    // --------------------------------------Pivot Table -------------------------------------

    this.GetPivotURLControl = function () {
        switch (this.GenType) {
            case "C#":
                return "QueryViewer/xpertPivot/xpertPivot.swf"

            case "Java":
                return this.GetJavaBaseUrl(true) + "QueryViewer/xpertPivot/xpertPivot.swf";
            case "Ruby":
                return "QueryViewer/xpertPivot/xpertPivot.swf"
            default:
                return "QueryViewer/xpertPivot/xpertPivot.swf"
        }
    }

    this.BelongAnyAutoRefreshGroup = function () {
        var my_refreshgroup = this.AutoRefreshGroup;
        return ((my_refreshgroup != undefined) && (my_refreshgroup != null) && (my_refreshgroup != "") && (this.trim(my_refreshgroup) != ""));
    }

    this.tryToRenderPivotTable = function () {
        var errMsg = "";

        // Si hay que traer el estado salvado de la metadata se trae
        if ((this.RememberLayout) && (!this.isdynamicrefresh) && (this.BelongAnyAutoRefreshGroup())) this.loadQueryAxesAndParametersState();

        // Ejecuto el primer servicio y verifico que no haya habido error
        var d1 = new Date();
        var t1 = d1.getTime();

        if (this.RenderHtmlAndJs()) {
            this.calculatePivottableMetadata_JS((function (resText) {
                this.hideActivityIndicator();
                var d2 = new Date();
                var t2 = d2.getTime();
                if (!this.anyError(resText) || this.debugServices) {
                    this.pivotTableMetadata = resText;
                    if ((this.ServerPagingForTable) && (this.RealType == "Table") && (!gx.lang.gxBoolean(this.IsExternalQuery))) {
                    	var previusStateSave = false;
                    	if (OAT.getStateWhenServingPaging) {
                    		previusStateSave = OAT.getStateWhenServingPaging( this.userControlId()  + '_' + this.ObjectName, this.ObjectName)
                    	}
                    	if ((!previusStateSave) || (!this.RememberLayout)){
                            if (this.UseRecordsetCache && this.RecordsetChanged())
                                this.getRecordsetCacheKey((function (recordsetCacheKey) {
                                    this.RecordsetCacheOldKey = (this.RecordsetCacheActualKey ? this.RecordsetCacheActualKey : "");
                                    this.RecordsetCacheActualKey = recordsetCacheKey;
                                    this.callServiceGetPageDataForTable(t1, t2);
                                }).closure(this));
                            else
                               this.callServiceGetPageDataForTable(t1, t2);
                        } else {
                        	this.hideActivityIndicator();
                        	this.renderPivotTable_JS("");
                        }
					}																
                    else if ((this.ServerPagingForPivotTable) && (this.RealType == "PivotTable") && (!gx.lang.gxBoolean(this.IsExternalQuery))) {
                    	if (this.debugServices){
                    		this.getPageDataForPivotTable((function (resXML) {
                    			var d3 = new Date();
                        	    var t3 = d3.getTime();
                        	    this.hideActivityIndicator();
                        	    if (!this.anyError(resXML)){
                        	    	this.pivotTablePageData = resXML;
                        	    	this.saveQueryAxesAndParametersState();
                        	    	this.renderPivotServices(this.pivotTableMetadata, this.pivotTablePageData, t2 - t1, t3 - t2);
                        	    } else {
                        	      	errMsg = this.getErrorFromText(resXML);
                        	        this.renderError(errMsg);
                        	    }
                    		}).closure(this), [1, this.PageSize, true, [], [], [], true ]);		
                    	} else {
                    		var previusStateSave = false;
                    		if (OAT.getStateWhenServingPaging) {
                    			previusStateSave = OAT.getStateWhenServingPaging( this.userControlId()  + '_' + this.ObjectName, this.ObjectName)
                    		}
                    	/*if ((!previusStateSave) || (!this.RememberLayout)){   
                        	this.getPageDataForPivotTable((function (resXML) {
                        	    var d3 = new Date();
                        	    var t3 = d3.getTime();
                        	    this.hideActivityIndicator();
                        	    if (!this.anyError(resText) || this.debugServices) {
                        	        this.pivotTablePageData = resText;
                        	        this.saveQueryAxesAndParametersState();
                        	        if (this.debugServices)
                        	            this.renderPivotServices(this.pivotTableMetadata, this.pivotTablePageData, t2 - t1, t3 - t2);
                        	        else {
                        	            this.renderPivotTable_JS(resXML);
                        	        }
                        	    } else {
                        	        errMsg = this.getErrorFromText(resText);
                        	        this.renderError(errMsg);
                        	    }
                        	}).closure(this), [1, this.PageSize, true, [], [], [] ]);
                        } else {*/
                        	this.hideActivityIndicator();
                        	this.renderPivotTable_JS("");
                        //}
                        }
					}																
                    else {
                        this.calculatePivottableData_JS((function (resText) {
                            this.hideActivityIndicator();
                            var d3 = new Date();
                            var t3 = d3.getTime();
                            if (!this.anyError(resText) || this.debugServices) {
                                this.pivotTableData = resText;
                                this.saveQueryAxesAndParametersState();
                                if (this.debugServices)
                                    this.renderPivotServices(this.pivotTableMetadata, this.pivotTableData, t2 - t1, t3 - t2);
                                else
                                    this.renderPivotTable_JS();
                            } else {
                                errMsg = this.getErrorFromText(resText);
                                this.renderError(errMsg);
                            }
                        }).closure(this));
                    }
                } else {
                    errMsg = this.getErrorFromText(resText);
                    this.renderError(errMsg);
                }
            }).closure(this));
        } else {
            this.calculatePivottableMetadata((function (resText) {
                this.hideActivityIndicator();
                var d2 = new Date();
                var t2 = d2.getTime();
                if (!this.anyError(resText) || this.debugServices) {
                    this.pivotTableMetadata = resText;
                    if (this.isdynamicrefresh) this.SetMetadataPivotStringXMLState(this.pivotTableMetadata);

                    // Ejecuto el segundo servicio y verifico que no haya habido error
                    this.calculatePivottableData((function (resText) {
                        this.hideActivityIndicator();
                        var d3 = new Date();
                        var t3 = d3.getTime();
                        if (!this.anyError(resText) || this.debugServices) {
                            this.pivotTableData = resText;
                            this.saveQueryAxesAndParametersState();
                            if (this.debugServices) this.renderPivotServices(this.pivotTableMetadata, this.pivotTableData, t2 - t1, t3 - t2);
                            else this.renderPivotTable_JS();
                        } else {
                            errMsg = this.getErrorFromText(resText);
                            this.renderError(errMsg);
                        }
                    }).closure(this));
                } else {
                    errMsg = this.getErrorFromText(resText);
                    this.renderError(errMsg);
                }
            }).closure(this));
        }
    }

	this.RecordsetChanged =  function()
	{
		var recordsetChanged;
		var runtimeParametersJSON = this.RuntimeParametersJSON();
		var queryInfoSQLSentence = "";
		var queryInfoParametersJSON = "";
		if (this.QueryInfo != "")
		{
			var queryInfo = eval('(' + this.QueryInfo + ')');
			queryInfoSQLSentence = queryInfo.SQLSentence;
			queryInfoParametersJSON = gx.json.serializeJson(queryInfo.Parameters);
		}	
		if (this.ObjectName != this._ObjectName || this.ObjectType != this._ObjectType || this.ObjectId != this._ObjectId || runtimeParametersJSON != this._runtimeParametersJSON || queryInfoSQLSentence != this._queryInfoSQLSentence || queryInfoParametersJSON != this._queryInfoParametersJSON)
		{
			this._ObjectName 			  = this.ObjectName;
			this._ObjectType 			  = this.ObjectType;
			this._ObjectId   			  = this.ObjectId;
			this._runtimeParametersJSON   = runtimeParametersJSON;
			this._queryInfoSQLSentence    = queryInfoSQLSentence;
			this._queryInfoParametersJSON = queryInfoParametersJSON;
			return true;
		}
		else
			return false;
	}
	
	this.callServiceGetPageDataForTable = function(t1, t2)
	{
		this.getPageDataForTable((function (resXML) {
			this.hideActivityIndicator();
			var d3 = new Date();
			var t3 = d3.getTime();
			if (!this.anyError(resXML) || this.debugServices) {
				this.tablePageData = resXML;
				this.saveQueryAxesAndParametersState();
				if (this.debugServices)
					this.renderPivotServices(this.pivotTableMetadata, this.tablePageData, t2 - t1, t3 - t2);
				else {
					this.renderPivotTable_JS(resXML);
				}
			} else {
				errMsg = this.getErrorFromText(resXML);
				this.renderError(errMsg);
			}
		}).closure(this), [1, this.PageSize, true, "", "", [] ]);
	}
	
	this.pivotParams = {};
	
	this.renderPivotTable_JS = function(data) {
        if (this.RenderHtmlAndJs()) {
            if (!this.AutoResize) {
                this.getContainerControl().style.height = gx.dom.addUnits(this.Height);
                this.getContainerControl().style.width = gx.dom.addUnits(this.Width);
            } else {
                switch (this.AutoResizeType) {
                    case "Both":
                        // do nothing
                        break;
                    case "Vertical":
                    	if (!gx.util.browser.isIE()){
                        	this.getContainerControl().style.width = gx.dom.addUnits(this.Width);
                        } 
                        break;
                    case "Horizontal":
                        this.getContainerControl().style.height = gx.dom.addUnits(this.Height);
                        break;
                }
            }
            if ( (!this.AutoResize) || ((this.AutoResize) && (this.AutoResizeType == "Vertical") && (!gx.util.browser.isIE())) ){ 
            	this.getContainerControl().style.overflow = 'auto';	
            } 
            var page = this.userControlId() + "_" + this.ObjectName + "_pivot_page"; //this.ControlName +
            this.pivotParams.page = page;
            var content = this.userControlId() + "_" + this.ObjectName + "_pivot_content"; //this.ControlName
            this.pivotParams.content = content;
            var chart = this.userControlId() + "_" + this.ObjectName + "_pivot_chart"; //this.ControlName
            this.pivotParams.chart = chart;
            if (this.RealType == "PivotTable") {
                var buff = '<div id="' + page + '"></div><div id="' + content + '"></div><div id="' + chart + '"></div>';
                document.getElementById(this.ContainerName).innerHTML = buff.toString();
            } else {
                if (this.RealType == "Table") {
                    document.getElementById(this.ContainerName).innerHTML = "";
                }
            }
            this.pivotParams.container = this.getContainerControl();
            this.pivotParams.RealType = this.RealType;
            this.pivotParams.ObjectName = this.ObjectName;
            this.pivotParams.ControlName = this.ControlName;
            this.pivotParams.PageSize = ((this.Paging) && (this.Paging!="false")) ? this.PageSize : undefined;
            this.pivotParams.metadata = this.pivotTableMetadata;
            this.pivotParams.data = (this.pivotTableData) ? this.pivotTableData : data;
            this.pivotParams.UcId = this.userControlId();
            this.pivotParams.ShrinkToFit = queryself.ShrinkToFit;
			this.pivotParams.DisableColumnSort = queryself.DisableColumnSort;
			this.pivotParams.RememberLayout = this.RememberLayout;
			this.pivotParams.ShowDataLabelsIn = this.ShowDataLabelsIn;
			this.pivotParams.ServerPaging = this.ServerPagingForTable && (!gx.lang.gxBoolean(this.IsExternalQuery));
			this.pivotParams.ServerPagingPivot = this.ServerPagingForPivotTable && (!gx.lang.gxBoolean(this.IsExternalQuery));;
			this.pivotParams.ServerPagingCacheSize = 0
			this.pivotParams.UseRecordsetCache = this.UseRecordsetCache;
        	if (OAT.Loader != undefined){	
            	renderJSPivot(queryself.pivotParams, QueryViewerCollection, queryself);
            } else {
            	OAT.Loader.addListener(function () {
                    renderJSPivot(this.pivotParams, QueryViewerCollection, queryself);
                });
            }
        } else {
            var UCId = this.userControlId();
            var cssName = "xpertCube.css";
            this.Language = (gx.languageCode.toLowerCase() == "german" ? "ger" : gx.languageCode);
            var emptyCell = "";
            var backColor = "#FFFFFF";
            var container = this.getContainerControl();
            var autorefresh = true;
            this.verticalPageSize = (gx.lang.gxBoolean(this.Paging) ? this.PageSize : 0);
            this.horizontalPageSize = (gx.lang.gxBoolean(this.Paging) ? this.PageSize : 0);
            var urlControl = this.GetPivotURLControl();

            swf(UCId, urlControl, this.Left, this.Top, this.fixSize(this.Width), this.fixSize(this.Height), '',

            function (ctl) {
                if (ctl.configServices) ctl.configServices('', '', '', cssName, QueryViewerCollection[ctl.id].Language, emptyCell, (QueryViewerCollection[ctl.id].RealType == "Table"), autorefresh, QueryViewerCollection[ctl.id].verticalPageSize, QueryViewerCollection[ctl.id].horizontalPageSize, 'GET', false, !gx.lang.gxBoolean(QueryViewerCollection[ctl.id].HideMeasuresPropertiesBox), QueryViewerCollection[ctl.id].ExportHeader, QueryViewerCollection[ctl.id].ExportHeaderHeight, !gx.lang.gxBoolean(QueryViewerCollection[ctl.id].DisableExpandCollapse), true, !gx.lang.gxBoolean(QueryViewerCollection[ctl.id].DisableColumnSort))
                if (ctl.setContextMenuOptions) ctl.setContextMenuOptions(true, true, true, gx.lang.gxBoolean(QueryViewerCollection[ctl.id].ExportToXML), gx.lang.gxBoolean(QueryViewerCollection[ctl.id].ExportToHTML), gx.lang.gxBoolean(QueryViewerCollection[ctl.id].ExportToXLSX), gx.lang.gxBoolean(QueryViewerCollection[ctl.id].ExportToXLS), gx.lang.gxBoolean(QueryViewerCollection[ctl.id].ExportToPDF))
                if (ctl.configStyle) ctl.configStyle(QueryViewerCollection[ctl.id].FontFamily, QueryViewerCollection[ctl.id].FontSize, QueryViewerCollection[ctl.id].getHtmlColor(QueryViewerCollection[ctl.id].FontColor), backColor, QueryViewerCollection[ctl.id].FontFamily, QueryViewerCollection[ctl.id].FontSize, QueryViewerCollection[ctl.id].getHtmlColor(QueryViewerCollection[ctl.id].FontColor), backColor, QueryViewerCollection[ctl.id].FontFamily, QueryViewerCollection[ctl.id].FontSize, QueryViewerCollection[ctl.id].getHtmlColor(QueryViewerCollection[ctl.id].FontColor), backColor, QueryViewerCollection[ctl.id].FontFamily, QueryViewerCollection[ctl.id].FontSize, QueryViewerCollection[ctl.id].getHtmlColor(QueryViewerCollection[ctl.id].FontColor), backColor, QueryViewerCollection[ctl.id].FontFamily, QueryViewerCollection[ctl.id].FontSize, QueryViewerCollection[ctl.id].getHtmlColor(QueryViewerCollection[ctl.id].FontColor), backColor)
                if (ctl.loadFrom) ctl.loadFrom(QueryViewerCollection[ctl.id].ObjectName); // No está cargando de acá. Es solo para setear el nombre correcto en los diálogos de exportación
                if (ctl.setInitialMetadata) ctl.setInitialMetadata(QueryViewerCollection[ctl.id].pivotTableMetadata);
                if (ctl.setData) ctl.setData(QueryViewerCollection[ctl.id].pivotTableData);
                if (ctl.setColumnAutoShrink) ctl.setColumnAutoShrink(gx.lang.gxBoolean(QueryViewerCollection[ctl.id].ShrinkToFit));
                if (ctl.refresh) ctl.refresh();
            }, {
                itemClick: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onItemClickEvent(info.data, false);
                },
                itemDoubleClick: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onItemClickEvent(info.data, true);
                },
                itemExpand: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onItemExpandCollapseEvent(info.data, false);
                },
                itemCollapse: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onItemExpandCollapseEvent(info.data, true);
                },
                dragAndDrop: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onDragAndDropEvent(info.data);
                },
                aggregatorChanged: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onAggregatorChangedEvent(info.data);
                },
                valuesChanged: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onValuesChangedEvent(info.data);
                },
                popupClick: function (ctl, info) {},
                onResized: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onResizePivotEvent(ctl, info);
                },
                onRestoreDefaultView: function (ctl, info) {
                    QueryViewerCollection[ctl.id].onRestoreDefaultView(info.data);
                }
            },
            container);
        }
    }

    this.getPivotTableDataURL = function () {
		return this.getServiceURL("getdataforpivottable");
    }

    this.getPivotTableMetadataURL = function () {
		return this.getServiceURL("getmetadataforpivottable");
    }

    this.getAttributeValuesURL = function () {
		return this.getServiceURL("getattributevalues");
    }

    this.getPageDataForTableURL = function () {
		return this.getServiceURL("getpagedatafortable");
    }

    this.getPageDataForPivotTableURL = function () {
		return this.getServiceURL("getpagedataforpivottable");
    }
	
    this.renderPivotServices = function (xml1, xml2, time1, time2) {
        var xml1Encoded = gx.html.encode(xml1, true, true);
        var xml2Encoded = gx.html.encode(xml2, true, true);
        this.AdjustSize();
        var buffer1 = "<div style=\"overflow:auto; border-right: silver thin solid; border-top: silver thin solid; border-left: silver thin solid; border-bottom: silver thin solid; width: " + this.Width + "px; height: " + parseInt(this.Height) / 2 + "px; font-size: " + this.FontSize + "px; color: " + this.getHtmlColor(this.FontColor) + "; font-family: " + this.FontFamily + "\">" + xml1Encoded + "</div>";
        buffer1 += "Time: " + time1 / 1000 + " seconds";
        var buffer2 = "<div style=\"overflow:auto; border-right: silver thin solid; border-top: silver thin solid; border-left: silver thin solid; border-bottom: silver thin solid; width: " + this.Width + "px; height: " + parseInt(this.Height) / 2 + "px; font-size: " + this.FontSize + "px; color: " + this.getHtmlColor(this.FontColor) + "; font-family: " + this.FontFamily + "\">" + xml2Encoded + "</div>";
        buffer2 += "Time: " + time2 / 1000 + " seconds";
        this.setHtml(buffer1 + buffer2);
    }

    // ---------------------------------------- Chart ----------------------------------------


    this.applyFiltersInXMLChart = function (ChartXml) {
        var xmlDoc;
        if (!this.BelongAnyAutoRefreshGroup()) return ChartXml;

        if ((this.Axes == null) || (this.Axes == undefined) || (this.Axes == 0) || (this.Axes.length == 0)) return ChartXml;

        xmlDoc = this.xmlDocument(ChartXml);

        if (this.getSingleElementByTagName(xmlDoc.documentElement, "dataset") == null) return this.applyFiltersInXMLChartSingleSerie(xmlDoc);
        else return this.applyFiltersInXMLChartMultiSerie(xmlDoc);
    }

    this.trim = function (str) {
        if (!str || typeof str != 'string') return null;
        return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
    }

    this.GetAxisByName = function (Axes, AxisTitle) {
        // precondicion el axis esta en la collectiion de axis
        encontre = false;
        i = 0;
        while ((!encontre) && (i < Axes.length)) {

            encontre = this.trim(Axes[i].Title) == this.trim(AxisTitle);
            if (!encontre) i++;
        }
        if (encontre) return Axes[i];
        else return null;
    }

    this.GetCategoriesChartOrder = function (Axes, categoriesStr) {
        var categories = {};
        var Axis;
        var h1 = 0;
        for (h = 0; h < categoriesStr.length; h++) {
            Axis = this.GetAxisByName(Axes, categoriesStr[h]);
            if (Axis != null) {
                categories[h1] = Axis;
                h1++;
            }
        }
        return categories;
    }

    this.IsValidValue = function (value, axis) {

        if ((axis.Filter == null) || (axis.Filter == undefined)) return false;


        if ((axis.Filter.Values == null) || (axis.Filter.Values == undefined)) return false;

        if (axis.Filter.Values.length == 0) return false;


        encontre = false;
        i1 = 0;

        while ((!encontre) && (i1 < axis.Filter.Values.length)) {
            encontre = this.trim(value) == this.trim(axis.Filter.Values[i1]);
            if (!encontre) i1++;

        }
        return encontre;
    }

    this.IsValidRow = function (axesvalue, categories) {
        var values = axesvalue.split(",");
        isvalid = true;
        i2 = 0;
        while ((isvalid) && (categories[i2] != undefined)) {
            isvalid = this.IsValidValue(values[i2], categories[i2]);
            i2++;
        }
        return isvalid;
    }

    this.applyFiltersInXMLChartSingleSerie = function (xmlDoc) {
        var xaxis = xmlDoc.documentElement.getAttribute("xAxisName");
        var yaxis = xmlDoc.documentElement.getAttribute("yAxisName");
        var categoriesStr = xaxis.split(",");
        var categories = this.GetCategoriesChartOrder(this.Axes, categoriesStr);

        var metadata = this.GetMetadataFromChart(xmlDoc);
        var xml = "";

        var xml_index = [];
        var xml_values = [];
        var xml_color = [];
        var xml_xValues = [];
        xml_i = 0;

        xml = "<graph xAxisName=" + '"' + xaxis + '"' + " yAxisName=" + '"' + yaxis + '"' + " baseFont=" + '"' + metadata["baseFont"] + '"' + " baseFontSize=" + '"' + metadata["baseFontSize"] + '"' + " baseFontColor=" + '"' + metadata["baseFontColor"] + '"' + " rotateNames=" + '"' + metadata["rotateNames"] + '"' + " showValues=" + '"' + metadata["showValues"] + '"' + " numberSuffix=" + '"' + metadata["numberSuffix"] + '"' + " decimalPrecision=" + '"' + metadata["decimalPrecision"] + '"' + " yAxisMinValue=" + '"' + metadata["yAxisMinValue"] + '"' + " yAxisMaxValue=" + '"' + metadata["yAxisMaxValue"] + '"' + " showNames=" + '"' + metadata["showNames"] + '"' + " formatNumberScale=" + '"' + metadata["formatNumberScale"] + '"' + ">";

        var setnodes = this.getMultipleElementsByTagName(xmlDoc.documentElement, "set");

        for (u = 0; u < setnodes.length; u++) {
            var name = setnodes[u].getAttribute("hoverText");
            var value = setnodes[u].getAttribute("value");
            var color = setnodes[u].getAttribute("color");
            var xValue = setnodes[u].getAttribute("xValue");

            if ((xValue==undefined)){
                xValue = name; //hover text, axis
            }
            if (this.IsValidRow(xValue, categories)) {
                if (xml_values[name] == undefined) {
                    // inserto valor nuevo
                    xml_index[xml_i] = name;
                    xml_values[name] = value;
                    xml_color[name] = color;
                    xml_xValues[name] = xValue;
                    xml_i++;
                } else {
                    //adiciono valor al existente
                    val = xml_values[name];
                    number_val = parseFloat(val.replace(",", "."));
                    newval = value;
                    number_newval = parseFloat(newval.replace(",", "."));
                    number_total = number_val + number_newval;
                    xml_values[name] = number_total + '';
                }
            }
        }

        var maxLength = this.GetMaxCategoryLabelLength(xml_index.length);
        for (k = 0; k < xml_index.length; k++) {
            n = xml_index[k];
            var nameReduced = this.GetCategoryLabel(n, maxLength);
            xml += "<set name=" + '"' + nameReduced + '"' + " value=" + '"' + xml_values[n] + '"' + " color=" + '"' + xml_color[n] + '"' + " hoverText=" + '"' + n + '"' + ' xValue="' + xml_xValues[n] + '"></set>';
        }

        xml += "</graph>";
        return xml;
    }

    this.applyFiltersInXMLChartMultiSerie = function (xmlDocument) {

        var xaxis = xmlDocument.documentElement.getAttribute("xAxisName");
        var categoriesStr = xaxis.split(",");
        var categories = this.GetCategoriesChartOrder(this.Axes, categoriesStr);

        var metadata = this.GetMetadataFromChart(xmlDocument);

        var xml = "<graph xAxisName=" + '"' + xaxis + '"' + " baseFont=" + '"' + metadata["baseFont"] + '"' + " baseFontSize=" + '"' + metadata["baseFontSize"] + '"' + " baseFontColor=" + '"' + metadata["baseFontColor"] + '"' + " rotateNames=" + '"' + metadata["rotateNames"] + '"' + " showValues=" + '"' + metadata["showValues"] + '"' + " numberSuffix=" + '"' + metadata["numberSuffix"] + '"' + " decimalPrecision=" + '"' + metadata["decimalPrecision"] + '"' + " yAxisMinValue=" + '"' + metadata["yAxisMinValue"] + '"' + " yAxisMaxValue=" + '"' + metadata["yAxisMaxValue"] + '"' + " formatNumberScale=" + '"' + metadata["formatNumberScale"] + '"' + ">";

        var categoriesnode = this.getSingleElementByTagName(xmlDocument.documentElement, "categories");
        var categoriescollection = this.getMultipleElementsByTagName(categoriesnode, "category");

        var categories_by_key = [];
        var categories_by_index = [];
        var xvalues_by_index = [];
        for (u2 = 0; u2 < categoriescollection.length; u2++) {
            var name = categoriescollection[u2].getAttribute("hoverText");
            var xValue = categoriescollection[u2].getAttribute("xValue");
            if (this.IsValidRow(xValue, categories)) {
                if (categories_by_key[name] == undefined) {
                    //valor nuevo
                    var valuearryay = [];
                    valuearryay.push(u2);
                    categories_by_key[name] = valuearryay;
                    categories_by_index.push(name);
                    xvalues_by_index.push(xValue);
                } else {
                    // valor ya existente
                    var valuearryay = categories_by_key[name];
                    valuearryay.push(u2);
                    categories_by_key[name] = valuearryay;
                }
            }
        }

        xml += "<categories>";
        var maxLength = this.GetMaxCategoryLabelLength(categories_by_index.length);
        for (c1 = 0; c1 < categories_by_index.length; c1++) {
            var namevalue = categories_by_index[c1];
            var nameReduced = this.GetCategoryLabel(namevalue, maxLength);
            var xValue = xvalues_by_index[c1];
            xml += "<category name=" + '"' + nameReduced + '" hoverText="' + namevalue + '"' + ' xValue="' + xValue + '"></category>';
        }
        xml += "</categories>";

        var datasetcollection = this.getMultipleElementsByTagName(xmlDocument.documentElement, "dataset");

        for (u3 = 0; u3 < datasetcollection.length; u3++) {

            var dataset_u3 = datasetcollection[u3];
            var seriesName = dataset_u3.getAttribute("seriesName");
            var parentYAxis = dataset_u3.getAttribute("parentYAxis");
            var color = dataset_u3.getAttribute("color");


            xml += "<dataset seriesName=" + '"' + seriesName + '"' + " parentYAxis=" + '"' + parentYAxis + '"' + " color=" + '"' + color + '"' + " >"
            var setvaluescollection = this.getMultipleElementsByTagName(dataset_u3, "set");

            for (c1 = 0; c1 < categories_by_index.length; c1++) {

                var namevalue = categories_by_index[c1];
                var array_index = categories_by_key[namevalue];


                var totalserie = 0;
                for (h3 = 0; h3 < array_index.length; h3++) {

                    var index = array_index[h3];
                    var newvalue = setvaluescollection[index].getAttribute("value");
                    var valuefloat = parseFloat(newvalue.replace(",", "."));
                    totalserie = totalserie + valuefloat;
                }

                xml += "<set value=" + '"' + totalserie + '"' + "></set>";
            }

            xml += "</dataset>";
        }

        xml += "</graph>"
        return xml;

    }

    this.GetMaxCategoryLabelLength = function (totLabels) // Rutina sacada de los servicios, del procedure gxpl_GetMaxCategoryLabelLength
    {
        if (this.RealChartType != "Bar" && this.RealChartType != "StackedBar" && this.RealChartType != "Pie" && this.RealChartType != "Pie3D" && this.RealChartType != "Doughnut" && this.RealChartType != "Funnel" && this.XAxisLabels == "Horizontally") return parseInt((parseInt(this.Width) / totLabels) * (200 / 1680) * (10 / parseInt(this.FontSize)));
        else return 0;
    }

    this.GetCategoryLabel = function (label, maxLength) // Rutina sacada de los servicios, de parte del procedure gxpl_GetCategoryValue
    {
        if (maxLength != 0 && label.length > maxLength) {
            if (maxLength > 3) return label.substr(0, maxLength - 3) + "...";
            else return "";
        } else return label;
    }

    this.DecordeValuesCollection = function (ValuesCollection) {

        var result = [];
        var i = 0;

        for (i = 0; i < ValuesCollection.length; i++) {
            result.push(decodeURIComponent(ValuesCollection[i]));
        }
        return result;


    }
    this.GetAxesCollectionFromJSON = function (AxesJSON) {
        var axesresult = [];
        var i = 0;

        for (i = 0; i < AxesJSON.RuntimeFields.length; i++) {
            var axis = {};
            axis.Name = decodeURIComponent(AxesJSON.RuntimeFields[i].Name);
            axis.Title = decodeURIComponent(AxesJSON.RuntimeFields[i].Caption);
            axis.Filter = {};
            if (AxesJSON.RuntimeFields[i].Filter) {
                axis.Filter.Type = AxesJSON.RuntimeFields[i].Filter.Type;
                if (AxesJSON.RuntimeFields[i].Filter.Values != undefined) {
                    axis.Filter.Values = this.DecordeValuesCollection(AxesJSON.RuntimeFields[i].Filter.Values);
                }
            }
            if (AxesJSON.RuntimeFields[i].Axis.Type == "Rows") {
                axis.Type = "Row";
            } else {
                if (AxesJSON.RuntimeFields[i].Axis.Type == "Columns") {
                    axis.Type = "Column";
                } else {

                    if (AxesJSON.RuntimeFields[i].Axis.Type == "Pages") {
                        axis.Type = "Page";
                    } else {
                        if (AxesJSON.RuntimeFields[i].Axis.Type == "Hidden") {
                            axis.Type = "NotShow";
                        } else {
                            axis.Type = "Data";
                        }

                    }
                }
            }

            if ((axis.Filter != undefined) && (axis.Filter.Values == undefined)) {
                if (axis.Filter.Type == "ShowSomeValues") axis.Filter.Type = "ShowAllValues"
            }

            if ((axis.ExpandCollapse != undefined) && (axis.ExpandCollapse.Values == undefined)) {
                if (axis.ExpandCollapse.Type == "ExpandSomeValues") axis.ExpandCollapse.Type = "CollapseAllValues"
            }

            if ((axis.AxisOrder != undefined) && (axis.AxisOrder.Values == undefined)) {
                if (axis.AxisOrder.Type == "Custom") axis.AxisOrder.Type = "Ascending"
            }

            axesresult.push(axis);
        } // for

        return axesresult;

    }

    this.GetParameterCollectionFromJSON = function (ParametersJSON) {
        var parametersresult = [];
        var i = 0;
        for (i = 0; i < ParametersJSON.RuntimeParameters.length; i++) {
            var p = {};
            p.Name = decodeURIComponent(ParametersJSON.RuntimeParameters[i].Name);
            p.Value = decodeURIComponent(ParametersJSON.RuntimeParameters[i].Value);
            parametersresult.push(p);
        }

        return parametersresult;
    }


    this.loadQueryAxesAndParametersState = function () {
        if (this.BelongAnyAutoRefreshGroup()) {
            var path = window.location.pathname;

            var cookieparameterID = path + "_" + this.userControlId() + "_" + this.ObjectId + "_" + this.ObjectName + "Parameters";
            var cookieAxesID = path + "_" + this.userControlId() + "_" + this.ObjectId + "_" + this.ObjectName + "Axes";
            var parametersJSONString = this.readCookie(cookieparameterID);
            var AxesJSONString = this.readCookie(cookieAxesID);

            if (!((parametersJSONString == null) || (parametersJSONString == undefined) || (this.trim(parametersJSONString)) == "{}")) {
                var parametersJSON = eval('(' + parametersJSONString + ')');
                this.Parameters = this.GetParameterCollectionFromJSON(parametersJSON);
            }

            if (!((AxesJSONString == null) || (AxesJSONString == undefined) || (this.trim(AxesJSONString) == "{}"))) {
                var AxesJSON = eval('(' + AxesJSONString + ')');
                this.Axes = this.GetAxesCollectionFromJSON(AxesJSON)
            }

        }

    }

    this.saveQueryAxesAndParametersState = function () {
        if (this.BelongAnyAutoRefreshGroup()) {
            var parametersJSON = "{" + this.RuntimeParametersJSON() + "}";
            var fieldJSON = "{" + this.RuntimeFieldsJSON(false) + "}";
            var path = window.location.pathname;

            var cookieparameterID = path + "_" + this.userControlId() + "_" + this.ObjectId + "_" + this.ObjectName + "Parameters";
            var cookieAxesID = path + "_" + this.userControlId() + "_" + this.ObjectId + "_" + this.ObjectName + "Axes";
            var days = 365;

            this.createCookie(cookieparameterID, parametersJSON, days);
            this.createCookie(cookieAxesID, fieldJSON, days);
        }
    }

    this.tryToRenderChart = function () {
        var errMsg = "";

        // Si hay que traer el estado salvado de la metadata se trae
        if ((this.RememberLayout) && (!this.isdynamicrefresh) && (this.BelongAnyAutoRefreshGroup())) this.loadQueryAxesAndParametersState();
        
        if (this.RenderHtmlAndJs()) {
            this.getSeriesCount_JS((function (resText) {
                this.hideActivityIndicator();
                if (!this.anyError(resText) || this.debugServices) {
                    this.chartSeriesCount = resText;
                    // Ejecuto el segundo servicio y verifico que no haya habido error
                    var d1 = new Date();
                    var t1 = d1.getTime();
                    this.getChartData_JS((function (resText) {
                        this.hideActivityIndicator();
                        var d2 = new Date();
                        var t2 = d2.getTime();
                        if (!this.anyError(resText) || this.debugServices) {
                            this.chartXmlData = this.applyFiltersInXMLChart(resText);
                            //this.saveQueryAxesAndParametersState();
                            if (this.debugServices) 
                                this.renderChartServices(this.chartXmlData, t2 - t1);
                            else {
                                    this.RenderChartAndSetAutoresize();
                            }
                        } else {
                            errMsg = this.getErrorFromText(resText);
                            this.renderError(errMsg);
                        }
                    }).closure(this));
                } else {
                    errMsg = this.getErrorFromText(resText);
                    this.renderError(errMsg);
                }				
            }).closure(this)); 
        } else {
            // Ejecuto el primer servicio y verifico que no haya habido error
            var resText = this.getSeriesCount((function (resText) {
                this.hideActivityIndicator();
                if (!this.anyError(resText) || this.debugServices) {
                    this.chartSeriesCount = resText;
                    // Ejecuto el segundo servicio y verifico que no haya habido error
                    var d1 = new Date();
                    var t1 = d1.getTime();
                    this.getChartData((function (resText) {
                        this.hideActivityIndicator();
                        var d2 = new Date();
                        var t2 = d2.getTime();
                        if (!this.anyError(resText) || this.debugServices) {
                            this.chartXmlData = this.applyFiltersInXMLChart(resText);
                            this.saveQueryAxesAndParametersState();
                            if (this.debugServices) this.renderChartServices(this.chartXmlData, t2 - t1);
                            else {
                                this.RenderChartAndSetAutoresize();
                            }
                        } else {
                            errMsg = this.getErrorFromText(resText);
                            this.renderError(errMsg);
                        }
                    }).closure(this));
                } else {
                    errMsg = this.getErrorFromText(resText);
                    this.renderError(errMsg);
                }
            }).closure(this));
        }
    }

    this.renderChart_JS = function() {
        if (this.RenderHtmlAndJs())
            this.renderChart_JSforRenderTypeHtmlAndJs();
        else
            this.renderChart_JSforRenderTypeFlash();
    }

    this.RenderChartAndSetAutoresize = function () {
        this.ForceChartRendering = true; // Esta variable es para forzar el dibujado de las gráficas con autoresize cuando se cambia alguna de sus propiedades (ver issue 26466)
        if (!gx.lang.gxBoolean(this.AutoResize) && this.IntervalCheckChartSize == undefined) // No tiene autoresize y no se lo acaban de sacar. Por lo tanto la dibujo
            this.renderChart_JS();
        else {
            if (gx.lang.gxBoolean(this.AutoResize) && this.IntervalCheckChartSize == undefined) { // Tiene autoresize y se lo acaban de poner. Seteo el intervalo para que se chequee el tamaño cada 1 minuto
                switch (this.AutoResizeType) {
                    case "Both":
                        var container = this.getContainerControl();
                        container.style.width = "100%";
                        container.style.height = "100%";
                        var code = "try { " + this.me() + ".checkChartSize() } catch (e) {}";
                        this.IntervalCheckChartSize = setInterval(code, 1000);
                        return;
                    case "Vertical":
                        this.getContainerControl().style.width = gx.dom.addUnits(this.Width);
                        break;
                    case "Horizontal":
                        this.getContainerControl().style.height = gx.dom.addUnits(this.Height);
                        break;
                }
                this.renderChart_JS();
            } else if (!gx.lang.gxBoolean(this.AutoResize) && this.IntervalCheckChartSize != undefined) { // No tiene autoresize, pero tenía. Borro el intervalo para que no se actualice más el tamaño
                this.IntervalCheckChartSize = clearInterval(this.IntervalCheckChartSize);
            }
        }
    }

    this.checkChartSize = function () {
        if (this.LastHeight == undefined) this.LastHeight = parseInt(this.fixSize(this.Height));
        if (this.LastWidth == undefined) this.LastWidth = parseInt(this.fixSize(this.Width));
        var container = this.getContainerControl();
        var height = (container.offsetHeight != 0 ? container.offsetHeight : container.parentNode.offsetHeight);
        var width = (container.offsetWidth != 0 ? container.offsetWidth : container.parentNode.offsetWidth);
        if (this.ForceChartRendering || (height != this.LastHeight && width != this.LastWidth)) {
            this.Height = height.toString();
            this.Width = width.toString();
            this.LastHeight = height;
            this.LastWidth = width;
            this.renderChart_JS();
            this.ForceChartRendering = false;
        }
    }

    this.getChartType_forHightCharts = function () {

        switch (this.RealChartType) {
            case "Column":
            case "Column3D":
            case "StackedColumn":
            case "StackedColumn3D":
                return "column";
            case "Bar":
            case "StackedBar":
                return "bar";
            case "Area":
            case "StackedArea":
                return "area";
            case "Line":
                return "line";
            case "ColumnLine":
            case "Column3DLine":
                return "column";
            case "Pie":
            case "Pie3D":
            case "Doughnut":
                return "pie";
            case "Funnel":
                return "funnel";
            case "Timeline":
                return "line";
            default:
                return "line";
        }
    }


    this.setPlotOptions_forHighCharts = function (options, showvalues) {
        if (!gx.lang.gxBoolean(this.AutoResize)) {
            options.chart.height = this.fixSize(this.Height);
            options.chart.width = this.fixSize(this.Width);
        }

        options.plotOptions = {};
        options.plotOptions.series = {};
        options.plotOptions.series.events = {};
        options.plotOptions.series.events.legendItemClick = function (event) {
            return false;
        }
        if (showvalues) {
            options.plotOptions.series.dataLabels = {};
            options.plotOptions.series.dataLabels.enabled = true;
            options.plotOptions.series.dataLabels.formatter = function () {
                var value;
                if (this.series.chart.options.chart.type == "pie") {
                    value = '<b>' + this.point.name + '</b>: ' + Highcharts.numberFormat(this.point.y, options.numberDecimalPrecision, options.numberDecimalSeparator, options.numberThousandSeparator);
                } else {
                    value = QueryViewerCollection[this.series.chart.options.viewerId].formatNumber(
                    this.y,
                    this.series.chart.options.numberPrefix,
                    this.series.chart.options.numberSuffix,
                    this.series.chart.options.numberDecimalSeparator,
                    this.series.chart.options.numberDecimalPrecision,
                    this.series.chart.options.numberThousandSeparator);
                }    
                return value;
            }
        }

        // Asocia el manejador para el evento click de la chart
        options.plotOptions.series.events.click = this.onHighchartsItemClickEventHandler;

        if (this.RealChartType == "Timeline") {
            // 21-02-2013
            //    * MRevetria: Para el caso de la time se setea esta configuracion para que la highcart interpole los datos, evitando que se generen
            // saltos (gaps) en la la linea graficada. Cuando se tienen datos faltantes para el eje x (fechas para las cuales no se obtuvieron datos)
            options.plotOptions.series.connectNulls = true;

            // 20-08-2012
            //    * MRevetria: Para la timeline la anterior es toda la configuracion necesario para el objeto plotOptions.
            return;
        }

        switch (options.chart.type) {
            case "bar":
                options.plotOptions.bar = {};
                options.plotOptions.bar.dataLabels = {};
                options.plotOptions.bar.dataLabels.enabled = showvalues;

                if (this.RealChartType == "StackedBar") {
                    options.plotOptions.series.stacking = 'normal';
                    options.plotOptions.bar.stacking = 'normal';
                }
                break;
            case "column":
                options.plotOptions.column = {};
                options.plotOptions.column.dataLabels = {};
                options.plotOptions.column.dataLabels.enabled = showvalues;

                if ((this.RealChartType == "StackedColumn") || (this.RealChartType == "StackedColumn3D")) {
                    options.plotOptions.series.stacking = 'normal';
                    options.plotOptions.column.stacking = 'normal';
                }

                break;
            case "area":
                options.plotOptions.area = {};
                options.plotOptions.area.dataLabels = {};
                options.plotOptions.area.dataLabels.enabled = showvalues;

                if (this.RealChartType == "StackedArea") {
                    options.plotOptions.area.stacking = 'normal';
                }


                break;

            case "line":
                options.plotOptions.line = {};
                options.plotOptions.line.dataLabels = {};
                options.plotOptions.line.dataLabels.enabled = showvalues;
                break;

            case "pie":
                options.plotOptions.pie = {};
                options.plotOptions.pie.dataLabels = {};
                options.plotOptions.pie.dataLabels.enabled = showvalues;

                if (this.RealChartType == "Doughnut") {
                    options.plotOptions.pie.innerSize = '35%';
                }

                break;
            default:

        }
    }

	this.setLanguageOptions_forHighCharts = function(){
	
		Highcharts.setOptions({  
			lang: {  
					//loading: 'Cargando...',  
					months: [gx.getMessage("GXPL_QViewerJanuary"), gx.getMessage("GXPL_QViewerFebruary"), gx.getMessage("GXPL_QViewerMarch"), gx.getMessage("GXPL_QViewerApril"), gx.getMessage("GXPL_QViewerMay"), gx.getMessage("GXPL_QViewerJune"), gx.getMessage("GXPL_QViewerJuly"), gx.getMessage("GXPL_QViewerAugust"), gx.getMessage("GXPL_QViewerSeptember"), gx.getMessage("GXPL_QViewerOctober"), gx.getMessage("GXPL_QViewerNovember"), gx.getMessage("GXPL_QViewerDecember")],  
					weekdays: [gx.getMessage("GXPL_QViewerSunday"), gx.getMessage("GXPL_QViewerMonday"), gx.getMessage("GXPL_QViewerTuesday"), gx.getMessage("GXPL_QViewerWednesday"), gx.getMessage("GXPL_QViewerThursday"), gx.getMessage("GXPL_QViewerFriday"), gx.getMessage("GXPL_QViewerSaturday")],  
					shortMonths: [gx.getMessage("GXPL_QViewerJanuary").substring(0,3), gx.getMessage("GXPL_QViewerFebruary").substring(0,3), gx.getMessage("GXPL_QViewerMarch").substring(0,3), gx.getMessage("GXPL_QViewerApril").substring(0,3), gx.getMessage("GXPL_QViewerMay").substring(0,3), gx.getMessage("GXPL_QViewerJune").substring(0,3), gx.getMessage("GXPL_QViewerJuly").substring(0,3), gx.getMessage("GXPL_QViewerAugust").substring(0,3), gx.getMessage("GXPL_QViewerSeptember").substring(0,3), gx.getMessage("GXPL_QViewerOctober").substring(0,3), gx.getMessage("GXPL_QViewerNovember").substring(0,3), gx.getMessage("GXPL_QViewerDecember").substring(0,3)]
					/*,  
					exportButtonTitle: "Exportar",  
					printButtonTitle: "Importar",  
					rangeSelectorFrom: "De",  
					rangeSelectorTo: "A",  
					rangeSelectorZoom: "Periodo",  
					downloadPNG: 'Descargar gráfica PNG',  
					downloadJPEG: 'Descargar gráfica JPEG',  
					downloadPDF: 'Descargar gráfica PDF',  
					downloadSVG: 'Descargar gráfica SVG',  
					printChart: 'Imprimir Gráfica',  
					thousandsSep: ",",  
					decimalPoint: '.'  
					*/
			}   
		});  		
	}
		
    this.loadHighChart_fromMultiSerie = function (chartData) {
        var options = {};
        var isTimeline = this.RealChartType == "Timeline";

        options.chart = {};
        options.chart.renderTo = this.getContainerControl();

        if ((this.RealChartType != "ColumnLine") && (this.RealChartType != "Column3DLine")) {
            options.chart.type = this.getChartType_forHightCharts();
        }

        options.credits = {};
        options.credits.enabled = false;

        this.setPlotOptions_forHighCharts(options, gx.lang.gxBoolean(this.ShowValues));
		this.setLanguageOptions_forHighCharts();

        options.title = {};
        options.title.text = null;

        options.xAxis = {};

        if (this.XAxisLabels == "Vertically") {
            options.xAxis.labels = {};
            options.xAxis.labels.rotation = 270;
            options.xAxis.labels.y = 5;
            options.xAxis.labels.align = "right";
        }

        if (!isTimeline) {
            options.xAxis.categories = [];
        }

        var xmlDoc = this.xmlDocument(chartData);
        var graph = this.getSingleElementByTagName(xmlDoc, "graph");

        var xAxisName = graph.getAttribute("xAxisName");
        var yAxisName = graph.getAttribute("yAxisName");
        options.numberDecimalPrecision = graph.getAttribute("decimalPrecision");
        options.numberPrefix = graph.getAttribute("numberPrefix");
        options.numberSuffix = graph.getAttribute("numberSuffix");
        options.numberDecimalSeparator = graph.getAttribute("decimalSeparator");
        options.numberThousandSeparator = graph.getAttribute("thousandSeparator");

        options.viewerId = this.userControlId(); // Almacena el identificador del control en las opciones de la grafica
        options.xAxis.title = {};
        options.xAxis.title.text = xAxisName;

        if ((this.RealChartType == "ColumnLine") || (this.RealChartType == "Column3DLine")) {
            options.yAxis = [];
            options.yAxis[0] = {};

            options.yAxis[0].title = {};
            options.yAxis[0].title.text = yAxisName;

            options.yAxis[1] = {};
            options.yAxis[1].title = {};
            options.yAxis[1].title.text = ""; // El eje secundario por ahora no es posible setearle titulo

            options.yAxis[0].opposite = true;
        } else {
            options.yAxis = {};
            options.yAxis.title = {};
            options.yAxis.title.text = yAxisName;
        }

        var categoriesnodes;
        if (!gx.util.browser.isIE()) {
            categoriesnodes = this.getMultipleElementsByTagName(graph, "category");
        } else {
            categoriesnodes = this.getMultipleElementsByTagName(this.getSingleElementByTagName(graph, "categories"), "category");
        }
        var fullNames = [];
        var xAxisLabelsEnabled = false;
        options.xAxis.fullCategoriesValues = [];
        for (i = 0; i < categoriesnodes.length; i++) {
            var nameReduced = categoriesnodes[i].getAttribute("name");
            var name = categoriesnodes[i].getAttribute("hoverText");
            if (nameReduced != "") xAxisLabelsEnabled = true;
            if (!isTimeline) {
                options.xAxis.categories[i] = nameReduced;
            }
            options.xAxis.fullCategoriesValues[i] = name;
            fullNames[i] = name;
        }
        if (!isTimeline) {
            if (options.xAxis.labels == undefined) options.xAxis.labels = {};
            options.xAxis.labels.enabled = xAxisLabelsEnabled;
        }

        var datasetnodes = this.getMultipleElementsByTagName(graph, "dataset");
        options.series = [];

        if (!isTimeline) {
            var is3dChart = this.RealChartType == "Column3D" || this.RealChartType == "StackedColumn3D" || this.RealChartType == "Column3DLine";
            var isColumnAndLine = this.RealChartType == "Column3DLine";

            for (i = 0; i < datasetnodes.length; i++) {
                var serieName = datasetnodes[i].getAttribute("seriesName");
                options.series[i] = {};
                options.series[i].name = serieName;
                options.series[i].data = [];

                var serieColor = datasetnodes[i].getAttribute("color");
                // Si es una grafica Column3DLine, entonces no pinta con degrade la linea (la ultima serie)
                options.series[i].color = this.CreateColorObjectForHighcharts(this.convertValueToColor(serieColor), is3dChart && (!isColumnAndLine || i < (datasetnodes.length - 1)));
                var setnodes = this.getMultipleElementsByTagName(datasetnodes[i], "set");

                for (j = 0; j < setnodes.length; j++) {
                    var value = parseFloat(this.trim(setnodes[j].getAttribute("value")).replace(",", "."));
                    var name = fullNames[j];
                    options.series[i].data[j] = {};
                    options.series[i].data[j].y = value;
                    options.series[i].data[j].id = name;
                }

            }
        }

        if ((this.RealChartType == "ColumnLine") || (this.RealChartType == "Column3DLine")) {
            if (datasetnodes.length > 1) {

                var k = 0;
                while (k < datasetnodes.length) {
                    options.series[k].type = 'column';
                    options.series[k].yAxis = 1;

                    if ((k + 1) < datasetnodes.length) options.series[k + 1].type = 'line';
                    k = k + 2;
                }
            }
        } else if (isTimeline) {
            // Codigo de carga ejecutado solo para el caso de graficas Timeline
            options.tooltip = {"backgroundColor": "rgba(255, 255, 255, 0.85)", "borderColor": "#CCCCCC", "borderWidth": 1, "borderRadius": 1, "shadow": true};			
            options.tooltip.shared = true;
            options.tooltip.useHTML = false; //Hay un bug que hace que si esta en true se muestra el html por fuera del aréa del tooltip
            options.tooltip.formatter = function () {
                // Agrupa la lista de puntos por fecha
                var points_by_date = [];
                for (var ind = 0; this.points[ind] != undefined; ind++) {
                    var date = this.points[ind].point.real_x ? this.points[ind].point.real_x : this.x;
                    if (points_by_date[date] == undefined) {
                        points_by_date[date] = [];
                    }
                    points_by_date[date].push(this.points[ind].point);
                }
                var res = '';
				var oldUtc;
                for(var key_date in points_by_date) {
                    var utc = parseInt(key_date);
                    if (!isNaN(utc)) {
                        var points = points_by_date[key_date];
                        for(var ind = 0; points[ind] != undefined; ind++) {
                            var p = points[ind];
                            var serie_name = p.real_serie_name ? p.real_serie_name : p.series.name;
							var colorSpan = '<span style="color:' + p.series.color + ';">'; 							
							var colorSpanBold = '<span style="font-weight:bold;color:' + p.series.color + ';">'; 	
							var boldSpan = '<span style="font-weight:bold;">'; 
							
							if (this.points.length > 0 && this.points[0].series.chart.options.comparing){ //Si esta habilitada la comparación (la fecha es diferente con el período anterior)
								//Se muestra toda la información de cada serie con el color correspondiente
								res += colorSpanBold + Highcharts.dateFormat("%A, %B %d, %Y", utc, true) + '</span><br/>';
								res += colorSpan + serie_name + ': </span>' + colorSpanBold + p.y + '</span><br/>';		
							}
							else{							
								//Se muestra el nombre de cada con el color correspondiente. La fecha que es común va arriba en negro. El valor de cada serie en bold (sin color)							
								if (oldUtc != utc){
									res += Highcharts.dateFormat("%A, %B %d, %Y", utc, true) + '<br/>';
									oldUtc = utc;
								}
								res += colorSpan + serie_name + ': </span>' + boldSpan + p.y + '</span><br/>';
							}													
                        }
                    }
                }

                return res;
            }
            if (!gx.lang.gxBoolean(this.AutoResize)) {
                options.chart.height = this.Height - 35; // Resta a la altura de la grafica el alto del div que contiene los botones de zoom predefinidos
            }
            options.chart.zoomType = 'x';
            options.chart.events = {};
            options.chart.events.selection = function (event) {			
                // Desmarca el botón de zoom seleccionado cuando se hace un zoom seleccionando puntos en la gráfica
				deselectActiveZoom();
            };

            options.plotOptions.line = {};
            options.plotOptions.line.marker = {};
            options.plotOptions.line.marker.enabled = true;

            options.xAxis.type = 'datetime';
            options.xAxis.id = 'xaxis';
            var datePattern = /^\d{1,2}[-\/]\d{1,2}[-\/]\d{2,4}$/;
            var dataAreDateTime = false;
            var minDate = undefined;
            var maxDate = undefined;
            for (ns = 0; ns < datasetnodes.length; ns++) {
                var serieName = datasetnodes[ns].getAttribute("seriesName");
                options.series[ns] = {};
                options.series[ns].name = serieName;
                options.series[ns].data = [];
                var setnodes = this.getMultipleElementsByTagName(datasetnodes[ns], "set");
                if (setnodes.length >= 100) {
                    options.plotOptions.line.marker.enabled = false;
                }
                for (i = 0; i < setnodes.length; i++) {
                    var name = fullNames[i];
                    var value = parseFloat(this.trim(setnodes[i].getAttribute("value")).replace(",", "."));
                    options.series[ns].data[i] = [];
                    var date = new gx.date.gxdate(name);
                    options.series[ns].data[i][0] = date.Value.getTime() - date.Value.getTimezoneOffset() * 60000;
                    options.series[ns].data[i][1] = value;
                    dataAreDateTime = dataAreDateTime || !datePattern.test(name);
                    if (!maxDate || date.Value > maxDate.Value) {
                        maxDate = date;
                    }
                    if (!minDate || date.Value < minDate.Value) {
                        minDate = date;
                    }
                }
            }
            if (!dataAreDateTime) {
                options.xAxis.maxZoom = 10 * 24 * 3600 * 1000;
                if (maxDate != undefined && minDate != undefined && (maxDate.Value.getTime() - minDate.Value.getTime() < 10 * 24 * 3600 * 1000)) {
                    options.xAxis.tickInterval = 24 * 3600 * 1000;
                }
            }

            return options;
        }

        if (!isTimeline) {
            options.tooltip = {};
            options.tooltip.formatter = function () {
                return '<b>' + (this.point.id != "" ? this.point.id : this.series.name) + '</b>: ' + this.point.y;
            }
        }
        return options;
    }

    this.convertValueToColor = function (value) {
        var valueColor;
        if (value.indexOf("#") == -1) valueColor = "#" + value;
        else valueColor = value;

        var vColor = valueColor.substring(1, valueColor.length);


        while (vColor.length < 6)
        vColor = "0" + vColor;

        return "#" + vColor;

    }

    this.loadHighChart_fromOneSerie = function (chartData) {

        var options = {};

        options.chart = {};
        options.chart.renderTo = this.getContainerControl();
        options.chart.type = this.getChartType_forHightCharts();

        options.credits = {};
        options.credits.enabled = false;

        this.setPlotOptions_forHighCharts(options, gx.lang.gxBoolean(this.ShowValues));
		this.setLanguageOptions_forHighCharts();
		
        options.title = {};
        options.title.text = null;
        options.xAxis = {};

        if (this.XAxisLabels == "Vertically") {
            options.xAxis.labels = {};
            options.xAxis.labels.rotation = 270;
            options.xAxis.labels.y = 5;
            options.xAxis.labels.align = "right";
        }

        if (this.RealChartType != "Timeline") {
            options.xAxis.categories = [];
        }


        var xmlDoc = this.xmlDocument(chartData);
        var graph = this.getSingleElementByTagName(xmlDoc, "graph");
        var setnodes = this.getMultipleElementsByTagName(graph, "set");


        var xAxisName = graph.getAttribute("xAxisName");
        var yAxisName = graph.getAttribute("yAxisName");
        options.numberPrefix = graph.getAttribute("numberPrefix");
        options.numberSuffix = graph.getAttribute("numberSuffix");
        options.numberDecimalSeparator = graph.getAttribute("decimalSeparator");
        options.numberDecimalPrecision = graph.getAttribute("decimalPrecision");
        options.numberThousandSeparator = graph.getAttribute("thousandSeparator");

        options.xAxis.title = {};
        options.xAxis.title.text = xAxisName;

        options.viewerId = this.userControlId(); // Almacena el identificador del control en las opciones de la grafica
        options.yAxis = {};
        options.yAxis.title = {};
        options.yAxis.title.text = yAxisName;

        options.series = [];
        options.series[0] = {};
        options.series[0].name = xAxisName;
        options.series[0].data = [];

        if (options.chart.type == "pie") {
            options.tooltip = {};
            options.tooltip.formatter = function () {
                var percentage = Math.round(this.percentage * 10) / 10 //
                return '<b>' + (this.point.name != "" ? this.point.name : this.series.chart.options.yAxis[0].title.text) + '</b>: ' + percentage + '%';
            }
            options.plotOptions.pie.allowPointSelect = true;
            options.plotOptions.pie.cursor = 'pointer';
            options.plotOptions.pie.dataLabels.color = '#000000';
            options.plotOptions.pie.dataLabels.connectorColor = '#000000';
            options.plotOptions.pie.dataLabels.enabled = true;
            options.plotOptions.pie.dataLabels.formatter = function() {
                return '<b>' + this.point.name + '</b>:' + Highcharts.numberFormat(this.point.y, options.numberDecimalPrecision, options.numberDecimalSeparator, options.numberThousandSeparator);
            };

            options.xAxis.fullCategoriesValues = [];
            for (i = 0; i < setnodes.length; i++) {
                var name = setnodes[i].getAttribute("name");
                var value = parseFloat(this.trim(setnodes[i].getAttribute("value")).replace(",", "."));
                var color = this.convertValueToColor(setnodes[i].getAttribute("color"));
                options.xAxis.fullCategoriesValues[i] = setnodes[i].getAttribute("hoverText");
                options.series[0].data[i] = {};
                options.series[0].data[i].name = name;
                options.series[0].data[i].y = value;
                options.series[0].data[i].color = color;
            }
            return options;
        } else if (this.RealChartType == "Timeline") {
            // Codigo de carga ejecutado solo para el caso de graficas Timeline
            options.tooltip = {"backgroundColor": "rgba(255, 255, 255, 0.85)", "borderColor": "#CCCCCC", "borderWidth": 1, "borderRadius": 1, "shadow": true};			
            options.tooltip.shared = true;
            options.tooltip.useHTML = false; //Hay un bug que hace que si esta en true se muestra el html por fuera del aréa del tooltip
            options.tooltip.formatter = function () {
                // Agrupa la lista de puntos por fecha
                var points_by_date = [];
                for (var ind = 0; this.points[ind] != undefined; ind++) {
                    var date = this.points[ind].point.real_x ? this.points[ind].point.real_x : this.x;
                    if (points_by_date[date] == undefined) {
                        points_by_date[date] = [];
                    }
                    points_by_date[date].push(this.points[ind].point);
                }
                var res = '';
                var currentTotal = 0;
                var previousTotal = 0;
				var oldUtc;
                for(var key_date in points_by_date) {
                    var utc = parseInt(key_date);
                    if (!isNaN(utc)) {
                        var points = points_by_date[key_date];
                        for(var ind = 0; points[ind] != undefined; ind++) {
                            var p = points[ind];
                            var serie_name = p.real_serie_name ? p.real_serie_name : p.series.name;
							var colorSpan = '<span style="color:' + p.series.color + ';">'; 							
							var colorSpanBold = '<span style="font-weight:bold;color:' + p.series.color + ';">'; 	
							var boldSpan = '<span style="font-weight:bold;">'; 
							
                            if (p.real_x) {
                                previousTotal += p.y;
                            } else {
                                currentTotal += p.y;
                            }
							
							if (this.points.length > 0 && this.points[0].series.chart.options.comparing){ //Si esta habilitada la comparación (la fecha es diferente con el período anterior)
								//Se muestra toda la información de cada serie con el color correspondiente
								res += colorSpanBold + Highcharts.dateFormat("%A, %B %d, %Y", utc, true) + '</span><br/>';
								res += colorSpan + serie_name + ': </span>' + colorSpanBold + p.y + '</span><br/>';		
							}
							else{							
								//Se muestra el nombre de cada con el color correspondiente. La fecha que es común va arriba en negro. El valor de cada serie en bold (sin color)							
								if (oldUtc != utc){
									res += Highcharts.dateFormat("%A, %B %d, %Y", utc, true) + '<br/>';
									oldUtc = utc;
								}
								res += colorSpan + serie_name + ': </span>' + boldSpan + p.y + '</span><br/>';
							}
                        }
                    }
                }

                // Agrega la informacion de cambio entre una seria y la otra. Esto solo para el caso de tener una serie en la query.
                if (this.points.length > 0 && this.points[0].series.chart.options.comparing) {

					//Si las dos series tienen valores en el punto actual
					if (previousTotal > 0 && currentTotal > 0){				
					
						var change = ((currentTotal - previousTotal)/previousTotal)*100;

						change = QueryViewerCollection[this.points[0].series.chart.options.viewerId].formatNumber(
							change,
							this.points[0].series.chart.options.numberPrefix,
							this.points[0].series.chart.options.numberSuffix,
							this.points[0].series.chart.options.numberDecimalSeparator,
							"2",                                                              // Fuerza a dos la cantidad de numeros decimales.
							this.points[0].series.chart.options.numberThousandSeparator);
							
						res += '<span>' + gx.getMessage("GXPL_QViewerJSChartChange") + '</span><span style="font-weight:bold;"> ' + change + '%</span>'; // + (change.indexOf("-") == 0 ? ';color:red' : '')  + ';"> ' + change + '%</span>'
					
					}
                }

                return res;
            }

            options.plotOptions.line = {};
            options.plotOptions.line.marker = {};
            options.plotOptions.line.marker.enabled = setnodes.length < 100;

            if (!gx.lang.gxBoolean(this.AutoResize)) {
                options.chart.height = this.Height - 35; // Resta la altura del div con los botones de zoom al ocupado por la grafica.
            }
            options.series[0].name = yAxisName;
            options.chart.zoomType = 'x';
            options.chart.events = {};			
            options.chart.events.selection = function (event) {			
                // Desmarca el botón de zoom seleccionado cuando se hace un zoom seleccionando puntos en la gráfica
				deselectActiveZoom();
            };

            options.xAxis.type = 'datetime';
            options.xAxis.id = 'xaxis';
            var datePattern = /^\d{1,2}[-\/]\d{1,2}[-\/]\d{2,4}$/;
            var dataAreDateTime = false;
            var minDate = undefined;
            var maxDate = undefined;
            options.xAxis.fullCategoriesValues = [];
            for (i = 0; i < setnodes.length; i++) {
                var name = setnodes[i].getAttribute("hoverText");
                var value = parseFloat(this.trim(setnodes[i].getAttribute("value")).replace(",", "."));
                options.series[0].data[i] = [];
                var date = new gx.date.gxdate(name);
                options.xAxis.fullCategoriesValues[i] = name;
                options.series[0].data[i][0] = date.Value.getTime() - date.Value.getTimezoneOffset() * 60000;
                options.series[0].data[i][1] = value;
                dataAreDateTime = dataAreDateTime || !datePattern.test(name);
                if (!maxDate || date.Value > maxDate.Value) {
                    maxDate = date;
                }
                if (!minDate || date.Value < minDate.Value) {
                    minDate = date;
                }
            }
            if (!dataAreDateTime) {
                options.xAxis.maxZoom = 10 * 24 * 3600 * 1000;
                if (maxDate != undefined && minDate != undefined && (maxDate.Value.getTime() - minDate.Value.getTime() < 10 * 24 * 3600 * 1000)) {
                    options.xAxis.tickInterval = 24 * 3600 * 1000;
                }
            }
            return options;
        } else {
            var is3dChart = this.RealChartType == "Column3D" || this.RealChartType == "StackedColumn3D" || this.RealChartType == "Column3DLine";

            options.tooltip = {};
            options.tooltip.formatter = function() {
                return '<b>' + (this.point.id != "" ? this.point.id : this.series.chart.options.yAxis[0].title.text) + '</b>: ' + Highcharts.numberFormat(this.point.y, options.numberDecimalPrecision, options.numberDecimalSeparator, options.numberThousandSeparator);
            }

            options.xAxis.fullCategoriesValues = [];
            var xAxisLabelsEnabled = false;
            for (i = 0; i < setnodes.length; i++) {
                var name = setnodes[i].getAttribute("hoverText");
                var nameReduced = setnodes[i].getAttribute("name");
                if (nameReduced != "") xAxisLabelsEnabled = true;
                var value = parseFloat(this.trim(setnodes[i].getAttribute("value")).replace(",", "."));
                var color = this.convertValueToColor(setnodes[i].getAttribute("color"));
                options.xAxis.fullCategoriesValues[i] = name;
                options.xAxis.categories[i] = nameReduced;
                options.series[0].data[i] = {};
                options.series[0].data[i].y = value;
                options.series[0].data[i].color = this.CreateColorObjectForHighcharts(color, is3dChart);
                options.series[0].data[i].id = name;
            }
            if (options.xAxis.labels == undefined) options.xAxis.labels = {};
            options.xAxis.labels.enabled = xAxisLabelsEnabled;
            return options;
        }
    }


    this.formatNumber = function (number, prefix, suffix, decimalSeparator, decimalPrecision, thousandSeparator) {
        if (number === undefined || number === null || number === "") {
            return "";
        }

        if (prefix === undefined || prefix === null) prefix = "";
        if (suffix === undefined || suffix === null) suffix = "";
        // Ajustar los numeros decimales de acuerdo al argumento decimalPrecision
        var fix = parseInt(decimalPrecision);
        var fixedNum = number;
        if (fix != NaN) {
            fixedNum = fixedNum.toFixed(fix);
        }

        // Trabaja con la representacion en strings del numero dado.
        var str_num = fixedNum + "";

        // Agregar el separador de miles
        if (thousandSeparator !== null && thousandSeparator !== undefined && thousandSeparator != "") {
            x = str_num.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            str_num = x1 + x2;
        }

        // Reemplazar el separador decimal, por defecto es el '.'.
        if (decimalSeparator !== null && decimalSeparator !== undefined && decimalSeparator != "") {
            str_num = str_num.replace(".", decimalSeparator);
        }

        // Concatenar el prefijo y el sufijo
        str_num = prefix + str_num + suffix;


        return str_num;
    }

    this.CreateColorObjectForHighcharts = function (color, isIn3d) {
        var colorObj;
        if (!isIn3d) {
            colorObj = color;
        } else {
            colorObj = {};
            colorObj.linearGradient = {};
            colorObj.linearGradient.x1 = 0;
            colorObj.linearGradient.y1 = 0;
            colorObj.linearGradient.x2 = 1;
            colorObj.linearGradient.y2 = 0;
            colorObj.stops = [];
            colorObj.stops[0] = [];
            colorObj.stops[0][0] = 0;
            colorObj.stops[0][1] = color;
            colorObj.stops[1] = [];
            colorObj.stops[1][0] = 0.3;
            colorObj.stops[1][1] = 'white';
            colorObj.stops[2] = [];
            colorObj.stops[2][0] = 1;
            colorObj.stops[2][1] = color;
        }
        return colorObj;
    }
	function hideLegend(legend){
		if (legend.group)
			legend.group.hide();
			//legend.box.hide(); //legend.box no existe mas en la versión 4
		
	}
	
	function showLegend(legend){
		if (legend.group)
			legend.group.show();
			//legend.box.show(); //legend.box no existe mas en la versión 4
		
	}

    var chart1;
    this.renderChart_JSforRenderTypeHtmlAndJs = function () {
        var i = 0;
        var options;

        if ((this.RealChartType == "Pie" || this.RealChartType == "Pie3D" || this.RealChartType == "Doughnut" || this.RealChartType == "Funnel") && (this.chartSeriesCount != "1"))
            this.renderError("Too many series for a " + this.RealChartType + " chart");
        else {

            if (this.chartSeriesCount <= 1)
                options = this.loadHighChart_fromOneSerie(this.chartXmlData);
            else
                options = this.loadHighChart_fromMultiSerie(this.chartXmlData);

            options.chart.events = options.chart.events || {};
            options.chart.events.redraw = function(evt) {
                var chart = evt.target;
                if (chart && chart.series.length == 1) 
					hideLegend(chart.legend)
            };

            if (this.RealChartType == "Timeline") {
                chart1 = new Highcharts.Chart(options, this.HCFinishedLoadingCallback);
                chart1.pointer.onContainerMouseDown_old = chart1.pointer.onContainerMouseDown;
                chart1.pointer.onContainerMouseDown = function (e) {
                    var source = gx.evt.source(e);
                    if (gx.dom.isChildNode(source, gx.dom.el(options.viewerId + "_options")))
                        return;

                    this.onContainerMouseDown_old(e);
                };
            } else {
                chart1 = new Highcharts.Chart(options, function (chart) {
                    // Issue 27389: oculta el cuadro de leyendas cuando la grafica contiene solo una serie
                    if (chart.series.length == 1)
						hideLegend(chart.legend);

                    var indSer, indData;
                    jQuery.each(chart.xAxis[0].ticks, function (i, tick) {
                        if (tick && tick.label) {
                            tick.label.on("click", function (event) {
                                QueryViewerCollection[chart.options.viewerId].onHighchartsXAxisClickEventHandler(event, i, tick, chart);
                            });
                        }
                    });
                });
            }
        }
    }

/*******************************************************************************************
/									ZOOMING & COMPARE
/******************************^************************************************************/

	// Mantiene el identificador del control de zoom que fue clickeado por ultima vez
	var prevClickedCtrlId;	
	var	viewerId;
	
	/*
	 * Funciones auxiliares comunes
	 */

	// Determina si el navegador es Microsoft Internet Explorer en una versión anterior a la 9	
	function isOldIEf(){
		return gx.util.browser.isIE() && gx.util.browser.ieVersion() <= 8.0;
	}
	
	var isOldIE = isOldIEf();	
	
	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}	
	
	function getZoomId(z) {
		return (viewerId + "_Zoom_" + z + "m");
	}
	
	function getZoomControl(z) {
		if (isNumeric(z))
			return gx.dom.el(getZoomId(z));
		else //control id
			return gx.dom.el(z);
	}	
	
	function triggerZoom(z) {
		zoom = getZoomControl(z);
		if (zoom)
			zoom.onclick();
	}		
	
	/*
	function selectGreaterZoom(){
		if getZoomControl(12)
			triggerZoom(12)
	}*/
	
	function changeZoomControlColor(z, rgb) {
		zoom = getZoomControl(z);
		if (zoom){
			if (isOldIE)
				zoom.parentNode.style.backgroundColor = rgb;
			else
				zoom.childNodes[0].childNodes[0].setAttribute("fill", rgb);
		}
	}	
	
	function selectZoom(z) {
		changeZoomControlColor(z, "rgb(161, 193, 227)");
	}
		
	function deselectZoom(z) {
		changeZoomControlColor(z, "rgb(240, 240, 240)");
	}
	
	function hideZoom(z) {		
		zoom = getZoomControl(z);
		if (zoom)
			zoom.style.display = "none";
	}	
	
	function showZoom(z) {
		zoom = getZoomControl(z);
		if (zoom)
			zoom.style.display = "";
	}	
		
	function highlightZoom(z) {
		changeZoomControlColor(z, "rgb(194, 220, 255)");
	}

	function getSelectedZoomFactor(){
		return parseInt(prevClickedCtrlId.substring(0, prevClickedCtrlId.length -1).substring(prevClickedCtrlId.indexOf("_Zoom_") + 6));
	}
	
	//Esta función se invoca más arriba en el handler del evento de selección de la timeline
	function deselectActiveZoom(){
		deselectZoom(prevClickedCtrlId);
	}
	
	//This function execute when the Highcharts object is finished loading and rendering.
    this.HCFinishedLoadingCallback = function ($this) {

        function mouseOver() {
            if (this.id == prevClickedCtrlId) return;
			highlightZoom(this.id);
        }

        function mouseOut() {
            if (this.id == prevClickedCtrlId) return;
			deselectZoom(this.id);
        }

        // Asocia el manejador para el evento click sobre el eje x
        var indSer, indData;
        jQuery.each($this.xAxis[0].ticks, function (tick_index, tick) {
            if (tick && tick.label) {
                tick.label.on("click", function (event) {
                    QueryViewerCollection[$this.options.viewerId].onHighchartsXAxisClickEventHandler(event, tick_index, tick, $this);
                });
            }
        });

        // Issue 27389: oculta el cuadro de leyendas cuando la grafica contiene solo una serie
        if ($this.series.length == 1)
			hideLegend($this.legend);

        // Crea un nuevo div conteniendo los links para hacer zoom predefenidos programaticamente.
        viewerId = $this.options.viewerId;
        var qryViewer = QueryViewerCollection[viewerId];
        var div = $this.options.chart.renderTo;
        var div2 = document.createElement('div');
        var fontFamily = qryViewer.FontFamily;
        var fontSize = qryViewer.FontSize;
        var fontColor = qryViewer.getHtmlColor(qryViewer.FontColor);
        div2.id = viewerId + "_options";
        div2.style.height = "35px";

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Crea el contenedor con las opciones de zoom predefinidas
        var winTime = $this.get('xaxis').getExtremes().dataMax - $this.get('xaxis').getExtremes().dataMin;
        var left = 47;
        var zoom_html = '';
        if (!isOldIE) {
            zoom_html = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="35"><text x="10" y="25" style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + '"><tspan x="10">' + gx.getMessage("GXPL_QViewerJSChartZoom") + '</tspan></text>';
            if (winTime > 30.42 * 24 * 3600 * 1000) {
                // Ventana de tiempo mayor a un mes => habilita zoom predefinido de un mes
                zoom_html += '<a id="' + viewerId + '_Zoom_1m"><g zIndex="7" style="cursor:default;text-align:center;" transform="translate(' + left + ',10)"><rect rx="0" ry="0" fill="rgb(240,240,240)" x="0.5" y="0.5" width="30" height="18" stroke-width="1" stroke="#999"></rect><text x="5.5" y="14" style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';fill:black;" zIndex="1"><tspan x="5.5">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToOneMonth") + '</tspan></text></g></a>';
                left += 32;
                if (winTime > 60.83 * 24 * 3600 * 1000) {
                    // Ventana de tiempo mayor a 2 meses => habilita zoom predefinido de 2 meses
                    zoom_html += '<a id="' + viewerId + '_Zoom_2m"><g zIndex="7" style="cursor:default;text-align:center;" transform="translate(' + left + ',10)"><rect rx="0" ry="0" fill="rgb(240,240,240)" x="0.5" y="0.5" width="30" height="18" stroke-width="1" stroke="#999"></rect><text x="5.5" y="14" style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';fill:black;" zIndex="1"><tspan x="5.5">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToTwoMonth") + '</tspan></text></g></a>';
                    left += 32;
                    if (winTime > 91.25 * 24 * 3600 * 1000) {
                        // Ventana de tiempo mayor a 3 meses => habilita zoom predefinido de 3 meses
                        zoom_html += '<a id="' + viewerId + '_Zoom_3m"><g zIndex="7" style="cursor:default;text-align:center;" transform="translate(' + left + ',10)"><rect rx="0" ry="0" fill="rgb(240,240,240)" x="0.5" y="0.5" width="30" height="18" stroke-width="1" stroke="#999"></rect><text x="5.5" y="14" style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';fill:black;" zIndex="1"><tspan x="5.5">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToThreeMonth") + '</tspan></text></g></a>';
                        left += 32;
                        if (winTime > 182.5 * 24 * 3600 * 1000) {
                            // Ventana de tiempo mayor a 6 meses => habilita zoom predefinido de 6 meses
                            zoom_html += '<a id="' + viewerId + '_Zoom_6m"><g zIndex="7" style="cursor:default;text-align:center;" transform="translate(' + left + ',10)"><rect rx="0" ry="0" fill="rgb(240,240,240)" x="0.5" y="0.5" width="30" height="18" stroke-width="1" stroke="#999"></rect><text x="5.5" y="14" style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';fill:black;" zIndex="1"><tspan x="5.5">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToSixMonth") + '</tspan></text></g></a>';
                            left += 32;
                            if (winTime > 365 * 24 * 3600 * 1000) {
                                // Ventana de tiempo mayor a 1 año => habilita zoom predefinido de 1 año
                                zoom_html += '<a id="' + viewerId + '_Zoom_12m"><g zIndex="7" style="cursor:default;text-align:center;" transform="translate(' + left + ',10)"><rect rx="0" ry="0" fill="rgb(240,240,240)" x="0.5" y="0.5" width="30" height="18" stroke-width="1" stroke="#999"></rect><text x="5.5" y="14" style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';fill:black;" zIndex="1"><tspan x="5.5">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToOneYear") + '</tspan></text></g></a>';
                                left += 32;
                            }
                        }
                    }
                }
            }
            // Agrega el boton de zoom "All" siempre visible
            zoom_html += '<a id="' + viewerId + '_Zoom_0m"><g zIndex="7" style="cursor:default;text-align:center;" transform="translate(' + left + ',10)"><rect rx="0" ry="0" fill="rgb(240,240,240)" x="0.5" y="0.5" width="30" height="18" stroke-width="1" stroke="#999"></rect><text x="5.5" y="14" style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';fill:black;" zIndex="1"><tspan x="5.5">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToAll") + '</tspan></text></g></a>';
        } else {
            zoom_html = '<table><tr>';
            zoom_html += '<td style="padding-top: 1px; padding-right: 3px; padding-bottom: 1px; padding-left: 3px;"><span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + '">' + gx.getMessage("GXPL_QViewerJSChartZoom") + '</span></td>';
            if (winTime > 30.42 * 24 * 3600 * 1000) {
                // Ventana de tiempo mayor a un mes => habilita zoom predefinido de un mes
                zoom_html += '<td style="min-width: 32px; min-height: 20px;"><div style="text-align: center; padding-top: 1px; padding-right: 3px; padding-bottom: 1px; padding-left: 3px; background-color: rgb(240, 240, 240); cursor:default;"><a id="' + viewerId + '_Zoom_1m"><span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';" zIndex="1">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToOneMonth") + '</span></a></div></td>';
                if (winTime > 60.83 * 24 * 3600 * 1000) {
                    // Ventana de tiempo mayor a 2 meses => habilita zoom predefinido de 2 meses
                    zoom_html += '<td style="min-width: 32px; min-height: 20px;"><div style="text-align: center; padding-top: 1px; padding-right: 3px; padding-bottom: 1px; padding-left: 3px; background-color: rgb(240, 240, 240); cursor:default;"><a id="' + viewerId + '_Zoom_2m"><span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';" zIndex="1">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToTwoMonth") + '</span></a></div></td>';
                    if (winTime > 91.25 * 24 * 3600 * 1000) {
                        // Ventana de tiempo mayor a 3 meses => habilita zoom predefinido de 3 meses
                        zoom_html += '<td style="min-width: 32px; min-height: 20px;"><div style="text-align: center; padding-top: 1px; padding-right: 3px; padding-bottom: 1px; padding-left: 3px; background-color: rgb(240, 240, 240); cursor:default;"><a id="' + viewerId + '_Zoom_3m"><span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';" zIndex="1">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToThreeMonth") + '</span></a></div></td>';
                        if (winTime > 182.5 * 24 * 3600 * 1000) {
                            // Ventana de tiempo mayor a 6 meses => habilita zoom predefinido de 6 meses
                            zoom_html += '<td style="min-width: 32px; min-height: 20px;"><div style="text-align: center; padding-top: 1px; padding-right: 3px; padding-bottom: 1px; padding-left: 3px; background-color: rgb(240, 240, 240); cursor:default;"><a id="' + viewerId + '_Zoom_6m"><span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';" zIndex="1">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToSixMonth") + '</span></a></div></td>';
                            if (winTime > 365 * 24 * 3600 * 1000) {
                                // Ventana de tiempo mayor a 1 año => habilita zoom predefinido de 1 año
                                zoom_html += '<td style="min-width: 32px; min-height: 20px;"><div style="text-align: center; padding-top: 1px; padding-right: 3px; padding-bottom: 1px; padding-left: 3px; background-color: rgb(240, 240, 240); cursor:default;"><a id="' + viewerId + '_Zoom_12m"><span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';" zIndex="1">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToOneYear") + '</span></a></div></td>';
                            }
                        }
                    }
                }
            }
            // Agrega el boton de zoom "All" siempre visible
            zoom_html += '<td style="min-width: 32px; min-height: 20px;"><div style="text-align: center; padding-top: 1px; padding-right: 3px; padding-bottom: 1px; padding-left: 3px; background-color:: rgb(240, 240, 240); cursor:default;"><a id="' + viewerId + '_Zoom_0m"><span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + ';" zIndex="1">' + gx.getMessage("GXPL_QViewerJSChartZoomLevelToAll") + '</span></a></div></td>';
            zoom_html += '</tr></table>';
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Crea el contenedor con las opciones de "compare to past"
        var compare_html = '<input id="' + div2.id + '_compare_enable" type="checkbox" style="margin:0 2px 0; position:relative; top:2px;">' + '<span style="font-family:' + fontFamily + ';font-size:' + fontSize + 'px;color:' + fontColor + '">' + gx.getMessage("GXPL_QViewerCompareWith") + '</span>' + '<select id="' + div2.id + '_compare_options" style="font-size: ' + fontSize + 'px;">' + '<option value="PrevPeriod">' + gx.getMessage("GXPL_QViewerPreviousPeriod") + '</span></option>' + '<option value="PrevYear">' + gx.getMessage("GXPL_QViewerPreviousYear") + '</option>' + '</select>' + '</td>' + '</tr>' + '</table>';
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////    

        var htmlBuffer = [
            '<div id="', div2.id, '_tb">',
                '<div id="',  div2.id + '_zoom" style="float:left;min-width:260px;">',
                    '<div style="margin-left: ', (isOldIE ? '25' : '15'), 'px;">', 
                        zoom_html, 
                    '</div>',
                '</div>',
                '<div id="', div2.id, '_compare" style="float:right;margin:11px 4px 0 0;">',
                    '<div style="font-size: ', fontSize, 'px; color: ', fontColor, '; font-family: ', fontFamily, '; vertical-align: middle;">', 
                        compare_html, 
                    '</div>',
                '</div>',
            '</div>'
        ];
        var html = htmlBuffer.join('');

        // Cierra el inicio del html del div con los botones de zoom predefinidos   
        html += isOldIE ? '' : '</svg>';


        div2.innerHTML = html;
        var parentElement = div.firstChild,
            parentHeight = parentElement.style.height;
        parentElement.insertBefore(div2, parentElement.firstChild);
        if (parentHeight.indexOf("px") >= 0) {
            parentElement.style.height = gx.dom.addUnits(parseInt(parentHeight, 10) + 35);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////
        // Event handlers para las opciones de "compare to past"
        function CompareToFunction() {
            // La variable $this es la grafica para la que se ejecuto el evento
            var viewerId = $this.options.viewerId;
            var qryViewer = QueryViewerCollection[viewerId];
            qryViewer.getChartData((function (data) {
                qryViewer.hideActivityIndicator();
                var xmlDoc = qryViewer.xmlDocument(data);
                var graph = qryViewer.getSingleElementByTagName(xmlDoc, "graph");
                var datasetnodes = qryViewer.getMultipleElementsByTagName(graph, "dataset");
                var setnodes = qryViewer.getMultipleElementsByTagName(graph, "set");
                if (datasetnodes.length == 0) {
                    datasetnodes = [setnodes];
                }

                if (qryViewer.chartSeriesCount > 1) {
                    var categoriesnodes;
                    if (!gx.util.browser.isIE()) {
                        categoriesnodes = qryViewer.getMultipleElementsByTagName(graph, "category");
                    } else {
                        categoriesnodes = qryViewer.getMultipleElementsByTagName(this.getSingleElementByTagName(graph, "categories"), "category");
                    }
                    var categories = []
                    for (ind = 0; categoriesnodes[ind] != undefined; ind++) {
                        categories[ind] = categoriesnodes[ind].getAttribute("hoverText");
                    }
                }

                // Chequea si esta marcado el chkbox que indica que se quiere comparar
                var compare = gx.dom.el(div2.id + '_compare_enable').checked;
                $this.options.comparing = compare;

				if (compare){
					var selectedZoom = getSelectedZoomFactor();
					if (selectedZoom == 0) {//Zoom all
						var zoom_3m = getZoomControl(3);
						if (zoom_3m)
							triggerZoom(3);
						else
							triggerZoom(1);
					}
				}
				
                // Obtiene el tipo de periodo contra el que se quiere comparar
                var extremes = $this.get('xaxis').getExtremes();
				extremes.min = extremes.userMin; //Sin esto, la llamada a setextremes (con redraw en false) realizado en el zoom no actualiza el min hasta el próximo dibujado.
				extremes.max = extremes.userMax;
				
                if (compare) {
				
                    var options = gx.dom.el(div2.id + '_compare_options').children;
                    var selectedOptionValue;
                    for (ind = 0; options[ind] != undefined && selectedOptionValue == undefined; ind++) {
                        if (options[ind].selected) {
                            selectedOptionValue = options[ind].value;
                        }
                    }
                    var minDateCompare;
                    var maxDateCompare;
                    if (selectedOptionValue == 'PrevPeriod') {
                        maxDateCompare = new Date(extremes.min);
                        minDateCompare = new Date(extremes.min - (extremes.max - extremes.min));
                    } else if (selectedOptionValue == 'PrevYear') {
                        minDateCompare = new Date(extremes.min);
                        minDateCompare = new Date(minDateCompare.setFullYear(minDateCompare.getFullYear() - 1));
                        maxDateCompare = new Date(extremes.max);
                        maxDateCompare = new Date(maxDateCompare.setFullYear(maxDateCompare.getFullYear() - 1));
                    }
                    minDateCompare = minDateCompare.getTime();
                    maxDateCompare = maxDateCompare.getTime();

                    // Issue 27389: muestra el cuadro de leyendas cuando se está comparando un período de tiempo, en estos
                    // casos siempre se estará viendo más de una serie.
					showLegend($this.legend);				
					
					hideZoom(viewerId + "_Zoom_0m");//Si esta habilitada la comparación oculto el zoom all
                } 
				else
					showZoom(viewerId + "_Zoom_0m");

                // Elimina todas las series existentes de la grafica
                var series_colors = []
                while ($this.series.length > 0) {
                    if (!$this.options.colors_used) {
                        series_colors.push($this.series[0].color);
                    }
                    $this.series[0].remove(true)
                }
                if (!$this.options.colors_used) {
                    $this.options.colors_used = series_colors;
                }

                // Carga las series con los datos que correspondan
                for (ns = 0; datasetnodes[ns] != undefined; ns++) {
                    var serieName = datasetnodes.length > 1 ? datasetnodes[ns].getAttribute("seriesName") : graph.getAttribute("yAxisName");

                    // Serie con el periodo seleccionado por el usuario
                    var serie1 = {};
                    serie1.color = $this.options.colors_used[ns];
                    serie1.id = serieName + "1";
                    serie1.name = serieName;
                    serie1.data = [];

                    if (compare) {
                        // Serie con el periodo contra el que se va a comparar
                        var serie2 = {};
                        serie2.id = serieName + "2";
                        serie2.name = serieName + " (" + Highcharts.dateFormat("%m/%d/%y", minDateCompare, true) + " - " + Highcharts.dateFormat("%m/%d/%y", maxDateCompare, true) + ")";
                        serie2.data = [];
                    }

                    var setnodes = datasetnodes.length > 1 ? qryViewer.getMultipleElementsByTagName(datasetnodes[ns], "set") : datasetnodes[ns];
                    for (i = 0; setnodes[i] != undefined; i++) {
                        var value = parseFloat(qryViewer.trim(setnodes[i].getAttribute("value")).replace(",", "."));
                        var date = new gx.date.gxdate(
                        (qryViewer.chartSeriesCount > 1 ? categories[i] : setnodes[i].getAttribute("hoverText")));
                        var utcValue = date.Value.getTime() + date.Value.getTimezoneOffset() * 60000;
                        var original_utc_value = date.Value.getTime() + date.Value.getTimezoneOffset() * 60000;
                        if (compare) {
                            var addToSerie1 = false;
                            var addToSerie2 = false;
                            if (utcValue <= extremes.max && utcValue >= extremes.min) {
                                addToSerie1 = true;
                            } else if (utcValue <= maxDateCompare && utcValue >= minDateCompare) {
                                // Se asegura de mo agregar un valor a las dos series
                                addToSerie2 = true;
                                var tmpDate = new Date(utcValue);
                                if (selectedOptionValue == "PrevPeriod") {
                                    if ($this.options.predef_zoom == "1m") {
                                        utcValue = tmpDate.setMonth(tmpDate.getMonth() + 1);
                                    } else if ($this.options.predef_zoom == "2m") {
                                        utcValue = tmpDate.setMonth(tmpDate.getMonth() + 2);
                                    } else if ($this.options.predef_zoom == "3m") {
                                        utcValue = tmpDate.setMonth(tmpDate.getMonth() + 3);
                                    } else if ($this.options.predef_zoom == "6m") {
                                        utcValue = tmpDate.setMonth(tmpDate.getMonth() + 6);
                                    } else if ($this.options.predef_zoom == "1y") {
                                        utcValue = tmpDate.setFullYear(tmpDate.getFullYear() + 1);
                                    } else {
                                        utcValue += extremes.max - extremes.min;
                                    }
                                } else if (selectedOptionValue == "PrevYear") {
                                    utcValue = tmpDate.setFullYear(tmpDate.getFullYear() + 1);
                                }
                            }
                            if (addToSerie1 || addToSerie2) {
                                var point = {};
                                point.x = utcValue;
                                point.y = value;
                                if (addToSerie1) {
                                    serie1.data.push(point);
                                }
                                if (addToSerie2) {
                                    point.real_x = original_utc_value;
                                    point.real_serie_name = serieName;
                                    serie2.data.push(point);
                                }
                            }
                        } else {
                            serie1.data.push([utcValue, value]);
                        }
                    }

                    $this.addSeries(serie1);
                    if (compare) {
                        $this.addSeries(serie2);
                    }
                }

                if ($this.series.length == 1) 
                    // Issue 27389: oculta el cuadro de leyendas si solo hay una serie en la grafica. Este caso puede darse
                    // cuando se comparó un período de tiempo en una gráfica con una sola serie originalmente y luego se
                    // dejó de comparar
					hideLegend($this);                
				else
					showLegend($this);
				
            }).closure(this));
        }

        gx.dom.el(div2.id + '_compare_enable').onclick = function () {
            CompareToFunction();
        };
        gx.dom.el(div2.id + '_compare_options').onchange = function () {
            if (gx.dom.el(div2.id + '_compare_enable').checked) {
                CompareToFunction();
            }
        };
 
		var doZoom = function (zoomFactor) {
		
			var compare = gx.dom.el(div2.id + '_compare_enable').checked;
		 
			if (zoomFactor > 0){
			
                var minDate, maxDate;
                var extremes = $this.get('xaxis').getExtremes();
				maxDate = extremes.max;
				minDate = maxDate - 30.417 * zoomFactor * 24 * 3600 * 1000; //30.4166 = 365dias/12meses
				/* GF: este código no es válido cuando esta habilitada la comparación pues el dataMax esta influenciado por los valores del período anterior
				https://issues.genexus.com/viewissue.aspx?41041,
                maxDate = (extremes.max > extremes.dataMax ? extremes.dataMax : extremes.max) + 10 * zoomFactor * 3600 * 1000;
                minDate = maxDate - 30.417 * zoomFactor * 24 * 3600 * 1000; //30.4166 = 365dias/12meses
                if (minDate < extremes.dataMin) {
                    minDate = extremes.dataMin - 10 * zoomFactor * 3600 * 1000;
                    maxDate = minDate + 30.417 * zoomFactor * 24 * 3600 * 1000;
                    if (maxDate > extremes.dataMax) {
                        maxDate = extremes.dataMax;
                    }
                }*/
				var redraw = (compare)? false : true;
                $this.get('xaxis').setExtremes(minDate, maxDate, redraw);
				
			}
			else //Zoom All
				$this.zoomOut();
				
			//Si esta habilitada la comparación recalculo las fechas
			if (compare)
				CompareToFunction();
				
			//Resalto el selector de zoom seleccionado
			deselectZoom(prevClickedCtrlId);
			selectZoom(this.id);			
			
			$this.options.predef_zoom = zoomFactor + "m";
			prevClickedCtrlId = this.id;			
			
        };
		
        ////////////////////////////////////////////////////////////////////////////////////////////		
        // Carga los links de zooms con los event handlers necesarios
        // Zoom automatico a x meses		
		var array_zooms = [0,1,2,3,6,12];
		for (var index in array_zooms){		
			var x = array_zooms[index];
			var zoomXm = gx.dom.el(viewerId + "_Zoom_" + x + "m");
			if (zoomXm) {
				zoomXm.onclick = doZoom.closure(zoomXm, [x]);
				zoomXm.onmouseover = mouseOver;
				zoomXm.onmouseout = mouseOut;
			}
		}
        ////////////////////////////////////////////////////////////////////////////////////////////

        // Al final, se establece por default un zoom de tres meses
		triggerZoom(3);		
			
    }
	
/******************************END ZOOMING & COMPARE***********************************************/	


    this.renderChart_JSforRenderTypeFlash = function () {
        var UCId = this.userControlId();
        // Check for not valid charts
        if ((this.RealChartType == "Pie" || this.RealChartType == "Pie3D" || this.RealChartType == "Doughnut" || this.RealChartType == "Funnel") && (this.chartSeriesCount != "1")) this.renderError("Too many series for a " + this.RealChartType + " chart");
        else {
            this.getSwfFilePath(this.chartSeriesCount, (function(swfPath) {
                var chart1 = new FusionCharts(swfPath, UCId, this.fixSize(this.Width), this.fixSize(this.Height), "0", "0");
                chart1.setDataXML(this.chartXmlData);
                chart1.setTransparent(true);
                chart1.render(this.getContainerControl().id);
            }).closure(this));
        }
    }

    this.getSwfFilePath = function (seriesCount, callback) {
        this.getSwfFileName(seriesCount, (function (swfFileName) {
            if (this.GenType == "")
                this.GenType = "C#";
            switch (this.GenType) {
                case "C#":
                    callback("QueryViewer/FusionCharts/" + swfFileName);
                    break;
                case "Java":
                    callback(this.GetJavaBaseUrl(true) + "QueryViewer/FusionCharts/" + swfFileName);
                    break;
                case "Ruby":
                    callback("QueryViewer/FusionCharts/" + swfFileName);
                    break;
                default:
                    callback("");
            }
        }).closure(this));
    }

    this.getChartDataURL = function () {
		return this.getServiceURL("getdataforchart");
    }

    this.getSeriesCountURL = function () {
		return this.getServiceURL("getseriescount");
    }

    this.getRecordsetCacheKeyURL = function () {
        return this.getServiceURL("getrecordsetcachekey");
    }
	
    this.getQueryParametersURL = function () {
		return this.getServiceURL("getqueryparameters");
    }
    
    this.getSwfFileName = function (seriesCount, callback) {
        var resolveSwf = (function (seriesCount) {
            var swf = "";
            switch (this.RealChartType) {
                case "Column":
                    if (seriesCount == "1") swf = "FCF_Column2D.swf";
                    else swf = "FCF_MSColumn2D.swf";
                    break;
                case "Column3D":
                    if (seriesCount == "1") swf = "FCF_Column3D.swf";
                    else swf = "FCF_MSColumn3D.swf";
                    break;
                case "StackedColumn":
                    if (seriesCount != "1") swf = "FCF_StackedColumn2D.swf";
                    else swf = "FCF_Column2D.swf"; // "Stacked" ignored
                    break;
                case "StackedColumn3D":
                    if (seriesCount != "1") swf = "FCF_StackedColumn3D.swf";
                    else swf = "FCF_Column3D.swf"; // "Stacked" ignored
                    break;
                case "Bar":
                    if (seriesCount == "1") swf = "FCF_Bar2D.swf";
                    else swf = "FCF_MSBar2D.swf";
                    break;
                case "StackedBar":
                    if (seriesCount != "1") swf = "FCF_StackedBar2D.swf";
                    else swf = "FCF_Bar2D.swf"; // "Stacked" ignored
                    break;
                case "Area":
                    if (seriesCount == "1") swf = "FCF_Area2D.swf";
                    else swf = "FCF_MSArea2D.swf";
                    break;
                case "StackedArea":
                    if (seriesCount != "1") swf = "FCF_StackedArea2D.swf";
                    else swf = "FCF_Area2D.swf"; // "Stacked" ignored
                    break;
                case "Line":
                    if (seriesCount == "1") swf = "FCF_Line.swf";
                    else swf = "FCF_MSLine.swf";
                    break;
                case "Pie":
                    swf = "FCF_Pie2D.swf";
                    break;
                case "Pie3D":
                    swf = "FCF_Pie3D.swf";
                    break;
                case "Doughnut":
                    swf = "FCF_Doughnut2D.swf";
                    break;
                case "Funnel":
                    swf = "FCF_Funnel.swf";
                    break;
                case "ColumnLine":
                    if (seriesCount != "1") swf = "FCF_MSColumn2DLineDY.swf";
                    else swf = "FCF_Column2D.swf"; // "Line" ignored
                    break;
                case "Column3DLine":
                    if (seriesCount != "1") swf = "FCF_MSColumn3DLineDY.swf";
                    else swf = "FCF_Column3D.swf"; // "Line" ignored
                    break;
                default:
                    if (seriesCount == "1") swf = "FCF_Column2D.swf";
                    else swf = "FCF_MSColumn2D.swf";
                    break;
            }
            return swf;
        }).closure(this);

        if (seriesCount == "") {
            this.getSeriesCount(function (seriesCount) {
                callback(resolveSwf(seriesCount));
            });
        }
        else {
            callback(resolveSwf(seriesCount));
        }
    }

    this.renderChartServices = function (xml, time) {
        var xmlEncoded = gx.html.encode(xml, true, true);
        this.AdjustSize();
        var buffer = "<div style=\"overflow:auto; border-right: silver thin solid; border-top: silver thin solid; border-left: silver thin solid; border-bottom: silver thin solid; width: " + this.Width + "px; height: " + this.Height + "px; font-size: " + this.FontSize + "px; color: " + this.getHtmlColor(this.FontColor) + "; font-family: " + this.FontFamily + "\">" + xmlEncoded + "</div>";
        buffer += "Time: " + time / 1000 + " seconds";
        this.setHtml(buffer);
    }

    // ---------------------------------------- Ajax ----------------------------------------	
	
	this.getObjectBasicInfo = function () {
		var str = "";
		str += (this.ParentObject.PackageName != '' ? '"ApplicationNamespace":"' + this.ParentObject.PackageName + '"' + ',' : '');
		
		if (this.Object) {			
			var array = eval(this.Object);
			str += '"ObjectName":"' + array[1] + '"';
		}
		else {		
			str += '"ObjectName":"' + this.ObjectName + '"';
			str += ',';
			str += '"Alt_ObjectType":"' + this.ObjectType + '"';
			str += ',';
			str += '"Alt_ObjectId":' + this.ObjectId;
		}
		
		return str;
	}	
	
    this.getQueryParametersPostInfo = function () {
        var str = "";
        str += '{';
		str += this.getObjectBasicInfo();       
		str += (this.ObjectInfo != '' ? ',' + '"ObjectInfo":' + this.ObjectInfo : '');
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.DefaultOutputPostInfo = function () {
        var str = "";
        str += '{';
        str += this.getObjectBasicInfo();
        str += (this.ObjectInfo != '' ? ',' + '"ObjectInfo":' + this.ObjectInfo : '');
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.SeriesCountPostInfo = function () {
        var str = "";
        var runtimeFieldsJSON = this.RuntimeFieldsJSON(true);
        str += '{';
        str += this.getObjectBasicInfo();
        str += (this.QueryInfo != '' ? ',' + '"QueryInfo":' + this.QueryInfo : '');
        str += (runtimeFieldsJSON != '' ? ',' + runtimeFieldsJSON : '');
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.RecordsetCacheKeyPostInfo = function () {
        return "";
    }
	
    this.ChartDataPostInfo = function () {
        var str = "";
        var runtimeFieldsJSON = this.RuntimeFieldsJSON(true);
        var runtimeParametersJSON = this.RuntimeParametersJSON();
        str += '{';        
        str += this.getObjectBasicInfo();
        str += (this.QueryInfo != '' ? ',' + '"QueryInfo":' + this.QueryInfo : '');
        str += (this.AppSettings != '' ? ',' + '"AppSettings":' + this.AppSettings : '');
        str += (runtimeFieldsJSON != '' ? ',' + runtimeFieldsJSON : '');
        str += (runtimeParametersJSON != '' ? ',' + runtimeParametersJSON : '');
        str += ',';
        str += '"ChartType":"' + this.RealChartType + '"';
        str += ',';
        str += '"FontFamily":"' + this.FontFamily + '"';
        str += ',';
        str += '"FontColor":"' + this.getHtmlColor(this.FontColor).replace("#", "") + '"';
        str += ',';
        str += '"FontSize":"' + this.FontSize + '"';
        str += ',';
        str += '"RotateNames":"' + (this.XAxisLabels == "Vertically" ? "1" : "0") + '"';
        str += ',';
        str += '"ShowValues":"' + (gx.lang.gxBoolean(this.ShowValues) ? "1" : "0") + '"';
        str += ',';
        str += '"AllowAxesOrderChange":' + this.AllowChangeAxesOrder;
        str += ',';
        str += '"UseCache":' + this.UseCache;
        str += ',';
        str += '"Width":"' + this.Width + '"';

        if ((this.trim(this.XAxisTitle) != "") && (this.trim(this.XAxisTitle) != null)) {
            str += ',';
            str += '"XAxisTitle":"' + this.XAxisTitle + '"';

        }
        if ((this.trim(this.YAxisTitle) != "") && (this.trim(this.YAxisTitle) != null)) {
            str += ',';
            str += '"YAxisTitle":"' + this.YAxisTitle + '"';
        }
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.PivottableMetadataPostInfo = function () {
        var str = "";
        var runtimeFieldsJSON = this.RuntimeFieldsJSON(true);
        str += '{';
        str += this.getObjectBasicInfo();
        str += (this.QueryInfo != '' ? ',' + '"QueryInfo":' + this.QueryInfo : '');
        str += (runtimeFieldsJSON != '' ? ',' + runtimeFieldsJSON : '');
        str += ',';
        str += '"TableType":"' + this.RealType + '"';
        str += ',';
        str += '"AllowAxesOrderChange":' + this.AllowChangeAxesOrder;
        str += ',';
        str += '"RememberLayout":' + this.RememberLayout;
        str += ',';
        str += '"ShowDataLabelsIn":"' + this.ShowDataLabelsIn + '"';
        str += ',';
        str += '"ShowAxesSelectors":"' + this.ShowAxesSelectors + '"';
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.PivottableDataPostInfo = function () {
        var str = "";
        var runtimeParametersJSON = this.RuntimeParametersJSON();
        var runtimeFieldsJSON = this.RuntimeFieldsJSON(true);
        str += '{';
        str += this.getObjectBasicInfo();
        str += (this.QueryInfo != '' ? ',' + '"QueryInfo":' + this.QueryInfo : '');
        str += (this.AppSettings != '' ? ',' + '"AppSettings":' + this.AppSettings : '');
        str += (runtimeParametersJSON != '' ? ',' + runtimeParametersJSON : '');
        str += (runtimeFieldsJSON != '' ? ',' + runtimeFieldsJSON : '');
        str += ',';
        str += '"TableType":"' + this.RealType + '"';
        str += ',';
        str += '"AllowAxesOrderChange":' + this.AllowChangeAxesOrder;
        str += ',';
        str += '"UseCache":' + this.UseCache;
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.AttributeValuesPostInfo = function (DataField, PageNumber, PageSize, Filter) {
        var str = "";
        var runtimeParametersJSON = this.RuntimeParametersJSON();
        var runtimeFieldsJSON = this.RuntimeFieldsJSON(true);
        str += '{';
        str += this.getObjectBasicInfo();
        str += ',';
        str += '"DataField":"' + DataField + '"';
        str += ',';
        str += '"PageNumber":' + PageNumber;
        str += ',';
        str += '"PageSize":' + PageSize;
        str += ',';
        str += '"Filter":"' + Filter + '"';
        str += (this.QueryInfo != '' ? ',' + '"QueryInfo":' + this.QueryInfo : '');
        str += (this.AppSettings != '' ? ',' + '"AppSettings":' + this.AppSettings : '');
        str += (runtimeParametersJSON != '' ? ',' + runtimeParametersJSON : '');
        str += (runtimeFieldsJSON != '' ? ',' + runtimeFieldsJSON : '');
        str += ',';
        str += '"TableType":"' + this.RealType + '"';
        str += ',';
        str += '"AllowAxesOrderChange":' + this.AllowChangeAxesOrder;
        str += ',';
        str += '"UseCache":' + this.UseCache;
        str += ',';
        str += '"RecordsetCacheInfo":' + this.RecordsetCacheInfoJSON();
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.TablePageDataPostInfo = function (PageNumber, PageSize, ReturnTotPages, DataFieldOrder, OrderType, Filters) {
        var str = "";
        var runtimeParametersJSON = this.RuntimeParametersJSON();
        var runtimeFieldsJSON = this.RuntimeFieldsJSON(true);
        var FiltersJSON = this.FiltersJSON(Filters);
        str += '{';
        str += this.getObjectBasicInfo();
        str += ',';
        str += '"PageNumber":' + PageNumber;
        str += ',';
        str += '"PageSize":' + (gx.lang.gxBoolean(this.Paging) ? PageSize : 0);
        str += ',';
        str += '"ReturnTotPages":' + ReturnTotPages;
        str += ',';
        str += '"Order":' + this.OrderJSON(DataFieldOrder, OrderType);
        str += (FiltersJSON != '' ? ',' + FiltersJSON : '');
        str += (this.QueryInfo != '' ? ',' + '"QueryInfo":' + this.QueryInfo : '');
        str += (this.AppSettings != '' ? ',' + '"AppSettings":' + this.AppSettings : '');
        str += (runtimeParametersJSON != '' ? ',' + runtimeParametersJSON : '');
        str += (runtimeFieldsJSON != '' ? ',' + runtimeFieldsJSON : '');
        str += ',';
        str += '"AllowAxesOrderChange":' + this.AllowChangeAxesOrder;
        str += ',';
        str += '"UseCache":' + this.UseCache;
        str += ',';
        str += '"RecordsetCacheInfo":' + this.RecordsetCacheInfoJSON();
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }

    this.PivotTablePageDataPostInfo = function (PageNumber, PageSize, ReturnTotPages, AxesInfo, Filters, ExpandCollapse , LayoutChanged) {
        var str = "";
        var runtimeParametersJSON = this.RuntimeParametersJSON();
        var runtimeFieldsJSON = this.RuntimeFieldsJSON(true);
        var AxesInfoJSON = this.AxesInfoJSON(AxesInfo);
        var FiltersJSON = this.FiltersJSON(Filters);
        var ExpandCollapseJSON = this.ExpandCollapseJSON(ExpandCollapse);
        str += '{';
        str += this.getObjectBasicInfo();
        str += ',';
        str += '"PageNumber":' + PageNumber;
        str += ',';
        str += '"PageSize":' + (gx.lang.gxBoolean(this.Paging) ? PageSize : 0);
        str += ',';
        str += '"ReturnTotPages":' + ReturnTotPages;
        str += ',';
        str += '"ShowDataLabelsIn":"' + this.ShowDataLabelsIn + '"';
        str += (AxesInfoJSON != '' ? ',' + AxesInfoJSON : '');
        str += (FiltersJSON != '' ? ',' + FiltersJSON : '');
        str += (ExpandCollapseJSON != '' ? ',' + ExpandCollapseJSON : '');
        str += (this.QueryInfo != '' ? ',' + '"QueryInfo":' + this.QueryInfo : '');
        str += (this.AppSettings != '' ? ',' + '"AppSettings":' + this.AppSettings : '');
        str += (runtimeParametersJSON != '' ? ',' + runtimeParametersJSON : '');
        str += (runtimeFieldsJSON != '' ? ',' + runtimeFieldsJSON : '');
        str += ',';
        str += '"AllowAxesOrderChange":' + this.AllowChangeAxesOrder;
        str += ',';
        str += '"UseCache":' + this.UseCache;
        str += ',';
        str += '"RecordsetCacheInfo":' + this.RecordsetCacheInfoJSON();
        str += ',';
        str += '"LayoutChanged":' + LayoutChanged;
        str += '}';
        return "Data=" + encodeURIComponent(str);
    }
	
	this.RecordsetCacheInfoJSON = function() {
            if (this.UseRecordsetCache)
                return '{"ActualKey":"' + this.RecordsetCacheActualKey + '","OldKey":"' + this.RecordsetCacheOldKey + '","MinutesToKeep":' + this.MinutesToKeepInRecordsetCache + ',"MaximumSize":' + this.MaximumCacheSize + '}';
            else
                return '{"ActualKey":"","OldKey":"","MinutesToKeep":0,"MaximumSize":0}';
	}

	this.OrderJSON = function(DataField, Type) {
		return '{"DataField":"' + DataField + '","Type":"' + Type + '"}';
	}

    this.AxesInfoJSON = function (AxesInfo) {
        var strAxesInfo = '';
        var existAxesInfo = false;
        for (var i = 0; AxesInfo[i] != undefined; i++) {
            existAxesInfo = true;
            strAxesInfo += (strAxesInfo != '' ? ',' : '');
            strAxesInfo += '{';
            strAxesInfo += '"DataField":"' + AxesInfo[i].DataField + '"';
            strAxesInfo += ',';
            strAxesInfo += '"Axis":{"Type":"' + AxesInfo[i].Axis.Type + '","Position":' + AxesInfo[i].Axis.Position + '}';
            strAxesInfo += ',';
            strAxesInfo += '"Order":"' + AxesInfo[i].Order + '"';
            strAxesInfo += ',';
            strAxesInfo += '"Subtotals":' + AxesInfo[i].Subtotals;
            strAxesInfo += '}';
        }
        if (existAxesInfo) strAxesInfo = '"AxesInfo":[' + strAxesInfo + ']';
        return strAxesInfo;
    }

    this.FiltersJSON = function (Filters) {
        var strFilters = '';
        var existFilters = false;
        for (i = 0; Filters[i] != undefined; i++) {
            existFilters = true;
            strFilters += (strFilters != '' ? ',' : '');
            strFilters += '{';
            strFilters += '"DataField":"' + Filters[i].DataField + '"';
            strFilters += ',';
            strFilters += '"NullIncluded":' + Filters[i].NullIncluded;
            strIncludedValues = "";
			existValues = false;
			for (j = 0; Filters[i].NotNullValues.Included[j] != undefined; j++) {
				existValues = true;
				strIncludedValues += (strIncludedValues != '' ? ',' : '');
				strIncludedValues += '"' + encodeURIComponent(Filters[i].NotNullValues.Included[j]) + '"';
			}
			if (existValues) strIncludedValues = ',' + '"Included":[' + strIncludedValues + ']';
            strExcludedValues = "";
			existValues = false;
			for (j = 0; Filters[i].NotNullValues.Excluded[j] != undefined; j++) {
				existValues = true;
				strExcludedValues += (strExcludedValues != '' ? ',' : '');
				strExcludedValues += '"' + encodeURIComponent(Filters[i].NotNullValues.Excluded[j]) + '"';
			}
			if (existValues) strExcludedValues = ',' + '"Excluded":[' + strExcludedValues + ']';
            strFilters += ',';
            strFilters += '"NotNullValues":{"DefaultAction":"' + Filters[i].NotNullValues.DefaultAction + '"' + strIncludedValues + strExcludedValues + '}';
            strFilters += '}';
        }
        if (existFilters) strFilters = '"Filters":[' + strFilters + ']';
        return strFilters;
    }

    this.ExpandCollapseJSON = function (ExpandCollapse) {
        var strExpandCollapse = '';
        var existExpandCollapse = false;
        for (var i = 0; ExpandCollapse[i] != undefined; i++) {
            existExpandCollapse = true;
            strExpandCollapse += (strExpandCollapse != '' ? ',' : '');
            strExpandCollapse += '{';
            strExpandCollapse += '"DataField":"' + ExpandCollapse[i].DataField + '"';
            strExpandCollapse += ',';
            strExpandCollapse += '"NullExpanded":' + ExpandCollapse[i].NullExpanded;
            strExpandedValues = "";
			existValues = false;
			for (var j = 0; ExpandCollapse[i].NotNullValues.Expanded[j] != undefined; j++) {
				existValues = true;
				strExpandedValues += (strExpandedValues != '' ? ',' : '');
				strExpandedValues += '"' + encodeURIComponent(ExpandCollapse[i].NotNullValues.Expanded[j]) + '"';
			}
			if (existValues) strExpandedValues = ',' + '"Expanded":[' + strExpandedValues + ']';
            strCollapsedValues = "";
			existValues = false;
			for (var j = 0; ExpandCollapse[i].NotNullValues.Collapsed[j] != undefined; j++) {
				existValues = true;
				strCollapsedValues += (strCollapsedValues != '' ? ',' : '');
				strCollapsedValues += '"' + encodeURIComponent(ExpandCollapse[i].NotNullValues.Collapsed[j]) + '"';
			}
			if (existValues) strCollapsedValues = ',' + '"Collapsed":[' + strCollapsedValues + ']';
            strExpandCollapse += ',';
            strExpandCollapse += '"NotNullValues":{"DefaultAction":"' + ExpandCollapse[i].NotNullValues.DefaultAction + '"' + strExpandedValues + strCollapsedValues + '}';
            strExpandCollapse += '}';
        }
        if (existExpandCollapse) strExpandCollapse = '"ExpandCollapse":[' + strExpandCollapse + ']';
        return strExpandCollapse;
    }
	
    this.RuntimeParametersJSON = function () {
        var strParameters = '';
        var existParameters = false;
		
		if (this.Object) {
			var array = eval(this.Object);
			for (i = 2; i < array.length; i++) {
				existParameters = true;						
				strParameters += (strParameters != '' ? ',' : '');
				strParameters += '{';
				strParameters += '"Value":"' + encodeURIComponent(array[i]) + '"';
				strParameters += '}';
			}
		}
		else {		
			for (i = 0; this.Parameters[i] != undefined; i++) {
				existParameters = true;
				strParameters += (strParameters != '' ? ',' : '');
				strParameters += '{';
				strParameters += '"Name":"' + this.Parameters[i].Name + '"';
				strParameters += ',';
				strParameters += '"Value":"' + encodeURIComponent(this.Parameters[i].Value) + '"';
				strParameters += '}';
			}
		}
        if (existParameters) strParameters = '"RuntimeParameters":[' + strParameters + ']';
        return strParameters;
    }

    this.ValuesStylesJSON = function (axis) {
        var strStyles = '';
        var existStyles = false;
        if (axis.Format != undefined) {
            if (axis.Format.ValuesStyles != undefined) {
                for (var i = 0; axis.Format.ValuesStyles[i] != undefined; i++) {
                    existStyles = true;
                    strStyles += (strStyles != '' ? ',' : '');
                    strStyles += '{';
                    strStyles += '"Value":"' + encodeURIComponent(axis.Format.ValuesStyles[i].Value) + '"';
                    strStyles += ',';
                    strStyles += '"Style":"' + encodeURIComponent(axis.Format.ValuesStyles[i].StyleOrClass) + '"';
                    strStyles += ',';
                    strStyles += '"Propagate":' + (axis.Format.ValuesStyles[i].ApplyToFileOrColumn ? "true" : "false");
                    strStyles += '}';
                }
            }
        }
        if (existStyles) strStyles = '"ValuesStyles":[' + strStyles + ']';
        return strStyles;
    }

    this.ConditionalStylesJSON = function (axis) {
        var strStyles = '';
        var existStyles = false;
        if (axis.Format != undefined) {
            if (axis.Format.ConditionalStyles != undefined) {
                for (var i = 0; axis.Format.ConditionalStyles[i] != undefined; i++) {
                    existStyles = true;
                    strStyles += (strStyles != '' ? ',' : '');
                    strStyles += '{';
                    strStyles += '"Operator":"' + axis.Format.ConditionalStyles[i].Operator + '"';
                    strStyles += ',';
                    strStyles += '"Value1":"' + encodeURIComponent(axis.Format.ConditionalStyles[i].Value1) + '"';
                    if (axis.Format.ConditionalStyles[i].Operator == "IN") {
                        strStyles += ',';
                        strStyles += '"Value2":"' + encodeURIComponent(axis.Format.ConditionalStyles[i].Value2) + '"';
                    }
                    strStyles += ',';
                    strStyles += '"Style":"' + encodeURIComponent(axis.Format.ConditionalStyles[i].StyleOrClass) + '"';
                    strStyles += '}';
                }
            }
        }
        if (existStyles) strStyles = '"ConditionalStyles":[' + strStyles + ']';
        return strStyles;
    }

    this.GroupingJSON = function(axis) {
        var strGrouping = '';
        if (axis.Grouping != undefined) {
            strGrouping += '{';
            strGrouping += '"GroupByYear":' + (axis.Grouping.GroupByYear ? "true" : "false");
            strGrouping += ',';
            strGrouping += '"YearTitle":"' + axis.Grouping.YearTitle + '"';
            strGrouping += ',';
            strGrouping += '"GroupBySemester":' + (axis.Grouping.GroupBySemester ? "true" : "false");
            strGrouping += ',';
            strGrouping += '"SemesterTitle":"' + axis.Grouping.SemesterTitle + '"';
            strGrouping += ',';
            strGrouping += '"GroupByQuarter":' + (axis.Grouping.GroupByQuarter ? "true" : "false");
            strGrouping += ',';
            strGrouping += '"QuarterTitle":"' + axis.Grouping.QuarterTitle + '"';
            strGrouping += ',';
            strGrouping += '"GroupByMonth":' + (axis.Grouping.GroupByMonth ? "true" : "false");
            strGrouping += ',';
            strGrouping += '"MonthTitle":"' + axis.Grouping.MonthTitle + '"';
            strGrouping += ',';
            strGrouping += '"GroupByDayOfWeek":' + (axis.Grouping.GroupByDayOfWeek ? "true" : "false");
            strGrouping += ',';
            strGrouping += '"DayOfWeekTitle":"' + axis.Grouping.DayOfWeekTitle + '"';
            strGrouping += ',';
            strGrouping += '"HideValue":' + (axis.Grouping.HideValue ? "true" : "false");
            strGrouping += '}';
            strGrouping = '"Grouping":' + strGrouping;
        }
        return strGrouping;
    }

    this.RuntimeFieldsJSON = function(encodevaluescollection) {
        var strFields = '';
        var strValues = '';
        var existFields;
        var existValues;
        var picture;
        var color;
        var style;
        var subtotals;
        var CanDragToPages;
        var orderType;
        var expandCollapseType;
        var filterType;
        existFields = false;
        for (i = 0; this.Axes[i] != undefined; i++) {
            existFields = true;
            strFields += (strFields != '' ? ',' : '');
            strFields += '{';
            picture = '';
            color = '';
            style = '';
            subtotals = '';
            CanDragToPages = '';
            orderType = '';
            expandCollapseType = '';
            filterType = '';
            var valuesStyles = this.ValuesStylesJSON(this.Axes[i]);
            var conditionalStyles = this.ConditionalStylesJSON(this.Axes[i]);
            var grouping = this.GroupingJSON(this.Axes[i]);
            if (this.Axes[i].Format != undefined) {
                picture = (this.Axes[i].Format.Picture ? this.Axes[i].Format.Picture : "");
                // color = this.getHtmlColor(this.Axes[i].Format.Color); Color property is deprecated (PMusso)
                style = (this.Axes[i].Format.Style ? this.Axes[i].Format.Style : "");
                subtotals = (this.Axes[i].Format.Subtotals ? this.Axes[i].Format.Subtotals : "");
                CanDragToPages = (this.Axes[i].Format.CanDragToPages ? this.Axes[i].Format.CanDragToPages : "");
            }
            if (this.Axes[i].AxisOrder != undefined) {
                orderType = this.Axes[i].AxisOrder.Type;
            }
            if (this.Axes[i].ExpandCollapse != undefined) {
                expandCollapseType = this.Axes[i].ExpandCollapse.Type;
            }
            if (this.Axes[i].Filter != undefined) {
                filterType = this.Axes[i].Filter.Type;
            }
            if (filterType == undefined) filterType = "ShowSomeValues";

            strFields += '"Name":"' + encodeURIComponent(this.Axes[i].Name) + '"';
            strFields += ',';
            strFields += '"Caption":"' + encodeURIComponent(this.Axes[i].Title) + '"';
            strFields += ',';
            var t = this.Axes[i].Type;
            strFields += '"Axis":{"Type":"' + (t == "Row" ? "Rows" : (t == "Column" ? "Columns" : (t == "Page" ? "Pages" : (t == "NotShow" ? "Hidden" : t)))) + '"}';
            strFields += ',';
            strFields += '"Picture":"' + encodeURIComponent(picture) + '"';
            strFields += ',';
            strFields += '"Color":"' + color + '"';
            strFields += ',';
            strFields += '"Style":"' + style + '"';
            strFields += ',';
            strFields += '"Subtotals":"' + subtotals + '"';
            strFields += ',';
            strFields += '"CanDragToPages":"' + CanDragToPages + '"';
            if (orderType != '') {
                var sdtOrderType = this.SdtWithValuesJSON("Order", orderType, "Custom", this.Axes[i].AxisOrder.Values, encodevaluescollection);
                strFields += (sdtOrderType != '' ? ',' + sdtOrderType : '');
            }
            if (expandCollapseType != '') {
                var sdtExpandCollapse = this.SdtWithValuesJSON("ExpandCollapse", expandCollapseType, "ExpandSomeValues", this.Axes[i].ExpandCollapse.Values, encodevaluescollection);
                strFields += (sdtExpandCollapse != '' ? ',' + sdtExpandCollapse : '');
            }
            if (filterType != '' && this.Axes[i].Filter.Values) {
                var sdtFilter = this.SdtWithValuesJSON("Filter", filterType, "ShowSomeValues", this.Axes[i].Filter.Values, encodevaluescollection);
                strFields += (sdtFilter != '' ? ',' + sdtFilter : '');
            }
            strFields += (valuesStyles != '' ? ',' + valuesStyles : '');
            strFields += (conditionalStyles != '' ? ',' + conditionalStyles : '');
            strFields += (grouping != '' ? ',' + grouping : '');
            strFields += '}';
        }
        if (existFields) strFields = '"RuntimeFields":[' + strFields + "]";
        return strFields;
    }

    this.SdtWithValuesJSON = function (sdtName, actualType, typeWithValues, values, encodevalues) {
        var strAux = '';
        if (actualType != '') {
            strAux += '"' + sdtName + '":';
            strAux += '{';
            strAux += '"Type":"' + actualType + '"';
            strValues = "";
            if (actualType == typeWithValues) {
                existValues = false;
                for (j = 0; values[j] != undefined; j++) {
                    existValues = true;
                    strValues += (strValues != '' ? ',' : '');
                    if (encodevalues) strValues += '"' + encodeURIComponent(values[j]) + '"';
                    else strValues += '"' + values[j] + '"';
                }
                if (existValues) strValues = ',' + '"Values":[' + strValues + ']';
            }
            strAux += strValues;
            strAux += '}';
        }
        return strAux;
    }

    this.xmlDocument = function (text) {
        var xmlDoc;
        if (!gx.util.browser.isIE() || gx.util.browser.ieVersion() >= 12) {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(text, "text/xml");
        } else // Internet Explorer
        {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = "false";
            xmlDoc.loadXML(text);
        }
        return xmlDoc;
    }

    this.getXMLHttpRequest = function () {
        if (window.XMLHttpRequest) {
            return new window.XMLHttpRequest;
        } else {
            try {
                return new ActiveXObject("MSXML2.XMLHTTP.3.0");
            } catch (ex) {
                return null;
            }
        }
    }
    
    // Asynchronous Ajax Calls
    this.calculatePivottableData_JS = this.createServerCallFn(this.getPivotTableDataURL, this.PivottableDataPostInfo, "GetDataForPivotTable");

    this.calculatePivottableMetadata = this.createServerCallFn(this.getPivotTableMetadataURL, this.PivottableMetadataPostInfo, "GetMetadataForPivotTable", this.replaceCssClasses);

    this.calculatePivottableMetadata_JS = this.createServerCallFn(this.getPivotTableMetadataURL, this.PivottableMetadataPostInfo, "GetMetadataForPivotTable", this.replaceCssClasses);

    this.calculatePivottableData = this.createServerCallFn(this.getPivotTableDataURL, this.PivottableDataPostInfo, "GetDataForPivotTable");

    this.getDefaultOutput = this.createServerCallFn(this.getDefaultOutputURL, this.DefaultOutputPostInfo, "GetDefaultOutput");

    this.getSeriesCount = this.createServerCallFn(this.getSeriesCountURL, this.SeriesCountPostInfo, "GetSeriesCount");
    
    this.getSeriesCount_JS = this.createServerCallFn(this.getSeriesCountURL, this.SeriesCountPostInfo, "GetSeriesCount");

    this.getChartData = this.createServerCallFn(this.getChartDataURL, this.ChartDataPostInfo, "GetDataForChart");
    
    this.getChartData_JS = this.createServerCallFn(this.getChartDataURL, this.ChartDataPostInfo, "GetDataForChart");

    this.getQueryParameters = this.createServerCallFn(this.getQueryParametersURL, this.getQueryParametersPostInfo, "GetQueryParameters");

    this.getQueryParameters2 = this.createServerCallFn(this.getQueryParametersURL, this.getQueryParametersPostInfo, "GetQueryParameters");
	
    this.getAttributeValues = this.createServerCallFn(this.getAttributeValuesURL, this.AttributeValuesPostInfo, "GetAttributeValues");

    this.getPageDataForTable = this.createServerCallFn(this.getPageDataForTableURL, this.TablePageDataPostInfo, "GetPageDataForTable");

    this.getPageDataForPivotTable = this.createServerCallFn(this.getPageDataForPivotTableURL, this.PivotTablePageDataPostInfo, "GetPageDataForPivotTable");

    this.getRecordsetCacheKey = this.createServerCallFn(this.getRecordsetCacheKeyURL, this.RecordsetCacheKeyPostInfo, "GetRecordsetCacheKey");
	
    // Synchronous Ajax Calls
    this.getPivottableData_JS = function () {return this.CallServerSync(this.getPivotTableDataURL, this.PivottableDataPostInfo)};
    
    this.getPivotTableMetadataSync = function () {return this.CallServerSync(this.getPivotTableMetadataURL, this.PivottableMetadataPostInfo)};

    this.getAttributeValuesSync = function(postInfoParms) {return this.CallServerSync(this.getAttributeValuesURL, this.AttributeValuesPostInfo, postInfoParms)};
	
    this.getPageDataForTableSync = function(postInfoParms) {return this.CallServerSync(this.getPageDataForTableURL, this.TablePageDataPostInfo, postInfoParms)};
	
    this.getPageDataForPivotTableSync = function(postInfoParms) {return this.CallServerSync(this.getPageDataForPivotTableURL, this.PivotTablePageDataPostInfo, postInfoParms)};
	
    this.getRecordsetCacheKeySync = function () {return this.CallServerSync(this.getRecordsetCacheKeyURL, this.RecordsetCacheKeyPostInfo)};
	
}
/* END OF FILE - ..\QueryViewerRender.src.js - */
/* END OF FILE - ..\QueryViewerRender.src.js - */
