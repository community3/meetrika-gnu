﻿/* START OF FILE - ..\xpertPivot\swf.src.js - */
if (!swfBridge){var swfBridge={}, gxpl_isIE = (navigator.appVersion.indexOf("MSIE") != -1), gxpl_isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1), gxpl_isOpera = (navigator.userAgent.indexOf("Opera") != -1);
	function applicationReady(id)				{if(!swfBridge[id]){swfBridge[id]={ready:false,initializers:[],listeners:{}}}swfBridge[id].ready=true;for(var i=0;i<swfBridge[id].initializers.length;i++){swfBridge[id].initializers[i](document.getElementById(id));}}
	function addSWFInitializer(id,fn)			{if(!swfBridge[id]){swfBridge[id]={ready:false,initializers:[],listeners:{}}}if(swfBridge[id].ready){fn(document.getElementById(id))}else{swfBridge[id].initializers.push(fn);}}
	function addSWFListener(id,evt,fn)			{if(!swfBridge[id]){swfBridge[id]={ready:false,initializers:[],listeners:{}}}if(!swfBridge[id].listeners[evt]){swfBridge[id].listeners[evt]=[]}swfBridge[id].listeners[evt].push(fn);}
	function onSWFEvent(id,evt,info)			{if(!swfBridge[id]){swfBridge[id]={ready:false,initializers:[],listeners:{}}}if(swfBridge[id].listeners[evt]){for(var i=0;i<swfBridge[id].listeners[evt].length;i++){swfBridge[id].listeners[evt][i](document.getElementById(id),info);}}}
	function notifyEvent(id,c,e,d)				{onSWFEvent(id,e,{control:c,event:e,data:d});}
	function swf(id,a,x,y,w,h,v,initer,listeners,c) { 	
		if (isPivotLoaded(id)) { 
			applicationReady(id);
		} else { 		
			if (!detectFlash(10,0,0)) {if (!detectFlash(6,0,65)) {installFlash(id,a,x,y,w,h,v,c);return;}upgradeFlash(id,a,x,y,w,h,v,c);return;}
			fixSizes(c,x,y,w,h); flashObject(id,a,x,y,w,h,v,c); addSWFInitializer(id,initer); for(var l in listeners){addSWFListener(id,l,listeners[l]);}
		}
	} 
	function flashObject(id,a,x,y,w,h,v,c) 		{
		var strHTML='\n';
		if (gxpl_isIE && gxpl_isWin && !gxpl_isOpera) {
			strHTML+='<object id="'+id+'" width="'+w+'" height="'+h+'" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">\n';
			strHTML+='\t<param name="FlashVars" 		value="id='+id+"&"+v+'">\n';
			strHTML+='\t<param name="src" 				value="'+a+'">\n';
			strHTML+='\t<param name="allowScriptAccess" value="always">\n';
			strHTML+='\t<param name="wmode" 			value="transparent">\n';
			strHTML+='\t<param name="quality" 			value="best">\n';
			strHTML+='\t<param name="menu" 				value="false">\n';
			strHTML+='</object>\n';
			if (c){c.innerHTML=strHTML}else{document.write(strHTML);}
			//window.attachEvent("onbeforeprint", function(){ var obj = document.getElementById(id); obj.beforePrint(); });
			//window.attachEvent("onafterprint" , function(){ var obj = document.getElementById(id); obj.afterPrint();  });
		} else {
			strHTML+='<embed id="'+id+'" name="'+id+'" src="'+a+'" FlashVars="id='+id+"&"+v+'" menu="false" allowScriptAccess="allways" bgcolor="#FFFFFF" quality="best" pluginspage="http://www.adobe.com/go/getflashplayer" type="application/x-shockwave-flash" width="'+w+'" height="'+h+'" swLiveConnect="true" wmode="transparent"></embed>\n';
			if (c){c.innerHTML=strHTML}else{document.write(strHTML);}
		}		
	}
	function fixSizes(c,x,y,w,h) 				{
		try{
			//******************************************************
			//CODIGO INTRUSIVO QUE CORRIGE DIMENSIONES DE TABLAS HTML
			if(x==undefined){x="0"}
			if(y==undefined){y="0"}
			if(w==undefined||w=='100%'){w='99.6%'}
			if(h==undefined||h=='100%'){h='98.7%'}
			if (h.indexOf('%')>=0&&gxpl_isIE&&gxpl_isWin&&!gxpl_isOpera){h=(document.body.offsetHeight-190)*Number(h.replace('%',''))/100;}
			var container=c;
			while (container!=document.body) {
				if (container.tagName=='TABLE'||container.tagName=='TR'||container.tagName=='TD'||container.tagName=='DIV') {
					if (w.indexOf('%')>=0){container.style.width  = w;}
					if (h.indexOf('%')>=0){container.style.height = h;}
				}
				container=container.parentNode;
			}
			//FIN DEL CODIGO INTRUSIVO
			//***********************
			c.width=w;c.height=h;
		}catch(e){}
	}
	function printPreview(objId, imgData, W,H) {
		return;		// Feature disabled
		//var obj 			= document.getElementById(objId); if (obj == null && document.embeds != null) { obj = document.embeds[objId]; } if (obj == null) { return; }
		//var parent 			= obj.parentNode; if (parent == null) { return; }
		//var head 			= document.getElementsByTagName('head'); head = ((head.length != 1) ? null : head[0]);
		//var style 			= document.createElement('style'); style.setAttribute('type','text/css'); style.setAttribute('media','screen');
		//var imgDescriptor 	= 'img#'+objId+'_screen';
		//var imgRule 		= "width: "+W+";\n"+"height: "+H+";\n"+"padding: 0;\n"+"margin: 0;\n"+"border: 0;\n"+"display: none;"; style.appendChild(document.createTextNode(imgDescriptor + '{' + imgRule + "}\n")); head.appendChild(style); style = document.createElement('style'); style.setAttribute('type','text/css'); style.setAttribute('media','print'); imgDescriptor = 'img#'+objId+'_screen'; imgRule = 'display: block;'; style.appendChild(document.createTextNode(imgDescriptor + '{' + imgRule + '}'));
		//var objDescriptor 	= 'embed#'+objId; var objRule = 'display: none;'; style.appendChild(document.createTextNode(objDescriptor + '{' + objRule + '}')); head.appendChild(style);
		//var needAppend 		= false; var img = document.getElementById(objId+"_screen"); if (img == null) { img = document.createElement('img'); img.setAttribute('id',objId+"_screen"); needAppend = true; } img.src = 'data:image/png;base64,'+imgData; if (needAppend) { parent.appendChild(img); }
	}
	function fixBoolean(v,d)					{try{if(typeof(v)=='boolean'){return v;}if(typeof(v)=='string'){return (v.toLowerCase()=='true')}}catch(e){}return d;}
	function isPivotLoaded(id)					{try{var ctl=document.getElementById(id);if(ctl){if(gxpl_isIE){return ctl.object.Movie.indexOf("xpertPivot.swf") >= 0;}else{return ctl.src.indexOf("xpertPivot.swf") >= 0;}}}catch(e){}return false;}
	function installFlash(id,a,x,y,w,h,v,c)		{var strHTML = 'This content requires the Adobe Flash Player. <a href=http://www.adobe.com/go/getflash/>Get Flash</a>';if (c){c.innerHTML=strHTML;c.style.width=w;c.style.height=h;}else{document.write(strHTML);}}
	function upgradeFlash(id,a,x,y,w,h,v,c)		{document.title = document.title.slice(0, 47) + ' - Flash Player Installation';flashObject(id,a.replace('xpertPivot','playerProductInstall'),x,y,w,h,'MMredirectURL='+window.location+'&MMplayerType='+(gxpl_isIE?'ActiveX':'PlugIn')+'&MMdoctitle='+document.title,c);}
	function detectFlash(major,minor,revision) 	{
		if (navigator.plugins != null && navigator.plugins.length > 0 && (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"])) {
			var flashDescription = navigator.plugins["Shockwave Flash 2.0"]?navigator.plugins["Shockwave Flash 2.0"].description:navigator.plugins["Shockwave Flash"].description;
			var descArray        = flashDescription.split(" ");
			var tempArrayMajor   = descArray[2].split(".");			
			var versionMajor     = tempArrayMajor[0]; 
			var versionMinor     = tempArrayMajor[1];
			var versionRevision  = descArray[3];
			if (versionRevision == "") { versionRevision = descArray[4]; }
			if (versionRevision[0] == "d") { versionRevision = versionRevision.substring(1);} else if (versionRevision[0] == "r") { versionRevision = versionRevision.substring(1); if (versionRevision.indexOf("d") > 0) { versionRevision = versionRevision.substring(0, versionRevision.indexOf("d")); } }
			return (major < versionMajor) || (major == versionMajor && minor < versionMinor) || (major == versionMajor && minor == versionMinor && revision < versionRevision);
		} else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") != -1) { return  major <= 4; 
		} else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") != -1) { return  major <= 3;
		} else if (navigator.userAgent.toLowerCase().indexOf("webtv")     != -1) { return  major <= 2;
		} else if ( gxpl_isIE && gxpl_isWin && !gxpl_isOpera ) { 
			var version, axo;
			if (!version) { try { axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7"); version = axo.GetVariable("$version");  																} catch (e) { } }
			if (!version) { try { axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6"); version = "WIN 6,0,21,0"; axo.AllowScriptAccess = "always"; version = axo.GetVariable("$version"); 	} catch (e) { } }
			if (!version) { try { axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3"); version = axo.GetVariable("$version"); 																} catch (e) { } }
			if (!version) { try { axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3"); version = "WIN 3,0,18,0"; 																			} catch (e) { } }
			if (!version) { try { axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");   version = "WIN 2,0,0,11"; 																			} catch (e) {return false;} }
			tempArray             = version.split(" "); 	// ["WIN", "2,0,0,11"]
			tempString            = tempArray[1];			// "2,0,0,11"
			versionArray      	  = tempString.split(",");	// ['2', '0', '0', '11']
			var versionMajor      = versionArray[0]; 
			var versionMinor      = versionArray[1];
			var versionRevision   = versionArray[2];
			return (major < versionMajor) || (major == versionMajor && minor < versionMinor) || (major == versionMajor && minor == versionMinor && revision < versionRevision);
		}		
		return false;
	}
}
/* END OF FILE - ..\xpertPivot\swf.src.js - */
