/**@preserve  GeneXus C# 10_3_14-114418 on 3/24/2020 22:57:28.32
*/
gx.evt.autoSkip = false;
gx.define('contagemresultadochcklstlogwc', true, function (CmpContext) {
   this.ServerClass =  "contagemresultadochcklstlogwc" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV35ContagemResultadoChckLstLog_OSCodigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO",'.') ;
      this.A602ContagemResultado_OSVinculada=gx.fn.getIntegerValue("CONTAGEMRESULTADO_OSVINCULADA",'.') ;
      this.AV17ContagemResultadoChckLstLog_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO",'.') ;
      this.A456ContagemResultado_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CODIGO",'.') ;
      this.AV41Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.A811ContagemResultadoChckLstLog_ChckLstCod=gx.fn.getIntegerValue("CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD",'.') ;
      this.A812ContagemResultadoChckLstLog_ChckLstDes=gx.fn.getControlValue("CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES") ;
      this.A824ContagemResultadoChckLstLog_Etapa=gx.fn.getIntegerValue("CONTAGEMRESULTADOCHCKLSTLOG_ETAPA",'.') ;
      this.A1841Check_Nome=gx.fn.getControlValue("CHECK_NOME") ;
      this.AV19ContagemResultado_OSVinculada=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_OSVINCULADA",'.') ;
   };
   this.Validv_Tfcontagemresultadochcklstlog_datahora=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV22TFContagemResultadoChckLstLog_DataHora)==0) || new gx.date.gxdate( this.AV22TFContagemResultadoChckLstLog_DataHora ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Chck Lst Log_Data Hora fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultadochcklstlog_datahora_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV23TFContagemResultadoChckLstLog_DataHora_To)==0) || new gx.date.gxdate( this.AV23TFContagemResultadoChckLstLog_DataHora_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado Chck Lst Log_Data Hora_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadochcklstlog_datahoraauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate)==0) || new gx.date.gxdate( this.AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Chck Lst Log_Data Hora Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultadochcklstlog_datahoraauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo)==0) || new gx.date.gxdate( this.AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado Chck Lst Log_Data Hora Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s132_client=function()
   {
      this.s152_client();
      if ( this.AV13OrderedBy == 1 )
      {
         this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 2 )
      {
         this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s152_client=function()
   {
      this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.SortedStatus =  ""  ;
      this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.SortedStatus =  ""  ;
   };
   this.e17en2_client=function()
   {
      this.clearMessages();
      if ( this.A824ContagemResultadoChckLstLog_Etapa == 0 )
      {
         this.INNEWWINDOWContainer.Target =  gx.http.formatLink("wp_checklistanalise.aspx",[this.AV17ContagemResultadoChckLstLog_Codigo, 0])  ;
      }
      else
      {
         this.INNEWWINDOWContainer.Target =  gx.http.formatLink("wp_checklist.aspx",[this.AV19ContagemResultado_OSVinculada, 0])  ;
      }
      this.INNEWWINDOWContainer.OpenWindow() ;
      this.refreshOutputs([{ctrl:this.INNEWWINDOWContainer}]);
   };
   this.e11en2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12en2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13en2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e18en2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e19en2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,9,10,11,12,18,22,24,25,26,27,28,29,30,31,32,34,36];
   this.GXLastCtrlId =36;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",8,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"contagemresultadochcklstlogwc",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addSingleLineEdit(814,9,"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA","","","ContagemResultadoChckLstLog_DataHora","dtime",0,"px",14,14,"right",null,[],814,"ContagemResultadoChckLstLog_DataHora",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit("Descricao",10,"vDESCRICAO","Descrição","","Descricao","vchar",410,"px",500,80,"left",null,[],"Descricao","Descricao",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(817,11,"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME","","","ContagemResultadoChckLstLog_UsuarioNome","char",0,"px",100,80,"left",null,[],817,"ContagemResultadoChckLstLog_UsuarioNome",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addBitmap("&Viewchcklst","vVIEWCHCKLST",12,20,"px",17,"px","e17en2_client","","","Image","");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 23, 22, "DVelop_WorkWithPlusUtilities", this.CmpContext + "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer = gx.uc.getNew(this, 33, 22, "BootstrapDropDownOptions", this.CmpContext + "DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer", "Ddo_contagemresultadochcklstlog_datahora");
   var DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer = this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer;
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.addV2CFunction('AV31DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV31DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV31DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.addV2CFunction('AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData', "vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLEFILTERDATA",UC.ParentObject.AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData); });
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.addEventHandler("OnOptionClicked", this.e12en2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer);
   this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer = gx.uc.getNew(this, 35, 22, "BootstrapDropDownOptions", this.CmpContext + "DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer", "Ddo_contagemresultadochcklstlog_usuarionome");
   var DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer = this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer;
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("DataListProc", "Datalistproc", "GetContagemResultadoChckLstLogWCFilterData", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.addV2CFunction('AV31DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV31DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV31DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.addV2CFunction('AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData', "vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.addC2VFunction(function(UC) { UC.ParentObject.AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLEFILTERDATA",UC.ParentObject.AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData); });
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.addEventHandler("OnOptionClicked", this.e13en2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer);
   this.INNEWWINDOWContainer = gx.uc.getNew(this, 21, 9, "InNewWindow", this.CmpContext + "INNEWWINDOWContainer", "Innewwindow");
   var INNEWWINDOWContainer = this.INNEWWINDOWContainer;
   INNEWWINDOWContainer.setProp("Width", "Width", "50", "str");
   INNEWWINDOWContainer.setProp("Height", "Height", "50", "str");
   INNEWWINDOWContainer.setProp("Name", "Name", "", "str");
   INNEWWINDOWContainer.setDynProp("Target", "Target", "", "str");
   INNEWWINDOWContainer.setProp("Fullscreen", "Fullscreen", false, "bool");
   INNEWWINDOWContainer.setProp("Location", "Location", true, "bool");
   INNEWWINDOWContainer.setProp("MenuBar", "Menubar", true, "bool");
   INNEWWINDOWContainer.setProp("Resizable", "Resizable", true, "bool");
   INNEWWINDOWContainer.setProp("Scrollbars", "Scrollbars", true, "bool");
   INNEWWINDOWContainer.setProp("TitleBar", "Titlebar", true, "bool");
   INNEWWINDOWContainer.setProp("ToolBar", "Toolbar", true, "bool");
   INNEWWINDOWContainer.setProp("directories", "Directories", true, "bool");
   INNEWWINDOWContainer.setProp("status", "Status", true, "bool");
   INNEWWINDOWContainer.setProp("copyhistory", "Copyhistory", true, "bool");
   INNEWWINDOWContainer.setProp("top", "Top", "5", "str");
   INNEWWINDOWContainer.setProp("left", "Left", "5", "str");
   INNEWWINDOWContainer.setProp("fitscreen", "Fitscreen", false, "bool");
   INNEWWINDOWContainer.setProp("RefreshParentOnClose", "Refreshparentonclose", false, "bool");
   INNEWWINDOWContainer.setProp("Targets", "Targets", '', "str");
   INNEWWINDOWContainer.setProp("Visible", "Visible", true, "bool");
   INNEWWINDOWContainer.setProp("Enabled", "Enabled", true, "boolean");
   INNEWWINDOWContainer.setProp("Class", "Class", "", "char");
   INNEWWINDOWContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(INNEWWINDOWContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 15, 9, "DVelop_DVPaginationBar", this.CmpContext + "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV33GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV33GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV33GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV34GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV34GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV34GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11en2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[5]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[9]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA",gxz:"Z814ContagemResultadoChckLstLog_DataHora",gxold:"O814ContagemResultadoChckLstLog_DataHora",gxvar:"A814ContagemResultadoChckLstLog_DataHora",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A814ContagemResultadoChckLstLog_DataHora=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z814ContagemResultadoChckLstLog_DataHora=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA",row || gx.fn.currentGridRowImpl(8),gx.O.A814ContagemResultadoChckLstLog_DataHora,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A814ContagemResultadoChckLstLog_DataHora=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA",row || gx.fn.currentGridRowImpl(8))},nac:gx.falseFn};
   GXValidFnc[10]={lvl:2,type:"vchar",len:500,dec:0,sign:false,ro:0,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDESCRICAO",gxz:"ZV16Descricao",gxold:"OV16Descricao",gxvar:"AV16Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV16Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV16Descricao=Value},v2c:function(row){gx.fn.setGridControlValue("vDESCRICAO",row || gx.fn.currentGridRowImpl(8),gx.O.AV16Descricao,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV16Descricao=this.val()},val:function(row){return gx.fn.getGridControlValue("vDESCRICAO",row || gx.fn.currentGridRowImpl(8))},nac:gx.falseFn};
   GXValidFnc[11]={lvl:2,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME",gxz:"Z817ContagemResultadoChckLstLog_UsuarioNome",gxold:"O817ContagemResultadoChckLstLog_UsuarioNome",gxvar:"A817ContagemResultadoChckLstLog_UsuarioNome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A817ContagemResultadoChckLstLog_UsuarioNome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z817ContagemResultadoChckLstLog_UsuarioNome=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME",row || gx.fn.currentGridRowImpl(8),gx.O.A817ContagemResultadoChckLstLog_UsuarioNome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A817ContagemResultadoChckLstLog_UsuarioNome=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME",row || gx.fn.currentGridRowImpl(8))},nac:gx.falseFn};
   GXValidFnc[12]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:8,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vVIEWCHCKLST",gxz:"ZV18ViewChckLst",gxold:"OV18ViewChckLst",gxvar:"AV18ViewChckLst",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV18ViewChckLst=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV18ViewChckLst=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vVIEWCHCKLST",row || gx.fn.currentGridRowImpl(8),gx.O.AV18ViewChckLst,gx.O.AV40Viewchcklst_GXI)},c2v:function(){gx.O.AV40Viewchcklst_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV18ViewChckLst=this.val()},val:function(row){return gx.fn.getGridControlValue("vVIEWCHCKLST",row || gx.fn.currentGridRowImpl(8))},val_GXI:function(row){return gx.fn.getGridControlValue("vVIEWCHCKLST_GXI",row || gx.fn.currentGridRowImpl(8))}, gxvar_GXI:'AV40Viewchcklst_GXI',nac:gx.falseFn};
   GXValidFnc[18]={fld:"TBLUSERTABLE",grid:0};
   GXValidFnc[22]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO",gxz:"Z1853ContagemResultadoChckLstLog_OSCodigo",gxold:"O1853ContagemResultadoChckLstLog_OSCodigo",gxvar:"A1853ContagemResultadoChckLstLog_OSCodigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1853ContagemResultadoChckLstLog_OSCodigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1853ContagemResultadoChckLstLog_OSCodigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO",gx.O.A1853ContagemResultadoChckLstLog_OSCodigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1853ContagemResultadoChckLstLog_OSCodigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 22 , function() {
   });
   GXValidFnc[24]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDBY",gx.O.AV13OrderedBy,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[25]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[26]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadochcklstlog_datahora,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA",gxz:"ZV22TFContagemResultadoChckLstLog_DataHora",gxold:"OV22TFContagemResultadoChckLstLog_DataHora",gxvar:"AV22TFContagemResultadoChckLstLog_DataHora",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[26],ip:[26],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV22TFContagemResultadoChckLstLog_DataHora=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22TFContagemResultadoChckLstLog_DataHora=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA",gx.O.AV22TFContagemResultadoChckLstLog_DataHora,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV22TFContagemResultadoChckLstLog_DataHora=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA")},nac:gx.falseFn};
   GXValidFnc[27]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultadochcklstlog_datahora_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO",gxz:"ZV23TFContagemResultadoChckLstLog_DataHora_To",gxold:"OV23TFContagemResultadoChckLstLog_DataHora_To",gxvar:"AV23TFContagemResultadoChckLstLog_DataHora_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[27],ip:[27],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23TFContagemResultadoChckLstLog_DataHora_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV23TFContagemResultadoChckLstLog_DataHora_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO",gx.O.AV23TFContagemResultadoChckLstLog_DataHora_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23TFContagemResultadoChckLstLog_DataHora_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO")},nac:gx.falseFn};
   GXValidFnc[28]={fld:"DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATES",grid:0};
   GXValidFnc[29]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadochcklstlog_datahoraauxdate,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATE",gxz:"ZV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate",gxold:"OV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate",gxvar:"AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[29],ip:[29],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATE",gx.O.AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATE")},nac:gx.falseFn};
   GXValidFnc[30]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultadochcklstlog_datahoraauxdateto,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATETO",gxz:"ZV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo",gxold:"OV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo",gxvar:"AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[30],ip:[30],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATETO",gx.O.AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[31]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME",gxz:"ZV28TFContagemResultadoChckLstLog_UsuarioNome",gxold:"OV28TFContagemResultadoChckLstLog_UsuarioNome",gxvar:"AV28TFContagemResultadoChckLstLog_UsuarioNome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV28TFContagemResultadoChckLstLog_UsuarioNome=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV28TFContagemResultadoChckLstLog_UsuarioNome=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME",gx.O.AV28TFContagemResultadoChckLstLog_UsuarioNome,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV28TFContagemResultadoChckLstLog_UsuarioNome=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME")},nac:gx.falseFn};
   GXValidFnc[32]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL",gxz:"ZV29TFContagemResultadoChckLstLog_UsuarioNome_Sel",gxold:"OV29TFContagemResultadoChckLstLog_UsuarioNome_Sel",gxvar:"AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV29TFContagemResultadoChckLstLog_UsuarioNome_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL",gx.O.AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL")},nac:gx.falseFn};
   GXValidFnc[34]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE",gxz:"ZV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace",gxold:"OV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace",gxvar:"AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE",gx.O.AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[36]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE",gxz:"ZV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace",gxold:"OV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace",gxvar:"AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE",gx.O.AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.Z814ContagemResultadoChckLstLog_DataHora = gx.date.nullDate() ;
   this.O814ContagemResultadoChckLstLog_DataHora = gx.date.nullDate() ;
   this.ZV16Descricao = "" ;
   this.OV16Descricao = "" ;
   this.Z817ContagemResultadoChckLstLog_UsuarioNome = "" ;
   this.O817ContagemResultadoChckLstLog_UsuarioNome = "" ;
   this.ZV18ViewChckLst = "" ;
   this.OV18ViewChckLst = "" ;
   this.A1853ContagemResultadoChckLstLog_OSCodigo = 0 ;
   this.Z1853ContagemResultadoChckLstLog_OSCodigo = 0 ;
   this.O1853ContagemResultadoChckLstLog_OSCodigo = 0 ;
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV22TFContagemResultadoChckLstLog_DataHora = gx.date.nullDate() ;
   this.ZV22TFContagemResultadoChckLstLog_DataHora = gx.date.nullDate() ;
   this.OV22TFContagemResultadoChckLstLog_DataHora = gx.date.nullDate() ;
   this.AV23TFContagemResultadoChckLstLog_DataHora_To = gx.date.nullDate() ;
   this.ZV23TFContagemResultadoChckLstLog_DataHora_To = gx.date.nullDate() ;
   this.OV23TFContagemResultadoChckLstLog_DataHora_To = gx.date.nullDate() ;
   this.AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = gx.date.nullDate() ;
   this.ZV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = gx.date.nullDate() ;
   this.OV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = gx.date.nullDate() ;
   this.AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.ZV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.OV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.AV28TFContagemResultadoChckLstLog_UsuarioNome = "" ;
   this.ZV28TFContagemResultadoChckLstLog_UsuarioNome = "" ;
   this.OV28TFContagemResultadoChckLstLog_UsuarioNome = "" ;
   this.AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = "" ;
   this.ZV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = "" ;
   this.OV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = "" ;
   this.AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = "" ;
   this.ZV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = "" ;
   this.OV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = "" ;
   this.AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = "" ;
   this.ZV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = "" ;
   this.OV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = "" ;
   this.AV33GridCurrentPage = 0 ;
   this.A1853ContagemResultadoChckLstLog_OSCodigo = 0 ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV22TFContagemResultadoChckLstLog_DataHora = gx.date.nullDate() ;
   this.AV23TFContagemResultadoChckLstLog_DataHora_To = gx.date.nullDate() ;
   this.AV24DDO_ContagemResultadoChckLstLog_DataHoraAuxDate = gx.date.nullDate() ;
   this.AV25DDO_ContagemResultadoChckLstLog_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.AV28TFContagemResultadoChckLstLog_UsuarioNome = "" ;
   this.AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel = "" ;
   this.AV31DDO_TitleSettingsIcons = {} ;
   this.AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace = "" ;
   this.AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace = "" ;
   this.AV35ContagemResultadoChckLstLog_OSCodigo = 0 ;
   this.A1839Check_Codigo = 0 ;
   this.A1841Check_Nome = "" ;
   this.A811ContagemResultadoChckLstLog_ChckLstCod = 0 ;
   this.A812ContagemResultadoChckLstLog_ChckLstDes = "" ;
   this.A824ContagemResultadoChckLstLog_Etapa = 0 ;
   this.A822ContagemResultadoChckLstLog_UsuarioCod = 0 ;
   this.A823ContagemResultadoChckLstLog_PessoaCod = 0 ;
   this.A814ContagemResultadoChckLstLog_DataHora = gx.date.nullDate() ;
   this.AV16Descricao = "" ;
   this.A817ContagemResultadoChckLstLog_UsuarioNome = "" ;
   this.AV18ViewChckLst = "" ;
   this.A602ContagemResultado_OSVinculada = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.AV17ContagemResultadoChckLstLog_Codigo = 0 ;
   this.AV41Pgmname = "" ;
   this.AV19ContagemResultado_OSVinculada = 0 ;
   this.Events = {"e11en2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12en2_client": ["DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA.ONOPTIONCLICKED", true] ,"e13en2_client": ["DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME.ONOPTIONCLICKED", true] ,"e18en2_client": ["ENTER", true] ,"e19en2_client": ["CANCEL", true] ,"e17en2_client": ["VVIEWCHCKLST.CLICK", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV21ContagemResultadoChckLstLog_DataHoraTitleFilterData',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContagemResultadoChckLstLog_UsuarioNomeTitleFilterData',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLEFILTERDATA',pic:'',nv:null},{ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA","Title")',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'Title'},{ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME","Title")',ctrl:'CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'Title'},{av:'AV33GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV34GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV19ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.SortedStatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'SortedStatus'},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.SortedStatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'SortedStatus'}]];
   this.EvtParms["DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22TFContagemResultadoChckLstLog_DataHora',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV23TFContagemResultadoChckLstLog_DataHora_To',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'AV35ContagemResultadoChckLstLog_OSCodigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26ddo_ContagemResultadoChckLstLog_DataHoraTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContagemResultadoChckLstLog_UsuarioNomeTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.SelectedValue_get',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'SelectedValue_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOMEContainer.SortedStatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',prop:'SortedStatus'},{av:'AV28TFContagemResultadoChckLstLog_UsuarioNome',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME',pic:'@!',nv:''},{av:'AV29TFContagemResultadoChckLstLog_UsuarioNome_Sel',fld:'vTFCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME_SEL',pic:'@!',nv:''},{av:'this.DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORAContainer.SortedStatus',ctrl:'DDO_CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA',prop:'SortedStatus'}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A811ContagemResultadoChckLstLog_ChckLstCod',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD',pic:'ZZZZZ9',nv:0},{av:'A812ContagemResultadoChckLstLog_ChckLstDes',fld:'CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES',pic:'',nv:''},{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'A1841Check_Nome',fld:'CHECK_NOME',pic:'@!',nv:''}],[{av:'AV16Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'AV18ViewChckLst',fld:'vVIEWCHCKLST',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vVIEWCHCKLST","Tooltiptext")',ctrl:'vVIEWCHCKLST',prop:'Tooltiptext'}]];
   this.EvtParms["VVIEWCHCKLST.CLICK"] = [[{av:'A824ContagemResultadoChckLstLog_Etapa',fld:'CONTAGEMRESULTADOCHCKLSTLOG_ETAPA',pic:'9',nv:0},{av:'AV17ContagemResultadoChckLstLog_Codigo',fld:'vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0}],[{av:'this.INNEWWINDOWContainer.Target',ctrl:'INNEWWINDOW',prop:'Target'}]];
   this.setVCMap("AV35ContagemResultadoChckLstLog_OSCodigo", "vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO", 0, "int");
   this.setVCMap("A602ContagemResultado_OSVinculada", "CONTAGEMRESULTADO_OSVINCULADA", 0, "int");
   this.setVCMap("AV17ContagemResultadoChckLstLog_Codigo", "vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO", 0, "int");
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV41Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("A811ContagemResultadoChckLstLog_ChckLstCod", "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD", 0, "int");
   this.setVCMap("A812ContagemResultadoChckLstLog_ChckLstDes", "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES", 0, "vchar");
   this.setVCMap("A824ContagemResultadoChckLstLog_Etapa", "CONTAGEMRESULTADOCHCKLSTLOG_ETAPA", 0, "int");
   this.setVCMap("A1841Check_Nome", "CHECK_NOME", 0, "char");
   this.setVCMap("AV19ContagemResultado_OSVinculada", "vCONTAGEMRESULTADO_OSVINCULADA", 0, "int");
   this.setVCMap("AV35ContagemResultadoChckLstLog_OSCodigo", "vCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO", 0, "int");
   this.setVCMap("A602ContagemResultado_OSVinculada", "CONTAGEMRESULTADO_OSVINCULADA", 0, "int");
   this.setVCMap("AV17ContagemResultadoChckLstLog_Codigo", "vCONTAGEMRESULTADOCHCKLSTLOG_CODIGO", 0, "int");
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV41Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("A811ContagemResultadoChckLstLog_ChckLstCod", "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD", 0, "int");
   this.setVCMap("A812ContagemResultadoChckLstLog_ChckLstDes", "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES", 0, "vchar");
   this.setVCMap("A824ContagemResultadoChckLstLog_Etapa", "CONTAGEMRESULTADOCHCKLSTLOG_ETAPA", 0, "int");
   this.setVCMap("A1841Check_Nome", "CHECK_NOME", 0, "char");
   GridContainer.addRefreshingVar(this.GXValidFnc[24]);
   GridContainer.addRefreshingVar(this.GXValidFnc[25]);
   GridContainer.addRefreshingVar(this.GXValidFnc[26]);
   GridContainer.addRefreshingVar(this.GXValidFnc[27]);
   GridContainer.addRefreshingVar(this.GXValidFnc[31]);
   GridContainer.addRefreshingVar(this.GXValidFnc[32]);
   GridContainer.addRefreshingVar({rfrVar:"AV35ContagemResultadoChckLstLog_OSCodigo"});
   GridContainer.addRefreshingVar(this.GXValidFnc[34]);
   GridContainer.addRefreshingVar(this.GXValidFnc[36]);
   GridContainer.addRefreshingVar({rfrVar:"A602ContagemResultado_OSVinculada"});
   GridContainer.addRefreshingVar({rfrVar:"AV17ContagemResultadoChckLstLog_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A456ContagemResultado_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"AV41Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"A811ContagemResultadoChckLstLog_ChckLstCod"});
   GridContainer.addRefreshingVar({rfrVar:"A812ContagemResultadoChckLstLog_ChckLstDes"});
   GridContainer.addRefreshingVar({rfrVar:"A824ContagemResultadoChckLstLog_Etapa"});
   GridContainer.addRefreshingVar({rfrVar:"A1841Check_Nome"});
   this.InitStandaloneVars( );
});
