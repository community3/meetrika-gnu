/*
               File: ContagemResultadoEvidencia
        Description: Anexar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:20:55.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoevidencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel4"+"_"+"CONTAGEMRESULTADOEVIDENCIA_ENTIDADE") == 0 )
         {
            A1493ContagemResultadoEvidencia_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1493ContagemResultadoEvidencia_Owner = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
            A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n52Contratada_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX4ASACONTAGEMRESULTADOEVIDENCIA_ENTIDADE2076( A1493ContagemResultadoEvidencia_Owner, A52Contratada_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_17") == 0 )
         {
            A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_17( A456ContagemResultado_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_19") == 0 )
         {
            A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n490ContagemResultado_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_19( A490ContagemResultado_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_18") == 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_18( A645TipoDocumento_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV8ContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ContagemResultadoEvidencia_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADOEVIDENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContagemResultadoEvidencia_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Anexar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoEvidencia_Arquivo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoevidencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoevidencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContagemResultadoEvidencia_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8ContagemResultadoEvidencia_Codigo = aP1_ContagemResultadoEvidencia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2076( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2076e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultado_Codigo_Visible, edtContagemResultado_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoEvidencia.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoEvidencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")), ((edtContagemResultadoEvidencia_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoEvidencia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoEvidencia_Codigo_Visible, edtContagemResultadoEvidencia_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoEvidencia.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2076( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2076( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2076e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_19_2076( true) ;
         }
         return  ;
      }

      protected void wb_table3_19_2076e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2076e( true) ;
         }
         else
         {
            wb_table1_2_2076e( false) ;
         }
      }

      protected void wb_table3_19_2076( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_19_2076e( true) ;
         }
         else
         {
            wb_table3_19_2076e( false) ;
         }
      }

      protected void wb_table2_5_2076( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_2076( true) ;
         }
         return  ;
      }

      protected void wb_table4_11_2076e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2076e( true) ;
         }
         else
         {
            wb_table2_5_2076e( false) ;
         }
      }

      protected void wb_table4_11_2076( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoevidencia_arquivo_Internalname, "Arquivo", "", "", lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            edtContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            edtContagemResultadoEvidencia_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
            edtContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) )
            {
               gxblobfileaux.Source = A588ContagemResultadoEvidencia_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtContagemResultadoEvidencia_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtContagemResultadoEvidencia_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A588ContagemResultadoEvidencia_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n588ContagemResultadoEvidencia_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
                  edtContagemResultadoEvidencia_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtContagemResultadoEvidencia_Arquivo_Internalname, StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo), context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Filetype)) ? A588ContagemResultadoEvidencia_Arquivo : edtContagemResultadoEvidencia_Arquivo_Filetype)) : edtContagemResultadoEvidencia_Arquivo_Contenttype), true, "", edtContagemResultadoEvidencia_Arquivo_Parameters, edtContagemResultadoEvidencia_Arquivo_Display, edtContagemResultadoEvidencia_Arquivo_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtContagemResultadoEvidencia_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", "", "HLP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_2076e( true) ;
         }
         else
         {
            wb_table4_11_2076e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11202 */
         E11202 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A588ContagemResultadoEvidencia_Arquivo = cgiGet( edtContagemResultadoEvidencia_Arquivo_Internalname);
               n588ContagemResultadoEvidencia_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
               n588ContagemResultadoEvidencia_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A456ContagemResultado_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               else
               {
                  A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               A586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoEvidencia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
               /* Read saved values. */
               Z586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z586ContagemResultadoEvidencia_Codigo"), ",", "."));
               Z591ContagemResultadoEvidencia_Data = context.localUtil.CToT( cgiGet( "Z591ContagemResultadoEvidencia_Data"), 0);
               n591ContagemResultadoEvidencia_Data = ((DateTime.MinValue==A591ContagemResultadoEvidencia_Data) ? true : false);
               Z1393ContagemResultadoEvidencia_RdmnCreated = context.localUtil.CToT( cgiGet( "Z1393ContagemResultadoEvidencia_RdmnCreated"), 0);
               n1393ContagemResultadoEvidencia_RdmnCreated = ((DateTime.MinValue==A1393ContagemResultadoEvidencia_RdmnCreated) ? true : false);
               Z1493ContagemResultadoEvidencia_Owner = (int)(context.localUtil.CToN( cgiGet( "Z1493ContagemResultadoEvidencia_Owner"), ",", "."));
               n1493ContagemResultadoEvidencia_Owner = ((0==A1493ContagemResultadoEvidencia_Owner) ? true : false);
               Z456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z456ContagemResultado_Codigo"), ",", "."));
               Z645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A591ContagemResultadoEvidencia_Data = context.localUtil.CToT( cgiGet( "Z591ContagemResultadoEvidencia_Data"), 0);
               n591ContagemResultadoEvidencia_Data = false;
               n591ContagemResultadoEvidencia_Data = ((DateTime.MinValue==A591ContagemResultadoEvidencia_Data) ? true : false);
               A1393ContagemResultadoEvidencia_RdmnCreated = context.localUtil.CToT( cgiGet( "Z1393ContagemResultadoEvidencia_RdmnCreated"), 0);
               n1393ContagemResultadoEvidencia_RdmnCreated = false;
               n1393ContagemResultadoEvidencia_RdmnCreated = ((DateTime.MinValue==A1393ContagemResultadoEvidencia_RdmnCreated) ? true : false);
               A1493ContagemResultadoEvidencia_Owner = (int)(context.localUtil.CToN( cgiGet( "Z1493ContagemResultadoEvidencia_Owner"), ",", "."));
               n1493ContagemResultadoEvidencia_Owner = false;
               n1493ContagemResultadoEvidencia_Owner = ((0==A1493ContagemResultadoEvidencia_Owner) ? true : false);
               A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = false;
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "N456ContagemResultado_Codigo"), ",", "."));
               N645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "N645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A457ContagemResultado_Demanda = cgiGet( "CONTAGEMRESULTADO_DEMANDA");
               n457ContagemResultado_Demanda = false;
               A493ContagemResultado_DemandaFM = cgiGet( "CONTAGEMRESULTADO_DEMANDAFM");
               n493ContagemResultado_DemandaFM = false;
               A501ContagemResultado_OsFsOsFm = cgiGet( "CONTAGEMRESULTADO_OSFSOSFM");
               A1493ContagemResultadoEvidencia_Owner = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADOEVIDENCIA_OWNER"), ",", "."));
               n1493ContagemResultadoEvidencia_Owner = ((0==A1493ContagemResultadoEvidencia_Owner) ? true : false);
               A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_AREATRABALHOCOD"), ",", "."));
               A1494ContagemResultadoEvidencia_Entidade = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_ENTIDADE");
               AV8ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTAGEMRESULTADOEVIDENCIA_CODIGO"), ",", "."));
               AV12Insert_ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMRESULTADO_CODIGO"), ",", "."));
               AV14Insert_TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TIPODOCUMENTO_CODIGO"), ",", "."));
               A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "TIPODOCUMENTO_CODIGO"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTAGEMRESULTADO_CODIGO"), ",", "."));
               A589ContagemResultadoEvidencia_NomeArq = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ");
               n589ContagemResultadoEvidencia_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq)) ? true : false);
               A590ContagemResultadoEvidencia_TipoArq = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ");
               n590ContagemResultadoEvidencia_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq)) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A591ContagemResultadoEvidencia_Data = context.localUtil.CToT( cgiGet( "CONTAGEMRESULTADOEVIDENCIA_DATA"), 0);
               n591ContagemResultadoEvidencia_Data = ((DateTime.MinValue==A591ContagemResultadoEvidencia_Data) ? true : false);
               A1449ContagemResultadoEvidencia_Link = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_LINK");
               n1449ContagemResultadoEvidencia_Link = false;
               n1449ContagemResultadoEvidencia_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A1449ContagemResultadoEvidencia_Link)) ? true : false);
               A587ContagemResultadoEvidencia_Descricao = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_DESCRICAO");
               n587ContagemResultadoEvidencia_Descricao = false;
               n587ContagemResultadoEvidencia_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A587ContagemResultadoEvidencia_Descricao)) ? true : false);
               A1393ContagemResultadoEvidencia_RdmnCreated = context.localUtil.CToT( cgiGet( "CONTAGEMRESULTADOEVIDENCIA_RDMNCREATED"), 0);
               n1393ContagemResultadoEvidencia_RdmnCreated = ((DateTime.MinValue==A1393ContagemResultadoEvidencia_RdmnCreated) ? true : false);
               A490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CONTRATADACOD"), ",", "."));
               A646TipoDocumento_Nome = cgiGet( "TIPODOCUMENTO_NOME");
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               edtContagemResultadoEvidencia_Arquivo_Filetype = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filetype");
               edtContagemResultadoEvidencia_Arquivo_Filename = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filename");
               edtContagemResultadoEvidencia_Arquivo_Filename = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filename");
               edtContagemResultadoEvidencia_Arquivo_Filetype = cgiGet( "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) )
               {
                  edtContagemResultadoEvidencia_Arquivo_Filename = (String)(CGIGetFileName(edtContagemResultadoEvidencia_Arquivo_Internalname));
                  edtContagemResultadoEvidencia_Arquivo_Filetype = (String)(CGIGetFileType(edtContagemResultadoEvidencia_Arquivo_Internalname));
               }
               A590ContagemResultadoEvidencia_TipoArq = edtContagemResultadoEvidencia_Arquivo_Filetype;
               n590ContagemResultadoEvidencia_TipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
               A589ContagemResultadoEvidencia_NomeArq = edtContagemResultadoEvidencia_Arquivo_Filename;
               n589ContagemResultadoEvidencia_NomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) )
               {
                  GXCCtlgxBlob = "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO" + "_gxBlob";
                  A588ContagemResultadoEvidencia_Arquivo = cgiGet( GXCCtlgxBlob);
                  n588ContagemResultadoEvidencia_Arquivo = false;
                  n588ContagemResultadoEvidencia_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContagemResultadoEvidencia";
               A586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoEvidencia_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1393ContagemResultadoEvidencia_RdmnCreated, "99/99/99 99:99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1493ContagemResultadoEvidencia_Owner), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagemresultadoevidencia:[SecurityCheckFailed value for]"+"ContagemResultadoEvidencia_Codigo:"+context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contagemresultadoevidencia:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contagemresultadoevidencia:[SecurityCheckFailed value for]"+"TipoDocumento_Codigo:"+context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contagemresultadoevidencia:[SecurityCheckFailed value for]"+"ContagemResultadoEvidencia_Data:"+context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99"));
                  GXUtil.WriteLog("contagemresultadoevidencia:[SecurityCheckFailed value for]"+"ContagemResultadoEvidencia_RdmnCreated:"+context.localUtil.Format( A1393ContagemResultadoEvidencia_RdmnCreated, "99/99/99 99:99"));
                  GXUtil.WriteLog("contagemresultadoevidencia:[SecurityCheckFailed value for]"+"ContagemResultadoEvidencia_Owner:"+context.localUtil.Format( (decimal)(A1493ContagemResultadoEvidencia_Owner), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A586ContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode76 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode76;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound76 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_200( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTAGEMRESULTADOEVIDENCIA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContagemResultadoEvidencia_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11202 */
                           E11202 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12202 */
                           E12202 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12202 */
            E12202 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2076( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2076( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_200( )
      {
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2076( ) ;
            }
            else
            {
               CheckExtendedTable2076( ) ;
               CloseExtendedTableCursors2076( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption200( )
      {
      }

      protected void E11202( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV10TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV10TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV10TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemResultado_Codigo") == 0 )
               {
                  AV12Insert_ContagemResultado_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_ContagemResultado_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "TipoDocumento_Codigo") == 0 )
               {
                  AV14Insert_TipoDocumento_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_TipoDocumento_Codigo), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtContagemResultado_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Visible), 5, 0)));
         edtContagemResultadoEvidencia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoEvidencia_Codigo_Visible), 5, 0)));
         edtContagemResultadoEvidencia_Arquivo_Display = 1;
      }

      protected void E12202( )
      {
         /* After Trn Routine */
         if ( false )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwcontagemresultadoevidencia.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2076( short GX_JID )
      {
         if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z591ContagemResultadoEvidencia_Data = T00203_A591ContagemResultadoEvidencia_Data[0];
               Z1393ContagemResultadoEvidencia_RdmnCreated = T00203_A1393ContagemResultadoEvidencia_RdmnCreated[0];
               Z1493ContagemResultadoEvidencia_Owner = T00203_A1493ContagemResultadoEvidencia_Owner[0];
               Z456ContagemResultado_Codigo = T00203_A456ContagemResultado_Codigo[0];
               Z645TipoDocumento_Codigo = T00203_A645TipoDocumento_Codigo[0];
            }
            else
            {
               Z591ContagemResultadoEvidencia_Data = A591ContagemResultadoEvidencia_Data;
               Z1393ContagemResultadoEvidencia_RdmnCreated = A1393ContagemResultadoEvidencia_RdmnCreated;
               Z1493ContagemResultadoEvidencia_Owner = A1493ContagemResultadoEvidencia_Owner;
               Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            }
         }
         if ( GX_JID == -16 )
         {
            Z586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
            Z589ContagemResultadoEvidencia_NomeArq = A589ContagemResultadoEvidencia_NomeArq;
            Z590ContagemResultadoEvidencia_TipoArq = A590ContagemResultadoEvidencia_TipoArq;
            Z1449ContagemResultadoEvidencia_Link = A1449ContagemResultadoEvidencia_Link;
            Z588ContagemResultadoEvidencia_Arquivo = A588ContagemResultadoEvidencia_Arquivo;
            Z591ContagemResultadoEvidencia_Data = A591ContagemResultadoEvidencia_Data;
            Z587ContagemResultadoEvidencia_Descricao = A587ContagemResultadoEvidencia_Descricao;
            Z1393ContagemResultadoEvidencia_RdmnCreated = A1393ContagemResultadoEvidencia_RdmnCreated;
            Z1493ContagemResultadoEvidencia_Owner = A1493ContagemResultadoEvidencia_Owner;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z457ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContagemResultadoEvidencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoEvidencia_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV15Pgmname = "ContagemResultadoEvidencia";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         edtContagemResultadoEvidencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoEvidencia_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV8ContagemResultadoEvidencia_Codigo) )
         {
            A586ContagemResultadoEvidencia_Codigo = AV8ContagemResultadoEvidencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContagemResultado_Codigo) )
         {
            edtContagemResultado_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtContagemResultado_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_TipoDocumento_Codigo) )
         {
            A645TipoDocumento_Codigo = AV14Insert_TipoDocumento_Codigo;
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContagemResultado_Codigo) )
         {
            A456ContagemResultado_Codigo = AV12Insert_ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A591ContagemResultadoEvidencia_Data) && ( Gx_BScreen == 0 ) )
         {
            A591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            n591ContagemResultadoEvidencia_Data = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A591ContagemResultadoEvidencia_Data", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00205 */
            pr_default.execute(3, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = T00205_A646TipoDocumento_Nome[0];
            pr_default.close(3);
            /* Using cursor T00204 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
            A490ContagemResultado_ContratadaCod = T00204_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T00204_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = T00204_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T00204_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T00204_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T00204_n493ContagemResultado_DemandaFM[0];
            pr_default.close(2);
            /* Using cursor T00206 */
            pr_default.execute(4, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            A52Contratada_AreaTrabalhoCod = T00206_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = T00206_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(4);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         }
      }

      protected void Load2076( )
      {
         /* Using cursor T00207 */
         pr_default.execute(5, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound76 = 1;
            A490ContagemResultado_ContratadaCod = T00207_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T00207_n490ContagemResultado_ContratadaCod[0];
            A589ContagemResultadoEvidencia_NomeArq = T00207_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = T00207_n589ContagemResultadoEvidencia_NomeArq[0];
            edtContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A590ContagemResultadoEvidencia_TipoArq = T00207_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = T00207_n590ContagemResultadoEvidencia_TipoArq[0];
            edtContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
            A457ContagemResultado_Demanda = T00207_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T00207_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T00207_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T00207_n493ContagemResultado_DemandaFM[0];
            A1449ContagemResultadoEvidencia_Link = T00207_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = T00207_n1449ContagemResultadoEvidencia_Link[0];
            A591ContagemResultadoEvidencia_Data = T00207_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = T00207_n591ContagemResultadoEvidencia_Data[0];
            A587ContagemResultadoEvidencia_Descricao = T00207_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = T00207_n587ContagemResultadoEvidencia_Descricao[0];
            A1393ContagemResultadoEvidencia_RdmnCreated = T00207_A1393ContagemResultadoEvidencia_RdmnCreated[0];
            n1393ContagemResultadoEvidencia_RdmnCreated = T00207_n1393ContagemResultadoEvidencia_RdmnCreated[0];
            A646TipoDocumento_Nome = T00207_A646TipoDocumento_Nome[0];
            A1493ContagemResultadoEvidencia_Owner = T00207_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = T00207_n1493ContagemResultadoEvidencia_Owner[0];
            A456ContagemResultado_Codigo = T00207_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A645TipoDocumento_Codigo = T00207_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T00207_n645TipoDocumento_Codigo[0];
            A52Contratada_AreaTrabalhoCod = T00207_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = T00207_n52Contratada_AreaTrabalhoCod[0];
            A588ContagemResultadoEvidencia_Arquivo = T00207_A588ContagemResultadoEvidencia_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
            n588ContagemResultadoEvidencia_Arquivo = T00207_n588ContagemResultadoEvidencia_Arquivo[0];
            ZM2076( -16) ;
         }
         pr_default.close(5);
         OnLoadActions2076( ) ;
      }

      protected void OnLoadActions2076( )
      {
         GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
         new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1494ContagemResultadoEvidencia_Entidade", A1494ContagemResultadoEvidencia_Entidade);
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV7ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
      }

      protected void CheckExtendedTable2076( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00204 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A490ContagemResultado_ContratadaCod = T00204_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T00204_n490ContagemResultado_ContratadaCod[0];
         A457ContagemResultado_Demanda = T00204_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T00204_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T00204_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T00204_n493ContagemResultado_DemandaFM[0];
         pr_default.close(2);
         /* Using cursor T00206 */
         pr_default.execute(4, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T00206_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = T00206_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(4);
         GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
         new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1494ContagemResultadoEvidencia_Entidade", A1494ContagemResultadoEvidencia_Entidade);
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV7ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         /* Using cursor T00205 */
         pr_default.execute(3, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = T00205_A646TipoDocumento_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors2076( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_17( int A456ContagemResultado_Codigo )
      {
         /* Using cursor T00208 */
         pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A490ContagemResultado_ContratadaCod = T00208_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T00208_n490ContagemResultado_ContratadaCod[0];
         A457ContagemResultado_Demanda = T00208_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T00208_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T00208_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T00208_n493ContagemResultado_DemandaFM[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( A457ContagemResultado_Demanda)+"\""+","+"\""+GXUtil.EncodeJSConstant( A493ContagemResultado_DemandaFM)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_19( int A490ContagemResultado_ContratadaCod )
      {
         /* Using cursor T00209 */
         pr_default.execute(7, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T00209_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = T00209_n52Contratada_AreaTrabalhoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_18( int A645TipoDocumento_Codigo )
      {
         /* Using cursor T002010 */
         pr_default.execute(8, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = T002010_A646TipoDocumento_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A646TipoDocumento_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey2076( )
      {
         /* Using cursor T002011 */
         pr_default.execute(9, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound76 = 1;
         }
         else
         {
            RcdFound76 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00203 */
         pr_default.execute(1, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2076( 16) ;
            RcdFound76 = 1;
            A586ContagemResultadoEvidencia_Codigo = T00203_A586ContagemResultadoEvidencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
            A589ContagemResultadoEvidencia_NomeArq = T00203_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = T00203_n589ContagemResultadoEvidencia_NomeArq[0];
            edtContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A590ContagemResultadoEvidencia_TipoArq = T00203_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = T00203_n590ContagemResultadoEvidencia_TipoArq[0];
            edtContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Filetype", edtContagemResultadoEvidencia_Arquivo_Filetype);
            A1449ContagemResultadoEvidencia_Link = T00203_A1449ContagemResultadoEvidencia_Link[0];
            n1449ContagemResultadoEvidencia_Link = T00203_n1449ContagemResultadoEvidencia_Link[0];
            A591ContagemResultadoEvidencia_Data = T00203_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = T00203_n591ContagemResultadoEvidencia_Data[0];
            A587ContagemResultadoEvidencia_Descricao = T00203_A587ContagemResultadoEvidencia_Descricao[0];
            n587ContagemResultadoEvidencia_Descricao = T00203_n587ContagemResultadoEvidencia_Descricao[0];
            A1393ContagemResultadoEvidencia_RdmnCreated = T00203_A1393ContagemResultadoEvidencia_RdmnCreated[0];
            n1393ContagemResultadoEvidencia_RdmnCreated = T00203_n1393ContagemResultadoEvidencia_RdmnCreated[0];
            A1493ContagemResultadoEvidencia_Owner = T00203_A1493ContagemResultadoEvidencia_Owner[0];
            n1493ContagemResultadoEvidencia_Owner = T00203_n1493ContagemResultadoEvidencia_Owner[0];
            A456ContagemResultado_Codigo = T00203_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A645TipoDocumento_Codigo = T00203_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T00203_n645TipoDocumento_Codigo[0];
            A588ContagemResultadoEvidencia_Arquivo = T00203_A588ContagemResultadoEvidencia_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
            n588ContagemResultadoEvidencia_Arquivo = T00203_n588ContagemResultadoEvidencia_Arquivo[0];
            Z586ContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
            sMode76 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2076( ) ;
            if ( AnyError == 1 )
            {
               RcdFound76 = 0;
               InitializeNonKey2076( ) ;
            }
            Gx_mode = sMode76;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound76 = 0;
            InitializeNonKey2076( ) ;
            sMode76 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode76;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2076( ) ;
         if ( RcdFound76 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound76 = 0;
         /* Using cursor T002012 */
         pr_default.execute(10, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T002012_A586ContagemResultadoEvidencia_Codigo[0] < A586ContagemResultadoEvidencia_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T002012_A586ContagemResultadoEvidencia_Codigo[0] > A586ContagemResultadoEvidencia_Codigo ) ) )
            {
               A586ContagemResultadoEvidencia_Codigo = T002012_A586ContagemResultadoEvidencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
               RcdFound76 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound76 = 0;
         /* Using cursor T002013 */
         pr_default.execute(11, new Object[] {A586ContagemResultadoEvidencia_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T002013_A586ContagemResultadoEvidencia_Codigo[0] > A586ContagemResultadoEvidencia_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T002013_A586ContagemResultadoEvidencia_Codigo[0] < A586ContagemResultadoEvidencia_Codigo ) ) )
            {
               A586ContagemResultadoEvidencia_Codigo = T002013_A586ContagemResultadoEvidencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
               RcdFound76 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2076( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoEvidencia_Arquivo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2076( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound76 == 1 )
            {
               if ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo )
               {
                  A586ContagemResultadoEvidencia_Codigo = Z586ContagemResultadoEvidencia_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOEVIDENCIA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoEvidencia_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoEvidencia_Arquivo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2076( ) ;
                  GX_FocusControl = edtContagemResultadoEvidencia_Arquivo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoEvidencia_Arquivo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2076( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOEVIDENCIA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoEvidencia_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoEvidencia_Arquivo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2076( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A586ContagemResultadoEvidencia_Codigo != Z586ContagemResultadoEvidencia_Codigo )
         {
            A586ContagemResultadoEvidencia_Codigo = Z586ContagemResultadoEvidencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOEVIDENCIA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoEvidencia_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoEvidencia_Arquivo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2076( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00202 */
            pr_default.execute(0, new Object[] {A586ContagemResultadoEvidencia_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoEvidencia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z591ContagemResultadoEvidencia_Data != T00202_A591ContagemResultadoEvidencia_Data[0] ) || ( Z1393ContagemResultadoEvidencia_RdmnCreated != T00202_A1393ContagemResultadoEvidencia_RdmnCreated[0] ) || ( Z1493ContagemResultadoEvidencia_Owner != T00202_A1493ContagemResultadoEvidencia_Owner[0] ) || ( Z456ContagemResultado_Codigo != T00202_A456ContagemResultado_Codigo[0] ) || ( Z645TipoDocumento_Codigo != T00202_A645TipoDocumento_Codigo[0] ) )
            {
               if ( Z591ContagemResultadoEvidencia_Data != T00202_A591ContagemResultadoEvidencia_Data[0] )
               {
                  GXUtil.WriteLog("contagemresultadoevidencia:[seudo value changed for attri]"+"ContagemResultadoEvidencia_Data");
                  GXUtil.WriteLogRaw("Old: ",Z591ContagemResultadoEvidencia_Data);
                  GXUtil.WriteLogRaw("Current: ",T00202_A591ContagemResultadoEvidencia_Data[0]);
               }
               if ( Z1393ContagemResultadoEvidencia_RdmnCreated != T00202_A1393ContagemResultadoEvidencia_RdmnCreated[0] )
               {
                  GXUtil.WriteLog("contagemresultadoevidencia:[seudo value changed for attri]"+"ContagemResultadoEvidencia_RdmnCreated");
                  GXUtil.WriteLogRaw("Old: ",Z1393ContagemResultadoEvidencia_RdmnCreated);
                  GXUtil.WriteLogRaw("Current: ",T00202_A1393ContagemResultadoEvidencia_RdmnCreated[0]);
               }
               if ( Z1493ContagemResultadoEvidencia_Owner != T00202_A1493ContagemResultadoEvidencia_Owner[0] )
               {
                  GXUtil.WriteLog("contagemresultadoevidencia:[seudo value changed for attri]"+"ContagemResultadoEvidencia_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z1493ContagemResultadoEvidencia_Owner);
                  GXUtil.WriteLogRaw("Current: ",T00202_A1493ContagemResultadoEvidencia_Owner[0]);
               }
               if ( Z456ContagemResultado_Codigo != T00202_A456ContagemResultado_Codigo[0] )
               {
                  GXUtil.WriteLog("contagemresultadoevidencia:[seudo value changed for attri]"+"ContagemResultado_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z456ContagemResultado_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00202_A456ContagemResultado_Codigo[0]);
               }
               if ( Z645TipoDocumento_Codigo != T00202_A645TipoDocumento_Codigo[0] )
               {
                  GXUtil.WriteLog("contagemresultadoevidencia:[seudo value changed for attri]"+"TipoDocumento_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z645TipoDocumento_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00202_A645TipoDocumento_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoEvidencia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2076( )
      {
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2076( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2076( 0) ;
            CheckOptimisticConcurrency2076( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2076( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2076( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002014 */
                     A589ContagemResultadoEvidencia_NomeArq = edtContagemResultadoEvidencia_Arquivo_Filename;
                     n589ContagemResultadoEvidencia_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
                     A590ContagemResultadoEvidencia_TipoArq = edtContagemResultadoEvidencia_Arquivo_Filetype;
                     n590ContagemResultadoEvidencia_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
                     pr_default.execute(12, new Object[] {n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n1449ContagemResultadoEvidencia_Link, A1449ContagemResultadoEvidencia_Link, n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, n591ContagemResultadoEvidencia_Data, A591ContagemResultadoEvidencia_Data, n587ContagemResultadoEvidencia_Descricao, A587ContagemResultadoEvidencia_Descricao, n1393ContagemResultadoEvidencia_RdmnCreated, A1393ContagemResultadoEvidencia_RdmnCreated, n1493ContagemResultadoEvidencia_Owner, A1493ContagemResultadoEvidencia_Owner, A456ContagemResultado_Codigo, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     A586ContagemResultadoEvidencia_Codigo = T002014_A586ContagemResultadoEvidencia_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption200( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2076( ) ;
            }
            EndLevel2076( ) ;
         }
         CloseExtendedTableCursors2076( ) ;
      }

      protected void Update2076( )
      {
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2076( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2076( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2076( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2076( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002015 */
                     A589ContagemResultadoEvidencia_NomeArq = edtContagemResultadoEvidencia_Arquivo_Filename;
                     n589ContagemResultadoEvidencia_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
                     A590ContagemResultadoEvidencia_TipoArq = edtContagemResultadoEvidencia_Arquivo_Filetype;
                     n590ContagemResultadoEvidencia_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
                     pr_default.execute(13, new Object[] {n589ContagemResultadoEvidencia_NomeArq, A589ContagemResultadoEvidencia_NomeArq, n590ContagemResultadoEvidencia_TipoArq, A590ContagemResultadoEvidencia_TipoArq, n1449ContagemResultadoEvidencia_Link, A1449ContagemResultadoEvidencia_Link, n591ContagemResultadoEvidencia_Data, A591ContagemResultadoEvidencia_Data, n587ContagemResultadoEvidencia_Descricao, A587ContagemResultadoEvidencia_Descricao, n1393ContagemResultadoEvidencia_RdmnCreated, A1393ContagemResultadoEvidencia_RdmnCreated, n1493ContagemResultadoEvidencia_Owner, A1493ContagemResultadoEvidencia_Owner, A456ContagemResultado_Codigo, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A586ContagemResultadoEvidencia_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoEvidencia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2076( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2076( ) ;
         }
         CloseExtendedTableCursors2076( ) ;
      }

      protected void DeferredUpdate2076( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T002016 */
            pr_default.execute(14, new Object[] {n588ContagemResultadoEvidencia_Arquivo, A588ContagemResultadoEvidencia_Arquivo, A586ContagemResultadoEvidencia_Codigo});
            pr_default.close(14);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate2076( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2076( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2076( ) ;
            AfterConfirm2076( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2076( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002017 */
                  pr_default.execute(15, new Object[] {A586ContagemResultadoEvidencia_Codigo});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode76 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2076( ) ;
         Gx_mode = sMode76;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2076( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002018 */
            pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo});
            A490ContagemResultado_ContratadaCod = T002018_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T002018_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = T002018_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = T002018_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = T002018_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = T002018_n493ContagemResultado_DemandaFM[0];
            pr_default.close(16);
            /* Using cursor T002019 */
            pr_default.execute(17, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            A52Contratada_AreaTrabalhoCod = T002019_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = T002019_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(17);
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
            if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV7ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
            }
            GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
            new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
            A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1494ContagemResultadoEvidencia_Entidade", A1494ContagemResultadoEvidencia_Entidade);
            /* Using cursor T002020 */
            pr_default.execute(18, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = T002020_A646TipoDocumento_Nome[0];
            pr_default.close(18);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002021 */
            pr_default.execute(19, new Object[] {A586ContagemResultadoEvidencia_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos das OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
         }
      }

      protected void EndLevel2076( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2076( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(18);
            pr_default.close(17);
            context.CommitDataStores( "ContagemResultadoEvidencia");
            if ( AnyError == 0 )
            {
               ConfirmValues200( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(18);
            pr_default.close(17);
            context.RollbackDataStores( "ContagemResultadoEvidencia");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2076( )
      {
         /* Scan By routine */
         /* Using cursor T002022 */
         pr_default.execute(20);
         RcdFound76 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound76 = 1;
            A586ContagemResultadoEvidencia_Codigo = T002022_A586ContagemResultadoEvidencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2076( )
      {
         /* Scan next routine */
         pr_default.readNext(20);
         RcdFound76 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound76 = 1;
            A586ContagemResultadoEvidencia_Codigo = T002022_A586ContagemResultadoEvidencia_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2076( )
      {
         pr_default.close(20);
      }

      protected void AfterConfirm2076( )
      {
         /* After Confirm Rules */
         if ( ! T00203_n588ContagemResultadoEvidencia_Arquivo[0] )
         {
            A589ContagemResultadoEvidencia_NomeArq = edtContagemResultadoEvidencia_Arquivo_Filename;
            n589ContagemResultadoEvidencia_NomeArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
         }
         if ( ! T00203_n588ContagemResultadoEvidencia_Arquivo[0] )
         {
            A590ContagemResultadoEvidencia_TipoArq = edtContagemResultadoEvidencia_Arquivo_Filetype;
            n590ContagemResultadoEvidencia_TipoArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
         }
      }

      protected void BeforeInsert2076( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2076( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2076( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2076( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2076( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2076( )
      {
         edtContagemResultadoEvidencia_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoEvidencia_Arquivo_Enabled), 5, 0)));
         edtContagemResultado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         edtContagemResultadoEvidencia_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoEvidencia_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues200( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120205724");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8ContagemResultadoEvidencia_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z591ContagemResultadoEvidencia_Data", context.localUtil.TToC( Z591ContagemResultadoEvidencia_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1393ContagemResultadoEvidencia_RdmnCreated", context.localUtil.TToC( Z1393ContagemResultadoEvidencia_RdmnCreated, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1493ContagemResultadoEvidencia_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSFSOSFM", A501ContagemResultado_OsFsOsFm);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_ENTIDADE", StringUtil.RTrim( A1494ContagemResultadoEvidencia_Entidade));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ", StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_DATA", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_LINK", A1449ContagemResultadoEvidencia_Link);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_DESCRICAO", A587ContagemResultadoEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_RDMNCREATED", context.localUtil.TToC( A1393ContagemResultadoEvidencia_RdmnCreated, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_NOME", StringUtil.RTrim( A646TipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADOEVIDENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8ContagemResultadoEvidencia_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A588ContagemResultadoEvidencia_Arquivo);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filetype", StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Filetype));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filename", StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filename", StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO_Filetype", StringUtil.RTrim( edtContagemResultadoEvidencia_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContagemResultadoEvidencia";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1393ContagemResultadoEvidencia_RdmnCreated, "99/99/99 99:99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1493ContagemResultadoEvidencia_Owner), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadoevidencia:[SendSecurityCheck value for]"+"ContagemResultadoEvidencia_Codigo:"+context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contagemresultadoevidencia:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contagemresultadoevidencia:[SendSecurityCheck value for]"+"TipoDocumento_Codigo:"+context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contagemresultadoevidencia:[SendSecurityCheck value for]"+"ContagemResultadoEvidencia_Data:"+context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99"));
         GXUtil.WriteLog("contagemresultadoevidencia:[SendSecurityCheck value for]"+"ContagemResultadoEvidencia_RdmnCreated:"+context.localUtil.Format( A1393ContagemResultadoEvidencia_RdmnCreated, "99/99/99 99:99"));
         GXUtil.WriteLog("contagemresultadoevidencia:[SendSecurityCheck value for]"+"ContagemResultadoEvidencia_Owner:"+context.localUtil.Format( (decimal)(A1493ContagemResultadoEvidencia_Owner), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoevidencia.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8ContagemResultadoEvidencia_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoEvidencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Anexar" ;
      }

      protected void InitializeNonKey2076( )
      {
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
         A456ContagemResultado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         AV7ContagemResultado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         A589ContagemResultadoEvidencia_NomeArq = "";
         n589ContagemResultadoEvidencia_NomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
         A590ContagemResultadoEvidencia_TipoArq = "";
         n590ContagemResultadoEvidencia_TipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
         A1494ContagemResultadoEvidencia_Entidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1494ContagemResultadoEvidencia_Entidade", A1494ContagemResultadoEvidencia_Entidade);
         A501ContagemResultado_OsFsOsFm = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
         A457ContagemResultado_Demanda = "";
         n457ContagemResultado_Demanda = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         A52Contratada_AreaTrabalhoCod = 0;
         n52Contratada_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A1449ContagemResultadoEvidencia_Link = "";
         n1449ContagemResultadoEvidencia_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1449ContagemResultadoEvidencia_Link", A1449ContagemResultadoEvidencia_Link);
         A588ContagemResultadoEvidencia_Arquivo = "";
         n588ContagemResultadoEvidencia_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A588ContagemResultadoEvidencia_Arquivo));
         n588ContagemResultadoEvidencia_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A588ContagemResultadoEvidencia_Arquivo)) ? true : false);
         A587ContagemResultadoEvidencia_Descricao = "";
         n587ContagemResultadoEvidencia_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A587ContagemResultadoEvidencia_Descricao", A587ContagemResultadoEvidencia_Descricao);
         A1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         n1393ContagemResultadoEvidencia_RdmnCreated = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1393ContagemResultadoEvidencia_RdmnCreated", context.localUtil.TToC( A1393ContagemResultadoEvidencia_RdmnCreated, 8, 5, 0, 3, "/", ":", " "));
         A646TipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
         A1493ContagemResultadoEvidencia_Owner = 0;
         n1493ContagemResultadoEvidencia_Owner = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
         A591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A591ContagemResultadoEvidencia_Data", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         Z591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         Z1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         Z1493ContagemResultadoEvidencia_Owner = 0;
         Z456ContagemResultado_Codigo = 0;
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll2076( )
      {
         A586ContagemResultadoEvidencia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
         InitializeNonKey2076( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A591ContagemResultadoEvidencia_Data = i591ContagemResultadoEvidencia_Data;
         n591ContagemResultadoEvidencia_Data = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A591ContagemResultadoEvidencia_Data", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120205739");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoevidencia.js", "?20203120205740");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultadoevidencia_arquivo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOEVIDENCIA_ARQUIVO";
         edtContagemResultadoEvidencia_Arquivo_Internalname = "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultadoEvidencia_Codigo_Internalname = "CONTAGEMRESULTADOEVIDENCIA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Anexar";
         edtContagemResultadoEvidencia_Arquivo_Filename = "";
         edtContagemResultadoEvidencia_Arquivo_Jsonclick = "";
         edtContagemResultadoEvidencia_Arquivo_Parameters = "";
         edtContagemResultadoEvidencia_Arquivo_Contenttype = "";
         edtContagemResultadoEvidencia_Arquivo_Filetype = "";
         edtContagemResultadoEvidencia_Arquivo_Display = 0;
         edtContagemResultadoEvidencia_Arquivo_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContagemResultadoEvidencia_Codigo_Jsonclick = "";
         edtContagemResultadoEvidencia_Codigo_Enabled = 0;
         edtContagemResultadoEvidencia_Codigo_Visible = 1;
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Enabled = 1;
         edtContagemResultado_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX4ASACONTAGEMRESULTADOEVIDENCIA_ENTIDADE2076( int A1493ContagemResultadoEvidencia_Owner ,
                                                                    int A52Contratada_AreaTrabalhoCod )
      {
         GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
         new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1494ContagemResultadoEvidencia_Entidade", A1494ContagemResultadoEvidencia_Entidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1494ContagemResultadoEvidencia_Entidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contagemresultado_codigo( String GX_Parm1 ,
                                                  int GX_Parm2 ,
                                                  int GX_Parm3 ,
                                                  int GX_Parm4 ,
                                                  int GX_Parm5 ,
                                                  String GX_Parm6 ,
                                                  String GX_Parm7 ,
                                                  String GX_Parm8 ,
                                                  String GX_Parm9 ,
                                                  int GX_Parm10 )
      {
         Gx_mode = GX_Parm1;
         A456ContagemResultado_Codigo = GX_Parm2;
         A490ContagemResultado_ContratadaCod = GX_Parm3;
         n490ContagemResultado_ContratadaCod = false;
         A1493ContagemResultadoEvidencia_Owner = GX_Parm4;
         n1493ContagemResultadoEvidencia_Owner = false;
         A52Contratada_AreaTrabalhoCod = GX_Parm5;
         n52Contratada_AreaTrabalhoCod = false;
         A457ContagemResultado_Demanda = GX_Parm6;
         n457ContagemResultado_Demanda = false;
         A493ContagemResultado_DemandaFM = GX_Parm7;
         n493ContagemResultado_DemandaFM = false;
         A1494ContagemResultadoEvidencia_Entidade = GX_Parm8;
         A501ContagemResultado_OsFsOsFm = GX_Parm9;
         AV7ContagemResultado_Codigo = GX_Parm10;
         /* Using cursor T002018 */
         pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
         }
         A490ContagemResultado_ContratadaCod = T002018_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T002018_n490ContagemResultado_ContratadaCod[0];
         A457ContagemResultado_Demanda = T002018_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = T002018_n457ContagemResultado_Demanda[0];
         A493ContagemResultado_DemandaFM = T002018_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = T002018_n493ContagemResultado_DemandaFM[0];
         pr_default.close(16);
         /* Using cursor T002019 */
         pr_default.execute(17, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T002019_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = T002019_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(17);
         GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
         new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
         A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
         A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV7ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A490ContagemResultado_ContratadaCod = 0;
            n490ContagemResultado_ContratadaCod = false;
            A457ContagemResultado_Demanda = "";
            n457ContagemResultado_Demanda = false;
            A493ContagemResultado_DemandaFM = "";
            n493ContagemResultado_DemandaFM = false;
            A52Contratada_AreaTrabalhoCod = 0;
            n52Contratada_AreaTrabalhoCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(A457ContagemResultado_Demanda);
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1494ContagemResultadoEvidencia_Entidade));
         isValidOutput.Add(A501ContagemResultado_OsFsOsFm);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8ContagemResultadoEvidencia_Codigo',fld:'vCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12202',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(18);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         Z1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A588ContagemResultadoEvidencia_Arquivo = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         A1393ContagemResultadoEvidencia_RdmnCreated = (DateTime)(DateTime.MinValue);
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A501ContagemResultado_OsFsOsFm = "";
         A1494ContagemResultadoEvidencia_Entidade = "";
         A1449ContagemResultadoEvidencia_Link = "";
         A587ContagemResultadoEvidencia_Descricao = "";
         A646TipoDocumento_Nome = "";
         AV15Pgmname = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode76 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z589ContagemResultadoEvidencia_NomeArq = "";
         Z590ContagemResultadoEvidencia_TipoArq = "";
         Z1449ContagemResultadoEvidencia_Link = "";
         Z588ContagemResultadoEvidencia_Arquivo = "";
         Z587ContagemResultadoEvidencia_Descricao = "";
         Z646TipoDocumento_Nome = "";
         Z457ContagemResultado_Demanda = "";
         Z493ContagemResultado_DemandaFM = "";
         T00205_A646TipoDocumento_Nome = new String[] {""} ;
         T00204_A490ContagemResultado_ContratadaCod = new int[1] ;
         T00204_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T00204_A457ContagemResultado_Demanda = new String[] {""} ;
         T00204_n457ContagemResultado_Demanda = new bool[] {false} ;
         T00204_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T00204_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T00206_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T00206_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T00207_A490ContagemResultado_ContratadaCod = new int[1] ;
         T00207_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T00207_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T00207_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         T00207_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         T00207_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         T00207_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         T00207_A457ContagemResultado_Demanda = new String[] {""} ;
         T00207_n457ContagemResultado_Demanda = new bool[] {false} ;
         T00207_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T00207_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T00207_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         T00207_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         T00207_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         T00207_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         T00207_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         T00207_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         T00207_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         T00207_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         T00207_A646TipoDocumento_Nome = new String[] {""} ;
         T00207_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         T00207_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         T00207_A456ContagemResultado_Codigo = new int[1] ;
         T00207_A645TipoDocumento_Codigo = new int[1] ;
         T00207_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00207_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T00207_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T00207_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         T00207_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         T00208_A490ContagemResultado_ContratadaCod = new int[1] ;
         T00208_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T00208_A457ContagemResultado_Demanda = new String[] {""} ;
         T00208_n457ContagemResultado_Demanda = new bool[] {false} ;
         T00208_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T00208_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T00209_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T00209_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T002010_A646TipoDocumento_Nome = new String[] {""} ;
         T002011_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T00203_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T00203_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         T00203_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         T00203_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         T00203_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         T00203_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         T00203_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         T00203_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         T00203_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         T00203_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         T00203_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         T00203_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         T00203_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         T00203_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         T00203_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         T00203_A456ContagemResultado_Codigo = new int[1] ;
         T00203_A645TipoDocumento_Codigo = new int[1] ;
         T00203_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00203_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         T00203_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         T002012_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T002013_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T00202_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T00202_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         T00202_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         T00202_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         T00202_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         T00202_A1449ContagemResultadoEvidencia_Link = new String[] {""} ;
         T00202_n1449ContagemResultadoEvidencia_Link = new bool[] {false} ;
         T00202_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         T00202_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         T00202_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         T00202_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         T00202_A1393ContagemResultadoEvidencia_RdmnCreated = new DateTime[] {DateTime.MinValue} ;
         T00202_n1393ContagemResultadoEvidencia_RdmnCreated = new bool[] {false} ;
         T00202_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         T00202_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         T00202_A456ContagemResultado_Codigo = new int[1] ;
         T00202_A645TipoDocumento_Codigo = new int[1] ;
         T00202_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00202_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         T00202_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         T002014_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T002018_A490ContagemResultado_ContratadaCod = new int[1] ;
         T002018_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T002018_A457ContagemResultado_Demanda = new String[] {""} ;
         T002018_n457ContagemResultado_Demanda = new bool[] {false} ;
         T002018_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T002018_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T002019_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T002019_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T002020_A646TipoDocumento_Nome = new String[] {""} ;
         T002021_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         T002022_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         GXt_char1 = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoevidencia__default(),
            new Object[][] {
                new Object[] {
               T00202_A586ContagemResultadoEvidencia_Codigo, T00202_A589ContagemResultadoEvidencia_NomeArq, T00202_n589ContagemResultadoEvidencia_NomeArq, T00202_A590ContagemResultadoEvidencia_TipoArq, T00202_n590ContagemResultadoEvidencia_TipoArq, T00202_A1449ContagemResultadoEvidencia_Link, T00202_n1449ContagemResultadoEvidencia_Link, T00202_A591ContagemResultadoEvidencia_Data, T00202_n591ContagemResultadoEvidencia_Data, T00202_A587ContagemResultadoEvidencia_Descricao,
               T00202_n587ContagemResultadoEvidencia_Descricao, T00202_A1393ContagemResultadoEvidencia_RdmnCreated, T00202_n1393ContagemResultadoEvidencia_RdmnCreated, T00202_A1493ContagemResultadoEvidencia_Owner, T00202_n1493ContagemResultadoEvidencia_Owner, T00202_A456ContagemResultado_Codigo, T00202_A645TipoDocumento_Codigo, T00202_n645TipoDocumento_Codigo, T00202_A588ContagemResultadoEvidencia_Arquivo, T00202_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               T00203_A586ContagemResultadoEvidencia_Codigo, T00203_A589ContagemResultadoEvidencia_NomeArq, T00203_n589ContagemResultadoEvidencia_NomeArq, T00203_A590ContagemResultadoEvidencia_TipoArq, T00203_n590ContagemResultadoEvidencia_TipoArq, T00203_A1449ContagemResultadoEvidencia_Link, T00203_n1449ContagemResultadoEvidencia_Link, T00203_A591ContagemResultadoEvidencia_Data, T00203_n591ContagemResultadoEvidencia_Data, T00203_A587ContagemResultadoEvidencia_Descricao,
               T00203_n587ContagemResultadoEvidencia_Descricao, T00203_A1393ContagemResultadoEvidencia_RdmnCreated, T00203_n1393ContagemResultadoEvidencia_RdmnCreated, T00203_A1493ContagemResultadoEvidencia_Owner, T00203_n1493ContagemResultadoEvidencia_Owner, T00203_A456ContagemResultado_Codigo, T00203_A645TipoDocumento_Codigo, T00203_n645TipoDocumento_Codigo, T00203_A588ContagemResultadoEvidencia_Arquivo, T00203_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               T00204_A490ContagemResultado_ContratadaCod, T00204_n490ContagemResultado_ContratadaCod, T00204_A457ContagemResultado_Demanda, T00204_n457ContagemResultado_Demanda, T00204_A493ContagemResultado_DemandaFM, T00204_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T00205_A646TipoDocumento_Nome
               }
               , new Object[] {
               T00206_A52Contratada_AreaTrabalhoCod, T00206_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T00207_A490ContagemResultado_ContratadaCod, T00207_n490ContagemResultado_ContratadaCod, T00207_A586ContagemResultadoEvidencia_Codigo, T00207_A589ContagemResultadoEvidencia_NomeArq, T00207_n589ContagemResultadoEvidencia_NomeArq, T00207_A590ContagemResultadoEvidencia_TipoArq, T00207_n590ContagemResultadoEvidencia_TipoArq, T00207_A457ContagemResultado_Demanda, T00207_n457ContagemResultado_Demanda, T00207_A493ContagemResultado_DemandaFM,
               T00207_n493ContagemResultado_DemandaFM, T00207_A1449ContagemResultadoEvidencia_Link, T00207_n1449ContagemResultadoEvidencia_Link, T00207_A591ContagemResultadoEvidencia_Data, T00207_n591ContagemResultadoEvidencia_Data, T00207_A587ContagemResultadoEvidencia_Descricao, T00207_n587ContagemResultadoEvidencia_Descricao, T00207_A1393ContagemResultadoEvidencia_RdmnCreated, T00207_n1393ContagemResultadoEvidencia_RdmnCreated, T00207_A646TipoDocumento_Nome,
               T00207_A1493ContagemResultadoEvidencia_Owner, T00207_n1493ContagemResultadoEvidencia_Owner, T00207_A456ContagemResultado_Codigo, T00207_A645TipoDocumento_Codigo, T00207_n645TipoDocumento_Codigo, T00207_A52Contratada_AreaTrabalhoCod, T00207_n52Contratada_AreaTrabalhoCod, T00207_A588ContagemResultadoEvidencia_Arquivo, T00207_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               T00208_A490ContagemResultado_ContratadaCod, T00208_n490ContagemResultado_ContratadaCod, T00208_A457ContagemResultado_Demanda, T00208_n457ContagemResultado_Demanda, T00208_A493ContagemResultado_DemandaFM, T00208_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T00209_A52Contratada_AreaTrabalhoCod, T00209_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T002010_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002011_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               T002012_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               T002013_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               T002014_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002018_A490ContagemResultado_ContratadaCod, T002018_n490ContagemResultado_ContratadaCod, T002018_A457ContagemResultado_Demanda, T002018_n457ContagemResultado_Demanda, T002018_A493ContagemResultado_DemandaFM, T002018_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               T002019_A52Contratada_AreaTrabalhoCod, T002019_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T002020_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002021_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               T002022_A586ContagemResultadoEvidencia_Codigo
               }
            }
         );
         Z591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         A591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         i591ContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         n591ContagemResultadoEvidencia_Data = false;
         AV15Pgmname = "ContagemResultadoEvidencia";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short edtContagemResultadoEvidencia_Arquivo_Display ;
      private short Gx_BScreen ;
      private short RcdFound76 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV8ContagemResultadoEvidencia_Codigo ;
      private int Z586ContagemResultadoEvidencia_Codigo ;
      private int Z1493ContagemResultadoEvidencia_Owner ;
      private int Z456ContagemResultado_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int N456ContagemResultado_Codigo ;
      private int N645TipoDocumento_Codigo ;
      private int A1493ContagemResultadoEvidencia_Owner ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A645TipoDocumento_Codigo ;
      private int AV8ContagemResultadoEvidencia_Codigo ;
      private int trnEnded ;
      private int edtContagemResultado_Codigo_Visible ;
      private int edtContagemResultado_Codigo_Enabled ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int edtContagemResultadoEvidencia_Codigo_Enabled ;
      private int edtContagemResultadoEvidencia_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContagemResultadoEvidencia_Arquivo_Enabled ;
      private int AV12Insert_ContagemResultado_Codigo ;
      private int AV14Insert_TipoDocumento_Codigo ;
      private int AV7ContagemResultado_Codigo ;
      private int AV16GXV1 ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int Z52Contratada_AreaTrabalhoCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoEvidencia_Arquivo_Internalname ;
      private String TempTags ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultadoEvidencia_Codigo_Internalname ;
      private String edtContagemResultadoEvidencia_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_arquivo_Internalname ;
      private String lblTextblockcontagemresultadoevidencia_arquivo_Jsonclick ;
      private String edtContagemResultadoEvidencia_Arquivo_Filename ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String edtContagemResultadoEvidencia_Arquivo_Filetype ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String edtContagemResultadoEvidencia_Arquivo_Contenttype ;
      private String edtContagemResultadoEvidencia_Arquivo_Parameters ;
      private String edtContagemResultadoEvidencia_Arquivo_Jsonclick ;
      private String A1494ContagemResultadoEvidencia_Entidade ;
      private String A646TipoDocumento_Nome ;
      private String AV15Pgmname ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode76 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z589ContagemResultadoEvidencia_NomeArq ;
      private String Z590ContagemResultadoEvidencia_TipoArq ;
      private String Z646TipoDocumento_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXt_char1 ;
      private DateTime Z591ContagemResultadoEvidencia_Data ;
      private DateTime Z1393ContagemResultadoEvidencia_RdmnCreated ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private DateTime A1393ContagemResultadoEvidencia_RdmnCreated ;
      private DateTime i591ContagemResultadoEvidencia_Data ;
      private bool entryPointCalled ;
      private bool n1493ContagemResultadoEvidencia_Owner ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n645TipoDocumento_Codigo ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n1449ContagemResultadoEvidencia_Link ;
      private bool n587ContagemResultadoEvidencia_Descricao ;
      private bool returnInSub ;
      private String A1449ContagemResultadoEvidencia_Link ;
      private String A587ContagemResultadoEvidencia_Descricao ;
      private String Z1449ContagemResultadoEvidencia_Link ;
      private String Z587ContagemResultadoEvidencia_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String Z457ContagemResultado_Demanda ;
      private String Z493ContagemResultado_DemandaFM ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private String Z588ContagemResultadoEvidencia_Arquivo ;
      private IGxSession AV11WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T00205_A646TipoDocumento_Nome ;
      private int[] T00204_A490ContagemResultado_ContratadaCod ;
      private bool[] T00204_n490ContagemResultado_ContratadaCod ;
      private String[] T00204_A457ContagemResultado_Demanda ;
      private bool[] T00204_n457ContagemResultado_Demanda ;
      private String[] T00204_A493ContagemResultado_DemandaFM ;
      private bool[] T00204_n493ContagemResultado_DemandaFM ;
      private int[] T00206_A52Contratada_AreaTrabalhoCod ;
      private bool[] T00206_n52Contratada_AreaTrabalhoCod ;
      private int[] T00207_A490ContagemResultado_ContratadaCod ;
      private bool[] T00207_n490ContagemResultado_ContratadaCod ;
      private int[] T00207_A586ContagemResultadoEvidencia_Codigo ;
      private String[] T00207_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] T00207_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] T00207_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] T00207_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] T00207_A457ContagemResultado_Demanda ;
      private bool[] T00207_n457ContagemResultado_Demanda ;
      private String[] T00207_A493ContagemResultado_DemandaFM ;
      private bool[] T00207_n493ContagemResultado_DemandaFM ;
      private String[] T00207_A1449ContagemResultadoEvidencia_Link ;
      private bool[] T00207_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] T00207_A591ContagemResultadoEvidencia_Data ;
      private bool[] T00207_n591ContagemResultadoEvidencia_Data ;
      private String[] T00207_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] T00207_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] T00207_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] T00207_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private String[] T00207_A646TipoDocumento_Nome ;
      private int[] T00207_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] T00207_n1493ContagemResultadoEvidencia_Owner ;
      private int[] T00207_A456ContagemResultado_Codigo ;
      private int[] T00207_A645TipoDocumento_Codigo ;
      private bool[] T00207_n645TipoDocumento_Codigo ;
      private int[] T00207_A52Contratada_AreaTrabalhoCod ;
      private bool[] T00207_n52Contratada_AreaTrabalhoCod ;
      private String[] T00207_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] T00207_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] T00208_A490ContagemResultado_ContratadaCod ;
      private bool[] T00208_n490ContagemResultado_ContratadaCod ;
      private String[] T00208_A457ContagemResultado_Demanda ;
      private bool[] T00208_n457ContagemResultado_Demanda ;
      private String[] T00208_A493ContagemResultado_DemandaFM ;
      private bool[] T00208_n493ContagemResultado_DemandaFM ;
      private int[] T00209_A52Contratada_AreaTrabalhoCod ;
      private bool[] T00209_n52Contratada_AreaTrabalhoCod ;
      private String[] T002010_A646TipoDocumento_Nome ;
      private int[] T002011_A586ContagemResultadoEvidencia_Codigo ;
      private int[] T00203_A586ContagemResultadoEvidencia_Codigo ;
      private String[] T00203_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] T00203_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] T00203_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] T00203_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] T00203_A1449ContagemResultadoEvidencia_Link ;
      private bool[] T00203_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] T00203_A591ContagemResultadoEvidencia_Data ;
      private bool[] T00203_n591ContagemResultadoEvidencia_Data ;
      private String[] T00203_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] T00203_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] T00203_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] T00203_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private int[] T00203_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] T00203_n1493ContagemResultadoEvidencia_Owner ;
      private int[] T00203_A456ContagemResultado_Codigo ;
      private int[] T00203_A645TipoDocumento_Codigo ;
      private bool[] T00203_n645TipoDocumento_Codigo ;
      private String[] T00203_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] T00203_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] T002012_A586ContagemResultadoEvidencia_Codigo ;
      private int[] T002013_A586ContagemResultadoEvidencia_Codigo ;
      private int[] T00202_A586ContagemResultadoEvidencia_Codigo ;
      private String[] T00202_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] T00202_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] T00202_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] T00202_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] T00202_A1449ContagemResultadoEvidencia_Link ;
      private bool[] T00202_n1449ContagemResultadoEvidencia_Link ;
      private DateTime[] T00202_A591ContagemResultadoEvidencia_Data ;
      private bool[] T00202_n591ContagemResultadoEvidencia_Data ;
      private String[] T00202_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] T00202_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] T00202_A1393ContagemResultadoEvidencia_RdmnCreated ;
      private bool[] T00202_n1393ContagemResultadoEvidencia_RdmnCreated ;
      private int[] T00202_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] T00202_n1493ContagemResultadoEvidencia_Owner ;
      private int[] T00202_A456ContagemResultado_Codigo ;
      private int[] T00202_A645TipoDocumento_Codigo ;
      private bool[] T00202_n645TipoDocumento_Codigo ;
      private String[] T00202_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] T00202_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] T002014_A586ContagemResultadoEvidencia_Codigo ;
      private int[] T002018_A490ContagemResultado_ContratadaCod ;
      private bool[] T002018_n490ContagemResultado_ContratadaCod ;
      private String[] T002018_A457ContagemResultado_Demanda ;
      private bool[] T002018_n457ContagemResultado_Demanda ;
      private String[] T002018_A493ContagemResultado_DemandaFM ;
      private bool[] T002018_n493ContagemResultado_DemandaFM ;
      private int[] T002019_A52Contratada_AreaTrabalhoCod ;
      private bool[] T002019_n52Contratada_AreaTrabalhoCod ;
      private String[] T002020_A646TipoDocumento_Nome ;
      private int[] T002021_A1769ContagemResultadoArtefato_Codigo ;
      private int[] T002022_A586ContagemResultadoEvidencia_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class contagemresultadoevidencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00207 ;
          prmT00207 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00204 ;
          prmT00204 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00206 ;
          prmT00206 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00205 ;
          prmT00205 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00208 ;
          prmT00208 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00209 ;
          prmT00209 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002010 ;
          prmT002010 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002011 ;
          prmT002011 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00203 ;
          prmT00203 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002012 ;
          prmT002012 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002013 ;
          prmT002013 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00202 ;
          prmT00202 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002014 ;
          prmT002014 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoEvidencia_RdmnCreated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002015 ;
          prmT002015 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultadoEvidencia_RdmnCreated",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoEvidencia_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002016 ;
          prmT002016 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002017 ;
          prmT002017 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002020 ;
          prmT002020 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002021 ;
          prmT002021 = new Object[] {
          new Object[] {"@ContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002022 ;
          prmT002022 = new Object[] {
          } ;
          Object[] prmT002018 ;
          prmT002018 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002019 ;
          prmT002019 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00202", "SELECT [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Owner], [ContagemResultado_Codigo], [TipoDocumento_Codigo], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (UPDLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00202,1,0,true,false )
             ,new CursorDef("T00203", "SELECT [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Owner], [ContagemResultado_Codigo], [TipoDocumento_Codigo], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00203,1,0,true,false )
             ,new CursorDef("T00204", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00204,1,0,true,false )
             ,new CursorDef("T00205", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00205,1,0,true,false )
             ,new CursorDef("T00206", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00206,1,0,true,false )
             ,new CursorDef("T00207", "SELECT T3.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, TM1.[ContagemResultadoEvidencia_Codigo], TM1.[ContagemResultadoEvidencia_NomeArq], TM1.[ContagemResultadoEvidencia_TipoArq], T3.[ContagemResultado_Demanda], T3.[ContagemResultado_DemandaFM], TM1.[ContagemResultadoEvidencia_Link], TM1.[ContagemResultadoEvidencia_Data], TM1.[ContagemResultadoEvidencia_Descricao], TM1.[ContagemResultadoEvidencia_RdmnCreated], T2.[TipoDocumento_Nome], TM1.[ContagemResultadoEvidencia_Owner], TM1.[ContagemResultado_Codigo], TM1.[TipoDocumento_Codigo], T4.[Contratada_AreaTrabalhoCod], TM1.[ContagemResultadoEvidencia_Arquivo] FROM ((([ContagemResultadoEvidencia] TM1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = TM1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[ContagemResultado_ContratadaCod]) WHERE TM1.[ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo ORDER BY TM1.[ContagemResultadoEvidencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00207,100,0,true,false )
             ,new CursorDef("T00208", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00208,1,0,true,false )
             ,new CursorDef("T00209", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00209,1,0,true,false )
             ,new CursorDef("T002010", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002010,1,0,true,false )
             ,new CursorDef("T002011", "SELECT [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002011,1,0,true,false )
             ,new CursorDef("T002012", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE ( [ContagemResultadoEvidencia_Codigo] > @ContagemResultadoEvidencia_Codigo) ORDER BY [ContagemResultadoEvidencia_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002012,1,0,true,true )
             ,new CursorDef("T002013", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE ( [ContagemResultadoEvidencia_Codigo] < @ContagemResultadoEvidencia_Codigo) ORDER BY [ContagemResultadoEvidencia_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002013,1,0,true,true )
             ,new CursorDef("T002014", "INSERT INTO [ContagemResultadoEvidencia]([ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Link], [ContagemResultadoEvidencia_Arquivo], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Descricao], [ContagemResultadoEvidencia_RdmnCreated], [ContagemResultadoEvidencia_Owner], [ContagemResultado_Codigo], [TipoDocumento_Codigo]) VALUES(@ContagemResultadoEvidencia_NomeArq, @ContagemResultadoEvidencia_TipoArq, @ContagemResultadoEvidencia_Link, @ContagemResultadoEvidencia_Arquivo, @ContagemResultadoEvidencia_Data, @ContagemResultadoEvidencia_Descricao, @ContagemResultadoEvidencia_RdmnCreated, @ContagemResultadoEvidencia_Owner, @ContagemResultado_Codigo, @TipoDocumento_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002014)
             ,new CursorDef("T002015", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_NomeArq]=@ContagemResultadoEvidencia_NomeArq, [ContagemResultadoEvidencia_TipoArq]=@ContagemResultadoEvidencia_TipoArq, [ContagemResultadoEvidencia_Link]=@ContagemResultadoEvidencia_Link, [ContagemResultadoEvidencia_Data]=@ContagemResultadoEvidencia_Data, [ContagemResultadoEvidencia_Descricao]=@ContagemResultadoEvidencia_Descricao, [ContagemResultadoEvidencia_RdmnCreated]=@ContagemResultadoEvidencia_RdmnCreated, [ContagemResultadoEvidencia_Owner]=@ContagemResultadoEvidencia_Owner, [ContagemResultado_Codigo]=@ContagemResultado_Codigo, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmT002015)
             ,new CursorDef("T002016", "UPDATE [ContagemResultadoEvidencia] SET [ContagemResultadoEvidencia_Arquivo]=@ContagemResultadoEvidencia_Arquivo  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmT002016)
             ,new CursorDef("T002017", "DELETE FROM [ContagemResultadoEvidencia]  WHERE [ContagemResultadoEvidencia_Codigo] = @ContagemResultadoEvidencia_Codigo", GxErrorMask.GX_NOMASK,prmT002017)
             ,new CursorDef("T002018", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_Demanda], [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002018,1,0,true,false )
             ,new CursorDef("T002019", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002019,1,0,true,false )
             ,new CursorDef("T002020", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002020,1,0,true,false )
             ,new CursorDef("T002021", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_EvdCod] = @ContagemResultadoEvidencia_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002021,1,0,true,true )
             ,new CursorDef("T002022", "SELECT [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) ORDER BY [ContagemResultadoEvidencia_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002022,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(11, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(11, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDateTime(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 50) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getBLOBFile(16, rslt.getString(4, 10), rslt.getString(3, 50)) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[15]);
                }
                stmt.SetParameter(9, (int)parms[16]);
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[18]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                stmt.SetParameter(10, (int)parms[17]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
