/*
               File: PromptPadroesArtefatos
        Description: Selecione Padroes Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:37:47.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptpadroesartefatos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptpadroesartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptpadroesartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutPadroesArtefatos_Codigo ,
                           ref String aP1_InOutPadroesArtefatos_Descricao )
      {
         this.AV7InOutPadroesArtefatos_Codigo = aP0_InOutPadroesArtefatos_Codigo;
         this.AV8InOutPadroesArtefatos_Descricao = aP1_InOutPadroesArtefatos_Descricao;
         executePrivate();
         aP0_InOutPadroesArtefatos_Codigo=this.AV7InOutPadroesArtefatos_Codigo;
         aP1_InOutPadroesArtefatos_Descricao=this.AV8InOutPadroesArtefatos_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_65 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_65_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_65_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17PadroesArtefatos_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
               AV18MetodologiaFases_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22PadroesArtefatos_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
               AV23MetodologiaFases_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV34TFPadroesArtefatos_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFPadroesArtefatos_Descricao", AV34TFPadroesArtefatos_Descricao);
               AV35TFPadroesArtefatos_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFPadroesArtefatos_Descricao_Sel", AV35TFPadroesArtefatos_Descricao_Sel);
               AV38TFMetodologiaFases_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFMetodologiaFases_Nome", AV38TFMetodologiaFases_Nome);
               AV39TFMetodologiaFases_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFMetodologiaFases_Nome_Sel", AV39TFMetodologiaFases_Nome_Sel);
               AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace", AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace);
               AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace", AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace);
               AV48Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutPadroesArtefatos_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutPadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutPadroesArtefatos_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutPadroesArtefatos_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutPadroesArtefatos_Descricao", AV8InOutPadroesArtefatos_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA832( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV48Pgmname = "PromptPadroesArtefatos";
               context.Gx_err = 0;
               WS832( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE832( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117374723");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptpadroesartefatos.aspx") + "?" + UrlEncode("" +AV7InOutPadroesArtefatos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutPadroesArtefatos_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPADROESARTEFATOS_DESCRICAO1", AV17PadroesArtefatos_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vMETODOLOGIAFASES_NOME1", StringUtil.RTrim( AV18MetodologiaFases_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vPADROESARTEFATOS_DESCRICAO2", AV22PadroesArtefatos_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vMETODOLOGIAFASES_NOME2", StringUtil.RTrim( AV23MetodologiaFases_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPADROESARTEFATOS_DESCRICAO", AV34TFPadroesArtefatos_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFPADROESARTEFATOS_DESCRICAO_SEL", AV35TFPadroesArtefatos_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_NOME", StringUtil.RTrim( AV38TFMetodologiaFases_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIAFASES_NOME_SEL", StringUtil.RTrim( AV39TFMetodologiaFases_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_65", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_65), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPADROESARTEFATOS_DESCRICAOTITLEFILTERDATA", AV33PadroesArtefatos_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPADROESARTEFATOS_DESCRICAOTITLEFILTERDATA", AV33PadroesArtefatos_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMETODOLOGIAFASES_NOMETITLEFILTERDATA", AV37MetodologiaFases_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMETODOLOGIAFASES_NOMETITLEFILTERDATA", AV37MetodologiaFases_NomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV48Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTPADROESARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutPadroesArtefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTPADROESARTEFATOS_DESCRICAO", AV8InOutPadroesArtefatos_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Caption", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Cls", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_padroesartefatos_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_padroesartefatos_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_padroesartefatos_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_padroesartefatos_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_padroesartefatos_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_padroesartefatos_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Caption", StringUtil.RTrim( Ddo_metodologiafases_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Tooltip", StringUtil.RTrim( Ddo_metodologiafases_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Cls", StringUtil.RTrim( Ddo_metodologiafases_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_metodologiafases_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_metodologiafases_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_metodologiafases_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_metodologiafases_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Sortedstatus", StringUtil.RTrim( Ddo_metodologiafases_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includefilter", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filtertype", StringUtil.RTrim( Ddo_metodologiafases_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_metodologiafases_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Datalisttype", StringUtil.RTrim( Ddo_metodologiafases_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Datalistproc", StringUtil.RTrim( Ddo_metodologiafases_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_metodologiafases_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Sortasc", StringUtil.RTrim( Ddo_metodologiafases_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Sortdsc", StringUtil.RTrim( Ddo_metodologiafases_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Loadingdata", StringUtil.RTrim( Ddo_metodologiafases_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Cleanfilter", StringUtil.RTrim( Ddo_metodologiafases_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Noresultsfound", StringUtil.RTrim( Ddo_metodologiafases_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_metodologiafases_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PADROESARTEFATOS_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_padroesartefatos_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Activeeventkey", StringUtil.RTrim( Ddo_metodologiafases_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_metodologiafases_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIAFASES_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_metodologiafases_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm832( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptPadroesArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Padroes Artefatos" ;
      }

      protected void WB830( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_832( true) ;
         }
         else
         {
            wb_table1_2_832( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_832e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(75, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpadroesartefatos_descricao_Internalname, AV34TFPadroesArtefatos_Descricao, StringUtil.RTrim( context.localUtil.Format( AV34TFPadroesArtefatos_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpadroesartefatos_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfpadroesartefatos_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfpadroesartefatos_descricao_sel_Internalname, AV35TFPadroesArtefatos_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV35TFPadroesArtefatos_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfpadroesartefatos_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfpadroesartefatos_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_nome_Internalname, StringUtil.RTrim( AV38TFMetodologiaFases_Nome), StringUtil.RTrim( context.localUtil.Format( AV38TFMetodologiaFases_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologiafases_nome_sel_Internalname, StringUtil.RTrim( AV39TFMetodologiaFases_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV39TFMetodologiaFases_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologiafases_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologiafases_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PADROESARTEFATOS_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Internalname, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", 0, edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptPadroesArtefatos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_METODOLOGIAFASES_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", 0, edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptPadroesArtefatos.htm");
         }
         wbLoad = true;
      }

      protected void START832( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Padroes Artefatos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP830( ) ;
      }

      protected void WS832( )
      {
         START832( ) ;
         EVT832( ) ;
      }

      protected void EVT832( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11832 */
                           E11832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_PADROESARTEFATOS_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12832 */
                           E12832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_METODOLOGIAFASES_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13832 */
                           E13832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14832 */
                           E14832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15832 */
                           E15832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16832 */
                           E16832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17832 */
                           E17832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18832 */
                           E18832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19832 */
                           E19832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20832 */
                           E20832 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_65_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
                           SubsflControlProps_652( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV47Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A150PadroesArtefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPadroesArtefatos_Codigo_Internalname), ",", "."));
                           A151PadroesArtefatos_Descricao = StringUtil.Upper( cgiGet( edtPadroesArtefatos_Descricao_Internalname));
                           A147MetodologiaFases_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMetodologiaFases_Codigo_Internalname), ",", "."));
                           A148MetodologiaFases_Nome = StringUtil.Upper( cgiGet( edtMetodologiaFases_Nome_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E21832 */
                                 E21832 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E22832 */
                                 E22832 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23832 */
                                 E23832 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Padroesartefatos_descricao1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPADROESARTEFATOS_DESCRICAO1"), AV17PadroesArtefatos_Descricao1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Metodologiafases_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME1"), AV18MetodologiaFases_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Padroesartefatos_descricao2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vPADROESARTEFATOS_DESCRICAO2"), AV22PadroesArtefatos_Descricao2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Metodologiafases_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME2"), AV23MetodologiaFases_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfpadroesartefatos_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPADROESARTEFATOS_DESCRICAO"), AV34TFPadroesArtefatos_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfpadroesartefatos_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPADROESARTEFATOS_DESCRICAO_SEL"), AV35TFPadroesArtefatos_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfmetodologiafases_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME"), AV38TFMetodologiaFases_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfmetodologiafases_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME_SEL"), AV39TFMetodologiaFases_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E24832 */
                                       E24832 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE832( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm832( ) ;
            }
         }
      }

      protected void PA832( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PADROESARTEFATOS_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector1.addItem("METODOLOGIAFASES_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PADROESARTEFATOS_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector2.addItem("METODOLOGIAFASES_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_652( ) ;
         while ( nGXsfl_65_idx <= nRC_GXsfl_65 )
         {
            sendrow_652( ) ;
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17PadroesArtefatos_Descricao1 ,
                                       String AV18MetodologiaFases_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22PadroesArtefatos_Descricao2 ,
                                       String AV23MetodologiaFases_Nome2 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       String AV34TFPadroesArtefatos_Descricao ,
                                       String AV35TFPadroesArtefatos_Descricao_Sel ,
                                       String AV38TFMetodologiaFases_Nome ,
                                       String AV39TFMetodologiaFases_Nome_Sel ,
                                       String AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace ,
                                       String AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace ,
                                       String AV48Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF832( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PADROESARTEFATOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PADROESARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A150PadroesArtefatos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PADROESARTEFATOS_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A151PadroesArtefatos_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "PADROESARTEFATOS_DESCRICAO", A151PadroesArtefatos_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIAFASES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF832( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV48Pgmname = "PromptPadroesArtefatos";
         context.Gx_err = 0;
      }

      protected void RF832( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 65;
         /* Execute user event: E22832 */
         E22832 ();
         nGXsfl_65_idx = 1;
         sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
         SubsflControlProps_652( ) ;
         nGXsfl_65_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_652( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17PadroesArtefatos_Descricao1 ,
                                                 AV18MetodologiaFases_Nome1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22PadroesArtefatos_Descricao2 ,
                                                 AV23MetodologiaFases_Nome2 ,
                                                 AV35TFPadroesArtefatos_Descricao_Sel ,
                                                 AV34TFPadroesArtefatos_Descricao ,
                                                 AV39TFMetodologiaFases_Nome_Sel ,
                                                 AV38TFMetodologiaFases_Nome ,
                                                 A151PadroesArtefatos_Descricao ,
                                                 A148MetodologiaFases_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17PadroesArtefatos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17PadroesArtefatos_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
            lV17PadroesArtefatos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17PadroesArtefatos_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
            lV18MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18MetodologiaFases_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
            lV18MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18MetodologiaFases_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
            lV22PadroesArtefatos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22PadroesArtefatos_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
            lV22PadroesArtefatos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22PadroesArtefatos_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
            lV23MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23MetodologiaFases_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
            lV23MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23MetodologiaFases_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
            lV34TFPadroesArtefatos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV34TFPadroesArtefatos_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFPadroesArtefatos_Descricao", AV34TFPadroesArtefatos_Descricao);
            lV38TFMetodologiaFases_Nome = StringUtil.PadR( StringUtil.RTrim( AV38TFMetodologiaFases_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFMetodologiaFases_Nome", AV38TFMetodologiaFases_Nome);
            /* Using cursor H00832 */
            pr_default.execute(0, new Object[] {lV17PadroesArtefatos_Descricao1, lV17PadroesArtefatos_Descricao1, lV18MetodologiaFases_Nome1, lV18MetodologiaFases_Nome1, lV22PadroesArtefatos_Descricao2, lV22PadroesArtefatos_Descricao2, lV23MetodologiaFases_Nome2, lV23MetodologiaFases_Nome2, lV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, lV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_65_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A148MetodologiaFases_Nome = H00832_A148MetodologiaFases_Nome[0];
               A147MetodologiaFases_Codigo = H00832_A147MetodologiaFases_Codigo[0];
               A151PadroesArtefatos_Descricao = H00832_A151PadroesArtefatos_Descricao[0];
               A150PadroesArtefatos_Codigo = H00832_A150PadroesArtefatos_Codigo[0];
               A148MetodologiaFases_Nome = H00832_A148MetodologiaFases_Nome[0];
               /* Execute user event: E23832 */
               E23832 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 65;
            WB830( ) ;
         }
         nGXsfl_65_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17PadroesArtefatos_Descricao1 ,
                                              AV18MetodologiaFases_Nome1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22PadroesArtefatos_Descricao2 ,
                                              AV23MetodologiaFases_Nome2 ,
                                              AV35TFPadroesArtefatos_Descricao_Sel ,
                                              AV34TFPadroesArtefatos_Descricao ,
                                              AV39TFMetodologiaFases_Nome_Sel ,
                                              AV38TFMetodologiaFases_Nome ,
                                              A151PadroesArtefatos_Descricao ,
                                              A148MetodologiaFases_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17PadroesArtefatos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17PadroesArtefatos_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
         lV17PadroesArtefatos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17PadroesArtefatos_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
         lV18MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18MetodologiaFases_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
         lV18MetodologiaFases_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18MetodologiaFases_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
         lV22PadroesArtefatos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22PadroesArtefatos_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
         lV22PadroesArtefatos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV22PadroesArtefatos_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
         lV23MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23MetodologiaFases_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
         lV23MetodologiaFases_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23MetodologiaFases_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
         lV34TFPadroesArtefatos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV34TFPadroesArtefatos_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFPadroesArtefatos_Descricao", AV34TFPadroesArtefatos_Descricao);
         lV38TFMetodologiaFases_Nome = StringUtil.PadR( StringUtil.RTrim( AV38TFMetodologiaFases_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFMetodologiaFases_Nome", AV38TFMetodologiaFases_Nome);
         /* Using cursor H00833 */
         pr_default.execute(1, new Object[] {lV17PadroesArtefatos_Descricao1, lV17PadroesArtefatos_Descricao1, lV18MetodologiaFases_Nome1, lV18MetodologiaFases_Nome1, lV22PadroesArtefatos_Descricao2, lV22PadroesArtefatos_Descricao2, lV23MetodologiaFases_Nome2, lV23MetodologiaFases_Nome2, lV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, lV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel});
         GRID_nRecordCount = H00833_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP830( )
      {
         /* Before Start, stand alone formulas. */
         AV48Pgmname = "PromptPadroesArtefatos";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21832 */
         E21832 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV41DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPADROESARTEFATOS_DESCRICAOTITLEFILTERDATA"), AV33PadroesArtefatos_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMETODOLOGIAFASES_NOMETITLEFILTERDATA"), AV37MetodologiaFases_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17PadroesArtefatos_Descricao1 = StringUtil.Upper( cgiGet( edtavPadroesartefatos_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
            AV18MetodologiaFases_Nome1 = StringUtil.Upper( cgiGet( edtavMetodologiafases_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22PadroesArtefatos_Descricao2 = StringUtil.Upper( cgiGet( edtavPadroesartefatos_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
            AV23MetodologiaFases_Nome2 = StringUtil.Upper( cgiGet( edtavMetodologiafases_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV34TFPadroesArtefatos_Descricao = StringUtil.Upper( cgiGet( edtavTfpadroesartefatos_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFPadroesArtefatos_Descricao", AV34TFPadroesArtefatos_Descricao);
            AV35TFPadroesArtefatos_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfpadroesartefatos_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFPadroesArtefatos_Descricao_Sel", AV35TFPadroesArtefatos_Descricao_Sel);
            AV38TFMetodologiaFases_Nome = StringUtil.Upper( cgiGet( edtavTfmetodologiafases_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFMetodologiaFases_Nome", AV38TFMetodologiaFases_Nome);
            AV39TFMetodologiaFases_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmetodologiafases_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFMetodologiaFases_Nome_Sel", AV39TFMetodologiaFases_Nome_Sel);
            AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace", AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace);
            AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace = cgiGet( edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace", AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_65 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_65"), ",", "."));
            AV43GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV44GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_padroesartefatos_descricao_Caption = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Caption");
            Ddo_padroesartefatos_descricao_Tooltip = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Tooltip");
            Ddo_padroesartefatos_descricao_Cls = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Cls");
            Ddo_padroesartefatos_descricao_Filteredtext_set = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Filteredtext_set");
            Ddo_padroesartefatos_descricao_Selectedvalue_set = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Selectedvalue_set");
            Ddo_padroesartefatos_descricao_Dropdownoptionstype = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Dropdownoptionstype");
            Ddo_padroesartefatos_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_padroesartefatos_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Includesortasc"));
            Ddo_padroesartefatos_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Includesortdsc"));
            Ddo_padroesartefatos_descricao_Sortedstatus = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Sortedstatus");
            Ddo_padroesartefatos_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Includefilter"));
            Ddo_padroesartefatos_descricao_Filtertype = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Filtertype");
            Ddo_padroesartefatos_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Filterisrange"));
            Ddo_padroesartefatos_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Includedatalist"));
            Ddo_padroesartefatos_descricao_Datalisttype = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Datalisttype");
            Ddo_padroesartefatos_descricao_Datalistproc = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Datalistproc");
            Ddo_padroesartefatos_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_padroesartefatos_descricao_Sortasc = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Sortasc");
            Ddo_padroesartefatos_descricao_Sortdsc = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Sortdsc");
            Ddo_padroesartefatos_descricao_Loadingdata = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Loadingdata");
            Ddo_padroesartefatos_descricao_Cleanfilter = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Cleanfilter");
            Ddo_padroesartefatos_descricao_Noresultsfound = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Noresultsfound");
            Ddo_padroesartefatos_descricao_Searchbuttontext = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Searchbuttontext");
            Ddo_metodologiafases_nome_Caption = cgiGet( "DDO_METODOLOGIAFASES_NOME_Caption");
            Ddo_metodologiafases_nome_Tooltip = cgiGet( "DDO_METODOLOGIAFASES_NOME_Tooltip");
            Ddo_metodologiafases_nome_Cls = cgiGet( "DDO_METODOLOGIAFASES_NOME_Cls");
            Ddo_metodologiafases_nome_Filteredtext_set = cgiGet( "DDO_METODOLOGIAFASES_NOME_Filteredtext_set");
            Ddo_metodologiafases_nome_Selectedvalue_set = cgiGet( "DDO_METODOLOGIAFASES_NOME_Selectedvalue_set");
            Ddo_metodologiafases_nome_Dropdownoptionstype = cgiGet( "DDO_METODOLOGIAFASES_NOME_Dropdownoptionstype");
            Ddo_metodologiafases_nome_Titlecontrolidtoreplace = cgiGet( "DDO_METODOLOGIAFASES_NOME_Titlecontrolidtoreplace");
            Ddo_metodologiafases_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includesortasc"));
            Ddo_metodologiafases_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includesortdsc"));
            Ddo_metodologiafases_nome_Sortedstatus = cgiGet( "DDO_METODOLOGIAFASES_NOME_Sortedstatus");
            Ddo_metodologiafases_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includefilter"));
            Ddo_metodologiafases_nome_Filtertype = cgiGet( "DDO_METODOLOGIAFASES_NOME_Filtertype");
            Ddo_metodologiafases_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Filterisrange"));
            Ddo_metodologiafases_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIAFASES_NOME_Includedatalist"));
            Ddo_metodologiafases_nome_Datalisttype = cgiGet( "DDO_METODOLOGIAFASES_NOME_Datalisttype");
            Ddo_metodologiafases_nome_Datalistproc = cgiGet( "DDO_METODOLOGIAFASES_NOME_Datalistproc");
            Ddo_metodologiafases_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_METODOLOGIAFASES_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_metodologiafases_nome_Sortasc = cgiGet( "DDO_METODOLOGIAFASES_NOME_Sortasc");
            Ddo_metodologiafases_nome_Sortdsc = cgiGet( "DDO_METODOLOGIAFASES_NOME_Sortdsc");
            Ddo_metodologiafases_nome_Loadingdata = cgiGet( "DDO_METODOLOGIAFASES_NOME_Loadingdata");
            Ddo_metodologiafases_nome_Cleanfilter = cgiGet( "DDO_METODOLOGIAFASES_NOME_Cleanfilter");
            Ddo_metodologiafases_nome_Noresultsfound = cgiGet( "DDO_METODOLOGIAFASES_NOME_Noresultsfound");
            Ddo_metodologiafases_nome_Searchbuttontext = cgiGet( "DDO_METODOLOGIAFASES_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_padroesartefatos_descricao_Activeeventkey = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Activeeventkey");
            Ddo_padroesartefatos_descricao_Filteredtext_get = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Filteredtext_get");
            Ddo_padroesartefatos_descricao_Selectedvalue_get = cgiGet( "DDO_PADROESARTEFATOS_DESCRICAO_Selectedvalue_get");
            Ddo_metodologiafases_nome_Activeeventkey = cgiGet( "DDO_METODOLOGIAFASES_NOME_Activeeventkey");
            Ddo_metodologiafases_nome_Filteredtext_get = cgiGet( "DDO_METODOLOGIAFASES_NOME_Filteredtext_get");
            Ddo_metodologiafases_nome_Selectedvalue_get = cgiGet( "DDO_METODOLOGIAFASES_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPADROESARTEFATOS_DESCRICAO1"), AV17PadroesArtefatos_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME1"), AV18MetodologiaFases_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPADROESARTEFATOS_DESCRICAO2"), AV22PadroesArtefatos_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vMETODOLOGIAFASES_NOME2"), AV23MetodologiaFases_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPADROESARTEFATOS_DESCRICAO"), AV34TFPadroesArtefatos_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPADROESARTEFATOS_DESCRICAO_SEL"), AV35TFPadroesArtefatos_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME"), AV38TFMetodologiaFases_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIAFASES_NOME_SEL"), AV39TFMetodologiaFases_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21832 */
         E21832 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21832( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "PADROESARTEFATOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "PADROESARTEFATOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfpadroesartefatos_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpadroesartefatos_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpadroesartefatos_descricao_Visible), 5, 0)));
         edtavTfpadroesartefatos_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfpadroesartefatos_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfpadroesartefatos_descricao_sel_Visible), 5, 0)));
         edtavTfmetodologiafases_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_nome_Visible), 5, 0)));
         edtavTfmetodologiafases_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologiafases_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologiafases_nome_sel_Visible), 5, 0)));
         Ddo_padroesartefatos_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_PadroesArtefatos_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_padroesartefatos_descricao_Internalname, "TitleControlIdToReplace", Ddo_padroesartefatos_descricao_Titlecontrolidtoreplace);
         AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace = Ddo_padroesartefatos_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace", AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace);
         edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_metodologiafases_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_MetodologiaFases_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "TitleControlIdToReplace", Ddo_metodologiafases_nome_Titlecontrolidtoreplace);
         AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace = Ddo_metodologiafases_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace", AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace);
         edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Padroes Artefatos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o", 0);
         cmbavOrderedby.addItem("2", "Fase", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV41DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV41DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E22832( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33PadroesArtefatos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37MetodologiaFases_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPadroesArtefatos_Descricao_Titleformat = 2;
         edtPadroesArtefatos_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPadroesArtefatos_Descricao_Internalname, "Title", edtPadroesArtefatos_Descricao_Title);
         edtMetodologiaFases_Nome_Titleformat = 2;
         edtMetodologiaFases_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fase", AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologiaFases_Nome_Internalname, "Title", edtMetodologiaFases_Nome_Title);
         AV43GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridCurrentPage), 10, 0)));
         AV44GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33PadroesArtefatos_DescricaoTitleFilterData", AV33PadroesArtefatos_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37MetodologiaFases_NomeTitleFilterData", AV37MetodologiaFases_NomeTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11832( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV42PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV42PageToGo) ;
         }
      }

      protected void E12832( )
      {
         /* Ddo_padroesartefatos_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_padroesartefatos_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_padroesartefatos_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_padroesartefatos_descricao_Internalname, "SortedStatus", Ddo_padroesartefatos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_padroesartefatos_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_padroesartefatos_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_padroesartefatos_descricao_Internalname, "SortedStatus", Ddo_padroesartefatos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_padroesartefatos_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFPadroesArtefatos_Descricao = Ddo_padroesartefatos_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFPadroesArtefatos_Descricao", AV34TFPadroesArtefatos_Descricao);
            AV35TFPadroesArtefatos_Descricao_Sel = Ddo_padroesartefatos_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFPadroesArtefatos_Descricao_Sel", AV35TFPadroesArtefatos_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13832( )
      {
         /* Ddo_metodologiafases_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_metodologiafases_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologiafases_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologiafases_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFMetodologiaFases_Nome = Ddo_metodologiafases_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFMetodologiaFases_Nome", AV38TFMetodologiaFases_Nome);
            AV39TFMetodologiaFases_Nome_Sel = Ddo_metodologiafases_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFMetodologiaFases_Nome_Sel", AV39TFMetodologiaFases_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E23832( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV47Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 65;
         }
         sendrow_652( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_65_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(65, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E24832 */
         E24832 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24832( )
      {
         /* Enter Routine */
         AV7InOutPadroesArtefatos_Codigo = A150PadroesArtefatos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutPadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutPadroesArtefatos_Codigo), 6, 0)));
         AV8InOutPadroesArtefatos_Descricao = A151PadroesArtefatos_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutPadroesArtefatos_Descricao", AV8InOutPadroesArtefatos_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutPadroesArtefatos_Codigo,(String)AV8InOutPadroesArtefatos_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14832( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E18832( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
      }

      protected void E15832( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19832( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E16832( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17PadroesArtefatos_Descricao1, AV18MetodologiaFases_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22PadroesArtefatos_Descricao2, AV23MetodologiaFases_Nome2, AV19DynamicFiltersEnabled2, AV34TFPadroesArtefatos_Descricao, AV35TFPadroesArtefatos_Descricao_Sel, AV38TFMetodologiaFases_Nome, AV39TFMetodologiaFases_Nome_Sel, AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace, AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace, AV48Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20832( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E17832( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_padroesartefatos_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_padroesartefatos_descricao_Internalname, "SortedStatus", Ddo_padroesartefatos_descricao_Sortedstatus);
         Ddo_metodologiafases_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_padroesartefatos_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_padroesartefatos_descricao_Internalname, "SortedStatus", Ddo_padroesartefatos_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_metodologiafases_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SortedStatus", Ddo_metodologiafases_nome_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavPadroesartefatos_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPadroesartefatos_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPadroesartefatos_descricao1_Visible), 5, 0)));
         edtavMetodologiafases_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 )
         {
            edtavPadroesartefatos_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPadroesartefatos_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPadroesartefatos_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
         {
            edtavMetodologiafases_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavPadroesartefatos_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPadroesartefatos_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPadroesartefatos_descricao2_Visible), 5, 0)));
         edtavMetodologiafases_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 )
         {
            edtavPadroesartefatos_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPadroesartefatos_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPadroesartefatos_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
         {
            edtavMetodologiafases_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavMetodologiafases_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavMetodologiafases_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "PADROESARTEFATOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22PadroesArtefatos_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFPadroesArtefatos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFPadroesArtefatos_Descricao", AV34TFPadroesArtefatos_Descricao);
         Ddo_padroesartefatos_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_padroesartefatos_descricao_Internalname, "FilteredText_set", Ddo_padroesartefatos_descricao_Filteredtext_set);
         AV35TFPadroesArtefatos_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFPadroesArtefatos_Descricao_Sel", AV35TFPadroesArtefatos_Descricao_Sel);
         Ddo_padroesartefatos_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_padroesartefatos_descricao_Internalname, "SelectedValue_set", Ddo_padroesartefatos_descricao_Selectedvalue_set);
         AV38TFMetodologiaFases_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFMetodologiaFases_Nome", AV38TFMetodologiaFases_Nome);
         Ddo_metodologiafases_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "FilteredText_set", Ddo_metodologiafases_nome_Filteredtext_set);
         AV39TFMetodologiaFases_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFMetodologiaFases_Nome_Sel", AV39TFMetodologiaFases_Nome_Sel);
         Ddo_metodologiafases_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologiafases_nome_Internalname, "SelectedValue_set", Ddo_metodologiafases_nome_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "PADROESARTEFATOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17PadroesArtefatos_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S132( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17PadroesArtefatos_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PadroesArtefatos_Descricao1", AV17PadroesArtefatos_Descricao1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18MetodologiaFases_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18MetodologiaFases_Nome1", AV18MetodologiaFases_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22PadroesArtefatos_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22PadroesArtefatos_Descricao2", AV22PadroesArtefatos_Descricao2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23MetodologiaFases_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23MetodologiaFases_Nome2", AV23MetodologiaFases_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFPadroesArtefatos_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPADROESARTEFATOS_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFPadroesArtefatos_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFPadroesArtefatos_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPADROESARTEFATOS_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFPadroesArtefatos_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFMetodologiaFases_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIAFASES_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFMetodologiaFases_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFMetodologiaFases_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIAFASES_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFMetodologiaFases_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV48Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17PadroesArtefatos_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17PadroesArtefatos_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18MetodologiaFases_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18MetodologiaFases_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22PadroesArtefatos_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22PadroesArtefatos_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23MetodologiaFases_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23MetodologiaFases_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_832( true) ;
         }
         else
         {
            wb_table2_5_832( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_832e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_832( true) ;
         }
         else
         {
            wb_table3_59_832( false) ;
         }
         return  ;
      }

      protected void wb_table3_59_832e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_832e( true) ;
         }
         else
         {
            wb_table1_2_832e( false) ;
         }
      }

      protected void wb_table3_59_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_62_832( true) ;
         }
         else
         {
            wb_table4_62_832( false) ;
         }
         return  ;
      }

      protected void wb_table4_62_832e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_832e( true) ;
         }
         else
         {
            wb_table3_59_832e( false) ;
         }
      }

      protected void wb_table4_62_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"65\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPadroesArtefatos_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtPadroesArtefatos_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPadroesArtefatos_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMetodologiaFases_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMetodologiaFases_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMetodologiaFases_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A150PadroesArtefatos_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A151PadroesArtefatos_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPadroesArtefatos_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPadroesArtefatos_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A148MetodologiaFases_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMetodologiaFases_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologiaFases_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 65 )
         {
            wbEnd = 0;
            nRC_GXsfl_65 = (short)(nGXsfl_65_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_62_832e( true) ;
         }
         else
         {
            wb_table4_62_832e( false) ;
         }
      }

      protected void wb_table2_5_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptPadroesArtefatos.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_832( true) ;
         }
         else
         {
            wb_table5_14_832( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_832e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_832e( true) ;
         }
         else
         {
            wb_table2_5_832e( false) ;
         }
      }

      protected void wb_table5_14_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_832( true) ;
         }
         else
         {
            wb_table6_19_832( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_832e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_832e( true) ;
         }
         else
         {
            wb_table5_14_832e( false) ;
         }
      }

      protected void wb_table6_19_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptPadroesArtefatos.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_832( true) ;
         }
         else
         {
            wb_table7_28_832( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_832e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptPadroesArtefatos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptPadroesArtefatos.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_46_832( true) ;
         }
         else
         {
            wb_table8_46_832( false) ;
         }
         return  ;
      }

      protected void wb_table8_46_832e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_832e( true) ;
         }
         else
         {
            wb_table6_19_832e( false) ;
         }
      }

      protected void wb_table8_46_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptPadroesArtefatos.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPadroesartefatos_descricao2_Internalname, AV22PadroesArtefatos_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV22PadroesArtefatos_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPadroesartefatos_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPadroesartefatos_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMetodologiafases_nome2_Internalname, StringUtil.RTrim( AV23MetodologiaFases_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23MetodologiaFases_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMetodologiafases_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMetodologiafases_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_46_832e( true) ;
         }
         else
         {
            wb_table8_46_832e( false) ;
         }
      }

      protected void wb_table7_28_832( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptPadroesArtefatos.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPadroesartefatos_descricao1_Internalname, AV17PadroesArtefatos_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV17PadroesArtefatos_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPadroesartefatos_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPadroesartefatos_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMetodologiafases_nome1_Internalname, StringUtil.RTrim( AV18MetodologiaFases_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18MetodologiaFases_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMetodologiafases_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavMetodologiafases_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptPadroesArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_832e( true) ;
         }
         else
         {
            wb_table7_28_832e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutPadroesArtefatos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutPadroesArtefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutPadroesArtefatos_Codigo), 6, 0)));
         AV8InOutPadroesArtefatos_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutPadroesArtefatos_Descricao", AV8InOutPadroesArtefatos_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA832( ) ;
         WS832( ) ;
         WE832( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117375047");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptpadroesartefatos.js", "?20203117375048");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_652( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_65_idx;
         edtPadroesArtefatos_Codigo_Internalname = "PADROESARTEFATOS_CODIGO_"+sGXsfl_65_idx;
         edtPadroesArtefatos_Descricao_Internalname = "PADROESARTEFATOS_DESCRICAO_"+sGXsfl_65_idx;
         edtMetodologiaFases_Codigo_Internalname = "METODOLOGIAFASES_CODIGO_"+sGXsfl_65_idx;
         edtMetodologiaFases_Nome_Internalname = "METODOLOGIAFASES_NOME_"+sGXsfl_65_idx;
      }

      protected void SubsflControlProps_fel_652( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_65_fel_idx;
         edtPadroesArtefatos_Codigo_Internalname = "PADROESARTEFATOS_CODIGO_"+sGXsfl_65_fel_idx;
         edtPadroesArtefatos_Descricao_Internalname = "PADROESARTEFATOS_DESCRICAO_"+sGXsfl_65_fel_idx;
         edtMetodologiaFases_Codigo_Internalname = "METODOLOGIAFASES_CODIGO_"+sGXsfl_65_fel_idx;
         edtMetodologiaFases_Nome_Internalname = "METODOLOGIAFASES_NOME_"+sGXsfl_65_fel_idx;
      }

      protected void sendrow_652( )
      {
         SubsflControlProps_652( ) ;
         WB830( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_65_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_65_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_65_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 66,'',false,'',65)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV47Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV47Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_65_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPadroesArtefatos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A150PadroesArtefatos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPadroesArtefatos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPadroesArtefatos_Descricao_Internalname,(String)A151PadroesArtefatos_Descricao,StringUtil.RTrim( context.localUtil.Format( A151PadroesArtefatos_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPadroesArtefatos_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologiaFases_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A147MetodologiaFases_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologiaFases_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologiaFases_Nome_Internalname,StringUtil.RTrim( A148MetodologiaFases_Nome),StringUtil.RTrim( context.localUtil.Format( A148MetodologiaFases_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologiaFases_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_PADROESARTEFATOS_CODIGO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( (decimal)(A150PadroesArtefatos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PADROESARTEFATOS_DESCRICAO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A151PadroesArtefatos_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIAFASES_CODIGO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( (decimal)(A147MetodologiaFases_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         /* End function sendrow_652 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavPadroesartefatos_descricao1_Internalname = "vPADROESARTEFATOS_DESCRICAO1";
         edtavMetodologiafases_nome1_Internalname = "vMETODOLOGIAFASES_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavPadroesartefatos_descricao2_Internalname = "vPADROESARTEFATOS_DESCRICAO2";
         edtavMetodologiafases_nome2_Internalname = "vMETODOLOGIAFASES_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtPadroesArtefatos_Codigo_Internalname = "PADROESARTEFATOS_CODIGO";
         edtPadroesArtefatos_Descricao_Internalname = "PADROESARTEFATOS_DESCRICAO";
         edtMetodologiaFases_Codigo_Internalname = "METODOLOGIAFASES_CODIGO";
         edtMetodologiaFases_Nome_Internalname = "METODOLOGIAFASES_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfpadroesartefatos_descricao_Internalname = "vTFPADROESARTEFATOS_DESCRICAO";
         edtavTfpadroesartefatos_descricao_sel_Internalname = "vTFPADROESARTEFATOS_DESCRICAO_SEL";
         edtavTfmetodologiafases_nome_Internalname = "vTFMETODOLOGIAFASES_NOME";
         edtavTfmetodologiafases_nome_sel_Internalname = "vTFMETODOLOGIAFASES_NOME_SEL";
         Ddo_padroesartefatos_descricao_Internalname = "DDO_PADROESARTEFATOS_DESCRICAO";
         edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Internalname = "vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_metodologiafases_nome_Internalname = "DDO_METODOLOGIAFASES_NOME";
         edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname = "vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtMetodologiaFases_Nome_Jsonclick = "";
         edtMetodologiaFases_Codigo_Jsonclick = "";
         edtPadroesArtefatos_Descricao_Jsonclick = "";
         edtPadroesArtefatos_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavMetodologiafases_nome1_Jsonclick = "";
         edtavPadroesartefatos_descricao1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavMetodologiafases_nome2_Jsonclick = "";
         edtavPadroesartefatos_descricao2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtMetodologiaFases_Nome_Titleformat = 0;
         edtPadroesArtefatos_Descricao_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavMetodologiafases_nome2_Visible = 1;
         edtavPadroesartefatos_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavMetodologiafases_nome1_Visible = 1;
         edtavPadroesartefatos_descricao1_Visible = 1;
         edtMetodologiaFases_Nome_Title = "Fase";
         edtPadroesArtefatos_Descricao_Title = "Descri��o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfmetodologiafases_nome_sel_Jsonclick = "";
         edtavTfmetodologiafases_nome_sel_Visible = 1;
         edtavTfmetodologiafases_nome_Jsonclick = "";
         edtavTfmetodologiafases_nome_Visible = 1;
         edtavTfpadroesartefatos_descricao_sel_Jsonclick = "";
         edtavTfpadroesartefatos_descricao_sel_Visible = 1;
         edtavTfpadroesartefatos_descricao_Jsonclick = "";
         edtavTfpadroesartefatos_descricao_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_metodologiafases_nome_Searchbuttontext = "Pesquisar";
         Ddo_metodologiafases_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_metodologiafases_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_metodologiafases_nome_Loadingdata = "Carregando dados...";
         Ddo_metodologiafases_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_metodologiafases_nome_Sortasc = "Ordenar de A � Z";
         Ddo_metodologiafases_nome_Datalistupdateminimumcharacters = 0;
         Ddo_metodologiafases_nome_Datalistproc = "GetPromptPadroesArtefatosFilterData";
         Ddo_metodologiafases_nome_Datalisttype = "Dynamic";
         Ddo_metodologiafases_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_metodologiafases_nome_Filtertype = "Character";
         Ddo_metodologiafases_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_metodologiafases_nome_Titlecontrolidtoreplace = "";
         Ddo_metodologiafases_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_metodologiafases_nome_Cls = "ColumnSettings";
         Ddo_metodologiafases_nome_Tooltip = "Op��es";
         Ddo_metodologiafases_nome_Caption = "";
         Ddo_padroesartefatos_descricao_Searchbuttontext = "Pesquisar";
         Ddo_padroesartefatos_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_padroesartefatos_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_padroesartefatos_descricao_Loadingdata = "Carregando dados...";
         Ddo_padroesartefatos_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_padroesartefatos_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_padroesartefatos_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_padroesartefatos_descricao_Datalistproc = "GetPromptPadroesArtefatosFilterData";
         Ddo_padroesartefatos_descricao_Datalisttype = "Dynamic";
         Ddo_padroesartefatos_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_padroesartefatos_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_padroesartefatos_descricao_Filtertype = "Character";
         Ddo_padroesartefatos_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_padroesartefatos_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_padroesartefatos_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_padroesartefatos_descricao_Titlecontrolidtoreplace = "";
         Ddo_padroesartefatos_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_padroesartefatos_descricao_Cls = "ColumnSettings";
         Ddo_padroesartefatos_descricao_Tooltip = "Op��es";
         Ddo_padroesartefatos_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Padroes Artefatos";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''}],oparms:[{av:'AV33PadroesArtefatos_DescricaoTitleFilterData',fld:'vPADROESARTEFATOS_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37MetodologiaFases_NomeTitleFilterData',fld:'vMETODOLOGIAFASES_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtPadroesArtefatos_Descricao_Titleformat',ctrl:'PADROESARTEFATOS_DESCRICAO',prop:'Titleformat'},{av:'edtPadroesArtefatos_Descricao_Title',ctrl:'PADROESARTEFATOS_DESCRICAO',prop:'Title'},{av:'edtMetodologiaFases_Nome_Titleformat',ctrl:'METODOLOGIAFASES_NOME',prop:'Titleformat'},{av:'edtMetodologiaFases_Nome_Title',ctrl:'METODOLOGIAFASES_NOME',prop:'Title'},{av:'AV43GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV44GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PADROESARTEFATOS_DESCRICAO.ONOPTIONCLICKED","{handler:'E12832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_padroesartefatos_descricao_Activeeventkey',ctrl:'DDO_PADROESARTEFATOS_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_padroesartefatos_descricao_Filteredtext_get',ctrl:'DDO_PADROESARTEFATOS_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_padroesartefatos_descricao_Selectedvalue_get',ctrl:'DDO_PADROESARTEFATOS_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_padroesartefatos_descricao_Sortedstatus',ctrl:'DDO_PADROESARTEFATOS_DESCRICAO',prop:'SortedStatus'},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_metodologiafases_nome_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_METODOLOGIAFASES_NOME.ONOPTIONCLICKED","{handler:'E13832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_metodologiafases_nome_Activeeventkey',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'ActiveEventKey'},{av:'Ddo_metodologiafases_nome_Filteredtext_get',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'FilteredText_get'},{av:'Ddo_metodologiafases_nome_Selectedvalue_get',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_metodologiafases_nome_Sortedstatus',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SortedStatus'},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_padroesartefatos_descricao_Sortedstatus',ctrl:'DDO_PADROESARTEFATOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E23832',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E24832',iparms:[{av:'A150PadroesArtefatos_Codigo',fld:'PADROESARTEFATOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A151PadroesArtefatos_Descricao',fld:'PADROESARTEFATOS_DESCRICAO',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutPadroesArtefatos_Codigo',fld:'vINOUTPADROESARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutPadroesArtefatos_Descricao',fld:'vINOUTPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'edtavPadroesartefatos_descricao2_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO2',prop:'Visible'},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPadroesartefatos_descricao1_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO1',prop:'Visible'},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19832',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavPadroesartefatos_descricao1_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO1',prop:'Visible'},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'edtavPadroesartefatos_descricao2_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO2',prop:'Visible'},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavPadroesartefatos_descricao1_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO1',prop:'Visible'},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20832',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavPadroesartefatos_descricao2_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO2',prop:'Visible'},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E17832',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace',fld:'vDDO_PADROESARTEFATOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace',fld:'vDDO_METODOLOGIAFASES_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV34TFPadroesArtefatos_Descricao',fld:'vTFPADROESARTEFATOS_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_padroesartefatos_descricao_Filteredtext_set',ctrl:'DDO_PADROESARTEFATOS_DESCRICAO',prop:'FilteredText_set'},{av:'AV35TFPadroesArtefatos_Descricao_Sel',fld:'vTFPADROESARTEFATOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_padroesartefatos_descricao_Selectedvalue_set',ctrl:'DDO_PADROESARTEFATOS_DESCRICAO',prop:'SelectedValue_set'},{av:'AV38TFMetodologiaFases_Nome',fld:'vTFMETODOLOGIAFASES_NOME',pic:'@!',nv:''},{av:'Ddo_metodologiafases_nome_Filteredtext_set',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'FilteredText_set'},{av:'AV39TFMetodologiaFases_Nome_Sel',fld:'vTFMETODOLOGIAFASES_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_metodologiafases_nome_Selectedvalue_set',ctrl:'DDO_METODOLOGIAFASES_NOME',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17PadroesArtefatos_Descricao1',fld:'vPADROESARTEFATOS_DESCRICAO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavPadroesartefatos_descricao1_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO1',prop:'Visible'},{av:'edtavMetodologiafases_nome1_Visible',ctrl:'vMETODOLOGIAFASES_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22PadroesArtefatos_Descricao2',fld:'vPADROESARTEFATOS_DESCRICAO2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV18MetodologiaFases_Nome1',fld:'vMETODOLOGIAFASES_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23MetodologiaFases_Nome2',fld:'vMETODOLOGIAFASES_NOME2',pic:'@!',nv:''},{av:'edtavPadroesartefatos_descricao2_Visible',ctrl:'vPADROESARTEFATOS_DESCRICAO2',prop:'Visible'},{av:'edtavMetodologiafases_nome2_Visible',ctrl:'vMETODOLOGIAFASES_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutPadroesArtefatos_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_padroesartefatos_descricao_Activeeventkey = "";
         Ddo_padroesartefatos_descricao_Filteredtext_get = "";
         Ddo_padroesartefatos_descricao_Selectedvalue_get = "";
         Ddo_metodologiafases_nome_Activeeventkey = "";
         Ddo_metodologiafases_nome_Filteredtext_get = "";
         Ddo_metodologiafases_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17PadroesArtefatos_Descricao1 = "";
         AV18MetodologiaFases_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22PadroesArtefatos_Descricao2 = "";
         AV23MetodologiaFases_Nome2 = "";
         AV34TFPadroesArtefatos_Descricao = "";
         AV35TFPadroesArtefatos_Descricao_Sel = "";
         AV38TFMetodologiaFases_Nome = "";
         AV39TFMetodologiaFases_Nome_Sel = "";
         AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace = "";
         AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace = "";
         AV48Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV41DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33PadroesArtefatos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37MetodologiaFases_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_padroesartefatos_descricao_Filteredtext_set = "";
         Ddo_padroesartefatos_descricao_Selectedvalue_set = "";
         Ddo_padroesartefatos_descricao_Sortedstatus = "";
         Ddo_metodologiafases_nome_Filteredtext_set = "";
         Ddo_metodologiafases_nome_Selectedvalue_set = "";
         Ddo_metodologiafases_nome_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV47Select_GXI = "";
         A151PadroesArtefatos_Descricao = "";
         A148MetodologiaFases_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17PadroesArtefatos_Descricao1 = "";
         lV18MetodologiaFases_Nome1 = "";
         lV22PadroesArtefatos_Descricao2 = "";
         lV23MetodologiaFases_Nome2 = "";
         lV34TFPadroesArtefatos_Descricao = "";
         lV38TFMetodologiaFases_Nome = "";
         H00832_A148MetodologiaFases_Nome = new String[] {""} ;
         H00832_A147MetodologiaFases_Codigo = new int[1] ;
         H00832_A151PadroesArtefatos_Descricao = new String[] {""} ;
         H00832_A150PadroesArtefatos_Codigo = new int[1] ;
         H00833_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptpadroesartefatos__default(),
            new Object[][] {
                new Object[] {
               H00832_A148MetodologiaFases_Nome, H00832_A147MetodologiaFases_Codigo, H00832_A151PadroesArtefatos_Descricao, H00832_A150PadroesArtefatos_Codigo
               }
               , new Object[] {
               H00833_AGRID_nRecordCount
               }
            }
         );
         AV48Pgmname = "PromptPadroesArtefatos";
         /* GeneXus formulas. */
         AV48Pgmname = "PromptPadroesArtefatos";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_65 ;
      private short nGXsfl_65_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_65_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtPadroesArtefatos_Descricao_Titleformat ;
      private short edtMetodologiaFases_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutPadroesArtefatos_Codigo ;
      private int wcpOAV7InOutPadroesArtefatos_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_padroesartefatos_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_metodologiafases_nome_Datalistupdateminimumcharacters ;
      private int edtavTfpadroesartefatos_descricao_Visible ;
      private int edtavTfpadroesartefatos_descricao_sel_Visible ;
      private int edtavTfmetodologiafases_nome_Visible ;
      private int edtavTfmetodologiafases_nome_sel_Visible ;
      private int edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Visible ;
      private int A150PadroesArtefatos_Codigo ;
      private int A147MetodologiaFases_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV42PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavPadroesartefatos_descricao1_Visible ;
      private int edtavMetodologiafases_nome1_Visible ;
      private int edtavPadroesartefatos_descricao2_Visible ;
      private int edtavMetodologiafases_nome2_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV43GridCurrentPage ;
      private long AV44GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_padroesartefatos_descricao_Activeeventkey ;
      private String Ddo_padroesartefatos_descricao_Filteredtext_get ;
      private String Ddo_padroesartefatos_descricao_Selectedvalue_get ;
      private String Ddo_metodologiafases_nome_Activeeventkey ;
      private String Ddo_metodologiafases_nome_Filteredtext_get ;
      private String Ddo_metodologiafases_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_65_idx="0001" ;
      private String AV18MetodologiaFases_Nome1 ;
      private String AV23MetodologiaFases_Nome2 ;
      private String AV38TFMetodologiaFases_Nome ;
      private String AV39TFMetodologiaFases_Nome_Sel ;
      private String AV48Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_padroesartefatos_descricao_Caption ;
      private String Ddo_padroesartefatos_descricao_Tooltip ;
      private String Ddo_padroesartefatos_descricao_Cls ;
      private String Ddo_padroesartefatos_descricao_Filteredtext_set ;
      private String Ddo_padroesartefatos_descricao_Selectedvalue_set ;
      private String Ddo_padroesartefatos_descricao_Dropdownoptionstype ;
      private String Ddo_padroesartefatos_descricao_Titlecontrolidtoreplace ;
      private String Ddo_padroesartefatos_descricao_Sortedstatus ;
      private String Ddo_padroesartefatos_descricao_Filtertype ;
      private String Ddo_padroesartefatos_descricao_Datalisttype ;
      private String Ddo_padroesartefatos_descricao_Datalistproc ;
      private String Ddo_padroesartefatos_descricao_Sortasc ;
      private String Ddo_padroesartefatos_descricao_Sortdsc ;
      private String Ddo_padroesartefatos_descricao_Loadingdata ;
      private String Ddo_padroesartefatos_descricao_Cleanfilter ;
      private String Ddo_padroesartefatos_descricao_Noresultsfound ;
      private String Ddo_padroesartefatos_descricao_Searchbuttontext ;
      private String Ddo_metodologiafases_nome_Caption ;
      private String Ddo_metodologiafases_nome_Tooltip ;
      private String Ddo_metodologiafases_nome_Cls ;
      private String Ddo_metodologiafases_nome_Filteredtext_set ;
      private String Ddo_metodologiafases_nome_Selectedvalue_set ;
      private String Ddo_metodologiafases_nome_Dropdownoptionstype ;
      private String Ddo_metodologiafases_nome_Titlecontrolidtoreplace ;
      private String Ddo_metodologiafases_nome_Sortedstatus ;
      private String Ddo_metodologiafases_nome_Filtertype ;
      private String Ddo_metodologiafases_nome_Datalisttype ;
      private String Ddo_metodologiafases_nome_Datalistproc ;
      private String Ddo_metodologiafases_nome_Sortasc ;
      private String Ddo_metodologiafases_nome_Sortdsc ;
      private String Ddo_metodologiafases_nome_Loadingdata ;
      private String Ddo_metodologiafases_nome_Cleanfilter ;
      private String Ddo_metodologiafases_nome_Noresultsfound ;
      private String Ddo_metodologiafases_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfpadroesartefatos_descricao_Internalname ;
      private String edtavTfpadroesartefatos_descricao_Jsonclick ;
      private String edtavTfpadroesartefatos_descricao_sel_Internalname ;
      private String edtavTfpadroesartefatos_descricao_sel_Jsonclick ;
      private String edtavTfmetodologiafases_nome_Internalname ;
      private String edtavTfmetodologiafases_nome_Jsonclick ;
      private String edtavTfmetodologiafases_nome_sel_Internalname ;
      private String edtavTfmetodologiafases_nome_sel_Jsonclick ;
      private String edtavDdo_padroesartefatos_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_metodologiafases_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtPadroesArtefatos_Codigo_Internalname ;
      private String edtPadroesArtefatos_Descricao_Internalname ;
      private String edtMetodologiaFases_Codigo_Internalname ;
      private String A148MetodologiaFases_Nome ;
      private String edtMetodologiaFases_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV18MetodologiaFases_Nome1 ;
      private String lV23MetodologiaFases_Nome2 ;
      private String lV38TFMetodologiaFases_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavPadroesartefatos_descricao1_Internalname ;
      private String edtavMetodologiafases_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavPadroesartefatos_descricao2_Internalname ;
      private String edtavMetodologiafases_nome2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_padroesartefatos_descricao_Internalname ;
      private String Ddo_metodologiafases_nome_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtPadroesArtefatos_Descricao_Title ;
      private String edtMetodologiaFases_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavPadroesartefatos_descricao2_Jsonclick ;
      private String edtavMetodologiafases_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavPadroesartefatos_descricao1_Jsonclick ;
      private String edtavMetodologiafases_nome1_Jsonclick ;
      private String sGXsfl_65_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtPadroesArtefatos_Codigo_Jsonclick ;
      private String edtPadroesArtefatos_Descricao_Jsonclick ;
      private String edtMetodologiaFases_Codigo_Jsonclick ;
      private String edtMetodologiaFases_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_padroesartefatos_descricao_Includesortasc ;
      private bool Ddo_padroesartefatos_descricao_Includesortdsc ;
      private bool Ddo_padroesartefatos_descricao_Includefilter ;
      private bool Ddo_padroesartefatos_descricao_Filterisrange ;
      private bool Ddo_padroesartefatos_descricao_Includedatalist ;
      private bool Ddo_metodologiafases_nome_Includesortasc ;
      private bool Ddo_metodologiafases_nome_Includesortdsc ;
      private bool Ddo_metodologiafases_nome_Includefilter ;
      private bool Ddo_metodologiafases_nome_Filterisrange ;
      private bool Ddo_metodologiafases_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV8InOutPadroesArtefatos_Descricao ;
      private String wcpOAV8InOutPadroesArtefatos_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17PadroesArtefatos_Descricao1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV22PadroesArtefatos_Descricao2 ;
      private String AV34TFPadroesArtefatos_Descricao ;
      private String AV35TFPadroesArtefatos_Descricao_Sel ;
      private String AV36ddo_PadroesArtefatos_DescricaoTitleControlIdToReplace ;
      private String AV40ddo_MetodologiaFases_NomeTitleControlIdToReplace ;
      private String AV47Select_GXI ;
      private String A151PadroesArtefatos_Descricao ;
      private String lV17PadroesArtefatos_Descricao1 ;
      private String lV22PadroesArtefatos_Descricao2 ;
      private String lV34TFPadroesArtefatos_Descricao ;
      private String AV31Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutPadroesArtefatos_Codigo ;
      private String aP1_InOutPadroesArtefatos_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private String[] H00832_A148MetodologiaFases_Nome ;
      private int[] H00832_A147MetodologiaFases_Codigo ;
      private String[] H00832_A151PadroesArtefatos_Descricao ;
      private int[] H00832_A150PadroesArtefatos_Codigo ;
      private long[] H00833_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33PadroesArtefatos_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37MetodologiaFases_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV41DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptpadroesartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00832( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17PadroesArtefatos_Descricao1 ,
                                             String AV18MetodologiaFases_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22PadroesArtefatos_Descricao2 ,
                                             String AV23MetodologiaFases_Nome2 ,
                                             String AV35TFPadroesArtefatos_Descricao_Sel ,
                                             String AV34TFPadroesArtefatos_Descricao ,
                                             String AV39TFMetodologiaFases_Nome_Sel ,
                                             String AV38TFMetodologiaFases_Nome ,
                                             String A151PadroesArtefatos_Descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[MetodologiaFases_Nome], T1.[MetodologiaFases_Codigo], T1.[PadroesArtefatos_Descricao], T1.[PadroesArtefatos_Codigo]";
         sFromString = " FROM ([PadroesArtefatos] T1 WITH (NOLOCK) INNER JOIN [MetodologiaFases] T2 WITH (NOLOCK) ON T2.[MetodologiaFases_Codigo] = T1.[MetodologiaFases_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17PadroesArtefatos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV17PadroesArtefatos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV17PadroesArtefatos_Descricao1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17PadroesArtefatos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV17PadroesArtefatos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV17PadroesArtefatos_Descricao1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV18MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV18MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV18MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV18MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22PadroesArtefatos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV22PadroesArtefatos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV22PadroesArtefatos_Descricao2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22PadroesArtefatos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV22PadroesArtefatos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV22PadroesArtefatos_Descricao2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV23MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV23MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV23MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV23MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV35TFPadroesArtefatos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFPadroesArtefatos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV34TFPadroesArtefatos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV34TFPadroesArtefatos_Descricao)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFPadroesArtefatos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] = @AV35TFPadroesArtefatos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] = @AV35TFPadroesArtefatos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39TFMetodologiaFases_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFMetodologiaFases_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV38TFMetodologiaFases_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV38TFMetodologiaFases_Nome)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFMetodologiaFases_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] = @AV39TFMetodologiaFases_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] = @AV39TFMetodologiaFases_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[PadroesArtefatos_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[PadroesArtefatos_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[MetodologiaFases_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[MetodologiaFases_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[PadroesArtefatos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00833( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17PadroesArtefatos_Descricao1 ,
                                             String AV18MetodologiaFases_Nome1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22PadroesArtefatos_Descricao2 ,
                                             String AV23MetodologiaFases_Nome2 ,
                                             String AV35TFPadroesArtefatos_Descricao_Sel ,
                                             String AV34TFPadroesArtefatos_Descricao ,
                                             String AV39TFMetodologiaFases_Nome_Sel ,
                                             String AV38TFMetodologiaFases_Nome ,
                                             String A151PadroesArtefatos_Descricao ,
                                             String A148MetodologiaFases_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([PadroesArtefatos] T1 WITH (NOLOCK) INNER JOIN [MetodologiaFases] T2 WITH (NOLOCK) ON T2.[MetodologiaFases_Codigo] = T1.[MetodologiaFases_Codigo])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17PadroesArtefatos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV17PadroesArtefatos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV17PadroesArtefatos_Descricao1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17PadroesArtefatos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV17PadroesArtefatos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV17PadroesArtefatos_Descricao1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV18MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV18MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "METODOLOGIAFASES_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18MetodologiaFases_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV18MetodologiaFases_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV18MetodologiaFases_Nome1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22PadroesArtefatos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV22PadroesArtefatos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV22PadroesArtefatos_Descricao2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "PADROESARTEFATOS_DESCRICAO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22PadroesArtefatos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like '%' + @lV22PadroesArtefatos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like '%' + @lV22PadroesArtefatos_Descricao2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV23MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV23MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "METODOLOGIAFASES_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23MetodologiaFases_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like '%' + @lV23MetodologiaFases_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like '%' + @lV23MetodologiaFases_Nome2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV35TFPadroesArtefatos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFPadroesArtefatos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] like @lV34TFPadroesArtefatos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] like @lV34TFPadroesArtefatos_Descricao)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFPadroesArtefatos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[PadroesArtefatos_Descricao] = @AV35TFPadroesArtefatos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[PadroesArtefatos_Descricao] = @AV35TFPadroesArtefatos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39TFMetodologiaFases_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFMetodologiaFases_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] like @lV38TFMetodologiaFases_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] like @lV38TFMetodologiaFases_Nome)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFMetodologiaFases_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[MetodologiaFases_Nome] = @AV39TFMetodologiaFases_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[MetodologiaFases_Nome] = @AV39TFMetodologiaFases_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00832(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] );
               case 1 :
                     return conditional_H00833(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (bool)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00832 ;
          prmH00832 = new Object[] {
          new Object[] {"@lV17PadroesArtefatos_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV17PadroesArtefatos_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV18MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22PadroesArtefatos_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV22PadroesArtefatos_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV23MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34TFPadroesArtefatos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV35TFPadroesArtefatos_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV38TFMetodologiaFases_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV39TFMetodologiaFases_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00833 ;
          prmH00833 = new Object[] {
          new Object[] {"@lV17PadroesArtefatos_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV17PadroesArtefatos_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV18MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18MetodologiaFases_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22PadroesArtefatos_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV22PadroesArtefatos_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV23MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23MetodologiaFases_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34TFPadroesArtefatos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV35TFPadroesArtefatos_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV38TFMetodologiaFases_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV39TFMetodologiaFases_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00832", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00832,11,0,true,false )
             ,new CursorDef("H00833", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00833,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
