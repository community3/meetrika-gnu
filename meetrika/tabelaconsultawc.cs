/*
               File: TabelaConsultaWC
        Description: Detalhes da Tabela
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:17.66
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tabelaconsultawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public tabelaconsultawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public tabelaconsultawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Tabela_Codigo )
      {
         this.A172Tabela_Codigo = aP0_Tabela_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbFuncaoDados_Tipo = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
         cmbavFuncaoapf_tipo = new GXCombobox();
         cmbavFuncaoapf_complexidade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A172Tabela_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridfapf") == 0 )
               {
                  nRC_GXsfl_62 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_62_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_62_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridfapf_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridfapf") == 0 )
               {
                  subGridfapf_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV8FuncaoAPF_Codigo);
                  A183FuncaoAPF_Ativo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
                  A166FuncaoAPF_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
                  A184FuncaoAPF_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
                  A185FuncaoAPF_Complexidade = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
                  A388FuncaoAPF_TD = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
                  A387FuncaoAPF_AR = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
                  A386FuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
                  AV6FD_PF = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFd_pf_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FD_PF), 8, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridfapf_refresh( subGridfapf_Rows, A165FuncaoAPF_Codigo, AV8FuncaoAPF_Codigo, A183FuncaoAPF_Ativo, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A185FuncaoAPF_Complexidade, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A386FuncaoAPF_PF, AV6FD_PF, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridfd") == 0 )
               {
                  nRC_GXsfl_27 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_27_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_27_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  edtTabela_Codigo_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Visible), 5, 0)));
                  AV16GridFAPFCurrentPage = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGridfapfcurrentpage_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridFAPFCurrentPage), 10, 0)));
                  edtavGridfapfcurrentpage_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGridfapfcurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridfapfcurrentpage_Visible), 5, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridfd_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridfd") == 0 )
               {
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  edtTabela_Codigo_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Visible), 5, 0)));
                  AV16GridFAPFCurrentPage = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGridfapfcurrentpage_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridFAPFCurrentPage), 10, 0)));
                  edtavGridfapfcurrentpage_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGridfapfcurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridfapfcurrentpage_Visible), 5, 0)));
                  A377FuncaoDados_PF = NumberUtil.Val( GetNextPar( ), ".");
                  A183FuncaoAPF_Ativo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV8FuncaoAPF_Codigo);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridfd_refresh( A172Tabela_Codigo, AV16GridFAPFCurrentPage, A377FuncaoDados_PF, A183FuncaoAPF_Ativo, A165FuncaoAPF_Codigo, AV8FuncaoAPF_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAA72( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavTabela_pf_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_pf_Enabled), 5, 0)));
               edtavFuncaoapf_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome_Enabled), 5, 0)));
               cmbavFuncaoapf_tipo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo.Enabled), 5, 0)));
               cmbavFuncaoapf_complexidade.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade.Enabled), 5, 0)));
               edtavFuncaoapf_td_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_td_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_td_Enabled), 5, 0)));
               edtavFuncaoapf_ar_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_ar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_ar_Enabled), 5, 0)));
               edtavFuncaoapf_pf_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf_Enabled), 5, 0)));
               edtavFd_pf_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFd_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFd_pf_Enabled), 5, 0)));
               WSA72( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Detalhes da Tabela") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311771779");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tabelaconsultawc.aspx") + "?" + UrlEncode("" +A172Tabela_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_27", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_27), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA172Tabela_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA172Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_ATIVO", StringUtil.RTrim( A183FuncaoAPF_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPF_CODIGO", AV8FuncaoAPF_Codigo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPF_CODIGO", AV8FuncaoAPF_Codigo);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_TIPO", StringUtil.RTrim( A184FuncaoAPF_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_TD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_AR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vTABELA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Tabela_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vTABELA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Tabela_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Width", StringUtil.RTrim( Dvpanel_panel_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Cls", StringUtil.RTrim( Dvpanel_panel_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Title", StringUtil.RTrim( Dvpanel_panel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Collapsible", StringUtil.BoolToStr( Dvpanel_panel_Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Collapsed", StringUtil.BoolToStr( Dvpanel_panel_Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Autowidth", StringUtil.BoolToStr( Dvpanel_panel_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Autoheight", StringUtil.BoolToStr( Dvpanel_panel_Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_panel_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Iconposition", StringUtil.RTrim( Dvpanel_panel_Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_PANEL_Autoscroll", StringUtil.BoolToStr( Dvpanel_panel_Autoscroll));
         GxWebStd.gx_hidden_field( context, sPrefix+"TABELA_CODIGO_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Codigo_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDFAPFCURRENTPAGE_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGridfapfcurrentpage_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDFAPFPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridfapfpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormA72( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("tabelaconsultawc.js", "?2020311771797");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "TabelaConsultaWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Detalhes da Tabela" ;
      }

      protected void WBA70( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "tabelaconsultawc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
            }
            wb_table1_2_A72( true) ;
         }
         else
         {
            wb_table1_2_A72( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_A72e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTA72( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Detalhes da Tabela", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPA70( ) ;
            }
         }
      }

      protected void WSA72( )
      {
         STARTA72( ) ;
         EVTA72( ) ;
      }

      protected void EVTA72( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDFAPFPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11A72 */
                                    E11A72 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA70( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavTabela_pf_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "GRIDFD.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 32), "GRIDFAPFPAGINATIONBAR.CHANGEPAGE") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "GRIDFD.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "GRIDFAPF.LOAD") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPA70( ) ;
                              }
                              nGXsfl_27_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
                              SubsflControlProps_272( ) ;
                              GXCCtl = "GRIDFAPF_Rows_" + sGXsfl_27_idx;
                              subGridfapf_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+GXCCtl), ",", "."));
                              GXCCtl = "GRIDFAPF_Rows_" + sGXsfl_27_idx;
                              GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Rows), 6, 0, ".", "")));
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Class_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Class = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "Class", Gridfapfpaginationbar_Class);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_First_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_First = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "First", Gridfapfpaginationbar_First);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Previous_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Previous = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "Previous", Gridfapfpaginationbar_Previous);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Next_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Next = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "Next", Gridfapfpaginationbar_Next);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Last_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Last = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "Last", Gridfapfpaginationbar_Last);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Caption_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Caption = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "Caption", Gridfapfpaginationbar_Caption);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Showfirst_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "ShowFirst", StringUtil.BoolToStr( Gridfapfpaginationbar_Showfirst));
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Showprevious_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "ShowPrevious", StringUtil.BoolToStr( Gridfapfpaginationbar_Showprevious));
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Shownext_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "ShowNext", StringUtil.BoolToStr( Gridfapfpaginationbar_Shownext));
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Showlast_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+GXCCtl));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "ShowLast", StringUtil.BoolToStr( Gridfapfpaginationbar_Showlast));
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Pagestoshow_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+GXCCtl), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "PagesToShow", StringUtil.LTrim( StringUtil.Str( (decimal)(Gridfapfpaginationbar_Pagestoshow), 9, 0)));
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Pagingbuttonsposition_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "PagingButtonsPosition", Gridfapfpaginationbar_Pagingbuttonsposition);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Pagingcaptionposition_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "PagingCaptionPosition", Gridfapfpaginationbar_Pagingcaptionposition);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Emptygridclass_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Emptygridclass = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "EmptyGridClass", Gridfapfpaginationbar_Emptygridclass);
                              GXCCtl = "GRIDFAPFPAGINATIONBAR_Emptygridcaption_" + sGXsfl_27_idx;
                              Gridfapfpaginationbar_Emptygridcaption = cgiGet( sPrefix+GXCCtl);
                              context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Gridfapfpaginationbar_Internalname, "EmptyGridCaption", Gridfapfpaginationbar_Emptygridcaption);
                              A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
                              if ( StringUtil.Len( sPrefix) == 0 )
                              {
                                 A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              }
                              cmbFuncaoDados_Tipo.Name = cmbFuncaoDados_Tipo_Internalname;
                              cmbFuncaoDados_Tipo.CurrentValue = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                              A373FuncaoDados_Tipo = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                              cmbFuncaoDados_Complexidade.Name = cmbFuncaoDados_Complexidade_Internalname;
                              cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
                              A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
                              A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
                              AV6FD_PF = (int)(context.localUtil.CToN( cgiGet( edtavFd_pf_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFd_pf_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FD_PF), 8, 0)));
                              GXCCtl = "vGRIDFAPFPAGECOUNT_" + sGXsfl_27_idx;
                              AV17GridFAPFPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+GXCCtl), ",", "."));
                              GXCCtl = "GRIDFAPF_nFirstRecordOnPage_" + sGXsfl_27_idx;
                              GRIDFAPF_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+GXCCtl), ",", "."));
                              GXCCtl = "GRIDFAPF_nEOF_" + sGXsfl_27_idx;
                              GRIDFAPF_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+GXCCtl), ",", "."));
                              GXCCtl = "nRC_GXsfl_62_" + sGXsfl_27_idx;
                              nRC_GXsfl_62 = (short)(context.localUtil.CToN( cgiGet( sPrefix+GXCCtl), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_pf_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12A72 */
                                          E12A72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_pf_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13A72 */
                                          E13A72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDFD.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_pf_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14A72 */
                                          E14A72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDFAPFPAGINATIONBAR.CHANGEPAGE") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_pf_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E11A72 */
                                          E11A72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDFD.REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_pf_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15A72 */
                                          E15A72 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_pf_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPA70( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavTabela_pf_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                                 sEvtType = StringUtil.Right( sEvt, 4);
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                                 if ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "GRIDFAPF.LOAD") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPA70( ) ;
                                    }
                                    nGXsfl_62_idx = (short)(NumberUtil.Val( sEvtType, "."));
                                    sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0") + sGXsfl_27_idx;
                                    SubsflControlProps_624( ) ;
                                    AV10FuncaoAPF_Nome = cgiGet( edtavFuncaoapf_nome_Internalname);
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_nome_Internalname, AV10FuncaoAPF_Nome);
                                    cmbavFuncaoapf_tipo.Name = cmbavFuncaoapf_tipo_Internalname;
                                    cmbavFuncaoapf_tipo.CurrentValue = cgiGet( cmbavFuncaoapf_tipo_Internalname);
                                    AV13FuncaoAPF_Tipo = cgiGet( cmbavFuncaoapf_tipo_Internalname);
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, AV13FuncaoAPF_Tipo);
                                    cmbavFuncaoapf_complexidade.Name = cmbavFuncaoapf_complexidade_Internalname;
                                    cmbavFuncaoapf_complexidade.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
                                    AV9FuncaoAPF_Complexidade = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, AV9FuncaoAPF_Complexidade);
                                    AV12FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", "."));
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_TD), 4, 0)));
                                    AV7FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", "."));
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_AR), 4, 0)));
                                    AV11FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".");
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.Str( AV11FuncaoAPF_PF, 14, 5)));
                                    sEvtType = StringUtil.Right( sEvt, 1);
                                    if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                                    {
                                       sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                       if ( StringUtil.StrCmp(sEvt, "GRIDFAPF.LOAD") == 0 )
                                       {
                                          if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                          {
                                             context.wbHandled = 1;
                                             if ( ! wbErr )
                                             {
                                                dynload_actions( ) ;
                                                GX_FocusControl = edtavTabela_pf_Internalname;
                                                context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                                /* Execute user event: E16A74 */
                                                E16A74 ();
                                             }
                                          }
                                       }
                                       else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                       {
                                          if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                          {
                                             STRUPA70( ) ;
                                          }
                                          if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                          {
                                             context.wbHandled = 1;
                                             if ( ! wbErr )
                                             {
                                                dynload_actions( ) ;
                                                GX_FocusControl = edtavTabela_pf_Internalname;
                                                context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                             }
                                          }
                                       }
                                    }
                                    else
                                    {
                                    }
                                 }
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEA72( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormA72( ) ;
            }
         }
      }

      protected void PAA72( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_27_idx;
            cmbFuncaoDados_Tipo.Name = GXCCtl;
            cmbFuncaoDados_Tipo.WebTags = "";
            cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
            cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
            cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
            if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
            {
               A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_27_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            GXCCtl = "vFUNCAOAPF_TIPO_" + sGXsfl_62_idx;
            cmbavFuncaoapf_tipo.Name = GXCCtl;
            cmbavFuncaoapf_tipo.WebTags = "";
            cmbavFuncaoapf_tipo.addItem("", "(Nenhum)", 0);
            cmbavFuncaoapf_tipo.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo.ItemCount > 0 )
            {
               AV13FuncaoAPF_Tipo = cmbavFuncaoapf_tipo.getValidValue(AV13FuncaoAPF_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, AV13FuncaoAPF_Tipo);
            }
            GXCCtl = "vFUNCAOAPF_COMPLEXIDADE_" + sGXsfl_62_idx;
            cmbavFuncaoapf_complexidade.Name = GXCCtl;
            cmbavFuncaoapf_complexidade.WebTags = "";
            cmbavFuncaoapf_complexidade.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade.ItemCount > 0 )
            {
               AV9FuncaoAPF_Complexidade = cmbavFuncaoapf_complexidade.getValidValue(AV9FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, AV9FuncaoAPF_Complexidade);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavTabela_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridfd_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_272( ) ;
         while ( nGXsfl_27_idx <= nRC_GXsfl_27 )
         {
            sendrow_272( ) ;
            nGXsfl_27_idx = (short)(((subGridfd_Islastpage==1)&&(nGXsfl_27_idx+1>subGridfd_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         context.GX_webresponse.AddString(GridfdContainer.ToJavascriptSource());
         /* End function gxnrGridfd_newrow */
      }

      protected void gxnrGridfapf_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_624( ) ;
         while ( nGXsfl_62_idx <= nRC_GXsfl_62 )
         {
            sendrow_624( ) ;
            nGXsfl_62_idx = (short)(((subGridfapf_Islastpage==1)&&(nGXsfl_62_idx+1>subGridfapf_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0") + sGXsfl_27_idx;
            SubsflControlProps_624( ) ;
         }
         context.GX_webresponse.AddString(GridfapfContainer.ToJavascriptSource());
         /* End function gxnrGridfapf_newrow */
      }

      protected void gxgrGridfapf_refresh( int subGridfapf_Rows ,
                                           int A165FuncaoAPF_Codigo ,
                                           IGxCollection AV8FuncaoAPF_Codigo ,
                                           String A183FuncaoAPF_Ativo ,
                                           String A166FuncaoAPF_Nome ,
                                           String A184FuncaoAPF_Tipo ,
                                           String A185FuncaoAPF_Complexidade ,
                                           short A388FuncaoAPF_TD ,
                                           short A387FuncaoAPF_AR ,
                                           decimal A386FuncaoAPF_PF ,
                                           int AV6FD_PF ,
                                           String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GXCCtl = "GRIDFAPF_Rows_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Rows), 6, 0, ".", "")));
         /* Execute user event: E13A72 */
         E13A72 ();
         GRIDFAPF_nCurrentRecord = 0;
         RFA74( ) ;
         /* End function gxgrGridfapf_refresh */
      }

      protected void gxgrGridfd_refresh( int A172Tabela_Codigo ,
                                         long AV16GridFAPFCurrentPage ,
                                         decimal A377FuncaoDados_PF ,
                                         String A183FuncaoAPF_Ativo ,
                                         int A165FuncaoAPF_Codigo ,
                                         IGxCollection AV8FuncaoAPF_Codigo ,
                                         String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDFD_nCurrentRecord = 0;
         RFA72( ) ;
         /* End function gxgrGridfd_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_COMPLEXIDADE", StringUtil.RTrim( A376FuncaoDados_Complexidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         /* Execute user event: E13A72 */
         E13A72 ();
         RFA72( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavTabela_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_pf_Enabled), 5, 0)));
         edtavFuncaoapf_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome_Enabled), 5, 0)));
         cmbavFuncaoapf_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo.Enabled), 5, 0)));
         cmbavFuncaoapf_complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade.Enabled), 5, 0)));
         edtavFuncaoapf_td_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_td_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_td_Enabled), 5, 0)));
         edtavFuncaoapf_ar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_ar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_ar_Enabled), 5, 0)));
         edtavFuncaoapf_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf_Enabled), 5, 0)));
         edtavFd_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFd_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFd_pf_Enabled), 5, 0)));
      }

      protected void RFA72( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridfdContainer.ClearRows();
         }
         wbStart = 27;
         /* Execute user event: E15A72 */
         E15A72 ();
         nGXsfl_27_idx = 1;
         sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
         SubsflControlProps_272( ) ;
         nGXsfl_27_Refreshing = 1;
         GridfdContainer.AddObjectProperty("GridName", "Gridfd");
         GridfdContainer.AddObjectProperty("CmpContext", sPrefix);
         GridfdContainer.AddObjectProperty("InMasterPage", "false");
         GridfdContainer.AddObjectProperty("Class", StringUtil.RTrim( ""));
         GridfdContainer.AddObjectProperty("Class", "");
         GridfdContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridfdContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridfdContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Backcolorstyle), 1, 0, ".", "")));
         GridfdContainer.PageSize = subGridfd_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_272( ) ;
            /* Using cursor H00A72 */
            pr_default.execute(0, new Object[] {A172Tabela_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A394FuncaoDados_Ativo = H00A72_A394FuncaoDados_Ativo[0];
               A373FuncaoDados_Tipo = H00A72_A373FuncaoDados_Tipo[0];
               A369FuncaoDados_Nome = H00A72_A369FuncaoDados_Nome[0];
               A755FuncaoDados_Contar = H00A72_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H00A72_n755FuncaoDados_Contar[0];
               A368FuncaoDados_Codigo = H00A72_A368FuncaoDados_Codigo[0];
               A394FuncaoDados_Ativo = H00A72_A394FuncaoDados_Ativo[0];
               A373FuncaoDados_Tipo = H00A72_A373FuncaoDados_Tipo[0];
               A369FuncaoDados_Nome = H00A72_A369FuncaoDados_Nome[0];
               A755FuncaoDados_Contar = H00A72_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H00A72_n755FuncaoDados_Contar[0];
               GXt_char1 = A376FuncaoDados_Complexidade;
               new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A376FuncaoDados_Complexidade = GXt_char1;
               GXt_int2 = A374FuncaoDados_DER;
               new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A374FuncaoDados_DER = GXt_int2;
               GXt_int2 = A375FuncaoDados_RLR;
               new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
               A375FuncaoDados_RLR = GXt_int2;
               if ( A755FuncaoDados_Contar )
               {
                  GXt_int2 = (short)(A377FuncaoDados_PF);
                  new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A368FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A368FuncaoDados_Codigo), 6, 0)));
                  A377FuncaoDados_PF = (decimal)(GXt_int2);
               }
               else
               {
                  A377FuncaoDados_PF = 0;
               }
               /* Execute user event: E14A72 */
               E14A72 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            wbEnd = 27;
            WBA70( ) ;
         }
         nGXsfl_27_Refreshing = 0;
      }

      protected void RFA74( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridfapfContainer.ClearRows();
         }
         wbStart = 62;
         nGXsfl_62_idx = 1;
         sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0") + sGXsfl_27_idx;
         SubsflControlProps_624( ) ;
         nGXsfl_62_Refreshing = 1;
         GridfapfContainer.AddObjectProperty("GridName", "Gridfapf");
         GridfapfContainer.AddObjectProperty("CmpContext", sPrefix);
         GridfapfContainer.AddObjectProperty("InMasterPage", "false");
         GridfapfContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridfapfContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridfapfContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridfapfContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Backcolorstyle), 1, 0, ".", "")));
         GridfapfContainer.PageSize = subGridfapf_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_624( ) ;
            /* Execute user event: E16A74 */
            E16A74 ();
            wbEnd = 62;
            WBA70( ) ;
         }
         nGXsfl_62_Refreshing = 0;
      }

      protected int subGridfd_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridfd_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridfd_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridfd_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridfapf_Pagecount( )
      {
         GRIDFAPF_nRecordCount = subGridfapf_Recordcount( );
         if ( ((int)((GRIDFAPF_nRecordCount) % (subGridfapf_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDFAPF_nRecordCount/ (decimal)(subGridfapf_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDFAPF_nRecordCount/ (decimal)(subGridfapf_Recordsperpage( ))))+1) ;
      }

      protected int subGridfapf_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridfapf_Recordsperpage( )
      {
         if ( subGridfapf_Rows > 0 )
         {
            return subGridfapf_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridfapf_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridfapf_firstpage( )
      {
         GRIDFAPF_nFirstRecordOnPage = 0;
         GXCCtl = "GRIDFAPF_nFirstRecordOnPage_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridfapf_refresh( subGridfapf_Rows, A165FuncaoAPF_Codigo, AV8FuncaoAPF_Codigo, A183FuncaoAPF_Ativo, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A185FuncaoAPF_Complexidade, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A386FuncaoAPF_PF, AV6FD_PF, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridfapf_nextpage( )
      {
         if ( GRIDFAPF_nEOF == 0 )
         {
            GRIDFAPF_nFirstRecordOnPage = (long)(GRIDFAPF_nFirstRecordOnPage+subGridfapf_Recordsperpage( ));
         }
         GXCCtl = "GRIDFAPF_nFirstRecordOnPage_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridfapf_refresh( subGridfapf_Rows, A165FuncaoAPF_Codigo, AV8FuncaoAPF_Codigo, A183FuncaoAPF_Ativo, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A185FuncaoAPF_Complexidade, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A386FuncaoAPF_PF, AV6FD_PF, sPrefix) ;
         }
         return (short)(((GRIDFAPF_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridfapf_previouspage( )
      {
         if ( GRIDFAPF_nFirstRecordOnPage >= subGridfapf_Recordsperpage( ) )
         {
            GRIDFAPF_nFirstRecordOnPage = (long)(GRIDFAPF_nFirstRecordOnPage-subGridfapf_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GXCCtl = "GRIDFAPF_nFirstRecordOnPage_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridfapf_refresh( subGridfapf_Rows, A165FuncaoAPF_Codigo, AV8FuncaoAPF_Codigo, A183FuncaoAPF_Ativo, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A185FuncaoAPF_Complexidade, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A386FuncaoAPF_PF, AV6FD_PF, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridfapf_lastpage( )
      {
         subGridfapf_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGridfapf_refresh( subGridfapf_Rows, A165FuncaoAPF_Codigo, AV8FuncaoAPF_Codigo, A183FuncaoAPF_Ativo, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A185FuncaoAPF_Complexidade, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A386FuncaoAPF_PF, AV6FD_PF, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridfapf_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDFAPF_nFirstRecordOnPage = (long)(subGridfapf_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDFAPF_nFirstRecordOnPage = 0;
         }
         GXCCtl = "GRIDFAPF_nFirstRecordOnPage_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridfapf_refresh( subGridfapf_Rows, A165FuncaoAPF_Codigo, AV8FuncaoAPF_Codigo, A183FuncaoAPF_Ativo, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A185FuncaoAPF_Complexidade, A388FuncaoAPF_TD, A387FuncaoAPF_AR, A386FuncaoAPF_PF, AV6FD_PF, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPA70( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavTabela_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTabela_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTabela_pf_Enabled), 5, 0)));
         edtavFuncaoapf_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome_Enabled), 5, 0)));
         cmbavFuncaoapf_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo.Enabled), 5, 0)));
         cmbavFuncaoapf_complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade.Enabled), 5, 0)));
         edtavFuncaoapf_td_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_td_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_td_Enabled), 5, 0)));
         edtavFuncaoapf_ar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_ar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_ar_Enabled), 5, 0)));
         edtavFuncaoapf_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf_Enabled), 5, 0)));
         edtavFd_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFd_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFd_pf_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12A72 */
         E12A72 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTabela_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTabela_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTABELA_PF");
               GX_FocusControl = edtavTabela_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14Tabela_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14Tabela_PF", StringUtil.LTrim( StringUtil.Str( AV14Tabela_PF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vTABELA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Tabela_PF, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV14Tabela_PF = context.localUtil.CToN( cgiGet( edtavTabela_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14Tabela_PF", StringUtil.LTrim( StringUtil.Str( AV14Tabela_PF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vTABELA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Tabela_PF, "ZZ,ZZZ,ZZ9.999")));
            }
            /* Read saved values. */
            nRC_GXsfl_27 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_27"), ",", "."));
            wcpOA172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA172Tabela_Codigo"), ",", "."));
            Dvpanel_panel_Width = cgiGet( sPrefix+"DVPANEL_PANEL_Width");
            Dvpanel_panel_Cls = cgiGet( sPrefix+"DVPANEL_PANEL_Cls");
            Dvpanel_panel_Title = cgiGet( sPrefix+"DVPANEL_PANEL_Title");
            Dvpanel_panel_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_PANEL_Collapsible"));
            Dvpanel_panel_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_PANEL_Collapsed"));
            Dvpanel_panel_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_PANEL_Autowidth"));
            Dvpanel_panel_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_PANEL_Autoheight"));
            Dvpanel_panel_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_PANEL_Showcollapseicon"));
            Dvpanel_panel_Iconposition = cgiGet( sPrefix+"DVPANEL_PANEL_Iconposition");
            Dvpanel_panel_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_PANEL_Autoscroll"));
            Gridfapfpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDFAPFPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12A72 */
         E12A72 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12A72( )
      {
         /* Start Routine */
         edtTabela_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTabela_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Visible), 5, 0)));
         subGridfapf_Rows = 10;
         GXCCtl = "GRIDFAPF_Rows_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Rows), 6, 0, ".", "")));
         AV16GridFAPFCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGridfapfcurrentpage_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridFAPFCurrentPage), 10, 0)));
         edtavGridfapfcurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGridfapfcurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridfapfcurrentpage_Visible), 5, 0)));
         AV17GridFAPFPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17GridFAPFPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GridFAPFPageCount), 10, 0)));
         GXt_decimal3 = AV14Tabela_PF;
         new prc_tabela_pf(context ).execute(  A172Tabela_Codigo, out  GXt_decimal3) ;
         AV14Tabela_PF = GXt_decimal3;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14Tabela_PF", StringUtil.LTrim( StringUtil.Str( AV14Tabela_PF, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vTABELA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Tabela_PF, "ZZ,ZZZ,ZZ9.999")));
      }

      protected void E13A72( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
      }

      private void E14A72( )
      {
         /* Gridfd_Load Routine */
         AV6FD_PF = (int)(A377FuncaoDados_PF);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFd_pf_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FD_PF), 8, 0)));
         /* Using cursor H00A73 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A183FuncaoAPF_Ativo = H00A73_A183FuncaoAPF_Ativo[0];
            A165FuncaoAPF_Codigo = H00A73_A165FuncaoAPF_Codigo[0];
            A183FuncaoAPF_Ativo = H00A73_A183FuncaoAPF_Ativo[0];
            AV8FuncaoAPF_Codigo.Add(A165FuncaoAPF_Codigo, 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 27;
         }
         sendrow_272( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_27_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(27, GridfdRow);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV8FuncaoAPF_Codigo", AV8FuncaoAPF_Codigo);
      }

      protected void E11A72( )
      {
         /* Gridfapfpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridfapfpaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV16GridFAPFCurrentPage = (long)(AV16GridFAPFCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGridfapfcurrentpage_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridFAPFCurrentPage), 10, 0)));
            subgridfapf_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridfapfpaginationbar_Selectedpage, "Next") == 0 )
         {
            AV16GridFAPFCurrentPage = (long)(AV16GridFAPFCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGridfapfcurrentpage_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridFAPFCurrentPage), 10, 0)));
            subgridfapf_nextpage( ) ;
         }
         else
         {
            AV15PageToGo = (int)(NumberUtil.Val( Gridfapfpaginationbar_Selectedpage, "."));
            AV16GridFAPFCurrentPage = AV15PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavGridfapfcurrentpage_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GridFAPFCurrentPage), 10, 0)));
            subgridfapf_gotopage( AV15PageToGo) ;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E15A72( )
      {
         /* Gridfd_Refresh Routine */
         AV6FD_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFd_pf_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FD_PF), 8, 0)));
      }

      private void E16A74( )
      {
         /* Gridfapf_Load Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A165FuncaoAPF_Codigo ,
                                              AV8FuncaoAPF_Codigo ,
                                              A183FuncaoAPF_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         /* Using cursor H00A74 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A183FuncaoAPF_Ativo = H00A74_A183FuncaoAPF_Ativo[0];
            A166FuncaoAPF_Nome = H00A74_A166FuncaoAPF_Nome[0];
            A184FuncaoAPF_Tipo = H00A74_A184FuncaoAPF_Tipo[0];
            A165FuncaoAPF_Codigo = H00A74_A165FuncaoAPF_Codigo[0];
            GXt_decimal3 = A386FuncaoAPF_PF;
            new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A386FuncaoAPF_PF = GXt_decimal3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
            GXt_int2 = A387FuncaoAPF_AR;
            new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A387FuncaoAPF_AR = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
            GXt_int2 = A388FuncaoAPF_TD;
            new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A388FuncaoAPF_TD = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
            GXt_char1 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A185FuncaoAPF_Complexidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            AV10FuncaoAPF_Nome = A166FuncaoAPF_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_nome_Internalname, AV10FuncaoAPF_Nome);
            AV13FuncaoAPF_Tipo = A184FuncaoAPF_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, AV13FuncaoAPF_Tipo);
            AV9FuncaoAPF_Complexidade = A185FuncaoAPF_Complexidade;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, AV9FuncaoAPF_Complexidade);
            AV12FuncaoAPF_TD = A388FuncaoAPF_TD;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV12FuncaoAPF_TD), 4, 0)));
            AV7FuncaoAPF_AR = A387FuncaoAPF_AR;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7FuncaoAPF_AR), 4, 0)));
            AV11FuncaoAPF_PF = A386FuncaoAPF_PF;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.Str( AV11FuncaoAPF_PF, 14, 5)));
            AV6FD_PF = (int)(AV6FD_PF+A386FuncaoAPF_PF);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFd_pf_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FD_PF), 8, 0)));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 62;
            }
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               GXCCtl = "GRIDFAPF_nFirstRecordOnPage_" + sGXsfl_27_idx;
               GRIDFAPF_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+GXCCtl), ",", "."));
            }
            else
            {
               GRIDFAPF_nFirstRecordOnPage = 0;
            }
            if ( ( subGridfapf_Islastpage == 1 ) || ( subGridfapf_Rows == 0 ) || ( ( GRIDFAPF_nCurrentRecord >= GRIDFAPF_nFirstRecordOnPage ) && ( GRIDFAPF_nCurrentRecord < GRIDFAPF_nFirstRecordOnPage + subGridfapf_Recordsperpage( ) ) ) )
            {
               sendrow_624( ) ;
               GRIDFAPF_nEOF = 1;
               GXCCtl = "GRIDFAPF_nEOF_" + sGXsfl_27_idx;
               GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nEOF), 1, 0, ".", "")));
               if ( ( subGridfapf_Islastpage == 1 ) && ( ((int)((GRIDFAPF_nCurrentRecord) % (subGridfapf_Recordsperpage( )))) == 0 ) )
               {
                  GRIDFAPF_nFirstRecordOnPage = GRIDFAPF_nCurrentRecord;
               }
            }
            if ( GRIDFAPF_nCurrentRecord >= GRIDFAPF_nFirstRecordOnPage + subGridfapf_Recordsperpage( ) )
            {
               GRIDFAPF_nEOF = 0;
               GXCCtl = "GRIDFAPF_nEOF_" + sGXsfl_27_idx;
               GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nEOF), 1, 0, ".", "")));
            }
            GRIDFAPF_nCurrentRecord = (long)(GRIDFAPF_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_62_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(62, GridfapfRow);
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         cmbavFuncaoapf_tipo.CurrentValue = StringUtil.RTrim( AV13FuncaoAPF_Tipo);
         cmbavFuncaoapf_complexidade.CurrentValue = StringUtil.RTrim( AV9FuncaoAPF_Complexidade);
      }

      protected void wb_table1_2_A72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_A72( true) ;
         }
         else
         {
            wb_table2_8_A72( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_A72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_A72e( true) ;
         }
         else
         {
            wb_table1_2_A72e( false) ;
         }
      }

      protected void wb_table2_8_A72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_11_A72( true) ;
         }
         else
         {
            wb_table3_11_A72( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_A72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_PANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_PANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_21_A72( true) ;
         }
         else
         {
            wb_table4_21_A72( false) ;
         }
         return  ;
      }

      protected void wb_table4_21_A72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_A72e( true) ;
         }
         else
         {
            wb_table2_8_A72e( false) ;
         }
      }

      protected void wb_table4_21_A72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPanel_Internalname, tblPanel_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_24_A72( true) ;
         }
         else
         {
            wb_table5_24_A72( false) ;
         }
         return  ;
      }

      protected void wb_table5_24_A72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_21_A72e( true) ;
         }
         else
         {
            wb_table4_21_A72e( false) ;
         }
      }

      protected void wb_table5_24_A72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable_Internalname, tblTable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridfdContainer.SetIsFreestyle(true);
            GridfdContainer.SetWrapped(nGXWrapped);
            if ( GridfdContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridfdContainer"+"DivS\" data-gxgridid=\"27\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridfd_Internalname, subGridfd_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
               GridfdContainer.AddObjectProperty("GridName", "Gridfd");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridfdContainer = new GXWebGrid( context);
               }
               else
               {
                  GridfdContainer.Clear();
               }
               GridfdContainer.SetIsFreestyle(true);
               GridfdContainer.SetWrapped(nGXWrapped);
               GridfdContainer.AddObjectProperty("GridName", "Gridfd");
               GridfdContainer.AddObjectProperty("Class", StringUtil.RTrim( ""));
               GridfdContainer.AddObjectProperty("Class", "");
               GridfdContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridfdContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridfdContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Backcolorstyle), 1, 0, ".", "")));
               GridfdContainer.AddObjectProperty("CmpContext", sPrefix);
               GridfdContainer.AddObjectProperty("InMasterPage", "false");
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", lblTextblockfuncaodados_nome_Caption);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", A369FuncaoDados_Nome);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridfdColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTabela_Codigo_Visible), 5, 0, ".", "")));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", lblTextblockfuncaodados_tipo_Caption);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.RTrim( A373FuncaoDados_Tipo));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", lblTextblockfuncaodados_complexidade_Caption);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.RTrim( A376FuncaoDados_Complexidade));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", lblTextblockfuncaodados_der_Caption);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", lblTextblockfuncaodados_rlr_Caption);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", lblTextblockfuncaodados_pf_Caption);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16GridFAPFCurrentPage), 10, 0, ".", "")));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", lblTextblockfuncaodados_pf_Caption);
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridfdColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6FD_PF), 8, 0, ".", "")));
               GridfdColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFd_pf_Enabled), 5, 0, ".", "")));
               GridfdContainer.AddColumnProperties(GridfdColumn);
               GridfdContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Allowselection), 1, 0, ".", "")));
               GridfdContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Selectioncolor), 9, 0, ".", "")));
               GridfdContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Allowhovering), 1, 0, ".", "")));
               GridfdContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Hoveringcolor), 9, 0, ".", "")));
               GridfdContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Allowcollapsing), 1, 0, ".", "")));
               GridfdContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfd_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 27 )
         {
            wbEnd = 0;
            nRC_GXsfl_27 = (short)(nGXsfl_27_idx-1);
            if ( GridfdContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridfdContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridfd", GridfdContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridfdContainerData", GridfdContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridfdContainerData"+"V", GridfdContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridfdContainerData"+"V"+"\" value='"+GridfdContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_24_A72e( true) ;
         }
         else
         {
            wb_table5_24_A72e( false) ;
         }
      }

      protected void wb_table3_11_A72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_pf_Internalname, "Pontos de Fun��o Total:", "", "", lblTextblocktabela_pf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_TabelaConsultaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTabela_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14Tabela_PF, 14, 5, ",", "")), ((edtavTabela_pf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14Tabela_PF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV14Tabela_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,16);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTabela_pf_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTabela_pf_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_TabelaConsultaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_A72e( true) ;
         }
         else
         {
            wb_table3_11_A72e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A172Tabela_Codigo = Convert.ToInt32(getParm(obj,0));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAA72( ) ;
         WSA72( ) ;
         WEA72( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA172Tabela_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAA72( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "tabelaconsultawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAA72( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A172Tabela_Codigo = Convert.ToInt32(getParm(obj,2));
         }
         wcpOA172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA172Tabela_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A172Tabela_Codigo != wcpOA172Tabela_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA172Tabela_Codigo = A172Tabela_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA172Tabela_Codigo = cgiGet( sPrefix+"A172Tabela_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA172Tabela_Codigo) > 0 )
         {
            A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA172Tabela_Codigo), ",", "."));
         }
         else
         {
            A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A172Tabela_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAA72( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSA72( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSA72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A172Tabela_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA172Tabela_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A172Tabela_Codigo_CTRL", StringUtil.RTrim( sCtrlA172Tabela_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEA72( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311771942");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("tabelaconsultawc.js", "?2020311771942");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_624( )
      {
         edtavFuncaoapf_nome_Internalname = sPrefix+"vFUNCAOAPF_NOME_"+sGXsfl_62_idx;
         cmbavFuncaoapf_tipo_Internalname = sPrefix+"vFUNCAOAPF_TIPO_"+sGXsfl_62_idx;
         cmbavFuncaoapf_complexidade_Internalname = sPrefix+"vFUNCAOAPF_COMPLEXIDADE_"+sGXsfl_62_idx;
         edtavFuncaoapf_td_Internalname = sPrefix+"vFUNCAOAPF_TD_"+sGXsfl_62_idx;
         edtavFuncaoapf_ar_Internalname = sPrefix+"vFUNCAOAPF_AR_"+sGXsfl_62_idx;
         edtavFuncaoapf_pf_Internalname = sPrefix+"vFUNCAOAPF_PF_"+sGXsfl_62_idx;
      }

      protected void SubsflControlProps_fel_624( )
      {
         edtavFuncaoapf_nome_Internalname = sPrefix+"vFUNCAOAPF_NOME_"+sGXsfl_62_fel_idx;
         cmbavFuncaoapf_tipo_Internalname = sPrefix+"vFUNCAOAPF_TIPO_"+sGXsfl_62_fel_idx;
         cmbavFuncaoapf_complexidade_Internalname = sPrefix+"vFUNCAOAPF_COMPLEXIDADE_"+sGXsfl_62_fel_idx;
         edtavFuncaoapf_td_Internalname = sPrefix+"vFUNCAOAPF_TD_"+sGXsfl_62_fel_idx;
         edtavFuncaoapf_ar_Internalname = sPrefix+"vFUNCAOAPF_AR_"+sGXsfl_62_fel_idx;
         edtavFuncaoapf_pf_Internalname = sPrefix+"vFUNCAOAPF_PF_"+sGXsfl_62_fel_idx;
      }

      protected void sendrow_624( )
      {
         SubsflControlProps_624( ) ;
         WBA70( ) ;
         if ( ( subGridfapf_Rows * 1 == 0 ) || ( nGXsfl_62_idx <= subGridfapf_Recordsperpage( ) * 1 ) )
         {
            GridfapfRow = GXWebRow.GetNew(context,GridfapfContainer);
            if ( subGridfapf_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridfapf_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridfapf_Class, "") != 0 )
               {
                  subGridfapf_Linesclass = subGridfapf_Class+"Odd";
               }
            }
            else if ( subGridfapf_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridfapf_Backstyle = 0;
               subGridfapf_Backcolor = subGridfapf_Allbackcolor;
               if ( StringUtil.StrCmp(subGridfapf_Class, "") != 0 )
               {
                  subGridfapf_Linesclass = subGridfapf_Class+"Uniform";
               }
            }
            else if ( subGridfapf_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridfapf_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridfapf_Class, "") != 0 )
               {
                  subGridfapf_Linesclass = subGridfapf_Class+"Odd";
               }
               subGridfapf_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridfapf_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridfapf_Backstyle = 1;
               if ( ((int)((nGXsfl_62_idx) % (2))) == 0 )
               {
                  subGridfapf_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridfapf_Class, "") != 0 )
                  {
                     subGridfapf_Linesclass = subGridfapf_Class+"Even";
                  }
               }
               else
               {
                  subGridfapf_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridfapf_Class, "") != 0 )
                  {
                     subGridfapf_Linesclass = subGridfapf_Class+"Odd";
                  }
               }
            }
            if ( GridfapfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridfapf_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_62_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridfapfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridfapfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_nome_Internalname,(String)AV10FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncaoapf_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridfapfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_62_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAOAPF_TIPO_" + sGXsfl_62_idx;
               cmbavFuncaoapf_tipo.Name = GXCCtl;
               cmbavFuncaoapf_tipo.WebTags = "";
               cmbavFuncaoapf_tipo.addItem("", "(Nenhum)", 0);
               cmbavFuncaoapf_tipo.addItem("EE", "EE", 0);
               cmbavFuncaoapf_tipo.addItem("SE", "SE", 0);
               cmbavFuncaoapf_tipo.addItem("CE", "CE", 0);
               cmbavFuncaoapf_tipo.addItem("NM", "NM", 0);
               if ( cmbavFuncaoapf_tipo.ItemCount > 0 )
               {
                  AV13FuncaoAPF_Tipo = cmbavFuncaoapf_tipo.getValidValue(AV13FuncaoAPF_Tipo);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, AV13FuncaoAPF_Tipo);
               }
            }
            /* ComboBox */
            GridfapfRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavFuncaoapf_tipo,(String)cmbavFuncaoapf_tipo_Internalname,StringUtil.RTrim( AV13FuncaoAPF_Tipo),(short)1,(String)cmbavFuncaoapf_tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavFuncaoapf_tipo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)false});
            cmbavFuncaoapf_tipo.CurrentValue = StringUtil.RTrim( AV13FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo_Internalname, "Values", (String)(cmbavFuncaoapf_tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridfapfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_62_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAOAPF_COMPLEXIDADE_" + sGXsfl_62_idx;
               cmbavFuncaoapf_complexidade.Name = GXCCtl;
               cmbavFuncaoapf_complexidade.WebTags = "";
               cmbavFuncaoapf_complexidade.addItem("E", "", 0);
               cmbavFuncaoapf_complexidade.addItem("B", "Baixa", 0);
               cmbavFuncaoapf_complexidade.addItem("M", "M�dia", 0);
               cmbavFuncaoapf_complexidade.addItem("A", "Alta", 0);
               if ( cmbavFuncaoapf_complexidade.ItemCount > 0 )
               {
                  AV9FuncaoAPF_Complexidade = cmbavFuncaoapf_complexidade.getValidValue(AV9FuncaoAPF_Complexidade);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, AV9FuncaoAPF_Complexidade);
               }
            }
            /* ComboBox */
            GridfapfRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavFuncaoapf_complexidade,(String)cmbavFuncaoapf_complexidade_Internalname,StringUtil.RTrim( AV9FuncaoAPF_Complexidade),(short)1,(String)cmbavFuncaoapf_complexidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavFuncaoapf_complexidade.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)false});
            cmbavFuncaoapf_complexidade.CurrentValue = StringUtil.RTrim( AV9FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_complexidade_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridfapfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridfapfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_td_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12FuncaoAPF_TD), 4, 0, ",", "")),((edtavFuncaoapf_td_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12FuncaoAPF_TD), "ZZZ9")) : context.localUtil.Format( (decimal)(AV12FuncaoAPF_TD), "ZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_td_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncaoapf_td_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridfapfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridfapfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_ar_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_AR), 4, 0, ",", "")),((edtavFuncaoapf_ar_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7FuncaoAPF_AR), "ZZZ9")) : context.localUtil.Format( (decimal)(AV7FuncaoAPF_AR), "ZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_ar_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncaoapf_ar_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridfapfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridfapfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_pf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV11FuncaoAPF_PF, 14, 5, ",", "")),((edtavFuncaoapf_pf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV11FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV11FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_pf_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncaoapf_pf_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)62,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
            GXCCtl = "FUNCAOAPF_CODIGO_" + sGXsfl_62_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
            GXCCtl = "FUNCAOAPF_PF_" + sGXsfl_62_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")));
            GXCCtl = "FUNCAOAPF_AR_" + sGXsfl_62_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")));
            GXCCtl = "FUNCAOAPF_TD_" + sGXsfl_62_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")));
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_62_idx;
            GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( A185FuncaoAPF_Complexidade));
            GridfapfContainer.AddRow(GridfapfRow);
            nGXsfl_62_idx = (short)(((subGridfapf_Islastpage==1)&&(nGXsfl_62_idx+1>subGridfapf_Recordsperpage( )) ? 1 : nGXsfl_62_idx+1));
            sGXsfl_62_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_62_idx), 4, 0)), 4, "0") + sGXsfl_27_idx;
            SubsflControlProps_624( ) ;
         }
         /* End function sendrow_624 */
      }

      protected void SubsflControlProps_272( )
      {
         lblTextblockfuncaodados_nome_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_NOME_"+sGXsfl_27_idx;
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME_"+sGXsfl_27_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_27_idx;
         lblTextblockfuncaodados_tipo_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_TIPO_"+sGXsfl_27_idx;
         cmbFuncaoDados_Tipo_Internalname = sPrefix+"FUNCAODADOS_TIPO_"+sGXsfl_27_idx;
         lblTextblockfuncaodados_complexidade_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_COMPLEXIDADE_"+sGXsfl_27_idx;
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_27_idx;
         lblTextblockfuncaodados_der_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_DER_"+sGXsfl_27_idx;
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER_"+sGXsfl_27_idx;
         lblTextblockfuncaodados_rlr_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_RLR_"+sGXsfl_27_idx;
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR_"+sGXsfl_27_idx;
         lblTextblockfuncaodados_pf_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_PF_"+sGXsfl_27_idx;
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF_"+sGXsfl_27_idx;
         Gridfapfpaginationbar_Internalname = sPrefix+"GRIDFAPFPAGINATIONBAR_"+sGXsfl_27_idx;
         edtavGridfapfcurrentpage_Internalname = sPrefix+"vGRIDFAPFCURRENTPAGE_"+sGXsfl_27_idx;
         lblTextblockfd_pf_Internalname = sPrefix+"TEXTBLOCKFD_PF_"+sGXsfl_27_idx;
         edtavFd_pf_Internalname = sPrefix+"vFD_PF_"+sGXsfl_27_idx;
         subGridfapf_Internalname = sPrefix+"GRIDFAPF_"+sGXsfl_27_idx;
      }

      protected void SubsflControlProps_fel_272( )
      {
         lblTextblockfuncaodados_nome_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_NOME_"+sGXsfl_27_fel_idx;
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME_"+sGXsfl_27_fel_idx;
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO_"+sGXsfl_27_fel_idx;
         lblTextblockfuncaodados_tipo_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_TIPO_"+sGXsfl_27_fel_idx;
         cmbFuncaoDados_Tipo_Internalname = sPrefix+"FUNCAODADOS_TIPO_"+sGXsfl_27_fel_idx;
         lblTextblockfuncaodados_complexidade_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_COMPLEXIDADE_"+sGXsfl_27_fel_idx;
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_27_fel_idx;
         lblTextblockfuncaodados_der_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_DER_"+sGXsfl_27_fel_idx;
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER_"+sGXsfl_27_fel_idx;
         lblTextblockfuncaodados_rlr_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_RLR_"+sGXsfl_27_fel_idx;
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR_"+sGXsfl_27_fel_idx;
         lblTextblockfuncaodados_pf_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_PF_"+sGXsfl_27_fel_idx;
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF_"+sGXsfl_27_fel_idx;
         Gridfapfpaginationbar_Internalname = sPrefix+"GRIDFAPFPAGINATIONBAR_"+sGXsfl_27_fel_idx;
         edtavGridfapfcurrentpage_Internalname = sPrefix+"vGRIDFAPFCURRENTPAGE_"+sGXsfl_27_fel_idx;
         lblTextblockfd_pf_Internalname = sPrefix+"TEXTBLOCKFD_PF_"+sGXsfl_27_fel_idx;
         edtavFd_pf_Internalname = sPrefix+"vFD_PF_"+sGXsfl_27_fel_idx;
         subGridfapf_Internalname = sPrefix+"GRIDFAPF_"+sGXsfl_27_fel_idx;
      }

      protected void sendrow_272( )
      {
         SubsflControlProps_272( ) ;
         WBA70( ) ;
         GridfdRow = GXWebRow.GetNew(context,GridfdContainer);
         if ( subGridfd_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridfd_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridfd_Class, "") != 0 )
            {
               subGridfd_Linesclass = subGridfd_Class+"Odd";
            }
         }
         else if ( subGridfd_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridfd_Backstyle = 0;
            subGridfd_Backcolor = subGridfd_Allbackcolor;
            if ( StringUtil.StrCmp(subGridfd_Class, "") != 0 )
            {
               subGridfd_Linesclass = subGridfd_Class+"Uniform";
            }
         }
         else if ( subGridfd_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridfd_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridfd_Class, "") != 0 )
            {
               subGridfd_Linesclass = subGridfd_Class+"Odd";
            }
            subGridfd_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridfd_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridfd_Backstyle = 1;
            if ( ((int)(((nGXsfl_27_idx-1)/ (decimal)(1)) % (2))) == 0 )
            {
               subGridfd_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridfd_Class, "") != 0 )
               {
                  subGridfd_Linesclass = subGridfd_Class+"Odd";
               }
            }
            else
            {
               subGridfd_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridfd_Class, "") != 0 )
               {
                  subGridfd_Linesclass = subGridfd_Class+"Even";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            if ( ( 1 == 0 ) && ( nGXsfl_27_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subGridfd_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_27_idx+"\">") ;
            }
            if ( 1 > 0 )
            {
               if ( ( 1 == 1 ) || ( ((int)((nGXsfl_27_idx) % (1))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subGridfd_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_27_idx+"\">") ;
               }
            }
         }
         GridfdRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridfd_Linesclass,(String)""});
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridfdRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockfuncaodados_nome_Internalname,(String)"Nome:",(String)"",(String)"",(String)lblTextblockfuncaodados_nome_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridfdRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Nome_Internalname,(String)A369FuncaoDados_Nome,(String)"",(String)"",(short)0,(short)1,(short)0,(short)0,(short)80,(String)"chr",(short)3,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"200",(short)-1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         /* Single line edit */
         ROClassString = "Attribute";
         GridfdRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(int)edtTabela_Codigo_Visible,(short)0,(short)0,(String)"text",(String)"",(short)6,(String)"chr",(short)1,(String)"row",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridfdRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockfuncaodados_tipo_Internalname,(String)"Tipo:",(String)"",(String)"",(String)lblTextblockfuncaodados_tipo_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_27_idx;
         cmbFuncaoDados_Tipo.Name = GXCCtl;
         cmbFuncaoDados_Tipo.WebTags = "";
         cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
         cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
         cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
         cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
         if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
         {
            A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
         }
         /* ComboBox */
         GridfdRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Tipo,(String)cmbFuncaoDados_Tipo_Internalname,StringUtil.RTrim( A373FuncaoDados_Tipo),(short)1,(String)cmbFuncaoDados_Tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)1,(short)0,(short)1,(short)0,(short)0,(String)"em",(short)0,(String)"",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
         cmbFuncaoDados_Tipo.CurrentValue = StringUtil.RTrim( A373FuncaoDados_Tipo);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Tipo_Internalname, "Values", (String)(cmbFuncaoDados_Tipo.ToJavascriptSource()));
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridfdRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockfuncaodados_complexidade_Internalname,(String)"CP:",(String)"",(String)"",(String)lblTextblockfuncaodados_complexidade_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_27_idx;
         cmbFuncaoDados_Complexidade.Name = GXCCtl;
         cmbFuncaoDados_Complexidade.WebTags = "";
         cmbFuncaoDados_Complexidade.addItem("E", "", 0);
         cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
         cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
         cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
         if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
         {
            A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
         }
         /* ComboBox */
         GridfdRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Complexidade,(String)cmbFuncaoDados_Complexidade_Internalname,StringUtil.RTrim( A376FuncaoDados_Complexidade),(short)1,(String)cmbFuncaoDados_Complexidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)1,(short)0,(short)1,(short)0,(short)0,(String)"em",(short)0,(String)"",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
         cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridfdRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockfuncaodados_der_Internalname,(String)"DER:",(String)"",(String)"",(String)lblTextblockfuncaodados_der_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridfdRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_DER_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_DER_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)4,(String)"chr",(short)1,(String)"row",(short)4,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridfdRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockfuncaodados_rlr_Internalname,(String)"RLR:",(String)"",(String)"",(String)lblTextblockfuncaodados_rlr_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridfdRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_RLR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_RLR_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)4,(String)"chr",(short)1,(String)"row",(short)4,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridfdRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockfuncaodados_pf_Internalname,(String)"PF:",(String)"",(String)"",(String)lblTextblockfuncaodados_pf_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridfdRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")),context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)14,(String)"chr",(short)1,(String)"row",(short)14,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("row");
         }
         GridfdRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridfd_Linesclass,(String)""});
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "vertical-align:top")+"\""});
         /* Table start */
         GridfdRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTablemergedgridfapf_Internalname+"_"+sGXsfl_27_idx,(short)1,(String)"TableMerged",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)0,(short)0,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         GridfdRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Table start */
         GridfdRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblGridfapftablewithpaginationbar_Internalname+"_"+sGXsfl_27_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)0,(short)0,(String)"",(short)100,(String)"",(String)"px",(String)"%"});
         GridfdRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /*  Child Grid Control  */
         GridfdRow.AddColumnProperties("subfile", -1, isAjaxCallMode( ), new Object[] {(String)"GridfapfContainer"});
         if ( isAjaxCallMode( ) )
         {
            GridfapfContainer = new GXWebGrid( context);
         }
         else
         {
            GridfapfContainer.Clear();
         }
         GridfapfContainer.SetWrapped(nGXWrapped);
         if ( GridfapfContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<div id=\""+sPrefix+"GridfapfContainer"+"DivS\" data-gxgridid=\"62\">") ;
            sStyleString = "";
            GxWebStd.gx_table_start( context, subGridfapf_Internalname, subGridfapf_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
            /* Subfile titles */
            context.WriteHtmlText( "<tr") ;
            context.WriteHtmlTextNl( ">") ;
            if ( subGridfapf_Backcolorstyle == 0 )
            {
               subGridfapf_Titlebackstyle = 0;
               if ( StringUtil.Len( subGridfapf_Class) > 0 )
               {
                  subGridfapf_Linesclass = subGridfapf_Class+"Title";
               }
            }
            else
            {
               subGridfapf_Titlebackstyle = 1;
               if ( subGridfapf_Backcolorstyle == 1 )
               {
                  subGridfapf_Titlebackcolor = subGridfapf_Allbackcolor;
                  if ( StringUtil.Len( subGridfapf_Class) > 0 )
                  {
                     subGridfapf_Linesclass = subGridfapf_Class+"UniformTitle";
                  }
               }
               else
               {
                  if ( StringUtil.Len( subGridfapf_Class) > 0 )
                  {
                     subGridfapf_Linesclass = subGridfapf_Class+"Title";
                  }
               }
            }
            context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfapf_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "Fun��es de Transa��o") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfapf_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "Tipo") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfapf_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "CP") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfapf_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "DER") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfapf_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "RLR") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridfapf_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
            context.SendWebValue( "PF") ;
            context.WriteHtmlTextNl( "</th>") ;
            context.WriteHtmlTextNl( "</tr>") ;
            GridfapfContainer.AddObjectProperty("GridName", "Gridfapf");
         }
         else
         {
            GridfapfContainer.AddObjectProperty("GridName", "Gridfapf");
            GridfapfContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            GridfapfContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Backcolorstyle), 1, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("CmpContext", sPrefix);
            GridfapfContainer.AddObjectProperty("InMasterPage", "false");
            GridfapfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridfapfColumn.AddObjectProperty("Value", AV10FuncaoAPF_Nome);
            GridfapfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_nome_Enabled), 5, 0, ".", "")));
            GridfapfContainer.AddColumnProperties(GridfapfColumn);
            GridfapfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridfapfColumn.AddObjectProperty("Value", StringUtil.RTrim( AV13FuncaoAPF_Tipo));
            GridfapfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapf_tipo.Enabled), 5, 0, ".", "")));
            GridfapfContainer.AddColumnProperties(GridfapfColumn);
            GridfapfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridfapfColumn.AddObjectProperty("Value", StringUtil.RTrim( AV9FuncaoAPF_Complexidade));
            GridfapfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapf_complexidade.Enabled), 5, 0, ".", "")));
            GridfapfContainer.AddColumnProperties(GridfapfColumn);
            GridfapfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridfapfColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12FuncaoAPF_TD), 4, 0, ".", "")));
            GridfapfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_td_Enabled), 5, 0, ".", "")));
            GridfapfContainer.AddColumnProperties(GridfapfColumn);
            GridfapfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridfapfColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7FuncaoAPF_AR), 4, 0, ".", "")));
            GridfapfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_ar_Enabled), 5, 0, ".", "")));
            GridfapfContainer.AddColumnProperties(GridfapfColumn);
            GridfapfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridfapfColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV11FuncaoAPF_PF, 14, 5, ".", "")));
            GridfapfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_pf_Enabled), 5, 0, ".", "")));
            GridfapfContainer.AddColumnProperties(GridfapfColumn);
            GridfapfContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Allowselection), 1, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Selectioncolor), 9, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Allowhovering), 1, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Hoveringcolor), 9, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Allowcollapsing), 1, 0, ".", "")));
            GridfapfContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Collapsed), 1, 0, ".", "")));
         }
         RFA74( ) ;
         nRC_GXsfl_62 = (short)(nGXsfl_62_idx-1);
         GXCCtl = "nRC_GXsfl_62_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_62), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         if ( GridfapfContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "</table>") ;
         }
         else
         {
            if ( ! isAjaxCallMode( ) )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"GridfapfContainerData"+"_"+sGXsfl_27_idx, GridfapfContainer.ToJavascriptSource());
            }
            if ( isAjaxCallMode( ) )
            {
               GridfdRow.AddGrid("Gridfapf", GridfapfContainer);
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"GridfapfContainerData"+"V_"+sGXsfl_27_idx, GridfapfContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridfapfContainerData"+"V_"+sGXsfl_27_idx+"\" value='"+GridfapfContainer.GridValuesHidden()+"'/>") ;
            }
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("row");
         }
         GridfdRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* User Defined Control */
         GridfdRow.AddColumnProperties("usercontrol", -1, isAjaxCallMode( ), new Object[] {(String)sPrefix+"GRIDFAPFPAGINATIONBARContainer"+"_"+sGXsfl_27_idx,(short)-1});
         /* Single line edit */
         ROClassString = "Attribute";
         GridfdRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavGridfapfcurrentpage_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16GridFAPFCurrentPage), 10, 0, ",", "")),context.localUtil.Format( (decimal)(AV16GridFAPFCurrentPage), "ZZZZZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavGridfapfcurrentpage_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(int)edtavGridfapfcurrentpage_Visible,(short)0,(short)0,(String)"text",(String)"",(short)10,(String)"chr",(short)1,(String)"row",(short)10,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("row");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("table");
         }
         /* End of table */
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\""});
         /* Table start */
         GridfdRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblUnnamedtable2_Internalname+"_"+sGXsfl_27_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         GridfdRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridfdRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockfd_pf_Internalname,(String)"PF:",(String)"",(String)"",(String)lblTextblockfd_pf_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         GridfdRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridfdRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFd_pf_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6FD_PF), 8, 0, ",", "")),((edtavFd_pf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6FD_PF), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV6FD_PF), "ZZZZZZZ9")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFd_pf_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavFd_pf_Enabled,(short)0,(String)"text",(String)"",(short)8,(String)"chr",(short)1,(String)"row",(short)8,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("row");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("table");
         }
         /* End of table */
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("row");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("table");
         }
         /* End of table */
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("cell");
         }
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            GridfdContainer.CloseTag("row");
         }
         GXCCtl = "vGRIDFAPFPAGECOUNT_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17GridFAPFPageCount), 10, 0, ",", "")));
         GXCCtl = "GRIDFAPF_nFirstRecordOnPage_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nFirstRecordOnPage), 15, 0, ",", "")));
         GXCCtl = "GRIDFAPF_nEOF_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDFAPF_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_COMPLEXIDADE"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sPrefix+sGXsfl_27_idx, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_DER"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sPrefix+sGXsfl_27_idx, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_RLR"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sPrefix+sGXsfl_27_idx, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAODADOS_PF"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sPrefix+sGXsfl_27_idx, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         GXCCtl = "GRIDFAPF_Rows_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridfapf_Rows), 6, 0, ".", "")));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Class_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Class));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_First_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_First));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Previous_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Previous));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Next_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Next));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Last_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Last));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Caption_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Caption));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Showfirst_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Gridfapfpaginationbar_Showfirst));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Showprevious_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Gridfapfpaginationbar_Showprevious));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Shownext_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Gridfapfpaginationbar_Shownext));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Showlast_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.BoolToStr( Gridfapfpaginationbar_Showlast));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Pagestoshow_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridfapfpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Pagingbuttonsposition_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Pagingbuttonsposition));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Pagingcaptionposition_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Pagingcaptionposition));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Emptygridclass_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Emptygridclass));
         GXCCtl = "GRIDFAPFPAGINATIONBAR_Emptygridcaption_" + sGXsfl_27_idx;
         GxWebStd.gx_hidden_field( context, sPrefix+GXCCtl, StringUtil.RTrim( Gridfapfpaginationbar_Emptygridcaption));
         GRIDFAPF_nFirstRecordOnPage = 0;
         GRIDFAPF_nCurrentRecord = 0;
         /* End of Columns property logic. */
         if ( GridfdContainer.GetWrapped() == 1 )
         {
            if ( 1 > 0 )
            {
               if ( ((int)((nGXsfl_27_idx) % (1))) == 0 )
               {
                  context.WriteHtmlTextNl( "</tr>") ;
               }
            }
         }
         GridfdContainer.AddRow(GridfdRow);
         nGXsfl_27_idx = (short)(((subGridfd_Islastpage==1)&&(nGXsfl_27_idx+1>subGridfd_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
         sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
         SubsflControlProps_272( ) ;
         /* End function sendrow_272 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktabela_pf_Internalname = sPrefix+"TEXTBLOCKTABELA_PF";
         edtavTabela_pf_Internalname = sPrefix+"vTABELA_PF";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         lblTextblockfuncaodados_nome_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_NOME";
         edtFuncaoDados_Nome_Internalname = sPrefix+"FUNCAODADOS_NOME";
         edtTabela_Codigo_Internalname = sPrefix+"TABELA_CODIGO";
         lblTextblockfuncaodados_tipo_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_TIPO";
         cmbFuncaoDados_Tipo_Internalname = sPrefix+"FUNCAODADOS_TIPO";
         lblTextblockfuncaodados_complexidade_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_COMPLEXIDADE";
         cmbFuncaoDados_Complexidade_Internalname = sPrefix+"FUNCAODADOS_COMPLEXIDADE";
         lblTextblockfuncaodados_der_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_DER";
         edtFuncaoDados_DER_Internalname = sPrefix+"FUNCAODADOS_DER";
         lblTextblockfuncaodados_rlr_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_RLR";
         edtFuncaoDados_RLR_Internalname = sPrefix+"FUNCAODADOS_RLR";
         lblTextblockfuncaodados_pf_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_PF";
         edtFuncaoDados_PF_Internalname = sPrefix+"FUNCAODADOS_PF";
         edtavFuncaoapf_nome_Internalname = sPrefix+"vFUNCAOAPF_NOME";
         cmbavFuncaoapf_tipo_Internalname = sPrefix+"vFUNCAOAPF_TIPO";
         cmbavFuncaoapf_complexidade_Internalname = sPrefix+"vFUNCAOAPF_COMPLEXIDADE";
         edtavFuncaoapf_td_Internalname = sPrefix+"vFUNCAOAPF_TD";
         edtavFuncaoapf_ar_Internalname = sPrefix+"vFUNCAOAPF_AR";
         edtavFuncaoapf_pf_Internalname = sPrefix+"vFUNCAOAPF_PF";
         Gridfapfpaginationbar_Internalname = sPrefix+"GRIDFAPFPAGINATIONBAR";
         edtavGridfapfcurrentpage_Internalname = sPrefix+"vGRIDFAPFCURRENTPAGE";
         tblGridfapftablewithpaginationbar_Internalname = sPrefix+"GRIDFAPFTABLEWITHPAGINATIONBAR";
         lblTextblockfd_pf_Internalname = sPrefix+"TEXTBLOCKFD_PF";
         edtavFd_pf_Internalname = sPrefix+"vFD_PF";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblTablemergedgridfapf_Internalname = sPrefix+"TABLEMERGEDGRIDFAPF";
         tblTable_Internalname = sPrefix+"TABLE";
         tblPanel_Internalname = sPrefix+"PANEL";
         Dvpanel_panel_Internalname = sPrefix+"DVPANEL_PANEL";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Form.Internalname = sPrefix+"FORM";
         subGridfapf_Internalname = sPrefix+"GRIDFAPF";
         subGridfd_Internalname = sPrefix+"GRIDFD";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavFd_pf_Jsonclick = "";
         edtavGridfapfcurrentpage_Jsonclick = "";
         subGridfapf_Allowcollapsing = 0;
         subGridfapf_Allowselection = 0;
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_DER_Jsonclick = "";
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         cmbFuncaoDados_Tipo_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         subGridfd_Class = "";
         edtavFuncaoapf_pf_Jsonclick = "";
         edtavFuncaoapf_pf_Enabled = 0;
         edtavFuncaoapf_ar_Jsonclick = "";
         edtavFuncaoapf_ar_Enabled = 0;
         edtavFuncaoapf_td_Jsonclick = "";
         edtavFuncaoapf_td_Enabled = 0;
         cmbavFuncaoapf_complexidade_Jsonclick = "";
         cmbavFuncaoapf_complexidade.Enabled = 0;
         cmbavFuncaoapf_tipo_Jsonclick = "";
         cmbavFuncaoapf_tipo.Enabled = 0;
         edtavFuncaoapf_nome_Jsonclick = "";
         edtavFuncaoapf_nome_Enabled = 0;
         subGridfapf_Class = "WorkWithBorder WorkWith";
         edtavTabela_pf_Jsonclick = "";
         edtavTabela_pf_Enabled = 1;
         subGridfd_Allowcollapsing = 0;
         edtavFd_pf_Enabled = 0;
         lblTextblockfuncaodados_pf_Caption = "PF:";
         lblTextblockfuncaodados_rlr_Caption = "RLR:";
         lblTextblockfuncaodados_der_Caption = "DER:";
         lblTextblockfuncaodados_complexidade_Caption = "CP:";
         lblTextblockfuncaodados_tipo_Caption = "Tipo:";
         lblTextblockfuncaodados_nome_Caption = "Nome:";
         subGridfapf_Backcolorstyle = 3;
         subGridfd_Backcolorstyle = 0;
         Gridfapfpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridfapfpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridfapfpaginationbar_Pagingcaptionposition = "Left";
         Gridfapfpaginationbar_Pagingbuttonsposition = "Right";
         Gridfapfpaginationbar_Pagestoshow = 5;
         Gridfapfpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridfapfpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridfapfpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridfapfpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridfapfpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridfapfpaginationbar_Last = "�|";
         Gridfapfpaginationbar_Next = "�";
         Gridfapfpaginationbar_Previous = "�";
         Gridfapfpaginationbar_First = "|�";
         Gridfapfpaginationbar_Class = "PaginationBar";
         Dvpanel_panel_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_panel_Iconposition = "left";
         Dvpanel_panel_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_panel_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_panel_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_panel_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_panel_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_panel_Title = "Fun��es de Dados";
         Dvpanel_panel_Cls = "GXUI-DVelop-Panel";
         Dvpanel_panel_Width = "100%";
         edtavGridfapfcurrentpage_Visible = 1;
         edtTabela_Codigo_Visible = 1;
         subGridfapf_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDFD_nFirstRecordOnPage',nv:0},{av:'GRIDFD_nEOF',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtTabela_Codigo_Visible',ctrl:'TABELA_CODIGO',prop:'Visible'},{av:'AV16GridFAPFCurrentPage',fld:'vGRIDFAPFCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavGridfapfcurrentpage_Visible',ctrl:'vGRIDFAPFCURRENTPAGE',prop:'Visible'},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'GRIDFAPF_nFirstRecordOnPage',nv:0},{av:'GRIDFAPF_nEOF',nv:0},{av:'subGridfapf_Rows',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'',nv:null},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6FD_PF',fld:'vFD_PF',pic:'ZZZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRIDFD.LOAD","{handler:'E14A72',iparms:[{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'',nv:null}],oparms:[{av:'AV6FD_PF',fld:'vFD_PF',pic:'ZZZZZZZ9',nv:0},{av:'AV8FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'',nv:null}]}");
         setEventMetadata("GRIDFAPF.LOAD","{handler:'E16A74',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'',nv:null},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6FD_PF',fld:'vFD_PF',pic:'ZZZZZZZ9',nv:0}],oparms:[{av:'AV10FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV13FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV9FuncaoAPF_Complexidade',fld:'vFUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'AV12FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV7FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV11FuncaoAPF_PF',fld:'vFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6FD_PF',fld:'vFD_PF',pic:'ZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDFAPFPAGINATIONBAR.CHANGEPAGE","{handler:'E11A72',iparms:[{av:'GRIDFD_nFirstRecordOnPage',nv:0},{av:'GRIDFD_nEOF',nv:0},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'edtTabela_Codigo_Visible',ctrl:'TABELA_CODIGO',prop:'Visible'},{av:'AV16GridFAPFCurrentPage',fld:'vGRIDFAPFCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavGridfapfcurrentpage_Visible',ctrl:'vGRIDFAPFCURRENTPAGE',prop:'Visible'},{av:'A377FuncaoDados_PF',fld:'FUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'GRIDFAPF_nFirstRecordOnPage',nv:0},{av:'GRIDFAPF_nEOF',nv:0},{av:'subGridfapf_Rows',nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6FD_PF',fld:'vFD_PF',pic:'ZZZZZZZ9',nv:0},{av:'Gridfapfpaginationbar_Selectedpage',ctrl:'GRIDFAPFPAGINATIONBAR',prop:'SelectedPage'}],oparms:[{av:'AV16GridFAPFCurrentPage',fld:'vGRIDFAPFCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDFD.REFRESH","{handler:'E15A72',iparms:[],oparms:[{av:'AV6FD_PF',fld:'vFD_PF',pic:'ZZZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridfapfpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV8FuncaoAPF_Codigo = new GxSimpleCollection();
         A183FuncaoAPF_Ativo = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         A369FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         A376FuncaoDados_Complexidade = "";
         AV10FuncaoAPF_Nome = "";
         AV13FuncaoAPF_Tipo = "";
         AV9FuncaoAPF_Complexidade = "";
         GridfdContainer = new GXWebGrid( context);
         GridfapfContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00A72_A172Tabela_Codigo = new int[1] ;
         H00A72_A394FuncaoDados_Ativo = new String[] {""} ;
         H00A72_A373FuncaoDados_Tipo = new String[] {""} ;
         H00A72_A369FuncaoDados_Nome = new String[] {""} ;
         H00A72_A755FuncaoDados_Contar = new bool[] {false} ;
         H00A72_n755FuncaoDados_Contar = new bool[] {false} ;
         H00A72_A368FuncaoDados_Codigo = new int[1] ;
         A394FuncaoDados_Ativo = "";
         H00A73_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00A73_A165FuncaoAPF_Codigo = new int[1] ;
         GridfdRow = new GXWebRow();
         H00A74_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00A74_A166FuncaoAPF_Nome = new String[] {""} ;
         H00A74_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00A74_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char1 = "";
         GridfapfRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         GridfdColumn = new GXWebColumn();
         lblTextblocktabela_pf_Jsonclick = "";
         TempTags = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA172Tabela_Codigo = "";
         subGridfapf_Linesclass = "";
         ROClassString = "";
         subGridfd_Linesclass = "";
         lblTextblockfuncaodados_nome_Jsonclick = "";
         lblTextblockfuncaodados_tipo_Jsonclick = "";
         lblTextblockfuncaodados_complexidade_Jsonclick = "";
         lblTextblockfuncaodados_der_Jsonclick = "";
         lblTextblockfuncaodados_rlr_Jsonclick = "";
         lblTextblockfuncaodados_pf_Jsonclick = "";
         GridfapfColumn = new GXWebColumn();
         lblTextblockfd_pf_Jsonclick = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tabelaconsultawc__default(),
            new Object[][] {
                new Object[] {
               H00A72_A172Tabela_Codigo, H00A72_A394FuncaoDados_Ativo, H00A72_A373FuncaoDados_Tipo, H00A72_A369FuncaoDados_Nome, H00A72_A755FuncaoDados_Contar, H00A72_n755FuncaoDados_Contar, H00A72_A368FuncaoDados_Codigo
               }
               , new Object[] {
               H00A73_A183FuncaoAPF_Ativo, H00A73_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00A74_A183FuncaoAPF_Ativo, H00A74_A166FuncaoAPF_Nome, H00A74_A184FuncaoAPF_Tipo, H00A74_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavTabela_pf_Enabled = 0;
         edtavFuncaoapf_nome_Enabled = 0;
         cmbavFuncaoapf_tipo.Enabled = 0;
         cmbavFuncaoapf_complexidade.Enabled = 0;
         edtavFuncaoapf_td_Enabled = 0;
         edtavFuncaoapf_ar_Enabled = 0;
         edtavFuncaoapf_pf_Enabled = 0;
         edtavFd_pf_Enabled = 0;
      }

      private short nRC_GXsfl_62 ;
      private short nGXsfl_62_idx=1 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short nRC_GXsfl_27 ;
      private short nGXsfl_27_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short GRIDFAPF_nEOF ;
      private short AV12FuncaoAPF_TD ;
      private short AV7FuncaoAPF_AR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_27_Refreshing=0 ;
      private short subGridfd_Backcolorstyle ;
      private short nGXsfl_62_Refreshing=0 ;
      private short subGridfapf_Backcolorstyle ;
      private short GRIDFD_nEOF ;
      private short GXt_int2 ;
      private short subGridfd_Allowselection ;
      private short subGridfd_Allowhovering ;
      private short subGridfd_Allowcollapsing ;
      private short subGridfd_Collapsed ;
      private short subGridfapf_Backstyle ;
      private short subGridfd_Backstyle ;
      private short subGridfapf_Titlebackstyle ;
      private short subGridfapf_Allowselection ;
      private short subGridfapf_Allowhovering ;
      private short subGridfapf_Allowcollapsing ;
      private short subGridfapf_Collapsed ;
      private int A172Tabela_Codigo ;
      private int wcpOA172Tabela_Codigo ;
      private int edtTabela_Codigo_Visible ;
      private int edtavGridfapfcurrentpage_Visible ;
      private int subGridfapf_Rows ;
      private int A165FuncaoAPF_Codigo ;
      private int AV6FD_PF ;
      private int edtavTabela_pf_Enabled ;
      private int edtavFuncaoapf_nome_Enabled ;
      private int edtavFuncaoapf_td_Enabled ;
      private int edtavFuncaoapf_ar_Enabled ;
      private int edtavFuncaoapf_pf_Enabled ;
      private int edtavFd_pf_Enabled ;
      private int A368FuncaoDados_Codigo ;
      private int Gridfapfpaginationbar_Pagestoshow ;
      private int subGridfd_Islastpage ;
      private int subGridfapf_Islastpage ;
      private int AV15PageToGo ;
      private int subGridfd_Selectioncolor ;
      private int subGridfd_Hoveringcolor ;
      private int idxLst ;
      private int subGridfapf_Backcolor ;
      private int subGridfapf_Allbackcolor ;
      private int subGridfd_Backcolor ;
      private int subGridfd_Allbackcolor ;
      private int subGridfapf_Titlebackcolor ;
      private int subGridfapf_Selectioncolor ;
      private int subGridfapf_Hoveringcolor ;
      private long AV16GridFAPFCurrentPage ;
      private long GRIDFAPF_nFirstRecordOnPage ;
      private long AV17GridFAPFPageCount ;
      private long GRIDFAPF_nCurrentRecord ;
      private long GRIDFD_nCurrentRecord ;
      private long GRIDFD_nFirstRecordOnPage ;
      private long GRIDFAPF_nRecordCount ;
      private decimal A386FuncaoAPF_PF ;
      private decimal A377FuncaoDados_PF ;
      private decimal AV14Tabela_PF ;
      private decimal AV11FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String Gridfapfpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_62_idx="0001" ;
      private String A183FuncaoAPF_Ativo ;
      private String A184FuncaoAPF_Tipo ;
      private String A185FuncaoAPF_Complexidade ;
      private String edtavFd_pf_Internalname ;
      private String GXKey ;
      private String sGXsfl_27_idx="0001" ;
      private String edtTabela_Codigo_Internalname ;
      private String edtavGridfapfcurrentpage_Internalname ;
      private String edtavTabela_pf_Internalname ;
      private String edtavFuncaoapf_nome_Internalname ;
      private String cmbavFuncaoapf_tipo_Internalname ;
      private String cmbavFuncaoapf_complexidade_Internalname ;
      private String edtavFuncaoapf_td_Internalname ;
      private String edtavFuncaoapf_ar_Internalname ;
      private String edtavFuncaoapf_pf_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_panel_Width ;
      private String Dvpanel_panel_Cls ;
      private String Dvpanel_panel_Title ;
      private String Dvpanel_panel_Iconposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Gridfapfpaginationbar_Class ;
      private String Gridfapfpaginationbar_Internalname ;
      private String Gridfapfpaginationbar_First ;
      private String Gridfapfpaginationbar_Previous ;
      private String Gridfapfpaginationbar_Next ;
      private String Gridfapfpaginationbar_Last ;
      private String Gridfapfpaginationbar_Caption ;
      private String Gridfapfpaginationbar_Pagingbuttonsposition ;
      private String Gridfapfpaginationbar_Pagingcaptionposition ;
      private String Gridfapfpaginationbar_Emptygridclass ;
      private String Gridfapfpaginationbar_Emptygridcaption ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String cmbFuncaoDados_Tipo_Internalname ;
      private String A373FuncaoDados_Tipo ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String A376FuncaoDados_Complexidade ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_PF_Internalname ;
      private String AV13FuncaoAPF_Tipo ;
      private String AV9FuncaoAPF_Complexidade ;
      private String scmdbuf ;
      private String A394FuncaoDados_Ativo ;
      private String GXt_char1 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablecontent_Internalname ;
      private String tblPanel_Internalname ;
      private String tblTable_Internalname ;
      private String subGridfd_Internalname ;
      private String lblTextblockfuncaodados_nome_Caption ;
      private String lblTextblockfuncaodados_tipo_Caption ;
      private String lblTextblockfuncaodados_complexidade_Caption ;
      private String lblTextblockfuncaodados_der_Caption ;
      private String lblTextblockfuncaodados_rlr_Caption ;
      private String lblTextblockfuncaodados_pf_Caption ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblocktabela_pf_Internalname ;
      private String lblTextblocktabela_pf_Jsonclick ;
      private String TempTags ;
      private String edtavTabela_pf_Jsonclick ;
      private String sCtrlA172Tabela_Codigo ;
      private String sGXsfl_62_fel_idx="0001" ;
      private String subGridfapf_Class ;
      private String subGridfapf_Linesclass ;
      private String ROClassString ;
      private String edtavFuncaoapf_nome_Jsonclick ;
      private String cmbavFuncaoapf_tipo_Jsonclick ;
      private String cmbavFuncaoapf_complexidade_Jsonclick ;
      private String edtavFuncaoapf_td_Jsonclick ;
      private String edtavFuncaoapf_ar_Jsonclick ;
      private String edtavFuncaoapf_pf_Jsonclick ;
      private String lblTextblockfuncaodados_nome_Internalname ;
      private String lblTextblockfuncaodados_tipo_Internalname ;
      private String lblTextblockfuncaodados_complexidade_Internalname ;
      private String lblTextblockfuncaodados_der_Internalname ;
      private String lblTextblockfuncaodados_rlr_Internalname ;
      private String lblTextblockfuncaodados_pf_Internalname ;
      private String lblTextblockfd_pf_Internalname ;
      private String subGridfapf_Internalname ;
      private String sGXsfl_27_fel_idx="0001" ;
      private String subGridfd_Class ;
      private String subGridfd_Linesclass ;
      private String lblTextblockfuncaodados_nome_Jsonclick ;
      private String edtTabela_Codigo_Jsonclick ;
      private String lblTextblockfuncaodados_tipo_Jsonclick ;
      private String cmbFuncaoDados_Tipo_Jsonclick ;
      private String lblTextblockfuncaodados_complexidade_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String lblTextblockfuncaodados_der_Jsonclick ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String lblTextblockfuncaodados_rlr_Jsonclick ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String lblTextblockfuncaodados_pf_Jsonclick ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String tblTablemergedgridfapf_Internalname ;
      private String tblGridfapftablewithpaginationbar_Internalname ;
      private String edtavGridfapfcurrentpage_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockfd_pf_Jsonclick ;
      private String edtavFd_pf_Jsonclick ;
      private String Dvpanel_panel_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool Dvpanel_panel_Collapsible ;
      private bool Dvpanel_panel_Collapsed ;
      private bool Dvpanel_panel_Autowidth ;
      private bool Dvpanel_panel_Autoheight ;
      private bool Dvpanel_panel_Showcollapseicon ;
      private bool Dvpanel_panel_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool Gridfapfpaginationbar_Showfirst ;
      private bool Gridfapfpaginationbar_Showprevious ;
      private bool Gridfapfpaginationbar_Shownext ;
      private bool Gridfapfpaginationbar_Showlast ;
      private bool n755FuncaoDados_Contar ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A166FuncaoAPF_Nome ;
      private String A369FuncaoDados_Nome ;
      private String AV10FuncaoAPF_Nome ;
      private GXWebGrid GridfdContainer ;
      private GXWebGrid GridfapfContainer ;
      private GXWebRow GridfdRow ;
      private GXWebRow GridfapfRow ;
      private GXWebColumn GridfdColumn ;
      private GXWebColumn GridfapfColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoDados_Tipo ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private GXCombobox cmbavFuncaoapf_tipo ;
      private GXCombobox cmbavFuncaoapf_complexidade ;
      private IDataStoreProvider pr_default ;
      private int[] H00A72_A172Tabela_Codigo ;
      private String[] H00A72_A394FuncaoDados_Ativo ;
      private String[] H00A72_A373FuncaoDados_Tipo ;
      private String[] H00A72_A369FuncaoDados_Nome ;
      private bool[] H00A72_A755FuncaoDados_Contar ;
      private bool[] H00A72_n755FuncaoDados_Contar ;
      private int[] H00A72_A368FuncaoDados_Codigo ;
      private String[] H00A73_A183FuncaoAPF_Ativo ;
      private int[] H00A73_A165FuncaoAPF_Codigo ;
      private String[] H00A74_A183FuncaoAPF_Ativo ;
      private String[] H00A74_A166FuncaoAPF_Nome ;
      private String[] H00A74_A184FuncaoAPF_Tipo ;
      private int[] H00A74_A165FuncaoAPF_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8FuncaoAPF_Codigo ;
   }

   public class tabelaconsultawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00A74( IGxContext context ,
                                             int A165FuncaoAPF_Codigo ,
                                             IGxCollection AV8FuncaoAPF_Codigo ,
                                             String A183FuncaoAPF_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_Ativo], [FuncaoAPF_Nome], [FuncaoAPF_Tipo], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV8FuncaoAPF_Codigo, "[FuncaoAPF_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and ([FuncaoAPF_Ativo] = 'A')";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Codigo]";
         GXv_Object4[0] = scmdbuf;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00A74(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00A72 ;
          prmH00A72 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00A73 ;
          prmH00A73 = new Object[] {
          } ;
          Object[] prmH00A74 ;
          prmH00A74 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00A72", "SELECT T1.[Tabela_Codigo], T2.[FuncaoDados_Ativo], T2.[FuncaoDados_Tipo], T2.[FuncaoDados_Nome], T2.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo] FROM ([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoDados_Codigo]) WHERE (T1.[Tabela_Codigo] = @Tabela_Codigo) AND (T2.[FuncaoDados_Ativo] = 'A') ORDER BY T1.[Tabela_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A72,11,0,true,false )
             ,new CursorDef("H00A73", "SELECT DISTINCT NULL AS [FuncaoAPF_Ativo], [FuncaoAPF_Codigo] FROM ( SELECT TOP(100) PERCENT T2.[FuncaoAPF_Ativo], T1.[FuncaoAPF_Codigo] FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) WHERE T2.[FuncaoAPF_Ativo] = 'A' ORDER BY T1.[FuncaoAPF_Codigo]) DistinctT ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A73,100,0,false,false )
             ,new CursorDef("H00A74", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00A74,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
