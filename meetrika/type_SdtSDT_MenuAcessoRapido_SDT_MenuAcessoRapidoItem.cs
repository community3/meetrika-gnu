/*
               File: type_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem
        Description: SDT_MenuAcessoRapido
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:43.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem" )]
   [XmlType(TypeName =  "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem : GxUserType
   {
      public SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao = "";
      }

      public SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem obj ;
         obj = this;
         obj.gxTpr_Menu_codigo = deserialized.gxTpr_Menu_codigo;
         obj.gxTpr_Menu_nome = deserialized.gxTpr_Menu_nome;
         obj.gxTpr_Menu_link = deserialized.gxTpr_Menu_link;
         obj.gxTpr_Menu_imagem = deserialized.gxTpr_Menu_imagem;
         obj.gxTpr_Menu_imagem_gxi = deserialized.gxTpr_Menu_imagem_gxi;
         obj.gxTpr_Menu_descricao = deserialized.gxTpr_Menu_descricao;
         obj.gxTpr_Menu_ordem = deserialized.gxTpr_Menu_ordem;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Codigo") )
               {
                  gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Nome") )
               {
                  gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Link") )
               {
                  gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem") )
               {
                  gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_GXI") )
               {
                  gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Descricao") )
               {
                  gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ordem") )
               {
                  gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Menu_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Menu_Nome", StringUtil.RTrim( gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Menu_Link", StringUtil.RTrim( gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Menu_Imagem", StringUtil.RTrim( gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Menu_Imagem_GXI", StringUtil.RTrim( gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Menu_Descricao", StringUtil.RTrim( gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Menu_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Menu_Codigo", gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_codigo, false);
         AddObjectProperty("Menu_Nome", gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome, false);
         AddObjectProperty("Menu_Link", gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link, false);
         AddObjectProperty("Menu_Imagem", gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem, false);
         AddObjectProperty("Menu_Imagem_GXI", gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi, false);
         AddObjectProperty("Menu_Descricao", gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao, false);
         AddObjectProperty("Menu_Ordem", gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_ordem, false);
         return  ;
      }

      [  SoapElement( ElementName = "Menu_Codigo" )]
      [  XmlElement( ElementName = "Menu_Codigo"   )]
      public int gxTpr_Menu_codigo
      {
         get {
            return gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_codigo ;
         }

         set {
            gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Nome" )]
      [  XmlElement( ElementName = "Menu_Nome"   )]
      public String gxTpr_Menu_nome
      {
         get {
            return gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome ;
         }

         set {
            gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Link" )]
      [  XmlElement( ElementName = "Menu_Link"   )]
      public String gxTpr_Menu_link
      {
         get {
            return gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link ;
         }

         set {
            gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Imagem" )]
      [  XmlElement( ElementName = "Menu_Imagem"   )]
      [GxUpload()]
      public String gxTpr_Menu_imagem
      {
         get {
            return gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem ;
         }

         set {
            gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Imagem_GXI" )]
      [  XmlElement( ElementName = "Menu_Imagem_GXI"   )]
      public String gxTpr_Menu_imagem_gxi
      {
         get {
            return gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi ;
         }

         set {
            gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Descricao" )]
      [  XmlElement( ElementName = "Menu_Descricao"   )]
      public String gxTpr_Menu_descricao
      {
         get {
            return gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao ;
         }

         set {
            gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Ordem" )]
      [  XmlElement( ElementName = "Menu_Ordem"   )]
      public short gxTpr_Menu_ordem
      {
         get {
            return gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_ordem ;
         }

         set {
            gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_ordem = (short)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao = "";
         gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_ordem = 0;
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_ordem ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_codigo ;
      protected String gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_nome ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_link ;
      protected String gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem_gxi ;
      protected String gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_descricao ;
      protected String gxTv_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_Menu_imagem ;
   }

   [DataContract(Name = @"SDT_MenuAcessoRapido.SDT_MenuAcessoRapidoItem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_RESTInterface : GxGenericCollectionItem<SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_RESTInterface( ) : base()
      {
      }

      public SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem_RESTInterface( SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Menu_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Menu_codigo
      {
         get {
            return sdt.gxTpr_Menu_codigo ;
         }

         set {
            sdt.gxTpr_Menu_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_Nome" , Order = 1 )]
      public String gxTpr_Menu_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Menu_nome) ;
         }

         set {
            sdt.gxTpr_Menu_nome = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Link" , Order = 2 )]
      public String gxTpr_Menu_link
      {
         get {
            return sdt.gxTpr_Menu_link ;
         }

         set {
            sdt.gxTpr_Menu_link = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Imagem" , Order = 3 )]
      [GxUpload()]
      public String gxTpr_Menu_imagem
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Menu_imagem)) ? PathUtil.RelativePath( sdt.gxTpr_Menu_imagem) : StringUtil.RTrim( sdt.gxTpr_Menu_imagem_gxi)) ;
         }

         set {
            sdt.gxTpr_Menu_imagem = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Descricao" , Order = 5 )]
      public String gxTpr_Menu_descricao
      {
         get {
            return sdt.gxTpr_Menu_descricao ;
         }

         set {
            sdt.gxTpr_Menu_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Ordem" , Order = 6 )]
      public Nullable<short> gxTpr_Menu_ordem
      {
         get {
            return sdt.gxTpr_Menu_ordem ;
         }

         set {
            sdt.gxTpr_Menu_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem sdt
      {
         get {
            return (SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem() ;
         }
      }

   }

}
