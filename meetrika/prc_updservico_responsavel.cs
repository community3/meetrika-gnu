/*
               File: PRC_UpdServico_Responsavel
        Description: Upd Servico_Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:40.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updservico_responsavel : GXProcedure
   {
      public prc_updservico_responsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updservico_responsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Contratante_Codigo ,
                           int aP2_Servico_Codigo ,
                           int aP3_Usuario_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8Contratante_Codigo = aP1_Contratante_Codigo;
         this.AV9Servico_Codigo = aP2_Servico_Codigo;
         this.AV10Usuario_Codigo = aP3_Usuario_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Gx_mode ,
                                 int aP1_Contratante_Codigo ,
                                 int aP2_Servico_Codigo ,
                                 int aP3_Usuario_Codigo )
      {
         prc_updservico_responsavel objprc_updservico_responsavel;
         objprc_updservico_responsavel = new prc_updservico_responsavel();
         objprc_updservico_responsavel.Gx_mode = aP0_Gx_mode;
         objprc_updservico_responsavel.AV8Contratante_Codigo = aP1_Contratante_Codigo;
         objprc_updservico_responsavel.AV9Servico_Codigo = aP2_Servico_Codigo;
         objprc_updservico_responsavel.AV10Usuario_Codigo = aP3_Usuario_Codigo;
         objprc_updservico_responsavel.context.SetSubmitInitialConfig(context);
         objprc_updservico_responsavel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updservico_responsavel);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updservico_responsavel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV14GXLvl6 = 0;
            /* Using cursor P00BL2 */
            pr_default.execute(0, new Object[] {AV9Servico_Codigo, AV8Contratante_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1552ServicoResponsavel_CteCteCod = P00BL2_A1552ServicoResponsavel_CteCteCod[0];
               n1552ServicoResponsavel_CteCteCod = P00BL2_n1552ServicoResponsavel_CteCteCod[0];
               A1549ServicoResponsavel_SrvCod = P00BL2_A1549ServicoResponsavel_SrvCod[0];
               n1549ServicoResponsavel_SrvCod = P00BL2_n1549ServicoResponsavel_SrvCod[0];
               A1550ServicoResponsavel_CteUsrCod = P00BL2_A1550ServicoResponsavel_CteUsrCod[0];
               n1550ServicoResponsavel_CteUsrCod = P00BL2_n1550ServicoResponsavel_CteUsrCod[0];
               A1548ServicoResponsavel_Codigo = P00BL2_A1548ServicoResponsavel_Codigo[0];
               AV14GXLvl6 = 1;
               if ( (0==AV10Usuario_Codigo) )
               {
                  A1550ServicoResponsavel_CteUsrCod = 0;
                  n1550ServicoResponsavel_CteUsrCod = false;
                  n1550ServicoResponsavel_CteUsrCod = true;
               }
               else
               {
                  A1550ServicoResponsavel_CteUsrCod = AV10Usuario_Codigo;
                  n1550ServicoResponsavel_CteUsrCod = false;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00BL3 */
               pr_default.execute(1, new Object[] {n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod, A1548ServicoResponsavel_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
               if (true) break;
               /* Using cursor P00BL4 */
               pr_default.execute(2, new Object[] {n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod, A1548ServicoResponsavel_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( AV14GXLvl6 == 0 )
            {
               if ( AV10Usuario_Codigo > 0 )
               {
                  /*
                     INSERT RECORD ON TABLE ServicoResponsavel

                  */
                  A1549ServicoResponsavel_SrvCod = AV9Servico_Codigo;
                  n1549ServicoResponsavel_SrvCod = false;
                  A1552ServicoResponsavel_CteCteCod = AV8Contratante_Codigo;
                  n1552ServicoResponsavel_CteCteCod = false;
                  A1550ServicoResponsavel_CteUsrCod = AV10Usuario_Codigo;
                  n1550ServicoResponsavel_CteUsrCod = false;
                  /* Using cursor P00BL5 */
                  pr_default.execute(3, new Object[] {n1549ServicoResponsavel_SrvCod, A1549ServicoResponsavel_SrvCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod, n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
                  A1548ServicoResponsavel_Codigo = P00BL5_A1548ServicoResponsavel_Codigo[0];
                  pr_default.close(3);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
                  if ( (pr_default.getStatus(3) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  /* End Insert */
               }
            }
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            /* Using cursor P00BL6 */
            pr_default.execute(4, new Object[] {AV8Contratante_Codigo, AV10Usuario_Codigo, AV9Servico_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1550ServicoResponsavel_CteUsrCod = P00BL6_A1550ServicoResponsavel_CteUsrCod[0];
               n1550ServicoResponsavel_CteUsrCod = P00BL6_n1550ServicoResponsavel_CteUsrCod[0];
               A1552ServicoResponsavel_CteCteCod = P00BL6_A1552ServicoResponsavel_CteCteCod[0];
               n1552ServicoResponsavel_CteCteCod = P00BL6_n1552ServicoResponsavel_CteCteCod[0];
               A1549ServicoResponsavel_SrvCod = P00BL6_A1549ServicoResponsavel_SrvCod[0];
               n1549ServicoResponsavel_SrvCod = P00BL6_n1549ServicoResponsavel_SrvCod[0];
               A1548ServicoResponsavel_Codigo = P00BL6_A1548ServicoResponsavel_Codigo[0];
               /* Using cursor P00BL7 */
               pr_default.execute(5, new Object[] {A1548ServicoResponsavel_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdServico_Responsavel");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BL2_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         P00BL2_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         P00BL2_A1549ServicoResponsavel_SrvCod = new int[1] ;
         P00BL2_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         P00BL2_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         P00BL2_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         P00BL2_A1548ServicoResponsavel_Codigo = new int[1] ;
         P00BL5_A1548ServicoResponsavel_Codigo = new int[1] ;
         Gx_emsg = "";
         P00BL6_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         P00BL6_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         P00BL6_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         P00BL6_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         P00BL6_A1549ServicoResponsavel_SrvCod = new int[1] ;
         P00BL6_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         P00BL6_A1548ServicoResponsavel_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updservico_responsavel__default(),
            new Object[][] {
                new Object[] {
               P00BL2_A1552ServicoResponsavel_CteCteCod, P00BL2_n1552ServicoResponsavel_CteCteCod, P00BL2_A1549ServicoResponsavel_SrvCod, P00BL2_n1549ServicoResponsavel_SrvCod, P00BL2_A1550ServicoResponsavel_CteUsrCod, P00BL2_n1550ServicoResponsavel_CteUsrCod, P00BL2_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00BL5_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               P00BL6_A1550ServicoResponsavel_CteUsrCod, P00BL6_n1550ServicoResponsavel_CteUsrCod, P00BL6_A1552ServicoResponsavel_CteCteCod, P00BL6_n1552ServicoResponsavel_CteCteCod, P00BL6_A1549ServicoResponsavel_SrvCod, P00BL6_n1549ServicoResponsavel_SrvCod, P00BL6_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14GXLvl6 ;
      private int AV8Contratante_Codigo ;
      private int AV9Servico_Codigo ;
      private int AV10Usuario_Codigo ;
      private int A1552ServicoResponsavel_CteCteCod ;
      private int A1549ServicoResponsavel_SrvCod ;
      private int A1550ServicoResponsavel_CteUsrCod ;
      private int A1548ServicoResponsavel_Codigo ;
      private int GX_INS177 ;
      private String Gx_mode ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private bool n1552ServicoResponsavel_CteCteCod ;
      private bool n1549ServicoResponsavel_SrvCod ;
      private bool n1550ServicoResponsavel_CteUsrCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BL2_A1552ServicoResponsavel_CteCteCod ;
      private bool[] P00BL2_n1552ServicoResponsavel_CteCteCod ;
      private int[] P00BL2_A1549ServicoResponsavel_SrvCod ;
      private bool[] P00BL2_n1549ServicoResponsavel_SrvCod ;
      private int[] P00BL2_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] P00BL2_n1550ServicoResponsavel_CteUsrCod ;
      private int[] P00BL2_A1548ServicoResponsavel_Codigo ;
      private int[] P00BL5_A1548ServicoResponsavel_Codigo ;
      private int[] P00BL6_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] P00BL6_n1550ServicoResponsavel_CteUsrCod ;
      private int[] P00BL6_A1552ServicoResponsavel_CteCteCod ;
      private bool[] P00BL6_n1552ServicoResponsavel_CteCteCod ;
      private int[] P00BL6_A1549ServicoResponsavel_SrvCod ;
      private bool[] P00BL6_n1549ServicoResponsavel_SrvCod ;
      private int[] P00BL6_A1548ServicoResponsavel_Codigo ;
   }

   public class prc_updservico_responsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BL2 ;
          prmP00BL2 = new Object[] {
          new Object[] {"@AV9Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BL3 ;
          prmP00BL3 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BL4 ;
          prmP00BL4 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BL5 ;
          prmP00BL5 = new Object[] {
          new Object[] {"@ServicoResponsavel_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BL6 ;
          prmP00BL6 = new Object[] {
          new Object[] {"@AV8Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BL7 ;
          prmP00BL7 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BL2", "SELECT TOP 1 [ServicoResponsavel_CteCteCod], [ServicoResponsavel_SrvCod], [ServicoResponsavel_CteUsrCod], [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (UPDLOCK) WHERE ([ServicoResponsavel_SrvCod] = @AV9Servico_Codigo) AND ([ServicoResponsavel_CteCteCod] = @AV8Contratante_Codigo) ORDER BY [ServicoResponsavel_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BL2,1,0,true,true )
             ,new CursorDef("P00BL3", "UPDATE [ServicoResponsavel] SET [ServicoResponsavel_CteUsrCod]=@ServicoResponsavel_CteUsrCod  WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BL3)
             ,new CursorDef("P00BL4", "UPDATE [ServicoResponsavel] SET [ServicoResponsavel_CteUsrCod]=@ServicoResponsavel_CteUsrCod  WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BL4)
             ,new CursorDef("P00BL5", "INSERT INTO [ServicoResponsavel]([ServicoResponsavel_SrvCod], [ServicoResponsavel_CteUsrCod], [ServicoResponsavel_CteCteCod]) VALUES(@ServicoResponsavel_SrvCod, @ServicoResponsavel_CteUsrCod, @ServicoResponsavel_CteCteCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00BL5)
             ,new CursorDef("P00BL6", "SELECT [ServicoResponsavel_CteUsrCod], [ServicoResponsavel_CteCteCod], [ServicoResponsavel_SrvCod], [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (UPDLOCK) WHERE ([ServicoResponsavel_CteCteCod] = @AV8Contratante_Codigo and [ServicoResponsavel_CteUsrCod] = @AV10Usuario_Codigo) AND ([ServicoResponsavel_SrvCod] = @AV9Servico_Codigo) ORDER BY [ServicoResponsavel_CteCteCod], [ServicoResponsavel_CteUsrCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BL6,1,0,true,false )
             ,new CursorDef("P00BL7", "DELETE FROM [ServicoResponsavel]  WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BL7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
