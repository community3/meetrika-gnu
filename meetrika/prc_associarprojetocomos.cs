/*
               File: PRC_AssociarProjetoComOS
        Description: Associar Projeto Com OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:36.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_associarprojetocomos : GXProcedure
   {
      public prc_associarprojetocomos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_associarprojetocomos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Projeto_Codigo ,
                           ref int aP1_ContagemResultado_Codigo )
      {
         this.AV8Projeto_Codigo = aP0_Projeto_Codigo;
         this.A456ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP1_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public int executeUdp( int aP0_Projeto_Codigo )
      {
         this.AV8Projeto_Codigo = aP0_Projeto_Codigo;
         this.A456ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP1_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         return A456ContagemResultado_Codigo ;
      }

      public void executeSubmit( int aP0_Projeto_Codigo ,
                                 ref int aP1_ContagemResultado_Codigo )
      {
         prc_associarprojetocomos objprc_associarprojetocomos;
         objprc_associarprojetocomos = new prc_associarprojetocomos();
         objprc_associarprojetocomos.AV8Projeto_Codigo = aP0_Projeto_Codigo;
         objprc_associarprojetocomos.A456ContagemResultado_Codigo = aP1_ContagemResultado_Codigo;
         objprc_associarprojetocomos.context.SetSubmitInitialConfig(context);
         objprc_associarprojetocomos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_associarprojetocomos);
         aP1_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_associarprojetocomos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00B52 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1544ContagemResultado_ProjetoCod = P00B52_A1544ContagemResultado_ProjetoCod[0];
            n1544ContagemResultado_ProjetoCod = P00B52_n1544ContagemResultado_ProjetoCod[0];
            A1544ContagemResultado_ProjetoCod = AV8Projeto_Codigo;
            n1544ContagemResultado_ProjetoCod = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00B53 */
            pr_default.execute(1, new Object[] {n1544ContagemResultado_ProjetoCod, A1544ContagemResultado_ProjetoCod, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00B54 */
            pr_default.execute(2, new Object[] {n1544ContagemResultado_ProjetoCod, A1544ContagemResultado_ProjetoCod, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AssociarProjetoComOS");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00B52_A456ContagemResultado_Codigo = new int[1] ;
         P00B52_A1544ContagemResultado_ProjetoCod = new int[1] ;
         P00B52_n1544ContagemResultado_ProjetoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_associarprojetocomos__default(),
            new Object[][] {
                new Object[] {
               P00B52_A456ContagemResultado_Codigo, P00B52_A1544ContagemResultado_ProjetoCod, P00B52_n1544ContagemResultado_ProjetoCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Projeto_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A1544ContagemResultado_ProjetoCod ;
      private String scmdbuf ;
      private bool n1544ContagemResultado_ProjetoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP1_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00B52_A456ContagemResultado_Codigo ;
      private int[] P00B52_A1544ContagemResultado_ProjetoCod ;
      private bool[] P00B52_n1544ContagemResultado_ProjetoCod ;
   }

   public class prc_associarprojetocomos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00B52 ;
          prmP00B52 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B53 ;
          prmP00B53 = new Object[] {
          new Object[] {"@ContagemResultado_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B54 ;
          prmP00B54 = new Object[] {
          new Object[] {"@ContagemResultado_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00B52", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_ProjetoCod] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B52,1,0,true,true )
             ,new CursorDef("P00B53", "UPDATE [ContagemResultado] SET [ContagemResultado_ProjetoCod]=@ContagemResultado_ProjetoCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B53)
             ,new CursorDef("P00B54", "UPDATE [ContagemResultado] SET [ContagemResultado_ProjetoCod]=@ContagemResultado_ProjetoCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B54)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
