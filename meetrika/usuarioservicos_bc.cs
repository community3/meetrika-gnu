/*
               File: UsuarioServicos_BC
        Description: Usuario Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:14.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuarioservicos_bc : GXHttpHandler, IGxSilentTrn
   {
      public usuarioservicos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuarioservicos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow2M102( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey2M102( ) ;
         standaloneModal( ) ;
         AddRow2M102( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
               Z829UsuarioServicos_ServicoCod = A829UsuarioServicos_ServicoCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_2M0( )
      {
         BeforeValidate2M102( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2M102( ) ;
            }
            else
            {
               CheckExtendedTable2M102( ) ;
               if ( AnyError == 0 )
               {
                  ZM2M102( 2) ;
                  ZM2M102( 3) ;
               }
               CloseExtendedTableCursors2M102( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM2M102( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1517UsuarioServicos_TmpEstAnl = A1517UsuarioServicos_TmpEstAnl;
            Z1513UsuarioServicos_TmpEstExc = A1513UsuarioServicos_TmpEstExc;
            Z1514UsuarioServicos_TmpEstCrr = A1514UsuarioServicos_TmpEstCrr;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z831UsuarioServicos_ServicoSigla = A831UsuarioServicos_ServicoSigla;
            Z832UsuarioServicos_ServicoAtivo = A832UsuarioServicos_ServicoAtivo;
         }
         if ( GX_JID == -1 )
         {
            Z1517UsuarioServicos_TmpEstAnl = A1517UsuarioServicos_TmpEstAnl;
            Z1513UsuarioServicos_TmpEstExc = A1513UsuarioServicos_TmpEstExc;
            Z1514UsuarioServicos_TmpEstCrr = A1514UsuarioServicos_TmpEstCrr;
            Z828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
            Z829UsuarioServicos_ServicoCod = A829UsuarioServicos_ServicoCod;
            Z831UsuarioServicos_ServicoSigla = A831UsuarioServicos_ServicoSigla;
            Z832UsuarioServicos_ServicoAtivo = A832UsuarioServicos_ServicoAtivo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load2M102( )
      {
         /* Using cursor BC002M6 */
         pr_default.execute(4, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound102 = 1;
            A831UsuarioServicos_ServicoSigla = BC002M6_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = BC002M6_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = BC002M6_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = BC002M6_n832UsuarioServicos_ServicoAtivo[0];
            A1517UsuarioServicos_TmpEstAnl = BC002M6_A1517UsuarioServicos_TmpEstAnl[0];
            n1517UsuarioServicos_TmpEstAnl = BC002M6_n1517UsuarioServicos_TmpEstAnl[0];
            A1513UsuarioServicos_TmpEstExc = BC002M6_A1513UsuarioServicos_TmpEstExc[0];
            n1513UsuarioServicos_TmpEstExc = BC002M6_n1513UsuarioServicos_TmpEstExc[0];
            A1514UsuarioServicos_TmpEstCrr = BC002M6_A1514UsuarioServicos_TmpEstCrr[0];
            n1514UsuarioServicos_TmpEstCrr = BC002M6_n1514UsuarioServicos_TmpEstCrr[0];
            ZM2M102( -1) ;
         }
         pr_default.close(4);
         OnLoadActions2M102( ) ;
      }

      protected void OnLoadActions2M102( )
      {
      }

      protected void CheckExtendedTable2M102( )
      {
         standaloneModal( ) ;
         /* Using cursor BC002M4 */
         pr_default.execute(2, new Object[] {A828UsuarioServicos_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC002M5 */
         pr_default.execute(3, new Object[] {A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario Servicos_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_SERVICOCOD");
            AnyError = 1;
         }
         A831UsuarioServicos_ServicoSigla = BC002M5_A831UsuarioServicos_ServicoSigla[0];
         n831UsuarioServicos_ServicoSigla = BC002M5_n831UsuarioServicos_ServicoSigla[0];
         A832UsuarioServicos_ServicoAtivo = BC002M5_A832UsuarioServicos_ServicoAtivo[0];
         n832UsuarioServicos_ServicoAtivo = BC002M5_n832UsuarioServicos_ServicoAtivo[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors2M102( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2M102( )
      {
         /* Using cursor BC002M7 */
         pr_default.execute(5, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound102 = 1;
         }
         else
         {
            RcdFound102 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC002M3 */
         pr_default.execute(1, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2M102( 1) ;
            RcdFound102 = 1;
            A1517UsuarioServicos_TmpEstAnl = BC002M3_A1517UsuarioServicos_TmpEstAnl[0];
            n1517UsuarioServicos_TmpEstAnl = BC002M3_n1517UsuarioServicos_TmpEstAnl[0];
            A1513UsuarioServicos_TmpEstExc = BC002M3_A1513UsuarioServicos_TmpEstExc[0];
            n1513UsuarioServicos_TmpEstExc = BC002M3_n1513UsuarioServicos_TmpEstExc[0];
            A1514UsuarioServicos_TmpEstCrr = BC002M3_A1514UsuarioServicos_TmpEstCrr[0];
            n1514UsuarioServicos_TmpEstCrr = BC002M3_n1514UsuarioServicos_TmpEstCrr[0];
            A828UsuarioServicos_UsuarioCod = BC002M3_A828UsuarioServicos_UsuarioCod[0];
            A829UsuarioServicos_ServicoCod = BC002M3_A829UsuarioServicos_ServicoCod[0];
            Z828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
            Z829UsuarioServicos_ServicoCod = A829UsuarioServicos_ServicoCod;
            sMode102 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load2M102( ) ;
            if ( AnyError == 1 )
            {
               RcdFound102 = 0;
               InitializeNonKey2M102( ) ;
            }
            Gx_mode = sMode102;
         }
         else
         {
            RcdFound102 = 0;
            InitializeNonKey2M102( ) ;
            sMode102 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode102;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_2M0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency2M102( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC002M2 */
            pr_default.execute(0, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioServicos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1517UsuarioServicos_TmpEstAnl != BC002M2_A1517UsuarioServicos_TmpEstAnl[0] ) || ( Z1513UsuarioServicos_TmpEstExc != BC002M2_A1513UsuarioServicos_TmpEstExc[0] ) || ( Z1514UsuarioServicos_TmpEstCrr != BC002M2_A1514UsuarioServicos_TmpEstCrr[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"UsuarioServicos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2M102( )
      {
         BeforeValidate2M102( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2M102( 0) ;
            CheckOptimisticConcurrency2M102( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2M102( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2M102( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002M8 */
                     pr_default.execute(6, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, n1514UsuarioServicos_TmpEstCrr, A1514UsuarioServicos_TmpEstCrr, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2M102( ) ;
            }
            EndLevel2M102( ) ;
         }
         CloseExtendedTableCursors2M102( ) ;
      }

      protected void Update2M102( )
      {
         BeforeValidate2M102( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2M102( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2M102( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2M102( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002M9 */
                     pr_default.execute(7, new Object[] {n1517UsuarioServicos_TmpEstAnl, A1517UsuarioServicos_TmpEstAnl, n1513UsuarioServicos_TmpEstExc, A1513UsuarioServicos_TmpEstExc, n1514UsuarioServicos_TmpEstCrr, A1514UsuarioServicos_TmpEstCrr, A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioServicos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2M102( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2M102( ) ;
         }
         CloseExtendedTableCursors2M102( ) ;
      }

      protected void DeferredUpdate2M102( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate2M102( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2M102( ) ;
            AfterConfirm2M102( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2M102( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC002M10 */
                  pr_default.execute(8, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode102 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2M102( ) ;
         Gx_mode = sMode102;
      }

      protected void OnDeleteControls2M102( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002M11 */
            pr_default.execute(9, new Object[] {A829UsuarioServicos_ServicoCod});
            A831UsuarioServicos_ServicoSigla = BC002M11_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = BC002M11_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = BC002M11_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = BC002M11_n832UsuarioServicos_ServicoAtivo[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel2M102( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2M102( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2M102( )
      {
         /* Using cursor BC002M12 */
         pr_default.execute(10, new Object[] {A828UsuarioServicos_UsuarioCod, A829UsuarioServicos_ServicoCod});
         RcdFound102 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound102 = 1;
            A831UsuarioServicos_ServicoSigla = BC002M12_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = BC002M12_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = BC002M12_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = BC002M12_n832UsuarioServicos_ServicoAtivo[0];
            A1517UsuarioServicos_TmpEstAnl = BC002M12_A1517UsuarioServicos_TmpEstAnl[0];
            n1517UsuarioServicos_TmpEstAnl = BC002M12_n1517UsuarioServicos_TmpEstAnl[0];
            A1513UsuarioServicos_TmpEstExc = BC002M12_A1513UsuarioServicos_TmpEstExc[0];
            n1513UsuarioServicos_TmpEstExc = BC002M12_n1513UsuarioServicos_TmpEstExc[0];
            A1514UsuarioServicos_TmpEstCrr = BC002M12_A1514UsuarioServicos_TmpEstCrr[0];
            n1514UsuarioServicos_TmpEstCrr = BC002M12_n1514UsuarioServicos_TmpEstCrr[0];
            A828UsuarioServicos_UsuarioCod = BC002M12_A828UsuarioServicos_UsuarioCod[0];
            A829UsuarioServicos_ServicoCod = BC002M12_A829UsuarioServicos_ServicoCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2M102( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound102 = 0;
         ScanKeyLoad2M102( ) ;
      }

      protected void ScanKeyLoad2M102( )
      {
         sMode102 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound102 = 1;
            A831UsuarioServicos_ServicoSigla = BC002M12_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = BC002M12_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = BC002M12_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = BC002M12_n832UsuarioServicos_ServicoAtivo[0];
            A1517UsuarioServicos_TmpEstAnl = BC002M12_A1517UsuarioServicos_TmpEstAnl[0];
            n1517UsuarioServicos_TmpEstAnl = BC002M12_n1517UsuarioServicos_TmpEstAnl[0];
            A1513UsuarioServicos_TmpEstExc = BC002M12_A1513UsuarioServicos_TmpEstExc[0];
            n1513UsuarioServicos_TmpEstExc = BC002M12_n1513UsuarioServicos_TmpEstExc[0];
            A1514UsuarioServicos_TmpEstCrr = BC002M12_A1514UsuarioServicos_TmpEstCrr[0];
            n1514UsuarioServicos_TmpEstCrr = BC002M12_n1514UsuarioServicos_TmpEstCrr[0];
            A828UsuarioServicos_UsuarioCod = BC002M12_A828UsuarioServicos_UsuarioCod[0];
            A829UsuarioServicos_ServicoCod = BC002M12_A829UsuarioServicos_ServicoCod[0];
         }
         Gx_mode = sMode102;
      }

      protected void ScanKeyEnd2M102( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm2M102( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2M102( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2M102( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2M102( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2M102( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2M102( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2M102( )
      {
      }

      protected void AddRow2M102( )
      {
         VarsToRow102( bcUsuarioServicos) ;
      }

      protected void ReadRow2M102( )
      {
         RowToVars102( bcUsuarioServicos, 1) ;
      }

      protected void InitializeNonKey2M102( )
      {
         A831UsuarioServicos_ServicoSigla = "";
         n831UsuarioServicos_ServicoSigla = false;
         A832UsuarioServicos_ServicoAtivo = false;
         n832UsuarioServicos_ServicoAtivo = false;
         A1517UsuarioServicos_TmpEstAnl = 0;
         n1517UsuarioServicos_TmpEstAnl = false;
         A1513UsuarioServicos_TmpEstExc = 0;
         n1513UsuarioServicos_TmpEstExc = false;
         A1514UsuarioServicos_TmpEstCrr = 0;
         n1514UsuarioServicos_TmpEstCrr = false;
         Z1517UsuarioServicos_TmpEstAnl = 0;
         Z1513UsuarioServicos_TmpEstExc = 0;
         Z1514UsuarioServicos_TmpEstCrr = 0;
      }

      protected void InitAll2M102( )
      {
         A828UsuarioServicos_UsuarioCod = 0;
         A829UsuarioServicos_ServicoCod = 0;
         InitializeNonKey2M102( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow102( SdtUsuarioServicos obj102 )
      {
         obj102.gxTpr_Mode = Gx_mode;
         obj102.gxTpr_Usuarioservicos_servicosigla = A831UsuarioServicos_ServicoSigla;
         obj102.gxTpr_Usuarioservicos_servicoativo = A832UsuarioServicos_ServicoAtivo;
         obj102.gxTpr_Usuarioservicos_tmpestanl = A1517UsuarioServicos_TmpEstAnl;
         obj102.gxTpr_Usuarioservicos_tmpestexc = A1513UsuarioServicos_TmpEstExc;
         obj102.gxTpr_Usuarioservicos_tmpestcrr = A1514UsuarioServicos_TmpEstCrr;
         obj102.gxTpr_Usuarioservicos_usuariocod = A828UsuarioServicos_UsuarioCod;
         obj102.gxTpr_Usuarioservicos_servicocod = A829UsuarioServicos_ServicoCod;
         obj102.gxTpr_Usuarioservicos_usuariocod_Z = Z828UsuarioServicos_UsuarioCod;
         obj102.gxTpr_Usuarioservicos_servicocod_Z = Z829UsuarioServicos_ServicoCod;
         obj102.gxTpr_Usuarioservicos_servicosigla_Z = Z831UsuarioServicos_ServicoSigla;
         obj102.gxTpr_Usuarioservicos_servicoativo_Z = Z832UsuarioServicos_ServicoAtivo;
         obj102.gxTpr_Usuarioservicos_tmpestanl_Z = Z1517UsuarioServicos_TmpEstAnl;
         obj102.gxTpr_Usuarioservicos_tmpestexc_Z = Z1513UsuarioServicos_TmpEstExc;
         obj102.gxTpr_Usuarioservicos_tmpestcrr_Z = Z1514UsuarioServicos_TmpEstCrr;
         obj102.gxTpr_Usuarioservicos_servicosigla_N = (short)(Convert.ToInt16(n831UsuarioServicos_ServicoSigla));
         obj102.gxTpr_Usuarioservicos_servicoativo_N = (short)(Convert.ToInt16(n832UsuarioServicos_ServicoAtivo));
         obj102.gxTpr_Usuarioservicos_tmpestanl_N = (short)(Convert.ToInt16(n1517UsuarioServicos_TmpEstAnl));
         obj102.gxTpr_Usuarioservicos_tmpestexc_N = (short)(Convert.ToInt16(n1513UsuarioServicos_TmpEstExc));
         obj102.gxTpr_Usuarioservicos_tmpestcrr_N = (short)(Convert.ToInt16(n1514UsuarioServicos_TmpEstCrr));
         obj102.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow102( SdtUsuarioServicos obj102 )
      {
         obj102.gxTpr_Usuarioservicos_usuariocod = A828UsuarioServicos_UsuarioCod;
         obj102.gxTpr_Usuarioservicos_servicocod = A829UsuarioServicos_ServicoCod;
         return  ;
      }

      public void RowToVars102( SdtUsuarioServicos obj102 ,
                                int forceLoad )
      {
         Gx_mode = obj102.gxTpr_Mode;
         A831UsuarioServicos_ServicoSigla = obj102.gxTpr_Usuarioservicos_servicosigla;
         n831UsuarioServicos_ServicoSigla = false;
         A832UsuarioServicos_ServicoAtivo = obj102.gxTpr_Usuarioservicos_servicoativo;
         n832UsuarioServicos_ServicoAtivo = false;
         A1517UsuarioServicos_TmpEstAnl = obj102.gxTpr_Usuarioservicos_tmpestanl;
         n1517UsuarioServicos_TmpEstAnl = false;
         A1513UsuarioServicos_TmpEstExc = obj102.gxTpr_Usuarioservicos_tmpestexc;
         n1513UsuarioServicos_TmpEstExc = false;
         A1514UsuarioServicos_TmpEstCrr = obj102.gxTpr_Usuarioservicos_tmpestcrr;
         n1514UsuarioServicos_TmpEstCrr = false;
         A828UsuarioServicos_UsuarioCod = obj102.gxTpr_Usuarioservicos_usuariocod;
         A829UsuarioServicos_ServicoCod = obj102.gxTpr_Usuarioservicos_servicocod;
         Z828UsuarioServicos_UsuarioCod = obj102.gxTpr_Usuarioservicos_usuariocod_Z;
         Z829UsuarioServicos_ServicoCod = obj102.gxTpr_Usuarioservicos_servicocod_Z;
         Z831UsuarioServicos_ServicoSigla = obj102.gxTpr_Usuarioservicos_servicosigla_Z;
         Z832UsuarioServicos_ServicoAtivo = obj102.gxTpr_Usuarioservicos_servicoativo_Z;
         Z1517UsuarioServicos_TmpEstAnl = obj102.gxTpr_Usuarioservicos_tmpestanl_Z;
         Z1513UsuarioServicos_TmpEstExc = obj102.gxTpr_Usuarioservicos_tmpestexc_Z;
         Z1514UsuarioServicos_TmpEstCrr = obj102.gxTpr_Usuarioservicos_tmpestcrr_Z;
         n831UsuarioServicos_ServicoSigla = (bool)(Convert.ToBoolean(obj102.gxTpr_Usuarioservicos_servicosigla_N));
         n832UsuarioServicos_ServicoAtivo = (bool)(Convert.ToBoolean(obj102.gxTpr_Usuarioservicos_servicoativo_N));
         n1517UsuarioServicos_TmpEstAnl = (bool)(Convert.ToBoolean(obj102.gxTpr_Usuarioservicos_tmpestanl_N));
         n1513UsuarioServicos_TmpEstExc = (bool)(Convert.ToBoolean(obj102.gxTpr_Usuarioservicos_tmpestexc_N));
         n1514UsuarioServicos_TmpEstCrr = (bool)(Convert.ToBoolean(obj102.gxTpr_Usuarioservicos_tmpestcrr_N));
         Gx_mode = obj102.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A828UsuarioServicos_UsuarioCod = (int)getParm(obj,0);
         A829UsuarioServicos_ServicoCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey2M102( ) ;
         ScanKeyStart2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC002M13 */
            pr_default.execute(11, new Object[] {A828UsuarioServicos_UsuarioCod});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Usuario_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC002M11 */
            pr_default.execute(9, new Object[] {A829UsuarioServicos_ServicoCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Usuario Servicos_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_SERVICOCOD");
               AnyError = 1;
            }
            A831UsuarioServicos_ServicoSigla = BC002M11_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = BC002M11_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = BC002M11_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = BC002M11_n832UsuarioServicos_ServicoAtivo[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
            Z829UsuarioServicos_ServicoCod = A829UsuarioServicos_ServicoCod;
         }
         ZM2M102( -1) ;
         OnLoadActions2M102( ) ;
         AddRow2M102( ) ;
         ScanKeyEnd2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars102( bcUsuarioServicos, 0) ;
         ScanKeyStart2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC002M13 */
            pr_default.execute(11, new Object[] {A828UsuarioServicos_UsuarioCod});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Usuario_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_USUARIOCOD");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC002M11 */
            pr_default.execute(9, new Object[] {A829UsuarioServicos_ServicoCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Usuario Servicos_Servicos'.", "ForeignKeyNotFound", 1, "USUARIOSERVICOS_SERVICOCOD");
               AnyError = 1;
            }
            A831UsuarioServicos_ServicoSigla = BC002M11_A831UsuarioServicos_ServicoSigla[0];
            n831UsuarioServicos_ServicoSigla = BC002M11_n831UsuarioServicos_ServicoSigla[0];
            A832UsuarioServicos_ServicoAtivo = BC002M11_A832UsuarioServicos_ServicoAtivo[0];
            n832UsuarioServicos_ServicoAtivo = BC002M11_n832UsuarioServicos_ServicoAtivo[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z828UsuarioServicos_UsuarioCod = A828UsuarioServicos_UsuarioCod;
            Z829UsuarioServicos_ServicoCod = A829UsuarioServicos_ServicoCod;
         }
         ZM2M102( -1) ;
         OnLoadActions2M102( ) ;
         AddRow2M102( ) ;
         ScanKeyEnd2M102( ) ;
         if ( RcdFound102 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars102( bcUsuarioServicos, 0) ;
         nKeyPressed = 1;
         GetKey2M102( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert2M102( ) ;
         }
         else
         {
            if ( RcdFound102 == 1 )
            {
               if ( ( A828UsuarioServicos_UsuarioCod != Z828UsuarioServicos_UsuarioCod ) || ( A829UsuarioServicos_ServicoCod != Z829UsuarioServicos_ServicoCod ) )
               {
                  A828UsuarioServicos_UsuarioCod = Z828UsuarioServicos_UsuarioCod;
                  A829UsuarioServicos_ServicoCod = Z829UsuarioServicos_ServicoCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update2M102( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A828UsuarioServicos_UsuarioCod != Z828UsuarioServicos_UsuarioCod ) || ( A829UsuarioServicos_ServicoCod != Z829UsuarioServicos_ServicoCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2M102( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2M102( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow102( bcUsuarioServicos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars102( bcUsuarioServicos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey2M102( ) ;
         if ( RcdFound102 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A828UsuarioServicos_UsuarioCod != Z828UsuarioServicos_UsuarioCod ) || ( A829UsuarioServicos_ServicoCod != Z829UsuarioServicos_ServicoCod ) )
            {
               A828UsuarioServicos_UsuarioCod = Z828UsuarioServicos_UsuarioCod;
               A829UsuarioServicos_ServicoCod = Z829UsuarioServicos_ServicoCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A828UsuarioServicos_UsuarioCod != Z828UsuarioServicos_UsuarioCod ) || ( A829UsuarioServicos_ServicoCod != Z829UsuarioServicos_ServicoCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
         context.RollbackDataStores( "UsuarioServicos_BC");
         VarsToRow102( bcUsuarioServicos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcUsuarioServicos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcUsuarioServicos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcUsuarioServicos )
         {
            bcUsuarioServicos = (SdtUsuarioServicos)(sdt);
            if ( StringUtil.StrCmp(bcUsuarioServicos.gxTpr_Mode, "") == 0 )
            {
               bcUsuarioServicos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow102( bcUsuarioServicos) ;
            }
            else
            {
               RowToVars102( bcUsuarioServicos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcUsuarioServicos.gxTpr_Mode, "") == 0 )
            {
               bcUsuarioServicos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars102( bcUsuarioServicos, 1) ;
         return  ;
      }

      public SdtUsuarioServicos UsuarioServicos_BC
      {
         get {
            return bcUsuarioServicos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z831UsuarioServicos_ServicoSigla = "";
         A831UsuarioServicos_ServicoSigla = "";
         BC002M6_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         BC002M6_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         BC002M6_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M6_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M6_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         BC002M6_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         BC002M6_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         BC002M6_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         BC002M6_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         BC002M6_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         BC002M6_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC002M6_A829UsuarioServicos_ServicoCod = new int[1] ;
         BC002M4_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC002M5_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         BC002M5_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         BC002M5_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M5_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M7_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC002M7_A829UsuarioServicos_ServicoCod = new int[1] ;
         BC002M3_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         BC002M3_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         BC002M3_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         BC002M3_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         BC002M3_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         BC002M3_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         BC002M3_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC002M3_A829UsuarioServicos_ServicoCod = new int[1] ;
         sMode102 = "";
         BC002M2_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         BC002M2_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         BC002M2_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         BC002M2_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         BC002M2_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         BC002M2_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         BC002M2_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC002M2_A829UsuarioServicos_ServicoCod = new int[1] ;
         BC002M11_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         BC002M11_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         BC002M11_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M11_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M12_A831UsuarioServicos_ServicoSigla = new String[] {""} ;
         BC002M12_n831UsuarioServicos_ServicoSigla = new bool[] {false} ;
         BC002M12_A832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M12_n832UsuarioServicos_ServicoAtivo = new bool[] {false} ;
         BC002M12_A1517UsuarioServicos_TmpEstAnl = new int[1] ;
         BC002M12_n1517UsuarioServicos_TmpEstAnl = new bool[] {false} ;
         BC002M12_A1513UsuarioServicos_TmpEstExc = new int[1] ;
         BC002M12_n1513UsuarioServicos_TmpEstExc = new bool[] {false} ;
         BC002M12_A1514UsuarioServicos_TmpEstCrr = new int[1] ;
         BC002M12_n1514UsuarioServicos_TmpEstCrr = new bool[] {false} ;
         BC002M12_A828UsuarioServicos_UsuarioCod = new int[1] ;
         BC002M12_A829UsuarioServicos_ServicoCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC002M13_A828UsuarioServicos_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuarioservicos_bc__default(),
            new Object[][] {
                new Object[] {
               BC002M2_A1517UsuarioServicos_TmpEstAnl, BC002M2_n1517UsuarioServicos_TmpEstAnl, BC002M2_A1513UsuarioServicos_TmpEstExc, BC002M2_n1513UsuarioServicos_TmpEstExc, BC002M2_A1514UsuarioServicos_TmpEstCrr, BC002M2_n1514UsuarioServicos_TmpEstCrr, BC002M2_A828UsuarioServicos_UsuarioCod, BC002M2_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               BC002M3_A1517UsuarioServicos_TmpEstAnl, BC002M3_n1517UsuarioServicos_TmpEstAnl, BC002M3_A1513UsuarioServicos_TmpEstExc, BC002M3_n1513UsuarioServicos_TmpEstExc, BC002M3_A1514UsuarioServicos_TmpEstCrr, BC002M3_n1514UsuarioServicos_TmpEstCrr, BC002M3_A828UsuarioServicos_UsuarioCod, BC002M3_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               BC002M4_A828UsuarioServicos_UsuarioCod
               }
               , new Object[] {
               BC002M5_A831UsuarioServicos_ServicoSigla, BC002M5_n831UsuarioServicos_ServicoSigla, BC002M5_A832UsuarioServicos_ServicoAtivo, BC002M5_n832UsuarioServicos_ServicoAtivo
               }
               , new Object[] {
               BC002M6_A831UsuarioServicos_ServicoSigla, BC002M6_n831UsuarioServicos_ServicoSigla, BC002M6_A832UsuarioServicos_ServicoAtivo, BC002M6_n832UsuarioServicos_ServicoAtivo, BC002M6_A1517UsuarioServicos_TmpEstAnl, BC002M6_n1517UsuarioServicos_TmpEstAnl, BC002M6_A1513UsuarioServicos_TmpEstExc, BC002M6_n1513UsuarioServicos_TmpEstExc, BC002M6_A1514UsuarioServicos_TmpEstCrr, BC002M6_n1514UsuarioServicos_TmpEstCrr,
               BC002M6_A828UsuarioServicos_UsuarioCod, BC002M6_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               BC002M7_A828UsuarioServicos_UsuarioCod, BC002M7_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002M11_A831UsuarioServicos_ServicoSigla, BC002M11_n831UsuarioServicos_ServicoSigla, BC002M11_A832UsuarioServicos_ServicoAtivo, BC002M11_n832UsuarioServicos_ServicoAtivo
               }
               , new Object[] {
               BC002M12_A831UsuarioServicos_ServicoSigla, BC002M12_n831UsuarioServicos_ServicoSigla, BC002M12_A832UsuarioServicos_ServicoAtivo, BC002M12_n832UsuarioServicos_ServicoAtivo, BC002M12_A1517UsuarioServicos_TmpEstAnl, BC002M12_n1517UsuarioServicos_TmpEstAnl, BC002M12_A1513UsuarioServicos_TmpEstExc, BC002M12_n1513UsuarioServicos_TmpEstExc, BC002M12_A1514UsuarioServicos_TmpEstCrr, BC002M12_n1514UsuarioServicos_TmpEstCrr,
               BC002M12_A828UsuarioServicos_UsuarioCod, BC002M12_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               BC002M13_A828UsuarioServicos_UsuarioCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound102 ;
      private int trnEnded ;
      private int Z828UsuarioServicos_UsuarioCod ;
      private int A828UsuarioServicos_UsuarioCod ;
      private int Z829UsuarioServicos_ServicoCod ;
      private int A829UsuarioServicos_ServicoCod ;
      private int Z1517UsuarioServicos_TmpEstAnl ;
      private int A1517UsuarioServicos_TmpEstAnl ;
      private int Z1513UsuarioServicos_TmpEstExc ;
      private int A1513UsuarioServicos_TmpEstExc ;
      private int Z1514UsuarioServicos_TmpEstCrr ;
      private int A1514UsuarioServicos_TmpEstCrr ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z831UsuarioServicos_ServicoSigla ;
      private String A831UsuarioServicos_ServicoSigla ;
      private String sMode102 ;
      private bool Z832UsuarioServicos_ServicoAtivo ;
      private bool A832UsuarioServicos_ServicoAtivo ;
      private bool n831UsuarioServicos_ServicoSigla ;
      private bool n832UsuarioServicos_ServicoAtivo ;
      private bool n1517UsuarioServicos_TmpEstAnl ;
      private bool n1513UsuarioServicos_TmpEstExc ;
      private bool n1514UsuarioServicos_TmpEstCrr ;
      private SdtUsuarioServicos bcUsuarioServicos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC002M6_A831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M6_n831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M6_A832UsuarioServicos_ServicoAtivo ;
      private bool[] BC002M6_n832UsuarioServicos_ServicoAtivo ;
      private int[] BC002M6_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] BC002M6_n1517UsuarioServicos_TmpEstAnl ;
      private int[] BC002M6_A1513UsuarioServicos_TmpEstExc ;
      private bool[] BC002M6_n1513UsuarioServicos_TmpEstExc ;
      private int[] BC002M6_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] BC002M6_n1514UsuarioServicos_TmpEstCrr ;
      private int[] BC002M6_A828UsuarioServicos_UsuarioCod ;
      private int[] BC002M6_A829UsuarioServicos_ServicoCod ;
      private int[] BC002M4_A828UsuarioServicos_UsuarioCod ;
      private String[] BC002M5_A831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M5_n831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M5_A832UsuarioServicos_ServicoAtivo ;
      private bool[] BC002M5_n832UsuarioServicos_ServicoAtivo ;
      private int[] BC002M7_A828UsuarioServicos_UsuarioCod ;
      private int[] BC002M7_A829UsuarioServicos_ServicoCod ;
      private int[] BC002M3_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] BC002M3_n1517UsuarioServicos_TmpEstAnl ;
      private int[] BC002M3_A1513UsuarioServicos_TmpEstExc ;
      private bool[] BC002M3_n1513UsuarioServicos_TmpEstExc ;
      private int[] BC002M3_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] BC002M3_n1514UsuarioServicos_TmpEstCrr ;
      private int[] BC002M3_A828UsuarioServicos_UsuarioCod ;
      private int[] BC002M3_A829UsuarioServicos_ServicoCod ;
      private int[] BC002M2_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] BC002M2_n1517UsuarioServicos_TmpEstAnl ;
      private int[] BC002M2_A1513UsuarioServicos_TmpEstExc ;
      private bool[] BC002M2_n1513UsuarioServicos_TmpEstExc ;
      private int[] BC002M2_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] BC002M2_n1514UsuarioServicos_TmpEstCrr ;
      private int[] BC002M2_A828UsuarioServicos_UsuarioCod ;
      private int[] BC002M2_A829UsuarioServicos_ServicoCod ;
      private String[] BC002M11_A831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M11_n831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M11_A832UsuarioServicos_ServicoAtivo ;
      private bool[] BC002M11_n832UsuarioServicos_ServicoAtivo ;
      private String[] BC002M12_A831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M12_n831UsuarioServicos_ServicoSigla ;
      private bool[] BC002M12_A832UsuarioServicos_ServicoAtivo ;
      private bool[] BC002M12_n832UsuarioServicos_ServicoAtivo ;
      private int[] BC002M12_A1517UsuarioServicos_TmpEstAnl ;
      private bool[] BC002M12_n1517UsuarioServicos_TmpEstAnl ;
      private int[] BC002M12_A1513UsuarioServicos_TmpEstExc ;
      private bool[] BC002M12_n1513UsuarioServicos_TmpEstExc ;
      private int[] BC002M12_A1514UsuarioServicos_TmpEstCrr ;
      private bool[] BC002M12_n1514UsuarioServicos_TmpEstCrr ;
      private int[] BC002M12_A828UsuarioServicos_UsuarioCod ;
      private int[] BC002M12_A829UsuarioServicos_ServicoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC002M13_A828UsuarioServicos_UsuarioCod ;
   }

   public class usuarioservicos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC002M6 ;
          prmBC002M6 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M4 ;
          prmBC002M4 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M5 ;
          prmBC002M5 = new Object[] {
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M7 ;
          prmBC002M7 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M3 ;
          prmBC002M3 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M2 ;
          prmBC002M2 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M8 ;
          prmBC002M8 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M9 ;
          prmBC002M9 = new Object[] {
          new Object[] {"@UsuarioServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M10 ;
          prmBC002M10 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M12 ;
          prmBC002M12 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M13 ;
          prmBC002M13 = new Object[] {
          new Object[] {"@UsuarioServicos_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002M11 ;
          prmBC002M11 = new Object[] {
          new Object[] {"@UsuarioServicos_ServicoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC002M2", "SELECT [UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (UPDLOCK) WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M2,1,0,true,false )
             ,new CursorDef("BC002M3", "SELECT [UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M3,1,0,true,false )
             ,new CursorDef("BC002M4", "SELECT [Usuario_Codigo] AS UsuarioServicos_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioServicos_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M4,1,0,true,false )
             ,new CursorDef("BC002M5", "SELECT [Servico_Sigla] AS UsuarioServicos_ServicoSigla, [Servico_Ativo] AS UsuarioServicos_ServicoAtivo FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M5,1,0,true,false )
             ,new CursorDef("BC002M6", "SELECT T2.[Servico_Sigla] AS UsuarioServicos_ServicoSigla, T2.[Servico_Ativo] AS UsuarioServicos_ServicoAtivo, TM1.[UsuarioServicos_TmpEstAnl], TM1.[UsuarioServicos_TmpEstExc], TM1.[UsuarioServicos_TmpEstCrr], TM1.[UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, TM1.[UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM ([UsuarioServicos] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[UsuarioServicos_ServicoCod]) WHERE TM1.[UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod and TM1.[UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod ORDER BY TM1.[UsuarioServicos_UsuarioCod], TM1.[UsuarioServicos_ServicoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M6,100,0,true,false )
             ,new CursorDef("BC002M7", "SELECT [UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, [UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M7,1,0,true,false )
             ,new CursorDef("BC002M8", "INSERT INTO [UsuarioServicos]([UsuarioServicos_TmpEstAnl], [UsuarioServicos_TmpEstExc], [UsuarioServicos_TmpEstCrr], [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod]) VALUES(@UsuarioServicos_TmpEstAnl, @UsuarioServicos_TmpEstExc, @UsuarioServicos_TmpEstCrr, @UsuarioServicos_UsuarioCod, @UsuarioServicos_ServicoCod)", GxErrorMask.GX_NOMASK,prmBC002M8)
             ,new CursorDef("BC002M9", "UPDATE [UsuarioServicos] SET [UsuarioServicos_TmpEstAnl]=@UsuarioServicos_TmpEstAnl, [UsuarioServicos_TmpEstExc]=@UsuarioServicos_TmpEstExc, [UsuarioServicos_TmpEstCrr]=@UsuarioServicos_TmpEstCrr  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK,prmBC002M9)
             ,new CursorDef("BC002M10", "DELETE FROM [UsuarioServicos]  WHERE [UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod AND [UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod", GxErrorMask.GX_NOMASK,prmBC002M10)
             ,new CursorDef("BC002M11", "SELECT [Servico_Sigla] AS UsuarioServicos_ServicoSigla, [Servico_Ativo] AS UsuarioServicos_ServicoAtivo FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @UsuarioServicos_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M11,1,0,true,false )
             ,new CursorDef("BC002M12", "SELECT T2.[Servico_Sigla] AS UsuarioServicos_ServicoSigla, T2.[Servico_Ativo] AS UsuarioServicos_ServicoAtivo, TM1.[UsuarioServicos_TmpEstAnl], TM1.[UsuarioServicos_TmpEstExc], TM1.[UsuarioServicos_TmpEstCrr], TM1.[UsuarioServicos_UsuarioCod] AS UsuarioServicos_UsuarioCod, TM1.[UsuarioServicos_ServicoCod] AS UsuarioServicos_ServicoCod FROM ([UsuarioServicos] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[UsuarioServicos_ServicoCod]) WHERE TM1.[UsuarioServicos_UsuarioCod] = @UsuarioServicos_UsuarioCod and TM1.[UsuarioServicos_ServicoCod] = @UsuarioServicos_ServicoCod ORDER BY TM1.[UsuarioServicos_UsuarioCod], TM1.[UsuarioServicos_ServicoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M12,100,0,true,false )
             ,new CursorDef("BC002M13", "SELECT [Usuario_Codigo] AS UsuarioServicos_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @UsuarioServicos_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002M13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                stmt.SetParameter(5, (int)parms[7]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
