/*
               File: PRC_UpdSrvVlrUndCnt
        Description: Upd Srv Vlr Und Cnt
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:36.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updsrvvlrundcnt : GXProcedure
   {
      public prc_updsrvvlrundcnt( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updsrvvlrundcnt( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo ,
                           decimal aP1_Valor )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8Valor = aP1_Valor;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      public void executeSubmit( ref int aP0_Contrato_Codigo ,
                                 decimal aP1_Valor )
      {
         prc_updsrvvlrundcnt objprc_updsrvvlrundcnt;
         objprc_updsrvvlrundcnt = new prc_updsrvvlrundcnt();
         objprc_updsrvvlrundcnt.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_updsrvvlrundcnt.AV8Valor = aP1_Valor;
         objprc_updsrvvlrundcnt.context.SetSubmitInitialConfig(context);
         objprc_updsrvvlrundcnt.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updsrvvlrundcnt);
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updsrvvlrundcnt)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00B82 */
         pr_default.execute(0, new Object[] {AV8Valor, A74Contrato_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdSrvVlrUndCnt");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updsrvvlrundcnt__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A74Contrato_Codigo ;
      private decimal AV8Valor ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_updsrvvlrundcnt__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00B82 ;
          prmP00B82 = new Object[] {
          new Object[] {"@Servico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00B82", "UPDATE [ContratoServicos] SET [Servico_VlrUnidadeContratada]=@Servico_VlrUnidadeContratada  WHERE [Contrato_Codigo] = @Contrato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B82)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
