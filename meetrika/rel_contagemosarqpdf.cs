/*
               File: REL_ContagemOSArqPDF
        Description: Relat�rio de Contagem OS em Arq PDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:15:3.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemosarqpdf : GXProcedure
   {
      public rel_contagemosarqpdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public rel_contagemosarqpdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           short aP2_OrderedBy ,
                           bool aP3_OrderedDsc ,
                           String aP4_GridStateXML ,
                           int aP5_ContagemResultado_LoteAceite ,
                           String aP6_QrCodePath ,
                           String aP7_QrCodeUrl ,
                           String aP8_Verificador ,
                           String aP9_FileName )
      {
         this.AV99Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV91ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV143OrderedBy = aP2_OrderedBy;
         this.AV144OrderedDsc = aP3_OrderedDsc;
         this.AV129GridStateXML = aP4_GridStateXML;
         this.AV39ContagemResultado_LoteAceite = aP5_ContagemResultado_LoteAceite;
         this.AV162QrCodePath = aP6_QrCodePath;
         this.AV163QrCodeUrl = aP7_QrCodeUrl;
         this.AV174Verificador = aP8_Verificador;
         this.AV125FileName = aP9_FileName;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 short aP2_OrderedBy ,
                                 bool aP3_OrderedDsc ,
                                 String aP4_GridStateXML ,
                                 int aP5_ContagemResultado_LoteAceite ,
                                 String aP6_QrCodePath ,
                                 String aP7_QrCodeUrl ,
                                 String aP8_Verificador ,
                                 String aP9_FileName )
      {
         rel_contagemosarqpdf objrel_contagemosarqpdf;
         objrel_contagemosarqpdf = new rel_contagemosarqpdf();
         objrel_contagemosarqpdf.AV99Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_contagemosarqpdf.AV91ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objrel_contagemosarqpdf.AV143OrderedBy = aP2_OrderedBy;
         objrel_contagemosarqpdf.AV144OrderedDsc = aP3_OrderedDsc;
         objrel_contagemosarqpdf.AV129GridStateXML = aP4_GridStateXML;
         objrel_contagemosarqpdf.AV39ContagemResultado_LoteAceite = aP5_ContagemResultado_LoteAceite;
         objrel_contagemosarqpdf.AV162QrCodePath = aP6_QrCodePath;
         objrel_contagemosarqpdf.AV163QrCodeUrl = aP7_QrCodeUrl;
         objrel_contagemosarqpdf.AV174Verificador = aP8_Verificador;
         objrel_contagemosarqpdf.AV125FileName = aP9_FileName;
         objrel_contagemosarqpdf.context.SetSubmitInitialConfig(context);
         objrel_contagemosarqpdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemosarqpdf);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemosarqpdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(60-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName(AV125FileName) ;
         getPrinter().GxSetDocFormat("PDF") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 9, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV177WWPContext) ;
            /* Using cursor P006X4 */
            pr_default.execute(0, new Object[] {AV39ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P006X4_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P006X4_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = P006X4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P006X4_n1603ContagemResultado_CntCod[0];
               A1604ContagemResultado_CntPrpCod = P006X4_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P006X4_n1604ContagemResultado_CntPrpCod[0];
               A1605ContagemResultado_CntPrpPesCod = P006X4_A1605ContagemResultado_CntPrpPesCod[0];
               n1605ContagemResultado_CntPrpPesCod = P006X4_n1605ContagemResultado_CntPrpPesCod[0];
               A597ContagemResultado_LoteAceiteCod = P006X4_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006X4_n597ContagemResultado_LoteAceiteCod[0];
               A1612ContagemResultado_CntNum = P006X4_A1612ContagemResultado_CntNum[0];
               n1612ContagemResultado_CntNum = P006X4_n1612ContagemResultado_CntNum[0];
               A1606ContagemResultado_CntPrpPesNom = P006X4_A1606ContagemResultado_CntPrpPesNom[0];
               n1606ContagemResultado_CntPrpPesNom = P006X4_n1606ContagemResultado_CntPrpPesNom[0];
               A1624ContagemResultado_DatVgnInc = P006X4_A1624ContagemResultado_DatVgnInc[0];
               n1624ContagemResultado_DatVgnInc = P006X4_n1624ContagemResultado_DatVgnInc[0];
               A39Contratada_Codigo = P006X4_A39Contratada_Codigo[0];
               n39Contratada_Codigo = P006X4_n39Contratada_Codigo[0];
               A1625ContagemResultado_DatIncTA = P006X4_A1625ContagemResultado_DatIncTA[0];
               n1625ContagemResultado_DatIncTA = P006X4_n1625ContagemResultado_DatIncTA[0];
               A456ContagemResultado_Codigo = P006X4_A456ContagemResultado_Codigo[0];
               A1603ContagemResultado_CntCod = P006X4_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P006X4_n1603ContagemResultado_CntCod[0];
               A1604ContagemResultado_CntPrpCod = P006X4_A1604ContagemResultado_CntPrpCod[0];
               n1604ContagemResultado_CntPrpCod = P006X4_n1604ContagemResultado_CntPrpCod[0];
               A1612ContagemResultado_CntNum = P006X4_A1612ContagemResultado_CntNum[0];
               n1612ContagemResultado_CntNum = P006X4_n1612ContagemResultado_CntNum[0];
               A1624ContagemResultado_DatVgnInc = P006X4_A1624ContagemResultado_DatVgnInc[0];
               n1624ContagemResultado_DatVgnInc = P006X4_n1624ContagemResultado_DatVgnInc[0];
               A39Contratada_Codigo = P006X4_A39Contratada_Codigo[0];
               n39Contratada_Codigo = P006X4_n39Contratada_Codigo[0];
               A1605ContagemResultado_CntPrpPesCod = P006X4_A1605ContagemResultado_CntPrpPesCod[0];
               n1605ContagemResultado_CntPrpPesCod = P006X4_n1605ContagemResultado_CntPrpPesCod[0];
               A1606ContagemResultado_CntPrpPesNom = P006X4_A1606ContagemResultado_CntPrpPesNom[0];
               n1606ContagemResultado_CntPrpPesNom = P006X4_n1606ContagemResultado_CntPrpPesNom[0];
               A1625ContagemResultado_DatIncTA = P006X4_A1625ContagemResultado_DatIncTA[0];
               n1625ContagemResultado_DatIncTA = P006X4_n1625ContagemResultado_DatIncTA[0];
               AV107Contrato_Numero = A1612ContagemResultado_CntNum;
               AV160Preposto = StringUtil.Trim( A1606ContagemResultado_CntPrpPesNom);
               AV105Contrato_DataInicioTA = A1625ContagemResultado_DatIncTA;
               if ( (DateTime.MinValue==AV105Contrato_DataInicioTA) )
               {
                  AV106Contrato_DataVigenciaInicio = A1624ContagemResultado_DatVgnInc;
               }
               else
               {
                  AV106Contrato_DataVigenciaInicio = AV105Contrato_DataInicioTA;
               }
               AV192Contratada_Codigo = A39Contratada_Codigo;
               /* Execute user subroutine: 'DADOSCONTRATADA' */
               S141 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            AV170TextoLink = StringUtil.Trim( AV163QrCodeUrl) + ", informando o c�digo " + StringUtil.Trim( AV174Verificador);
            AV8AssinadoDtHr = context.localUtil.DToC( Gx_date, 2, "/") + " em " + StringUtil.Substring( Gx_time, 1, 5) + ", conforme hor�rio oficial de Brasilia";
            AV161QrCode_Image = AV162QrCodePath;
            AV198Qrcode_image_GXI = GeneXus.Utils.GXDbFile.PathToUrl( AV162QrCodePath);
            AV151Paginas = (short)(AV151Paginas+1);
            /* Using cursor P006X5 */
            pr_default.execute(1, new Object[] {AV39ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A596Lote_Codigo = P006X5_A596Lote_Codigo[0];
               A562Lote_Numero = P006X5_A562Lote_Numero[0];
               A844Lote_DataContrato = P006X5_A844Lote_DataContrato[0];
               n844Lote_DataContrato = P006X5_n844Lote_DataContrato[0];
               AV137Length = (short)(StringUtil.Len( A562Lote_Numero)-6);
               AV140Lote_Numero = A562Lote_Numero;
               AV138Lote = StringUtil.Substring( A562Lote_Numero, StringUtil.Len( A562Lote_Numero)-3, 4) + "/" + StringUtil.PadL( StringUtil.Substring( StringUtil.Trim( A562Lote_Numero), 1, AV137Length), 3, "0");
               AV106Contrato_DataVigenciaInicio = A844Lote_DataContrato;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            /* Execute user subroutine: 'PRINTDATALOTE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H6X0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRINTDATALOTE' Routine */
         AV179x = (short)(AV179x+1);
         /* Execute user subroutine: 'PERIODO' */
         S121 ();
         if (returnInSub) return;
         if ( AV104Contratante_OSAutomatica )
         {
            /* Using cursor P006X6 */
            pr_default.execute(2, new Object[] {AV39ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(2) != 101) )
            {
               BRK6X6 = false;
               A597ContagemResultado_LoteAceiteCod = P006X6_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006X6_n597ContagemResultado_LoteAceiteCod[0];
               A1452ContagemResultado_SS = P006X6_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P006X6_n1452ContagemResultado_SS[0];
               A457ContagemResultado_Demanda = P006X6_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P006X6_n457ContagemResultado_Demanda[0];
               A489ContagemResultado_SistemaCod = P006X6_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P006X6_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P006X6_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006X6_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P006X6_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006X6_n515ContagemResultado_SistemaCoord[0];
               A512ContagemResultado_ValorPF = P006X6_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P006X6_n512ContagemResultado_ValorPF[0];
               A456ContagemResultado_Codigo = P006X6_A456ContagemResultado_Codigo[0];
               A509ContagemrResultado_SistemaSigla = P006X6_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006X6_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P006X6_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006X6_n515ContagemResultado_SistemaCoord[0];
               OV191ContagemResultado_SS = AV191ContagemResultado_SS;
               AV147Os_Header = StringUtil.Substring( AV140Lote_Numero, StringUtil.Len( AV140Lote_Numero)-3, 4) + "/" + "000" + StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0));
               AV166Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV168Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
               AV167Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV191ContagemResultado_SS = A1452ContagemResultado_SS;
               while ( (pr_default.getStatus(2) != 101) && ( P006X6_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( P006X6_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
               {
                  BRK6X6 = false;
                  A457ContagemResultado_Demanda = P006X6_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P006X6_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P006X6_A456ContagemResultado_Codigo[0];
                  AV130i = (short)(AV130i+1);
                  BRK6X6 = true;
                  pr_default.readNext(2);
               }
               /* Execute user subroutine: 'CONTAGENSDOSISTEMALOTE' */
               S136 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
               AV186ValorIni = (decimal)(AV183PFInicial*A512ContagemResultado_ValorPF);
               AV173Valor = (decimal)(AV159PFFinal*A512ContagemResultado_ValorPF);
               AV187ValorTotal = (decimal)(AV173Valor+AV186ValorIni);
               if ( ( AV173Valor == Convert.ToDecimal( 0 )) )
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 2), " ", "");
               }
               else if ( AV173Valor < 0.001m )
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 4);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 4), " ", "");
               }
               else if ( AV173Valor < 0.01m )
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 3);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 3), " ", "");
               }
               else
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 2), " ", "");
               }
               if ( ( AV186ValorIni == Convert.ToDecimal( 0 )) )
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 2), " ", "");
               }
               else if ( AV186ValorIni < 0.001m )
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 4);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 4), " ", "");
               }
               else if ( AV186ValorIni < 0.01m )
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 3);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 3), " ", "");
               }
               else
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 2), " ", "");
               }
               if ( AV187ValorTotal < 0.001m )
               {
                  AV188strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV187ValorTotal, 14, 4), " ", "");
               }
               else if ( AV187ValorTotal < 0.01m )
               {
                  AV188strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV187ValorTotal, 14, 3), " ", "");
               }
               else
               {
                  AV188strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV187ValorTotal, 14, 2), " ", "");
               }
               if ( AV183PFInicial + AV159PFFinal < 0.001m )
               {
                  AV189strPFTotal = StringUtil.Str( AV183PFInicial+AV159PFFinal, 14, 4);
               }
               else
               {
                  AV189strPFTotal = StringUtil.Str( AV183PFInicial+AV159PFFinal, 14, 3);
               }
               AV134ImprimindoOS = true;
               H6X0( false, 390) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("1.  Identifica��o da Empresa Contratada ", 17, Gx_line+13, 359, Gx_line+32, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("2. Informa��es sobre os Servi�os a serem Realizados", 17, Gx_line+186, 442, Gx_line+205, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV168Sistema_Sigla, "@!")), 92, Gx_line+204, 275, Gx_line+221, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV111DataInicio, "99/99/99"), 258, Gx_line+313, 328, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV110DataFim, "99/99/99"), 358, Gx_line+313, 428, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV103Contratada_PessoaNom, "@!")), 159, Gx_line+40, 889, Gx_line+57, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102Contratada_PessoaCNPJ, "")), 76, Gx_line+61, 186, Gx_line+78, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV154Pessoa_Endereco, "")), 17, Gx_line+100, 747, Gx_line+117, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV157Pessoa_Telefone, "")), 367, Gx_line+150, 477, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV153Pessoa_CEP, "")), 84, Gx_line+150, 158, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV155Pessoa_Fax, "")), 576, Gx_line+150, 686, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158Pessoa_UF, "@!")), 634, Gx_line+118, 660, Gx_line+135, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV141Municipio_Nome, "@!")), 84, Gx_line+122, 450, Gx_line+139, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV156Pessoa_IE, "99.999.999/999-99999")), 551, Gx_line+68, 698, Gx_line+85, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV175Volume, "")), 17, Gx_line+363, 495, Gx_line+380, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV131Identificacao1, "")), 17, Gx_line+254, 747, Gx_line+271, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV132Identificacao2, "")), 17, Gx_line+271, 747, Gx_line+288, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV133Identificacao3, "")), 17, Gx_line+287, 747, Gx_line+304, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("PF", 619, Gx_line+362, 636, Gx_line+381, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("a", 350, Gx_line+313, 358, Gx_line+332, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Nome da Empresa:", 17, Gx_line+40, 146, Gx_line+58, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Inscri��o Estadual:", 409, Gx_line+68, 538, Gx_line+86, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CNPJ:", 17, Gx_line+61, 63, Gx_line+79, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Endere�o:", 16, Gx_line+82, 93, Gx_line+100, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cidade:", 17, Gx_line+122, 77, Gx_line+140, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("UF:", 592, Gx_line+118, 621, Gx_line+136, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CEP:", 17, Gx_line+150, 68, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Telefone:", 276, Gx_line+150, 355, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Fax:", 517, Gx_line+150, 566, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Sistema:", 17, Gx_line+204, 86, Gx_line+222, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Servi�o:", 17, Gx_line+237, 184, Gx_line+256, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Volume de Servi�o:", 17, Gx_line+347, 150, Gx_line+366, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Per�odo de Execu��o do Servi�o:", 17, Gx_line+313, 234, Gx_line+332, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 1, 245, 245, 245) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV189strPFTotal, "")), 494, Gx_line+361, 612, Gx_line+381, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+162, 781, Gx_line+177, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+0, 781, Gx_line+15, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+390);
               H6X0( false, 354) ;
               getPrinter().GxDrawLine(275, Gx_line+317, 558, Gx_line+317, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("5. Solicitante", 17, Gx_line+150, 192, Gx_line+169, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("4. Gestor da Ordem de Servi�o", 17, Gx_line+100, 308, Gx_line+119, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("3. Local de Execu��o do Servi�o", 17, Gx_line+50, 325, Gx_line+69, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Relat�rios T�cnicos de Contagem de Pontos de Fun��o", 17, Gx_line+17, 326, Gx_line+35, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Minist�rio da Sa�de", 287, Gx_line+67, 420, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("DATASUS - Minist�rio da Sa�de", 100, Gx_line+167, 300, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cristiane Sousa de Oliveira", 17, Gx_line+117, 192, Gx_line+135, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV167Sistema_Coordenacao, "@!")), 473, Gx_line+202, 782, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Gestor Contratual", 328, Gx_line+317, 505, Gx_line+335, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 275, Gx_line+241, 517, Gx_line+260, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Unidade:", 17, Gx_line+167, 87, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Local de Execu��o:", 17, Gx_line+67, 275, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Produtos a Serem Entregues:", 17, Gx_line+0, 217, Gx_line+19, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Respons�vel pela solicita��o: (�rg�o, Secretaria, Coordena��o):", 17, Gx_line+200, 467, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+29, 781, Gx_line+46, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+79, 781, Gx_line+96, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+129, 781, Gx_line+146, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+354);
               H6X0( false, 406) ;
               getPrinter().GxDrawLine(19, Gx_line+188, 782, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(511, Gx_line+52, 511, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(657, Gx_line+51, 657, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+52, 19, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(187, Gx_line+51, 187, Gx_line+189, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(782, Gx_line+51, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(512, Gx_line+235, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+128, 782, Gx_line+128, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(20, Gx_line+80, 782, Gx_line+80, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+51, 782, Gx_line+51, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 17, Gx_line+317, 117, Gx_line+400) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("7. Ciente da Contratada", 17, Gx_line+260, 234, Gx_line+279, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("6. Custo da Ordem de Servi�o", 17, Gx_line+7, 267, Gx_line+26, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Contagem de PF Final", 27, Gx_line+145, 169, Gx_line+163, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 2)", 61, Gx_line+162, 128, Gx_line+180, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("100%", 569, Gx_line+150, 602, Gx_line+168, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("50%", 569, Gx_line+96, 594, Gx_line+114, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 1)", 69, Gx_line+102, 136, Gx_line+120, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Contagem de PF Inicial", 27, Gx_line+85, 177, Gx_line+103, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("O quadro a seguir descreve o custo dos servi�os a serem executados:", 17, Gx_line+31, 413, Gx_line+49, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV182strValor, "")), 675, Gx_line+150, 767, Gx_line+167, 2, 0, 0, 1) ;
               getPrinter().GxDrawText("(*)", 392, Gx_line+96, 417, Gx_line+114, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(*) Este valor j� esta l�quido conforme % ao lado.", 17, Gx_line+217, 325, Gx_line+235, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV185strValorIni, "")), 671, Gx_line+96, 767, Gx_line+113, 2, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("VALOR", 694, Gx_line+57, 752, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("% FASE", 550, Gx_line+57, 617, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("QUANTIDADE DE PONTOS DE FUN��O", 200, Gx_line+57, 475, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("SERVI�O", 58, Gx_line+57, 125, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Declaramos nossa ci�ncia e concord�ncia com rela��o aos servi�os discriminados no item 02.", 17, Gx_line+277, 700, Gx_line+296, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV181strPFFinal, "")), 273, Gx_line+150, 391, Gx_line+170, 1, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV188strValorTotal, "")), 675, Gx_line+203, 768, Gx_line+223, 2, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV184strPFInicial, "")), 273, Gx_line+95, 391, Gx_line+115, 1, 0, 0, 1) ;
               getPrinter().GxAttris("Calibri", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("TOTAL", 542, Gx_line+203, 642, Gx_line+223, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+244, 781, Gx_line+259, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("A autenticidade deste documento pode ser conferida no site", 133, Gx_line+367, 430, Gx_line+381, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(AV161QrCode_Image, 683, Gx_line+317, 773, Gx_line+403) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV170TextoLink, "")), 133, Gx_line+383, 708, Gx_line+398, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV8AssinadoDtHr, "")), 133, Gx_line+333, 447, Gx_line+350, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Documento assinado eletronicamente por", 133, Gx_line+317, 374, Gx_line+333, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV160Preposto, "@!")), 383, Gx_line+317, 1009, Gx_line+334, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+406);
               H6X0( false, 133) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("8. Cancelamento da Ordem de Servi�o", 17, Gx_line+15, 317, Gx_line+34, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Motivo do Cancelamento:", 17, Gx_line+31, 192, Gx_line+49, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor da Ordem de Servi�o:", 150, Gx_line+65, 433, Gx_line+83, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor de Contrato:", 492, Gx_line+65, 725, Gx_line+83, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data:", 25, Gx_line+65, 67, Gx_line+83, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Ci�ncia do Preposto da Contratada:", 400, Gx_line+115, 642, Gx_line+133, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Coordenador Geral Respons�vel:", 25, Gx_line+115, 342, Gx_line+133, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+0, 781, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+133);
               /* Eject command */
               Gx_OldLine = Gx_line;
               Gx_line = (int)(P_lines+1);
               if ( ! BRK6X6 )
               {
                  BRK6X6 = true;
                  pr_default.readNext(2);
               }
            }
            pr_default.close(2);
         }
         else
         {
            /* Using cursor P006X7 */
            pr_default.execute(3, new Object[] {AV39ContagemResultado_LoteAceite});
            while ( (pr_default.getStatus(3) != 101) )
            {
               BRK6X8 = false;
               A597ContagemResultado_LoteAceiteCod = P006X7_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = P006X7_n597ContagemResultado_LoteAceiteCod[0];
               A1452ContagemResultado_SS = P006X7_A1452ContagemResultado_SS[0];
               n1452ContagemResultado_SS = P006X7_n1452ContagemResultado_SS[0];
               A457ContagemResultado_Demanda = P006X7_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = P006X7_n457ContagemResultado_Demanda[0];
               A489ContagemResultado_SistemaCod = P006X7_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P006X7_n489ContagemResultado_SistemaCod[0];
               A509ContagemrResultado_SistemaSigla = P006X7_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006X7_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P006X7_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006X7_n515ContagemResultado_SistemaCoord[0];
               A512ContagemResultado_ValorPF = P006X7_A512ContagemResultado_ValorPF[0];
               n512ContagemResultado_ValorPF = P006X7_n512ContagemResultado_ValorPF[0];
               A456ContagemResultado_Codigo = P006X7_A456ContagemResultado_Codigo[0];
               A509ContagemrResultado_SistemaSigla = P006X7_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = P006X7_n509ContagemrResultado_SistemaSigla[0];
               A515ContagemResultado_SistemaCoord = P006X7_A515ContagemResultado_SistemaCoord[0];
               n515ContagemResultado_SistemaCoord = P006X7_n515ContagemResultado_SistemaCoord[0];
               OV191ContagemResultado_SS = AV191ContagemResultado_SS;
               AV147Os_Header = StringUtil.Substring( AV140Lote_Numero, StringUtil.Len( AV140Lote_Numero)-3, 4) + "/" + "000" + StringUtil.Trim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0));
               AV166Sistema_Codigo = A489ContagemResultado_SistemaCod;
               AV168Sistema_Sigla = A509ContagemrResultado_SistemaSigla;
               AV167Sistema_Coordenacao = A515ContagemResultado_SistemaCoord;
               AV191ContagemResultado_SS = A1452ContagemResultado_SS;
               while ( (pr_default.getStatus(3) != 101) && ( P006X7_A597ContagemResultado_LoteAceiteCod[0] == A597ContagemResultado_LoteAceiteCod ) && ( P006X7_A1452ContagemResultado_SS[0] == A1452ContagemResultado_SS ) )
               {
                  BRK6X8 = false;
                  A457ContagemResultado_Demanda = P006X7_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P006X7_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P006X7_A456ContagemResultado_Codigo[0];
                  AV130i = (short)(AV130i+1);
                  BRK6X8 = true;
                  pr_default.readNext(3);
               }
               /* Execute user subroutine: 'CONTAGENSDOSISTEMALOTE' */
               S136 ();
               if ( returnInSub )
               {
                  pr_default.close(3);
                  returnInSub = true;
                  if (true) return;
               }
               AV186ValorIni = (decimal)(AV183PFInicial*A512ContagemResultado_ValorPF);
               AV173Valor = (decimal)(AV159PFFinal*A512ContagemResultado_ValorPF);
               AV187ValorTotal = (decimal)(AV173Valor+AV186ValorIni);
               if ( ( AV173Valor == Convert.ToDecimal( 0 )) )
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 2), " ", "");
               }
               else if ( AV173Valor < 0.001m )
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 4);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 4), " ", "");
               }
               else if ( AV173Valor < 0.01m )
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 3);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 3), " ", "");
               }
               else
               {
                  AV173Valor = NumberUtil.Round( AV159PFFinal*A512ContagemResultado_ValorPF, 2);
                  AV182strValor = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV173Valor, 14, 2), " ", "");
               }
               if ( ( AV186ValorIni == Convert.ToDecimal( 0 )) )
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 2), " ", "");
               }
               else if ( AV186ValorIni < 0.001m )
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 4);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 4), " ", "");
               }
               else if ( AV186ValorIni < 0.01m )
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 3);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 3), " ", "");
               }
               else
               {
                  AV186ValorIni = NumberUtil.Round( AV183PFInicial*A512ContagemResultado_ValorPF, 2);
                  AV185strValorIni = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV186ValorIni, 14, 2), " ", "");
               }
               if ( AV187ValorTotal < 0.001m )
               {
                  AV188strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV187ValorTotal, 14, 4), " ", "");
               }
               else if ( AV187ValorTotal < 0.01m )
               {
                  AV188strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV187ValorTotal, 14, 3), " ", "");
               }
               else
               {
                  AV188strValorTotal = "R$ " + StringUtil.StringReplace( StringUtil.Str( AV187ValorTotal, 14, 2), " ", "");
               }
               if ( AV183PFInicial + AV159PFFinal < 0.001m )
               {
                  AV189strPFTotal = StringUtil.Str( AV183PFInicial+AV159PFFinal, 14, 4);
               }
               else
               {
                  AV189strPFTotal = StringUtil.Str( AV183PFInicial+AV159PFFinal, 14, 3);
               }
               AV134ImprimindoOS = true;
               H6X0( false, 390) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("1.  Identifica��o da Empresa Contratada ", 17, Gx_line+13, 359, Gx_line+32, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("2. Informa��es sobre os Servi�os a serem Realizados", 17, Gx_line+186, 442, Gx_line+205, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV168Sistema_Sigla, "@!")), 92, Gx_line+204, 275, Gx_line+221, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV111DataInicio, "99/99/99"), 258, Gx_line+313, 328, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV110DataFim, "99/99/99"), 358, Gx_line+313, 428, Gx_line+330, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV103Contratada_PessoaNom, "@!")), 159, Gx_line+40, 889, Gx_line+57, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV102Contratada_PessoaCNPJ, "")), 76, Gx_line+61, 186, Gx_line+78, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV154Pessoa_Endereco, "")), 17, Gx_line+100, 747, Gx_line+117, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV157Pessoa_Telefone, "")), 367, Gx_line+150, 477, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV153Pessoa_CEP, "")), 84, Gx_line+150, 158, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV155Pessoa_Fax, "")), 576, Gx_line+150, 686, Gx_line+167, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV158Pessoa_UF, "@!")), 634, Gx_line+118, 660, Gx_line+135, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV141Municipio_Nome, "@!")), 84, Gx_line+122, 450, Gx_line+139, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV156Pessoa_IE, "99.999.999/999-99999")), 551, Gx_line+68, 698, Gx_line+85, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV175Volume, "")), 17, Gx_line+363, 495, Gx_line+380, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV131Identificacao1, "")), 17, Gx_line+254, 747, Gx_line+271, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV132Identificacao2, "")), 17, Gx_line+271, 747, Gx_line+288, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV133Identificacao3, "")), 17, Gx_line+287, 747, Gx_line+304, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("PF", 619, Gx_line+362, 636, Gx_line+381, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("a", 350, Gx_line+313, 358, Gx_line+332, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Nome da Empresa:", 17, Gx_line+40, 146, Gx_line+58, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Inscri��o Estadual:", 409, Gx_line+68, 538, Gx_line+86, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CNPJ:", 17, Gx_line+61, 63, Gx_line+79, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Endere�o:", 16, Gx_line+82, 93, Gx_line+100, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cidade:", 17, Gx_line+122, 77, Gx_line+140, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("UF:", 592, Gx_line+118, 621, Gx_line+136, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("CEP:", 17, Gx_line+150, 68, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Telefone:", 276, Gx_line+150, 355, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Fax:", 517, Gx_line+150, 566, Gx_line+168, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Sistema:", 17, Gx_line+204, 86, Gx_line+222, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Servi�o:", 17, Gx_line+237, 184, Gx_line+256, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Volume de Servi�o:", 17, Gx_line+347, 150, Gx_line+366, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Per�odo de Execu��o do Servi�o:", 17, Gx_line+313, 234, Gx_line+332, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 1, 245, 245, 245) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV189strPFTotal, "")), 494, Gx_line+361, 612, Gx_line+381, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+162, 781, Gx_line+177, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+0, 781, Gx_line+15, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+390);
               H6X0( false, 354) ;
               getPrinter().GxDrawLine(275, Gx_line+317, 558, Gx_line+317, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("5. Solicitante", 17, Gx_line+150, 192, Gx_line+169, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("4. Gestor da Ordem de Servi�o", 17, Gx_line+100, 308, Gx_line+119, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("3. Local de Execu��o do Servi�o", 17, Gx_line+50, 325, Gx_line+69, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Relat�rios T�cnicos de Contagem de Pontos de Fun��o", 17, Gx_line+17, 326, Gx_line+35, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Minist�rio da Sa�de", 287, Gx_line+67, 420, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("DATASUS - Minist�rio da Sa�de", 100, Gx_line+167, 300, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Cristiane Sousa de Oliveira", 17, Gx_line+117, 192, Gx_line+135, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV167Sistema_Coordenacao, "@!")), 473, Gx_line+202, 782, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Gestor Contratual", 328, Gx_line+317, 505, Gx_line+335, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Bras�lia, ___ de ___________ de ____.", 275, Gx_line+241, 517, Gx_line+260, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Unidade:", 17, Gx_line+167, 87, Gx_line+185, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Identifica��o do Local de Execu��o:", 17, Gx_line+67, 275, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Produtos a Serem Entregues:", 17, Gx_line+0, 217, Gx_line+19, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Respons�vel pela solicita��o: (�rg�o, Secretaria, Coordena��o):", 17, Gx_line+200, 467, Gx_line+219, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+29, 781, Gx_line+46, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+79, 781, Gx_line+96, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+129, 781, Gx_line+146, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+354);
               H6X0( false, 406) ;
               getPrinter().GxDrawLine(19, Gx_line+188, 782, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(511, Gx_line+52, 511, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(657, Gx_line+51, 657, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+52, 19, Gx_line+188, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(187, Gx_line+51, 187, Gx_line+189, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(782, Gx_line+51, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(512, Gx_line+235, 782, Gx_line+235, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+128, 782, Gx_line+128, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(20, Gx_line+80, 782, Gx_line+80, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawLine(19, Gx_line+51, 782, Gx_line+51, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 17, Gx_line+317, 117, Gx_line+400) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("7. Ciente da Contratada", 17, Gx_line+260, 234, Gx_line+279, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("6. Custo da Ordem de Servi�o", 17, Gx_line+7, 267, Gx_line+26, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Contagem de PF Final", 27, Gx_line+145, 169, Gx_line+163, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 2)", 61, Gx_line+162, 128, Gx_line+180, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("100%", 569, Gx_line+150, 602, Gx_line+168, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("50%", 569, Gx_line+96, 594, Gx_line+114, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("(Parcela 1)", 69, Gx_line+102, 136, Gx_line+120, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Contagem de PF Inicial", 27, Gx_line+85, 177, Gx_line+103, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("O quadro a seguir descreve o custo dos servi�os a serem executados:", 17, Gx_line+31, 413, Gx_line+49, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV182strValor, "")), 675, Gx_line+150, 767, Gx_line+167, 2, 0, 0, 1) ;
               getPrinter().GxDrawText("(*)", 392, Gx_line+96, 417, Gx_line+114, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("(*) Este valor j� esta l�quido conforme % ao lado.", 17, Gx_line+217, 325, Gx_line+235, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV185strValorIni, "")), 671, Gx_line+96, 767, Gx_line+113, 2, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("VALOR", 694, Gx_line+57, 752, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("% FASE", 550, Gx_line+57, 617, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("QUANTIDADE DE PONTOS DE FUN��O", 200, Gx_line+57, 475, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxDrawText("SERVI�O", 58, Gx_line+57, 125, Gx_line+75, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Declaramos nossa ci�ncia e concord�ncia com rela��o aos servi�os discriminados no item 02.", 17, Gx_line+277, 700, Gx_line+296, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV181strPFFinal, "")), 273, Gx_line+150, 391, Gx_line+170, 1, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV188strValorTotal, "")), 675, Gx_line+203, 768, Gx_line+223, 2, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV184strPFInicial, "")), 273, Gx_line+95, 391, Gx_line+115, 1, 0, 0, 1) ;
               getPrinter().GxAttris("Calibri", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("TOTAL", 542, Gx_line+203, 642, Gx_line+223, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+244, 781, Gx_line+259, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("A autenticidade deste documento pode ser conferida no site", 133, Gx_line+367, 430, Gx_line+381, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(AV161QrCode_Image, 683, Gx_line+317, 773, Gx_line+403) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV170TextoLink, "")), 133, Gx_line+383, 708, Gx_line+398, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV8AssinadoDtHr, "")), 133, Gx_line+333, 447, Gx_line+350, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Documento assinado eletronicamente por", 133, Gx_line+317, 374, Gx_line+333, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 9, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV160Preposto, "@!")), 383, Gx_line+317, 1009, Gx_line+334, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+406);
               H6X0( false, 133) ;
               getPrinter().GxAttris("Arial", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("8. Cancelamento da Ordem de Servi�o", 17, Gx_line+15, 317, Gx_line+34, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Motivo do Cancelamento:", 17, Gx_line+31, 192, Gx_line+49, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor da Ordem de Servi�o:", 150, Gx_line+65, 433, Gx_line+83, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Gestor de Contrato:", 492, Gx_line+65, 725, Gx_line+83, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data:", 25, Gx_line+65, 67, Gx_line+83, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Ci�ncia do Preposto da Contratada:", 400, Gx_line+115, 642, Gx_line+133, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Assinatura do Coordenador Geral Respons�vel:", 25, Gx_line+115, 342, Gx_line+133, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("__________________________________________________________________________________________________________________________", 17, Gx_line+0, 781, Gx_line+17, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+133);
               /* Eject command */
               Gx_OldLine = Gx_line;
               Gx_line = (int)(P_lines+1);
               if ( ! BRK6X8 )
               {
                  BRK6X8 = true;
                  pr_default.readNext(3);
               }
            }
            pr_default.close(3);
         }
      }

      protected void S121( )
      {
         /* 'PERIODO' Routine */
         /* Using cursor P006X10 */
         pr_default.execute(4, new Object[] {AV39ContagemResultado_LoteAceite});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A40000GXC1 = P006X10_A40000GXC1[0];
            A40001GXC2 = P006X10_A40001GXC2[0];
         }
         else
         {
            A40000GXC1 = DateTime.MinValue;
            A40001GXC2 = DateTime.MinValue;
         }
         pr_default.close(4);
         if ( StringUtil.StrCmp(AV140Lote_Numero, "2012016") == 0 )
         {
            AV111DataInicio = context.localUtil.CToD( "04/01/2016", 2);
            AV110DataFim = context.localUtil.CToD( "08/01/2016", 2);
         }
         else
         {
            AV111DataInicio = A40000GXC1;
            AV110DataFim = A40001GXC2;
         }
      }

      protected void S136( )
      {
         /* 'CONTAGENSDOSISTEMALOTE' Routine */
         AV183PFInicial = 0;
         AV159PFFinal = 0;
         /* Using cursor P006X11 */
         pr_default.execute(5, new Object[] {AV39ContagemResultado_LoteAceite, AV191ContagemResultado_SS});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A489ContagemResultado_SistemaCod = P006X11_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P006X11_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P006X11_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P006X11_n1553ContagemResultado_CntSrvCod[0];
            A597ContagemResultado_LoteAceiteCod = P006X11_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P006X11_n597ContagemResultado_LoteAceiteCod[0];
            A1452ContagemResultado_SS = P006X11_A1452ContagemResultado_SS[0];
            n1452ContagemResultado_SS = P006X11_n1452ContagemResultado_SS[0];
            A1623ContagemResultado_CntSrvMmn = P006X11_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P006X11_n1623ContagemResultado_CntSrvMmn[0];
            A484ContagemResultado_StatusDmn = P006X11_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P006X11_n484ContagemResultado_StatusDmn[0];
            A1855ContagemResultado_PFCnc = P006X11_A1855ContagemResultado_PFCnc[0];
            n1855ContagemResultado_PFCnc = P006X11_n1855ContagemResultado_PFCnc[0];
            A509ContagemrResultado_SistemaSigla = P006X11_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006X11_n509ContagemrResultado_SistemaSigla[0];
            A456ContagemResultado_Codigo = P006X11_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P006X11_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P006X11_n509ContagemrResultado_SistemaSigla[0];
            A1623ContagemResultado_CntSrvMmn = P006X11_A1623ContagemResultado_CntSrvMmn[0];
            n1623ContagemResultado_CntSrvMmn = P006X11_n1623ContagemResultado_CntSrvMmn[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            if ( StringUtil.StrCmp(A1623ContagemResultado_CntSrvMmn, "I") == 0 )
            {
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV183PFInicial = (decimal)(AV183PFInicial+A1855ContagemResultado_PFCnc);
               }
               else
               {
                  AV183PFInicial = (decimal)(AV183PFInicial+A574ContagemResultado_PFFinal);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 )
               {
                  AV159PFFinal = (decimal)(AV159PFFinal+A1855ContagemResultado_PFCnc);
               }
               else
               {
                  AV159PFFinal = (decimal)(AV159PFFinal+A574ContagemResultado_PFFinal);
               }
            }
            pr_default.readNext(5);
         }
         pr_default.close(5);
         if ( ( AV159PFFinal > Convert.ToDecimal( 0 )) && ( AV159PFFinal < 0.001m ) )
         {
            AV181strPFFinal = StringUtil.Str( AV159PFFinal, 14, 4);
         }
         else
         {
            AV181strPFFinal = StringUtil.Str( AV159PFFinal, 14, 3);
         }
         if ( ( AV183PFInicial > Convert.ToDecimal( 0 )) && ( AV183PFInicial < 0.001m ) )
         {
            AV184strPFInicial = StringUtil.Str( AV183PFInicial, 14, 4);
         }
         else
         {
            AV184strPFInicial = StringUtil.Str( AV183PFInicial, 14, 3);
         }
      }

      protected void S141( )
      {
         /* 'DADOSCONTRATADA' Routine */
         /* Using cursor P006X12 */
         pr_default.execute(6, new Object[] {AV192Contratada_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A40Contratada_PessoaCod = P006X12_A40Contratada_PessoaCod[0];
            A503Pessoa_MunicipioCod = P006X12_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P006X12_n503Pessoa_MunicipioCod[0];
            A52Contratada_AreaTrabalhoCod = P006X12_A52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P006X12_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P006X12_n29Contratante_Codigo[0];
            A39Contratada_Codigo = P006X12_A39Contratada_Codigo[0];
            n39Contratada_Codigo = P006X12_n39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P006X12_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P006X12_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P006X12_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P006X12_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P006X12_A518Pessoa_IE[0];
            n518Pessoa_IE = P006X12_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P006X12_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P006X12_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P006X12_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P006X12_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P006X12_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P006X12_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P006X12_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P006X12_n523Pessoa_Fax[0];
            A520Pessoa_UF = P006X12_A520Pessoa_UF[0];
            n520Pessoa_UF = P006X12_n520Pessoa_UF[0];
            A26Municipio_Nome = P006X12_A26Municipio_Nome[0];
            n26Municipio_Nome = P006X12_n26Municipio_Nome[0];
            A593Contratante_OSAutomatica = P006X12_A593Contratante_OSAutomatica[0];
            A503Pessoa_MunicipioCod = P006X12_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = P006X12_n503Pessoa_MunicipioCod[0];
            A41Contratada_PessoaNom = P006X12_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P006X12_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P006X12_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P006X12_n42Contratada_PessoaCNPJ[0];
            A518Pessoa_IE = P006X12_A518Pessoa_IE[0];
            n518Pessoa_IE = P006X12_n518Pessoa_IE[0];
            A519Pessoa_Endereco = P006X12_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = P006X12_n519Pessoa_Endereco[0];
            A521Pessoa_CEP = P006X12_A521Pessoa_CEP[0];
            n521Pessoa_CEP = P006X12_n521Pessoa_CEP[0];
            A522Pessoa_Telefone = P006X12_A522Pessoa_Telefone[0];
            n522Pessoa_Telefone = P006X12_n522Pessoa_Telefone[0];
            A523Pessoa_Fax = P006X12_A523Pessoa_Fax[0];
            n523Pessoa_Fax = P006X12_n523Pessoa_Fax[0];
            A520Pessoa_UF = P006X12_A520Pessoa_UF[0];
            n520Pessoa_UF = P006X12_n520Pessoa_UF[0];
            A26Municipio_Nome = P006X12_A26Municipio_Nome[0];
            n26Municipio_Nome = P006X12_n26Municipio_Nome[0];
            A29Contratante_Codigo = P006X12_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P006X12_n29Contratante_Codigo[0];
            A593Contratante_OSAutomatica = P006X12_A593Contratante_OSAutomatica[0];
            AV103Contratada_PessoaNom = A41Contratada_PessoaNom;
            if ( StringUtil.StringSearch( A42Contratada_PessoaCNPJ, ".", 1) == 0 )
            {
               AV102Contratada_PessoaCNPJ = StringUtil.Substring( A42Contratada_PessoaCNPJ, 1, 2) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 3, 3) + "." + StringUtil.Substring( A42Contratada_PessoaCNPJ, 6, 3) + "/" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 9, 4) + "-" + StringUtil.Substring( A42Contratada_PessoaCNPJ, 13, 2);
            }
            else
            {
               AV102Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
            }
            if ( StringUtil.StringSearch( A518Pessoa_IE, ".", 1) == 0 )
            {
               AV156Pessoa_IE = StringUtil.Substring( A518Pessoa_IE, 1, 2) + "." + StringUtil.Substring( A518Pessoa_IE, 3, 3) + "." + StringUtil.Substring( A518Pessoa_IE, 6, 3) + "/" + StringUtil.Substring( A518Pessoa_IE, 9, 3) + "-" + StringUtil.Substring( A518Pessoa_IE, 12, 2);
            }
            else
            {
               AV156Pessoa_IE = A518Pessoa_IE;
            }
            AV154Pessoa_Endereco = A519Pessoa_Endereco;
            AV153Pessoa_CEP = A521Pessoa_CEP;
            AV157Pessoa_Telefone = A522Pessoa_Telefone;
            AV155Pessoa_Fax = A523Pessoa_Fax;
            AV158Pessoa_UF = A520Pessoa_UF;
            AV141Municipio_Nome = A26Municipio_Nome;
            AV104Contratante_OSAutomatica = A593Contratante_OSAutomatica;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
      }

      protected void H6X0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 647, Gx_line+4, 691, Gx_line+31, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV150Pagina), "ZZZ9")), 696, Gx_line+4, 729, Gx_line+33, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 730, Gx_line+4, 747, Gx_line+31, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV145Os, "")), 41, Gx_line+2, 166, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("SS", 17, Gx_line+2, 33, Gx_line+17, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("2", 755, Gx_line+4, 773, Gx_line+31, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+38);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               AV169SubTitulo = "ORDEM DE SERVI�O";
               AV123Emissao = AV111DataInicio;
               AV124EmissaoTA = AV111DataInicio;
               AV175Volume = "A previs�o de volume total de servi�o apresentado neste documento � de:";
               AV171TituloOS = "N� da SS";
               AV131Identificacao1 = "Esta Ordem de Servi�o tem como objetivo a presta��o de servi�os t�cnicos especializados para as atividades de";
               AV132Identificacao2 = "an�lise e mensura��o de software com base na t�cnica de contagem de pontos de fun��o do sistema acima";
               AV133Identificacao3 = "referenciado no �mbito do Minist�rio da Sa�de.";
               AV145Os = AV147Os_Header;
               if ( ( Gx_page /  ( decimal )( 2 ) != Convert.ToDecimal( NumberUtil.Int( (long)(Gx_page/ (decimal)(2))) )) )
               {
                  AV150Pagina = 1;
                  getPrinter().GxDrawBitMap(context.GetImagePath( "6ded99f5-9d9f-4337-aeda-7b9046b912b6", "", context.GetTheme( )), 349, Gx_line+0, 429, Gx_line+80) ;
                  getPrinter().GxDrawLine(314, Gx_line+201, 314, Gx_line+250, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+201, 428, Gx_line+250, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(604, Gx_line+201, 604, Gx_line+251, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(14, Gx_line+201, 778, Gx_line+201, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawRect(14, Gx_line+168, 778, Gx_line+251, 1, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(176, Gx_line+201, 176, Gx_line+249, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+169, 428, Gx_line+202, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(778, Gx_line+251, 778, Gx_line+286, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(14, Gx_line+250, 14, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(15, Gx_line+287, 778, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(314, Gx_line+251, 314, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(604, Gx_line+251, 604, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(428, Gx_line+251, 428, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxDrawLine(176, Gx_line+251, 176, Gx_line+287, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV171TituloOS, "")), 17, Gx_line+218, 174, Gx_line+235, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATASUS", 555, Gx_line+177, 664, Gx_line+195, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 139, Gx_line+177, 306, Gx_line+195, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATA DO CONTRATO", 616, Gx_line+218, 765, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("N� DO CONTRATO", 443, Gx_line+218, 590, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("DATA DE EMISS�O", 179, Gx_line+218, 310, Gx_line+236, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("LOTE", 322, Gx_line+217, 422, Gx_line+235, 1, 0, 0, 1) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV106Contrato_DataVigenciaInicio, "99/99/99"), 617, Gx_line+259, 766, Gx_line+276, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV107Contrato_Numero, "")), 441, Gx_line+258, 588, Gx_line+275, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV147Os_Header, "")), 17, Gx_line+256, 174, Gx_line+273, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV123Emissao, "99/99/99"), 182, Gx_line+258, 301, Gx_line+275, 1, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 11, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("MINIST�RIO DA SA�DE", 317, Gx_line+83, 462, Gx_line+102, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("SECRETARIA DE GEST�O ESTRAT�GICA E PARTICIPATIVA", 217, Gx_line+100, 562, Gx_line+119, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("DEPARTAMENTO DE INFORM�TICA DO SUS - DATASUS", 217, Gx_line+117, 556, Gx_line+136, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV169SubTitulo, "")), 217, Gx_line+138, 551, Gx_line+158, 1+256, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV138Lote, "")), 323, Gx_line+257, 421, Gx_line+274, 1, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+292);
               }
               else
               {
                  AV150Pagina = 2;
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
         add_metrics3( ) ;
         add_metrics4( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Arial", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics3( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics4( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV177WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P006X4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006X4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006X4_A1603ContagemResultado_CntCod = new int[1] ;
         P006X4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P006X4_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P006X4_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P006X4_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P006X4_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P006X4_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006X4_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006X4_A1612ContagemResultado_CntNum = new String[] {""} ;
         P006X4_n1612ContagemResultado_CntNum = new bool[] {false} ;
         P006X4_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P006X4_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P006X4_A1624ContagemResultado_DatVgnInc = new DateTime[] {DateTime.MinValue} ;
         P006X4_n1624ContagemResultado_DatVgnInc = new bool[] {false} ;
         P006X4_A39Contratada_Codigo = new int[1] ;
         P006X4_n39Contratada_Codigo = new bool[] {false} ;
         P006X4_A1625ContagemResultado_DatIncTA = new DateTime[] {DateTime.MinValue} ;
         P006X4_n1625ContagemResultado_DatIncTA = new bool[] {false} ;
         P006X4_A456ContagemResultado_Codigo = new int[1] ;
         A1612ContagemResultado_CntNum = "";
         A1606ContagemResultado_CntPrpPesNom = "";
         A1624ContagemResultado_DatVgnInc = DateTime.MinValue;
         A1625ContagemResultado_DatIncTA = DateTime.MinValue;
         AV107Contrato_Numero = "";
         AV160Preposto = "";
         AV105Contrato_DataInicioTA = DateTime.MinValue;
         AV106Contrato_DataVigenciaInicio = DateTime.MinValue;
         AV170TextoLink = "";
         AV8AssinadoDtHr = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         AV161QrCode_Image = "";
         AV198Qrcode_image_GXI = "";
         P006X5_A596Lote_Codigo = new int[1] ;
         P006X5_A562Lote_Numero = new String[] {""} ;
         P006X5_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         P006X5_n844Lote_DataContrato = new bool[] {false} ;
         A562Lote_Numero = "";
         A844Lote_DataContrato = DateTime.MinValue;
         AV140Lote_Numero = "";
         AV138Lote = "";
         P006X6_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006X6_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006X6_A1452ContagemResultado_SS = new int[1] ;
         P006X6_n1452ContagemResultado_SS = new bool[] {false} ;
         P006X6_A457ContagemResultado_Demanda = new String[] {""} ;
         P006X6_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006X6_A489ContagemResultado_SistemaCod = new int[1] ;
         P006X6_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006X6_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006X6_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006X6_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P006X6_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P006X6_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006X6_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P006X6_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         A509ContagemrResultado_SistemaSigla = "";
         A515ContagemResultado_SistemaCoord = "";
         AV191ContagemResultado_SS = 0;
         AV147Os_Header = "";
         AV168Sistema_Sigla = "";
         AV167Sistema_Coordenacao = "";
         AV182strValor = "";
         AV185strValorIni = "";
         AV188strValorTotal = "";
         AV189strPFTotal = "";
         AV111DataInicio = DateTime.MinValue;
         AV110DataFim = DateTime.MinValue;
         AV103Contratada_PessoaNom = "";
         AV102Contratada_PessoaCNPJ = "";
         AV154Pessoa_Endereco = "";
         AV157Pessoa_Telefone = "";
         AV153Pessoa_CEP = "";
         AV155Pessoa_Fax = "";
         AV158Pessoa_UF = "";
         AV141Municipio_Nome = "";
         AV156Pessoa_IE = "";
         AV175Volume = "";
         AV131Identificacao1 = "";
         AV132Identificacao2 = "";
         AV133Identificacao3 = "";
         AV181strPFFinal = "";
         AV184strPFInicial = "";
         AV161QrCode_Image = "";
         P006X7_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006X7_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006X7_A1452ContagemResultado_SS = new int[1] ;
         P006X7_n1452ContagemResultado_SS = new bool[] {false} ;
         P006X7_A457ContagemResultado_Demanda = new String[] {""} ;
         P006X7_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006X7_A489ContagemResultado_SistemaCod = new int[1] ;
         P006X7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006X7_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006X7_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006X7_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P006X7_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         P006X7_A512ContagemResultado_ValorPF = new decimal[1] ;
         P006X7_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P006X7_A456ContagemResultado_Codigo = new int[1] ;
         P006X10_A40000GXC1 = new DateTime[] {DateTime.MinValue} ;
         P006X10_A40001GXC2 = new DateTime[] {DateTime.MinValue} ;
         A40000GXC1 = DateTime.MinValue;
         A40001GXC2 = DateTime.MinValue;
         P006X11_A489ContagemResultado_SistemaCod = new int[1] ;
         P006X11_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P006X11_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006X11_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006X11_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P006X11_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P006X11_A1452ContagemResultado_SS = new int[1] ;
         P006X11_n1452ContagemResultado_SS = new bool[] {false} ;
         P006X11_A1623ContagemResultado_CntSrvMmn = new String[] {""} ;
         P006X11_n1623ContagemResultado_CntSrvMmn = new bool[] {false} ;
         P006X11_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006X11_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006X11_A1855ContagemResultado_PFCnc = new decimal[1] ;
         P006X11_n1855ContagemResultado_PFCnc = new bool[] {false} ;
         P006X11_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P006X11_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P006X11_A456ContagemResultado_Codigo = new int[1] ;
         A1623ContagemResultado_CntSrvMmn = "";
         A484ContagemResultado_StatusDmn = "";
         P006X12_A40Contratada_PessoaCod = new int[1] ;
         P006X12_A503Pessoa_MunicipioCod = new int[1] ;
         P006X12_n503Pessoa_MunicipioCod = new bool[] {false} ;
         P006X12_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P006X12_A29Contratante_Codigo = new int[1] ;
         P006X12_n29Contratante_Codigo = new bool[] {false} ;
         P006X12_A39Contratada_Codigo = new int[1] ;
         P006X12_n39Contratada_Codigo = new bool[] {false} ;
         P006X12_A41Contratada_PessoaNom = new String[] {""} ;
         P006X12_n41Contratada_PessoaNom = new bool[] {false} ;
         P006X12_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P006X12_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P006X12_A518Pessoa_IE = new String[] {""} ;
         P006X12_n518Pessoa_IE = new bool[] {false} ;
         P006X12_A519Pessoa_Endereco = new String[] {""} ;
         P006X12_n519Pessoa_Endereco = new bool[] {false} ;
         P006X12_A521Pessoa_CEP = new String[] {""} ;
         P006X12_n521Pessoa_CEP = new bool[] {false} ;
         P006X12_A522Pessoa_Telefone = new String[] {""} ;
         P006X12_n522Pessoa_Telefone = new bool[] {false} ;
         P006X12_A523Pessoa_Fax = new String[] {""} ;
         P006X12_n523Pessoa_Fax = new bool[] {false} ;
         P006X12_A520Pessoa_UF = new String[] {""} ;
         P006X12_n520Pessoa_UF = new bool[] {false} ;
         P006X12_A26Municipio_Nome = new String[] {""} ;
         P006X12_n26Municipio_Nome = new bool[] {false} ;
         P006X12_A593Contratante_OSAutomatica = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         A520Pessoa_UF = "";
         A26Municipio_Nome = "";
         AV145Os = "";
         AV169SubTitulo = "";
         AV123Emissao = DateTime.MinValue;
         AV124EmissaoTA = DateTime.MinValue;
         AV171TituloOS = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.rel_contagemosarqpdf__default(),
            new Object[][] {
                new Object[] {
               P006X4_A1553ContagemResultado_CntSrvCod, P006X4_n1553ContagemResultado_CntSrvCod, P006X4_A1603ContagemResultado_CntCod, P006X4_n1603ContagemResultado_CntCod, P006X4_A1604ContagemResultado_CntPrpCod, P006X4_n1604ContagemResultado_CntPrpCod, P006X4_A1605ContagemResultado_CntPrpPesCod, P006X4_n1605ContagemResultado_CntPrpPesCod, P006X4_A597ContagemResultado_LoteAceiteCod, P006X4_n597ContagemResultado_LoteAceiteCod,
               P006X4_A1612ContagemResultado_CntNum, P006X4_n1612ContagemResultado_CntNum, P006X4_A1606ContagemResultado_CntPrpPesNom, P006X4_n1606ContagemResultado_CntPrpPesNom, P006X4_A1624ContagemResultado_DatVgnInc, P006X4_n1624ContagemResultado_DatVgnInc, P006X4_A39Contratada_Codigo, P006X4_n39Contratada_Codigo, P006X4_A1625ContagemResultado_DatIncTA, P006X4_n1625ContagemResultado_DatIncTA,
               P006X4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006X5_A596Lote_Codigo, P006X5_A562Lote_Numero, P006X5_A844Lote_DataContrato, P006X5_n844Lote_DataContrato
               }
               , new Object[] {
               P006X6_A597ContagemResultado_LoteAceiteCod, P006X6_n597ContagemResultado_LoteAceiteCod, P006X6_A1452ContagemResultado_SS, P006X6_n1452ContagemResultado_SS, P006X6_A457ContagemResultado_Demanda, P006X6_n457ContagemResultado_Demanda, P006X6_A489ContagemResultado_SistemaCod, P006X6_n489ContagemResultado_SistemaCod, P006X6_A509ContagemrResultado_SistemaSigla, P006X6_n509ContagemrResultado_SistemaSigla,
               P006X6_A515ContagemResultado_SistemaCoord, P006X6_n515ContagemResultado_SistemaCoord, P006X6_A512ContagemResultado_ValorPF, P006X6_n512ContagemResultado_ValorPF, P006X6_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006X7_A597ContagemResultado_LoteAceiteCod, P006X7_n597ContagemResultado_LoteAceiteCod, P006X7_A1452ContagemResultado_SS, P006X7_n1452ContagemResultado_SS, P006X7_A457ContagemResultado_Demanda, P006X7_n457ContagemResultado_Demanda, P006X7_A489ContagemResultado_SistemaCod, P006X7_n489ContagemResultado_SistemaCod, P006X7_A509ContagemrResultado_SistemaSigla, P006X7_n509ContagemrResultado_SistemaSigla,
               P006X7_A515ContagemResultado_SistemaCoord, P006X7_n515ContagemResultado_SistemaCoord, P006X7_A512ContagemResultado_ValorPF, P006X7_n512ContagemResultado_ValorPF, P006X7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006X10_A40000GXC1, P006X10_A40001GXC2
               }
               , new Object[] {
               P006X11_A489ContagemResultado_SistemaCod, P006X11_n489ContagemResultado_SistemaCod, P006X11_A1553ContagemResultado_CntSrvCod, P006X11_n1553ContagemResultado_CntSrvCod, P006X11_A597ContagemResultado_LoteAceiteCod, P006X11_n597ContagemResultado_LoteAceiteCod, P006X11_A1452ContagemResultado_SS, P006X11_n1452ContagemResultado_SS, P006X11_A1623ContagemResultado_CntSrvMmn, P006X11_n1623ContagemResultado_CntSrvMmn,
               P006X11_A484ContagemResultado_StatusDmn, P006X11_n484ContagemResultado_StatusDmn, P006X11_A1855ContagemResultado_PFCnc, P006X11_n1855ContagemResultado_PFCnc, P006X11_A509ContagemrResultado_SistemaSigla, P006X11_n509ContagemrResultado_SistemaSigla, P006X11_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P006X12_A40Contratada_PessoaCod, P006X12_A503Pessoa_MunicipioCod, P006X12_n503Pessoa_MunicipioCod, P006X12_A52Contratada_AreaTrabalhoCod, P006X12_A29Contratante_Codigo, P006X12_n29Contratante_Codigo, P006X12_A39Contratada_Codigo, P006X12_A41Contratada_PessoaNom, P006X12_n41Contratada_PessoaNom, P006X12_A42Contratada_PessoaCNPJ,
               P006X12_n42Contratada_PessoaCNPJ, P006X12_A518Pessoa_IE, P006X12_n518Pessoa_IE, P006X12_A519Pessoa_Endereco, P006X12_n519Pessoa_Endereco, P006X12_A521Pessoa_CEP, P006X12_n521Pessoa_CEP, P006X12_A522Pessoa_Telefone, P006X12_n522Pessoa_Telefone, P006X12_A523Pessoa_Fax,
               P006X12_n523Pessoa_Fax, P006X12_A520Pessoa_UF, P006X12_n520Pessoa_UF, P006X12_A26Municipio_Nome, P006X12_n26Municipio_Nome, P006X12_A593Contratante_OSAutomatica
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV91ContagemResultado_StatusCnt ;
      private short AV143OrderedBy ;
      private short AV151Paginas ;
      private short AV137Length ;
      private short AV179x ;
      private short AV130i ;
      private short AV150Pagina ;
      private int AV99Contratada_AreaTrabalhoCod ;
      private int AV39ContagemResultado_LoteAceite ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A39Contratada_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int AV192Contratada_Codigo ;
      private int A596Lote_Codigo ;
      private int A1452ContagemResultado_SS ;
      private int A489ContagemResultado_SistemaCod ;
      private int OV191ContagemResultado_SS ;
      private int AV191ContagemResultado_SS ;
      private int AV166Sistema_Codigo ;
      private int Gx_OldLine ;
      private int A40Contratada_PessoaCod ;
      private int A503Pessoa_MunicipioCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A29Contratante_Codigo ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal AV186ValorIni ;
      private decimal AV183PFInicial ;
      private decimal AV173Valor ;
      private decimal AV159PFFinal ;
      private decimal AV187ValorTotal ;
      private decimal A1855ContagemResultado_PFCnc ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String AV174Verificador ;
      private String AV125FileName ;
      private String scmdbuf ;
      private String A1612ContagemResultado_CntNum ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String AV107Contrato_Numero ;
      private String AV160Preposto ;
      private String AV170TextoLink ;
      private String AV8AssinadoDtHr ;
      private String Gx_time ;
      private String A562Lote_Numero ;
      private String AV140Lote_Numero ;
      private String AV138Lote ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String AV168Sistema_Sigla ;
      private String AV182strValor ;
      private String AV185strValorIni ;
      private String AV188strValorTotal ;
      private String AV189strPFTotal ;
      private String AV103Contratada_PessoaNom ;
      private String AV157Pessoa_Telefone ;
      private String AV153Pessoa_CEP ;
      private String AV155Pessoa_Fax ;
      private String AV158Pessoa_UF ;
      private String AV141Municipio_Nome ;
      private String AV156Pessoa_IE ;
      private String AV175Volume ;
      private String AV131Identificacao1 ;
      private String AV132Identificacao2 ;
      private String AV133Identificacao3 ;
      private String AV181strPFFinal ;
      private String AV184strPFInicial ;
      private String A1623ContagemResultado_CntSrvMmn ;
      private String A484ContagemResultado_StatusDmn ;
      private String A41Contratada_PessoaNom ;
      private String A518Pessoa_IE ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private String A520Pessoa_UF ;
      private String A26Municipio_Nome ;
      private String AV171TituloOS ;
      private DateTime A1624ContagemResultado_DatVgnInc ;
      private DateTime A1625ContagemResultado_DatIncTA ;
      private DateTime AV105Contrato_DataInicioTA ;
      private DateTime AV106Contrato_DataVigenciaInicio ;
      private DateTime Gx_date ;
      private DateTime A844Lote_DataContrato ;
      private DateTime AV111DataInicio ;
      private DateTime AV110DataFim ;
      private DateTime A40000GXC1 ;
      private DateTime A40001GXC2 ;
      private DateTime AV123Emissao ;
      private DateTime AV124EmissaoTA ;
      private bool AV144OrderedDsc ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1612ContagemResultado_CntNum ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n1624ContagemResultado_DatVgnInc ;
      private bool n39Contratada_Codigo ;
      private bool n1625ContagemResultado_DatIncTA ;
      private bool returnInSub ;
      private bool n844Lote_DataContrato ;
      private bool AV104Contratante_OSAutomatica ;
      private bool BRK6X6 ;
      private bool n1452ContagemResultado_SS ;
      private bool n457ContagemResultado_Demanda ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n512ContagemResultado_ValorPF ;
      private bool AV134ImprimindoOS ;
      private bool BRK6X8 ;
      private bool n1623ContagemResultado_CntSrvMmn ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1855ContagemResultado_PFCnc ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n29Contratante_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n520Pessoa_UF ;
      private bool n26Municipio_Nome ;
      private bool A593Contratante_OSAutomatica ;
      private String AV129GridStateXML ;
      private String AV162QrCodePath ;
      private String AV163QrCodeUrl ;
      private String AV198Qrcode_image_GXI ;
      private String A457ContagemResultado_Demanda ;
      private String A515ContagemResultado_SistemaCoord ;
      private String AV147Os_Header ;
      private String AV167Sistema_Coordenacao ;
      private String AV102Contratada_PessoaCNPJ ;
      private String AV154Pessoa_Endereco ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String AV145Os ;
      private String AV169SubTitulo ;
      private String AV161QrCode_Image ;
      private String Qrcode_image ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P006X4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006X4_n1553ContagemResultado_CntSrvCod ;
      private int[] P006X4_A1603ContagemResultado_CntCod ;
      private bool[] P006X4_n1603ContagemResultado_CntCod ;
      private int[] P006X4_A1604ContagemResultado_CntPrpCod ;
      private bool[] P006X4_n1604ContagemResultado_CntPrpCod ;
      private int[] P006X4_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P006X4_n1605ContagemResultado_CntPrpPesCod ;
      private int[] P006X4_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006X4_n597ContagemResultado_LoteAceiteCod ;
      private String[] P006X4_A1612ContagemResultado_CntNum ;
      private bool[] P006X4_n1612ContagemResultado_CntNum ;
      private String[] P006X4_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P006X4_n1606ContagemResultado_CntPrpPesNom ;
      private DateTime[] P006X4_A1624ContagemResultado_DatVgnInc ;
      private bool[] P006X4_n1624ContagemResultado_DatVgnInc ;
      private int[] P006X4_A39Contratada_Codigo ;
      private bool[] P006X4_n39Contratada_Codigo ;
      private DateTime[] P006X4_A1625ContagemResultado_DatIncTA ;
      private bool[] P006X4_n1625ContagemResultado_DatIncTA ;
      private int[] P006X4_A456ContagemResultado_Codigo ;
      private int[] P006X5_A596Lote_Codigo ;
      private String[] P006X5_A562Lote_Numero ;
      private DateTime[] P006X5_A844Lote_DataContrato ;
      private bool[] P006X5_n844Lote_DataContrato ;
      private int[] P006X6_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006X6_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006X6_A1452ContagemResultado_SS ;
      private bool[] P006X6_n1452ContagemResultado_SS ;
      private String[] P006X6_A457ContagemResultado_Demanda ;
      private bool[] P006X6_n457ContagemResultado_Demanda ;
      private int[] P006X6_A489ContagemResultado_SistemaCod ;
      private bool[] P006X6_n489ContagemResultado_SistemaCod ;
      private String[] P006X6_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006X6_n509ContagemrResultado_SistemaSigla ;
      private String[] P006X6_A515ContagemResultado_SistemaCoord ;
      private bool[] P006X6_n515ContagemResultado_SistemaCoord ;
      private decimal[] P006X6_A512ContagemResultado_ValorPF ;
      private bool[] P006X6_n512ContagemResultado_ValorPF ;
      private int[] P006X6_A456ContagemResultado_Codigo ;
      private int[] P006X7_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006X7_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006X7_A1452ContagemResultado_SS ;
      private bool[] P006X7_n1452ContagemResultado_SS ;
      private String[] P006X7_A457ContagemResultado_Demanda ;
      private bool[] P006X7_n457ContagemResultado_Demanda ;
      private int[] P006X7_A489ContagemResultado_SistemaCod ;
      private bool[] P006X7_n489ContagemResultado_SistemaCod ;
      private String[] P006X7_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006X7_n509ContagemrResultado_SistemaSigla ;
      private String[] P006X7_A515ContagemResultado_SistemaCoord ;
      private bool[] P006X7_n515ContagemResultado_SistemaCoord ;
      private decimal[] P006X7_A512ContagemResultado_ValorPF ;
      private bool[] P006X7_n512ContagemResultado_ValorPF ;
      private int[] P006X7_A456ContagemResultado_Codigo ;
      private DateTime[] P006X10_A40000GXC1 ;
      private DateTime[] P006X10_A40001GXC2 ;
      private int[] P006X11_A489ContagemResultado_SistemaCod ;
      private bool[] P006X11_n489ContagemResultado_SistemaCod ;
      private int[] P006X11_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006X11_n1553ContagemResultado_CntSrvCod ;
      private int[] P006X11_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P006X11_n597ContagemResultado_LoteAceiteCod ;
      private int[] P006X11_A1452ContagemResultado_SS ;
      private bool[] P006X11_n1452ContagemResultado_SS ;
      private String[] P006X11_A1623ContagemResultado_CntSrvMmn ;
      private bool[] P006X11_n1623ContagemResultado_CntSrvMmn ;
      private String[] P006X11_A484ContagemResultado_StatusDmn ;
      private bool[] P006X11_n484ContagemResultado_StatusDmn ;
      private decimal[] P006X11_A1855ContagemResultado_PFCnc ;
      private bool[] P006X11_n1855ContagemResultado_PFCnc ;
      private String[] P006X11_A509ContagemrResultado_SistemaSigla ;
      private bool[] P006X11_n509ContagemrResultado_SistemaSigla ;
      private int[] P006X11_A456ContagemResultado_Codigo ;
      private int[] P006X12_A40Contratada_PessoaCod ;
      private int[] P006X12_A503Pessoa_MunicipioCod ;
      private bool[] P006X12_n503Pessoa_MunicipioCod ;
      private int[] P006X12_A52Contratada_AreaTrabalhoCod ;
      private int[] P006X12_A29Contratante_Codigo ;
      private bool[] P006X12_n29Contratante_Codigo ;
      private int[] P006X12_A39Contratada_Codigo ;
      private bool[] P006X12_n39Contratada_Codigo ;
      private String[] P006X12_A41Contratada_PessoaNom ;
      private bool[] P006X12_n41Contratada_PessoaNom ;
      private String[] P006X12_A42Contratada_PessoaCNPJ ;
      private bool[] P006X12_n42Contratada_PessoaCNPJ ;
      private String[] P006X12_A518Pessoa_IE ;
      private bool[] P006X12_n518Pessoa_IE ;
      private String[] P006X12_A519Pessoa_Endereco ;
      private bool[] P006X12_n519Pessoa_Endereco ;
      private String[] P006X12_A521Pessoa_CEP ;
      private bool[] P006X12_n521Pessoa_CEP ;
      private String[] P006X12_A522Pessoa_Telefone ;
      private bool[] P006X12_n522Pessoa_Telefone ;
      private String[] P006X12_A523Pessoa_Fax ;
      private bool[] P006X12_n523Pessoa_Fax ;
      private String[] P006X12_A520Pessoa_UF ;
      private bool[] P006X12_n520Pessoa_UF ;
      private String[] P006X12_A26Municipio_Nome ;
      private bool[] P006X12_n26Municipio_Nome ;
      private bool[] P006X12_A593Contratante_OSAutomatica ;
      private wwpbaseobjects.SdtWWPContext AV177WWPContext ;
   }

   public class rel_contagemosarqpdf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006X4 ;
          prmP006X4 = new Object[] {
          new Object[] {"@AV39ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006X5 ;
          prmP006X5 = new Object[] {
          new Object[] {"@AV39ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006X6 ;
          prmP006X6 = new Object[] {
          new Object[] {"@AV39ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006X7 ;
          prmP006X7 = new Object[] {
          new Object[] {"@AV39ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006X10 ;
          prmP006X10 = new Object[] {
          new Object[] {"@AV39ContagemResultado_LoteAceite",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006X11 ;
          prmP006X11 = new Object[] {
          new Object[] {"@AV39ContagemResultado_LoteAceite",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ContagemResultado_SS",SqlDbType.Int,8,0}
          } ;
          Object[] prmP006X12 ;
          prmP006X12 = new Object[] {
          new Object[] {"@AV192Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006X4", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T3.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T4.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T1.[ContagemResultado_LoteAceiteCod], T3.[Contrato_Numero] AS ContagemResultado_CntNum, T5.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T3.[Contrato_DataVigenciaInicio] AS ContagemResultado_DatVgnInc, T3.[Contratada_Codigo], COALESCE( T6.[ContratoTermoAditivo_DataInicio], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DatIncTA, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]),  (SELECT T7.[ContratoTermoAditivo_DataInicio], T7.[ContratoTermoAditivo_Codigo], T8.[GXC6] AS GXC6 FROM [ContratoTermoAditivo] T7 WITH (NOLOCK),  (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC6 FROM [ContratoTermoAditivo] WITH (NOLOCK) ) T8 WHERE T7.[ContratoTermoAditivo_Codigo] = T8.[GXC6] ) T6 WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV39ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006X4,1,0,true,true )
             ,new CursorDef("P006X5", "SELECT [Lote_Codigo], [Lote_Numero], [Lote_DataContrato] FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @AV39ContagemResultado_LoteAceite ORDER BY [Lote_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006X5,1,0,false,true )
             ,new CursorDef("P006X6", "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV39ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006X6,100,0,true,false )
             ,new CursorDef("P006X7", "SELECT T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T1.[ContagemResultado_ValorPF], T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_LoteAceiteCod] = @AV39ContagemResultado_LoteAceite ORDER BY T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T1.[ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006X7,100,0,true,false )
             ,new CursorDef("P006X10", "SELECT COALESCE( T1.[GXC1], convert( DATETIME, '17530101', 112 )) AS GXC1, COALESCE( T1.[GXC2], convert( DATETIME, '17530101', 112 )) AS GXC2 FROM (SELECT MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC1, MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS GXC2 FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) WHERE T2.[ContagemResultado_LoteAceiteCod] = @AV39ContagemResultado_LoteAceite ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006X10,1,0,true,true )
             ,new CursorDef("P006X11", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_LoteAceiteCod], T1.[ContagemResultado_SS], T3.[ContratoServicos_Momento] AS ContagemResultado_CntSrvMmn, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_PFCnc], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_LoteAceiteCod] = @AV39ContagemResultado_LoteAceite) AND (T1.[ContagemResultado_SS] = @AV191ContagemResultado_SS) ORDER BY T1.[ContagemResultado_LoteAceiteCod], T2.[Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006X11,100,0,true,false )
             ,new CursorDef("P006X12", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Contratante_Codigo], T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_IE], T2.[Pessoa_Endereco], T2.[Pessoa_CEP], T2.[Pessoa_Telefone], T2.[Pessoa_Fax], T3.[Estado_UF] AS Pessoa_UF, T3.[Municipio_Nome], T5.[Contratante_OSAutomatica] FROM (((([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Pessoa_MunicipioCod]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) WHERE T1.[Contratada_Codigo] = @AV192Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006X12,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 25) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 2) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 50) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
