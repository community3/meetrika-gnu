/*
               File: ServicoFluxo
        Description: Processo do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:56.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicofluxo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A1522ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A1522ServicoFluxo_ServicoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A1526ServicoFluxo_ServicoPos = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1526ServicoFluxo_ServicoPos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1526ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A1526ServicoFluxo_ServicoPos) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV36ServicoFluxo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36ServicoFluxo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICOFLUXO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36ServicoFluxo_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Processo do Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servicofluxo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicofluxo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ServicoFluxo_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV36ServicoFluxo_Codigo = aP1_ServicoFluxo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3V176( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3V176e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavRenumerardesde_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17RenumerarDesde), 4, 0, ",", "")), ((edtavRenumerardesde_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17RenumerarDesde), "ZZZ9")) : context.localUtil.Format( (decimal)(AV17RenumerarDesde), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRenumerardesde_Jsonclick, 0, "Attribute", "", "", "", edtavRenumerardesde_Visible, edtavRenumerardesde_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoFluxo.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavOrdemanterior_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24OrdemAnterior), 4, 0, ",", "")), ((edtavOrdemanterior_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24OrdemAnterior), "ZZZ9")) : context.localUtil.Format( (decimal)(AV24OrdemAnterior), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdemanterior_Jsonclick, 0, "Attribute", "", "", "", edtavOrdemanterior_Visible, edtavOrdemanterior_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoFluxo.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ",", "")), ((edtServicoFluxo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServicoFluxo_Codigo_Visible, edtServicoFluxo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoFluxo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3V176( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3V176( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3V176e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3V176e( true) ;
         }
         else
         {
            wb_table1_2_3V176e( false) ;
         }
      }

      protected void wb_table2_5_3V176( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservico_Internalname, lblTbservico_Caption, "", "", lblTbservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_14_3V176( true) ;
         }
         return  ;
      }

      protected void wb_table3_14_3V176e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_62_3V176( true) ;
         }
         return  ;
      }

      protected void wb_table4_62_3V176e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3V176e( true) ;
         }
         else
         {
            wb_table2_5_3V176e( false) ;
         }
      }

      protected void wb_table4_62_3V176( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_62_3V176e( true) ;
         }
         else
         {
            wb_table4_62_3V176e( false) ;
         }
      }

      protected void wb_table3_14_3V176( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_ordem_Internalname, "Ordem", "", "", lblTextblockservicofluxo_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ",", "")), ((edtServicoFluxo_Ordem_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")) : context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_Ordem_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoFluxo_Ordem_Enabled, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_srvposnome_Internalname, "Nome", "", "", lblTextblockservicofluxo_srvposnome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_SrvPosNome_Internalname, StringUtil.RTrim( A1558ServicoFluxo_SrvPosNome), StringUtil.RTrim( context.localUtil.Format( A1558ServicoFluxo_SrvPosNome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_SrvPosNome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoFluxo_SrvPosNome_Enabled, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_srvpossigla_Internalname, "Sigla", "", "", lblTextblockservicofluxo_srvpossigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_SrvPosSigla_Internalname, StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla), StringUtil.RTrim( context.localUtil.Format( A1527ServicoFluxo_SrvPosSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_SrvPosSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoFluxo_SrvPosSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_srvposprctmp_Internalname, "de Tempo", "", "", lblTextblockservicofluxo_srvposprctmp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_34_3V176( true) ;
         }
         return  ;
      }

      protected void wb_table5_34_3V176e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_srvposprcpgm_Internalname, "de Pagamento", "", "", lblTextblockservicofluxo_srvposprcpgm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_44_3V176( true) ;
         }
         return  ;
      }

      protected void wb_table6_44_3V176e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_srvposprccnc_Internalname, "de Cancelamento", "", "", lblTextblockservicofluxo_srvposprccnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_54_3V176( true) ;
         }
         return  ;
      }

      protected void wb_table7_54_3V176e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_14_3V176e( true) ;
         }
         else
         {
            wb_table3_14_3V176e( false) ;
         }
      }

      protected void wb_table7_54_3V176( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservicofluxo_srvposprccnc_Internalname, tblTablemergedservicofluxo_srvposprccnc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_SrvPosPrcCnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0, ",", "")), ((edtServicoFluxo_SrvPosPrcCnc_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), "ZZZ9")) : context.localUtil.Format( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_SrvPosPrcCnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoFluxo_SrvPosPrcCnc_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServicofluxo_srvposprccnc_righttext_Internalname, "� %", "", "", lblServicofluxo_srvposprccnc_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_54_3V176e( true) ;
         }
         else
         {
            wb_table7_54_3V176e( false) ;
         }
      }

      protected void wb_table6_44_3V176( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservicofluxo_srvposprcpgm_Internalname, tblTablemergedservicofluxo_srvposprcpgm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_SrvPosPrcPgm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0, ",", "")), ((edtServicoFluxo_SrvPosPrcPgm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), "ZZZ9")) : context.localUtil.Format( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_SrvPosPrcPgm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoFluxo_SrvPosPrcPgm_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServicofluxo_srvposprcpgm_righttext_Internalname, "� %", "", "", lblServicofluxo_srvposprcpgm_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_44_3V176e( true) ;
         }
         else
         {
            wb_table6_44_3V176e( false) ;
         }
      }

      protected void wb_table5_34_3V176( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservicofluxo_srvposprctmp_Internalname, tblTablemergedservicofluxo_srvposprctmp_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_SrvPosPrcTmp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0, ",", "")), ((edtServicoFluxo_SrvPosPrcTmp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_SrvPosPrcTmp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServicoFluxo_SrvPosPrcTmp_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServicofluxo_srvposprctmp_righttext_Internalname, "� %", "", "", lblServicofluxo_srvposprctmp_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_34_3V176e( true) ;
         }
         else
         {
            wb_table5_34_3V176e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113V2 */
         E113V2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtServicoFluxo_Ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServicoFluxo_Ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICOFLUXO_ORDEM");
                  AnyError = 1;
                  GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1532ServicoFluxo_Ordem = 0;
                  n1532ServicoFluxo_Ordem = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
               }
               else
               {
                  A1532ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Ordem_Internalname), ",", "."));
                  n1532ServicoFluxo_Ordem = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
               }
               n1532ServicoFluxo_Ordem = ((0==A1532ServicoFluxo_Ordem) ? true : false);
               A1558ServicoFluxo_SrvPosNome = StringUtil.Upper( cgiGet( edtServicoFluxo_SrvPosNome_Internalname));
               n1558ServicoFluxo_SrvPosNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1558ServicoFluxo_SrvPosNome", A1558ServicoFluxo_SrvPosNome);
               A1527ServicoFluxo_SrvPosSigla = StringUtil.Upper( cgiGet( edtServicoFluxo_SrvPosSigla_Internalname));
               n1527ServicoFluxo_SrvPosSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1527ServicoFluxo_SrvPosSigla", A1527ServicoFluxo_SrvPosSigla);
               A1554ServicoFluxo_SrvPosPrcTmp = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_SrvPosPrcTmp_Internalname), ",", "."));
               n1554ServicoFluxo_SrvPosPrcTmp = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1554ServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0)));
               A1555ServicoFluxo_SrvPosPrcPgm = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_SrvPosPrcPgm_Internalname), ",", "."));
               n1555ServicoFluxo_SrvPosPrcPgm = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1555ServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0)));
               A1556ServicoFluxo_SrvPosPrcCnc = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_SrvPosPrcCnc_Internalname), ",", "."));
               n1556ServicoFluxo_SrvPosPrcCnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1556ServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0)));
               AV17RenumerarDesde = (short)(context.localUtil.CToN( cgiGet( edtavRenumerardesde_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RenumerarDesde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17RenumerarDesde), 4, 0)));
               AV24OrdemAnterior = (short)(context.localUtil.CToN( cgiGet( edtavOrdemanterior_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OrdemAnterior", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OrdemAnterior), 4, 0)));
               A1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
               /* Read saved values. */
               Z1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1528ServicoFluxo_Codigo"), ",", "."));
               Z1532ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( "Z1532ServicoFluxo_Ordem"), ",", "."));
               n1532ServicoFluxo_Ordem = ((0==A1532ServicoFluxo_Ordem) ? true : false);
               Z1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1522ServicoFluxo_ServicoCod"), ",", "."));
               Z1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( "Z1526ServicoFluxo_ServicoPos"), ",", "."));
               n1526ServicoFluxo_ServicoPos = ((0==A1526ServicoFluxo_ServicoPos) ? true : false);
               A1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1522ServicoFluxo_ServicoCod"), ",", "."));
               A1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( "Z1526ServicoFluxo_ServicoPos"), ",", "."));
               n1526ServicoFluxo_ServicoPos = false;
               n1526ServicoFluxo_ServicoPos = ((0==A1526ServicoFluxo_ServicoPos) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "N1522ServicoFluxo_ServicoCod"), ",", "."));
               N1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( "N1526ServicoFluxo_ServicoPos"), ",", "."));
               n1526ServicoFluxo_ServicoPos = ((0==A1526ServicoFluxo_ServicoPos) ? true : false);
               AV36ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSERVICOFLUXO_CODIGO"), ",", "."));
               AV11Insert_ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICOFLUXO_SERVICOCOD"), ",", "."));
               A1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "SERVICOFLUXO_SERVICOCOD"), ",", "."));
               AV13Insert_ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICOFLUXO_SERVICOPOS"), ",", "."));
               A1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( "SERVICOFLUXO_SERVICOPOS"), ",", "."));
               n1526ServicoFluxo_ServicoPos = ((0==A1526ServicoFluxo_ServicoPos) ? true : false);
               A1523ServicoFluxo_ServicoSigla = cgiGet( "SERVICOFLUXO_SERVICOSIGLA");
               n1523ServicoFluxo_ServicoSigla = false;
               AV15ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( "vSERVICOFLUXO_ORDEM"), ",", "."));
               A1533ServicoFluxo_ServicoTpHrq = (short)(context.localUtil.CToN( cgiGet( "SERVICOFLUXO_SERVICOTPHRQ"), ",", "."));
               n1533ServicoFluxo_ServicoTpHrq = false;
               AV37Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ServicoFluxo";
               A1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("servicofluxo:[SecurityCheckFailed value for]"+"ServicoFluxo_Codigo:"+context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("servicofluxo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("servicofluxo:[SecurityCheckFailed value for]"+"ServicoFluxo_ServicoCod:"+context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9"));
                  GXUtil.WriteLog("servicofluxo:[SecurityCheckFailed value for]"+"ServicoFluxo_ServicoPos:"+context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1528ServicoFluxo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode176 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode176;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound176 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3V0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SERVICOFLUXO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtServicoFluxo_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113V2 */
                           E113V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123V2 */
                           E123V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123V2 */
            E123V2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3V176( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3V176( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRenumerardesde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRenumerardesde_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdemanterior_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdemanterior_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3V0( )
      {
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3V176( ) ;
            }
            else
            {
               CheckExtendedTable3V176( ) ;
               CloseExtendedTableCursors3V176( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3V0( )
      {
      }

      protected void E113V2( )
      {
         /* Start Routine */
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV37Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV38GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GXV1), 8, 0)));
            while ( AV38GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV38GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "ServicoFluxo_ServicoCod") == 0 )
               {
                  AV11Insert_ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ServicoFluxo_ServicoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "ServicoFluxo_ServicoPos") == 0 )
               {
                  AV13Insert_ServicoFluxo_ServicoPos = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_ServicoFluxo_ServicoPos), 6, 0)));
               }
               AV38GXV1 = (int)(AV38GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GXV1), 8, 0)));
            }
         }
         edtavRenumerardesde_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRenumerardesde_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRenumerardesde_Visible), 5, 0)));
         edtavOrdemanterior_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdemanterior_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdemanterior_Visible), 5, 0)));
         edtServicoFluxo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_Codigo_Visible), 5, 0)));
      }

      protected void E123V2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            new prc_reordenafluxo(context ).execute(  "S",  A1522ServicoFluxo_ServicoCod,  AV15ServicoFluxo_Ordem,  9999,  "+") ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoFluxo_Ordem), 4, 0)));
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            if ( AV17RenumerarDesde > AV24OrdemAnterior )
            {
               new prc_reordenafluxo(context ).execute(  "S",  A1522ServicoFluxo_ServicoCod,  AV24OrdemAnterior,  AV17RenumerarDesde,  "+") ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OrdemAnterior", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OrdemAnterior), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RenumerarDesde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17RenumerarDesde), 4, 0)));
            }
            else if ( AV17RenumerarDesde < AV24OrdemAnterior )
            {
               new prc_reordenafluxo(context ).execute(  "S",  A1522ServicoFluxo_ServicoCod,  AV17RenumerarDesde,  AV24OrdemAnterior,  "-") ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RenumerarDesde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17RenumerarDesde), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OrdemAnterior", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OrdemAnterior), 4, 0)));
            }
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         if ( false )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
            {
               context.wjLoc = formatLink("wwservicofluxo.aspx") ;
               context.wjLocDisableFrm = 1;
            }
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void ZM3V176( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1532ServicoFluxo_Ordem = T003V3_A1532ServicoFluxo_Ordem[0];
               Z1522ServicoFluxo_ServicoCod = T003V3_A1522ServicoFluxo_ServicoCod[0];
               Z1526ServicoFluxo_ServicoPos = T003V3_A1526ServicoFluxo_ServicoPos[0];
            }
            else
            {
               Z1532ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
               Z1522ServicoFluxo_ServicoCod = A1522ServicoFluxo_ServicoCod;
               Z1526ServicoFluxo_ServicoPos = A1526ServicoFluxo_ServicoPos;
            }
         }
         if ( GX_JID == -14 )
         {
            Z1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
            Z1532ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
            Z1522ServicoFluxo_ServicoCod = A1522ServicoFluxo_ServicoCod;
            Z1526ServicoFluxo_ServicoPos = A1526ServicoFluxo_ServicoPos;
            Z1523ServicoFluxo_ServicoSigla = A1523ServicoFluxo_ServicoSigla;
            Z1533ServicoFluxo_ServicoTpHrq = A1533ServicoFluxo_ServicoTpHrq;
            Z1558ServicoFluxo_SrvPosNome = A1558ServicoFluxo_SrvPosNome;
            Z1527ServicoFluxo_SrvPosSigla = A1527ServicoFluxo_SrvPosSigla;
            Z1554ServicoFluxo_SrvPosPrcTmp = A1554ServicoFluxo_SrvPosPrcTmp;
            Z1555ServicoFluxo_SrvPosPrcPgm = A1555ServicoFluxo_SrvPosPrcPgm;
            Z1556ServicoFluxo_SrvPosPrcCnc = A1556ServicoFluxo_SrvPosPrcCnc;
         }
      }

      protected void standaloneNotModal( )
      {
         edtServicoFluxo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_Codigo_Enabled), 5, 0)));
         AV37Pgmname = "ServicoFluxo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Pgmname", AV37Pgmname);
         edtServicoFluxo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV36ServicoFluxo_Codigo) )
         {
            A1528ServicoFluxo_Codigo = AV36ServicoFluxo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_ServicoFluxo_ServicoPos) )
         {
            A1526ServicoFluxo_ServicoPos = AV13Insert_ServicoFluxo_ServicoPos;
            n1526ServicoFluxo_ServicoPos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1526ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ServicoFluxo_ServicoCod) )
         {
            A1522ServicoFluxo_ServicoCod = AV11Insert_ServicoFluxo_ServicoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003V5 */
            pr_default.execute(3, new Object[] {n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
            A1558ServicoFluxo_SrvPosNome = T003V5_A1558ServicoFluxo_SrvPosNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1558ServicoFluxo_SrvPosNome", A1558ServicoFluxo_SrvPosNome);
            n1558ServicoFluxo_SrvPosNome = T003V5_n1558ServicoFluxo_SrvPosNome[0];
            A1527ServicoFluxo_SrvPosSigla = T003V5_A1527ServicoFluxo_SrvPosSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1527ServicoFluxo_SrvPosSigla", A1527ServicoFluxo_SrvPosSigla);
            n1527ServicoFluxo_SrvPosSigla = T003V5_n1527ServicoFluxo_SrvPosSigla[0];
            A1554ServicoFluxo_SrvPosPrcTmp = T003V5_A1554ServicoFluxo_SrvPosPrcTmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1554ServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0)));
            n1554ServicoFluxo_SrvPosPrcTmp = T003V5_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1555ServicoFluxo_SrvPosPrcPgm = T003V5_A1555ServicoFluxo_SrvPosPrcPgm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1555ServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0)));
            n1555ServicoFluxo_SrvPosPrcPgm = T003V5_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1556ServicoFluxo_SrvPosPrcCnc = T003V5_A1556ServicoFluxo_SrvPosPrcCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1556ServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0)));
            n1556ServicoFluxo_SrvPosPrcCnc = T003V5_n1556ServicoFluxo_SrvPosPrcCnc[0];
            pr_default.close(3);
            /* Using cursor T003V4 */
            pr_default.execute(2, new Object[] {A1522ServicoFluxo_ServicoCod});
            A1523ServicoFluxo_ServicoSigla = T003V4_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = T003V4_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = T003V4_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = T003V4_n1533ServicoFluxo_ServicoTpHrq[0];
            pr_default.close(2);
            lblTbservico_Caption = A1523ServicoFluxo_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
         }
      }

      protected void Load3V176( )
      {
         /* Using cursor T003V6 */
         pr_default.execute(4, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound176 = 1;
            A1523ServicoFluxo_ServicoSigla = T003V6_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = T003V6_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = T003V6_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = T003V6_n1533ServicoFluxo_ServicoTpHrq[0];
            A1532ServicoFluxo_Ordem = T003V6_A1532ServicoFluxo_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
            n1532ServicoFluxo_Ordem = T003V6_n1532ServicoFluxo_Ordem[0];
            A1558ServicoFluxo_SrvPosNome = T003V6_A1558ServicoFluxo_SrvPosNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1558ServicoFluxo_SrvPosNome", A1558ServicoFluxo_SrvPosNome);
            n1558ServicoFluxo_SrvPosNome = T003V6_n1558ServicoFluxo_SrvPosNome[0];
            A1527ServicoFluxo_SrvPosSigla = T003V6_A1527ServicoFluxo_SrvPosSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1527ServicoFluxo_SrvPosSigla", A1527ServicoFluxo_SrvPosSigla);
            n1527ServicoFluxo_SrvPosSigla = T003V6_n1527ServicoFluxo_SrvPosSigla[0];
            A1554ServicoFluxo_SrvPosPrcTmp = T003V6_A1554ServicoFluxo_SrvPosPrcTmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1554ServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0)));
            n1554ServicoFluxo_SrvPosPrcTmp = T003V6_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1555ServicoFluxo_SrvPosPrcPgm = T003V6_A1555ServicoFluxo_SrvPosPrcPgm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1555ServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0)));
            n1555ServicoFluxo_SrvPosPrcPgm = T003V6_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1556ServicoFluxo_SrvPosPrcCnc = T003V6_A1556ServicoFluxo_SrvPosPrcCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1556ServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0)));
            n1556ServicoFluxo_SrvPosPrcCnc = T003V6_n1556ServicoFluxo_SrvPosPrcCnc[0];
            A1522ServicoFluxo_ServicoCod = T003V6_A1522ServicoFluxo_ServicoCod[0];
            A1526ServicoFluxo_ServicoPos = T003V6_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = T003V6_n1526ServicoFluxo_ServicoPos[0];
            ZM3V176( -14) ;
         }
         pr_default.close(4);
         OnLoadActions3V176( ) ;
      }

      protected void OnLoadActions3V176( )
      {
         AV15ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoFluxo_Ordem), 4, 0)));
         AV24OrdemAnterior = A1532ServicoFluxo_Ordem;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OrdemAnterior", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OrdemAnterior), 4, 0)));
         lblTbservico_Caption = A1523ServicoFluxo_ServicoSigla;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
      }

      protected void CheckExtendedTable3V176( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         AV15ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoFluxo_Ordem), 4, 0)));
         AV24OrdemAnterior = A1532ServicoFluxo_Ordem;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OrdemAnterior", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OrdemAnterior), 4, 0)));
         if ( (0==A1532ServicoFluxo_Ordem) )
         {
            GX_msglist.addItem("Ordem � obrigat�rio!", 1, "SERVICOFLUXO_ORDEM");
            AnyError = 1;
            GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003V4 */
         pr_default.execute(2, new Object[] {A1522ServicoFluxo_ServicoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico_Servico Fluxo'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1523ServicoFluxo_ServicoSigla = T003V4_A1523ServicoFluxo_ServicoSigla[0];
         n1523ServicoFluxo_ServicoSigla = T003V4_n1523ServicoFluxo_ServicoSigla[0];
         A1533ServicoFluxo_ServicoTpHrq = T003V4_A1533ServicoFluxo_ServicoTpHrq[0];
         n1533ServicoFluxo_ServicoTpHrq = T003V4_n1533ServicoFluxo_ServicoTpHrq[0];
         pr_default.close(2);
         lblTbservico_Caption = A1523ServicoFluxo_ServicoSigla;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
         /* Using cursor T003V5 */
         pr_default.execute(3, new Object[] {n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1526ServicoFluxo_ServicoPos) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico Fluxo_Servico Posterior'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1558ServicoFluxo_SrvPosNome = T003V5_A1558ServicoFluxo_SrvPosNome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1558ServicoFluxo_SrvPosNome", A1558ServicoFluxo_SrvPosNome);
         n1558ServicoFluxo_SrvPosNome = T003V5_n1558ServicoFluxo_SrvPosNome[0];
         A1527ServicoFluxo_SrvPosSigla = T003V5_A1527ServicoFluxo_SrvPosSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1527ServicoFluxo_SrvPosSigla", A1527ServicoFluxo_SrvPosSigla);
         n1527ServicoFluxo_SrvPosSigla = T003V5_n1527ServicoFluxo_SrvPosSigla[0];
         A1554ServicoFluxo_SrvPosPrcTmp = T003V5_A1554ServicoFluxo_SrvPosPrcTmp[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1554ServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0)));
         n1554ServicoFluxo_SrvPosPrcTmp = T003V5_n1554ServicoFluxo_SrvPosPrcTmp[0];
         A1555ServicoFluxo_SrvPosPrcPgm = T003V5_A1555ServicoFluxo_SrvPosPrcPgm[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1555ServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0)));
         n1555ServicoFluxo_SrvPosPrcPgm = T003V5_n1555ServicoFluxo_SrvPosPrcPgm[0];
         A1556ServicoFluxo_SrvPosPrcCnc = T003V5_A1556ServicoFluxo_SrvPosPrcCnc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1556ServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0)));
         n1556ServicoFluxo_SrvPosPrcCnc = T003V5_n1556ServicoFluxo_SrvPosPrcCnc[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3V176( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_15( int A1522ServicoFluxo_ServicoCod )
      {
         /* Using cursor T003V7 */
         pr_default.execute(5, new Object[] {A1522ServicoFluxo_ServicoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico_Servico Fluxo'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1523ServicoFluxo_ServicoSigla = T003V7_A1523ServicoFluxo_ServicoSigla[0];
         n1523ServicoFluxo_ServicoSigla = T003V7_n1523ServicoFluxo_ServicoSigla[0];
         A1533ServicoFluxo_ServicoTpHrq = T003V7_A1533ServicoFluxo_ServicoTpHrq[0];
         n1533ServicoFluxo_ServicoTpHrq = T003V7_n1533ServicoFluxo_ServicoTpHrq[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1523ServicoFluxo_ServicoSigla))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_16( int A1526ServicoFluxo_ServicoPos )
      {
         /* Using cursor T003V8 */
         pr_default.execute(6, new Object[] {n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A1526ServicoFluxo_ServicoPos) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico Fluxo_Servico Posterior'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1558ServicoFluxo_SrvPosNome = T003V8_A1558ServicoFluxo_SrvPosNome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1558ServicoFluxo_SrvPosNome", A1558ServicoFluxo_SrvPosNome);
         n1558ServicoFluxo_SrvPosNome = T003V8_n1558ServicoFluxo_SrvPosNome[0];
         A1527ServicoFluxo_SrvPosSigla = T003V8_A1527ServicoFluxo_SrvPosSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1527ServicoFluxo_SrvPosSigla", A1527ServicoFluxo_SrvPosSigla);
         n1527ServicoFluxo_SrvPosSigla = T003V8_n1527ServicoFluxo_SrvPosSigla[0];
         A1554ServicoFluxo_SrvPosPrcTmp = T003V8_A1554ServicoFluxo_SrvPosPrcTmp[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1554ServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0)));
         n1554ServicoFluxo_SrvPosPrcTmp = T003V8_n1554ServicoFluxo_SrvPosPrcTmp[0];
         A1555ServicoFluxo_SrvPosPrcPgm = T003V8_A1555ServicoFluxo_SrvPosPrcPgm[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1555ServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0)));
         n1555ServicoFluxo_SrvPosPrcPgm = T003V8_n1555ServicoFluxo_SrvPosPrcPgm[0];
         A1556ServicoFluxo_SrvPosPrcCnc = T003V8_A1556ServicoFluxo_SrvPosPrcCnc[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1556ServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0)));
         n1556ServicoFluxo_SrvPosPrcCnc = T003V8_n1556ServicoFluxo_SrvPosPrcCnc[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1558ServicoFluxo_SrvPosNome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey3V176( )
      {
         /* Using cursor T003V9 */
         pr_default.execute(7, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound176 = 1;
         }
         else
         {
            RcdFound176 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003V3 */
         pr_default.execute(1, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3V176( 14) ;
            RcdFound176 = 1;
            A1528ServicoFluxo_Codigo = T003V3_A1528ServicoFluxo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
            A1532ServicoFluxo_Ordem = T003V3_A1532ServicoFluxo_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
            n1532ServicoFluxo_Ordem = T003V3_n1532ServicoFluxo_Ordem[0];
            A1522ServicoFluxo_ServicoCod = T003V3_A1522ServicoFluxo_ServicoCod[0];
            A1526ServicoFluxo_ServicoPos = T003V3_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = T003V3_n1526ServicoFluxo_ServicoPos[0];
            Z1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
            sMode176 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3V176( ) ;
            if ( AnyError == 1 )
            {
               RcdFound176 = 0;
               InitializeNonKey3V176( ) ;
            }
            Gx_mode = sMode176;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound176 = 0;
            InitializeNonKey3V176( ) ;
            sMode176 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode176;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3V176( ) ;
         if ( RcdFound176 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound176 = 0;
         /* Using cursor T003V10 */
         pr_default.execute(8, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003V10_A1528ServicoFluxo_Codigo[0] < A1528ServicoFluxo_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003V10_A1528ServicoFluxo_Codigo[0] > A1528ServicoFluxo_Codigo ) ) )
            {
               A1528ServicoFluxo_Codigo = T003V10_A1528ServicoFluxo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
               RcdFound176 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound176 = 0;
         /* Using cursor T003V11 */
         pr_default.execute(9, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T003V11_A1528ServicoFluxo_Codigo[0] > A1528ServicoFluxo_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T003V11_A1528ServicoFluxo_Codigo[0] < A1528ServicoFluxo_Codigo ) ) )
            {
               A1528ServicoFluxo_Codigo = T003V11_A1528ServicoFluxo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
               RcdFound176 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3V176( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3V176( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound176 == 1 )
            {
               if ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo )
               {
                  A1528ServicoFluxo_Codigo = Z1528ServicoFluxo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICOFLUXO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServicoFluxo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3V176( ) ;
                  GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3V176( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICOFLUXO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtServicoFluxo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3V176( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo )
         {
            A1528ServicoFluxo_Codigo = Z1528ServicoFluxo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICOFLUXO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServicoFluxo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3V176( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003V2 */
            pr_default.execute(0, new Object[] {A1528ServicoFluxo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoFluxo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1532ServicoFluxo_Ordem != T003V2_A1532ServicoFluxo_Ordem[0] ) || ( Z1522ServicoFluxo_ServicoCod != T003V2_A1522ServicoFluxo_ServicoCod[0] ) || ( Z1526ServicoFluxo_ServicoPos != T003V2_A1526ServicoFluxo_ServicoPos[0] ) )
            {
               if ( Z1532ServicoFluxo_Ordem != T003V2_A1532ServicoFluxo_Ordem[0] )
               {
                  GXUtil.WriteLog("servicofluxo:[seudo value changed for attri]"+"ServicoFluxo_Ordem");
                  GXUtil.WriteLogRaw("Old: ",Z1532ServicoFluxo_Ordem);
                  GXUtil.WriteLogRaw("Current: ",T003V2_A1532ServicoFluxo_Ordem[0]);
               }
               if ( Z1522ServicoFluxo_ServicoCod != T003V2_A1522ServicoFluxo_ServicoCod[0] )
               {
                  GXUtil.WriteLog("servicofluxo:[seudo value changed for attri]"+"ServicoFluxo_ServicoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1522ServicoFluxo_ServicoCod);
                  GXUtil.WriteLogRaw("Current: ",T003V2_A1522ServicoFluxo_ServicoCod[0]);
               }
               if ( Z1526ServicoFluxo_ServicoPos != T003V2_A1526ServicoFluxo_ServicoPos[0] )
               {
                  GXUtil.WriteLog("servicofluxo:[seudo value changed for attri]"+"ServicoFluxo_ServicoPos");
                  GXUtil.WriteLogRaw("Old: ",Z1526ServicoFluxo_ServicoPos);
                  GXUtil.WriteLogRaw("Current: ",T003V2_A1526ServicoFluxo_ServicoPos[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoFluxo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3V176( )
      {
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3V176( 0) ;
            CheckOptimisticConcurrency3V176( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3V176( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3V176( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003V12 */
                     pr_default.execute(10, new Object[] {n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem, A1522ServicoFluxo_ServicoCod, n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
                     A1528ServicoFluxo_Codigo = T003V12_A1528ServicoFluxo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3V0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3V176( ) ;
            }
            EndLevel3V176( ) ;
         }
         CloseExtendedTableCursors3V176( ) ;
      }

      protected void Update3V176( )
      {
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3V176( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3V176( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3V176( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003V13 */
                     pr_default.execute(11, new Object[] {n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem, A1522ServicoFluxo_ServicoCod, n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos, A1528ServicoFluxo_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoFluxo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3V176( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3V176( ) ;
         }
         CloseExtendedTableCursors3V176( ) ;
      }

      protected void DeferredUpdate3V176( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3V176( ) ;
            AfterConfirm3V176( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3V176( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003V14 */
                  pr_default.execute(12, new Object[] {A1528ServicoFluxo_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode176 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3V176( ) ;
         Gx_mode = sMode176;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3V176( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV15ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoFluxo_Ordem), 4, 0)));
            AV24OrdemAnterior = A1532ServicoFluxo_Ordem;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OrdemAnterior", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OrdemAnterior), 4, 0)));
            /* Using cursor T003V15 */
            pr_default.execute(13, new Object[] {A1522ServicoFluxo_ServicoCod});
            A1523ServicoFluxo_ServicoSigla = T003V15_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = T003V15_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = T003V15_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = T003V15_n1533ServicoFluxo_ServicoTpHrq[0];
            pr_default.close(13);
            lblTbservico_Caption = A1523ServicoFluxo_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
            /* Using cursor T003V16 */
            pr_default.execute(14, new Object[] {n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
            A1558ServicoFluxo_SrvPosNome = T003V16_A1558ServicoFluxo_SrvPosNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1558ServicoFluxo_SrvPosNome", A1558ServicoFluxo_SrvPosNome);
            n1558ServicoFluxo_SrvPosNome = T003V16_n1558ServicoFluxo_SrvPosNome[0];
            A1527ServicoFluxo_SrvPosSigla = T003V16_A1527ServicoFluxo_SrvPosSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1527ServicoFluxo_SrvPosSigla", A1527ServicoFluxo_SrvPosSigla);
            n1527ServicoFluxo_SrvPosSigla = T003V16_n1527ServicoFluxo_SrvPosSigla[0];
            A1554ServicoFluxo_SrvPosPrcTmp = T003V16_A1554ServicoFluxo_SrvPosPrcTmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1554ServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0)));
            n1554ServicoFluxo_SrvPosPrcTmp = T003V16_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1555ServicoFluxo_SrvPosPrcPgm = T003V16_A1555ServicoFluxo_SrvPosPrcPgm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1555ServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0)));
            n1555ServicoFluxo_SrvPosPrcPgm = T003V16_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1556ServicoFluxo_SrvPosPrcCnc = T003V16_A1556ServicoFluxo_SrvPosPrcCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1556ServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0)));
            n1556ServicoFluxo_SrvPosPrcCnc = T003V16_n1556ServicoFluxo_SrvPosPrcCnc[0];
            pr_default.close(14);
         }
      }

      protected void EndLevel3V176( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.CommitDataStores( "ServicoFluxo");
            if ( AnyError == 0 )
            {
               ConfirmValues3V0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.RollbackDataStores( "ServicoFluxo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3V176( )
      {
         /* Scan By routine */
         /* Using cursor T003V17 */
         pr_default.execute(15);
         RcdFound176 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound176 = 1;
            A1528ServicoFluxo_Codigo = T003V17_A1528ServicoFluxo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3V176( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound176 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound176 = 1;
            A1528ServicoFluxo_Codigo = T003V17_A1528ServicoFluxo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3V176( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm3V176( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3V176( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3V176( )
      {
         /* Before Update Rules */
         if ( A1532ServicoFluxo_Ordem != AV24OrdemAnterior )
         {
            A1532ServicoFluxo_Ordem = 0;
            n1532ServicoFluxo_Ordem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
         }
      }

      protected void BeforeDelete3V176( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3V176( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3V176( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3V176( )
      {
         edtServicoFluxo_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_Ordem_Enabled), 5, 0)));
         edtServicoFluxo_SrvPosNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_SrvPosNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_SrvPosNome_Enabled), 5, 0)));
         edtServicoFluxo_SrvPosSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_SrvPosSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_SrvPosSigla_Enabled), 5, 0)));
         edtServicoFluxo_SrvPosPrcTmp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_SrvPosPrcTmp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_SrvPosPrcTmp_Enabled), 5, 0)));
         edtServicoFluxo_SrvPosPrcPgm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_SrvPosPrcPgm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_SrvPosPrcPgm_Enabled), 5, 0)));
         edtServicoFluxo_SrvPosPrcCnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_SrvPosPrcCnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_SrvPosPrcCnc_Enabled), 5, 0)));
         edtavOrdemanterior_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdemanterior_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdemanterior_Enabled), 5, 0)));
         edtServicoFluxo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoFluxo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3V0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117285750");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV36ServicoFluxo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1528ServicoFluxo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1532ServicoFluxo_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1522ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1526ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1526ServicoFluxo_ServicoPos), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N1526ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSERVICOFLUXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36ServicoFluxo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICOFLUXO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICOFLUXO_SERVICOPOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_ServicoFluxo_ServicoPos), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_SERVICOPOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_SERVICOSIGLA", StringUtil.RTrim( A1523ServicoFluxo_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "vSERVICOFLUXO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15ServicoFluxo_Ordem), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_SERVICOTPHRQ", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV37Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICOFLUXO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36ServicoFluxo_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ServicoFluxo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicofluxo:[SendSecurityCheck value for]"+"ServicoFluxo_Codigo:"+context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("servicofluxo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("servicofluxo:[SendSecurityCheck value for]"+"ServicoFluxo_ServicoCod:"+context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9"));
         GXUtil.WriteLog("servicofluxo:[SendSecurityCheck value for]"+"ServicoFluxo_ServicoPos:"+context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV36ServicoFluxo_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ServicoFluxo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Processo do Servi�o" ;
      }

      protected void InitializeNonKey3V176( )
      {
         A1522ServicoFluxo_ServicoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
         A1526ServicoFluxo_ServicoPos = 0;
         n1526ServicoFluxo_ServicoPos = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1526ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0)));
         AV15ServicoFluxo_Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15ServicoFluxo_Ordem), 4, 0)));
         AV24OrdemAnterior = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24OrdemAnterior", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24OrdemAnterior), 4, 0)));
         A1523ServicoFluxo_ServicoSigla = "";
         n1523ServicoFluxo_ServicoSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1523ServicoFluxo_ServicoSigla", A1523ServicoFluxo_ServicoSigla);
         A1533ServicoFluxo_ServicoTpHrq = 0;
         n1533ServicoFluxo_ServicoTpHrq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1533ServicoFluxo_ServicoTpHrq", StringUtil.LTrim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0)));
         A1532ServicoFluxo_Ordem = 0;
         n1532ServicoFluxo_Ordem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
         n1532ServicoFluxo_Ordem = ((0==A1532ServicoFluxo_Ordem) ? true : false);
         A1558ServicoFluxo_SrvPosNome = "";
         n1558ServicoFluxo_SrvPosNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1558ServicoFluxo_SrvPosNome", A1558ServicoFluxo_SrvPosNome);
         A1527ServicoFluxo_SrvPosSigla = "";
         n1527ServicoFluxo_SrvPosSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1527ServicoFluxo_SrvPosSigla", A1527ServicoFluxo_SrvPosSigla);
         A1554ServicoFluxo_SrvPosPrcTmp = 0;
         n1554ServicoFluxo_SrvPosPrcTmp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1554ServicoFluxo_SrvPosPrcTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1554ServicoFluxo_SrvPosPrcTmp), 4, 0)));
         A1555ServicoFluxo_SrvPosPrcPgm = 0;
         n1555ServicoFluxo_SrvPosPrcPgm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1555ServicoFluxo_SrvPosPrcPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1555ServicoFluxo_SrvPosPrcPgm), 4, 0)));
         A1556ServicoFluxo_SrvPosPrcCnc = 0;
         n1556ServicoFluxo_SrvPosPrcCnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1556ServicoFluxo_SrvPosPrcCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1556ServicoFluxo_SrvPosPrcCnc), 4, 0)));
         Z1532ServicoFluxo_Ordem = 0;
         Z1522ServicoFluxo_ServicoCod = 0;
         Z1526ServicoFluxo_ServicoPos = 0;
      }

      protected void InitAll3V176( )
      {
         A1528ServicoFluxo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
         InitializeNonKey3V176( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117285760");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servicofluxo.js", "?20203117285760");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbservico_Internalname = "TBSERVICO";
         lblTextblockservicofluxo_ordem_Internalname = "TEXTBLOCKSERVICOFLUXO_ORDEM";
         edtServicoFluxo_Ordem_Internalname = "SERVICOFLUXO_ORDEM";
         lblTextblockservicofluxo_srvposnome_Internalname = "TEXTBLOCKSERVICOFLUXO_SRVPOSNOME";
         edtServicoFluxo_SrvPosNome_Internalname = "SERVICOFLUXO_SRVPOSNOME";
         lblTextblockservicofluxo_srvpossigla_Internalname = "TEXTBLOCKSERVICOFLUXO_SRVPOSSIGLA";
         edtServicoFluxo_SrvPosSigla_Internalname = "SERVICOFLUXO_SRVPOSSIGLA";
         lblTextblockservicofluxo_srvposprctmp_Internalname = "TEXTBLOCKSERVICOFLUXO_SRVPOSPRCTMP";
         edtServicoFluxo_SrvPosPrcTmp_Internalname = "SERVICOFLUXO_SRVPOSPRCTMP";
         lblServicofluxo_srvposprctmp_righttext_Internalname = "SERVICOFLUXO_SRVPOSPRCTMP_RIGHTTEXT";
         tblTablemergedservicofluxo_srvposprctmp_Internalname = "TABLEMERGEDSERVICOFLUXO_SRVPOSPRCTMP";
         lblTextblockservicofluxo_srvposprcpgm_Internalname = "TEXTBLOCKSERVICOFLUXO_SRVPOSPRCPGM";
         edtServicoFluxo_SrvPosPrcPgm_Internalname = "SERVICOFLUXO_SRVPOSPRCPGM";
         lblServicofluxo_srvposprcpgm_righttext_Internalname = "SERVICOFLUXO_SRVPOSPRCPGM_RIGHTTEXT";
         tblTablemergedservicofluxo_srvposprcpgm_Internalname = "TABLEMERGEDSERVICOFLUXO_SRVPOSPRCPGM";
         lblTextblockservicofluxo_srvposprccnc_Internalname = "TEXTBLOCKSERVICOFLUXO_SRVPOSPRCCNC";
         edtServicoFluxo_SrvPosPrcCnc_Internalname = "SERVICOFLUXO_SRVPOSPRCCNC";
         lblServicofluxo_srvposprccnc_righttext_Internalname = "SERVICOFLUXO_SRVPOSPRCCNC_RIGHTTEXT";
         tblTablemergedservicofluxo_srvposprccnc_Internalname = "TABLEMERGEDSERVICOFLUXO_SRVPOSPRCCNC";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablecontent_Internalname = "TABLECONTENT";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavRenumerardesde_Internalname = "vRENUMERARDESDE";
         edtavOrdemanterior_Internalname = "vORDEMANTERIOR";
         edtServicoFluxo_Codigo_Internalname = "SERVICOFLUXO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Processo do Servi�o";
         edtServicoFluxo_SrvPosPrcTmp_Jsonclick = "";
         edtServicoFluxo_SrvPosPrcTmp_Enabled = 0;
         edtServicoFluxo_SrvPosPrcPgm_Jsonclick = "";
         edtServicoFluxo_SrvPosPrcPgm_Enabled = 0;
         edtServicoFluxo_SrvPosPrcCnc_Jsonclick = "";
         edtServicoFluxo_SrvPosPrcCnc_Enabled = 0;
         edtServicoFluxo_SrvPosSigla_Jsonclick = "";
         edtServicoFluxo_SrvPosSigla_Enabled = 0;
         edtServicoFluxo_SrvPosNome_Jsonclick = "";
         edtServicoFluxo_SrvPosNome_Enabled = 0;
         edtServicoFluxo_Ordem_Jsonclick = "";
         edtServicoFluxo_Ordem_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         lblTbservico_Caption = "Servi�o";
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         edtServicoFluxo_Codigo_Jsonclick = "";
         edtServicoFluxo_Codigo_Enabled = 0;
         edtServicoFluxo_Codigo_Visible = 1;
         edtavOrdemanterior_Jsonclick = "";
         edtavOrdemanterior_Enabled = 0;
         edtavOrdemanterior_Visible = 1;
         edtavRenumerardesde_Jsonclick = "";
         edtavRenumerardesde_Enabled = 0;
         edtavRenumerardesde_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Servicofluxo_ordem( short GX_Parm1 ,
                                            short GX_Parm2 ,
                                            short GX_Parm3 )
      {
         A1532ServicoFluxo_Ordem = GX_Parm1;
         n1532ServicoFluxo_Ordem = false;
         AV15ServicoFluxo_Ordem = GX_Parm2;
         AV24OrdemAnterior = GX_Parm3;
         AV15ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
         AV24OrdemAnterior = A1532ServicoFluxo_Ordem;
         if ( (0==A1532ServicoFluxo_Ordem) )
         {
            GX_msglist.addItem("Ordem � obrigat�rio!", 1, "SERVICOFLUXO_ORDEM");
            AnyError = 1;
            GX_FocusControl = edtServicoFluxo_Ordem_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15ServicoFluxo_Ordem), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24OrdemAnterior), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV36ServicoFluxo_Codigo',fld:'vSERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123V2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A1522ServicoFluxo_ServicoCod',fld:'SERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoFluxo_Ordem',fld:'vSERVICOFLUXO_ORDEM',pic:'ZZZ9',nv:0},{av:'AV17RenumerarDesde',fld:'vRENUMERARDESDE',pic:'ZZZ9',nv:0},{av:'AV24OrdemAnterior',fld:'vORDEMANTERIOR',pic:'ZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         lblTbservico_Jsonclick = "";
         TempTags = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockservicofluxo_ordem_Jsonclick = "";
         lblTextblockservicofluxo_srvposnome_Jsonclick = "";
         A1558ServicoFluxo_SrvPosNome = "";
         lblTextblockservicofluxo_srvpossigla_Jsonclick = "";
         A1527ServicoFluxo_SrvPosSigla = "";
         lblTextblockservicofluxo_srvposprctmp_Jsonclick = "";
         lblTextblockservicofluxo_srvposprcpgm_Jsonclick = "";
         lblTextblockservicofluxo_srvposprccnc_Jsonclick = "";
         lblServicofluxo_srvposprccnc_righttext_Jsonclick = "";
         lblServicofluxo_srvposprcpgm_righttext_Jsonclick = "";
         lblServicofluxo_srvposprctmp_righttext_Jsonclick = "";
         A1523ServicoFluxo_ServicoSigla = "";
         AV37Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode176 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1523ServicoFluxo_ServicoSigla = "";
         Z1558ServicoFluxo_SrvPosNome = "";
         Z1527ServicoFluxo_SrvPosSigla = "";
         T003V5_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         T003V5_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         T003V5_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         T003V5_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         T003V5_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         T003V5_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         T003V5_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         T003V5_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         T003V5_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         T003V5_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         T003V4_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         T003V4_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         T003V4_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         T003V4_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         T003V6_A1528ServicoFluxo_Codigo = new int[1] ;
         T003V6_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         T003V6_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         T003V6_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         T003V6_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         T003V6_A1532ServicoFluxo_Ordem = new short[1] ;
         T003V6_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         T003V6_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         T003V6_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         T003V6_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         T003V6_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         T003V6_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         T003V6_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         T003V6_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         T003V6_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         T003V6_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         T003V6_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         T003V6_A1522ServicoFluxo_ServicoCod = new int[1] ;
         T003V6_A1526ServicoFluxo_ServicoPos = new int[1] ;
         T003V6_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         T003V7_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         T003V7_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         T003V7_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         T003V7_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         T003V8_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         T003V8_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         T003V8_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         T003V8_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         T003V8_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         T003V8_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         T003V8_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         T003V8_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         T003V8_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         T003V8_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         T003V9_A1528ServicoFluxo_Codigo = new int[1] ;
         T003V3_A1528ServicoFluxo_Codigo = new int[1] ;
         T003V3_A1532ServicoFluxo_Ordem = new short[1] ;
         T003V3_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         T003V3_A1522ServicoFluxo_ServicoCod = new int[1] ;
         T003V3_A1526ServicoFluxo_ServicoPos = new int[1] ;
         T003V3_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         T003V10_A1528ServicoFluxo_Codigo = new int[1] ;
         T003V11_A1528ServicoFluxo_Codigo = new int[1] ;
         T003V2_A1528ServicoFluxo_Codigo = new int[1] ;
         T003V2_A1532ServicoFluxo_Ordem = new short[1] ;
         T003V2_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         T003V2_A1522ServicoFluxo_ServicoCod = new int[1] ;
         T003V2_A1526ServicoFluxo_ServicoPos = new int[1] ;
         T003V2_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         T003V12_A1528ServicoFluxo_Codigo = new int[1] ;
         T003V15_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         T003V15_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         T003V15_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         T003V15_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         T003V16_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         T003V16_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         T003V16_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         T003V16_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         T003V16_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         T003V16_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         T003V16_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         T003V16_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         T003V16_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         T003V16_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         T003V17_A1528ServicoFluxo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicofluxo__default(),
            new Object[][] {
                new Object[] {
               T003V2_A1528ServicoFluxo_Codigo, T003V2_A1532ServicoFluxo_Ordem, T003V2_n1532ServicoFluxo_Ordem, T003V2_A1522ServicoFluxo_ServicoCod, T003V2_A1526ServicoFluxo_ServicoPos, T003V2_n1526ServicoFluxo_ServicoPos
               }
               , new Object[] {
               T003V3_A1528ServicoFluxo_Codigo, T003V3_A1532ServicoFluxo_Ordem, T003V3_n1532ServicoFluxo_Ordem, T003V3_A1522ServicoFluxo_ServicoCod, T003V3_A1526ServicoFluxo_ServicoPos, T003V3_n1526ServicoFluxo_ServicoPos
               }
               , new Object[] {
               T003V4_A1523ServicoFluxo_ServicoSigla, T003V4_n1523ServicoFluxo_ServicoSigla, T003V4_A1533ServicoFluxo_ServicoTpHrq, T003V4_n1533ServicoFluxo_ServicoTpHrq
               }
               , new Object[] {
               T003V5_A1558ServicoFluxo_SrvPosNome, T003V5_n1558ServicoFluxo_SrvPosNome, T003V5_A1527ServicoFluxo_SrvPosSigla, T003V5_n1527ServicoFluxo_SrvPosSigla, T003V5_A1554ServicoFluxo_SrvPosPrcTmp, T003V5_n1554ServicoFluxo_SrvPosPrcTmp, T003V5_A1555ServicoFluxo_SrvPosPrcPgm, T003V5_n1555ServicoFluxo_SrvPosPrcPgm, T003V5_A1556ServicoFluxo_SrvPosPrcCnc, T003V5_n1556ServicoFluxo_SrvPosPrcCnc
               }
               , new Object[] {
               T003V6_A1528ServicoFluxo_Codigo, T003V6_A1523ServicoFluxo_ServicoSigla, T003V6_n1523ServicoFluxo_ServicoSigla, T003V6_A1533ServicoFluxo_ServicoTpHrq, T003V6_n1533ServicoFluxo_ServicoTpHrq, T003V6_A1532ServicoFluxo_Ordem, T003V6_n1532ServicoFluxo_Ordem, T003V6_A1558ServicoFluxo_SrvPosNome, T003V6_n1558ServicoFluxo_SrvPosNome, T003V6_A1527ServicoFluxo_SrvPosSigla,
               T003V6_n1527ServicoFluxo_SrvPosSigla, T003V6_A1554ServicoFluxo_SrvPosPrcTmp, T003V6_n1554ServicoFluxo_SrvPosPrcTmp, T003V6_A1555ServicoFluxo_SrvPosPrcPgm, T003V6_n1555ServicoFluxo_SrvPosPrcPgm, T003V6_A1556ServicoFluxo_SrvPosPrcCnc, T003V6_n1556ServicoFluxo_SrvPosPrcCnc, T003V6_A1522ServicoFluxo_ServicoCod, T003V6_A1526ServicoFluxo_ServicoPos, T003V6_n1526ServicoFluxo_ServicoPos
               }
               , new Object[] {
               T003V7_A1523ServicoFluxo_ServicoSigla, T003V7_n1523ServicoFluxo_ServicoSigla, T003V7_A1533ServicoFluxo_ServicoTpHrq, T003V7_n1533ServicoFluxo_ServicoTpHrq
               }
               , new Object[] {
               T003V8_A1558ServicoFluxo_SrvPosNome, T003V8_n1558ServicoFluxo_SrvPosNome, T003V8_A1527ServicoFluxo_SrvPosSigla, T003V8_n1527ServicoFluxo_SrvPosSigla, T003V8_A1554ServicoFluxo_SrvPosPrcTmp, T003V8_n1554ServicoFluxo_SrvPosPrcTmp, T003V8_A1555ServicoFluxo_SrvPosPrcPgm, T003V8_n1555ServicoFluxo_SrvPosPrcPgm, T003V8_A1556ServicoFluxo_SrvPosPrcCnc, T003V8_n1556ServicoFluxo_SrvPosPrcCnc
               }
               , new Object[] {
               T003V9_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               T003V10_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               T003V11_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               T003V12_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003V15_A1523ServicoFluxo_ServicoSigla, T003V15_n1523ServicoFluxo_ServicoSigla, T003V15_A1533ServicoFluxo_ServicoTpHrq, T003V15_n1533ServicoFluxo_ServicoTpHrq
               }
               , new Object[] {
               T003V16_A1558ServicoFluxo_SrvPosNome, T003V16_n1558ServicoFluxo_SrvPosNome, T003V16_A1527ServicoFluxo_SrvPosSigla, T003V16_n1527ServicoFluxo_SrvPosSigla, T003V16_A1554ServicoFluxo_SrvPosPrcTmp, T003V16_n1554ServicoFluxo_SrvPosPrcTmp, T003V16_A1555ServicoFluxo_SrvPosPrcPgm, T003V16_n1555ServicoFluxo_SrvPosPrcPgm, T003V16_A1556ServicoFluxo_SrvPosPrcCnc, T003V16_n1556ServicoFluxo_SrvPosPrcCnc
               }
               , new Object[] {
               T003V17_A1528ServicoFluxo_Codigo
               }
            }
         );
         AV37Pgmname = "ServicoFluxo";
      }

      private short Z1532ServicoFluxo_Ordem ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short AV17RenumerarDesde ;
      private short AV24OrdemAnterior ;
      private short A1532ServicoFluxo_Ordem ;
      private short A1556ServicoFluxo_SrvPosPrcCnc ;
      private short A1555ServicoFluxo_SrvPosPrcPgm ;
      private short A1554ServicoFluxo_SrvPosPrcTmp ;
      private short AV15ServicoFluxo_Ordem ;
      private short A1533ServicoFluxo_ServicoTpHrq ;
      private short RcdFound176 ;
      private short GX_JID ;
      private short Z1533ServicoFluxo_ServicoTpHrq ;
      private short Z1554ServicoFluxo_SrvPosPrcTmp ;
      private short Z1555ServicoFluxo_SrvPosPrcPgm ;
      private short Z1556ServicoFluxo_SrvPosPrcCnc ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV36ServicoFluxo_Codigo ;
      private int Z1528ServicoFluxo_Codigo ;
      private int Z1522ServicoFluxo_ServicoCod ;
      private int Z1526ServicoFluxo_ServicoPos ;
      private int N1522ServicoFluxo_ServicoCod ;
      private int N1526ServicoFluxo_ServicoPos ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int A1526ServicoFluxo_ServicoPos ;
      private int AV36ServicoFluxo_Codigo ;
      private int trnEnded ;
      private int edtavRenumerardesde_Enabled ;
      private int edtavRenumerardesde_Visible ;
      private int edtavOrdemanterior_Enabled ;
      private int edtavOrdemanterior_Visible ;
      private int A1528ServicoFluxo_Codigo ;
      private int edtServicoFluxo_Codigo_Enabled ;
      private int edtServicoFluxo_Codigo_Visible ;
      private int lblTbjava_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtServicoFluxo_Ordem_Enabled ;
      private int edtServicoFluxo_SrvPosNome_Enabled ;
      private int edtServicoFluxo_SrvPosSigla_Enabled ;
      private int edtServicoFluxo_SrvPosPrcCnc_Enabled ;
      private int edtServicoFluxo_SrvPosPrcPgm_Enabled ;
      private int edtServicoFluxo_SrvPosPrcTmp_Enabled ;
      private int AV11Insert_ServicoFluxo_ServicoCod ;
      private int AV13Insert_ServicoFluxo_ServicoPos ;
      private int AV38GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtServicoFluxo_Ordem_Internalname ;
      private String edtavRenumerardesde_Internalname ;
      private String edtavRenumerardesde_Jsonclick ;
      private String edtavOrdemanterior_Internalname ;
      private String edtavOrdemanterior_Jsonclick ;
      private String edtServicoFluxo_Codigo_Internalname ;
      private String edtServicoFluxo_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbservico_Internalname ;
      private String lblTbservico_Caption ;
      private String lblTbservico_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservicofluxo_ordem_Internalname ;
      private String lblTextblockservicofluxo_ordem_Jsonclick ;
      private String edtServicoFluxo_Ordem_Jsonclick ;
      private String lblTextblockservicofluxo_srvposnome_Internalname ;
      private String lblTextblockservicofluxo_srvposnome_Jsonclick ;
      private String edtServicoFluxo_SrvPosNome_Internalname ;
      private String A1558ServicoFluxo_SrvPosNome ;
      private String edtServicoFluxo_SrvPosNome_Jsonclick ;
      private String lblTextblockservicofluxo_srvpossigla_Internalname ;
      private String lblTextblockservicofluxo_srvpossigla_Jsonclick ;
      private String edtServicoFluxo_SrvPosSigla_Internalname ;
      private String A1527ServicoFluxo_SrvPosSigla ;
      private String edtServicoFluxo_SrvPosSigla_Jsonclick ;
      private String lblTextblockservicofluxo_srvposprctmp_Internalname ;
      private String lblTextblockservicofluxo_srvposprctmp_Jsonclick ;
      private String lblTextblockservicofluxo_srvposprcpgm_Internalname ;
      private String lblTextblockservicofluxo_srvposprcpgm_Jsonclick ;
      private String lblTextblockservicofluxo_srvposprccnc_Internalname ;
      private String lblTextblockservicofluxo_srvposprccnc_Jsonclick ;
      private String tblTablemergedservicofluxo_srvposprccnc_Internalname ;
      private String edtServicoFluxo_SrvPosPrcCnc_Internalname ;
      private String edtServicoFluxo_SrvPosPrcCnc_Jsonclick ;
      private String lblServicofluxo_srvposprccnc_righttext_Internalname ;
      private String lblServicofluxo_srvposprccnc_righttext_Jsonclick ;
      private String tblTablemergedservicofluxo_srvposprcpgm_Internalname ;
      private String edtServicoFluxo_SrvPosPrcPgm_Internalname ;
      private String edtServicoFluxo_SrvPosPrcPgm_Jsonclick ;
      private String lblServicofluxo_srvposprcpgm_righttext_Internalname ;
      private String lblServicofluxo_srvposprcpgm_righttext_Jsonclick ;
      private String tblTablemergedservicofluxo_srvposprctmp_Internalname ;
      private String edtServicoFluxo_SrvPosPrcTmp_Internalname ;
      private String edtServicoFluxo_SrvPosPrcTmp_Jsonclick ;
      private String lblServicofluxo_srvposprctmp_righttext_Internalname ;
      private String lblServicofluxo_srvposprctmp_righttext_Jsonclick ;
      private String A1523ServicoFluxo_ServicoSigla ;
      private String AV37Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode176 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1523ServicoFluxo_ServicoSigla ;
      private String Z1558ServicoFluxo_SrvPosNome ;
      private String Z1527ServicoFluxo_SrvPosSigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1532ServicoFluxo_Ordem ;
      private bool n1558ServicoFluxo_SrvPosNome ;
      private bool n1527ServicoFluxo_SrvPosSigla ;
      private bool n1554ServicoFluxo_SrvPosPrcTmp ;
      private bool n1555ServicoFluxo_SrvPosPrcPgm ;
      private bool n1556ServicoFluxo_SrvPosPrcCnc ;
      private bool n1523ServicoFluxo_ServicoSigla ;
      private bool n1533ServicoFluxo_ServicoTpHrq ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T003V5_A1558ServicoFluxo_SrvPosNome ;
      private bool[] T003V5_n1558ServicoFluxo_SrvPosNome ;
      private String[] T003V5_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] T003V5_n1527ServicoFluxo_SrvPosSigla ;
      private short[] T003V5_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] T003V5_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] T003V5_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] T003V5_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] T003V5_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] T003V5_n1556ServicoFluxo_SrvPosPrcCnc ;
      private String[] T003V4_A1523ServicoFluxo_ServicoSigla ;
      private bool[] T003V4_n1523ServicoFluxo_ServicoSigla ;
      private short[] T003V4_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] T003V4_n1533ServicoFluxo_ServicoTpHrq ;
      private int[] T003V6_A1528ServicoFluxo_Codigo ;
      private String[] T003V6_A1523ServicoFluxo_ServicoSigla ;
      private bool[] T003V6_n1523ServicoFluxo_ServicoSigla ;
      private short[] T003V6_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] T003V6_n1533ServicoFluxo_ServicoTpHrq ;
      private short[] T003V6_A1532ServicoFluxo_Ordem ;
      private bool[] T003V6_n1532ServicoFluxo_Ordem ;
      private String[] T003V6_A1558ServicoFluxo_SrvPosNome ;
      private bool[] T003V6_n1558ServicoFluxo_SrvPosNome ;
      private String[] T003V6_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] T003V6_n1527ServicoFluxo_SrvPosSigla ;
      private short[] T003V6_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] T003V6_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] T003V6_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] T003V6_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] T003V6_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] T003V6_n1556ServicoFluxo_SrvPosPrcCnc ;
      private int[] T003V6_A1522ServicoFluxo_ServicoCod ;
      private int[] T003V6_A1526ServicoFluxo_ServicoPos ;
      private bool[] T003V6_n1526ServicoFluxo_ServicoPos ;
      private String[] T003V7_A1523ServicoFluxo_ServicoSigla ;
      private bool[] T003V7_n1523ServicoFluxo_ServicoSigla ;
      private short[] T003V7_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] T003V7_n1533ServicoFluxo_ServicoTpHrq ;
      private String[] T003V8_A1558ServicoFluxo_SrvPosNome ;
      private bool[] T003V8_n1558ServicoFluxo_SrvPosNome ;
      private String[] T003V8_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] T003V8_n1527ServicoFluxo_SrvPosSigla ;
      private short[] T003V8_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] T003V8_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] T003V8_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] T003V8_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] T003V8_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] T003V8_n1556ServicoFluxo_SrvPosPrcCnc ;
      private int[] T003V9_A1528ServicoFluxo_Codigo ;
      private int[] T003V3_A1528ServicoFluxo_Codigo ;
      private short[] T003V3_A1532ServicoFluxo_Ordem ;
      private bool[] T003V3_n1532ServicoFluxo_Ordem ;
      private int[] T003V3_A1522ServicoFluxo_ServicoCod ;
      private int[] T003V3_A1526ServicoFluxo_ServicoPos ;
      private bool[] T003V3_n1526ServicoFluxo_ServicoPos ;
      private int[] T003V10_A1528ServicoFluxo_Codigo ;
      private int[] T003V11_A1528ServicoFluxo_Codigo ;
      private int[] T003V2_A1528ServicoFluxo_Codigo ;
      private short[] T003V2_A1532ServicoFluxo_Ordem ;
      private bool[] T003V2_n1532ServicoFluxo_Ordem ;
      private int[] T003V2_A1522ServicoFluxo_ServicoCod ;
      private int[] T003V2_A1526ServicoFluxo_ServicoPos ;
      private bool[] T003V2_n1526ServicoFluxo_ServicoPos ;
      private int[] T003V12_A1528ServicoFluxo_Codigo ;
      private String[] T003V15_A1523ServicoFluxo_ServicoSigla ;
      private bool[] T003V15_n1523ServicoFluxo_ServicoSigla ;
      private short[] T003V15_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] T003V15_n1533ServicoFluxo_ServicoTpHrq ;
      private String[] T003V16_A1558ServicoFluxo_SrvPosNome ;
      private bool[] T003V16_n1558ServicoFluxo_SrvPosNome ;
      private String[] T003V16_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] T003V16_n1527ServicoFluxo_SrvPosSigla ;
      private short[] T003V16_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] T003V16_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] T003V16_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] T003V16_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] T003V16_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] T003V16_n1556ServicoFluxo_SrvPosPrcCnc ;
      private int[] T003V17_A1528ServicoFluxo_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class servicofluxo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003V6 ;
          prmT003V6 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V4 ;
          prmT003V4 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V5 ;
          prmT003V5 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V7 ;
          prmT003V7 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V8 ;
          prmT003V8 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V9 ;
          prmT003V9 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V3 ;
          prmT003V3 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V10 ;
          prmT003V10 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V11 ;
          prmT003V11 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V2 ;
          prmT003V2 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V12 ;
          prmT003V12 = new Object[] {
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V13 ;
          prmT003V13 = new Object[] {
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V14 ;
          prmT003V14 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V15 ;
          prmT003V15 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V16 ;
          prmT003V16 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003V17 ;
          prmT003V17 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T003V2", "SELECT [ServicoFluxo_Codigo], [ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, [ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos FROM [ServicoFluxo] WITH (UPDLOCK) WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V2,1,0,true,false )
             ,new CursorDef("T003V3", "SELECT [ServicoFluxo_Codigo], [ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, [ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V3,1,0,true,false )
             ,new CursorDef("T003V4", "SELECT [Servico_Sigla] AS ServicoFluxo_ServicoSigla, [Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V4,1,0,true,false )
             ,new CursorDef("T003V5", "SELECT [Servico_Nome] AS ServicoFluxo_SrvPosNome, [Servico_Sigla] AS ServicoFluxo_SrvPosSigla, [Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, [Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, [Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoPos ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V5,1,0,true,false )
             ,new CursorDef("T003V6", "SELECT TM1.[ServicoFluxo_Codigo], T2.[Servico_Sigla] AS ServicoFluxo_ServicoSigla, T2.[Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq, TM1.[ServicoFluxo_Ordem], T3.[Servico_Nome] AS ServicoFluxo_SrvPosNome, T3.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T3.[Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, T3.[Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, T3.[Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc, TM1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, TM1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos FROM (([ServicoFluxo] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[ServicoFluxo_ServicoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = TM1.[ServicoFluxo_ServicoPos]) WHERE TM1.[ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ORDER BY TM1.[ServicoFluxo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003V6,100,0,true,false )
             ,new CursorDef("T003V7", "SELECT [Servico_Sigla] AS ServicoFluxo_ServicoSigla, [Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V7,1,0,true,false )
             ,new CursorDef("T003V8", "SELECT [Servico_Nome] AS ServicoFluxo_SrvPosNome, [Servico_Sigla] AS ServicoFluxo_SrvPosSigla, [Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, [Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, [Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoPos ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V8,1,0,true,false )
             ,new CursorDef("T003V9", "SELECT [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003V9,1,0,true,false )
             ,new CursorDef("T003V10", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE ( [ServicoFluxo_Codigo] > @ServicoFluxo_Codigo) ORDER BY [ServicoFluxo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003V10,1,0,true,true )
             ,new CursorDef("T003V11", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE ( [ServicoFluxo_Codigo] < @ServicoFluxo_Codigo) ORDER BY [ServicoFluxo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003V11,1,0,true,true )
             ,new CursorDef("T003V12", "INSERT INTO [ServicoFluxo]([ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod], [ServicoFluxo_ServicoPos]) VALUES(@ServicoFluxo_Ordem, @ServicoFluxo_ServicoCod, @ServicoFluxo_ServicoPos); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003V12)
             ,new CursorDef("T003V13", "UPDATE [ServicoFluxo] SET [ServicoFluxo_Ordem]=@ServicoFluxo_Ordem, [ServicoFluxo_ServicoCod]=@ServicoFluxo_ServicoCod, [ServicoFluxo_ServicoPos]=@ServicoFluxo_ServicoPos  WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo", GxErrorMask.GX_NOMASK,prmT003V13)
             ,new CursorDef("T003V14", "DELETE FROM [ServicoFluxo]  WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo", GxErrorMask.GX_NOMASK,prmT003V14)
             ,new CursorDef("T003V15", "SELECT [Servico_Sigla] AS ServicoFluxo_ServicoSigla, [Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V15,1,0,true,false )
             ,new CursorDef("T003V16", "SELECT [Servico_Nome] AS ServicoFluxo_SrvPosNome, [Servico_Sigla] AS ServicoFluxo_SrvPosSigla, [Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, [Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, [Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoPos ",true, GxErrorMask.GX_NOMASK, false, this,prmT003V16,1,0,true,false )
             ,new CursorDef("T003V17", "SELECT [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) ORDER BY [ServicoFluxo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003V17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
