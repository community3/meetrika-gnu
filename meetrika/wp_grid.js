/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:22:43.50
*/
gx.evt.autoSkip = false;
gx.define('wp_grid', false, function () {
   this.ServerClass =  "wp_grid" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV114Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.A456ContagemResultado_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CODIGO",'.') ;
   };
   this.Validv_Tfcontagemresultado_dataentrega=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADO_DATAENTREGA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV65TFContagemResultado_DataEntrega)==0) || new gx.date.gxdate( this.AV65TFContagemResultado_DataEntrega ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado_Data Entrega fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultado_dataentrega_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADO_DATAENTREGA_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV66TFContagemResultado_DataEntrega_To)==0) || new gx.date.gxdate( this.AV66TFContagemResultado_DataEntrega_To ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado_Data Entrega_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultado_dataentregaauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV67DDO_ContagemResultado_DataEntregaAuxDate)==0) || new gx.date.gxdate( this.AV67DDO_ContagemResultado_DataEntregaAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado_Data Entrega Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultado_dataentregaauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV68DDO_ContagemResultado_DataEntregaAuxDateTo)==0) || new gx.date.gxdate( this.AV68DDO_ContagemResultado_DataEntregaAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado_Data Entrega Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultado_datadmn=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADO_DATADMN");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV71TFContagemResultado_DataDmn)==0) || new gx.date.gxdate( this.AV71TFContagemResultado_DataDmn ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado_Data Dmn fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfcontagemresultado_datadmn_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFCONTAGEMRESULTADO_DATADMN_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV72TFContagemResultado_DataDmn_To)==0) || new gx.date.gxdate( this.AV72TFContagemResultado_DataDmn_To ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFContagem Resultado_Data Dmn_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultado_datadmnauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV73DDO_ContagemResultado_DataDmnAuxDate)==0) || new gx.date.gxdate( this.AV73DDO_ContagemResultado_DataDmnAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado_Data Dmn Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultado_datadmnauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV74DDO_ContagemResultado_DataDmnAuxDateTo)==0) || new gx.date.gxdate( this.AV74DDO_ContagemResultado_DataDmnAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado_Data Dmn Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultado_horaentregaauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV79DDO_ContagemResultado_HoraEntregaAuxDate)==0) || new gx.date.gxdate( this.AV79DDO_ContagemResultado_HoraEntregaAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado_Hora Entrega Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_contagemresultado_horaentregaauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV80DDO_ContagemResultado_HoraEntregaAuxDateTo)==0) || new gx.date.gxdate( this.AV80DDO_ContagemResultado_HoraEntregaAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Contagem Resultado_Hora Entrega Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e21kv1_client=function()
   {
      this.clearMessages();
      this.refreshOutputs([]);
   };
   this.e11kv2_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12kv2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADO_DATAENTREGA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13kv2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADO_DATADMN.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14kv2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADO_HORAENTREGA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e15kv2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e16kv2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e17kv2_client=function()
   {
      this.executeServerEvent("DDO_CONTAGEMRESULTADO_SERVICOSIGLA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e22kv2_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e23kv2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,17,22,25,29,30,31,32,33,34,35,36,42,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,68,70,72,74,76,78];
   this.GXLastCtrlId =78;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",28,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wp_grid",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,gxui.GridExtension,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",29,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",30,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(472,31,"CONTAGEMRESULTADO_DATAENTREGA","","","ContagemResultado_DataEntrega","date",0,"px",8,8,"right",null,[],472,"ContagemResultado_DataEntrega",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(471,32,"CONTAGEMRESULTADO_DATADMN","","","ContagemResultado_DataDmn","date",56,"px",8,8,"right",null,[],471,"ContagemResultado_DataDmn",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(912,33,"CONTAGEMRESULTADO_HORAENTREGA","","","ContagemResultado_HoraEntrega","dtime",0,"px",5,5,"right",null,[],912,"ContagemResultado_HoraEntrega",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(500,34,"CONTAGEMRESULTADO_CONTRATADAPESSOANOM","","","ContagemResultado_ContratadaPessoaNom","char",0,"px",100,80,"left",null,[],500,"ContagemResultado_ContratadaPessoaNom",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(493,35,"CONTAGEMRESULTADO_DEMANDAFM","","","ContagemResultado_DemandaFM","svchar",80,"px",50,50,"left",null,[],493,"ContagemResultado_DemandaFM",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(801,36,"CONTAGEMRESULTADO_SERVICOSIGLA","","","ContagemResultado_ServicoSigla","char",0,"px",15,15,"left",null,[],801,"ContagemResultado_ServicoSigla",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.setRenderProp("Title", "Title", "", "str");
   GridContainer.setRenderProp("AddToParentGxUIControl", "Addtoparentgxuicontrol", false, "bool");
   GridContainer.setRenderProp("SelectedRow", "Selectedrow", '', "int");
   GridContainer.setRenderProp("UseThemeClasses", "Usethemeclasses", false, "bool");
   GridContainer.setRenderProp("EnableColumnLocking", "Enablecolumnlocking", false, "bool");
   GridContainer.setRenderProp("SelectionModel", "Selectionmodel", "RowSelectionModel", "str");
   GridContainer.setRenderProp("EditModel", "Editmodel", "CellEditModel", "str");
   GridContainer.setRenderProp("ClicksToEdit", "Clickstoedit", "1", "str");
   GridContainer.setRenderProp("DragDropGroup", "Dragdropgroup", "", "str");
   GridContainer.setRenderProp("DragDropText", "Dragdroptext", "", "str");
   GridContainer.setRenderProp("PrimaryButtonOnly", "Primarybuttononly", true, "bool");
   GridContainer.setRenderProp("Grouping", "Grouping", true, "bool");
   GridContainer.setRenderProp("ShowGroupingSummary", "Showgroupingsummary", false, "bool");
   GridContainer.setRenderProp("GroupField", "Groupfield", "ContagemResultado_DataEntrega", "str");
   GridContainer.setRenderProp("HideGroupedField", "Hidegroupedfield", false, "bool");
   GridContainer.setRenderProp("GroupTemplate", "Grouptemplate", "{name}", "str");
   GridContainer.setRenderProp("ForceFit", "Forcefit", false, "bool");
   GridContainer.setRenderProp("MinColumnWidth", "Mincolumnwidth", 25, "num");
   GridContainer.setRenderProp("EnableColumnHide", "Enablecolumnhide", true, "bool");
   GridContainer.setRenderProp("EnableColumnMove", "Enablecolumnmove", true, "bool");
   GridContainer.setRenderProp("RemoteSort", "Remotesort", true, "bool");
   GridContainer.setRenderProp("SortField", "Sortfield", "", "str");
   GridContainer.setRenderProp("SortOrder", "Sortorder", "ASC", "str");
   GridContainer.setRenderProp("FirstText", "Firsttext", "First Page", "str");
   GridContainer.setRenderProp("LastText", "Lasttext", "Last Page", "str");
   GridContainer.setRenderProp("NextText", "Nexttext", "Next Page", "str");
   GridContainer.setRenderProp("PreviousText", "Previoustext", "Previous Page", "str");
   GridContainer.setRenderProp("RefreshText", "Refreshtext", "Refresh", "str");
   GridContainer.setRenderProp("StatusText", "Statustext", "", "str");
   GridContainer.setRenderProp("Stateful", "Stateful", true, "bool");
   GridContainer.setRenderProp("StateId", "Stateid", "", "str");
   GridContainer.setRenderProp("Visible", "Visible", true, "boolean");
   GridContainer.setRenderProp("Enabled", "Enabled", true, "boolean");
   GridContainer.setRenderProp("Class", "Class", "WorkWithBorder WorkWith", "str");
   this.setGrid(GridContainer);
   this.DDO_CONTAGEMRESULTADO_DATAENTREGAContainer = gx.uc.getNew(this, 67, 46, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADO_DATAENTREGAContainer", "Ddo_contagemresultado_dataentrega");
   var DDO_CONTAGEMRESULTADO_DATAENTREGAContainer = this.DDO_CONTAGEMRESULTADO_DATAENTREGAContainer;
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.addV2CFunction('AV94DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV94DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV94DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.addV2CFunction('AV64ContagemResultado_DataEntregaTitleFilterData', "vCONTAGEMRESULTADO_DATAENTREGATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV64ContagemResultado_DataEntregaTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADO_DATAENTREGATITLEFILTERDATA",UC.ParentObject.AV64ContagemResultado_DataEntregaTitleFilterData); });
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.addEventHandler("OnOptionClicked", this.e12kv2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADO_DATAENTREGAContainer);
   this.DDO_CONTAGEMRESULTADO_DATADMNContainer = gx.uc.getNew(this, 69, 46, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADO_DATADMNContainer", "Ddo_contagemresultado_datadmn");
   var DDO_CONTAGEMRESULTADO_DATADMNContainer = this.DDO_CONTAGEMRESULTADO_DATADMNContainer;
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.addV2CFunction('AV94DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADO_DATADMNContainer.addC2VFunction(function(UC) { UC.ParentObject.AV94DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV94DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADO_DATADMNContainer.addV2CFunction('AV70ContagemResultado_DataDmnTitleFilterData', "vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADO_DATADMNContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70ContagemResultado_DataDmnTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA",UC.ParentObject.AV70ContagemResultado_DataDmnTitleFilterData); });
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADO_DATADMNContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADO_DATADMNContainer.addEventHandler("OnOptionClicked", this.e13kv2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADO_DATADMNContainer);
   this.DDO_CONTAGEMRESULTADO_HORAENTREGAContainer = gx.uc.getNew(this, 71, 46, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADO_HORAENTREGAContainer", "Ddo_contagemresultado_horaentrega");
   var DDO_CONTAGEMRESULTADO_HORAENTREGAContainer = this.DDO_CONTAGEMRESULTADO_HORAENTREGAContainer;
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.addV2CFunction('AV94DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV94DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV94DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.addV2CFunction('AV76ContagemResultado_HoraEntregaTitleFilterData', "vCONTAGEMRESULTADO_HORAENTREGATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV76ContagemResultado_HoraEntregaTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADO_HORAENTREGATITLEFILTERDATA",UC.ParentObject.AV76ContagemResultado_HoraEntregaTitleFilterData); });
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.addEventHandler("OnOptionClicked", this.e14kv2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADO_HORAENTREGAContainer);
   this.DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer = gx.uc.getNew(this, 73, 46, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer", "Ddo_contagemresultado_contratadapessoanom");
   var DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer = this.DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer;
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("DataListProc", "Datalistproc", "GetWP_GridFilterData", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.addV2CFunction('AV94DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV94DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV94DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.addV2CFunction('AV82ContagemResultado_ContratadaPessoaNomTitleFilterData', "vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV82ContagemResultado_ContratadaPessoaNomTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA",UC.ParentObject.AV82ContagemResultado_ContratadaPessoaNomTitleFilterData); });
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.addEventHandler("OnOptionClicked", this.e15kv2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer);
   this.DDO_CONTAGEMRESULTADO_DEMANDAFMContainer = gx.uc.getNew(this, 75, 46, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADO_DEMANDAFMContainer", "Ddo_contagemresultado_demandafm");
   var DDO_CONTAGEMRESULTADO_DEMANDAFMContainer = this.DDO_CONTAGEMRESULTADO_DEMANDAFMContainer;
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("DataListProc", "Datalistproc", "GetWP_GridFilterData", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.addV2CFunction('AV94DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV94DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV94DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.addV2CFunction('AV86ContagemResultado_DemandaFMTitleFilterData', "vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.addC2VFunction(function(UC) { UC.ParentObject.AV86ContagemResultado_DemandaFMTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA",UC.ParentObject.AV86ContagemResultado_DemandaFMTitleFilterData); });
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.addEventHandler("OnOptionClicked", this.e16kv2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADO_DEMANDAFMContainer);
   this.DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer = gx.uc.getNew(this, 77, 46, "BootstrapDropDownOptions", "DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer", "Ddo_contagemresultado_servicosigla");
   var DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer = this.DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer;
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("Icon", "Icon", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("Caption", "Caption", "", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("DataListProc", "Datalistproc", "GetWP_GridFilterData", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.addV2CFunction('AV94DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV94DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV94DDO_TitleSettingsIcons); });
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.addV2CFunction('AV90ContagemResultado_ServicoSiglaTitleFilterData', "vCONTAGEMRESULTADO_SERVICOSIGLATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV90ContagemResultado_ServicoSiglaTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vCONTAGEMRESULTADO_SERVICOSIGLATITLEFILTERDATA",UC.ParentObject.AV90ContagemResultado_ServicoSiglaTitleFilterData); });
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setProp("Class", "Class", "", "char");
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.addEventHandler("OnOptionClicked", this.e17kv2_client);
   this.setUserControl(DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 39, 29, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV96GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV96GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV96GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV97GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV97GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV97GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11kv2_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"CONTAGEMRESULTADOTITLE", format:0,grid:0};
   GXValidFnc[13]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[17]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[22]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[25]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[29]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV59Update",gxold:"OV59Update",gxvar:"AV59Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV59Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV59Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(28),gx.O.AV59Update,gx.O.AV112Update_GXI)},c2v:function(){gx.O.AV112Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV59Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(28))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(28))}, gxvar_GXI:'AV112Update_GXI',nac:gx.falseFn};
   GXValidFnc[30]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV60Delete",gxold:"OV60Delete",gxvar:"AV60Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV60Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV60Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(28),gx.O.AV60Delete,gx.O.AV113Delete_GXI)},c2v:function(){gx.O.AV113Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV60Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(28))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(28))}, gxvar_GXI:'AV113Delete_GXI',nac:gx.falseFn};
   GXValidFnc[31]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DATAENTREGA",gxz:"Z472ContagemResultado_DataEntrega",gxold:"O472ContagemResultado_DataEntrega",gxvar:"A472ContagemResultado_DataEntrega",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A472ContagemResultado_DataEntrega=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z472ContagemResultado_DataEntrega=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DATAENTREGA",row || gx.fn.currentGridRowImpl(28),gx.O.A472ContagemResultado_DataEntrega,0)},c2v:function(){if(this.val()!==undefined)gx.O.A472ContagemResultado_DataEntrega=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADO_DATAENTREGA",row || gx.fn.currentGridRowImpl(28))},nac:gx.falseFn};
   GXValidFnc[32]={lvl:2,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DATADMN",gxz:"Z471ContagemResultado_DataDmn",gxold:"O471ContagemResultado_DataDmn",gxvar:"A471ContagemResultado_DataDmn",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A471ContagemResultado_DataDmn=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z471ContagemResultado_DataDmn=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DATADMN",row || gx.fn.currentGridRowImpl(28),gx.O.A471ContagemResultado_DataDmn,0)},c2v:function(){if(this.val()!==undefined)gx.O.A471ContagemResultado_DataDmn=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADO_DATADMN",row || gx.fn.currentGridRowImpl(28))},nac:gx.falseFn};
   GXValidFnc[33]={lvl:2,type:"dtime",len:0,dec:5,sign:false,ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_HORAENTREGA",gxz:"Z912ContagemResultado_HoraEntrega",gxold:"O912ContagemResultado_HoraEntrega",gxvar:"A912ContagemResultado_HoraEntrega",dp:{f:0,st:true,wn:false,mf:false,pic:"99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A912ContagemResultado_HoraEntrega=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z912ContagemResultado_HoraEntrega=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_HORAENTREGA",row || gx.fn.currentGridRowImpl(28),gx.O.A912ContagemResultado_HoraEntrega,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A912ContagemResultado_HoraEntrega=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADO_HORAENTREGA",row || gx.fn.currentGridRowImpl(28))},nac:gx.falseFn};
   GXValidFnc[34]={lvl:2,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_CONTRATADAPESSOANOM",gxz:"Z500ContagemResultado_ContratadaPessoaNom",gxold:"O500ContagemResultado_ContratadaPessoaNom",gxvar:"A500ContagemResultado_ContratadaPessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A500ContagemResultado_ContratadaPessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z500ContagemResultado_ContratadaPessoaNom=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_CONTRATADAPESSOANOM",row || gx.fn.currentGridRowImpl(28),gx.O.A500ContagemResultado_ContratadaPessoaNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A500ContagemResultado_ContratadaPessoaNom=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_CONTRATADAPESSOANOM",row || gx.fn.currentGridRowImpl(28))},nac:gx.falseFn};
   GXValidFnc[35]={lvl:2,type:"svchar",len:50,dec:0,sign:false,ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DEMANDAFM",gxz:"Z493ContagemResultado_DemandaFM",gxold:"O493ContagemResultado_DemandaFM",gxvar:"A493ContagemResultado_DemandaFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A493ContagemResultado_DemandaFM=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z493ContagemResultado_DemandaFM=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DEMANDAFM",row || gx.fn.currentGridRowImpl(28),gx.O.A493ContagemResultado_DemandaFM,0)},c2v:function(){if(this.val()!==undefined)gx.O.A493ContagemResultado_DemandaFM=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_DEMANDAFM",row || gx.fn.currentGridRowImpl(28))},nac:gx.falseFn};
   GXValidFnc[36]={lvl:2,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:1,isacc:0,grid:28,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_SERVICOSIGLA",gxz:"Z801ContagemResultado_ServicoSigla",gxold:"O801ContagemResultado_ServicoSigla",gxvar:"A801ContagemResultado_ServicoSigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A801ContagemResultado_ServicoSigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z801ContagemResultado_ServicoSigla=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_SERVICOSIGLA",row || gx.fn.currentGridRowImpl(28),gx.O.A801ContagemResultado_ServicoSigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A801ContagemResultado_ServicoSigla=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_SERVICOSIGLA",row || gx.fn.currentGridRowImpl(28))},nac:gx.falseFn};
   GXValidFnc[42]={fld:"TBJAVA", format:1,grid:0};
   GXValidFnc[46]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultado_dataentrega,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_DATAENTREGA",gxz:"ZV65TFContagemResultado_DataEntrega",gxold:"OV65TFContagemResultado_DataEntrega",gxvar:"AV65TFContagemResultado_DataEntrega",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[46],ip:[46],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV65TFContagemResultado_DataEntrega=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV65TFContagemResultado_DataEntrega=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_DATAENTREGA",gx.O.AV65TFContagemResultado_DataEntrega,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV65TFContagemResultado_DataEntrega=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_DATAENTREGA")},nac:gx.falseFn};
   GXValidFnc[47]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultado_dataentrega_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_DATAENTREGA_TO",gxz:"ZV66TFContagemResultado_DataEntrega_To",gxold:"OV66TFContagemResultado_DataEntrega_To",gxvar:"AV66TFContagemResultado_DataEntrega_To",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[47],ip:[47],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV66TFContagemResultado_DataEntrega_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV66TFContagemResultado_DataEntrega_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_DATAENTREGA_TO",gx.O.AV66TFContagemResultado_DataEntrega_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV66TFContagemResultado_DataEntrega_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_DATAENTREGA_TO")},nac:gx.falseFn};
   GXValidFnc[48]={fld:"DDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATES",grid:0};
   GXValidFnc[49]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultado_dataentregaauxdate,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATE",gxz:"ZV67DDO_ContagemResultado_DataEntregaAuxDate",gxold:"OV67DDO_ContagemResultado_DataEntregaAuxDate",gxvar:"AV67DDO_ContagemResultado_DataEntregaAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[49],ip:[49],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV67DDO_ContagemResultado_DataEntregaAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV67DDO_ContagemResultado_DataEntregaAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATE",gx.O.AV67DDO_ContagemResultado_DataEntregaAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV67DDO_ContagemResultado_DataEntregaAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATE")},nac:gx.falseFn};
   GXValidFnc[50]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultado_dataentregaauxdateto,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATETO",gxz:"ZV68DDO_ContagemResultado_DataEntregaAuxDateTo",gxold:"OV68DDO_ContagemResultado_DataEntregaAuxDateTo",gxvar:"AV68DDO_ContagemResultado_DataEntregaAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[50],ip:[50],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV68DDO_ContagemResultado_DataEntregaAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV68DDO_ContagemResultado_DataEntregaAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATETO",gx.O.AV68DDO_ContagemResultado_DataEntregaAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV68DDO_ContagemResultado_DataEntregaAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_DATAENTREGAAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[51]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultado_datadmn,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_DATADMN",gxz:"ZV71TFContagemResultado_DataDmn",gxold:"OV71TFContagemResultado_DataDmn",gxvar:"AV71TFContagemResultado_DataDmn",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[51],ip:[51],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV71TFContagemResultado_DataDmn=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV71TFContagemResultado_DataDmn=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_DATADMN",gx.O.AV71TFContagemResultado_DataDmn,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV71TFContagemResultado_DataDmn=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_DATADMN")},nac:gx.falseFn};
   GXValidFnc[52]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfcontagemresultado_datadmn_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_DATADMN_TO",gxz:"ZV72TFContagemResultado_DataDmn_To",gxold:"OV72TFContagemResultado_DataDmn_To",gxvar:"AV72TFContagemResultado_DataDmn_To",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[52],ip:[52],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV72TFContagemResultado_DataDmn_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV72TFContagemResultado_DataDmn_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_DATADMN_TO",gx.O.AV72TFContagemResultado_DataDmn_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV72TFContagemResultado_DataDmn_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_DATADMN_TO")},nac:gx.falseFn};
   GXValidFnc[53]={fld:"DDO_CONTAGEMRESULTADO_DATADMNAUXDATES",grid:0};
   GXValidFnc[54]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultado_datadmnauxdate,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE",gxz:"ZV73DDO_ContagemResultado_DataDmnAuxDate",gxold:"OV73DDO_ContagemResultado_DataDmnAuxDate",gxvar:"AV73DDO_ContagemResultado_DataDmnAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[54],ip:[54],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV73DDO_ContagemResultado_DataDmnAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV73DDO_ContagemResultado_DataDmnAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE",gx.O.AV73DDO_ContagemResultado_DataDmnAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV73DDO_ContagemResultado_DataDmnAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_DATADMNAUXDATE")},nac:gx.falseFn};
   GXValidFnc[55]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultado_datadmnauxdateto,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO",gxz:"ZV74DDO_ContagemResultado_DataDmnAuxDateTo",gxold:"OV74DDO_ContagemResultado_DataDmnAuxDateTo",gxvar:"AV74DDO_ContagemResultado_DataDmnAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[55],ip:[55],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV74DDO_ContagemResultado_DataDmnAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV74DDO_ContagemResultado_DataDmnAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO",gx.O.AV74DDO_ContagemResultado_DataDmnAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV74DDO_ContagemResultado_DataDmnAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_DATADMNAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[56]={lvl:0,type:"dtime",len:0,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_HORAENTREGA",gxz:"ZV77TFContagemResultado_HoraEntrega",gxold:"OV77TFContagemResultado_HoraEntrega",gxvar:"AV77TFContagemResultado_HoraEntrega",dp:{f:0,st:true,wn:false,mf:false,pic:"99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV77TFContagemResultado_HoraEntrega=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV77TFContagemResultado_HoraEntrega=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_HORAENTREGA",gx.O.AV77TFContagemResultado_HoraEntrega,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV77TFContagemResultado_HoraEntrega=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADO_HORAENTREGA")},nac:gx.falseFn};
   GXValidFnc[57]={lvl:0,type:"dtime",len:0,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_HORAENTREGA_TO",gxz:"ZV78TFContagemResultado_HoraEntrega_To",gxold:"OV78TFContagemResultado_HoraEntrega_To",gxvar:"AV78TFContagemResultado_HoraEntrega_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV78TFContagemResultado_HoraEntrega_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV78TFContagemResultado_HoraEntrega_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_HORAENTREGA_TO",gx.O.AV78TFContagemResultado_HoraEntrega_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV78TFContagemResultado_HoraEntrega_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFCONTAGEMRESULTADO_HORAENTREGA_TO")},nac:gx.falseFn};
   GXValidFnc[58]={fld:"DDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATES",grid:0};
   GXValidFnc[59]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultado_horaentregaauxdate,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATE",gxz:"ZV79DDO_ContagemResultado_HoraEntregaAuxDate",gxold:"OV79DDO_ContagemResultado_HoraEntregaAuxDate",gxvar:"AV79DDO_ContagemResultado_HoraEntregaAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[59],ip:[59],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV79DDO_ContagemResultado_HoraEntregaAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV79DDO_ContagemResultado_HoraEntregaAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATE",gx.O.AV79DDO_ContagemResultado_HoraEntregaAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV79DDO_ContagemResultado_HoraEntregaAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATE")},nac:gx.falseFn};
   GXValidFnc[60]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_contagemresultado_horaentregaauxdateto,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATETO",gxz:"ZV80DDO_ContagemResultado_HoraEntregaAuxDateTo",gxold:"OV80DDO_ContagemResultado_HoraEntregaAuxDateTo",gxvar:"AV80DDO_ContagemResultado_HoraEntregaAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[60],ip:[60],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV80DDO_ContagemResultado_HoraEntregaAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV80DDO_ContagemResultado_HoraEntregaAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATETO",gx.O.AV80DDO_ContagemResultado_HoraEntregaAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV80DDO_ContagemResultado_HoraEntregaAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_HORAENTREGAAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[61]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM",gxz:"ZV83TFContagemResultado_ContratadaPessoaNom",gxold:"OV83TFContagemResultado_ContratadaPessoaNom",gxvar:"AV83TFContagemResultado_ContratadaPessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV83TFContagemResultado_ContratadaPessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV83TFContagemResultado_ContratadaPessoaNom=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM",gx.O.AV83TFContagemResultado_ContratadaPessoaNom,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV83TFContagemResultado_ContratadaPessoaNom=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM")},nac:gx.falseFn};
   GXValidFnc[62]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL",gxz:"ZV84TFContagemResultado_ContratadaPessoaNom_Sel",gxold:"OV84TFContagemResultado_ContratadaPessoaNom_Sel",gxvar:"AV84TFContagemResultado_ContratadaPessoaNom_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV84TFContagemResultado_ContratadaPessoaNom_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV84TFContagemResultado_ContratadaPessoaNom_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL",gx.O.AV84TFContagemResultado_ContratadaPessoaNom_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV84TFContagemResultado_ContratadaPessoaNom_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL")},nac:gx.falseFn};
   GXValidFnc[63]={lvl:0,type:"svchar",len:50,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_DEMANDAFM",gxz:"ZV87TFContagemResultado_DemandaFM",gxold:"OV87TFContagemResultado_DemandaFM",gxvar:"AV87TFContagemResultado_DemandaFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV87TFContagemResultado_DemandaFM=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV87TFContagemResultado_DemandaFM=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_DEMANDAFM",gx.O.AV87TFContagemResultado_DemandaFM,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV87TFContagemResultado_DemandaFM=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_DEMANDAFM")},nac:gx.falseFn};
   GXValidFnc[64]={lvl:0,type:"svchar",len:50,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_DEMANDAFM_SEL",gxz:"ZV88TFContagemResultado_DemandaFM_Sel",gxold:"OV88TFContagemResultado_DemandaFM_Sel",gxvar:"AV88TFContagemResultado_DemandaFM_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV88TFContagemResultado_DemandaFM_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV88TFContagemResultado_DemandaFM_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_DEMANDAFM_SEL",gx.O.AV88TFContagemResultado_DemandaFM_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV88TFContagemResultado_DemandaFM_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_DEMANDAFM_SEL")},nac:gx.falseFn};
   GXValidFnc[65]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_SERVICOSIGLA",gxz:"ZV91TFContagemResultado_ServicoSigla",gxold:"OV91TFContagemResultado_ServicoSigla",gxvar:"AV91TFContagemResultado_ServicoSigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV91TFContagemResultado_ServicoSigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV91TFContagemResultado_ServicoSigla=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_SERVICOSIGLA",gx.O.AV91TFContagemResultado_ServicoSigla,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV91TFContagemResultado_ServicoSigla=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_SERVICOSIGLA")},nac:gx.falseFn};
   GXValidFnc[66]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL",gxz:"ZV92TFContagemResultado_ServicoSigla_Sel",gxold:"OV92TFContagemResultado_ServicoSigla_Sel",gxvar:"AV92TFContagemResultado_ServicoSigla_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV92TFContagemResultado_ServicoSigla_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV92TFContagemResultado_ServicoSigla_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL",gx.O.AV92TFContagemResultado_ServicoSigla_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV92TFContagemResultado_ServicoSigla_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL")},nac:gx.falseFn};
   GXValidFnc[68]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE",gxz:"ZV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace",gxold:"OV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace",gxvar:"AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE",gx.O.AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[70]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE",gxz:"ZV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace",gxold:"OV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace",gxvar:"AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE",gx.O.AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[72]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE",gxz:"ZV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace",gxold:"OV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace",gxvar:"AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE",gx.O.AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[74]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE",gxz:"ZV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace",gxold:"OV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace",gxvar:"AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE",gx.O.AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[76]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE",gxz:"ZV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace",gxold:"OV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace",gxvar:"AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE",gx.O.AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[78]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE",gxz:"ZV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace",gxold:"OV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace",gxvar:"AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE",gx.O.AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.ZV59Update = "" ;
   this.OV59Update = "" ;
   this.ZV60Delete = "" ;
   this.OV60Delete = "" ;
   this.Z472ContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.O472ContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.Z471ContagemResultado_DataDmn = gx.date.nullDate() ;
   this.O471ContagemResultado_DataDmn = gx.date.nullDate() ;
   this.Z912ContagemResultado_HoraEntrega = gx.date.nullDate() ;
   this.O912ContagemResultado_HoraEntrega = gx.date.nullDate() ;
   this.Z500ContagemResultado_ContratadaPessoaNom = "" ;
   this.O500ContagemResultado_ContratadaPessoaNom = "" ;
   this.Z493ContagemResultado_DemandaFM = "" ;
   this.O493ContagemResultado_DemandaFM = "" ;
   this.Z801ContagemResultado_ServicoSigla = "" ;
   this.O801ContagemResultado_ServicoSigla = "" ;
   this.AV65TFContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.ZV65TFContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.OV65TFContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.AV66TFContagemResultado_DataEntrega_To = gx.date.nullDate() ;
   this.ZV66TFContagemResultado_DataEntrega_To = gx.date.nullDate() ;
   this.OV66TFContagemResultado_DataEntrega_To = gx.date.nullDate() ;
   this.AV67DDO_ContagemResultado_DataEntregaAuxDate = gx.date.nullDate() ;
   this.ZV67DDO_ContagemResultado_DataEntregaAuxDate = gx.date.nullDate() ;
   this.OV67DDO_ContagemResultado_DataEntregaAuxDate = gx.date.nullDate() ;
   this.AV68DDO_ContagemResultado_DataEntregaAuxDateTo = gx.date.nullDate() ;
   this.ZV68DDO_ContagemResultado_DataEntregaAuxDateTo = gx.date.nullDate() ;
   this.OV68DDO_ContagemResultado_DataEntregaAuxDateTo = gx.date.nullDate() ;
   this.AV71TFContagemResultado_DataDmn = gx.date.nullDate() ;
   this.ZV71TFContagemResultado_DataDmn = gx.date.nullDate() ;
   this.OV71TFContagemResultado_DataDmn = gx.date.nullDate() ;
   this.AV72TFContagemResultado_DataDmn_To = gx.date.nullDate() ;
   this.ZV72TFContagemResultado_DataDmn_To = gx.date.nullDate() ;
   this.OV72TFContagemResultado_DataDmn_To = gx.date.nullDate() ;
   this.AV73DDO_ContagemResultado_DataDmnAuxDate = gx.date.nullDate() ;
   this.ZV73DDO_ContagemResultado_DataDmnAuxDate = gx.date.nullDate() ;
   this.OV73DDO_ContagemResultado_DataDmnAuxDate = gx.date.nullDate() ;
   this.AV74DDO_ContagemResultado_DataDmnAuxDateTo = gx.date.nullDate() ;
   this.ZV74DDO_ContagemResultado_DataDmnAuxDateTo = gx.date.nullDate() ;
   this.OV74DDO_ContagemResultado_DataDmnAuxDateTo = gx.date.nullDate() ;
   this.AV77TFContagemResultado_HoraEntrega = gx.date.nullDate() ;
   this.ZV77TFContagemResultado_HoraEntrega = gx.date.nullDate() ;
   this.OV77TFContagemResultado_HoraEntrega = gx.date.nullDate() ;
   this.AV78TFContagemResultado_HoraEntrega_To = gx.date.nullDate() ;
   this.ZV78TFContagemResultado_HoraEntrega_To = gx.date.nullDate() ;
   this.OV78TFContagemResultado_HoraEntrega_To = gx.date.nullDate() ;
   this.AV79DDO_ContagemResultado_HoraEntregaAuxDate = gx.date.nullDate() ;
   this.ZV79DDO_ContagemResultado_HoraEntregaAuxDate = gx.date.nullDate() ;
   this.OV79DDO_ContagemResultado_HoraEntregaAuxDate = gx.date.nullDate() ;
   this.AV80DDO_ContagemResultado_HoraEntregaAuxDateTo = gx.date.nullDate() ;
   this.ZV80DDO_ContagemResultado_HoraEntregaAuxDateTo = gx.date.nullDate() ;
   this.OV80DDO_ContagemResultado_HoraEntregaAuxDateTo = gx.date.nullDate() ;
   this.AV83TFContagemResultado_ContratadaPessoaNom = "" ;
   this.ZV83TFContagemResultado_ContratadaPessoaNom = "" ;
   this.OV83TFContagemResultado_ContratadaPessoaNom = "" ;
   this.AV84TFContagemResultado_ContratadaPessoaNom_Sel = "" ;
   this.ZV84TFContagemResultado_ContratadaPessoaNom_Sel = "" ;
   this.OV84TFContagemResultado_ContratadaPessoaNom_Sel = "" ;
   this.AV87TFContagemResultado_DemandaFM = "" ;
   this.ZV87TFContagemResultado_DemandaFM = "" ;
   this.OV87TFContagemResultado_DemandaFM = "" ;
   this.AV88TFContagemResultado_DemandaFM_Sel = "" ;
   this.ZV88TFContagemResultado_DemandaFM_Sel = "" ;
   this.OV88TFContagemResultado_DemandaFM_Sel = "" ;
   this.AV91TFContagemResultado_ServicoSigla = "" ;
   this.ZV91TFContagemResultado_ServicoSigla = "" ;
   this.OV91TFContagemResultado_ServicoSigla = "" ;
   this.AV92TFContagemResultado_ServicoSigla_Sel = "" ;
   this.ZV92TFContagemResultado_ServicoSigla_Sel = "" ;
   this.OV92TFContagemResultado_ServicoSigla_Sel = "" ;
   this.AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = "" ;
   this.ZV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = "" ;
   this.OV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = "" ;
   this.AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = "" ;
   this.ZV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = "" ;
   this.OV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = "" ;
   this.AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = "" ;
   this.ZV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = "" ;
   this.OV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = "" ;
   this.AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = "" ;
   this.ZV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = "" ;
   this.OV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = "" ;
   this.AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "" ;
   this.ZV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "" ;
   this.OV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "" ;
   this.AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = "" ;
   this.ZV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = "" ;
   this.OV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = "" ;
   this.AV96GridCurrentPage = 0 ;
   this.AV65TFContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.AV66TFContagemResultado_DataEntrega_To = gx.date.nullDate() ;
   this.AV67DDO_ContagemResultado_DataEntregaAuxDate = gx.date.nullDate() ;
   this.AV68DDO_ContagemResultado_DataEntregaAuxDateTo = gx.date.nullDate() ;
   this.AV71TFContagemResultado_DataDmn = gx.date.nullDate() ;
   this.AV72TFContagemResultado_DataDmn_To = gx.date.nullDate() ;
   this.AV73DDO_ContagemResultado_DataDmnAuxDate = gx.date.nullDate() ;
   this.AV74DDO_ContagemResultado_DataDmnAuxDateTo = gx.date.nullDate() ;
   this.AV77TFContagemResultado_HoraEntrega = gx.date.nullDate() ;
   this.AV78TFContagemResultado_HoraEntrega_To = gx.date.nullDate() ;
   this.AV79DDO_ContagemResultado_HoraEntregaAuxDate = gx.date.nullDate() ;
   this.AV80DDO_ContagemResultado_HoraEntregaAuxDateTo = gx.date.nullDate() ;
   this.AV83TFContagemResultado_ContratadaPessoaNom = "" ;
   this.AV84TFContagemResultado_ContratadaPessoaNom_Sel = "" ;
   this.AV87TFContagemResultado_DemandaFM = "" ;
   this.AV88TFContagemResultado_DemandaFM_Sel = "" ;
   this.AV91TFContagemResultado_ServicoSigla = "" ;
   this.AV92TFContagemResultado_ServicoSigla_Sel = "" ;
   this.AV94DDO_TitleSettingsIcons = {} ;
   this.AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace = "" ;
   this.AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace = "" ;
   this.AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace = "" ;
   this.AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace = "" ;
   this.AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace = "" ;
   this.AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace = "" ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A490ContagemResultado_ContratadaCod = 0 ;
   this.A499ContagemResultado_ContratadaPessoaCod = 0 ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.A601ContagemResultado_Servico = 0 ;
   this.AV59Update = "" ;
   this.AV60Delete = "" ;
   this.A472ContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.A471ContagemResultado_DataDmn = gx.date.nullDate() ;
   this.A912ContagemResultado_HoraEntrega = gx.date.nullDate() ;
   this.A500ContagemResultado_ContratadaPessoaNom = "" ;
   this.A493ContagemResultado_DemandaFM = "" ;
   this.A801ContagemResultado_ServicoSigla = "" ;
   this.AV114Pgmname = "" ;
   this.Events = {"e11kv2_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12kv2_client": ["DDO_CONTAGEMRESULTADO_DATAENTREGA.ONOPTIONCLICKED", true] ,"e13kv2_client": ["DDO_CONTAGEMRESULTADO_DATADMN.ONOPTIONCLICKED", true] ,"e14kv2_client": ["DDO_CONTAGEMRESULTADO_HORAENTREGA.ONOPTIONCLICKED", true] ,"e15kv2_client": ["DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM.ONOPTIONCLICKED", true] ,"e16kv2_client": ["DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED", true] ,"e17kv2_client": ["DDO_CONTAGEMRESULTADO_SERVICOSIGLA.ONOPTIONCLICKED", true] ,"e22kv2_client": ["ENTER", true] ,"e23kv2_client": ["CANCEL", true] ,"e21kv1_client": ["'DOCERRAR'", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''}],[{av:'AV64ContagemResultado_DataEntregaTitleFilterData',fld:'vCONTAGEMRESULTADO_DATAENTREGATITLEFILTERDATA',pic:'',nv:null},{av:'AV70ContagemResultado_DataDmnTitleFilterData',fld:'vCONTAGEMRESULTADO_DATADMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV76ContagemResultado_HoraEntregaTitleFilterData',fld:'vCONTAGEMRESULTADO_HORAENTREGATITLEFILTERDATA',pic:'',nv:null},{av:'AV82ContagemResultado_ContratadaPessoaNomTitleFilterData',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV86ContagemResultado_DemandaFMTitleFilterData',fld:'vCONTAGEMRESULTADO_DEMANDAFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV90ContagemResultado_ServicoSiglaTitleFilterData',fld:'vCONTAGEMRESULTADO_SERVICOSIGLATITLEFILTERDATA',pic:'',nv:null},{ctrl:'CONTAGEMRESULTADO_DATAENTREGA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATAENTREGA","Title")',ctrl:'CONTAGEMRESULTADO_DATAENTREGA',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Title")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_HORAENTREGA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_HORAENTREGA","Title")',ctrl:'CONTAGEMRESULTADO_HORAENTREGA',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_CONTRATADAPESSOANOM","Title")',ctrl:'CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DEMANDAFM","Title")',ctrl:'CONTAGEMRESULTADO_DEMANDAFM',prop:'Title'},{ctrl:'CONTAGEMRESULTADO_SERVICOSIGLA',prop:'Titleformat'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_SERVICOSIGLA","Title")',ctrl:'CONTAGEMRESULTADO_SERVICOSIGLA',prop:'Title'},{av:'AV96GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV97GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_CONTAGEMRESULTADO_DATAENTREGA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADO_DATAENTREGA',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADO_DATAENTREGA',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADO_DATAENTREGAContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADO_DATAENTREGA',prop:'FilteredTextTo_get'}],[{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADO_DATADMN.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADO_DATADMNContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADO_DATADMNContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADO_DATADMNContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADO_DATADMN',prop:'FilteredTextTo_get'}],[{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADO_HORAENTREGA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADO_HORAENTREGA',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADO_HORAENTREGA',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADO_HORAENTREGAContainer.FilteredTextTo_get',ctrl:'DDO_CONTAGEMRESULTADO_HORAENTREGA',prop:'FilteredTextTo_get'}],[{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMContainer.SelectedValue_get',ctrl:'DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM',prop:'SelectedValue_get'}],[{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADO_DEMANDAFM.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADO_DEMANDAFMContainer.SelectedValue_get',ctrl:'DDO_CONTAGEMRESULTADO_DEMANDAFM',prop:'SelectedValue_get'}],[{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''}]];
   this.EvtParms["DDO_CONTAGEMRESULTADO_SERVICOSIGLA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV65TFContagemResultado_DataEntrega',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA',pic:'',nv:''},{av:'AV66TFContagemResultado_DataEntrega_To',fld:'vTFCONTAGEMRESULTADO_DATAENTREGA_TO',pic:'',nv:''},{av:'AV71TFContagemResultado_DataDmn',fld:'vTFCONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV72TFContagemResultado_DataDmn_To',fld:'vTFCONTAGEMRESULTADO_DATADMN_TO',pic:'',nv:''},{av:'AV77TFContagemResultado_HoraEntrega',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA',pic:'99:99',nv:''},{av:'AV78TFContagemResultado_HoraEntrega_To',fld:'vTFCONTAGEMRESULTADO_HORAENTREGA_TO',pic:'99:99',nv:''},{av:'AV83TFContagemResultado_ContratadaPessoaNom',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'AV84TFContagemResultado_ContratadaPessoaNom_Sel',fld:'vTFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL',pic:'@!',nv:''},{av:'AV87TFContagemResultado_DemandaFM',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV88TFContagemResultado_DemandaFM_Sel',fld:'vTFCONTAGEMRESULTADO_DEMANDAFM_SEL',pic:'',nv:''},{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''},{av:'AV69ddo_ContagemResultado_DataEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_ContagemResultado_DataDmnTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DATADMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_ContagemResultado_HoraEntregaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_HORAENTREGATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_ContagemResultado_ContratadaPessoaNomTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV89ddo_ContagemResultado_DemandaFMTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_DEMANDAFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV93ddo_ContagemResultado_ServicoSiglaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_SERVICOSIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV114Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.ActiveEventKey',ctrl:'DDO_CONTAGEMRESULTADO_SERVICOSIGLA',prop:'ActiveEventKey'},{av:'this.DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.FilteredText_get',ctrl:'DDO_CONTAGEMRESULTADO_SERVICOSIGLA',prop:'FilteredText_get'},{av:'this.DDO_CONTAGEMRESULTADO_SERVICOSIGLAContainer.SelectedValue_get',ctrl:'DDO_CONTAGEMRESULTADO_SERVICOSIGLA',prop:'SelectedValue_get'}],[{av:'AV91TFContagemResultado_ServicoSigla',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''},{av:'AV92TFContagemResultado_ServicoSigla_Sel',fld:'vTFCONTAGEMRESULTADO_SERVICOSIGLA_SEL',pic:'@!',nv:''}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV59Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV60Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'}]];
   this.EvtParms["'DOCERRAR'"] = [[],[]];
   this.setVCMap("AV114Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV114Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   GridContainer.addRefreshingVar(this.GXValidFnc[46]);
   GridContainer.addRefreshingVar(this.GXValidFnc[47]);
   GridContainer.addRefreshingVar(this.GXValidFnc[51]);
   GridContainer.addRefreshingVar(this.GXValidFnc[52]);
   GridContainer.addRefreshingVar(this.GXValidFnc[56]);
   GridContainer.addRefreshingVar(this.GXValidFnc[57]);
   GridContainer.addRefreshingVar(this.GXValidFnc[61]);
   GridContainer.addRefreshingVar(this.GXValidFnc[62]);
   GridContainer.addRefreshingVar(this.GXValidFnc[63]);
   GridContainer.addRefreshingVar(this.GXValidFnc[64]);
   GridContainer.addRefreshingVar(this.GXValidFnc[65]);
   GridContainer.addRefreshingVar(this.GXValidFnc[66]);
   GridContainer.addRefreshingVar(this.GXValidFnc[68]);
   GridContainer.addRefreshingVar(this.GXValidFnc[70]);
   GridContainer.addRefreshingVar(this.GXValidFnc[72]);
   GridContainer.addRefreshingVar(this.GXValidFnc[74]);
   GridContainer.addRefreshingVar(this.GXValidFnc[76]);
   GridContainer.addRefreshingVar(this.GXValidFnc[78]);
   GridContainer.addRefreshingVar({rfrVar:"AV114Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"A456ContagemResultado_Codigo"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_grid);
