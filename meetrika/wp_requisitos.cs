/*
               File: WP_Requisitos
        Description: Requisitos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:25:3.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_requisitos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_requisitos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_requisitos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavRequisito_prioridade = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridhst") == 0 )
            {
               nRC_GXsfl_89 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_89_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_89_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridhst_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridhst") == 0 )
            {
               subGridhst_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A471ContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               A493ContagemResultado_DemandaFM = GetNextPar( );
               n493ContagemResultado_DemandaFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
               AV14Solicitante = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Solicitante", AV14Solicitante);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"))));
               A892LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n892LogResponsavel_DemandaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               A1131LogResponsavel_Observacao = GetNextPar( );
               n1131LogResponsavel_Observacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1131LogResponsavel_Observacao", A1131LogResponsavel_Observacao);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridhst_refresh( subGridhst_Rows, A456ContagemResultado_Codigo, A471ContagemResultado_DataDmn, A493ContagemResultado_DemandaFM, AV14Solicitante, A892LogResponsavel_DemandaCod, A1131LogResponsavel_Observacao) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "WP_Requisitos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""));
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"Solicitante:"+StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!")));
               GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"ContagemResultado_DemandaFM:"+StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridrnf") == 0 )
            {
               nRC_GXsfl_299 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_299_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_299_idx = GetNextPar( );
               AV10Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV10Nome);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridrnf_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridrnf") == 0 )
            {
               AV10Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV10Nome);
               A1931Requisito_Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1931Requisito_Ordem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1931Requisito_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1931Requisito_Ordem), 3, 0)));
               A1999Requisito_ReqCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1999Requisito_ReqCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1999Requisito_ReqCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1999Requisito_ReqCod), 6, 0)));
               AV39Requisito_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Requisito_Codigo), 6, 0)));
               AV31i = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31i), 4, 0)));
               A2001Requisito_Identificador = GetNextPar( );
               n2001Requisito_Identificador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2001Requisito_Identificador", A2001Requisito_Identificador);
               A1927Requisito_Titulo = GetNextPar( );
               n1927Requisito_Titulo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1927Requisito_Titulo", A1927Requisito_Titulo);
               A1926Requisito_Agrupador = GetNextPar( );
               n1926Requisito_Agrupador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
               A1923Requisito_Descricao = GetNextPar( );
               n1923Requisito_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
               A1925Requisito_ReferenciaTecnica = GetNextPar( );
               n1925Requisito_ReferenciaTecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1925Requisito_ReferenciaTecnica", A1925Requisito_ReferenciaTecnica);
               A1929Requisito_Restricao = GetNextPar( );
               n1929Requisito_Restricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1929Requisito_Restricao", A1929Requisito_Restricao);
               A2002Requisito_Prioridade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n2002Requisito_Prioridade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2002Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A2002Requisito_Prioridade), 2, 0)));
               A1934Requisito_Status = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1934Requisito_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
               A2042TipoRequisito_Identificador = GetNextPar( );
               n2042TipoRequisito_Identificador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2042TipoRequisito_Identificador", A2042TipoRequisito_Identificador);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridrnf_refresh( AV10Nome, A1931Requisito_Ordem, A1999Requisito_ReqCod, AV39Requisito_Codigo, AV31i, A2001Requisito_Identificador, A1927Requisito_Titulo, A1926Requisito_Agrupador, A1923Requisito_Descricao, A1925Requisito_ReferenciaTecnica, A1929Requisito_Restricao, A2002Requisito_Prioridade, A1934Requisito_Status, A2042TipoRequisito_Identificador) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "WP_Requisitos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""));
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"Solicitante:"+StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!")));
               GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"ContagemResultado_DemandaFM:"+StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridrf") == 0 )
            {
               nRC_GXsfl_212 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_212_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_212_idx = GetNextPar( );
               edtavRequisito_codigo_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_codigo_Visible), 5, 0)));
               AV10Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV10Nome);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridrf_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridrf") == 0 )
            {
               edtavRequisito_codigo_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_codigo_Visible), 5, 0)));
               AV10Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV10Nome);
               A2003ContagemResultadoRequisito_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2003ContagemResultadoRequisito_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0)));
               A1926Requisito_Agrupador = GetNextPar( );
               n1926Requisito_Agrupador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1926Requisito_Agrupador", A1926Requisito_Agrupador);
               AV31i = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31i), 4, 0)));
               A2001Requisito_Identificador = GetNextPar( );
               n2001Requisito_Identificador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2001Requisito_Identificador", A2001Requisito_Identificador);
               A1923Requisito_Descricao = GetNextPar( );
               n1923Requisito_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1923Requisito_Descricao", A1923Requisito_Descricao);
               A2002Requisito_Prioridade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n2002Requisito_Prioridade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2002Requisito_Prioridade", StringUtil.LTrim( StringUtil.Str( (decimal)(A2002Requisito_Prioridade), 2, 0)));
               A1934Requisito_Status = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1934Requisito_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1934Requisito_Status", StringUtil.LTrim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)));
               A2042TipoRequisito_Identificador = GetNextPar( );
               n2042TipoRequisito_Identificador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2042TipoRequisito_Identificador", A2042TipoRequisito_Identificador);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridrf_refresh( AV10Nome, A2003ContagemResultadoRequisito_OSCod, A1926Requisito_Agrupador, AV31i, A2001Requisito_Identificador, A1923Requisito_Descricao, A2002Requisito_Prioridade, A1934Requisito_Status, A2042TipoRequisito_Identificador) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "WP_Requisitos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""));
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"Solicitante:"+StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!")));
               GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"ContagemResultado_DemandaFM:"+StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAQC2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavSolicitante_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitante_Enabled), 5, 0)));
               edtavResponsavel_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavResponsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavResponsavel_Enabled), 5, 0)));
               edtavData_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavData_Enabled), 5, 0)));
               edtavDemanda_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDemanda_Enabled), 5, 0)));
               edtavAutor_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAutor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAutor_Enabled), 5, 0)));
               edtavDescricaohis_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricaohis_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricaohis_Enabled), 5, 0)));
               edtavVersao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVersao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVersao_Enabled), 5, 0)));
               edtavRequisitante_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisitante_Enabled), 5, 0)));
               edtavSetor_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSetor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSetor_Enabled), 5, 0)));
               edtavServico_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_Enabled), 5, 0)));
               edtavIdentificador_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIdentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIdentificador_Enabled), 5, 0)));
               edtavTipo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipo_Enabled), 5, 0)));
               edtavAgrupador_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
               edtavNome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
               edtavDescricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
               edtavPrioridade_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrioridade_Enabled), 5, 0)));
               edtavStatus_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavStatus_Enabled), 5, 0)));
               edtavReferencia_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferencia_Enabled), 5, 0)));
               edtavRestricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRestricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRestricao_Enabled), 5, 0)));
               edtavRequisito_identificador_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_identificador_Enabled), 5, 0)));
               edtavRequisito_tipo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_tipo_Enabled), 5, 0)));
               edtavRequisito_agrupador_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_agrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_agrupador_Enabled), 5, 0)));
               edtavRequisito_titulo_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_titulo_Enabled), 5, 0)));
               edtavRequisito_descricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao_Enabled), 5, 0)));
               cmbavRequisito_prioridade.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRequisito_prioridade.Enabled), 5, 0)));
               edtavRequisito_status_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_status_Enabled), 5, 0)));
               edtavRequisito_referenciatecnica_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_referenciatecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_referenciatecnica_Enabled), 5, 0)));
               edtavRequisito_restricao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_restricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_restricao_Enabled), 5, 0)));
               WSQC2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEQC2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Requisitos") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122125428");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_requisitos.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_89", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_89), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_212", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_212), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OBSERVACAO", A514ContagemResultado_Observacao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_OBSERVACAO", A1131LogResponsavel_Observacao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOREQUISITO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2003ContagemResultadoRequisito_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_AGRUPADOR", A1926Requisito_Agrupador);
         GxWebStd.gx_hidden_field( context, "vI", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31i), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_IDENTIFICADOR", A2001Requisito_Identificador);
         GxWebStd.gx_hidden_field( context, "REQUISITO_DESCRICAO", A1923Requisito_Descricao);
         GxWebStd.gx_hidden_field( context, "REQUISITO_PRIORIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2002Requisito_Prioridade), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_STATUS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1934Requisito_Status), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TIPOREQUISITO_IDENTIFICADOR", A2042TipoRequisito_Identificador);
         GxWebStd.gx_hidden_field( context, "REQUISITO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1931Requisito_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_REQCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1999Requisito_ReqCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_TITULO", A1927Requisito_Titulo);
         GxWebStd.gx_hidden_field( context, "REQUISITO_REFERENCIATECNICA", A1925Requisito_ReferenciaTecnica);
         GxWebStd.gx_hidden_field( context, "REQUISITO_RESTRICAO", A1929Requisito_Restricao);
         GxWebStd.gx_hidden_field( context, "GRIDHST_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDHST_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSOLICITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vRESPONSAVEL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13Responsavel, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vREQUISITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Requisitante, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSETOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Setor, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8Servico, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_REFERENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1585ContagemResultado_Referencia, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_RESTRICOES", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1586ContagemResultado_Restricoes, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PRIORIDADEPREVISTA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1587ContagemResultado_PrioridadePrevista, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DATAPREVISTA", GetSecureSignedToken( "", context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "GRIDHST_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultado_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_Requisitos";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"Solicitante:"+StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!")));
         GXUtil.WriteLog("wp_requisitos:[SendSecurityCheck value for]"+"ContagemResultado_DemandaFM:"+StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")));
      }

      protected void RenderHtmlCloseFormQC2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_Requisitos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Requisitos" ;
      }

      protected void WBQC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_QC2( true) ;
         }
         else
         {
            wb_table1_2_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTQC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Requisitos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPQC0( ) ;
      }

      protected void WSQC2( )
      {
         STARTQC2( ) ;
         EVTQC2( ) ;
      }

      protected void EVTQC2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "GRIDHST.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                        {
                           nGXsfl_89_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
                           SubsflControlProps_892( ) ;
                           AV15Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavData_Internalname), 0));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavData_Internalname, context.localUtil.Format(AV15Data, "99/99/99"));
                           AV20Demanda = cgiGet( edtavDemanda_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDemanda_Internalname, AV20Demanda);
                           AV17Autor = StringUtil.Upper( cgiGet( edtavAutor_Internalname));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAutor_Internalname, AV17Autor);
                           AV28DescricaoHis = cgiGet( edtavDescricaohis_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricaohis_Internalname, AV28DescricaoHis);
                           AV19Versao = StringUtil.Upper( cgiGet( edtavVersao_Internalname));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVersao_Internalname, AV19Versao);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E11QC2 */
                                 E11QC2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRIDHST.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E12QC2 */
                                 E12QC2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    if ( ! Rfr0gs )
                                    {
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                        else if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "GRIDRF.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "GRIDRF.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "GRIDRNF.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "GRIDRNF.REFRESH") == 0 ) )
                        {
                           nGXsfl_212_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_212_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_212_idx), 4, 0)), 4, "0");
                           SubsflControlProps_2125( ) ;
                           AV39Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavRequisito_codigo_Internalname), ",", "."));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Requisito_Codigo), 6, 0)));
                           AV25Identificador = cgiGet( edtavIdentificador_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavIdentificador_Internalname, AV25Identificador);
                           AV38Tipo = cgiGet( edtavTipo_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTipo_Internalname, AV38Tipo);
                           AV24Agrupador = cgiGet( edtavAgrupador_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV24Agrupador);
                           AV10Nome = cgiGet( edtavNome_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV10Nome);
                           AV18Descricao = cgiGet( edtavDescricao_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao_Internalname, AV18Descricao);
                           AV26Prioridade = StringUtil.Upper( cgiGet( edtavPrioridade_Internalname));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrioridade_Internalname, AV26Prioridade);
                           AV27Status = StringUtil.Upper( cgiGet( edtavStatus_Internalname));
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavStatus_Internalname, AV27Status);
                           AV29Referencia = cgiGet( edtavReferencia_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavReferencia_Internalname, AV29Referencia);
                           AV30Restricao = cgiGet( edtavRestricao_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRestricao_Internalname, AV30Restricao);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "GRIDRF.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E13QC5 */
                                 E13QC5 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRIDRF.REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E14QC2 */
                                 E14QC2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                              sEvtType = StringUtil.Right( sEvt, 4);
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                              if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "GRIDRNF.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "GRIDRNF.REFRESH") == 0 ) )
                              {
                                 nGXsfl_299_idx = (short)(NumberUtil.Val( sEvtType, "."));
                                 sGXsfl_299_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_299_idx), 4, 0)), 4, "0") + sGXsfl_212_idx;
                                 SubsflControlProps_2997( ) ;
                                 AV40Requisito_Identificador = cgiGet( edtavRequisito_identificador_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_identificador_Internalname, AV40Requisito_Identificador);
                                 AV41Requisito_Tipo = cgiGet( edtavRequisito_tipo_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_tipo_Internalname, AV41Requisito_Tipo);
                                 AV33Requisito_Agrupador = cgiGet( edtavRequisito_agrupador_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_agrupador_Internalname, AV33Requisito_Agrupador);
                                 AV32Requisito_Titulo = cgiGet( edtavRequisito_titulo_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_titulo_Internalname, AV32Requisito_Titulo);
                                 AV34Requisito_Descricao = cgiGet( edtavRequisito_descricao_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_descricao_Internalname, AV34Requisito_Descricao);
                                 cmbavRequisito_prioridade.Name = cmbavRequisito_prioridade_Internalname;
                                 cmbavRequisito_prioridade.CurrentValue = cgiGet( cmbavRequisito_prioridade_Internalname);
                                 AV42Requisito_Prioridade = (short)(NumberUtil.Val( cgiGet( cmbavRequisito_prioridade_Internalname), "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavRequisito_prioridade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0)));
                                 AV43Requisito_Status = StringUtil.Upper( cgiGet( edtavRequisito_status_Internalname));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_status_Internalname, AV43Requisito_Status);
                                 AV35Requisito_ReferenciaTecnica = cgiGet( edtavRequisito_referenciatecnica_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_referenciatecnica_Internalname, AV35Requisito_ReferenciaTecnica);
                                 AV36Requisito_Restricao = cgiGet( edtavRequisito_restricao_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_restricao_Internalname, AV36Requisito_Restricao);
                                 sEvtType = StringUtil.Right( sEvt, 1);
                                 if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                                 {
                                    sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                    if ( StringUtil.StrCmp(sEvt, "GRIDRNF.LOAD") == 0 )
                                    {
                                       context.wbHandled = 1;
                                       dynload_actions( ) ;
                                       /* Execute user event: E15QC7 */
                                       E15QC7 ();
                                    }
                                    else if ( StringUtil.StrCmp(sEvt, "GRIDRNF.REFRESH") == 0 )
                                    {
                                       context.wbHandled = 1;
                                       dynload_actions( ) ;
                                       /* Execute user event: E16QC2 */
                                       E16QC2 ();
                                    }
                                    else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                    {
                                       context.wbHandled = 1;
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else
                                 {
                                 }
                              }
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEQC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQC2( ) ;
            }
         }
      }

      protected void PAQC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "vREQUISITO_PRIORIDADE_" + sGXsfl_299_idx;
            cmbavRequisito_prioridade.Name = GXCCtl;
            cmbavRequisito_prioridade.WebTags = "";
            cmbavRequisito_prioridade.addItem("1", "Alta", 0);
            cmbavRequisito_prioridade.addItem("2", "Alta M�dia", 0);
            cmbavRequisito_prioridade.addItem("3", "Alta Baixa", 0);
            cmbavRequisito_prioridade.addItem("4", "M�dia Alta", 0);
            cmbavRequisito_prioridade.addItem("5", "M�dia M�dia", 0);
            cmbavRequisito_prioridade.addItem("6", "M�dia Baixa", 0);
            cmbavRequisito_prioridade.addItem("7", "Baixa Alta", 0);
            cmbavRequisito_prioridade.addItem("8", "Baixa M�dia", 0);
            cmbavRequisito_prioridade.addItem("9", "Baixa Baixa", 0);
            if ( cmbavRequisito_prioridade.ItemCount > 0 )
            {
               AV42Requisito_Prioridade = (short)(NumberUtil.Val( cmbavRequisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavRequisito_prioridade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSolicitante_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridhst_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_892( ) ;
         while ( nGXsfl_89_idx <= nRC_GXsfl_89 )
         {
            sendrow_892( ) ;
            nGXsfl_89_idx = (short)(((subGridhst_Islastpage==1)&&(nGXsfl_89_idx+1>subGridhst_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         context.GX_webresponse.AddString(GridhstContainer.ToJavascriptSource());
         /* End function gxnrGridhst_newrow */
      }

      protected void gxnrGridrf_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_2125( ) ;
         while ( nGXsfl_212_idx <= nRC_GXsfl_212 )
         {
            sendrow_2125( ) ;
            nGXsfl_212_idx = (short)(((subGridrf_Islastpage==1)&&(nGXsfl_212_idx+1>subGridrf_Recordsperpage( )) ? 1 : nGXsfl_212_idx+1));
            sGXsfl_212_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_212_idx), 4, 0)), 4, "0");
            SubsflControlProps_2125( ) ;
         }
         context.GX_webresponse.AddString(GridrfContainer.ToJavascriptSource());
         /* End function gxnrGridrf_newrow */
      }

      protected void gxnrGridrnf_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_2997( ) ;
         while ( nGXsfl_299_idx <= nRC_GXsfl_299 )
         {
            sendrow_2997( ) ;
            nGXsfl_299_idx = (short)(((subGridrnf_Islastpage==1)&&(nGXsfl_299_idx+1>subGridrnf_Recordsperpage( )) ? 1 : nGXsfl_299_idx+1));
            sGXsfl_299_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_299_idx), 4, 0)), 4, "0") + sGXsfl_212_idx;
            SubsflControlProps_2997( ) ;
         }
         context.GX_webresponse.AddString(GridrnfContainer.ToJavascriptSource());
         /* End function gxnrGridrnf_newrow */
      }

      protected void gxgrGridhst_refresh( int subGridhst_Rows ,
                                          int A456ContagemResultado_Codigo ,
                                          DateTime A471ContagemResultado_DataDmn ,
                                          String A493ContagemResultado_DemandaFM ,
                                          String AV14Solicitante ,
                                          int A892LogResponsavel_DemandaCod ,
                                          String A1131LogResponsavel_Observacao )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRIDHST_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Rows), 6, 0, ".", "")));
         GRIDHST_nCurrentRecord = 0;
         RFQC2( ) ;
         /* End function gxgrGridhst_refresh */
      }

      protected void gxgrGridrnf_refresh( String AV10Nome ,
                                          short A1931Requisito_Ordem ,
                                          int A1999Requisito_ReqCod ,
                                          int AV39Requisito_Codigo ,
                                          short AV31i ,
                                          String A2001Requisito_Identificador ,
                                          String A1927Requisito_Titulo ,
                                          String A1926Requisito_Agrupador ,
                                          String A1923Requisito_Descricao ,
                                          String A1925Requisito_ReferenciaTecnica ,
                                          String A1929Requisito_Restricao ,
                                          short A2002Requisito_Prioridade ,
                                          short A1934Requisito_Status ,
                                          String A2042TipoRequisito_Identificador )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDRNF_nCurrentRecord = 0;
         RFQC7( ) ;
         /* End function gxgrGridrnf_refresh */
      }

      protected void gxgrGridrf_refresh( String AV10Nome ,
                                         int A2003ContagemResultadoRequisito_OSCod ,
                                         String A1926Requisito_Agrupador ,
                                         short AV31i ,
                                         String A2001Requisito_Identificador ,
                                         String A1923Requisito_Descricao ,
                                         short A2002Requisito_Prioridade ,
                                         short A1934Requisito_Status ,
                                         String A2042TipoRequisito_Identificador )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDRF_nCurrentRecord = 0;
         RFQC5( ) ;
         /* End function gxgrGridrf_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQC2( ) ;
         RFQC5( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSolicitante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitante_Enabled), 5, 0)));
         edtavResponsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavResponsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavResponsavel_Enabled), 5, 0)));
         edtavData_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavData_Enabled), 5, 0)));
         edtavDemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDemanda_Enabled), 5, 0)));
         edtavAutor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAutor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAutor_Enabled), 5, 0)));
         edtavDescricaohis_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricaohis_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricaohis_Enabled), 5, 0)));
         edtavVersao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVersao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVersao_Enabled), 5, 0)));
         edtavRequisitante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisitante_Enabled), 5, 0)));
         edtavSetor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSetor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSetor_Enabled), 5, 0)));
         edtavServico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_Enabled), 5, 0)));
         edtavIdentificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIdentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIdentificador_Enabled), 5, 0)));
         edtavTipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipo_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
         edtavPrioridade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrioridade_Enabled), 5, 0)));
         edtavStatus_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavStatus_Enabled), 5, 0)));
         edtavReferencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferencia_Enabled), 5, 0)));
         edtavRestricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRestricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRestricao_Enabled), 5, 0)));
         edtavRequisito_identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_identificador_Enabled), 5, 0)));
         edtavRequisito_tipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_tipo_Enabled), 5, 0)));
         edtavRequisito_agrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_agrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_agrupador_Enabled), 5, 0)));
         edtavRequisito_titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_titulo_Enabled), 5, 0)));
         edtavRequisito_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao_Enabled), 5, 0)));
         cmbavRequisito_prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRequisito_prioridade.Enabled), 5, 0)));
         edtavRequisito_status_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_status_Enabled), 5, 0)));
         edtavRequisito_referenciatecnica_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_referenciatecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_referenciatecnica_Enabled), 5, 0)));
         edtavRequisito_restricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_restricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_restricao_Enabled), 5, 0)));
      }

      protected void RFQC2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridhstContainer.ClearRows();
         }
         wbStart = 89;
         nGXsfl_89_idx = 1;
         sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
         SubsflControlProps_892( ) ;
         nGXsfl_89_Refreshing = 1;
         GridhstContainer.AddObjectProperty("GridName", "Gridhst");
         GridhstContainer.AddObjectProperty("CmpContext", "");
         GridhstContainer.AddObjectProperty("InMasterPage", "false");
         GridhstContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridhstContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridhstContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridhstContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Backcolorstyle), 1, 0, ".", "")));
         GridhstContainer.PageSize = subGridhst_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_892( ) ;
            /* Using cursor H00QC2 */
            pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
            nGXsfl_89_idx = 1;
            GRIDHST_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRIDHST_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGridhst_Rows == 0 ) || ( GRIDHST_nCurrentRecord < GRIDHST_nFirstRecordOnPage + subGridhst_Recordsperpage( ) ) ) ) )
            {
               A489ContagemResultado_SistemaCod = H00QC2_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00QC2_n489ContagemResultado_SistemaCod[0];
               A471ContagemResultado_DataDmn = H00QC2_A471ContagemResultado_DataDmn[0];
               A495ContagemResultado_SistemaNom = H00QC2_A495ContagemResultado_SistemaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A495ContagemResultado_SistemaNom", A495ContagemResultado_SistemaNom);
               n495ContagemResultado_SistemaNom = H00QC2_n495ContagemResultado_SistemaNom[0];
               A493ContagemResultado_DemandaFM = H00QC2_A493ContagemResultado_DemandaFM[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
               n493ContagemResultado_DemandaFM = H00QC2_n493ContagemResultado_DemandaFM[0];
               A509ContagemrResultado_SistemaSigla = H00QC2_A509ContagemrResultado_SistemaSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
               n509ContagemrResultado_SistemaSigla = H00QC2_n509ContagemrResultado_SistemaSigla[0];
               A494ContagemResultado_Descricao = H00QC2_A494ContagemResultado_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
               n494ContagemResultado_Descricao = H00QC2_n494ContagemResultado_Descricao[0];
               A514ContagemResultado_Observacao = H00QC2_A514ContagemResultado_Observacao[0];
               n514ContagemResultado_Observacao = H00QC2_n514ContagemResultado_Observacao[0];
               A1585ContagemResultado_Referencia = H00QC2_A1585ContagemResultado_Referencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1585ContagemResultado_Referencia", A1585ContagemResultado_Referencia);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_REFERENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1585ContagemResultado_Referencia, ""))));
               n1585ContagemResultado_Referencia = H00QC2_n1585ContagemResultado_Referencia[0];
               A1586ContagemResultado_Restricoes = H00QC2_A1586ContagemResultado_Restricoes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1586ContagemResultado_Restricoes", A1586ContagemResultado_Restricoes);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_RESTRICOES", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1586ContagemResultado_Restricoes, ""))));
               n1586ContagemResultado_Restricoes = H00QC2_n1586ContagemResultado_Restricoes[0];
               A1587ContagemResultado_PrioridadePrevista = H00QC2_A1587ContagemResultado_PrioridadePrevista[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1587ContagemResultado_PrioridadePrevista", A1587ContagemResultado_PrioridadePrevista);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_PRIORIDADEPREVISTA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1587ContagemResultado_PrioridadePrevista, ""))));
               n1587ContagemResultado_PrioridadePrevista = H00QC2_n1587ContagemResultado_PrioridadePrevista[0];
               A1351ContagemResultado_DataPrevista = H00QC2_A1351ContagemResultado_DataPrevista[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1351ContagemResultado_DataPrevista", context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DATAPREVISTA", GetSecureSignedToken( "", context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99")));
               n1351ContagemResultado_DataPrevista = H00QC2_n1351ContagemResultado_DataPrevista[0];
               A495ContagemResultado_SistemaNom = H00QC2_A495ContagemResultado_SistemaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A495ContagemResultado_SistemaNom", A495ContagemResultado_SistemaNom);
               n495ContagemResultado_SistemaNom = H00QC2_n495ContagemResultado_SistemaNom[0];
               A509ContagemrResultado_SistemaSigla = H00QC2_A509ContagemrResultado_SistemaSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
               n509ContagemrResultado_SistemaSigla = H00QC2_n509ContagemrResultado_SistemaSigla[0];
               /* Execute user event: E12QC2 */
               E12QC2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            GRIDHST_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRIDHST_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 89;
            WBQC0( ) ;
         }
         nGXsfl_89_Refreshing = 0;
      }

      protected void RFQC5( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridrfContainer.ClearRows();
         }
         wbStart = 212;
         /* Execute user event: E14QC2 */
         E14QC2 ();
         nGXsfl_212_idx = 1;
         sGXsfl_212_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_212_idx), 4, 0)), 4, "0");
         SubsflControlProps_2125( ) ;
         nGXsfl_212_Refreshing = 1;
         GridrfContainer.AddObjectProperty("GridName", "Gridrf");
         GridrfContainer.AddObjectProperty("CmpContext", "");
         GridrfContainer.AddObjectProperty("InMasterPage", "false");
         GridrfContainer.AddObjectProperty("Class", StringUtil.RTrim( ""));
         GridrfContainer.AddObjectProperty("Class", "");
         GridrfContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridrfContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridrfContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Backcolorstyle), 1, 0, ".", "")));
         GridrfContainer.PageSize = subGridrf_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_2125( ) ;
            /* Execute user event: E13QC5 */
            E13QC5 ();
            wbEnd = 212;
            WBQC0( ) ;
         }
         nGXsfl_212_Refreshing = 0;
      }

      protected void RFQC7( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridrnfContainer.ClearRows();
         }
         wbStart = 299;
         /* Execute user event: E16QC2 */
         E16QC2 ();
         nGXsfl_299_idx = 1;
         sGXsfl_299_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_299_idx), 4, 0)), 4, "0") + sGXsfl_212_idx;
         SubsflControlProps_2997( ) ;
         nGXsfl_299_Refreshing = 1;
         GridrnfContainer.AddObjectProperty("GridName", "Gridrnf");
         GridrnfContainer.AddObjectProperty("CmpContext", "");
         GridrnfContainer.AddObjectProperty("InMasterPage", "false");
         GridrnfContainer.AddObjectProperty("Class", StringUtil.RTrim( ""));
         GridrnfContainer.AddObjectProperty("Class", "");
         GridrnfContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridrnfContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridrnfContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Backcolorstyle), 1, 0, ".", "")));
         GridrnfContainer.PageSize = subGridrnf_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_2997( ) ;
            /* Execute user event: E15QC7 */
            E15QC7 ();
            wbEnd = 299;
            WBQC0( ) ;
         }
         nGXsfl_299_Refreshing = 0;
      }

      protected int subGridhst_Pagecount( )
      {
         GRIDHST_nRecordCount = subGridhst_Recordcount( );
         if ( ((int)((GRIDHST_nRecordCount) % (subGridhst_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDHST_nRecordCount/ (decimal)(subGridhst_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDHST_nRecordCount/ (decimal)(subGridhst_Recordsperpage( ))))+1) ;
      }

      protected int subGridhst_Recordcount( )
      {
         /* Using cursor H00QC3 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
         GRIDHST_nRecordCount = H00QC3_AGRIDHST_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRIDHST_nRecordCount) ;
      }

      protected int subGridhst_Recordsperpage( )
      {
         if ( subGridhst_Rows > 0 )
         {
            return subGridhst_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridhst_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDHST_nFirstRecordOnPage/ (decimal)(subGridhst_Recordsperpage( ))))+1) ;
      }

      protected short subgridhst_firstpage( )
      {
         GRIDHST_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRIDHST_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridhst_refresh( subGridhst_Rows, A456ContagemResultado_Codigo, A471ContagemResultado_DataDmn, A493ContagemResultado_DemandaFM, AV14Solicitante, A892LogResponsavel_DemandaCod, A1131LogResponsavel_Observacao) ;
         }
         return 0 ;
      }

      protected short subgridhst_nextpage( )
      {
         GRIDHST_nRecordCount = subGridhst_Recordcount( );
         if ( ( GRIDHST_nRecordCount >= subGridhst_Recordsperpage( ) ) && ( GRIDHST_nEOF == 0 ) )
         {
            GRIDHST_nFirstRecordOnPage = (long)(GRIDHST_nFirstRecordOnPage+subGridhst_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDHST_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridhst_refresh( subGridhst_Rows, A456ContagemResultado_Codigo, A471ContagemResultado_DataDmn, A493ContagemResultado_DemandaFM, AV14Solicitante, A892LogResponsavel_DemandaCod, A1131LogResponsavel_Observacao) ;
         }
         return (short)(((GRIDHST_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridhst_previouspage( )
      {
         if ( GRIDHST_nFirstRecordOnPage >= subGridhst_Recordsperpage( ) )
         {
            GRIDHST_nFirstRecordOnPage = (long)(GRIDHST_nFirstRecordOnPage-subGridhst_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRIDHST_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridhst_refresh( subGridhst_Rows, A456ContagemResultado_Codigo, A471ContagemResultado_DataDmn, A493ContagemResultado_DemandaFM, AV14Solicitante, A892LogResponsavel_DemandaCod, A1131LogResponsavel_Observacao) ;
         }
         return 0 ;
      }

      protected short subgridhst_lastpage( )
      {
         GRIDHST_nRecordCount = subGridhst_Recordcount( );
         if ( GRIDHST_nRecordCount > subGridhst_Recordsperpage( ) )
         {
            if ( ((int)((GRIDHST_nRecordCount) % (subGridhst_Recordsperpage( )))) == 0 )
            {
               GRIDHST_nFirstRecordOnPage = (long)(GRIDHST_nRecordCount-subGridhst_Recordsperpage( ));
            }
            else
            {
               GRIDHST_nFirstRecordOnPage = (long)(GRIDHST_nRecordCount-((int)((GRIDHST_nRecordCount) % (subGridhst_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDHST_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDHST_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridhst_refresh( subGridhst_Rows, A456ContagemResultado_Codigo, A471ContagemResultado_DataDmn, A493ContagemResultado_DemandaFM, AV14Solicitante, A892LogResponsavel_DemandaCod, A1131LogResponsavel_Observacao) ;
         }
         return 0 ;
      }

      protected int subgridhst_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDHST_nFirstRecordOnPage = (long)(subGridhst_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDHST_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRIDHST_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDHST_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridhst_refresh( subGridhst_Rows, A456ContagemResultado_Codigo, A471ContagemResultado_DataDmn, A493ContagemResultado_DemandaFM, AV14Solicitante, A892LogResponsavel_DemandaCod, A1131LogResponsavel_Observacao) ;
         }
         return (int)(0) ;
      }

      protected int subGridrf_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridrf_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridrf_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridrf_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridrnf_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridrnf_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridrnf_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridrnf_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPQC0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavSolicitante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitante_Enabled), 5, 0)));
         edtavResponsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavResponsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavResponsavel_Enabled), 5, 0)));
         edtavData_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavData_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavData_Enabled), 5, 0)));
         edtavDemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDemanda_Enabled), 5, 0)));
         edtavAutor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAutor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAutor_Enabled), 5, 0)));
         edtavDescricaohis_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricaohis_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricaohis_Enabled), 5, 0)));
         edtavVersao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVersao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVersao_Enabled), 5, 0)));
         edtavRequisitante_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisitante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisitante_Enabled), 5, 0)));
         edtavSetor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSetor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSetor_Enabled), 5, 0)));
         edtavServico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_Enabled), 5, 0)));
         edtavIdentificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavIdentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIdentificador_Enabled), 5, 0)));
         edtavTipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipo_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNome_Enabled), 5, 0)));
         edtavDescricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDescricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDescricao_Enabled), 5, 0)));
         edtavPrioridade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrioridade_Enabled), 5, 0)));
         edtavStatus_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavStatus_Enabled), 5, 0)));
         edtavReferencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferencia_Enabled), 5, 0)));
         edtavRestricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRestricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRestricao_Enabled), 5, 0)));
         edtavRequisito_identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_identificador_Enabled), 5, 0)));
         edtavRequisito_tipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_tipo_Enabled), 5, 0)));
         edtavRequisito_agrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_agrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_agrupador_Enabled), 5, 0)));
         edtavRequisito_titulo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_titulo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_titulo_Enabled), 5, 0)));
         edtavRequisito_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_descricao_Enabled), 5, 0)));
         cmbavRequisito_prioridade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_prioridade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRequisito_prioridade.Enabled), 5, 0)));
         edtavRequisito_status_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_status_Enabled), 5, 0)));
         edtavRequisito_referenciatecnica_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_referenciatecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_referenciatecnica_Enabled), 5, 0)));
         edtavRequisito_restricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_restricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_restricao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11QC2 */
         E11QC2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5LogoMarca = cgiGet( imgavLogomarca_Internalname);
            A495ContagemResultado_SistemaNom = StringUtil.Upper( cgiGet( edtContagemResultado_SistemaNom_Internalname));
            n495ContagemResultado_SistemaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A495ContagemResultado_SistemaNom", A495ContagemResultado_SistemaNom);
            AV14Solicitante = StringUtil.Upper( cgiGet( edtavSolicitante_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Solicitante", AV14Solicitante);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"))));
            AV13Responsavel = StringUtil.Upper( cgiGet( edtavResponsavel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Responsavel", AV13Responsavel);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13Responsavel, "@!"))));
            A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
            n493ContagemResultado_DemandaFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
            AV6Requisitante = StringUtil.Upper( cgiGet( edtavRequisitante_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Requisitante", AV6Requisitante);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREQUISITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Requisitante, "@!"))));
            AV7Setor = StringUtil.Upper( cgiGet( edtavSetor_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Setor", AV7Setor);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSETOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7Setor, "@!"))));
            AV8Servico = StringUtil.Upper( cgiGet( edtavServico_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Servico", AV8Servico);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8Servico, "@!"))));
            A509ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtContagemrResultado_SistemaSigla_Internalname));
            n509ContagemrResultado_SistemaSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
            A494ContagemResultado_Descricao = cgiGet( edtContagemResultado_Descricao_Internalname);
            n494ContagemResultado_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
            A1585ContagemResultado_Referencia = cgiGet( edtContagemResultado_Referencia_Internalname);
            n1585ContagemResultado_Referencia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1585ContagemResultado_Referencia", A1585ContagemResultado_Referencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_REFERENCIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1585ContagemResultado_Referencia, ""))));
            A1586ContagemResultado_Restricoes = cgiGet( edtContagemResultado_Restricoes_Internalname);
            n1586ContagemResultado_Restricoes = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1586ContagemResultado_Restricoes", A1586ContagemResultado_Restricoes);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_RESTRICOES", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1586ContagemResultado_Restricoes, ""))));
            A1587ContagemResultado_PrioridadePrevista = cgiGet( edtContagemResultado_PrioridadePrevista_Internalname);
            n1587ContagemResultado_PrioridadePrevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1587ContagemResultado_PrioridadePrevista", A1587ContagemResultado_PrioridadePrevista);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_PRIORIDADEPREVISTA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1587ContagemResultado_PrioridadePrevista, ""))));
            A1351ContagemResultado_DataPrevista = context.localUtil.CToT( cgiGet( edtContagemResultado_DataPrevista_Internalname), 0);
            n1351ContagemResultado_DataPrevista = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1351ContagemResultado_DataPrevista", context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DATAPREVISTA", GetSecureSignedToken( "", context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99")));
            /* Read saved values. */
            nRC_GXsfl_89 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_89"), ",", "."));
            nRC_GXsfl_212 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_212"), ",", "."));
            A514ContagemResultado_Observacao = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO");
            GRIDHST_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDHST_nFirstRecordOnPage"), ",", "."));
            GRIDHST_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDHST_nEOF"), ",", "."));
            subGridhst_Rows = (int)(context.localUtil.CToN( cgiGet( "GRIDHST_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRIDHST_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Rows), 6, 0, ".", "")));
            Contagemresultado_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_Requisitos";
            AV14Solicitante = cgiGet( edtavSolicitante_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Solicitante", AV14Solicitante);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"));
            A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
            n493ContagemResultado_DemandaFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_requisitos:[SecurityCheckFailed value for]"+"Solicitante:"+StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!")));
               GXUtil.WriteLog("wp_requisitos:[SecurityCheckFailed value for]"+"ContagemResultado_DemandaFM:"+StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11QC2 */
         E11QC2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11QC2( )
      {
         /* Start Routine */
         edtavRequisito_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRequisito_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRequisito_codigo_Visible), 5, 0)));
         subGridhst_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRIDHST_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Rows), 6, 0, ".", "")));
         subgridhst_gotopage( 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV37WWPContext) ;
         /* Using cursor H00QC4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A489ContagemResultado_SistemaCod = H00QC4_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00QC4_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = H00QC4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00QC4_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = H00QC4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00QC4_n601ContagemResultado_Servico[0];
            A1636ContagemResultado_ServicoSS = H00QC4_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = H00QC4_n1636ContagemResultado_ServicoSS[0];
            A602ContagemResultado_OSVinculada = H00QC4_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00QC4_n602ContagemResultado_OSVinculada[0];
            A801ContagemResultado_ServicoSigla = H00QC4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00QC4_n801ContagemResultado_ServicoSigla[0];
            A509ContagemrResultado_SistemaSigla = H00QC4_A509ContagemrResultado_SistemaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
            n509ContagemrResultado_SistemaSigla = H00QC4_n509ContagemrResultado_SistemaSigla[0];
            A508ContagemResultado_Owner = H00QC4_A508ContagemResultado_Owner[0];
            A890ContagemResultado_Responsavel = H00QC4_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = H00QC4_n890ContagemResultado_Responsavel[0];
            A509ContagemrResultado_SistemaSigla = H00QC4_A509ContagemrResultado_SistemaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
            n509ContagemrResultado_SistemaSigla = H00QC4_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = H00QC4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00QC4_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = H00QC4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = H00QC4_n801ContagemResultado_ServicoSigla[0];
            if ( A1636ContagemResultado_ServicoSS > 0 )
            {
               lblTbtitle_Caption = "DOCUMENTO DE VIS�O";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbtitle_Internalname, "Caption", lblTbtitle_Caption);
            }
            else
            {
               lblTbtitle_Caption = "LISTA DE REQUISITOS";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbtitle_Internalname, "Caption", lblTbtitle_Caption);
            }
            AV11OsVinculada = A602ContagemResultado_OSVinculada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11OsVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11OsVinculada), 6, 0)));
            /* Execute user subroutine: 'REQUISITANTE' */
            S113 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            AV8Servico = A801ContagemResultado_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Servico", AV8Servico);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8Servico, "@!"))));
            lblTbsistema_Caption = "Sigla do Sistema: "+A509ContagemrResultado_SistemaSigla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbsistema_Internalname, "Caption", lblTbsistema_Caption);
            AV9Usuario = A508ContagemResultado_Owner;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Usuario), 6, 0)));
            /* Execute user subroutine: 'USUARIO' */
            S123 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            AV14Solicitante = AV10Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Solicitante", AV14Solicitante);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSOLICITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!"))));
            AV9Usuario = A890ContagemResultado_Responsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Usuario), 6, 0)));
            /* Execute user subroutine: 'USUARIO' */
            S123 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            AV13Responsavel = AV10Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Responsavel", AV13Responsavel);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13Responsavel, "@!"))));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Using cursor H00QC5 */
         pr_default.execute(3, new Object[] {AV37WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A29Contratante_Codigo = H00QC5_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00QC5_n29Contratante_Codigo[0];
            A5AreaTrabalho_Codigo = H00QC5_A5AreaTrabalho_Codigo[0];
            A1125Contratante_LogoNomeArq = H00QC5_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = H00QC5_n1125Contratante_LogoNomeArq[0];
            A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A1126Contratante_LogoTipoArq = H00QC5_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = H00QC5_n1126Contratante_LogoTipoArq[0];
            A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            A10Contratante_NomeFantasia = H00QC5_A10Contratante_NomeFantasia[0];
            A1124Contratante_LogoArquivo = H00QC5_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = H00QC5_n1124Contratante_LogoArquivo[0];
            A1125Contratante_LogoNomeArq = H00QC5_A1125Contratante_LogoNomeArq[0];
            n1125Contratante_LogoNomeArq = H00QC5_n1125Contratante_LogoNomeArq[0];
            A1124Contratante_LogoArquivo_Filename = A1125Contratante_LogoNomeArq;
            A1126Contratante_LogoTipoArq = H00QC5_A1126Contratante_LogoTipoArq[0];
            n1126Contratante_LogoTipoArq = H00QC5_n1126Contratante_LogoTipoArq[0];
            A1124Contratante_LogoArquivo_Filetype = A1126Contratante_LogoTipoArq;
            A10Contratante_NomeFantasia = H00QC5_A10Contratante_NomeFantasia[0];
            A1124Contratante_LogoArquivo = H00QC5_A1124Contratante_LogoArquivo[0];
            n1124Contratante_LogoArquivo = H00QC5_n1124Contratante_LogoArquivo[0];
            AV5LogoMarca = A1124Contratante_LogoArquivo;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavLogomarca_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LogoMarca)) ? AV48Logomarca_GXI : context.convertURL( context.PathToRelativeUrl( AV5LogoMarca))));
            AV48Logomarca_GXI = GeneXus.Utils.GXDbFile.GetUriFromFile( A1125Contratante_LogoNomeArq, A1126Contratante_LogoTipoArq);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavLogomarca_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LogoMarca)) ? AV48Logomarca_GXI : context.convertURL( context.PathToRelativeUrl( AV5LogoMarca))));
            lblTbcontratante_Caption = A10Contratante_NomeFantasia;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbcontratante_Internalname, "Caption", lblTbcontratante_Caption);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      private void E12QC2( )
      {
         /* Gridhst_Load Routine */
         AV15Data = A471ContagemResultado_DataDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavData_Internalname, context.localUtil.Format(AV15Data, "99/99/99"));
         AV20Demanda = A493ContagemResultado_DemandaFM;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDemanda_Internalname, AV20Demanda);
         AV17Autor = AV14Solicitante;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAutor_Internalname, AV17Autor);
         /* Using cursor H00QC6 */
         pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A892LogResponsavel_DemandaCod = H00QC6_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = H00QC6_n892LogResponsavel_DemandaCod[0];
            A1131LogResponsavel_Observacao = H00QC6_A1131LogResponsavel_Observacao[0];
            n1131LogResponsavel_Observacao = H00QC6_n1131LogResponsavel_Observacao[0];
            AV28DescricaoHis = A1131LogResponsavel_Observacao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricaohis_Internalname, AV28DescricaoHis);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(4);
         }
         pr_default.close(4);
         AV19Versao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVersao_Internalname, AV19Versao);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 89;
         }
         if ( ( subGridhst_Islastpage == 1 ) || ( subGridhst_Rows == 0 ) || ( ( GRIDHST_nCurrentRecord >= GRIDHST_nFirstRecordOnPage ) && ( GRIDHST_nCurrentRecord < GRIDHST_nFirstRecordOnPage + subGridhst_Recordsperpage( ) ) ) )
         {
            sendrow_892( ) ;
         }
         GRIDHST_nCurrentRecord = (long)(GRIDHST_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_89_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(89, GridhstRow);
         }
      }

      protected void E14QC2( )
      {
         /* Gridrf_Refresh Routine */
         AV23NaoFuncionais.Clear();
         AV31i = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31i), 4, 0)));
      }

      protected void E16QC2( )
      {
         /* Gridrnf_Refresh Routine */
         AV31i = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31i), 4, 0)));
      }

      protected void S113( )
      {
         /* 'REQUISITANTE' Routine */
         /* Using cursor H00QC7 */
         pr_default.execute(5, new Object[] {AV11OsVinculada});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A508ContagemResultado_Owner = H00QC7_A508ContagemResultado_Owner[0];
            AV9Usuario = A508ContagemResultado_Owner;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Usuario), 6, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
         /* Execute user subroutine: 'USUARIO' */
         S123 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV6Requisitante = AV10Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Requisitante", AV6Requisitante);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREQUISITANTE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Requisitante, "@!"))));
      }

      protected void S123( )
      {
         /* 'USUARIO' Routine */
         AV10Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV10Nome);
         /* Using cursor H00QC8 */
         pr_default.execute(6, new Object[] {AV9Usuario});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A57Usuario_PessoaCod = H00QC8_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00QC8_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00QC8_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00QC8_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = H00QC8_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00QC8_n58Usuario_PessoaNom[0];
            AV10Nome = A58Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavNome_Internalname, AV10Nome);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
      }

      private void E13QC5( )
      {
         /* Gridrf_Load Routine */
         /* Using cursor H00QC9 */
         pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            A2004ContagemResultadoRequisito_ReqCod = H00QC9_A2004ContagemResultadoRequisito_ReqCod[0];
            A2049Requisito_TipoReqCod = H00QC9_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = H00QC9_n2049Requisito_TipoReqCod[0];
            A2003ContagemResultadoRequisito_OSCod = H00QC9_A2003ContagemResultadoRequisito_OSCod[0];
            A2001Requisito_Identificador = H00QC9_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = H00QC9_n2001Requisito_Identificador[0];
            A1923Requisito_Descricao = H00QC9_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = H00QC9_n1923Requisito_Descricao[0];
            A2002Requisito_Prioridade = H00QC9_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = H00QC9_n2002Requisito_Prioridade[0];
            A1934Requisito_Status = H00QC9_A1934Requisito_Status[0];
            n1934Requisito_Status = H00QC9_n1934Requisito_Status[0];
            A2042TipoRequisito_Identificador = H00QC9_A2042TipoRequisito_Identificador[0];
            n2042TipoRequisito_Identificador = H00QC9_n2042TipoRequisito_Identificador[0];
            A1926Requisito_Agrupador = H00QC9_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = H00QC9_n1926Requisito_Agrupador[0];
            A2049Requisito_TipoReqCod = H00QC9_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = H00QC9_n2049Requisito_TipoReqCod[0];
            A2001Requisito_Identificador = H00QC9_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = H00QC9_n2001Requisito_Identificador[0];
            A1923Requisito_Descricao = H00QC9_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = H00QC9_n1923Requisito_Descricao[0];
            A2002Requisito_Prioridade = H00QC9_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = H00QC9_n2002Requisito_Prioridade[0];
            A1934Requisito_Status = H00QC9_A1934Requisito_Status[0];
            n1934Requisito_Status = H00QC9_n1934Requisito_Status[0];
            A1926Requisito_Agrupador = H00QC9_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = H00QC9_n1926Requisito_Agrupador[0];
            A2042TipoRequisito_Identificador = H00QC9_A2042TipoRequisito_Identificador[0];
            n2042TipoRequisito_Identificador = H00QC9_n2042TipoRequisito_Identificador[0];
            AV31i = (short)(AV31i+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31i), 4, 0)));
            AV25Identificador = A2001Requisito_Identificador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavIdentificador_Internalname, AV25Identificador);
            lblTextblockidentificador_Caption = "2."+StringUtil.Trim( StringUtil.Str( (decimal)(AV31i), 4, 0))+" Identificador";
            AV24Agrupador = A1926Requisito_Agrupador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV24Agrupador);
            AV18Descricao = A1923Requisito_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDescricao_Internalname, AV18Descricao);
            AV29Referencia = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavReferencia_Internalname, AV29Referencia);
            AV30Restricao = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRestricao_Internalname, AV30Restricao);
            AV26Prioridade = gxdomainreqprioridade.getDescription(context,A2002Requisito_Prioridade);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrioridade_Internalname, AV26Prioridade);
            AV27Status = gxdomainstatusrequisito.getDescription(context,A1934Requisito_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavStatus_Internalname, AV27Status);
            AV38Tipo = A2042TipoRequisito_Identificador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavTipo_Internalname, AV38Tipo);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 212;
            }
            sendrow_2125( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_212_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(212, GridrfRow);
            }
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      private void E15QC7( )
      {
         /* Gridrnf_Load Routine */
         /* Using cursor H00QC10 */
         pr_default.execute(8, new Object[] {AV39Requisito_Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A2049Requisito_TipoReqCod = H00QC10_A2049Requisito_TipoReqCod[0];
            n2049Requisito_TipoReqCod = H00QC10_n2049Requisito_TipoReqCod[0];
            A1999Requisito_ReqCod = H00QC10_A1999Requisito_ReqCod[0];
            n1999Requisito_ReqCod = H00QC10_n1999Requisito_ReqCod[0];
            A2001Requisito_Identificador = H00QC10_A2001Requisito_Identificador[0];
            n2001Requisito_Identificador = H00QC10_n2001Requisito_Identificador[0];
            A1927Requisito_Titulo = H00QC10_A1927Requisito_Titulo[0];
            n1927Requisito_Titulo = H00QC10_n1927Requisito_Titulo[0];
            A1926Requisito_Agrupador = H00QC10_A1926Requisito_Agrupador[0];
            n1926Requisito_Agrupador = H00QC10_n1926Requisito_Agrupador[0];
            A1923Requisito_Descricao = H00QC10_A1923Requisito_Descricao[0];
            n1923Requisito_Descricao = H00QC10_n1923Requisito_Descricao[0];
            A1925Requisito_ReferenciaTecnica = H00QC10_A1925Requisito_ReferenciaTecnica[0];
            n1925Requisito_ReferenciaTecnica = H00QC10_n1925Requisito_ReferenciaTecnica[0];
            A1929Requisito_Restricao = H00QC10_A1929Requisito_Restricao[0];
            n1929Requisito_Restricao = H00QC10_n1929Requisito_Restricao[0];
            A2002Requisito_Prioridade = H00QC10_A2002Requisito_Prioridade[0];
            n2002Requisito_Prioridade = H00QC10_n2002Requisito_Prioridade[0];
            A1934Requisito_Status = H00QC10_A1934Requisito_Status[0];
            n1934Requisito_Status = H00QC10_n1934Requisito_Status[0];
            A2042TipoRequisito_Identificador = H00QC10_A2042TipoRequisito_Identificador[0];
            n2042TipoRequisito_Identificador = H00QC10_n2042TipoRequisito_Identificador[0];
            A1931Requisito_Ordem = H00QC10_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = H00QC10_n1931Requisito_Ordem[0];
            A2042TipoRequisito_Identificador = H00QC10_A2042TipoRequisito_Identificador[0];
            n2042TipoRequisito_Identificador = H00QC10_n2042TipoRequisito_Identificador[0];
            AV31i = (short)(AV31i+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31i", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31i), 4, 0)));
            lblTextblockrequisito_titulo_Caption = "3."+StringUtil.Trim( StringUtil.Str( (decimal)(AV31i), 4, 0))+" Nome";
            AV40Requisito_Identificador = A2001Requisito_Identificador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_identificador_Internalname, AV40Requisito_Identificador);
            AV32Requisito_Titulo = A1927Requisito_Titulo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_titulo_Internalname, AV32Requisito_Titulo);
            AV33Requisito_Agrupador = A1926Requisito_Agrupador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_agrupador_Internalname, AV33Requisito_Agrupador);
            AV34Requisito_Descricao = A1923Requisito_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_descricao_Internalname, AV34Requisito_Descricao);
            AV35Requisito_ReferenciaTecnica = A1925Requisito_ReferenciaTecnica;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_referenciatecnica_Internalname, AV35Requisito_ReferenciaTecnica);
            AV36Requisito_Restricao = A1929Requisito_Restricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_restricao_Internalname, AV36Requisito_Restricao);
            AV42Requisito_Prioridade = A2002Requisito_Prioridade;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavRequisito_prioridade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0)));
            AV43Requisito_Status = gxdomainstatusrequisito.getDescription(context,A1934Requisito_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_status_Internalname, AV43Requisito_Status);
            AV41Requisito_Tipo = A2042TipoRequisito_Identificador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRequisito_tipo_Internalname, AV41Requisito_Tipo);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 299;
            }
            sendrow_2997( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_299_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(299, GridrnfRow);
            }
            pr_default.readNext(8);
         }
         pr_default.close(8);
         cmbavRequisito_prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0));
      }

      protected void wb_table1_2_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table2_5_QC2( true) ;
         }
         else
         {
            wb_table2_5_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_32_QC2( true) ;
         }
         else
         {
            wb_table3_32_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table3_32_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_38_QC2( true) ;
         }
         else
         {
            wb_table4_38_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table4_38_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_48_QC2( true) ;
         }
         else
         {
            wb_table5_48_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table5_48_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_54_QC2( true) ;
         }
         else
         {
            wb_table6_54_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table6_54_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_77_QC2( true) ;
         }
         else
         {
            wb_table7_77_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table7_77_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_83_QC2( true) ;
         }
         else
         {
            wb_table8_83_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table8_83_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_97_QC2( true) ;
         }
         else
         {
            wb_table9_97_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table9_97_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QC2e( true) ;
         }
         else
         {
            wb_table1_2_QC2e( false) ;
         }
      }

      protected void wb_table9_97_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable8_Internalname, tblUnnamedtable8_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb1_Internalname, "1. Solicita��o de Servi�o", "", "", lblTb1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_103_QC2( true) ;
         }
         else
         {
            wb_table10_103_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table10_103_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTb2_Internalname, "2. Requisitos", "", "", lblTb2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_209_QC2( true) ;
         }
         else
         {
            wb_table11_209_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table11_209_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_97_QC2e( true) ;
         }
         else
         {
            wb_table9_97_QC2e( false) ;
         }
      }

      protected void wb_table11_209_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable10_Internalname, tblUnnamedtable10_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridrfContainer.SetIsFreestyle(true);
            GridrfContainer.SetWrapped(nGXWrapped);
            if ( GridrfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridrfContainer"+"DivS\" data-gxgridid=\"212\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridrf_Internalname, subGridrf_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
               GridrfContainer.AddObjectProperty("GridName", "Gridrf");
            }
            else
            {
               GridrfContainer.AddObjectProperty("GridName", "Gridrf");
               GridrfContainer.AddObjectProperty("Class", StringUtil.RTrim( ""));
               GridrfContainer.AddObjectProperty("Class", "");
               GridrfContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridrfContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridrfContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Backcolorstyle), 1, 0, ".", "")));
               GridrfContainer.AddObjectProperty("CmpContext", "");
               GridrfContainer.AddObjectProperty("InMasterPage", "false");
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTbspac1_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39Requisito_Codigo), 6, 0, ".", "")));
               GridrfColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_codigo_Visible), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblockidentificador_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", AV25Identificador);
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavIdentificador_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblocktipo_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", AV38Tipo);
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavTipo_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblockagrupador_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", AV24Agrupador);
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAgrupador_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblocknome_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", AV10Nome);
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNome_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblockdescricao_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", AV18Descricao);
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDescricao_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblockprioridade_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", StringUtil.RTrim( AV26Prioridade));
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrioridade_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblockstatus_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", StringUtil.RTrim( AV27Status));
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavStatus_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblockreferencia_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", AV29Referencia);
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavReferencia_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", lblTextblockrestricao_Caption);
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfColumn.AddObjectProperty("Value", AV30Restricao);
               GridrfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRestricao_Enabled), 5, 0, ".", "")));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrfContainer.AddColumnProperties(GridrfColumn);
               GridrfContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Allowselection), 1, 0, ".", "")));
               GridrfContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Selectioncolor), 9, 0, ".", "")));
               GridrfContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Allowhovering), 1, 0, ".", "")));
               GridrfContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Hoveringcolor), 9, 0, ".", "")));
               GridrfContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Allowcollapsing), 1, 0, ".", "")));
               GridrfContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrf_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 212 )
         {
            wbEnd = 0;
            nRC_GXsfl_212 = (short)(nGXsfl_212_idx-1);
            if ( GridrfContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridrfContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridrf", GridrfContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrfContainerData", GridrfContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrfContainerData"+"V", GridrfContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridrfContainerData"+"V"+"\" value='"+GridrfContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_209_QC2e( true) ;
         }
         else
         {
            wb_table11_209_QC2e( false) ;
         }
      }

      protected void wb_table10_103_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable9_Internalname, tblUnnamedtable9_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbspac2_Internalname, "����", "", "", lblTbspac2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_demandafm_Internalname, "Numero de Atendimento:", "", "", lblTextblockcontagemresultado_demandafm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DemandaFM_Internalname, A493ContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DemandaFM_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockrequisitante_Internalname, "Requisitante:", "", "", lblTextblockrequisitante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRequisitante_Internalname, StringUtil.RTrim( AV6Requisitante), StringUtil.RTrim( context.localUtil.Format( AV6Requisitante, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRequisitante_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRequisitante_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksetor_Internalname, "Setor:", "", "", lblTextblocksetor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSetor_Internalname, StringUtil.RTrim( AV7Setor), StringUtil.RTrim( context.localUtil.Format( AV7Setor, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSetor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSetor_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_Internalname, "Servi�o:", "", "", lblTextblockservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_Internalname, StringUtil.RTrim( AV8Servico), StringUtil.RTrim( context.localUtil.Format( AV8Servico, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavServico_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemrresultado_sistemasigla_Internalname, "Sistema", "", "", lblTextblockcontagemrresultado_sistemasigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemrResultado_SistemaSigla_Internalname, StringUtil.RTrim( A509ContagemrResultado_SistemaSigla), StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemrResultado_SistemaSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_descricao_Internalname, "Assunto:", "", "", lblTextblockcontagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Descricao_Internalname, A494ContagemResultado_Descricao, StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_observacao_Internalname, "Descri��o:", "", "", lblTextblockcontagemresultado_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEMRESULTADO_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_referencia_Internalname, "Refer�ncia t�cnica:", "", "", lblTextblockcontagemresultado_referencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultado_Referencia_Internalname, A1585ContagemResultado_Referencia, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_restricoes_Internalname, "Limita��o Geral:", "", "", lblTextblockcontagemresultado_restricoes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultado_Restricoes_Internalname, A1586ContagemResultado_Restricoes, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "250", 1, "", "", -1, true, "", "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_prioridadeprevista_Internalname, "Prioridade:", "", "", lblTextblockcontagemresultado_prioridadeprevista_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PrioridadePrevista_Internalname, A1587ContagemResultado_PrioridadePrevista, StringUtil.RTrim( context.localUtil.Format( A1587ContagemResultado_PrioridadePrevista, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PrioridadePrevista_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 100, "%", 1, "row", 250, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataprevista_Internalname, "Limita��o de Data:", "", "", lblTextblockcontagemresultado_dataprevista_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataPrevista_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataPrevista_Internalname, context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataPrevista_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Requisitos.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataPrevista_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Requisitos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_103_QC2e( true) ;
         }
         else
         {
            wb_table10_103_QC2e( false) ;
         }
      }

      protected void wb_table8_83_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_86_QC2( true) ;
         }
         else
         {
            wb_table12_86_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table12_86_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_83_QC2e( true) ;
         }
         else
         {
            wb_table8_83_QC2e( false) ;
         }
      }

      protected void wb_table12_86_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable11_Internalname, tblUnnamedtable11_Internalname, "", "Table", 0, "", "", 20, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridhstContainer.SetWrapped(nGXWrapped);
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridhstContainer"+"DivS\" data-gxgridid=\"89\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridhst_Internalname, subGridhst_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridhst_Backcolorstyle == 0 )
               {
                  subGridhst_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridhst_Class) > 0 )
                  {
                     subGridhst_Linesclass = subGridhst_Class+"Title";
                  }
               }
               else
               {
                  subGridhst_Titlebackstyle = 1;
                  if ( subGridhst_Backcolorstyle == 1 )
                  {
                     subGridhst_Titlebackcolor = subGridhst_Allbackcolor;
                     if ( StringUtil.Len( subGridhst_Class) > 0 )
                     {
                        subGridhst_Linesclass = subGridhst_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridhst_Class) > 0 )
                     {
                        subGridhst_Linesclass = subGridhst_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridhst_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGridhst_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridhst_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Autor") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridhst_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Descri��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridhst_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Vers�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridhstContainer.AddObjectProperty("GridName", "Gridhst");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridhstContainer = new GXWebGrid( context);
               }
               else
               {
                  GridhstContainer.Clear();
               }
               GridhstContainer.SetWrapped(nGXWrapped);
               GridhstContainer.AddObjectProperty("GridName", "Gridhst");
               GridhstContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridhstContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridhstContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridhstContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Backcolorstyle), 1, 0, ".", "")));
               GridhstContainer.AddObjectProperty("CmpContext", "");
               GridhstContainer.AddObjectProperty("InMasterPage", "false");
               GridhstColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridhstColumn.AddObjectProperty("Value", context.localUtil.Format(AV15Data, "99/99/99"));
               GridhstColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavData_Enabled), 5, 0, ".", "")));
               GridhstContainer.AddColumnProperties(GridhstColumn);
               GridhstColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridhstColumn.AddObjectProperty("Value", AV20Demanda);
               GridhstColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDemanda_Enabled), 5, 0, ".", "")));
               GridhstContainer.AddColumnProperties(GridhstColumn);
               GridhstColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridhstColumn.AddObjectProperty("Value", StringUtil.RTrim( AV17Autor));
               GridhstColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAutor_Enabled), 5, 0, ".", "")));
               GridhstContainer.AddColumnProperties(GridhstColumn);
               GridhstColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridhstColumn.AddObjectProperty("Value", AV28DescricaoHis);
               GridhstColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDescricaohis_Enabled), 5, 0, ".", "")));
               GridhstContainer.AddColumnProperties(GridhstColumn);
               GridhstColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridhstColumn.AddObjectProperty("Value", StringUtil.RTrim( AV19Versao));
               GridhstColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVersao_Enabled), 5, 0, ".", "")));
               GridhstContainer.AddColumnProperties(GridhstColumn);
               GridhstContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Allowselection), 1, 0, ".", "")));
               GridhstContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Selectioncolor), 9, 0, ".", "")));
               GridhstContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Allowhovering), 1, 0, ".", "")));
               GridhstContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Hoveringcolor), 9, 0, ".", "")));
               GridhstContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Allowcollapsing), 1, 0, ".", "")));
               GridhstContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridhst_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 89 )
         {
            wbEnd = 0;
            nRC_GXsfl_89 = (short)(nGXsfl_89_idx-1);
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridhstContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridhst", GridhstContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridhstContainerData", GridhstContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridhstContainerData"+"V", GridhstContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridhstContainerData"+"V"+"\" value='"+GridhstContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_86_QC2e( true) ;
         }
         else
         {
            wb_table12_86_QC2e( false) ;
         }
      }

      protected void wb_table7_77_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable6_Internalname, tblUnnamedtable6_Internalname, "", "TableFSHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbhistorico_Internalname, "HIST�RICO DE REVIS�O", "", "", lblTbhistorico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_77_QC2e( true) ;
         }
         else
         {
            wb_table7_77_QC2e( false) ;
         }
      }

      protected void wb_table6_54_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_57_QC2( true) ;
         }
         else
         {
            wb_table13_57_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table13_57_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_54_QC2e( true) ;
         }
         else
         {
            wb_table6_54_QC2e( false) ;
         }
      }

      protected void wb_table13_57_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable12_Internalname, tblUnnamedtable12_Internalname, "", "Table", 0, "", "", 20, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table14_60_QC2( true) ;
         }
         else
         {
            wb_table14_60_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table14_60_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table15_68_QC2( true) ;
         }
         else
         {
            wb_table15_68_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table15_68_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_57_QC2e( true) ;
         }
         else
         {
            wb_table13_57_QC2e( false) ;
         }
      }

      protected void wb_table15_68_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable14_Internalname, tblUnnamedtable14_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbresponsavel_Internalname, "Respons�vel", "", "", lblTbresponsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavResponsavel_Internalname, StringUtil.RTrim( AV13Responsavel), StringUtil.RTrim( context.localUtil.Format( AV13Responsavel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavResponsavel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavResponsavel_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "center", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_68_QC2e( true) ;
         }
         else
         {
            wb_table15_68_QC2e( false) ;
         }
      }

      protected void wb_table14_60_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable13_Internalname, tblUnnamedtable13_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsolicitante_Internalname, "Solicitante", "", "", lblTbsolicitante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSolicitante_Internalname, StringUtil.RTrim( AV14Solicitante), StringUtil.RTrim( context.localUtil.Format( AV14Solicitante, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitante_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSolicitante_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "center", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_60_QC2e( true) ;
         }
         else
         {
            wb_table14_60_QC2e( false) ;
         }
      }

      protected void wb_table5_48_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "TableFSHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbenvolvidos_Internalname, "ENVOLVIDOS", "", "", lblTbenvolvidos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_48_QC2e( true) ;
         }
         else
         {
            wb_table5_48_QC2e( false) ;
         }
      }

      protected void wb_table4_38_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbspac1_Internalname, "����", "", "", lblTbspac1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_sistemanom_Internalname, "Nome:", "", "", lblTextblockcontagemresultado_sistemanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_SistemaNom_Internalname, A495ContagemResultado_SistemaNom, StringUtil.RTrim( context.localUtil.Format( A495ContagemResultado_SistemaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_SistemaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_38_QC2e( true) ;
         }
         else
         {
            wb_table4_38_QC2e( false) ;
         }
      }

      protected void wb_table3_32_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "TableFSHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsistema_Internalname, lblTbsistema_Caption, "", "", lblTbsistema_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_32_QC2e( true) ;
         }
         else
         {
            wb_table3_32_QC2e( false) ;
         }
      }

      protected void wb_table2_5_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table16_8_QC2( true) ;
         }
         else
         {
            wb_table16_8_QC2( false) ;
         }
         return  ;
      }

      protected void wb_table16_8_QC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_QC2e( true) ;
         }
         else
         {
            wb_table2_5_QC2e( false) ;
         }
      }

      protected void wb_table16_8_QC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable15_Internalname, tblUnnamedtable15_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV5LogoMarca_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5LogoMarca))&&String.IsNullOrEmpty(StringUtil.RTrim( AV48Logomarca_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5LogoMarca)));
            GxWebStd.gx_bitmap( context, imgavLogomarca_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV5LogoMarca)) ? AV48Logomarca_GXI : context.PathToRelativeUrl( AV5LogoMarca)), "", "", "", context.GetTheme( ), 1, 0, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV5LogoMarca_IsBlob, false, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbcontratante_Internalname, lblTbcontratante_Caption, "", "", lblTbcontratante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsecretaria_Internalname, "�", "", "", lblTbsecretaria_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblinha1_Internalname, "----------------------------------------------------------------------------------------------------------------------", "", "", lblTblinha1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitle_Internalname, lblTbtitle_Caption, "", "", lblTbtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblinha2_Internalname, "----------------------------------------------------------------------------------------------------------------------", "", "", lblTblinha2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdepartamento_Internalname, "�", "", "", lblTbdepartamento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Requisitos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_8_QC2e( true) ;
         }
         else
         {
            wb_table16_8_QC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQC2( ) ;
         WSQC2( ) ;
         WEQC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122125715");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_requisitos.js", "?20203122125715");
            context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
            context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_892( )
      {
         edtavData_Internalname = "vDATA_"+sGXsfl_89_idx;
         edtavDemanda_Internalname = "vDEMANDA_"+sGXsfl_89_idx;
         edtavAutor_Internalname = "vAUTOR_"+sGXsfl_89_idx;
         edtavDescricaohis_Internalname = "vDESCRICAOHIS_"+sGXsfl_89_idx;
         edtavVersao_Internalname = "vVERSAO_"+sGXsfl_89_idx;
      }

      protected void SubsflControlProps_fel_892( )
      {
         edtavData_Internalname = "vDATA_"+sGXsfl_89_fel_idx;
         edtavDemanda_Internalname = "vDEMANDA_"+sGXsfl_89_fel_idx;
         edtavAutor_Internalname = "vAUTOR_"+sGXsfl_89_fel_idx;
         edtavDescricaohis_Internalname = "vDESCRICAOHIS_"+sGXsfl_89_fel_idx;
         edtavVersao_Internalname = "vVERSAO_"+sGXsfl_89_fel_idx;
      }

      protected void sendrow_892( )
      {
         SubsflControlProps_892( ) ;
         WBQC0( ) ;
         if ( ( subGridhst_Rows * 1 == 0 ) || ( nGXsfl_89_idx <= subGridhst_Recordsperpage( ) * 1 ) )
         {
            GridhstRow = GXWebRow.GetNew(context,GridhstContainer);
            if ( subGridhst_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridhst_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridhst_Class, "") != 0 )
               {
                  subGridhst_Linesclass = subGridhst_Class+"Odd";
               }
            }
            else if ( subGridhst_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridhst_Backstyle = 0;
               subGridhst_Backcolor = subGridhst_Allbackcolor;
               if ( StringUtil.StrCmp(subGridhst_Class, "") != 0 )
               {
                  subGridhst_Linesclass = subGridhst_Class+"Uniform";
               }
            }
            else if ( subGridhst_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridhst_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridhst_Class, "") != 0 )
               {
                  subGridhst_Linesclass = subGridhst_Class+"Odd";
               }
               subGridhst_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridhst_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridhst_Backstyle = 1;
               if ( ((int)((nGXsfl_89_idx) % (2))) == 0 )
               {
                  subGridhst_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridhst_Class, "") != 0 )
                  {
                     subGridhst_Linesclass = subGridhst_Class+"Even";
                  }
               }
               else
               {
                  subGridhst_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridhst_Class, "") != 0 )
                  {
                     subGridhst_Linesclass = subGridhst_Class+"Odd";
                  }
               }
            }
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridhst_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_89_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridhstRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavData_Internalname,context.localUtil.Format(AV15Data, "99/99/99"),context.localUtil.Format( AV15Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavData_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavData_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridhstRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDemanda_Internalname,(String)AV20Demanda,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDemanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDemanda_Enabled,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridhstRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAutor_Internalname,StringUtil.RTrim( AV17Autor),StringUtil.RTrim( context.localUtil.Format( AV17Autor, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAutor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAutor_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridhstRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDescricaohis_Internalname,(String)AV28DescricaoHis,(String)AV28DescricaoHis,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDescricaohis_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDescricaohis_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)89,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridhstContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridhstRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVersao_Internalname,StringUtil.RTrim( AV19Versao),StringUtil.RTrim( context.localUtil.Format( AV19Versao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVersao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVersao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GridhstContainer.AddRow(GridhstRow);
            nGXsfl_89_idx = (short)(((subGridhst_Islastpage==1)&&(nGXsfl_89_idx+1>subGridhst_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         /* End function sendrow_892 */
      }

      protected void SubsflControlProps_2997( )
      {
         lblTbspac6_Internalname = "TBSPAC6_"+sGXsfl_299_idx;
         lblTextblockrequisito_identificador_Internalname = "TEXTBLOCKREQUISITO_IDENTIFICADOR_"+sGXsfl_299_idx;
         edtavRequisito_identificador_Internalname = "vREQUISITO_IDENTIFICADOR_"+sGXsfl_299_idx;
         lblTextblockrequisito_tipo_Internalname = "TEXTBLOCKREQUISITO_TIPO_"+sGXsfl_299_idx;
         edtavRequisito_tipo_Internalname = "vREQUISITO_TIPO_"+sGXsfl_299_idx;
         lblTextblockrequisito_agrupador_Internalname = "TEXTBLOCKREQUISITO_AGRUPADOR_"+sGXsfl_299_idx;
         edtavRequisito_agrupador_Internalname = "vREQUISITO_AGRUPADOR_"+sGXsfl_299_idx;
         lblTextblockrequisito_titulo_Internalname = "TEXTBLOCKREQUISITO_TITULO_"+sGXsfl_299_idx;
         edtavRequisito_titulo_Internalname = "vREQUISITO_TITULO_"+sGXsfl_299_idx;
         lblTextblockrequisito_descricao_Internalname = "TEXTBLOCKREQUISITO_DESCRICAO_"+sGXsfl_299_idx;
         edtavRequisito_descricao_Internalname = "vREQUISITO_DESCRICAO_"+sGXsfl_299_idx;
         lblTextblockrequisito_prioridade_Internalname = "TEXTBLOCKREQUISITO_PRIORIDADE_"+sGXsfl_299_idx;
         cmbavRequisito_prioridade_Internalname = "vREQUISITO_PRIORIDADE_"+sGXsfl_299_idx;
         lblTextblockrequisito_status_Internalname = "TEXTBLOCKREQUISITO_STATUS_"+sGXsfl_299_idx;
         edtavRequisito_status_Internalname = "vREQUISITO_STATUS_"+sGXsfl_299_idx;
         lblTextblockrequisito_referenciatecnica_Internalname = "TEXTBLOCKREQUISITO_REFERENCIATECNICA_"+sGXsfl_299_idx;
         edtavRequisito_referenciatecnica_Internalname = "vREQUISITO_REFERENCIATECNICA_"+sGXsfl_299_idx;
         lblTextblockrequisito_restricao_Internalname = "TEXTBLOCKREQUISITO_RESTRICAO_"+sGXsfl_299_idx;
         edtavRequisito_restricao_Internalname = "vREQUISITO_RESTRICAO_"+sGXsfl_299_idx;
      }

      protected void SubsflControlProps_fel_2997( )
      {
         lblTbspac6_Internalname = "TBSPAC6_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_identificador_Internalname = "TEXTBLOCKREQUISITO_IDENTIFICADOR_"+sGXsfl_299_fel_idx;
         edtavRequisito_identificador_Internalname = "vREQUISITO_IDENTIFICADOR_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_tipo_Internalname = "TEXTBLOCKREQUISITO_TIPO_"+sGXsfl_299_fel_idx;
         edtavRequisito_tipo_Internalname = "vREQUISITO_TIPO_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_agrupador_Internalname = "TEXTBLOCKREQUISITO_AGRUPADOR_"+sGXsfl_299_fel_idx;
         edtavRequisito_agrupador_Internalname = "vREQUISITO_AGRUPADOR_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_titulo_Internalname = "TEXTBLOCKREQUISITO_TITULO_"+sGXsfl_299_fel_idx;
         edtavRequisito_titulo_Internalname = "vREQUISITO_TITULO_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_descricao_Internalname = "TEXTBLOCKREQUISITO_DESCRICAO_"+sGXsfl_299_fel_idx;
         edtavRequisito_descricao_Internalname = "vREQUISITO_DESCRICAO_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_prioridade_Internalname = "TEXTBLOCKREQUISITO_PRIORIDADE_"+sGXsfl_299_fel_idx;
         cmbavRequisito_prioridade_Internalname = "vREQUISITO_PRIORIDADE_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_status_Internalname = "TEXTBLOCKREQUISITO_STATUS_"+sGXsfl_299_fel_idx;
         edtavRequisito_status_Internalname = "vREQUISITO_STATUS_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_referenciatecnica_Internalname = "TEXTBLOCKREQUISITO_REFERENCIATECNICA_"+sGXsfl_299_fel_idx;
         edtavRequisito_referenciatecnica_Internalname = "vREQUISITO_REFERENCIATECNICA_"+sGXsfl_299_fel_idx;
         lblTextblockrequisito_restricao_Internalname = "TEXTBLOCKREQUISITO_RESTRICAO_"+sGXsfl_299_fel_idx;
         edtavRequisito_restricao_Internalname = "vREQUISITO_RESTRICAO_"+sGXsfl_299_fel_idx;
      }

      protected void sendrow_2997( )
      {
         SubsflControlProps_2997( ) ;
         WBQC0( ) ;
         GridrnfRow = GXWebRow.GetNew(context,GridrnfContainer);
         if ( subGridrnf_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridrnf_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridrnf_Class, "") != 0 )
            {
               subGridrnf_Linesclass = subGridrnf_Class+"Odd";
            }
         }
         else if ( subGridrnf_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridrnf_Backstyle = 0;
            subGridrnf_Backcolor = subGridrnf_Allbackcolor;
            if ( StringUtil.StrCmp(subGridrnf_Class, "") != 0 )
            {
               subGridrnf_Linesclass = subGridrnf_Class+"Uniform";
            }
         }
         else if ( subGridrnf_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridrnf_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridrnf_Class, "") != 0 )
            {
               subGridrnf_Linesclass = subGridrnf_Class+"Odd";
            }
            subGridrnf_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridrnf_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridrnf_Backstyle = 1;
            if ( ((int)(((nGXsfl_299_idx-1)/ (decimal)(1)) % (2))) == 0 )
            {
               subGridrnf_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridrnf_Class, "") != 0 )
               {
                  subGridrnf_Linesclass = subGridrnf_Class+"Odd";
               }
            }
            else
            {
               subGridrnf_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridrnf_Class, "") != 0 )
               {
                  subGridrnf_Linesclass = subGridrnf_Class+"Even";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            if ( ( 1 == 0 ) && ( nGXsfl_299_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subGridrnf_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_299_idx+"\">") ;
            }
            if ( 1 > 0 )
            {
               if ( ( 1 == 1 ) || ( ((int)((nGXsfl_299_idx) % (1))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subGridrnf_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_299_idx+"\">") ;
               }
            }
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTbspac6_Internalname,(String)"����",(String)"",(String)"",(String)lblTbspac6_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlockTitleWWP",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_identificador_Internalname,(String)"Identificador",(String)"",(String)"",(String)lblTextblockrequisito_identificador_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Table start */
         GridrnfRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTablemergedrequisito_identificador_Internalname+"_"+sGXsfl_299_idx,(short)1,(String)"TableMerged",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)0,(short)0,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrnfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_identificador_Internalname,(String)AV40Requisito_Identificador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRequisito_identificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavRequisito_identificador_Enabled,(short)0,(String)"text",(String)"",(short)15,(String)"chr",(short)1,(String)"row",(short)15,(short)0,(short)0,(short)299,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_tipo_Internalname,(String)"- Tipo",(String)"",(String)"",(String)lblTextblockrequisito_tipo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrnfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_tipo_Internalname,(String)AV41Requisito_Tipo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRequisito_tipo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavRequisito_tipo_Enabled,(short)0,(String)"text",(String)"",(short)15,(String)"chr",(short)1,(String)"row",(short)15,(short)0,(short)0,(short)299,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("table");
         }
         /* End of table */
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_agrupador_Internalname,(String)"Agrupador",(String)"",(String)"",(String)lblTextblockrequisito_agrupador_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrnfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_agrupador_Internalname,(String)AV33Requisito_Agrupador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRequisito_agrupador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavRequisito_agrupador_Enabled,(short)0,(String)"text",(String)"",(short)25,(String)"chr",(short)1,(String)"row",(short)25,(short)0,(short)0,(short)299,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_titulo_Internalname,(String)lblTextblockrequisito_titulo_Caption,(String)"",(String)"",(String)lblTextblockrequisito_titulo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrnfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_titulo_Internalname,(String)AV32Requisito_Titulo,(String)"",(String)"",(short)0,(short)1,(int)edtavRequisito_titulo_Enabled,(short)0,(short)80,(String)"chr",(short)4,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"250",(short)-1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_descricao_Internalname,(String)"Descri��o",(String)"",(String)"",(String)lblTextblockrequisito_descricao_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrnfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_descricao_Internalname,(String)AV34Requisito_Descricao,(String)"",(String)"",(short)2,(short)1,(int)edtavRequisito_descricao_Enabled,(short)0,(short)80,(String)"chr",(short)4,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"500",(short)1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_prioridade_Internalname,(String)"Prioridade",(String)"",(String)"",(String)lblTextblockrequisito_prioridade_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( ( nGXsfl_299_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vREQUISITO_PRIORIDADE_" + sGXsfl_299_idx;
            cmbavRequisito_prioridade.Name = GXCCtl;
            cmbavRequisito_prioridade.WebTags = "";
            cmbavRequisito_prioridade.addItem("1", "Alta", 0);
            cmbavRequisito_prioridade.addItem("2", "Alta M�dia", 0);
            cmbavRequisito_prioridade.addItem("3", "Alta Baixa", 0);
            cmbavRequisito_prioridade.addItem("4", "M�dia Alta", 0);
            cmbavRequisito_prioridade.addItem("5", "M�dia M�dia", 0);
            cmbavRequisito_prioridade.addItem("6", "M�dia Baixa", 0);
            cmbavRequisito_prioridade.addItem("7", "Baixa Alta", 0);
            cmbavRequisito_prioridade.addItem("8", "Baixa M�dia", 0);
            cmbavRequisito_prioridade.addItem("9", "Baixa Baixa", 0);
            if ( cmbavRequisito_prioridade.ItemCount > 0 )
            {
               AV42Requisito_Prioridade = (short)(NumberUtil.Val( cmbavRequisito_prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavRequisito_prioridade_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0)));
            }
         }
         /* ComboBox */
         GridrnfRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavRequisito_prioridade,(String)cmbavRequisito_prioridade_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0)),(short)1,(String)cmbavRequisito_prioridade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)1,cmbavRequisito_prioridade.Enabled,(short)0,(short)0,(short)0,(String)"em",(short)0,(String)"",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)false});
         cmbavRequisito_prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV42Requisito_Prioridade), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRequisito_prioridade_Internalname, "Values", (String)(cmbavRequisito_prioridade.ToJavascriptSource()));
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_status_Internalname,(String)"Status",(String)"",(String)"",(String)lblTextblockrequisito_status_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrnfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_status_Internalname,StringUtil.RTrim( AV43Requisito_Status),StringUtil.RTrim( context.localUtil.Format( AV43Requisito_Status, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRequisito_status_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavRequisito_status_Enabled,(short)0,(String)"text",(String)"",(short)50,(String)"chr",(short)1,(String)"row",(short)50,(short)0,(short)0,(short)299,(short)1,(short)-1,(short)-1,(bool)false,(String)"Nome",(String)"left",(bool)true});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_referenciatecnica_Internalname,"Ref. T�cnica",(String)"",(String)"",(String)lblTextblockrequisito_referenciatecnica_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrnfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_referenciatecnica_Internalname,(String)AV35Requisito_ReferenciaTecnica,(String)"",(String)"",(short)0,(short)1,(int)edtavRequisito_referenciatecnica_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"2097152",(short)-1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         GridrnfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrnf_Linesclass,(String)""});
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrnfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrequisito_restricao_Internalname,(String)"Restri��es",(String)"",(String)"",(String)lblTextblockrequisito_restricao_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrnfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_restricao_Internalname,(String)AV36Requisito_Restricao,(String)"",(String)"",(short)0,(short)1,(int)edtavRequisito_restricao_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"2097152",(short)-1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         GridrnfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("cell");
         }
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            GridrnfContainer.CloseTag("row");
         }
         /* End of Columns property logic. */
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            if ( 1 > 0 )
            {
               if ( ((int)((nGXsfl_299_idx) % (1))) == 0 )
               {
                  context.WriteHtmlTextNl( "</tr>") ;
               }
            }
         }
         GridrnfContainer.AddRow(GridrnfRow);
         nGXsfl_299_idx = (short)(((subGridrnf_Islastpage==1)&&(nGXsfl_299_idx+1>subGridrnf_Recordsperpage( )) ? 1 : nGXsfl_299_idx+1));
         sGXsfl_299_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_299_idx), 4, 0)), 4, "0") + sGXsfl_212_idx;
         SubsflControlProps_2997( ) ;
         /* End function sendrow_2997 */
      }

      protected void SubsflControlProps_2125( )
      {
         lblTbspac4_Internalname = "TBSPAC4_"+sGXsfl_212_idx;
         edtavRequisito_codigo_Internalname = "vREQUISITO_CODIGO_"+sGXsfl_212_idx;
         lblTextblockidentificador_Internalname = "TEXTBLOCKIDENTIFICADOR_"+sGXsfl_212_idx;
         edtavIdentificador_Internalname = "vIDENTIFICADOR_"+sGXsfl_212_idx;
         lblTextblocktipo_Internalname = "TEXTBLOCKTIPO_"+sGXsfl_212_idx;
         edtavTipo_Internalname = "vTIPO_"+sGXsfl_212_idx;
         lblTextblockagrupador_Internalname = "TEXTBLOCKAGRUPADOR_"+sGXsfl_212_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_212_idx;
         lblTextblocknome_Internalname = "TEXTBLOCKNOME_"+sGXsfl_212_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_212_idx;
         lblTextblockdescricao_Internalname = "TEXTBLOCKDESCRICAO_"+sGXsfl_212_idx;
         edtavDescricao_Internalname = "vDESCRICAO_"+sGXsfl_212_idx;
         lblTextblockprioridade_Internalname = "TEXTBLOCKPRIORIDADE_"+sGXsfl_212_idx;
         edtavPrioridade_Internalname = "vPRIORIDADE_"+sGXsfl_212_idx;
         lblTextblockstatus_Internalname = "TEXTBLOCKSTATUS_"+sGXsfl_212_idx;
         edtavStatus_Internalname = "vSTATUS_"+sGXsfl_212_idx;
         lblTextblockreferencia_Internalname = "TEXTBLOCKREFERENCIA_"+sGXsfl_212_idx;
         edtavReferencia_Internalname = "vREFERENCIA_"+sGXsfl_212_idx;
         lblTextblockrestricao_Internalname = "TEXTBLOCKRESTRICAO_"+sGXsfl_212_idx;
         edtavRestricao_Internalname = "vRESTRICAO_"+sGXsfl_212_idx;
         subGridrnf_Internalname = "GRIDRNF_"+sGXsfl_212_idx;
      }

      protected void SubsflControlProps_fel_2125( )
      {
         lblTbspac4_Internalname = "TBSPAC4_"+sGXsfl_212_fel_idx;
         edtavRequisito_codigo_Internalname = "vREQUISITO_CODIGO_"+sGXsfl_212_fel_idx;
         lblTextblockidentificador_Internalname = "TEXTBLOCKIDENTIFICADOR_"+sGXsfl_212_fel_idx;
         edtavIdentificador_Internalname = "vIDENTIFICADOR_"+sGXsfl_212_fel_idx;
         lblTextblocktipo_Internalname = "TEXTBLOCKTIPO_"+sGXsfl_212_fel_idx;
         edtavTipo_Internalname = "vTIPO_"+sGXsfl_212_fel_idx;
         lblTextblockagrupador_Internalname = "TEXTBLOCKAGRUPADOR_"+sGXsfl_212_fel_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_212_fel_idx;
         lblTextblocknome_Internalname = "TEXTBLOCKNOME_"+sGXsfl_212_fel_idx;
         edtavNome_Internalname = "vNOME_"+sGXsfl_212_fel_idx;
         lblTextblockdescricao_Internalname = "TEXTBLOCKDESCRICAO_"+sGXsfl_212_fel_idx;
         edtavDescricao_Internalname = "vDESCRICAO_"+sGXsfl_212_fel_idx;
         lblTextblockprioridade_Internalname = "TEXTBLOCKPRIORIDADE_"+sGXsfl_212_fel_idx;
         edtavPrioridade_Internalname = "vPRIORIDADE_"+sGXsfl_212_fel_idx;
         lblTextblockstatus_Internalname = "TEXTBLOCKSTATUS_"+sGXsfl_212_fel_idx;
         edtavStatus_Internalname = "vSTATUS_"+sGXsfl_212_fel_idx;
         lblTextblockreferencia_Internalname = "TEXTBLOCKREFERENCIA_"+sGXsfl_212_fel_idx;
         edtavReferencia_Internalname = "vREFERENCIA_"+sGXsfl_212_fel_idx;
         lblTextblockrestricao_Internalname = "TEXTBLOCKRESTRICAO_"+sGXsfl_212_fel_idx;
         edtavRestricao_Internalname = "vRESTRICAO_"+sGXsfl_212_fel_idx;
         subGridrnf_Internalname = "GRIDRNF_"+sGXsfl_212_fel_idx;
      }

      protected void sendrow_2125( )
      {
         SubsflControlProps_2125( ) ;
         WBQC0( ) ;
         GridrfRow = GXWebRow.GetNew(context,GridrfContainer);
         if ( subGridrf_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridrf_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridrf_Class, "") != 0 )
            {
               subGridrf_Linesclass = subGridrf_Class+"Odd";
            }
         }
         else if ( subGridrf_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridrf_Backstyle = 0;
            subGridrf_Backcolor = subGridrf_Allbackcolor;
            if ( StringUtil.StrCmp(subGridrf_Class, "") != 0 )
            {
               subGridrf_Linesclass = subGridrf_Class+"Uniform";
            }
         }
         else if ( subGridrf_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridrf_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridrf_Class, "") != 0 )
            {
               subGridrf_Linesclass = subGridrf_Class+"Odd";
            }
            subGridrf_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridrf_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridrf_Backstyle = 1;
            if ( ((int)(((nGXsfl_212_idx-1)/ (decimal)(1)) % (2))) == 0 )
            {
               subGridrf_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridrf_Class, "") != 0 )
               {
                  subGridrf_Linesclass = subGridrf_Class+"Odd";
               }
            }
            else
            {
               subGridrf_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridrf_Class, "") != 0 )
               {
                  subGridrf_Linesclass = subGridrf_Class+"Even";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            if ( ( 1 == 0 ) && ( nGXsfl_212_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subGridrf_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_212_idx+"\">") ;
            }
            if ( 1 > 0 )
            {
               if ( ( 1 == 1 ) || ( ((int)((nGXsfl_212_idx) % (1))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subGridrf_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_212_idx+"\">") ;
               }
            }
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTbspac4_Internalname,(String)"����",(String)"",(String)"",(String)lblTbspac4_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlockTitleWWP",(short)0,(String)"",(short)1,(short)1,(short)0});
         /* Single line edit */
         ROClassString = "Attribute";
         GridrfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRequisito_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39Requisito_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(AV39Requisito_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRequisito_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(int)edtavRequisito_codigo_Visible,(short)0,(short)0,(String)"text",(String)"",(short)6,(String)"chr",(short)1,(String)"row",(short)6,(short)0,(short)0,(short)212,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockidentificador_Internalname,(String)lblTextblockidentificador_Caption,(String)"",(String)"",(String)lblTextblockidentificador_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Table start */
         GridrfRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblTablemergedidentificador_Internalname+"_"+sGXsfl_212_idx,(short)1,(String)"TableMerged",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)0,(short)0,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavIdentificador_Internalname,(String)AV25Identificador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavIdentificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavIdentificador_Enabled,(short)0,(String)"text",(String)"",(short)15,(String)"chr",(short)1,(String)"row",(short)15,(short)0,(short)0,(short)212,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblocktipo_Internalname,(String)"- Tipo",(String)"",(String)"",(String)lblTextblocktipo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavTipo_Internalname,(String)AV38Tipo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavTipo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavTipo_Enabled,(short)0,(String)"text",(String)"",(short)15,(String)"chr",(short)1,(String)"row",(short)15,(short)0,(short)0,(short)212,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("table");
         }
         /* End of table */
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockagrupador_Internalname,(String)"Agrupador",(String)"",(String)"",(String)lblTextblockagrupador_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAgrupador_Internalname,(String)AV24Agrupador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAgrupador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavAgrupador_Enabled,(short)0,(String)"text",(String)"",(short)25,(String)"chr",(short)1,(String)"row",(short)25,(short)0,(short)0,(short)212,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblocknome_Internalname,(String)"Nome",(String)"",(String)"",(String)lblTextblocknome_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavNome_Internalname,(String)AV10Nome,(String)"",(String)"",(short)0,(short)1,(int)edtavNome_Enabled,(short)0,(short)80,(String)"chr",(short)4,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"250",(short)-1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockdescricao_Internalname,(String)"Descri��o",(String)"",(String)"",(String)lblTextblockdescricao_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",""+" style=\""+CSSHelper.Prettify( "vertical-align:top")+"\""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavDescricao_Internalname,(String)AV18Descricao,(String)"",(String)"",(short)2,(short)1,(int)edtavDescricao_Enabled,(short)0,(short)80,(String)"chr",(short)4,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"500",(short)1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockprioridade_Internalname,(String)"Prioridade",(String)"",(String)"",(String)lblTextblockprioridade_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrioridade_Internalname,StringUtil.RTrim( AV26Prioridade),StringUtil.RTrim( context.localUtil.Format( AV26Prioridade, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrioridade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavPrioridade_Enabled,(short)0,(String)"text",(String)"",(short)50,(String)"chr",(short)1,(String)"row",(short)50,(short)0,(short)0,(short)212,(short)1,(short)-1,(short)-1,(bool)false,(String)"Nome",(String)"left",(bool)true});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockstatus_Internalname,(String)"Status",(String)"",(String)"",(String)lblTextblockstatus_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridrfRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavStatus_Internalname,StringUtil.RTrim( AV27Status),StringUtil.RTrim( context.localUtil.Format( AV27Status, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavStatus_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)1,(int)edtavStatus_Enabled,(short)0,(String)"text",(String)"",(short)50,(String)"chr",(short)1,(String)"row",(short)50,(short)0,(short)0,(short)212,(short)1,(short)-1,(short)-1,(bool)false,(String)"Nome",(String)"left",(bool)true});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockreferencia_Internalname,(String)"Refer�ncia",(String)"",(String)"",(String)lblTextblockreferencia_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavReferencia_Internalname,(String)AV29Referencia,(String)"",(String)"",(short)0,(short)1,(int)edtavReferencia_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"2097152",(short)-1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         GridrfRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblTextblockrestricao_Internalname,(String)"Restri��es",(String)"",(String)"",(String)lblTextblockrestricao_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Multiple line edit */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         ClassString = "BootstrapAttribute";
         StyleString = "";
         GridrfRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavRestricao_Internalname,(String)AV30Restricao,(String)"",(String)"",(short)0,(short)1,(int)edtavRestricao_Enabled,(short)0,(short)80,(String)"chr",(short)10,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"2097152",(short)-1,(String)"",(String)"",(short)-1,(bool)false,(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         GridrfRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subGridrf_Linesclass,(String)""});
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /*  Child Grid Control  */
         GridrfRow.AddColumnProperties("subfile", -1, isAjaxCallMode( ), new Object[] {(String)"GridrnfContainer"});
         if ( isAjaxCallMode( ) )
         {
            GridrnfContainer = new GXWebGrid( context);
         }
         else
         {
            GridrnfContainer.Clear();
         }
         GridrnfContainer.SetIsFreestyle(true);
         GridrnfContainer.SetWrapped(nGXWrapped);
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<div id=\""+"GridrnfContainer"+"DivS\" data-gxgridid=\"299\">") ;
            sStyleString = "";
            GxWebStd.gx_table_start( context, subGridrnf_Internalname, subGridrnf_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            GridrnfContainer.AddObjectProperty("GridName", "Gridrnf");
         }
         else
         {
            GridrnfContainer.AddObjectProperty("GridName", "Gridrnf");
            GridrnfContainer.AddObjectProperty("Class", StringUtil.RTrim( ""));
            GridrnfContainer.AddObjectProperty("Class", "");
            GridrnfContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Backcolorstyle), 1, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("CmpContext", "");
            GridrnfContainer.AddObjectProperty("InMasterPage", "false");
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTbspac1_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblockidentificador_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", AV40Requisito_Identificador);
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_identificador_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblocktipo_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", AV41Requisito_Tipo);
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_tipo_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblockagrupador_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", AV33Requisito_Agrupador);
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_agrupador_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblocknome_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", AV32Requisito_Titulo);
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_titulo_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblockdescricao_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", AV34Requisito_Descricao);
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_descricao_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblockprioridade_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42Requisito_Prioridade), 2, 0, ".", "")));
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavRequisito_prioridade.Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblockstatus_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", StringUtil.RTrim( AV43Requisito_Status));
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_status_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblockrequisito_referenciatecnica_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", AV35Requisito_ReferenciaTecnica);
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_referenciatecnica_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", lblTextblockrestricao_Caption);
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfColumn.AddObjectProperty("Value", AV36Requisito_Restricao);
            GridrnfColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRequisito_restricao_Enabled), 5, 0, ".", "")));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridrnfContainer.AddColumnProperties(GridrnfColumn);
            GridrnfContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Allowselection), 1, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Selectioncolor), 9, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Allowhovering), 1, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Hoveringcolor), 9, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Allowcollapsing), 1, 0, ".", "")));
            GridrnfContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrnf_Collapsed), 1, 0, ".", "")));
         }
         RFQC7( ) ;
         nRC_GXsfl_299 = (short)(nGXsfl_299_idx-1);
         GXCCtl = "nRC_GXsfl_299_" + sGXsfl_212_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_299), 4, 0, ",", "")));
         if ( GridrnfContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "</table>") ;
         }
         else
         {
            if ( ! isAjaxCallMode( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridrnfContainerData"+"_"+sGXsfl_212_idx, GridrnfContainer.ToJavascriptSource());
            }
            if ( isAjaxCallMode( ) )
            {
               GridrfRow.AddGrid("Gridrnf", GridrnfContainer);
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridrnfContainerData"+"V_"+sGXsfl_212_idx, GridrnfContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridrnfContainerData"+"V_"+sGXsfl_212_idx+"\" value='"+GridrnfContainer.GridValuesHidden()+"'/>") ;
            }
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         GridrfRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("cell");
         }
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            GridrfContainer.CloseTag("row");
         }
         /* End of Columns property logic. */
         if ( GridrfContainer.GetWrapped() == 1 )
         {
            if ( 1 > 0 )
            {
               if ( ((int)((nGXsfl_212_idx) % (1))) == 0 )
               {
                  context.WriteHtmlTextNl( "</tr>") ;
               }
            }
         }
         GridrfContainer.AddRow(GridrfRow);
         nGXsfl_212_idx = (short)(((subGridrf_Islastpage==1)&&(nGXsfl_212_idx+1>subGridrf_Recordsperpage( )) ? 1 : nGXsfl_212_idx+1));
         sGXsfl_212_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_212_idx), 4, 0)), 4, "0");
         SubsflControlProps_2125( ) ;
         /* End function sendrow_2125 */
      }

      protected void init_default_properties( )
      {
         imgavLogomarca_Internalname = "vLOGOMARCA";
         lblTbcontratante_Internalname = "TBCONTRATANTE";
         lblTbsecretaria_Internalname = "TBSECRETARIA";
         lblTblinha1_Internalname = "TBLINHA1";
         lblTbtitle_Internalname = "TBTITLE";
         lblTblinha2_Internalname = "TBLINHA2";
         lblTbdepartamento_Internalname = "TBDEPARTAMENTO";
         tblUnnamedtable15_Internalname = "UNNAMEDTABLE15";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         lblTbsistema_Internalname = "TBSISTEMA";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         lblTbspac1_Internalname = "TBSPAC1";
         lblTextblockcontagemresultado_sistemanom_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SISTEMANOM";
         edtContagemResultado_SistemaNom_Internalname = "CONTAGEMRESULTADO_SISTEMANOM";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         lblTbenvolvidos_Internalname = "TBENVOLVIDOS";
         tblUnnamedtable4_Internalname = "UNNAMEDTABLE4";
         lblTbsolicitante_Internalname = "TBSOLICITANTE";
         edtavSolicitante_Internalname = "vSOLICITANTE";
         tblUnnamedtable13_Internalname = "UNNAMEDTABLE13";
         lblTbresponsavel_Internalname = "TBRESPONSAVEL";
         edtavResponsavel_Internalname = "vRESPONSAVEL";
         tblUnnamedtable14_Internalname = "UNNAMEDTABLE14";
         tblUnnamedtable12_Internalname = "UNNAMEDTABLE12";
         tblUnnamedtable5_Internalname = "UNNAMEDTABLE5";
         lblTbhistorico_Internalname = "TBHISTORICO";
         tblUnnamedtable6_Internalname = "UNNAMEDTABLE6";
         edtavData_Internalname = "vDATA";
         edtavDemanda_Internalname = "vDEMANDA";
         edtavAutor_Internalname = "vAUTOR";
         edtavDescricaohis_Internalname = "vDESCRICAOHIS";
         edtavVersao_Internalname = "vVERSAO";
         tblUnnamedtable11_Internalname = "UNNAMEDTABLE11";
         tblUnnamedtable7_Internalname = "UNNAMEDTABLE7";
         lblTb1_Internalname = "TB1";
         lblTbspac2_Internalname = "TBSPAC2";
         lblTextblockcontagemresultado_demandafm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         lblTextblockrequisitante_Internalname = "TEXTBLOCKREQUISITANTE";
         edtavRequisitante_Internalname = "vREQUISITANTE";
         lblTextblocksetor_Internalname = "TEXTBLOCKSETOR";
         edtavSetor_Internalname = "vSETOR";
         lblTextblockservico_Internalname = "TEXTBLOCKSERVICO";
         edtavServico_Internalname = "vSERVICO";
         lblTextblockcontagemrresultado_sistemasigla_Internalname = "TEXTBLOCKCONTAGEMRRESULTADO_SISTEMASIGLA";
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA";
         lblTextblockcontagemresultado_descricao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DESCRICAO";
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO";
         lblTextblockcontagemresultado_observacao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OBSERVACAO";
         Contagemresultado_observacao_Internalname = "CONTAGEMRESULTADO_OBSERVACAO";
         lblTextblockcontagemresultado_referencia_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_REFERENCIA";
         edtContagemResultado_Referencia_Internalname = "CONTAGEMRESULTADO_REFERENCIA";
         lblTextblockcontagemresultado_restricoes_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_RESTRICOES";
         edtContagemResultado_Restricoes_Internalname = "CONTAGEMRESULTADO_RESTRICOES";
         lblTextblockcontagemresultado_prioridadeprevista_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PRIORIDADEPREVISTA";
         edtContagemResultado_PrioridadePrevista_Internalname = "CONTAGEMRESULTADO_PRIORIDADEPREVISTA";
         lblTextblockcontagemresultado_dataprevista_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATAPREVISTA";
         edtContagemResultado_DataPrevista_Internalname = "CONTAGEMRESULTADO_DATAPREVISTA";
         tblUnnamedtable9_Internalname = "UNNAMEDTABLE9";
         lblTb2_Internalname = "TB2";
         lblTbspac4_Internalname = "TBSPAC4";
         edtavRequisito_codigo_Internalname = "vREQUISITO_CODIGO";
         lblTextblockidentificador_Internalname = "TEXTBLOCKIDENTIFICADOR";
         edtavIdentificador_Internalname = "vIDENTIFICADOR";
         lblTextblocktipo_Internalname = "TEXTBLOCKTIPO";
         edtavTipo_Internalname = "vTIPO";
         tblTablemergedidentificador_Internalname = "TABLEMERGEDIDENTIFICADOR";
         lblTextblockagrupador_Internalname = "TEXTBLOCKAGRUPADOR";
         edtavAgrupador_Internalname = "vAGRUPADOR";
         lblTextblocknome_Internalname = "TEXTBLOCKNOME";
         edtavNome_Internalname = "vNOME";
         lblTextblockdescricao_Internalname = "TEXTBLOCKDESCRICAO";
         edtavDescricao_Internalname = "vDESCRICAO";
         lblTextblockprioridade_Internalname = "TEXTBLOCKPRIORIDADE";
         edtavPrioridade_Internalname = "vPRIORIDADE";
         lblTextblockstatus_Internalname = "TEXTBLOCKSTATUS";
         edtavStatus_Internalname = "vSTATUS";
         lblTextblockreferencia_Internalname = "TEXTBLOCKREFERENCIA";
         edtavReferencia_Internalname = "vREFERENCIA";
         lblTextblockrestricao_Internalname = "TEXTBLOCKRESTRICAO";
         edtavRestricao_Internalname = "vRESTRICAO";
         lblTbspac6_Internalname = "TBSPAC6";
         lblTextblockrequisito_identificador_Internalname = "TEXTBLOCKREQUISITO_IDENTIFICADOR";
         edtavRequisito_identificador_Internalname = "vREQUISITO_IDENTIFICADOR";
         lblTextblockrequisito_tipo_Internalname = "TEXTBLOCKREQUISITO_TIPO";
         edtavRequisito_tipo_Internalname = "vREQUISITO_TIPO";
         tblTablemergedrequisito_identificador_Internalname = "TABLEMERGEDREQUISITO_IDENTIFICADOR";
         lblTextblockrequisito_agrupador_Internalname = "TEXTBLOCKREQUISITO_AGRUPADOR";
         edtavRequisito_agrupador_Internalname = "vREQUISITO_AGRUPADOR";
         lblTextblockrequisito_titulo_Internalname = "TEXTBLOCKREQUISITO_TITULO";
         edtavRequisito_titulo_Internalname = "vREQUISITO_TITULO";
         lblTextblockrequisito_descricao_Internalname = "TEXTBLOCKREQUISITO_DESCRICAO";
         edtavRequisito_descricao_Internalname = "vREQUISITO_DESCRICAO";
         lblTextblockrequisito_prioridade_Internalname = "TEXTBLOCKREQUISITO_PRIORIDADE";
         cmbavRequisito_prioridade_Internalname = "vREQUISITO_PRIORIDADE";
         lblTextblockrequisito_status_Internalname = "TEXTBLOCKREQUISITO_STATUS";
         edtavRequisito_status_Internalname = "vREQUISITO_STATUS";
         lblTextblockrequisito_referenciatecnica_Internalname = "TEXTBLOCKREQUISITO_REFERENCIATECNICA";
         edtavRequisito_referenciatecnica_Internalname = "vREQUISITO_REFERENCIATECNICA";
         lblTextblockrequisito_restricao_Internalname = "TEXTBLOCKREQUISITO_RESTRICAO";
         edtavRequisito_restricao_Internalname = "vREQUISITO_RESTRICAO";
         tblUnnamedtable10_Internalname = "UNNAMEDTABLE10";
         tblUnnamedtable8_Internalname = "UNNAMEDTABLE8";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGridhst_Internalname = "GRIDHST";
         subGridrnf_Internalname = "GRIDRNF";
         subGridrf_Internalname = "GRIDRF";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         subGridrnf_Allowcollapsing = 0;
         lblTextblockrequisito_referenciatecnica_Caption = "Ref. T�cnica";
         edtavStatus_Jsonclick = "";
         edtavPrioridade_Jsonclick = "";
         edtavAgrupador_Jsonclick = "";
         edtavTipo_Jsonclick = "";
         edtavIdentificador_Jsonclick = "";
         lblTextblockidentificador_Caption = "Identificador";
         edtavRequisito_codigo_Jsonclick = "";
         subGridrf_Class = "";
         edtavRequisito_restricao_Enabled = 0;
         edtavRequisito_referenciatecnica_Enabled = 0;
         edtavRequisito_status_Jsonclick = "";
         edtavRequisito_status_Enabled = 0;
         cmbavRequisito_prioridade_Jsonclick = "";
         cmbavRequisito_prioridade.Enabled = 0;
         edtavRequisito_descricao_Enabled = 0;
         edtavRequisito_titulo_Enabled = 0;
         lblTextblockrequisito_titulo_Caption = "Nome";
         edtavRequisito_agrupador_Jsonclick = "";
         edtavRequisito_agrupador_Enabled = 0;
         edtavRequisito_tipo_Jsonclick = "";
         edtavRequisito_tipo_Enabled = 0;
         edtavRequisito_identificador_Jsonclick = "";
         edtavRequisito_identificador_Enabled = 0;
         subGridrnf_Class = "";
         edtavVersao_Jsonclick = "";
         edtavDescricaohis_Jsonclick = "";
         edtavAutor_Jsonclick = "";
         edtavDemanda_Jsonclick = "";
         edtavData_Jsonclick = "";
         edtContagemResultado_SistemaNom_Jsonclick = "";
         edtavSolicitante_Jsonclick = "";
         edtavSolicitante_Enabled = 1;
         edtavResponsavel_Jsonclick = "";
         edtavResponsavel_Enabled = 1;
         subGridhst_Allowcollapsing = 0;
         subGridhst_Allowselection = 0;
         edtavVersao_Enabled = 0;
         edtavDescricaohis_Enabled = 0;
         edtavAutor_Enabled = 0;
         edtavDemanda_Enabled = 0;
         edtavData_Enabled = 0;
         subGridhst_Class = "WorkWithBorder WorkWith";
         edtContagemResultado_DataPrevista_Jsonclick = "";
         edtContagemResultado_PrioridadePrevista_Jsonclick = "";
         Contagemresultado_observacao_Enabled = Convert.ToBoolean( 0);
         edtContagemResultado_Descricao_Jsonclick = "";
         edtContagemrResultado_SistemaSigla_Jsonclick = "";
         edtavServico_Jsonclick = "";
         edtavServico_Enabled = 1;
         edtavSetor_Jsonclick = "";
         edtavSetor_Enabled = 1;
         edtavRequisitante_Jsonclick = "";
         edtavRequisitante_Enabled = 1;
         edtContagemResultado_DemandaFM_Jsonclick = "";
         subGridrf_Allowcollapsing = 0;
         edtavRestricao_Enabled = 0;
         lblTextblockrestricao_Caption = "Restri��es";
         edtavReferencia_Enabled = 0;
         lblTextblockreferencia_Caption = "Refer�ncia";
         edtavStatus_Enabled = 0;
         lblTextblockstatus_Caption = "Status";
         edtavPrioridade_Enabled = 0;
         lblTextblockprioridade_Caption = "Prioridade";
         edtavDescricao_Enabled = 0;
         lblTextblockdescricao_Caption = "Descri��o";
         edtavNome_Enabled = 0;
         lblTextblocknome_Caption = "Nome";
         edtavAgrupador_Enabled = 0;
         lblTextblockagrupador_Caption = "Agrupador";
         edtavTipo_Enabled = 0;
         lblTextblocktipo_Caption = "- Tipo";
         edtavIdentificador_Enabled = 0;
         lblTextblockidentificador_Caption = "Identificador";
         lblTbspac1_Caption = "����";
         lblTbcontratante_Caption = "(Contratante)";
         lblTbsistema_Caption = "Sigla do Sistema: xxxxxxxxxxxxxx";
         lblTbtitle_Caption = "LISTA DE REQUISITOS";
         subGridrnf_Backcolorstyle = 0;
         subGridrf_Backcolorstyle = 0;
         subGridhst_Backcolorstyle = 3;
         edtavRequisito_codigo_Visible = 1;
         subGridhst_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDHST_nFirstRecordOnPage',nv:0},{av:'GRIDHST_nEOF',nv:0},{av:'subGridhst_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',hsh:true,nv:''},{av:'AV14Solicitante',fld:'vSOLICITANTE',pic:'@!',hsh:true,nv:''},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A1131LogResponsavel_Observacao',fld:'LOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''},{av:'GRIDRF_nFirstRecordOnPage',nv:0},{av:'GRIDRF_nEOF',nv:0},{av:'edtavRequisito_codigo_Visible',ctrl:'vREQUISITO_CODIGO',prop:'Visible'},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'GRIDRNF_nFirstRecordOnPage',nv:0},{av:'GRIDRNF_nEOF',nv:0},{av:'AV10Nome',fld:'vNOME',pic:'',nv:''},{av:'A1931Requisito_Ordem',fld:'REQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV39Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31i',fld:'vI',pic:'ZZZ9',nv:0},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A1925Requisito_ReferenciaTecnica',fld:'REQUISITO_REFERENCIATECNICA',pic:'',nv:''},{av:'A1929Requisito_Restricao',fld:'REQUISITO_RESTRICAO',pic:'',nv:''},{av:'A2002Requisito_Prioridade',fld:'REQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A2042TipoRequisito_Identificador',fld:'TIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("GRIDRF.LOAD","{handler:'E13QC5',iparms:[{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV31i',fld:'vI',pic:'ZZZ9',nv:0},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A2002Requisito_Prioridade',fld:'REQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A2042TipoRequisito_Identificador',fld:'TIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''}],oparms:[{av:'AV31i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV25Identificador',fld:'vIDENTIFICADOR',pic:'',nv:''},{av:'lblTextblockidentificador_Caption',ctrl:'TEXTBLOCKIDENTIFICADOR',prop:'Caption'},{av:'AV24Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV18Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'AV29Referencia',fld:'vREFERENCIA',pic:'',nv:''},{av:'AV30Restricao',fld:'vRESTRICAO',pic:'',nv:''},{av:'AV26Prioridade',fld:'vPRIORIDADE',pic:'@!',nv:''},{av:'AV27Status',fld:'vSTATUS',pic:'@!',nv:''},{av:'AV38Tipo',fld:'vTIPO',pic:'',nv:''}]}");
         setEventMetadata("GRIDRNF.LOAD","{handler:'E15QC7',iparms:[{av:'A1931Requisito_Ordem',fld:'REQUISITO_ORDEM',pic:'ZZ9',nv:0},{av:'A1999Requisito_ReqCod',fld:'REQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV39Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31i',fld:'vI',pic:'ZZZ9',nv:0},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A1925Requisito_ReferenciaTecnica',fld:'REQUISITO_REFERENCIATECNICA',pic:'',nv:''},{av:'A1929Requisito_Restricao',fld:'REQUISITO_RESTRICAO',pic:'',nv:''},{av:'A2002Requisito_Prioridade',fld:'REQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A2042TipoRequisito_Identificador',fld:'TIPOREQUISITO_IDENTIFICADOR',pic:'',nv:''}],oparms:[{av:'AV31i',fld:'vI',pic:'ZZZ9',nv:0},{av:'lblTextblockrequisito_titulo_Caption',ctrl:'TEXTBLOCKREQUISITO_TITULO',prop:'Caption'},{av:'AV40Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV32Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV33Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV34Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV35Requisito_ReferenciaTecnica',fld:'vREQUISITO_REFERENCIATECNICA',pic:'',nv:''},{av:'AV36Requisito_Restricao',fld:'vREQUISITO_RESTRICAO',pic:'',nv:''},{av:'AV42Requisito_Prioridade',fld:'vREQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'AV43Requisito_Status',fld:'vREQUISITO_STATUS',pic:'@!',nv:''},{av:'AV41Requisito_Tipo',fld:'vREQUISITO_TIPO',pic:'',nv:''}]}");
         setEventMetadata("GRIDHST.LOAD","{handler:'E12QC2',iparms:[{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',hsh:true,nv:''},{av:'AV14Solicitante',fld:'vSOLICITANTE',pic:'@!',hsh:true,nv:''},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1131LogResponsavel_Observacao',fld:'LOGRESPONSAVEL_OBSERVACAO',pic:'',nv:''}],oparms:[{av:'AV15Data',fld:'vDATA',pic:'',nv:''},{av:'AV20Demanda',fld:'vDEMANDA',pic:'',nv:''},{av:'AV17Autor',fld:'vAUTOR',pic:'@!',nv:''},{av:'AV28DescricaoHis',fld:'vDESCRICAOHIS',pic:'',nv:''},{av:'AV19Versao',fld:'vVERSAO',pic:'@!',nv:''}]}");
         setEventMetadata("GRIDRF.REFRESH","{handler:'E14QC2',iparms:[],oparms:[{av:'AV31i',fld:'vI',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("GRIDRNF.REFRESH","{handler:'E16QC2',iparms:[],oparms:[{av:'AV31i',fld:'vI',pic:'ZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A493ContagemResultado_DemandaFM = "";
         AV14Solicitante = "";
         A1131LogResponsavel_Observacao = "";
         GXKey = "";
         forbiddenHiddens = "";
         AV10Nome = "";
         A2001Requisito_Identificador = "";
         A1927Requisito_Titulo = "";
         A1926Requisito_Agrupador = "";
         A1923Requisito_Descricao = "";
         A1925Requisito_ReferenciaTecnica = "";
         A1929Requisito_Restricao = "";
         A2042TipoRequisito_Identificador = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A514ContagemResultado_Observacao = "";
         AV13Responsavel = "";
         AV6Requisitante = "";
         AV7Setor = "";
         AV8Servico = "";
         A494ContagemResultado_Descricao = "";
         A1585ContagemResultado_Referencia = "";
         A1586ContagemResultado_Restricoes = "";
         A1587ContagemResultado_PrioridadePrevista = "";
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Data = DateTime.MinValue;
         AV20Demanda = "";
         AV17Autor = "";
         AV28DescricaoHis = "";
         AV19Versao = "";
         AV25Identificador = "";
         AV38Tipo = "";
         AV24Agrupador = "";
         AV18Descricao = "";
         AV26Prioridade = "";
         AV27Status = "";
         AV29Referencia = "";
         AV30Restricao = "";
         AV40Requisito_Identificador = "";
         AV41Requisito_Tipo = "";
         AV33Requisito_Agrupador = "";
         AV32Requisito_Titulo = "";
         AV34Requisito_Descricao = "";
         AV43Requisito_Status = "";
         AV35Requisito_ReferenciaTecnica = "";
         AV36Requisito_Restricao = "";
         GXCCtl = "";
         GridhstContainer = new GXWebGrid( context);
         GridrfContainer = new GXWebGrid( context);
         GridrnfContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00QC2_A489ContagemResultado_SistemaCod = new int[1] ;
         H00QC2_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00QC2_A456ContagemResultado_Codigo = new int[1] ;
         H00QC2_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00QC2_A495ContagemResultado_SistemaNom = new String[] {""} ;
         H00QC2_n495ContagemResultado_SistemaNom = new bool[] {false} ;
         H00QC2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00QC2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00QC2_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00QC2_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00QC2_A494ContagemResultado_Descricao = new String[] {""} ;
         H00QC2_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00QC2_A514ContagemResultado_Observacao = new String[] {""} ;
         H00QC2_n514ContagemResultado_Observacao = new bool[] {false} ;
         H00QC2_A1585ContagemResultado_Referencia = new String[] {""} ;
         H00QC2_n1585ContagemResultado_Referencia = new bool[] {false} ;
         H00QC2_A1586ContagemResultado_Restricoes = new String[] {""} ;
         H00QC2_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         H00QC2_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         H00QC2_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         H00QC2_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         H00QC2_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         A495ContagemResultado_SistemaNom = "";
         A509ContagemrResultado_SistemaSigla = "";
         H00QC3_AGRIDHST_nRecordCount = new long[1] ;
         AV5LogoMarca = "";
         hsh = "";
         AV37WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00QC4_A489ContagemResultado_SistemaCod = new int[1] ;
         H00QC4_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00QC4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00QC4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00QC4_A601ContagemResultado_Servico = new int[1] ;
         H00QC4_n601ContagemResultado_Servico = new bool[] {false} ;
         H00QC4_A456ContagemResultado_Codigo = new int[1] ;
         H00QC4_A1636ContagemResultado_ServicoSS = new int[1] ;
         H00QC4_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         H00QC4_A602ContagemResultado_OSVinculada = new int[1] ;
         H00QC4_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00QC4_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00QC4_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00QC4_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00QC4_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00QC4_A508ContagemResultado_Owner = new int[1] ;
         H00QC4_A890ContagemResultado_Responsavel = new int[1] ;
         H00QC4_n890ContagemResultado_Responsavel = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         H00QC5_A29Contratante_Codigo = new int[1] ;
         H00QC5_n29Contratante_Codigo = new bool[] {false} ;
         H00QC5_A5AreaTrabalho_Codigo = new int[1] ;
         H00QC5_A1125Contratante_LogoNomeArq = new String[] {""} ;
         H00QC5_n1125Contratante_LogoNomeArq = new bool[] {false} ;
         H00QC5_A1126Contratante_LogoTipoArq = new String[] {""} ;
         H00QC5_n1126Contratante_LogoTipoArq = new bool[] {false} ;
         H00QC5_A10Contratante_NomeFantasia = new String[] {""} ;
         H00QC5_A1124Contratante_LogoArquivo = new String[] {""} ;
         H00QC5_n1124Contratante_LogoArquivo = new bool[] {false} ;
         A1125Contratante_LogoNomeArq = "";
         A1124Contratante_LogoArquivo_Filename = "";
         A1126Contratante_LogoTipoArq = "";
         A1124Contratante_LogoArquivo_Filetype = "";
         A10Contratante_NomeFantasia = "";
         A1124Contratante_LogoArquivo = "";
         AV48Logomarca_GXI = "";
         H00QC6_A1797LogResponsavel_Codigo = new long[1] ;
         H00QC6_A892LogResponsavel_DemandaCod = new int[1] ;
         H00QC6_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00QC6_A1131LogResponsavel_Observacao = new String[] {""} ;
         H00QC6_n1131LogResponsavel_Observacao = new bool[] {false} ;
         GridhstRow = new GXWebRow();
         AV23NaoFuncionais = new GxSimpleCollection();
         H00QC7_A456ContagemResultado_Codigo = new int[1] ;
         H00QC7_A508ContagemResultado_Owner = new int[1] ;
         H00QC8_A57Usuario_PessoaCod = new int[1] ;
         H00QC8_A1Usuario_Codigo = new int[1] ;
         H00QC8_A58Usuario_PessoaNom = new String[] {""} ;
         H00QC8_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         H00QC9_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         H00QC9_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         H00QC9_A2049Requisito_TipoReqCod = new int[1] ;
         H00QC9_n2049Requisito_TipoReqCod = new bool[] {false} ;
         H00QC9_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         H00QC9_A2001Requisito_Identificador = new String[] {""} ;
         H00QC9_n2001Requisito_Identificador = new bool[] {false} ;
         H00QC9_A1923Requisito_Descricao = new String[] {""} ;
         H00QC9_n1923Requisito_Descricao = new bool[] {false} ;
         H00QC9_A2002Requisito_Prioridade = new short[1] ;
         H00QC9_n2002Requisito_Prioridade = new bool[] {false} ;
         H00QC9_A1934Requisito_Status = new short[1] ;
         H00QC9_n1934Requisito_Status = new bool[] {false} ;
         H00QC9_A2042TipoRequisito_Identificador = new String[] {""} ;
         H00QC9_n2042TipoRequisito_Identificador = new bool[] {false} ;
         H00QC9_A1926Requisito_Agrupador = new String[] {""} ;
         H00QC9_n1926Requisito_Agrupador = new bool[] {false} ;
         GridrfRow = new GXWebRow();
         H00QC10_A1919Requisito_Codigo = new int[1] ;
         H00QC10_A2049Requisito_TipoReqCod = new int[1] ;
         H00QC10_n2049Requisito_TipoReqCod = new bool[] {false} ;
         H00QC10_A1999Requisito_ReqCod = new int[1] ;
         H00QC10_n1999Requisito_ReqCod = new bool[] {false} ;
         H00QC10_A2001Requisito_Identificador = new String[] {""} ;
         H00QC10_n2001Requisito_Identificador = new bool[] {false} ;
         H00QC10_A1927Requisito_Titulo = new String[] {""} ;
         H00QC10_n1927Requisito_Titulo = new bool[] {false} ;
         H00QC10_A1926Requisito_Agrupador = new String[] {""} ;
         H00QC10_n1926Requisito_Agrupador = new bool[] {false} ;
         H00QC10_A1923Requisito_Descricao = new String[] {""} ;
         H00QC10_n1923Requisito_Descricao = new bool[] {false} ;
         H00QC10_A1925Requisito_ReferenciaTecnica = new String[] {""} ;
         H00QC10_n1925Requisito_ReferenciaTecnica = new bool[] {false} ;
         H00QC10_A1929Requisito_Restricao = new String[] {""} ;
         H00QC10_n1929Requisito_Restricao = new bool[] {false} ;
         H00QC10_A2002Requisito_Prioridade = new short[1] ;
         H00QC10_n2002Requisito_Prioridade = new bool[] {false} ;
         H00QC10_A1934Requisito_Status = new short[1] ;
         H00QC10_n1934Requisito_Status = new bool[] {false} ;
         H00QC10_A2042TipoRequisito_Identificador = new String[] {""} ;
         H00QC10_n2042TipoRequisito_Identificador = new bool[] {false} ;
         H00QC10_A1931Requisito_Ordem = new short[1] ;
         H00QC10_n1931Requisito_Ordem = new bool[] {false} ;
         GridrnfRow = new GXWebRow();
         sStyleString = "";
         lblTb1_Jsonclick = "";
         lblTb2_Jsonclick = "";
         GridrfColumn = new GXWebColumn();
         lblTbspac2_Jsonclick = "";
         lblTextblockcontagemresultado_demandafm_Jsonclick = "";
         lblTextblockrequisitante_Jsonclick = "";
         TempTags = "";
         lblTextblocksetor_Jsonclick = "";
         lblTextblockservico_Jsonclick = "";
         lblTextblockcontagemrresultado_sistemasigla_Jsonclick = "";
         lblTextblockcontagemresultado_descricao_Jsonclick = "";
         lblTextblockcontagemresultado_observacao_Jsonclick = "";
         lblTextblockcontagemresultado_referencia_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         lblTextblockcontagemresultado_restricoes_Jsonclick = "";
         lblTextblockcontagemresultado_prioridadeprevista_Jsonclick = "";
         lblTextblockcontagemresultado_dataprevista_Jsonclick = "";
         subGridhst_Linesclass = "";
         GridhstColumn = new GXWebColumn();
         lblTbhistorico_Jsonclick = "";
         lblTbresponsavel_Jsonclick = "";
         lblTbsolicitante_Jsonclick = "";
         lblTbenvolvidos_Jsonclick = "";
         lblTbspac1_Jsonclick = "";
         lblTextblockcontagemresultado_sistemanom_Jsonclick = "";
         lblTbsistema_Jsonclick = "";
         lblTbcontratante_Jsonclick = "";
         lblTbsecretaria_Jsonclick = "";
         lblTblinha1_Jsonclick = "";
         lblTbtitle_Jsonclick = "";
         lblTblinha2_Jsonclick = "";
         lblTbdepartamento_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         subGridrnf_Linesclass = "";
         lblTbspac6_Jsonclick = "";
         lblTextblockrequisito_identificador_Jsonclick = "";
         lblTextblockrequisito_tipo_Jsonclick = "";
         lblTextblockrequisito_agrupador_Jsonclick = "";
         lblTextblockrequisito_titulo_Jsonclick = "";
         lblTextblockrequisito_descricao_Jsonclick = "";
         lblTextblockrequisito_prioridade_Jsonclick = "";
         lblTextblockrequisito_status_Jsonclick = "";
         lblTextblockrequisito_referenciatecnica_Jsonclick = "";
         lblTextblockrequisito_restricao_Jsonclick = "";
         subGridrf_Linesclass = "";
         lblTbspac4_Jsonclick = "";
         lblTextblockidentificador_Jsonclick = "";
         lblTextblocktipo_Jsonclick = "";
         lblTextblockagrupador_Jsonclick = "";
         lblTextblocknome_Jsonclick = "";
         lblTextblockdescricao_Jsonclick = "";
         lblTextblockprioridade_Jsonclick = "";
         lblTextblockstatus_Jsonclick = "";
         lblTextblockreferencia_Jsonclick = "";
         lblTextblockrestricao_Jsonclick = "";
         GridrnfColumn = new GXWebColumn();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_requisitos__default(),
            new Object[][] {
                new Object[] {
               H00QC2_A489ContagemResultado_SistemaCod, H00QC2_n489ContagemResultado_SistemaCod, H00QC2_A456ContagemResultado_Codigo, H00QC2_A471ContagemResultado_DataDmn, H00QC2_A495ContagemResultado_SistemaNom, H00QC2_n495ContagemResultado_SistemaNom, H00QC2_A493ContagemResultado_DemandaFM, H00QC2_n493ContagemResultado_DemandaFM, H00QC2_A509ContagemrResultado_SistemaSigla, H00QC2_n509ContagemrResultado_SistemaSigla,
               H00QC2_A494ContagemResultado_Descricao, H00QC2_n494ContagemResultado_Descricao, H00QC2_A514ContagemResultado_Observacao, H00QC2_n514ContagemResultado_Observacao, H00QC2_A1585ContagemResultado_Referencia, H00QC2_n1585ContagemResultado_Referencia, H00QC2_A1586ContagemResultado_Restricoes, H00QC2_n1586ContagemResultado_Restricoes, H00QC2_A1587ContagemResultado_PrioridadePrevista, H00QC2_n1587ContagemResultado_PrioridadePrevista,
               H00QC2_A1351ContagemResultado_DataPrevista, H00QC2_n1351ContagemResultado_DataPrevista
               }
               , new Object[] {
               H00QC3_AGRIDHST_nRecordCount
               }
               , new Object[] {
               H00QC4_A489ContagemResultado_SistemaCod, H00QC4_n489ContagemResultado_SistemaCod, H00QC4_A1553ContagemResultado_CntSrvCod, H00QC4_n1553ContagemResultado_CntSrvCod, H00QC4_A601ContagemResultado_Servico, H00QC4_n601ContagemResultado_Servico, H00QC4_A456ContagemResultado_Codigo, H00QC4_A1636ContagemResultado_ServicoSS, H00QC4_n1636ContagemResultado_ServicoSS, H00QC4_A602ContagemResultado_OSVinculada,
               H00QC4_n602ContagemResultado_OSVinculada, H00QC4_A801ContagemResultado_ServicoSigla, H00QC4_n801ContagemResultado_ServicoSigla, H00QC4_A509ContagemrResultado_SistemaSigla, H00QC4_n509ContagemrResultado_SistemaSigla, H00QC4_A508ContagemResultado_Owner, H00QC4_A890ContagemResultado_Responsavel, H00QC4_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               H00QC5_A29Contratante_Codigo, H00QC5_n29Contratante_Codigo, H00QC5_A5AreaTrabalho_Codigo, H00QC5_A1125Contratante_LogoNomeArq, H00QC5_n1125Contratante_LogoNomeArq, H00QC5_A1126Contratante_LogoTipoArq, H00QC5_n1126Contratante_LogoTipoArq, H00QC5_A10Contratante_NomeFantasia, H00QC5_A1124Contratante_LogoArquivo, H00QC5_n1124Contratante_LogoArquivo
               }
               , new Object[] {
               H00QC6_A1797LogResponsavel_Codigo, H00QC6_A892LogResponsavel_DemandaCod, H00QC6_n892LogResponsavel_DemandaCod, H00QC6_A1131LogResponsavel_Observacao, H00QC6_n1131LogResponsavel_Observacao
               }
               , new Object[] {
               H00QC7_A456ContagemResultado_Codigo, H00QC7_A508ContagemResultado_Owner
               }
               , new Object[] {
               H00QC8_A57Usuario_PessoaCod, H00QC8_A1Usuario_Codigo, H00QC8_A58Usuario_PessoaNom, H00QC8_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00QC9_A2005ContagemResultadoRequisito_Codigo, H00QC9_A2004ContagemResultadoRequisito_ReqCod, H00QC9_A2049Requisito_TipoReqCod, H00QC9_n2049Requisito_TipoReqCod, H00QC9_A2003ContagemResultadoRequisito_OSCod, H00QC9_A2001Requisito_Identificador, H00QC9_n2001Requisito_Identificador, H00QC9_A1923Requisito_Descricao, H00QC9_n1923Requisito_Descricao, H00QC9_A2002Requisito_Prioridade,
               H00QC9_n2002Requisito_Prioridade, H00QC9_A1934Requisito_Status, H00QC9_n1934Requisito_Status, H00QC9_A2042TipoRequisito_Identificador, H00QC9_n2042TipoRequisito_Identificador, H00QC9_A1926Requisito_Agrupador, H00QC9_n1926Requisito_Agrupador
               }
               , new Object[] {
               H00QC10_A1919Requisito_Codigo, H00QC10_A2049Requisito_TipoReqCod, H00QC10_n2049Requisito_TipoReqCod, H00QC10_A1999Requisito_ReqCod, H00QC10_n1999Requisito_ReqCod, H00QC10_A2001Requisito_Identificador, H00QC10_n2001Requisito_Identificador, H00QC10_A1927Requisito_Titulo, H00QC10_n1927Requisito_Titulo, H00QC10_A1926Requisito_Agrupador,
               H00QC10_n1926Requisito_Agrupador, H00QC10_A1923Requisito_Descricao, H00QC10_n1923Requisito_Descricao, H00QC10_A1925Requisito_ReferenciaTecnica, H00QC10_n1925Requisito_ReferenciaTecnica, H00QC10_A1929Requisito_Restricao, H00QC10_n1929Requisito_Restricao, H00QC10_A2002Requisito_Prioridade, H00QC10_n2002Requisito_Prioridade, H00QC10_A1934Requisito_Status,
               H00QC10_A2042TipoRequisito_Identificador, H00QC10_n2042TipoRequisito_Identificador, H00QC10_A1931Requisito_Ordem, H00QC10_n1931Requisito_Ordem
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSolicitante_Enabled = 0;
         edtavResponsavel_Enabled = 0;
         edtavData_Enabled = 0;
         edtavDemanda_Enabled = 0;
         edtavAutor_Enabled = 0;
         edtavDescricaohis_Enabled = 0;
         edtavVersao_Enabled = 0;
         edtavRequisitante_Enabled = 0;
         edtavSetor_Enabled = 0;
         edtavServico_Enabled = 0;
         edtavIdentificador_Enabled = 0;
         edtavTipo_Enabled = 0;
         edtavAgrupador_Enabled = 0;
         edtavNome_Enabled = 0;
         edtavDescricao_Enabled = 0;
         edtavPrioridade_Enabled = 0;
         edtavStatus_Enabled = 0;
         edtavReferencia_Enabled = 0;
         edtavRestricao_Enabled = 0;
         edtavRequisito_identificador_Enabled = 0;
         edtavRequisito_tipo_Enabled = 0;
         edtavRequisito_agrupador_Enabled = 0;
         edtavRequisito_titulo_Enabled = 0;
         edtavRequisito_descricao_Enabled = 0;
         cmbavRequisito_prioridade.Enabled = 0;
         edtavRequisito_status_Enabled = 0;
         edtavRequisito_referenciatecnica_Enabled = 0;
         edtavRequisito_restricao_Enabled = 0;
      }

      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_89 ;
      private short nGXsfl_89_idx=1 ;
      private short nRC_GXsfl_299 ;
      private short nGXsfl_299_idx=1 ;
      private short A1931Requisito_Ordem ;
      private short AV31i ;
      private short A2002Requisito_Prioridade ;
      private short A1934Requisito_Status ;
      private short nRC_GXsfl_212 ;
      private short nGXsfl_212_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRIDHST_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV42Requisito_Prioridade ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_89_Refreshing=0 ;
      private short subGridhst_Backcolorstyle ;
      private short nGXsfl_212_Refreshing=0 ;
      private short subGridrf_Backcolorstyle ;
      private short nGXsfl_299_Refreshing=0 ;
      private short subGridrnf_Backcolorstyle ;
      private short subGridrf_Allowselection ;
      private short subGridrf_Allowhovering ;
      private short subGridrf_Allowcollapsing ;
      private short subGridrf_Collapsed ;
      private short subGridhst_Titlebackstyle ;
      private short subGridhst_Allowselection ;
      private short subGridhst_Allowhovering ;
      private short subGridhst_Allowcollapsing ;
      private short subGridhst_Collapsed ;
      private short subGridhst_Backstyle ;
      private short subGridrnf_Backstyle ;
      private short subGridrf_Backstyle ;
      private short subGridrnf_Allowselection ;
      private short subGridrnf_Allowhovering ;
      private short subGridrnf_Allowcollapsing ;
      private short subGridrnf_Collapsed ;
      private short GRIDRF_nEOF ;
      private short GRIDRNF_nEOF ;
      private int A456ContagemResultado_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int subGridhst_Rows ;
      private int A892LogResponsavel_DemandaCod ;
      private int A1999Requisito_ReqCod ;
      private int AV39Requisito_Codigo ;
      private int edtavRequisito_codigo_Visible ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int edtavSolicitante_Enabled ;
      private int edtavResponsavel_Enabled ;
      private int edtavData_Enabled ;
      private int edtavDemanda_Enabled ;
      private int edtavAutor_Enabled ;
      private int edtavDescricaohis_Enabled ;
      private int edtavVersao_Enabled ;
      private int edtavRequisitante_Enabled ;
      private int edtavSetor_Enabled ;
      private int edtavServico_Enabled ;
      private int edtavIdentificador_Enabled ;
      private int edtavTipo_Enabled ;
      private int edtavAgrupador_Enabled ;
      private int edtavNome_Enabled ;
      private int edtavDescricao_Enabled ;
      private int edtavPrioridade_Enabled ;
      private int edtavStatus_Enabled ;
      private int edtavReferencia_Enabled ;
      private int edtavRestricao_Enabled ;
      private int edtavRequisito_identificador_Enabled ;
      private int edtavRequisito_tipo_Enabled ;
      private int edtavRequisito_agrupador_Enabled ;
      private int edtavRequisito_titulo_Enabled ;
      private int edtavRequisito_descricao_Enabled ;
      private int edtavRequisito_status_Enabled ;
      private int edtavRequisito_referenciatecnica_Enabled ;
      private int edtavRequisito_restricao_Enabled ;
      private int subGridhst_Islastpage ;
      private int subGridrf_Islastpage ;
      private int subGridrnf_Islastpage ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A602ContagemResultado_OSVinculada ;
      private int A508ContagemResultado_Owner ;
      private int A890ContagemResultado_Responsavel ;
      private int AV11OsVinculada ;
      private int AV9Usuario ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2049Requisito_TipoReqCod ;
      private int subGridrf_Selectioncolor ;
      private int subGridrf_Hoveringcolor ;
      private int subGridhst_Titlebackcolor ;
      private int subGridhst_Allbackcolor ;
      private int subGridhst_Selectioncolor ;
      private int subGridhst_Hoveringcolor ;
      private int idxLst ;
      private int subGridhst_Backcolor ;
      private int subGridrnf_Backcolor ;
      private int subGridrnf_Allbackcolor ;
      private int subGridrf_Backcolor ;
      private int subGridrf_Allbackcolor ;
      private int subGridrnf_Selectioncolor ;
      private int subGridrnf_Hoveringcolor ;
      private long GRIDHST_nFirstRecordOnPage ;
      private long GRIDHST_nCurrentRecord ;
      private long GRIDRNF_nCurrentRecord ;
      private long GRIDRF_nCurrentRecord ;
      private long GRIDHST_nRecordCount ;
      private long GRIDRF_nFirstRecordOnPage ;
      private long GRIDRNF_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_89_idx="0001" ;
      private String AV14Solicitante ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sGXsfl_299_idx="0001" ;
      private String edtavNome_Internalname ;
      private String edtavRequisito_codigo_Internalname ;
      private String sGXsfl_212_idx="0001" ;
      private String edtavSolicitante_Internalname ;
      private String edtavResponsavel_Internalname ;
      private String edtavData_Internalname ;
      private String edtavDemanda_Internalname ;
      private String edtavAutor_Internalname ;
      private String edtavDescricaohis_Internalname ;
      private String edtavVersao_Internalname ;
      private String edtavRequisitante_Internalname ;
      private String edtavSetor_Internalname ;
      private String edtavServico_Internalname ;
      private String edtavIdentificador_Internalname ;
      private String edtavTipo_Internalname ;
      private String edtavAgrupador_Internalname ;
      private String edtavDescricao_Internalname ;
      private String edtavPrioridade_Internalname ;
      private String edtavStatus_Internalname ;
      private String edtavReferencia_Internalname ;
      private String edtavRestricao_Internalname ;
      private String edtavRequisito_identificador_Internalname ;
      private String edtavRequisito_tipo_Internalname ;
      private String edtavRequisito_agrupador_Internalname ;
      private String edtavRequisito_titulo_Internalname ;
      private String edtavRequisito_descricao_Internalname ;
      private String cmbavRequisito_prioridade_Internalname ;
      private String edtavRequisito_status_Internalname ;
      private String edtavRequisito_referenciatecnica_Internalname ;
      private String edtavRequisito_restricao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV13Responsavel ;
      private String AV6Requisitante ;
      private String AV7Setor ;
      private String AV8Servico ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV17Autor ;
      private String AV19Versao ;
      private String AV26Prioridade ;
      private String AV27Status ;
      private String AV43Requisito_Status ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String imgavLogomarca_Internalname ;
      private String edtContagemResultado_SistemaNom_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemrResultado_SistemaSigla_Internalname ;
      private String edtContagemResultado_Descricao_Internalname ;
      private String edtContagemResultado_Referencia_Internalname ;
      private String edtContagemResultado_Restricoes_Internalname ;
      private String edtContagemResultado_PrioridadePrevista_Internalname ;
      private String edtContagemResultado_DataPrevista_Internalname ;
      private String hsh ;
      private String A801ContagemResultado_ServicoSigla ;
      private String lblTbtitle_Caption ;
      private String lblTbtitle_Internalname ;
      private String lblTbsistema_Caption ;
      private String lblTbsistema_Internalname ;
      private String A1125Contratante_LogoNomeArq ;
      private String A1124Contratante_LogoArquivo_Filename ;
      private String A1126Contratante_LogoTipoArq ;
      private String A1124Contratante_LogoArquivo_Filetype ;
      private String A10Contratante_NomeFantasia ;
      private String lblTbcontratante_Caption ;
      private String lblTbcontratante_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String lblTextblockidentificador_Caption ;
      private String lblTextblockrequisito_titulo_Caption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblUnnamedtable8_Internalname ;
      private String lblTb1_Internalname ;
      private String lblTb1_Jsonclick ;
      private String lblTb2_Internalname ;
      private String lblTb2_Jsonclick ;
      private String tblUnnamedtable10_Internalname ;
      private String subGridrf_Internalname ;
      private String lblTbspac1_Caption ;
      private String lblTextblocktipo_Caption ;
      private String lblTextblockagrupador_Caption ;
      private String lblTextblocknome_Caption ;
      private String lblTextblockdescricao_Caption ;
      private String lblTextblockprioridade_Caption ;
      private String lblTextblockstatus_Caption ;
      private String lblTextblockreferencia_Caption ;
      private String lblTextblockrestricao_Caption ;
      private String tblUnnamedtable9_Internalname ;
      private String lblTbspac2_Internalname ;
      private String lblTbspac2_Jsonclick ;
      private String lblTextblockcontagemresultado_demandafm_Internalname ;
      private String lblTextblockcontagemresultado_demandafm_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String lblTextblockrequisitante_Internalname ;
      private String lblTextblockrequisitante_Jsonclick ;
      private String TempTags ;
      private String edtavRequisitante_Jsonclick ;
      private String lblTextblocksetor_Internalname ;
      private String lblTextblocksetor_Jsonclick ;
      private String edtavSetor_Jsonclick ;
      private String lblTextblockservico_Internalname ;
      private String lblTextblockservico_Jsonclick ;
      private String edtavServico_Jsonclick ;
      private String lblTextblockcontagemrresultado_sistemasigla_Internalname ;
      private String lblTextblockcontagemrresultado_sistemasigla_Jsonclick ;
      private String edtContagemrResultado_SistemaSigla_Jsonclick ;
      private String lblTextblockcontagemresultado_descricao_Internalname ;
      private String lblTextblockcontagemresultado_descricao_Jsonclick ;
      private String edtContagemResultado_Descricao_Jsonclick ;
      private String lblTextblockcontagemresultado_observacao_Internalname ;
      private String lblTextblockcontagemresultado_observacao_Jsonclick ;
      private String lblTextblockcontagemresultado_referencia_Internalname ;
      private String lblTextblockcontagemresultado_referencia_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblockcontagemresultado_restricoes_Internalname ;
      private String lblTextblockcontagemresultado_restricoes_Jsonclick ;
      private String lblTextblockcontagemresultado_prioridadeprevista_Internalname ;
      private String lblTextblockcontagemresultado_prioridadeprevista_Jsonclick ;
      private String edtContagemResultado_PrioridadePrevista_Jsonclick ;
      private String lblTextblockcontagemresultado_dataprevista_Internalname ;
      private String lblTextblockcontagemresultado_dataprevista_Jsonclick ;
      private String edtContagemResultado_DataPrevista_Jsonclick ;
      private String tblUnnamedtable7_Internalname ;
      private String tblUnnamedtable11_Internalname ;
      private String subGridhst_Internalname ;
      private String subGridhst_Class ;
      private String subGridhst_Linesclass ;
      private String tblUnnamedtable6_Internalname ;
      private String lblTbhistorico_Internalname ;
      private String lblTbhistorico_Jsonclick ;
      private String tblUnnamedtable5_Internalname ;
      private String tblUnnamedtable12_Internalname ;
      private String tblUnnamedtable14_Internalname ;
      private String lblTbresponsavel_Internalname ;
      private String lblTbresponsavel_Jsonclick ;
      private String edtavResponsavel_Jsonclick ;
      private String tblUnnamedtable13_Internalname ;
      private String lblTbsolicitante_Internalname ;
      private String lblTbsolicitante_Jsonclick ;
      private String edtavSolicitante_Jsonclick ;
      private String tblUnnamedtable4_Internalname ;
      private String lblTbenvolvidos_Internalname ;
      private String lblTbenvolvidos_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTbspac1_Internalname ;
      private String lblTbspac1_Jsonclick ;
      private String lblTextblockcontagemresultado_sistemanom_Internalname ;
      private String lblTextblockcontagemresultado_sistemanom_Jsonclick ;
      private String edtContagemResultado_SistemaNom_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTbsistema_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable15_Internalname ;
      private String lblTbcontratante_Jsonclick ;
      private String lblTbsecretaria_Internalname ;
      private String lblTbsecretaria_Jsonclick ;
      private String lblTblinha1_Internalname ;
      private String lblTblinha1_Jsonclick ;
      private String lblTbtitle_Jsonclick ;
      private String lblTblinha2_Internalname ;
      private String lblTblinha2_Jsonclick ;
      private String lblTbdepartamento_Internalname ;
      private String lblTbdepartamento_Jsonclick ;
      private String sGXsfl_89_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavData_Jsonclick ;
      private String edtavDemanda_Jsonclick ;
      private String edtavAutor_Jsonclick ;
      private String edtavDescricaohis_Jsonclick ;
      private String edtavVersao_Jsonclick ;
      private String lblTbspac6_Internalname ;
      private String lblTextblockrequisito_identificador_Internalname ;
      private String lblTextblockrequisito_tipo_Internalname ;
      private String lblTextblockrequisito_agrupador_Internalname ;
      private String lblTextblockrequisito_titulo_Internalname ;
      private String lblTextblockrequisito_descricao_Internalname ;
      private String lblTextblockrequisito_prioridade_Internalname ;
      private String lblTextblockrequisito_status_Internalname ;
      private String lblTextblockrequisito_referenciatecnica_Internalname ;
      private String lblTextblockrequisito_restricao_Internalname ;
      private String sGXsfl_299_fel_idx="0001" ;
      private String subGridrnf_Class ;
      private String subGridrnf_Linesclass ;
      private String lblTbspac6_Jsonclick ;
      private String lblTextblockrequisito_identificador_Jsonclick ;
      private String tblTablemergedrequisito_identificador_Internalname ;
      private String edtavRequisito_identificador_Jsonclick ;
      private String lblTextblockrequisito_tipo_Jsonclick ;
      private String edtavRequisito_tipo_Jsonclick ;
      private String lblTextblockrequisito_agrupador_Jsonclick ;
      private String edtavRequisito_agrupador_Jsonclick ;
      private String lblTextblockrequisito_titulo_Jsonclick ;
      private String lblTextblockrequisito_descricao_Jsonclick ;
      private String lblTextblockrequisito_prioridade_Jsonclick ;
      private String cmbavRequisito_prioridade_Jsonclick ;
      private String lblTextblockrequisito_status_Jsonclick ;
      private String edtavRequisito_status_Jsonclick ;
      private String lblTextblockrequisito_referenciatecnica_Jsonclick ;
      private String lblTextblockrequisito_restricao_Jsonclick ;
      private String lblTbspac4_Internalname ;
      private String lblTextblockidentificador_Internalname ;
      private String lblTextblocktipo_Internalname ;
      private String lblTextblockagrupador_Internalname ;
      private String lblTextblocknome_Internalname ;
      private String lblTextblockdescricao_Internalname ;
      private String lblTextblockprioridade_Internalname ;
      private String lblTextblockstatus_Internalname ;
      private String lblTextblockreferencia_Internalname ;
      private String lblTextblockrestricao_Internalname ;
      private String subGridrnf_Internalname ;
      private String sGXsfl_212_fel_idx="0001" ;
      private String subGridrf_Class ;
      private String subGridrf_Linesclass ;
      private String lblTbspac4_Jsonclick ;
      private String edtavRequisito_codigo_Jsonclick ;
      private String lblTextblockidentificador_Jsonclick ;
      private String tblTablemergedidentificador_Internalname ;
      private String edtavIdentificador_Jsonclick ;
      private String lblTextblocktipo_Jsonclick ;
      private String edtavTipo_Jsonclick ;
      private String lblTextblockagrupador_Jsonclick ;
      private String edtavAgrupador_Jsonclick ;
      private String lblTextblocknome_Jsonclick ;
      private String lblTextblockdescricao_Jsonclick ;
      private String lblTextblockprioridade_Jsonclick ;
      private String edtavPrioridade_Jsonclick ;
      private String lblTextblockstatus_Jsonclick ;
      private String edtavStatus_Jsonclick ;
      private String lblTextblockreferencia_Jsonclick ;
      private String lblTextblockrestricao_Jsonclick ;
      private String lblTextblockrequisito_referenciatecnica_Caption ;
      private String Contagemresultado_observacao_Internalname ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV15Data ;
      private bool entryPointCalled ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1131LogResponsavel_Observacao ;
      private bool n1931Requisito_Ordem ;
      private bool n1999Requisito_ReqCod ;
      private bool n2001Requisito_Identificador ;
      private bool n1927Requisito_Titulo ;
      private bool n1926Requisito_Agrupador ;
      private bool n1923Requisito_Descricao ;
      private bool n1925Requisito_ReferenciaTecnica ;
      private bool n1929Requisito_Restricao ;
      private bool n2002Requisito_Prioridade ;
      private bool n1934Requisito_Status ;
      private bool n2042TipoRequisito_Identificador ;
      private bool toggleJsOutput ;
      private bool Contagemresultado_observacao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n495ContagemResultado_SistemaNom ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n494ContagemResultado_Descricao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n1585ContagemResultado_Referencia ;
      private bool n1586ContagemResultado_Restricoes ;
      private bool n1587ContagemResultado_PrioridadePrevista ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool returnInSub ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n29Contratante_Codigo ;
      private bool n1125Contratante_LogoNomeArq ;
      private bool n1126Contratante_LogoTipoArq ;
      private bool n1124Contratante_LogoArquivo ;
      private bool n58Usuario_PessoaNom ;
      private bool n2049Requisito_TipoReqCod ;
      private bool AV5LogoMarca_IsBlob ;
      private String A1131LogResponsavel_Observacao ;
      private String A1923Requisito_Descricao ;
      private String A1925Requisito_ReferenciaTecnica ;
      private String A1929Requisito_Restricao ;
      private String A514ContagemResultado_Observacao ;
      private String AV28DescricaoHis ;
      private String AV18Descricao ;
      private String AV29Referencia ;
      private String AV30Restricao ;
      private String AV34Requisito_Descricao ;
      private String AV35Requisito_ReferenciaTecnica ;
      private String AV36Requisito_Restricao ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV10Nome ;
      private String A2001Requisito_Identificador ;
      private String A1927Requisito_Titulo ;
      private String A1926Requisito_Agrupador ;
      private String A2042TipoRequisito_Identificador ;
      private String A494ContagemResultado_Descricao ;
      private String A1585ContagemResultado_Referencia ;
      private String A1586ContagemResultado_Restricoes ;
      private String A1587ContagemResultado_PrioridadePrevista ;
      private String AV20Demanda ;
      private String AV25Identificador ;
      private String AV38Tipo ;
      private String AV24Agrupador ;
      private String AV40Requisito_Identificador ;
      private String AV41Requisito_Tipo ;
      private String AV33Requisito_Agrupador ;
      private String AV32Requisito_Titulo ;
      private String A495ContagemResultado_SistemaNom ;
      private String AV48Logomarca_GXI ;
      private String AV5LogoMarca ;
      private String A1124Contratante_LogoArquivo ;
      private GXWebGrid GridhstContainer ;
      private GXWebGrid GridrfContainer ;
      private GXWebGrid GridrnfContainer ;
      private GXWebRow GridhstRow ;
      private GXWebRow GridrfRow ;
      private GXWebRow GridrnfRow ;
      private GXWebColumn GridrfColumn ;
      private GXWebColumn GridhstColumn ;
      private GXWebColumn GridrnfColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private GXCombobox cmbavRequisito_prioridade ;
      private IDataStoreProvider pr_default ;
      private int[] H00QC2_A489ContagemResultado_SistemaCod ;
      private bool[] H00QC2_n489ContagemResultado_SistemaCod ;
      private int[] H00QC2_A456ContagemResultado_Codigo ;
      private DateTime[] H00QC2_A471ContagemResultado_DataDmn ;
      private String[] H00QC2_A495ContagemResultado_SistemaNom ;
      private bool[] H00QC2_n495ContagemResultado_SistemaNom ;
      private String[] H00QC2_A493ContagemResultado_DemandaFM ;
      private bool[] H00QC2_n493ContagemResultado_DemandaFM ;
      private String[] H00QC2_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00QC2_n509ContagemrResultado_SistemaSigla ;
      private String[] H00QC2_A494ContagemResultado_Descricao ;
      private bool[] H00QC2_n494ContagemResultado_Descricao ;
      private String[] H00QC2_A514ContagemResultado_Observacao ;
      private bool[] H00QC2_n514ContagemResultado_Observacao ;
      private String[] H00QC2_A1585ContagemResultado_Referencia ;
      private bool[] H00QC2_n1585ContagemResultado_Referencia ;
      private String[] H00QC2_A1586ContagemResultado_Restricoes ;
      private bool[] H00QC2_n1586ContagemResultado_Restricoes ;
      private String[] H00QC2_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] H00QC2_n1587ContagemResultado_PrioridadePrevista ;
      private DateTime[] H00QC2_A1351ContagemResultado_DataPrevista ;
      private bool[] H00QC2_n1351ContagemResultado_DataPrevista ;
      private long[] H00QC3_AGRIDHST_nRecordCount ;
      private int[] H00QC4_A489ContagemResultado_SistemaCod ;
      private bool[] H00QC4_n489ContagemResultado_SistemaCod ;
      private int[] H00QC4_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00QC4_n1553ContagemResultado_CntSrvCod ;
      private int[] H00QC4_A601ContagemResultado_Servico ;
      private bool[] H00QC4_n601ContagemResultado_Servico ;
      private int[] H00QC4_A456ContagemResultado_Codigo ;
      private int[] H00QC4_A1636ContagemResultado_ServicoSS ;
      private bool[] H00QC4_n1636ContagemResultado_ServicoSS ;
      private int[] H00QC4_A602ContagemResultado_OSVinculada ;
      private bool[] H00QC4_n602ContagemResultado_OSVinculada ;
      private String[] H00QC4_A801ContagemResultado_ServicoSigla ;
      private bool[] H00QC4_n801ContagemResultado_ServicoSigla ;
      private String[] H00QC4_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00QC4_n509ContagemrResultado_SistemaSigla ;
      private int[] H00QC4_A508ContagemResultado_Owner ;
      private int[] H00QC4_A890ContagemResultado_Responsavel ;
      private bool[] H00QC4_n890ContagemResultado_Responsavel ;
      private int[] H00QC5_A29Contratante_Codigo ;
      private bool[] H00QC5_n29Contratante_Codigo ;
      private int[] H00QC5_A5AreaTrabalho_Codigo ;
      private String[] H00QC5_A1125Contratante_LogoNomeArq ;
      private bool[] H00QC5_n1125Contratante_LogoNomeArq ;
      private String[] H00QC5_A1126Contratante_LogoTipoArq ;
      private bool[] H00QC5_n1126Contratante_LogoTipoArq ;
      private String[] H00QC5_A10Contratante_NomeFantasia ;
      private String[] H00QC5_A1124Contratante_LogoArquivo ;
      private bool[] H00QC5_n1124Contratante_LogoArquivo ;
      private long[] H00QC6_A1797LogResponsavel_Codigo ;
      private int[] H00QC6_A892LogResponsavel_DemandaCod ;
      private bool[] H00QC6_n892LogResponsavel_DemandaCod ;
      private String[] H00QC6_A1131LogResponsavel_Observacao ;
      private bool[] H00QC6_n1131LogResponsavel_Observacao ;
      private int[] H00QC7_A456ContagemResultado_Codigo ;
      private int[] H00QC7_A508ContagemResultado_Owner ;
      private int[] H00QC8_A57Usuario_PessoaCod ;
      private int[] H00QC8_A1Usuario_Codigo ;
      private String[] H00QC8_A58Usuario_PessoaNom ;
      private bool[] H00QC8_n58Usuario_PessoaNom ;
      private int[] H00QC9_A2005ContagemResultadoRequisito_Codigo ;
      private int[] H00QC9_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] H00QC9_A2049Requisito_TipoReqCod ;
      private bool[] H00QC9_n2049Requisito_TipoReqCod ;
      private int[] H00QC9_A2003ContagemResultadoRequisito_OSCod ;
      private String[] H00QC9_A2001Requisito_Identificador ;
      private bool[] H00QC9_n2001Requisito_Identificador ;
      private String[] H00QC9_A1923Requisito_Descricao ;
      private bool[] H00QC9_n1923Requisito_Descricao ;
      private short[] H00QC9_A2002Requisito_Prioridade ;
      private bool[] H00QC9_n2002Requisito_Prioridade ;
      private short[] H00QC9_A1934Requisito_Status ;
      private bool[] H00QC9_n1934Requisito_Status ;
      private String[] H00QC9_A2042TipoRequisito_Identificador ;
      private bool[] H00QC9_n2042TipoRequisito_Identificador ;
      private String[] H00QC9_A1926Requisito_Agrupador ;
      private bool[] H00QC9_n1926Requisito_Agrupador ;
      private int[] H00QC10_A1919Requisito_Codigo ;
      private int[] H00QC10_A2049Requisito_TipoReqCod ;
      private bool[] H00QC10_n2049Requisito_TipoReqCod ;
      private int[] H00QC10_A1999Requisito_ReqCod ;
      private bool[] H00QC10_n1999Requisito_ReqCod ;
      private String[] H00QC10_A2001Requisito_Identificador ;
      private bool[] H00QC10_n2001Requisito_Identificador ;
      private String[] H00QC10_A1927Requisito_Titulo ;
      private bool[] H00QC10_n1927Requisito_Titulo ;
      private String[] H00QC10_A1926Requisito_Agrupador ;
      private bool[] H00QC10_n1926Requisito_Agrupador ;
      private String[] H00QC10_A1923Requisito_Descricao ;
      private bool[] H00QC10_n1923Requisito_Descricao ;
      private String[] H00QC10_A1925Requisito_ReferenciaTecnica ;
      private bool[] H00QC10_n1925Requisito_ReferenciaTecnica ;
      private String[] H00QC10_A1929Requisito_Restricao ;
      private bool[] H00QC10_n1929Requisito_Restricao ;
      private short[] H00QC10_A2002Requisito_Prioridade ;
      private bool[] H00QC10_n2002Requisito_Prioridade ;
      private short[] H00QC10_A1934Requisito_Status ;
      private bool[] H00QC10_n1934Requisito_Status ;
      private String[] H00QC10_A2042TipoRequisito_Identificador ;
      private bool[] H00QC10_n2042TipoRequisito_Identificador ;
      private short[] H00QC10_A1931Requisito_Ordem ;
      private bool[] H00QC10_n1931Requisito_Ordem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV23NaoFuncionais ;
      private wwpbaseobjects.SdtWWPContext AV37WWPContext ;
   }

   public class wp_requisitos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QC2 ;
          prmH00QC2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC3 ;
          prmH00QC3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC4 ;
          prmH00QC4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC5 ;
          prmH00QC5 = new Object[] {
          new Object[] {"@AV37WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC6 ;
          prmH00QC6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC7 ;
          prmH00QC7 = new Object[] {
          new Object[] {"@AV11OsVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC8 ;
          prmH00QC8 = new Object[] {
          new Object[] {"@AV9Usuario",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC9 ;
          prmH00QC9 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00QC10 ;
          prmH00QC10 = new Object[] {
          new Object[] {"@AV39Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QC2", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataDmn], T2.[Sistema_Nome] AS ContagemResultado_SistemaNom, T1.[ContagemResultado_DemandaFM], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Observacao], T1.[ContagemResultado_Referencia], T1.[ContagemResultado_Restricoes], T1.[ContagemResultado_PrioridadePrevista], T1.[ContagemResultado_DataPrevista] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC2,1,0,true,true )
             ,new CursorDef("H00QC3", "SELECT COUNT(*) FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC3,1,0,true,true )
             ,new CursorDef("H00QC4", "SELECT TOP 1 T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_OSVinculada], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Responsavel] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC4,1,0,true,true )
             ,new CursorDef("H00QC5", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_LogoNomeArq], T2.[Contratante_LogoTipoArq], T2.[Contratante_NomeFantasia], T2.[Contratante_LogoArquivo] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV37WWPC_1Areatrabalho_codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC5,1,0,false,true )
             ,new CursorDef("H00QC6", "SELECT TOP 1 [LogResponsavel_Codigo], [LogResponsavel_DemandaCod], [LogResponsavel_Observacao] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ORDER BY [LogResponsavel_DemandaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC6,1,0,false,true )
             ,new CursorDef("H00QC7", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Owner] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV11OsVinculada ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC7,1,0,false,true )
             ,new CursorDef("H00QC8", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV9Usuario ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC8,1,0,false,true )
             ,new CursorDef("H00QC9", "SELECT T1.[ContagemResultadoRequisito_Codigo], T1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod, T2.[Requisito_TipoReqCod] AS Requisito_TipoReqCod, T1.[ContagemResultadoRequisito_OSCod], T2.[Requisito_Identificador], T2.[Requisito_Descricao], T2.[Requisito_Prioridade], T2.[Requisito_Status], T3.[TipoRequisito_Identificador], T2.[Requisito_Agrupador] FROM (([ContagemResultadoRequisito] T1 WITH (NOLOCK) INNER JOIN [Requisito] T2 WITH (NOLOCK) ON T2.[Requisito_Codigo] = T1.[ContagemResultadoRequisito_ReqCod]) LEFT JOIN [TipoRequisito] T3 WITH (NOLOCK) ON T3.[TipoRequisito_Codigo] = T2.[Requisito_TipoReqCod]) WHERE T1.[ContagemResultadoRequisito_OSCod] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoRequisito_OSCod], T2.[Requisito_Agrupador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC9,100,0,true,false )
             ,new CursorDef("H00QC10", "SELECT T1.[Requisito_Codigo], T1.[Requisito_TipoReqCod] AS Requisito_TipoReqCod, T1.[Requisito_ReqCod], T1.[Requisito_Identificador], T1.[Requisito_Titulo], T1.[Requisito_Agrupador], T1.[Requisito_Descricao], T1.[Requisito_ReferenciaTecnica], T1.[Requisito_Restricao], T1.[Requisito_Prioridade], T1.[Requisito_Status], T2.[TipoRequisito_Identificador], T1.[Requisito_Ordem] FROM ([Requisito] T1 WITH (NOLOCK) LEFT JOIN [TipoRequisito] T2 WITH (NOLOCK) ON T2.[TipoRequisito_Codigo] = T1.[Requisito_TipoReqCod]) WHERE T1.[Requisito_ReqCod] = @AV39Requisito_Codigo ORDER BY T1.[Requisito_Ordem] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QC10,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 25) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 25) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((String[]) buf[8])[0] = rslt.getBLOBFile(6, rslt.getString(4, 10), rslt.getString(3, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((short[]) buf[19])[0] = rslt.getShort(11) ;
                ((String[]) buf[20])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
