/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:20:30.99
*/
gx.evt.autoSkip = false;
gx.define('contagemresultadocontagens', false, function () {
   this.ServerClass =  "contagemresultadocontagens" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.A457ContagemResultado_Demanda=gx.fn.getControlValue("CONTAGEMRESULTADO_DEMANDA") ;
      this.A493ContagemResultado_DemandaFM=gx.fn.getControlValue("CONTAGEMRESULTADO_DEMANDAFM") ;
      this.AV7ContagemResultado_Codigo=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CODIGO",'.') ;
      this.AV8ContagemResultado_DataCnt=gx.fn.getDateValue("vCONTAGEMRESULTADO_DATACNT") ;
      this.AV18ContagemResultado_HoraCnt=gx.fn.getControlValue("vCONTAGEMRESULTADO_HORACNT") ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.AV10Insert_ContagemResultado_ContadorFMCod=gx.fn.getIntegerValue("vINSERT_CONTAGEMRESULTADO_CONTADORFMCOD",'.') ;
      this.AV11Insert_ContagemResultado_NaoCnfCntCod=gx.fn.getIntegerValue("vINSERT_CONTAGEMRESULTADO_NAOCNFCNTCOD",'.') ;
      this.A462ContagemResultado_Divergencia=gx.fn.getDecimalValue("CONTAGEMRESULTADO_DIVERGENCIA",'.',',') ;
      this.AV20CalculoDivergencia=gx.fn.getControlValue("vCALCULODIVERGENCIA") ;
      this.AV19IndiceDivergencia=gx.fn.getDecimalValue("vINDICEDIVERGENCIA",'.',',') ;
      this.A1553ContagemResultado_CntSrvCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CNTSRVCOD",'.') ;
      this.A833ContagemResultado_CstUntPrd=gx.fn.getDecimalValue("CONTAGEMRESULTADO_CSTUNTPRD",'.',',') ;
      this.AV35CalculoPFinal=gx.fn.getControlValue("vCALCULOPFINAL") ;
      this.A517ContagemResultado_Ultima=gx.fn.getControlValue("CONTAGEMRESULTADO_ULTIMA") ;
      this.A490ContagemResultado_ContratadaCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CONTRATADACOD",'.') ;
      this.A481ContagemResultado_TimeCnt=gx.fn.getDateTimeValue("CONTAGEMRESULTADO_TIMECNT") ;
      this.A901ContagemResultadoContagens_Prazo=gx.fn.getDateTimeValue("CONTAGEMRESULTADOCONTAGENS_PRAZO") ;
      this.A1756ContagemResultado_NvlCnt=gx.fn.getIntegerValue("CONTAGEMRESULTADO_NVLCNT",'.') ;
      this.A146Modulo_Codigo=gx.fn.getIntegerValue("MODULO_CODIGO",'.') ;
      this.A485ContagemResultado_EhValidacao=gx.fn.getControlValue("CONTAGEMRESULTADO_EHVALIDACAO") ;
      this.A484ContagemResultado_StatusDmn=gx.fn.getControlValue("CONTAGEMRESULTADO_STATUSDMN") ;
      this.A1389ContagemResultado_RdmnIssueId=gx.fn.getIntegerValue("CONTAGEMRESULTADO_RDMNISSUEID",'.') ;
      this.A890ContagemResultado_Responsavel=gx.fn.getIntegerValue("CONTAGEMRESULTADO_RESPONSAVEL",'.') ;
      this.A468ContagemResultado_NaoCnfDmnCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_NAOCNFDMNCOD",'.') ;
      this.A805ContagemResultado_ContratadaOrigemCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CONTRATADAORIGEMCOD",'.') ;
      this.A602ContagemResultado_OSVinculada=gx.fn.getIntegerValue("CONTAGEMRESULTADO_OSVINCULADA",'.') ;
      this.A999ContagemResultado_CrFMEhContratada=gx.fn.getControlValue("CONTAGEMRESULTADO_CRFMEHCONTRATADA") ;
      this.A1000ContagemResultado_CrFMEhContratante=gx.fn.getControlValue("CONTAGEMRESULTADO_CRFMEHCONTRATANTE") ;
      this.A479ContagemResultado_CrFMPessoaCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CRFMPESSOACOD",'.') ;
      this.A478ContagemResultado_NaoCnfCntNom=gx.fn.getControlValue("CONTAGEMRESULTADO_NAOCNFCNTNOM") ;
      this.A127Sistema_Codigo=gx.fn.getIntegerValue("SISTEMA_CODIGO",'.') ;
      this.A513Sistema_Coordenacao=gx.fn.getControlValue("SISTEMA_COORDENACAO") ;
      this.A52Contratada_AreaTrabalhoCod=gx.fn.getIntegerValue("CONTRATADA_AREATRABALHOCOD",'.') ;
      this.A601ContagemResultado_Servico=gx.fn.getIntegerValue("CONTAGEMRESULTADO_SERVICO",'.') ;
      this.A474ContagemResultado_ContadorFMNom=gx.fn.getControlValue("CONTAGEMRESULTADO_CONTADORFMNOM") ;
      this.AV37Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV34Confirmado=gx.fn.getControlValue("vCONFIRMADO") ;
      this.AV15WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV12TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
   };
   this.Valid_Contagemresultado_datacnt=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_DATACNT");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.A473ContagemResultado_DataCnt)==0) || new gx.date.gxdate( this.A473ContagemResultado_DataCnt ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Data fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultado_horacnt=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_HORACNT");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultado_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Contagemresultado_codigo",["gx.O.A456ContagemResultado_Codigo", "gx.O.A146Modulo_Codigo", "gx.O.A127Sistema_Codigo", "gx.O.A457ContagemResultado_Demanda", "gx.O.A493ContagemResultado_DemandaFM", "gx.O.A490ContagemResultado_ContratadaCod", "gx.O.A1553ContagemResultado_CntSrvCod", "gx.O.A485ContagemResultado_EhValidacao", "gx.O.A484ContagemResultado_StatusDmn", "gx.O.A1389ContagemResultado_RdmnIssueId", "gx.O.A890ContagemResultado_Responsavel", "gx.O.A468ContagemResultado_NaoCnfDmnCod", "gx.O.A805ContagemResultado_ContratadaOrigemCod", "gx.O.A602ContagemResultado_OSVinculada", "gx.O.A513Sistema_Coordenacao", "gx.O.A501ContagemResultado_OsFsOsFm", "gx.O.A52Contratada_AreaTrabalhoCod", "gx.O.A470ContagemResultado_ContadorFMCod", "gx.O.A601ContagemResultado_Servico"],["A146Modulo_Codigo", "A485ContagemResultado_EhValidacao", "A457ContagemResultado_Demanda", "A484ContagemResultado_StatusDmn", "A1389ContagemResultado_RdmnIssueId", "A493ContagemResultado_DemandaFM", "A890ContagemResultado_Responsavel", "A468ContagemResultado_NaoCnfDmnCod", "A490ContagemResultado_ContratadaCod", "A805ContagemResultado_ContratadaOrigemCod", "A1553ContagemResultado_CntSrvCod", "A602ContagemResultado_OSVinculada", "A127Sistema_Codigo", "A513Sistema_Coordenacao", "A501ContagemResultado_OsFsOsFm", "A52Contratada_AreaTrabalhoCod", "A470ContagemResultado_ContadorFMCod", "A601ContagemResultado_Servico"]);
      return true;
   }
   this.Valid_Contagemresultado_contadorfmcod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Contagemresultado_contadorfmcod",["gx.O.A470ContagemResultado_ContadorFMCod", "gx.O.A479ContagemResultado_CrFMPessoaCod", "gx.O.A1553ContagemResultado_CntSrvCod", "gx.num.urlDecimal(gx.O.A833ContagemResultado_CstUntPrd,\'.\',\',\')", "gx.O.A999ContagemResultado_CrFMEhContratada", "gx.O.A1000ContagemResultado_CrFMEhContratante", "gx.O.A474ContagemResultado_ContadorFMNom", "gx.O.AV35CalculoPFinal"],["A999ContagemResultado_CrFMEhContratada", "A1000ContagemResultado_CrFMEhContratante", "A479ContagemResultado_CrFMPessoaCod", "A474ContagemResultado_ContadorFMNom", "AV35CalculoPFinal"]);
      return true;
   }
   this.Valid_Contagemresultado_pfbfs=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_PFBFS");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultado_pflfs=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_PFLFS");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultado_pfbfm=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_PFBFM");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultado_pflfm=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_PFLFM");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultado_naocnfcntcod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Contagemresultado_naocnfcntcod",["gx.O.A469ContagemResultado_NaoCnfCntCod", "gx.O.A478ContagemResultado_NaoCnfCntNom"],["A478ContagemResultado_NaoCnfCntNom"]);
      return true;
   }
   this.Valid_Contagemresultado_statuscnt=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_STATUSCNT");
         this.AnyError  = 0;
         if ( ! ( ( this.A483ContagemResultado_StatusCnt == 1 ) || ( this.A483ContagemResultado_StatusCnt == 2 ) || ( this.A483ContagemResultado_StatusCnt == 3 ) || ( this.A483ContagemResultado_StatusCnt == 4 ) || ( this.A483ContagemResultado_StatusCnt == 5 ) || ( this.A483ContagemResultado_StatusCnt == 6 ) || ( this.A483ContagemResultado_StatusCnt == 7 ) ) )
         {
            try {
               gxballoon.setError("Campo Status fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }
         try {
            gx.fn.setCtrlProperty("CONTAGEMRESULTADO_PFBFM","Enabled", ((this.A483ContagemResultado_StatusCnt!=7)) );
         }
         catch(e){}
         try {
            gx.fn.setCtrlProperty("CONTAGEMRESULTADO_PFBFS","Enabled", ((this.A483ContagemResultado_StatusCnt!=7)) );
         }
         catch(e){}
         try {
            gx.fn.setCtrlProperty("CONTAGEMRESULTADO_PFLFM","Enabled", ((this.A483ContagemResultado_StatusCnt!=7)) );
         }
         catch(e){}
         try {
            gx.fn.setCtrlProperty("CONTAGEMRESULTADO_PFLFS","Enabled", ((this.A483ContagemResultado_StatusCnt!=7)) );
         }
         catch(e){}

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Contagemresultado_divergencia=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vCONTAGEMRESULTADO_DIVERGENCIA");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultadocontagens_esforco=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADOCONTAGENS_ESFORCO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e121v2_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e131v2_client=function()
   {
      this.executeServerEvent("CONTAGEMRESULTADO_PFBFS.ISVALID", true, null, false, true);
   };
   this.e141v2_client=function()
   {
      this.executeServerEvent("CONTAGEMRESULTADO_PFLFS.ISVALID", true, null, false, true);
   };
   this.e151v2_client=function()
   {
      this.executeServerEvent("CONTAGEMRESULTADO_PFBFM.ISVALID", true, null, false, true);
   };
   this.e161v2_client=function()
   {
      this.executeServerEvent("CONTAGEMRESULTADO_PFLFM.ISVALID", true, null, false, true);
   };
   this.e171v72_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e181v72_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,13,16,18,23,25,28,30,32,34,37,39,41,43,46,48,50,52,55,57,62,64,67,69,71,73,76,78,81,83,85,87,89,91,93,96,98,101,103,106,114];
   this.GXLastCtrlId =114;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 11, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Resultado da Contagem", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[13]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[16]={fld:"TEXTBLOCKCONTAGEMRESULTADO_OSFSOSFM", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"svchar",len:30,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_OSFSOSFM",gxz:"Z501ContagemResultado_OsFsOsFm",gxold:"O501ContagemResultado_OsFsOsFm",gxvar:"A501ContagemResultado_OsFsOsFm",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A501ContagemResultado_OsFsOsFm=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z501ContagemResultado_OsFsOsFm=Value},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADO_OSFSOSFM",gx.O.A501ContagemResultado_OsFsOsFm,0)},c2v:function(){if(this.val()!==undefined)gx.O.A501ContagemResultado_OsFsOsFm=this.val()},val:function(){return gx.fn.getControlValue("CONTAGEMRESULTADO_OSFSOSFM")},nac:gx.falseFn};
   GXValidFnc[23]={fld:"TEXTBLOCKCONTAGEMRESULTADO_DATACNT", format:0,grid:0};
   GXValidFnc[25]={fld:"TABLEMERGEDCONTAGEMRESULTADO_DATACNT",grid:0};
   GXValidFnc[28]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_datacnt,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DATACNT",gxz:"Z473ContagemResultado_DataCnt",gxold:"O473ContagemResultado_DataCnt",gxvar:"A473ContagemResultado_DataCnt",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[28],ip:[28],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A473ContagemResultado_DataCnt=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z473ContagemResultado_DataCnt=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADO_DATACNT",gx.O.A473ContagemResultado_DataCnt,0)},c2v:function(){if(this.val()!==undefined)gx.O.A473ContagemResultado_DataCnt=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("CONTAGEMRESULTADO_DATACNT")},nac:function(){return !(new gx.date.gxdate('').compare(this.AV8ContagemResultado_DataCnt)==0)}};
   GXValidFnc[30]={lvl:0,type:"char",len:5,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_horacnt,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_HORACNT",gxz:"Z511ContagemResultado_HoraCnt",gxold:"O511ContagemResultado_HoraCnt",gxvar:"A511ContagemResultado_HoraCnt",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A511ContagemResultado_HoraCnt=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z511ContagemResultado_HoraCnt=Value},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADO_HORACNT",gx.O.A511ContagemResultado_HoraCnt,0)},c2v:function(){if(this.val()!==undefined)gx.O.A511ContagemResultado_HoraCnt=this.val()},val:function(){return gx.fn.getControlValue("CONTAGEMRESULTADO_HORACNT")},nac:function(){return !((''==this.AV18ContagemResultado_HoraCnt))}};
   GXValidFnc[32]={fld:"TEXTBLOCKCONTAGEMRESULTADO_CONTADORFMCOD", format:0,grid:0};
   GXValidFnc[34]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_contadorfmcod,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_CONTADORFMCOD",gxz:"Z470ContagemResultado_ContadorFMCod",gxold:"O470ContagemResultado_ContadorFMCod",gxvar:"A470ContagemResultado_ContadorFMCod",ucs:[],op:[],ip:[34],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A470ContagemResultado_ContadorFMCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z470ContagemResultado_ContadorFMCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTAGEMRESULTADO_CONTADORFMCOD",gx.O.A470ContagemResultado_ContadorFMCod);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A470ContagemResultado_ContadorFMCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADO_CONTADORFMCOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 34 , function() {
   });
   GXValidFnc[37]={fld:"TEXTBLOCKCONTAGEMRESULTADO_PFBFS", format:0,grid:0};
   GXValidFnc[39]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_pfbfs,isvalid:'e131v2_client',rgrid:[],fld:"CONTAGEMRESULTADO_PFBFS",gxz:"Z458ContagemResultado_PFBFS",gxold:"O458ContagemResultado_PFBFS",gxvar:"A458ContagemResultado_PFBFS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A458ContagemResultado_PFBFS=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z458ContagemResultado_PFBFS=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("CONTAGEMRESULTADO_PFBFS",gx.O.A458ContagemResultado_PFBFS,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A458ContagemResultado_PFBFS=this.val()},val:function(){return gx.fn.getDecimalValue("CONTAGEMRESULTADO_PFBFS",'.',',')},nac:gx.falseFn};
   this.declareDomainHdlr( 39 , function() {
   });
   GXValidFnc[41]={fld:"TEXTBLOCKCONTAGEMRESULTADO_PFLFS", format:0,grid:0};
   GXValidFnc[43]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_pflfs,isvalid:'e141v2_client',rgrid:[],fld:"CONTAGEMRESULTADO_PFLFS",gxz:"Z459ContagemResultado_PFLFS",gxold:"O459ContagemResultado_PFLFS",gxvar:"A459ContagemResultado_PFLFS",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A459ContagemResultado_PFLFS=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z459ContagemResultado_PFLFS=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("CONTAGEMRESULTADO_PFLFS",gx.O.A459ContagemResultado_PFLFS,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A459ContagemResultado_PFLFS=this.val()},val:function(){return gx.fn.getDecimalValue("CONTAGEMRESULTADO_PFLFS",'.',',')},nac:gx.falseFn};
   this.declareDomainHdlr( 43 , function() {
   });
   GXValidFnc[46]={fld:"TEXTBLOCKCONTAGEMRESULTADO_PFBFM", format:0,grid:0};
   GXValidFnc[48]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_pfbfm,isvalid:'e151v2_client',rgrid:[],fld:"CONTAGEMRESULTADO_PFBFM",gxz:"Z460ContagemResultado_PFBFM",gxold:"O460ContagemResultado_PFBFM",gxvar:"A460ContagemResultado_PFBFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A460ContagemResultado_PFBFM=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z460ContagemResultado_PFBFM=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("CONTAGEMRESULTADO_PFBFM",gx.O.A460ContagemResultado_PFBFM,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A460ContagemResultado_PFBFM=this.val()},val:function(){return gx.fn.getDecimalValue("CONTAGEMRESULTADO_PFBFM",'.',',')},nac:gx.falseFn};
   this.declareDomainHdlr( 48 , function() {
   });
   GXValidFnc[50]={fld:"TEXTBLOCKCONTAGEMRESULTADO_PFLFM", format:0,grid:0};
   GXValidFnc[52]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_pflfm,isvalid:'e161v2_client',rgrid:[],fld:"CONTAGEMRESULTADO_PFLFM",gxz:"Z461ContagemResultado_PFLFM",gxold:"O461ContagemResultado_PFLFM",gxvar:"A461ContagemResultado_PFLFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A461ContagemResultado_PFLFM=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z461ContagemResultado_PFLFM=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("CONTAGEMRESULTADO_PFLFM",gx.O.A461ContagemResultado_PFLFM,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A461ContagemResultado_PFLFM=this.val()},val:function(){return gx.fn.getDecimalValue("CONTAGEMRESULTADO_PFLFM",'.',',')},nac:gx.falseFn};
   this.declareDomainHdlr( 52 , function() {
   });
   GXValidFnc[55]={fld:"TEXTBLOCKCONTAGEMRESULTADO_DEFLATOR", format:0,grid:0};
   GXValidFnc[57]={lvl:0,type:"decimal",len:6,dec:3,sign:false,pic:"Z9.999",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DEFLATOR",gxz:"Z800ContagemResultado_Deflator",gxold:"O800ContagemResultado_Deflator",gxvar:"A800ContagemResultado_Deflator",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A800ContagemResultado_Deflator=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z800ContagemResultado_Deflator=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("CONTAGEMRESULTADO_DEFLATOR",gx.O.A800ContagemResultado_Deflator,3,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A800ContagemResultado_Deflator=this.val()},val:function(){return gx.fn.getDecimalValue("CONTAGEMRESULTADO_DEFLATOR",'.',',')},nac:gx.falseFn};
   this.declareDomainHdlr( 57 , function() {
   });
   GXValidFnc[62]={fld:"TEXTBLOCKCONTAGEMRESULTADO_PARECERTCN", format:0,grid:0};
   GXValidFnc[64]={lvl:0,type:"vchar",len:500,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PARECERTCN",gxz:"Z463ContagemResultado_ParecerTcn",gxold:"O463ContagemResultado_ParecerTcn",gxvar:"A463ContagemResultado_ParecerTcn",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A463ContagemResultado_ParecerTcn=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z463ContagemResultado_ParecerTcn=Value},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADO_PARECERTCN",gx.O.A463ContagemResultado_ParecerTcn,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A463ContagemResultado_ParecerTcn=this.val()},val:function(){return gx.fn.getControlValue("CONTAGEMRESULTADO_PARECERTCN")},nac:gx.falseFn};
   this.declareDomainHdlr( 64 , function() {
   });
   GXValidFnc[67]={fld:"TEXTBLOCKCONTAGEMRESULTADO_NAOCNFCNTCOD", format:0,grid:0};
   GXValidFnc[69]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_naocnfcntcod,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_NAOCNFCNTCOD",gxz:"Z469ContagemResultado_NaoCnfCntCod",gxold:"O469ContagemResultado_NaoCnfCntCod",gxvar:"A469ContagemResultado_NaoCnfCntCod",ucs:[],op:[],ip:[69],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.A469ContagemResultado_NaoCnfCntCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z469ContagemResultado_NaoCnfCntCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTAGEMRESULTADO_NAOCNFCNTCOD",gx.O.A469ContagemResultado_NaoCnfCntCod);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A469ContagemResultado_NaoCnfCntCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADO_NAOCNFCNTCOD",'.')},nac:function(){return (this.Gx_mode=="INS")&&!((0==this.AV11Insert_ContagemResultado_NaoCnfCntCod))}};
   this.declareDomainHdlr( 69 , function() {
   });
   GXValidFnc[71]={fld:"TEXTBLOCKCONTAGEMRESULTADO_STATUSCNT", format:0,grid:0};
   GXValidFnc[73]={lvl:0,type:"int",len:2,dec:0,sign:false,pic:"Z9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_statuscnt,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_STATUSCNT",gxz:"Z483ContagemResultado_StatusCnt",gxold:"O483ContagemResultado_StatusCnt",gxvar:"A483ContagemResultado_StatusCnt",ucs:[],op:[73],ip:[73],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A483ContagemResultado_StatusCnt=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z483ContagemResultado_StatusCnt=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTAGEMRESULTADO_STATUSCNT",gx.O.A483ContagemResultado_StatusCnt);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A483ContagemResultado_StatusCnt=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADO_STATUSCNT",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 73 , function() {
   });
   GXValidFnc[76]={fld:"TEXTBLOCKCONTAGEMRESULTADO_DIVERGENCIA", format:0,grid:0};
   GXValidFnc[78]={fld:"TABLEMERGEDCONTAGEMRESULTADO_DIVERGENCIA",grid:0};
   GXValidFnc[81]={lvl:0,type:"decimal",len:6,dec:2,sign:false,pic:"ZZ9.99",ro:1,grid:0,gxgrid:null,fnc:this.Validv_Contagemresultado_divergencia,isvalid:null,rgrid:[],fld:"vCONTAGEMRESULTADO_DIVERGENCIA",gxz:"ZV27ContagemResultado_Divergencia",gxold:"OV27ContagemResultado_Divergencia",gxvar:"AV27ContagemResultado_Divergencia",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV27ContagemResultado_Divergencia=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV27ContagemResultado_Divergencia=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vCONTAGEMRESULTADO_DIVERGENCIA",gx.O.AV27ContagemResultado_Divergencia,2,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV27ContagemResultado_Divergencia=this.val()},val:function(){return gx.fn.getDecimalValue("vCONTAGEMRESULTADO_DIVERGENCIA",'.',',')},nac:gx.falseFn};
   GXValidFnc[83]={fld:"CONTAGEMRESULTADO_DIVERGENCIA_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[85]={fld:"TEXTBLOCKCONTAGEMRESULTADOCONTAGENS_ESFORCO", format:0,grid:0};
   GXValidFnc[87]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultadocontagens_esforco,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADOCONTAGENS_ESFORCO",gxz:"Z482ContagemResultadoContagens_Esforco",gxold:"O482ContagemResultadoContagens_Esforco",gxvar:"A482ContagemResultadoContagens_Esforco",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A482ContagemResultadoContagens_Esforco=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z482ContagemResultadoContagens_Esforco=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADOCONTAGENS_ESFORCO",gx.O.A482ContagemResultadoContagens_Esforco,0)},c2v:function(){if(this.val()!==undefined)gx.O.A482ContagemResultadoContagens_Esforco=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADOCONTAGENS_ESFORCO",'.')},nac:gx.falseFn};
   GXValidFnc[89]={fld:"CONTAGEMRESULTADOCONTAGENS_ESFORCO_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[91]={fld:"TEXTBLOCKCONTAGEMRESULTADO_PLANILHA", format:0,grid:0};
   GXValidFnc[93]={lvl:0,type:"bitstr",len:1024,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PLANILHA",gxz:"Z852ContagemResultado_Planilha",gxold:"O852ContagemResultado_Planilha",gxvar:"A852ContagemResultado_Planilha",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A852ContagemResultado_Planilha=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z852ContagemResultado_Planilha=Value},v2c:function(){gx.fn.setBlobValue("CONTAGEMRESULTADO_PLANILHA",gx.O.A852ContagemResultado_Planilha)},c2v:function(){if(this.val()!==undefined)gx.O.A852ContagemResultado_Planilha=this.val()},val:function(){return gx.fn.getBlobValue("CONTAGEMRESULTADO_PLANILHA")},nac:gx.falseFn};
   GXValidFnc[96]={fld:"TEXTBLOCKCONTAGEMRESULTADO_NOMEPLA", format:0,grid:0};
   GXValidFnc[98]={lvl:0,type:"char",len:50,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_NOMEPLA",gxz:"Z853ContagemResultado_NomePla",gxold:"O853ContagemResultado_NomePla",gxvar:"A853ContagemResultado_NomePla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A853ContagemResultado_NomePla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z853ContagemResultado_NomePla=Value},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADO_NOMEPLA",gx.O.A853ContagemResultado_NomePla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A853ContagemResultado_NomePla=this.val()},val:function(){return gx.fn.getControlValue("CONTAGEMRESULTADO_NOMEPLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 98 , function() {
   });
   GXValidFnc[101]={fld:"TEXTBLOCKCONTAGEMRESULTADO_TIPOPLA", format:0,grid:0};
   GXValidFnc[103]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_TIPOPLA",gxz:"Z854ContagemResultado_TipoPla",gxold:"O854ContagemResultado_TipoPla",gxvar:"A854ContagemResultado_TipoPla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A854ContagemResultado_TipoPla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z854ContagemResultado_TipoPla=Value},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADO_TIPOPLA",gx.O.A854ContagemResultado_TipoPla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A854ContagemResultado_TipoPla=this.val()},val:function(){return gx.fn.getControlValue("CONTAGEMRESULTADO_TIPOPLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 103 , function() {
   });
   GXValidFnc[106]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[114]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Contagemresultado_codigo,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_CODIGO",gxz:"Z456ContagemResultado_Codigo",gxold:"O456ContagemResultado_Codigo",gxvar:"A456ContagemResultado_Codigo",ucs:[],op:[34,18],ip:[34,18,114],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z456ContagemResultado_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTAGEMRESULTADO_CODIGO",gx.O.A456ContagemResultado_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTAGEMRESULTADO_CODIGO",'.')},nac:function(){return !((0==this.AV7ContagemResultado_Codigo))}};
   this.declareDomainHdlr( 114 , function() {
   });
   this.A501ContagemResultado_OsFsOsFm = "" ;
   this.Z501ContagemResultado_OsFsOsFm = "" ;
   this.O501ContagemResultado_OsFsOsFm = "" ;
   this.A473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.Z473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.O473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.A511ContagemResultado_HoraCnt = "" ;
   this.Z511ContagemResultado_HoraCnt = "" ;
   this.O511ContagemResultado_HoraCnt = "" ;
   this.A470ContagemResultado_ContadorFMCod = 0 ;
   this.Z470ContagemResultado_ContadorFMCod = 0 ;
   this.O470ContagemResultado_ContadorFMCod = 0 ;
   this.A458ContagemResultado_PFBFS = 0 ;
   this.Z458ContagemResultado_PFBFS = 0 ;
   this.O458ContagemResultado_PFBFS = 0 ;
   this.A459ContagemResultado_PFLFS = 0 ;
   this.Z459ContagemResultado_PFLFS = 0 ;
   this.O459ContagemResultado_PFLFS = 0 ;
   this.A460ContagemResultado_PFBFM = 0 ;
   this.Z460ContagemResultado_PFBFM = 0 ;
   this.O460ContagemResultado_PFBFM = 0 ;
   this.A461ContagemResultado_PFLFM = 0 ;
   this.Z461ContagemResultado_PFLFM = 0 ;
   this.O461ContagemResultado_PFLFM = 0 ;
   this.A800ContagemResultado_Deflator = 0 ;
   this.Z800ContagemResultado_Deflator = 0 ;
   this.O800ContagemResultado_Deflator = 0 ;
   this.A463ContagemResultado_ParecerTcn = "" ;
   this.Z463ContagemResultado_ParecerTcn = "" ;
   this.O463ContagemResultado_ParecerTcn = "" ;
   this.A469ContagemResultado_NaoCnfCntCod = 0 ;
   this.Z469ContagemResultado_NaoCnfCntCod = 0 ;
   this.O469ContagemResultado_NaoCnfCntCod = 0 ;
   this.A483ContagemResultado_StatusCnt = 0 ;
   this.Z483ContagemResultado_StatusCnt = 0 ;
   this.O483ContagemResultado_StatusCnt = 0 ;
   this.AV27ContagemResultado_Divergencia = 0 ;
   this.ZV27ContagemResultado_Divergencia = 0 ;
   this.OV27ContagemResultado_Divergencia = 0 ;
   this.A482ContagemResultadoContagens_Esforco = 0 ;
   this.Z482ContagemResultadoContagens_Esforco = 0 ;
   this.O482ContagemResultadoContagens_Esforco = 0 ;
   this.A852ContagemResultado_Planilha = "" ;
   this.Z852ContagemResultado_Planilha = "" ;
   this.O852ContagemResultado_Planilha = "" ;
   this.A853ContagemResultado_NomePla = "" ;
   this.Z853ContagemResultado_NomePla = "" ;
   this.O853ContagemResultado_NomePla = "" ;
   this.A854ContagemResultado_TipoPla = "" ;
   this.Z854ContagemResultado_TipoPla = "" ;
   this.O854ContagemResultado_TipoPla = "" ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.Z456ContagemResultado_Codigo = 0 ;
   this.O456ContagemResultado_Codigo = 0 ;
   this.A127Sistema_Codigo = 0 ;
   this.A146Modulo_Codigo = 0 ;
   this.AV15WWPContext = {} ;
   this.AV12TrnContext = {} ;
   this.AV38GXV1 = 0 ;
   this.AV10Insert_ContagemResultado_ContadorFMCod = 0 ;
   this.AV11Insert_ContagemResultado_NaoCnfCntCod = 0 ;
   this.AV35CalculoPFinal = "" ;
   this.AV19IndiceDivergencia = 0 ;
   this.AV27ContagemResultado_Divergencia = 0 ;
   this.AV7ContagemResultado_Codigo = 0 ;
   this.AV20CalculoDivergencia = "" ;
   this.AV29ContagemResultado_ValorPF = 0 ;
   this.AV13TrnContextAtt = {} ;
   this.AV18ContagemResultado_HoraCnt = "" ;
   this.AV16Contrato_CalculoDivergencia = "" ;
   this.AV17Contrato_IndiceDivergencia = 0 ;
   this.AV8ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.AV14WebSession = {} ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.A511ContagemResultado_HoraCnt = "" ;
   this.A470ContagemResultado_ContadorFMCod = 0 ;
   this.A469ContagemResultado_NaoCnfCntCod = 0 ;
   this.A482ContagemResultadoContagens_Esforco = 0 ;
   this.A462ContagemResultado_Divergencia = 0 ;
   this.A483ContagemResultado_StatusCnt = 0 ;
   this.Gx_BScreen = 0 ;
   this.AV37Pgmname = "" ;
   this.A501ContagemResultado_OsFsOsFm = "" ;
   this.A485ContagemResultado_EhValidacao = false ;
   this.A513Sistema_Coordenacao = "" ;
   this.A457ContagemResultado_Demanda = "" ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.A601ContagemResultado_Servico = 0 ;
   this.A490ContagemResultado_ContratadaCod = 0 ;
   this.A805ContagemResultado_ContratadaOrigemCod = 0 ;
   this.A52Contratada_AreaTrabalhoCod = 0 ;
   this.A890ContagemResultado_Responsavel = 0 ;
   this.A602ContagemResultado_OSVinculada = 0 ;
   this.A468ContagemResultado_NaoCnfDmnCod = 0 ;
   this.A484ContagemResultado_StatusDmn = "" ;
   this.A481ContagemResultado_TimeCnt = gx.date.nullDate() ;
   this.A901ContagemResultadoContagens_Prazo = gx.date.nullDate() ;
   this.A458ContagemResultado_PFBFS = 0 ;
   this.A459ContagemResultado_PFLFS = 0 ;
   this.A460ContagemResultado_PFBFM = 0 ;
   this.A461ContagemResultado_PFLFM = 0 ;
   this.A463ContagemResultado_ParecerTcn = "" ;
   this.A479ContagemResultado_CrFMPessoaCod = 0 ;
   this.A474ContagemResultado_ContadorFMNom = "" ;
   this.A999ContagemResultado_CrFMEhContratada = false ;
   this.A1000ContagemResultado_CrFMEhContratante = false ;
   this.A478ContagemResultado_NaoCnfCntNom = "" ;
   this.A517ContagemResultado_Ultima = false ;
   this.A800ContagemResultado_Deflator = 0 ;
   this.A833ContagemResultado_CstUntPrd = 0 ;
   this.A852ContagemResultado_Planilha = "" ;
   this.A1389ContagemResultado_RdmnIssueId = 0 ;
   this.A1756ContagemResultado_NvlCnt = 0 ;
   this.A854ContagemResultado_TipoPla = "" ;
   this.A853ContagemResultado_NomePla = "" ;
   this.A493ContagemResultado_DemandaFM = "" ;
   this.Gx_mode = "" ;
   this.AV34Confirmado = false ;
   this.Events = {"e121v2_client": ["AFTER TRN", true] ,"e131v2_client": ["CONTAGEMRESULTADO_PFBFS.ISVALID", true] ,"e141v2_client": ["CONTAGEMRESULTADO_PFLFS.ISVALID", true] ,"e151v2_client": ["CONTAGEMRESULTADO_PFBFM.ISVALID", true] ,"e161v2_client": ["CONTAGEMRESULTADO_PFLFM.ISVALID", true] ,"e171v72_client": ["ENTER", true] ,"e181v72_client": ["CANCEL", true]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8ContagemResultado_DataCnt',fld:'vCONTAGEMRESULTADO_DATACNT',pic:'',hsh:true,nv:''},{av:'AV18ContagemResultado_HoraCnt',fld:'vCONTAGEMRESULTADO_HORACNT',pic:'',hsh:true,nv:''}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A483ContagemResultado_StatusCnt',fld:'CONTAGEMRESULTADO_STATUSCNT',pic:'Z9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A463ContagemResultado_ParecerTcn',fld:'CONTAGEMRESULTADO_PARECERTCN',pic:'',nv:''},{av:'A1389ContagemResultado_RdmnIssueId',fld:'CONTAGEMRESULTADO_RDMNISSUEID',pic:'ZZZZZ9',nv:0},{av:'AV12TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[{av:'AV34Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["CONTAGEMRESULTADO_PFBFS.ISVALID"] = [[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]];
   this.EvtParms["CONTAGEMRESULTADO_PFLFS.ISVALID"] = [[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]];
   this.EvtParms["CONTAGEMRESULTADO_PFBFM.ISVALID"] = [[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]];
   this.EvtParms["CONTAGEMRESULTADO_PFLFM.ISVALID"] = [[{av:'AV20CalculoDivergencia',fld:'vCALCULODIVERGENCIA',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A460ContagemResultado_PFBFM',fld:'CONTAGEMRESULTADO_PFBFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A459ContagemResultado_PFLFS',fld:'CONTAGEMRESULTADO_PFLFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A461ContagemResultado_PFLFM',fld:'CONTAGEMRESULTADO_PFLFM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],[{av:'AV27ContagemResultado_Divergencia',fld:'vCONTAGEMRESULTADO_DIVERGENCIA',pic:'ZZ9.99',nv:0.0}]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("A457ContagemResultado_Demanda", "CONTAGEMRESULTADO_DEMANDA", 0, "svchar");
   this.setVCMap("A493ContagemResultado_DemandaFM", "CONTAGEMRESULTADO_DEMANDAFM", 0, "svchar");
   this.setVCMap("AV7ContagemResultado_Codigo", "vCONTAGEMRESULTADO_CODIGO", 0, "int");
   this.setVCMap("AV8ContagemResultado_DataCnt", "vCONTAGEMRESULTADO_DATACNT", 0, "date");
   this.setVCMap("AV18ContagemResultado_HoraCnt", "vCONTAGEMRESULTADO_HORACNT", 0, "char");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("AV10Insert_ContagemResultado_ContadorFMCod", "vINSERT_CONTAGEMRESULTADO_CONTADORFMCOD", 0, "int");
   this.setVCMap("AV11Insert_ContagemResultado_NaoCnfCntCod", "vINSERT_CONTAGEMRESULTADO_NAOCNFCNTCOD", 0, "int");
   this.setVCMap("A462ContagemResultado_Divergencia", "CONTAGEMRESULTADO_DIVERGENCIA", 0, "decimal");
   this.setVCMap("AV20CalculoDivergencia", "vCALCULODIVERGENCIA", 0, "char");
   this.setVCMap("AV19IndiceDivergencia", "vINDICEDIVERGENCIA", 0, "decimal");
   this.setVCMap("A1553ContagemResultado_CntSrvCod", "CONTAGEMRESULTADO_CNTSRVCOD", 0, "int");
   this.setVCMap("A833ContagemResultado_CstUntPrd", "CONTAGEMRESULTADO_CSTUNTPRD", 0, "decimal");
   this.setVCMap("AV35CalculoPFinal", "vCALCULOPFINAL", 0, "char");
   this.setVCMap("A517ContagemResultado_Ultima", "CONTAGEMRESULTADO_ULTIMA", 0, "boolean");
   this.setVCMap("A490ContagemResultado_ContratadaCod", "CONTAGEMRESULTADO_CONTRATADACOD", 0, "int");
   this.setVCMap("A481ContagemResultado_TimeCnt", "CONTAGEMRESULTADO_TIMECNT", 0, "dtime");
   this.setVCMap("A901ContagemResultadoContagens_Prazo", "CONTAGEMRESULTADOCONTAGENS_PRAZO", 0, "dtime");
   this.setVCMap("A1756ContagemResultado_NvlCnt", "CONTAGEMRESULTADO_NVLCNT", 0, "int");
   this.setVCMap("A146Modulo_Codigo", "MODULO_CODIGO", 0, "int");
   this.setVCMap("A485ContagemResultado_EhValidacao", "CONTAGEMRESULTADO_EHVALIDACAO", 0, "boolean");
   this.setVCMap("A484ContagemResultado_StatusDmn", "CONTAGEMRESULTADO_STATUSDMN", 0, "char");
   this.setVCMap("A1389ContagemResultado_RdmnIssueId", "CONTAGEMRESULTADO_RDMNISSUEID", 0, "int");
   this.setVCMap("A890ContagemResultado_Responsavel", "CONTAGEMRESULTADO_RESPONSAVEL", 0, "int");
   this.setVCMap("A468ContagemResultado_NaoCnfDmnCod", "CONTAGEMRESULTADO_NAOCNFDMNCOD", 0, "int");
   this.setVCMap("A805ContagemResultado_ContratadaOrigemCod", "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD", 0, "int");
   this.setVCMap("A602ContagemResultado_OSVinculada", "CONTAGEMRESULTADO_OSVINCULADA", 0, "int");
   this.setVCMap("A999ContagemResultado_CrFMEhContratada", "CONTAGEMRESULTADO_CRFMEHCONTRATADA", 0, "boolean");
   this.setVCMap("A1000ContagemResultado_CrFMEhContratante", "CONTAGEMRESULTADO_CRFMEHCONTRATANTE", 0, "boolean");
   this.setVCMap("A479ContagemResultado_CrFMPessoaCod", "CONTAGEMRESULTADO_CRFMPESSOACOD", 0, "int");
   this.setVCMap("A478ContagemResultado_NaoCnfCntNom", "CONTAGEMRESULTADO_NAOCNFCNTNOM", 0, "char");
   this.setVCMap("A127Sistema_Codigo", "SISTEMA_CODIGO", 0, "int");
   this.setVCMap("A513Sistema_Coordenacao", "SISTEMA_COORDENACAO", 0, "svchar");
   this.setVCMap("A52Contratada_AreaTrabalhoCod", "CONTRATADA_AREATRABALHOCOD", 0, "int");
   this.setVCMap("A601ContagemResultado_Servico", "CONTAGEMRESULTADO_SERVICO", 0, "int");
   this.setVCMap("A474ContagemResultado_ContadorFMNom", "CONTAGEMRESULTADO_CONTADORFMNOM", 0, "char");
   this.setVCMap("AV37Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV34Confirmado", "vCONFIRMADO", 0, "boolean");
   this.setVCMap("AV15WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV12TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.InitStandaloneVars( );
   this.LvlOlds[ 72 ]= ["O483ContagemResultado_StatusCnt"] ;
});
gx.createParentObj(contagemresultadocontagens);
