/*
               File: LoadAuditServico
        Description: Load Audit Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:21.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditservico : GXProcedure
   {
      public loadauditservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Servico_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Servico_Codigo = aP2_Servico_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Servico_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditservico objloadauditservico;
         objloadauditservico = new loadauditservico();
         objloadauditservico.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditservico.AV10AuditingObject = aP1_AuditingObject;
         objloadauditservico.AV16Servico_Codigo = aP2_Servico_Codigo;
         objloadauditservico.AV14ActualMode = aP3_ActualMode;
         objloadauditservico.context.SetSubmitInitialConfig(context);
         objloadauditservico.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditservico);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditservico)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A42 */
         pr_default.execute(0, new Object[] {AV16Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A156Servico_Descricao = P00A42_A156Servico_Descricao[0];
            n156Servico_Descricao = P00A42_n156Servico_Descricao[0];
            A157ServicoGrupo_Codigo = P00A42_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = P00A42_A158ServicoGrupo_Descricao[0];
            A633Servico_UO = P00A42_A633Servico_UO[0];
            n633Servico_UO = P00A42_n633Servico_UO[0];
            A1077Servico_UORespExclusiva = P00A42_A1077Servico_UORespExclusiva[0];
            n1077Servico_UORespExclusiva = P00A42_n1077Servico_UORespExclusiva[0];
            A631Servico_Vinculado = P00A42_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P00A42_n631Servico_Vinculado[0];
            A641Servico_VincDesc = P00A42_A641Servico_VincDesc[0];
            n641Servico_VincDesc = P00A42_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = P00A42_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = P00A42_n1557Servico_VincSigla[0];
            A889Servico_Terceriza = P00A42_A889Servico_Terceriza[0];
            n889Servico_Terceriza = P00A42_n889Servico_Terceriza[0];
            A1061Servico_Tela = P00A42_A1061Servico_Tela[0];
            n1061Servico_Tela = P00A42_n1061Servico_Tela[0];
            A1072Servico_Atende = P00A42_A1072Servico_Atende[0];
            n1072Servico_Atende = P00A42_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = P00A42_A1429Servico_ObrigaValores[0];
            n1429Servico_ObrigaValores = P00A42_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = P00A42_A1436Servico_ObjetoControle[0];
            A1530Servico_TipoHierarquia = P00A42_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00A42_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = P00A42_A1534Servico_PercTmp[0];
            n1534Servico_PercTmp = P00A42_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = P00A42_A1535Servico_PercPgm[0];
            n1535Servico_PercPgm = P00A42_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = P00A42_A1536Servico_PercCnc[0];
            n1536Servico_PercCnc = P00A42_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = P00A42_A1545Servico_Anterior[0];
            n1545Servico_Anterior = P00A42_n1545Servico_Anterior[0];
            A1546Servico_Posterior = P00A42_A1546Servico_Posterior[0];
            n1546Servico_Posterior = P00A42_n1546Servico_Posterior[0];
            A632Servico_Ativo = P00A42_A632Servico_Ativo[0];
            A1635Servico_IsPublico = P00A42_A1635Servico_IsPublico[0];
            A2047Servico_LinNegCod = P00A42_A2047Servico_LinNegCod[0];
            n2047Servico_LinNegCod = P00A42_n2047Servico_LinNegCod[0];
            A2048Servico_LinNegDsc = P00A42_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = P00A42_n2048Servico_LinNegDsc[0];
            A2092Servico_IsOrigemReferencia = P00A42_A2092Servico_IsOrigemReferencia[0];
            A2131Servico_PausaSLA = P00A42_A2131Servico_PausaSLA[0];
            A608Servico_Nome = P00A42_A608Servico_Nome[0];
            A605Servico_Sigla = P00A42_A605Servico_Sigla[0];
            A155Servico_Codigo = P00A42_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00A42_A158ServicoGrupo_Descricao[0];
            A641Servico_VincDesc = P00A42_A641Servico_VincDesc[0];
            n641Servico_VincDesc = P00A42_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = P00A42_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = P00A42_n1557Servico_VincSigla[0];
            A2048Servico_LinNegDsc = P00A42_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = P00A42_n2048Servico_LinNegDsc[0];
            GXt_int1 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
            A1551Servico_Responsavel = GXt_int1;
            GXt_int2 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
            A640Servico_Vinculados = GXt_int2;
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Servico";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A608Servico_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A156Servico_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A605Servico_Sigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo de Servi�os";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Descricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A158ServicoGrupo_Descricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_UO";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Organizacional";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A633Servico_UO), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_UORespExclusiva";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Visibilidade";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1077Servico_UORespExclusiva);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o vinculado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculados";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico_Vinculados";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_VincDesc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A641Servico_VincDesc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_VincSigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1557Servico_VincSigla;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Terceriza";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pode ser tercerizado?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A889Servico_Terceriza);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Tela";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tela";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1061Servico_Tela;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Atende";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Atende";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1072Servico_Atende;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_ObrigaValores";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Obriga Valores";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1429Servico_ObrigaValores;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_ObjetoControle";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Objeto de Controle";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1436Servico_ObjetoControle;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_TipoHierarquia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Hierarquia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PercTmp";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Tempo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PercPgm";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Pagamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PercCnc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Cancelamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Anterior";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico Anterior";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Posterior";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico Posterior";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Responsavel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Ativo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A632Servico_Ativo);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_IsPublico";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "P�blico?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1635Servico_IsPublico);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_LinNegCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Linha de Neg�cio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_LinNegDsc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2048Servico_LinNegDsc;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_IsOrigemReferencia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exige Origem de Refer�ncia";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Identificacao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Identifica��o do Servi�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2107Servico_Identificacao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PausaSLA";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pausar o prazo da SLA?";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A2131Servico_PausaSLA);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A43 */
         pr_default.execute(1, new Object[] {AV16Servico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A156Servico_Descricao = P00A43_A156Servico_Descricao[0];
            n156Servico_Descricao = P00A43_n156Servico_Descricao[0];
            A157ServicoGrupo_Codigo = P00A43_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = P00A43_A158ServicoGrupo_Descricao[0];
            A633Servico_UO = P00A43_A633Servico_UO[0];
            n633Servico_UO = P00A43_n633Servico_UO[0];
            A1077Servico_UORespExclusiva = P00A43_A1077Servico_UORespExclusiva[0];
            n1077Servico_UORespExclusiva = P00A43_n1077Servico_UORespExclusiva[0];
            A631Servico_Vinculado = P00A43_A631Servico_Vinculado[0];
            n631Servico_Vinculado = P00A43_n631Servico_Vinculado[0];
            A641Servico_VincDesc = P00A43_A641Servico_VincDesc[0];
            n641Servico_VincDesc = P00A43_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = P00A43_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = P00A43_n1557Servico_VincSigla[0];
            A889Servico_Terceriza = P00A43_A889Servico_Terceriza[0];
            n889Servico_Terceriza = P00A43_n889Servico_Terceriza[0];
            A1061Servico_Tela = P00A43_A1061Servico_Tela[0];
            n1061Servico_Tela = P00A43_n1061Servico_Tela[0];
            A1072Servico_Atende = P00A43_A1072Servico_Atende[0];
            n1072Servico_Atende = P00A43_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = P00A43_A1429Servico_ObrigaValores[0];
            n1429Servico_ObrigaValores = P00A43_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = P00A43_A1436Servico_ObjetoControle[0];
            A1530Servico_TipoHierarquia = P00A43_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00A43_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = P00A43_A1534Servico_PercTmp[0];
            n1534Servico_PercTmp = P00A43_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = P00A43_A1535Servico_PercPgm[0];
            n1535Servico_PercPgm = P00A43_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = P00A43_A1536Servico_PercCnc[0];
            n1536Servico_PercCnc = P00A43_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = P00A43_A1545Servico_Anterior[0];
            n1545Servico_Anterior = P00A43_n1545Servico_Anterior[0];
            A1546Servico_Posterior = P00A43_A1546Servico_Posterior[0];
            n1546Servico_Posterior = P00A43_n1546Servico_Posterior[0];
            A632Servico_Ativo = P00A43_A632Servico_Ativo[0];
            A1635Servico_IsPublico = P00A43_A1635Servico_IsPublico[0];
            A2047Servico_LinNegCod = P00A43_A2047Servico_LinNegCod[0];
            n2047Servico_LinNegCod = P00A43_n2047Servico_LinNegCod[0];
            A2048Servico_LinNegDsc = P00A43_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = P00A43_n2048Servico_LinNegDsc[0];
            A2092Servico_IsOrigemReferencia = P00A43_A2092Servico_IsOrigemReferencia[0];
            A2131Servico_PausaSLA = P00A43_A2131Servico_PausaSLA[0];
            A608Servico_Nome = P00A43_A608Servico_Nome[0];
            A605Servico_Sigla = P00A43_A605Servico_Sigla[0];
            A155Servico_Codigo = P00A43_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00A43_A158ServicoGrupo_Descricao[0];
            A641Servico_VincDesc = P00A43_A641Servico_VincDesc[0];
            n641Servico_VincDesc = P00A43_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = P00A43_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = P00A43_n1557Servico_VincSigla[0];
            A2048Servico_LinNegDsc = P00A43_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = P00A43_n2048Servico_LinNegDsc[0];
            GXt_int1 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
            A1551Servico_Responsavel = GXt_int1;
            GXt_int2 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
            A640Servico_Vinculados = GXt_int2;
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Servico";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "C�digo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A608Servico_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A156Servico_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A605Servico_Sigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo de Servi�os";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "ServicoGrupo_Descricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Grupo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A158ServicoGrupo_Descricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_UO";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Unidade Organizacional";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A633Servico_UO), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_UORespExclusiva";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Visibilidade";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1077Servico_UORespExclusiva);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servi�o vinculado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Vinculados";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico_Vinculados";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_VincDesc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A641Servico_VincDesc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_VincSigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Sigla";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1557Servico_VincSigla;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Terceriza";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pode ser tercerizado?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A889Servico_Terceriza);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Tela";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tela";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1061Servico_Tela;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Atende";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Atende";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1072Servico_Atende;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_ObrigaValores";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Obriga Valores";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1429Servico_ObrigaValores;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_ObjetoControle";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Objeto de Controle";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1436Servico_ObjetoControle;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_TipoHierarquia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo de Hierarquia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PercTmp";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Tempo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PercPgm";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Pagamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PercCnc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Perc. de Cancelamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Anterior";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico Anterior";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Posterior";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Servico Posterior";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Responsavel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Ativo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Ativo?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A632Servico_Ativo);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_IsPublico";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "P�blico?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1635Servico_IsPublico);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_LinNegCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Linha de Neg�cio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_LinNegDsc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2048Servico_LinNegDsc;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_IsOrigemReferencia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Exige Origem de Refer�ncia";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_Identificacao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Identifica��o do Servi�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2107Servico_Identificacao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Servico_PausaSLA";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pausar o prazo da SLA?";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2131Servico_PausaSLA);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV28GXV1 = 1;
               while ( AV28GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV28GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00A43_A155Servico_Codigo[0] == A155Servico_Codigo ) )
                  {
                     A156Servico_Descricao = P00A43_A156Servico_Descricao[0];
                     n156Servico_Descricao = P00A43_n156Servico_Descricao[0];
                     A157ServicoGrupo_Codigo = P00A43_A157ServicoGrupo_Codigo[0];
                     A158ServicoGrupo_Descricao = P00A43_A158ServicoGrupo_Descricao[0];
                     A633Servico_UO = P00A43_A633Servico_UO[0];
                     n633Servico_UO = P00A43_n633Servico_UO[0];
                     A1077Servico_UORespExclusiva = P00A43_A1077Servico_UORespExclusiva[0];
                     n1077Servico_UORespExclusiva = P00A43_n1077Servico_UORespExclusiva[0];
                     A631Servico_Vinculado = P00A43_A631Servico_Vinculado[0];
                     n631Servico_Vinculado = P00A43_n631Servico_Vinculado[0];
                     A641Servico_VincDesc = P00A43_A641Servico_VincDesc[0];
                     n641Servico_VincDesc = P00A43_n641Servico_VincDesc[0];
                     A1557Servico_VincSigla = P00A43_A1557Servico_VincSigla[0];
                     n1557Servico_VincSigla = P00A43_n1557Servico_VincSigla[0];
                     A889Servico_Terceriza = P00A43_A889Servico_Terceriza[0];
                     n889Servico_Terceriza = P00A43_n889Servico_Terceriza[0];
                     A1061Servico_Tela = P00A43_A1061Servico_Tela[0];
                     n1061Servico_Tela = P00A43_n1061Servico_Tela[0];
                     A1072Servico_Atende = P00A43_A1072Servico_Atende[0];
                     n1072Servico_Atende = P00A43_n1072Servico_Atende[0];
                     A1429Servico_ObrigaValores = P00A43_A1429Servico_ObrigaValores[0];
                     n1429Servico_ObrigaValores = P00A43_n1429Servico_ObrigaValores[0];
                     A1436Servico_ObjetoControle = P00A43_A1436Servico_ObjetoControle[0];
                     A1530Servico_TipoHierarquia = P00A43_A1530Servico_TipoHierarquia[0];
                     n1530Servico_TipoHierarquia = P00A43_n1530Servico_TipoHierarquia[0];
                     A1534Servico_PercTmp = P00A43_A1534Servico_PercTmp[0];
                     n1534Servico_PercTmp = P00A43_n1534Servico_PercTmp[0];
                     A1535Servico_PercPgm = P00A43_A1535Servico_PercPgm[0];
                     n1535Servico_PercPgm = P00A43_n1535Servico_PercPgm[0];
                     A1536Servico_PercCnc = P00A43_A1536Servico_PercCnc[0];
                     n1536Servico_PercCnc = P00A43_n1536Servico_PercCnc[0];
                     A1545Servico_Anterior = P00A43_A1545Servico_Anterior[0];
                     n1545Servico_Anterior = P00A43_n1545Servico_Anterior[0];
                     A1546Servico_Posterior = P00A43_A1546Servico_Posterior[0];
                     n1546Servico_Posterior = P00A43_n1546Servico_Posterior[0];
                     A632Servico_Ativo = P00A43_A632Servico_Ativo[0];
                     A1635Servico_IsPublico = P00A43_A1635Servico_IsPublico[0];
                     A2047Servico_LinNegCod = P00A43_A2047Servico_LinNegCod[0];
                     n2047Servico_LinNegCod = P00A43_n2047Servico_LinNegCod[0];
                     A2048Servico_LinNegDsc = P00A43_A2048Servico_LinNegDsc[0];
                     n2048Servico_LinNegDsc = P00A43_n2048Servico_LinNegDsc[0];
                     A2092Servico_IsOrigemReferencia = P00A43_A2092Servico_IsOrigemReferencia[0];
                     A2131Servico_PausaSLA = P00A43_A2131Servico_PausaSLA[0];
                     A608Servico_Nome = P00A43_A608Servico_Nome[0];
                     A605Servico_Sigla = P00A43_A605Servico_Sigla[0];
                     A158ServicoGrupo_Descricao = P00A43_A158ServicoGrupo_Descricao[0];
                     A641Servico_VincDesc = P00A43_A641Servico_VincDesc[0];
                     n641Servico_VincDesc = P00A43_n641Servico_VincDesc[0];
                     A1557Servico_VincSigla = P00A43_A1557Servico_VincSigla[0];
                     n1557Servico_VincSigla = P00A43_n1557Servico_VincSigla[0];
                     A2048Servico_LinNegDsc = P00A43_A2048Servico_LinNegDsc[0];
                     n2048Servico_LinNegDsc = P00A43_n2048Servico_LinNegDsc[0];
                     GXt_int1 = A1551Servico_Responsavel;
                     new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
                     A1551Servico_Responsavel = GXt_int1;
                     GXt_int2 = A640Servico_Vinculados;
                     new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
                     A640Servico_Vinculados = GXt_int2;
                     A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
                     AV30GXV2 = 1;
                     while ( AV30GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV30GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A608Servico_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Descricao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A156Servico_Descricao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Sigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A605Servico_Sigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ServicoGrupo_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "ServicoGrupo_Descricao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A158ServicoGrupo_Descricao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_UO") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A633Servico_UO), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_UORespExclusiva") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1077Servico_UORespExclusiva);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Vinculado") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Vinculados") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_VincDesc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A641Servico_VincDesc;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_VincSigla") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1557Servico_VincSigla;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Terceriza") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A889Servico_Terceriza);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Tela") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1061Servico_Tela;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Atende") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1072Servico_Atende;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_ObrigaValores") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1429Servico_ObrigaValores;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_ObjetoControle") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1436Servico_ObjetoControle;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_TipoHierarquia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_PercTmp") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_PercPgm") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_PercCnc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Anterior") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Posterior") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Responsavel") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Ativo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A632Servico_Ativo);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_IsPublico") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1635Servico_IsPublico);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_LinNegCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_LinNegDsc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2048Servico_LinNegDsc;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_IsOrigemReferencia") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_Identificacao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2107Servico_Identificacao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Servico_PausaSLA") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A2131Servico_PausaSLA);
                        }
                        AV30GXV2 = (int)(AV30GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV28GXV1 = (int)(AV28GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A42_A156Servico_Descricao = new String[] {""} ;
         P00A42_n156Servico_Descricao = new bool[] {false} ;
         P00A42_A157ServicoGrupo_Codigo = new int[1] ;
         P00A42_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00A42_A633Servico_UO = new int[1] ;
         P00A42_n633Servico_UO = new bool[] {false} ;
         P00A42_A1077Servico_UORespExclusiva = new bool[] {false} ;
         P00A42_n1077Servico_UORespExclusiva = new bool[] {false} ;
         P00A42_A631Servico_Vinculado = new int[1] ;
         P00A42_n631Servico_Vinculado = new bool[] {false} ;
         P00A42_A641Servico_VincDesc = new String[] {""} ;
         P00A42_n641Servico_VincDesc = new bool[] {false} ;
         P00A42_A1557Servico_VincSigla = new String[] {""} ;
         P00A42_n1557Servico_VincSigla = new bool[] {false} ;
         P00A42_A889Servico_Terceriza = new bool[] {false} ;
         P00A42_n889Servico_Terceriza = new bool[] {false} ;
         P00A42_A1061Servico_Tela = new String[] {""} ;
         P00A42_n1061Servico_Tela = new bool[] {false} ;
         P00A42_A1072Servico_Atende = new String[] {""} ;
         P00A42_n1072Servico_Atende = new bool[] {false} ;
         P00A42_A1429Servico_ObrigaValores = new String[] {""} ;
         P00A42_n1429Servico_ObrigaValores = new bool[] {false} ;
         P00A42_A1436Servico_ObjetoControle = new String[] {""} ;
         P00A42_A1530Servico_TipoHierarquia = new short[1] ;
         P00A42_n1530Servico_TipoHierarquia = new bool[] {false} ;
         P00A42_A1534Servico_PercTmp = new short[1] ;
         P00A42_n1534Servico_PercTmp = new bool[] {false} ;
         P00A42_A1535Servico_PercPgm = new short[1] ;
         P00A42_n1535Servico_PercPgm = new bool[] {false} ;
         P00A42_A1536Servico_PercCnc = new short[1] ;
         P00A42_n1536Servico_PercCnc = new bool[] {false} ;
         P00A42_A1545Servico_Anterior = new int[1] ;
         P00A42_n1545Servico_Anterior = new bool[] {false} ;
         P00A42_A1546Servico_Posterior = new int[1] ;
         P00A42_n1546Servico_Posterior = new bool[] {false} ;
         P00A42_A632Servico_Ativo = new bool[] {false} ;
         P00A42_A1635Servico_IsPublico = new bool[] {false} ;
         P00A42_A2047Servico_LinNegCod = new int[1] ;
         P00A42_n2047Servico_LinNegCod = new bool[] {false} ;
         P00A42_A2048Servico_LinNegDsc = new String[] {""} ;
         P00A42_n2048Servico_LinNegDsc = new bool[] {false} ;
         P00A42_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         P00A42_A2131Servico_PausaSLA = new bool[] {false} ;
         P00A42_A608Servico_Nome = new String[] {""} ;
         P00A42_A605Servico_Sigla = new String[] {""} ;
         P00A42_A155Servico_Codigo = new int[1] ;
         A156Servico_Descricao = "";
         A158ServicoGrupo_Descricao = "";
         A641Servico_VincDesc = "";
         A1557Servico_VincSigla = "";
         A1061Servico_Tela = "";
         A1072Servico_Atende = "";
         A1429Servico_ObrigaValores = "";
         A1436Servico_ObjetoControle = "";
         A2048Servico_LinNegDsc = "";
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         A2107Servico_Identificacao = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A43_A156Servico_Descricao = new String[] {""} ;
         P00A43_n156Servico_Descricao = new bool[] {false} ;
         P00A43_A157ServicoGrupo_Codigo = new int[1] ;
         P00A43_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00A43_A633Servico_UO = new int[1] ;
         P00A43_n633Servico_UO = new bool[] {false} ;
         P00A43_A1077Servico_UORespExclusiva = new bool[] {false} ;
         P00A43_n1077Servico_UORespExclusiva = new bool[] {false} ;
         P00A43_A631Servico_Vinculado = new int[1] ;
         P00A43_n631Servico_Vinculado = new bool[] {false} ;
         P00A43_A641Servico_VincDesc = new String[] {""} ;
         P00A43_n641Servico_VincDesc = new bool[] {false} ;
         P00A43_A1557Servico_VincSigla = new String[] {""} ;
         P00A43_n1557Servico_VincSigla = new bool[] {false} ;
         P00A43_A889Servico_Terceriza = new bool[] {false} ;
         P00A43_n889Servico_Terceriza = new bool[] {false} ;
         P00A43_A1061Servico_Tela = new String[] {""} ;
         P00A43_n1061Servico_Tela = new bool[] {false} ;
         P00A43_A1072Servico_Atende = new String[] {""} ;
         P00A43_n1072Servico_Atende = new bool[] {false} ;
         P00A43_A1429Servico_ObrigaValores = new String[] {""} ;
         P00A43_n1429Servico_ObrigaValores = new bool[] {false} ;
         P00A43_A1436Servico_ObjetoControle = new String[] {""} ;
         P00A43_A1530Servico_TipoHierarquia = new short[1] ;
         P00A43_n1530Servico_TipoHierarquia = new bool[] {false} ;
         P00A43_A1534Servico_PercTmp = new short[1] ;
         P00A43_n1534Servico_PercTmp = new bool[] {false} ;
         P00A43_A1535Servico_PercPgm = new short[1] ;
         P00A43_n1535Servico_PercPgm = new bool[] {false} ;
         P00A43_A1536Servico_PercCnc = new short[1] ;
         P00A43_n1536Servico_PercCnc = new bool[] {false} ;
         P00A43_A1545Servico_Anterior = new int[1] ;
         P00A43_n1545Servico_Anterior = new bool[] {false} ;
         P00A43_A1546Servico_Posterior = new int[1] ;
         P00A43_n1546Servico_Posterior = new bool[] {false} ;
         P00A43_A632Servico_Ativo = new bool[] {false} ;
         P00A43_A1635Servico_IsPublico = new bool[] {false} ;
         P00A43_A2047Servico_LinNegCod = new int[1] ;
         P00A43_n2047Servico_LinNegCod = new bool[] {false} ;
         P00A43_A2048Servico_LinNegDsc = new String[] {""} ;
         P00A43_n2048Servico_LinNegDsc = new bool[] {false} ;
         P00A43_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         P00A43_A2131Servico_PausaSLA = new bool[] {false} ;
         P00A43_A608Servico_Nome = new String[] {""} ;
         P00A43_A605Servico_Sigla = new String[] {""} ;
         P00A43_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditservico__default(),
            new Object[][] {
                new Object[] {
               P00A42_A156Servico_Descricao, P00A42_n156Servico_Descricao, P00A42_A157ServicoGrupo_Codigo, P00A42_A158ServicoGrupo_Descricao, P00A42_A633Servico_UO, P00A42_n633Servico_UO, P00A42_A1077Servico_UORespExclusiva, P00A42_n1077Servico_UORespExclusiva, P00A42_A631Servico_Vinculado, P00A42_n631Servico_Vinculado,
               P00A42_A641Servico_VincDesc, P00A42_n641Servico_VincDesc, P00A42_A1557Servico_VincSigla, P00A42_n1557Servico_VincSigla, P00A42_A889Servico_Terceriza, P00A42_n889Servico_Terceriza, P00A42_A1061Servico_Tela, P00A42_n1061Servico_Tela, P00A42_A1072Servico_Atende, P00A42_n1072Servico_Atende,
               P00A42_A1429Servico_ObrigaValores, P00A42_n1429Servico_ObrigaValores, P00A42_A1436Servico_ObjetoControle, P00A42_A1530Servico_TipoHierarquia, P00A42_n1530Servico_TipoHierarquia, P00A42_A1534Servico_PercTmp, P00A42_n1534Servico_PercTmp, P00A42_A1535Servico_PercPgm, P00A42_n1535Servico_PercPgm, P00A42_A1536Servico_PercCnc,
               P00A42_n1536Servico_PercCnc, P00A42_A1545Servico_Anterior, P00A42_n1545Servico_Anterior, P00A42_A1546Servico_Posterior, P00A42_n1546Servico_Posterior, P00A42_A632Servico_Ativo, P00A42_A1635Servico_IsPublico, P00A42_A2047Servico_LinNegCod, P00A42_n2047Servico_LinNegCod, P00A42_A2048Servico_LinNegDsc,
               P00A42_n2048Servico_LinNegDsc, P00A42_A2092Servico_IsOrigemReferencia, P00A42_A2131Servico_PausaSLA, P00A42_A608Servico_Nome, P00A42_A605Servico_Sigla, P00A42_A155Servico_Codigo
               }
               , new Object[] {
               P00A43_A156Servico_Descricao, P00A43_n156Servico_Descricao, P00A43_A157ServicoGrupo_Codigo, P00A43_A158ServicoGrupo_Descricao, P00A43_A633Servico_UO, P00A43_n633Servico_UO, P00A43_A1077Servico_UORespExclusiva, P00A43_n1077Servico_UORespExclusiva, P00A43_A631Servico_Vinculado, P00A43_n631Servico_Vinculado,
               P00A43_A641Servico_VincDesc, P00A43_n641Servico_VincDesc, P00A43_A1557Servico_VincSigla, P00A43_n1557Servico_VincSigla, P00A43_A889Servico_Terceriza, P00A43_n889Servico_Terceriza, P00A43_A1061Servico_Tela, P00A43_n1061Servico_Tela, P00A43_A1072Servico_Atende, P00A43_n1072Servico_Atende,
               P00A43_A1429Servico_ObrigaValores, P00A43_n1429Servico_ObrigaValores, P00A43_A1436Servico_ObjetoControle, P00A43_A1530Servico_TipoHierarquia, P00A43_n1530Servico_TipoHierarquia, P00A43_A1534Servico_PercTmp, P00A43_n1534Servico_PercTmp, P00A43_A1535Servico_PercPgm, P00A43_n1535Servico_PercPgm, P00A43_A1536Servico_PercCnc,
               P00A43_n1536Servico_PercCnc, P00A43_A1545Servico_Anterior, P00A43_n1545Servico_Anterior, P00A43_A1546Servico_Posterior, P00A43_n1546Servico_Posterior, P00A43_A632Servico_Ativo, P00A43_A1635Servico_IsPublico, P00A43_A2047Servico_LinNegCod, P00A43_n2047Servico_LinNegCod, P00A43_A2048Servico_LinNegDsc,
               P00A43_n2048Servico_LinNegDsc, P00A43_A2092Servico_IsOrigemReferencia, P00A43_A2131Servico_PausaSLA, P00A43_A608Servico_Nome, P00A43_A605Servico_Sigla, P00A43_A155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1530Servico_TipoHierarquia ;
      private short A1534Servico_PercTmp ;
      private short A1535Servico_PercPgm ;
      private short A1536Servico_PercCnc ;
      private short A640Servico_Vinculados ;
      private short GXt_int2 ;
      private int AV16Servico_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A633Servico_UO ;
      private int A631Servico_Vinculado ;
      private int A1545Servico_Anterior ;
      private int A1546Servico_Posterior ;
      private int A2047Servico_LinNegCod ;
      private int A155Servico_Codigo ;
      private int A1551Servico_Responsavel ;
      private int AV28GXV1 ;
      private int GXt_int1 ;
      private int AV30GXV2 ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A1557Servico_VincSigla ;
      private String A1061Servico_Tela ;
      private String A1072Servico_Atende ;
      private String A1429Servico_ObrigaValores ;
      private String A1436Servico_ObjetoControle ;
      private String A608Servico_Nome ;
      private String A605Servico_Sigla ;
      private bool returnInSub ;
      private bool n156Servico_Descricao ;
      private bool n633Servico_UO ;
      private bool A1077Servico_UORespExclusiva ;
      private bool n1077Servico_UORespExclusiva ;
      private bool n631Servico_Vinculado ;
      private bool n641Servico_VincDesc ;
      private bool n1557Servico_VincSigla ;
      private bool A889Servico_Terceriza ;
      private bool n889Servico_Terceriza ;
      private bool n1061Servico_Tela ;
      private bool n1072Servico_Atende ;
      private bool n1429Servico_ObrigaValores ;
      private bool n1530Servico_TipoHierarquia ;
      private bool n1534Servico_PercTmp ;
      private bool n1535Servico_PercPgm ;
      private bool n1536Servico_PercCnc ;
      private bool n1545Servico_Anterior ;
      private bool n1546Servico_Posterior ;
      private bool A632Servico_Ativo ;
      private bool A1635Servico_IsPublico ;
      private bool n2047Servico_LinNegCod ;
      private bool n2048Servico_LinNegDsc ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool A2131Servico_PausaSLA ;
      private String A156Servico_Descricao ;
      private String A641Servico_VincDesc ;
      private String A158ServicoGrupo_Descricao ;
      private String A2048Servico_LinNegDsc ;
      private String A2107Servico_Identificacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private String[] P00A42_A156Servico_Descricao ;
      private bool[] P00A42_n156Servico_Descricao ;
      private int[] P00A42_A157ServicoGrupo_Codigo ;
      private String[] P00A42_A158ServicoGrupo_Descricao ;
      private int[] P00A42_A633Servico_UO ;
      private bool[] P00A42_n633Servico_UO ;
      private bool[] P00A42_A1077Servico_UORespExclusiva ;
      private bool[] P00A42_n1077Servico_UORespExclusiva ;
      private int[] P00A42_A631Servico_Vinculado ;
      private bool[] P00A42_n631Servico_Vinculado ;
      private String[] P00A42_A641Servico_VincDesc ;
      private bool[] P00A42_n641Servico_VincDesc ;
      private String[] P00A42_A1557Servico_VincSigla ;
      private bool[] P00A42_n1557Servico_VincSigla ;
      private bool[] P00A42_A889Servico_Terceriza ;
      private bool[] P00A42_n889Servico_Terceriza ;
      private String[] P00A42_A1061Servico_Tela ;
      private bool[] P00A42_n1061Servico_Tela ;
      private String[] P00A42_A1072Servico_Atende ;
      private bool[] P00A42_n1072Servico_Atende ;
      private String[] P00A42_A1429Servico_ObrigaValores ;
      private bool[] P00A42_n1429Servico_ObrigaValores ;
      private String[] P00A42_A1436Servico_ObjetoControle ;
      private short[] P00A42_A1530Servico_TipoHierarquia ;
      private bool[] P00A42_n1530Servico_TipoHierarquia ;
      private short[] P00A42_A1534Servico_PercTmp ;
      private bool[] P00A42_n1534Servico_PercTmp ;
      private short[] P00A42_A1535Servico_PercPgm ;
      private bool[] P00A42_n1535Servico_PercPgm ;
      private short[] P00A42_A1536Servico_PercCnc ;
      private bool[] P00A42_n1536Servico_PercCnc ;
      private int[] P00A42_A1545Servico_Anterior ;
      private bool[] P00A42_n1545Servico_Anterior ;
      private int[] P00A42_A1546Servico_Posterior ;
      private bool[] P00A42_n1546Servico_Posterior ;
      private bool[] P00A42_A632Servico_Ativo ;
      private bool[] P00A42_A1635Servico_IsPublico ;
      private int[] P00A42_A2047Servico_LinNegCod ;
      private bool[] P00A42_n2047Servico_LinNegCod ;
      private String[] P00A42_A2048Servico_LinNegDsc ;
      private bool[] P00A42_n2048Servico_LinNegDsc ;
      private bool[] P00A42_A2092Servico_IsOrigemReferencia ;
      private bool[] P00A42_A2131Servico_PausaSLA ;
      private String[] P00A42_A608Servico_Nome ;
      private String[] P00A42_A605Servico_Sigla ;
      private int[] P00A42_A155Servico_Codigo ;
      private String[] P00A43_A156Servico_Descricao ;
      private bool[] P00A43_n156Servico_Descricao ;
      private int[] P00A43_A157ServicoGrupo_Codigo ;
      private String[] P00A43_A158ServicoGrupo_Descricao ;
      private int[] P00A43_A633Servico_UO ;
      private bool[] P00A43_n633Servico_UO ;
      private bool[] P00A43_A1077Servico_UORespExclusiva ;
      private bool[] P00A43_n1077Servico_UORespExclusiva ;
      private int[] P00A43_A631Servico_Vinculado ;
      private bool[] P00A43_n631Servico_Vinculado ;
      private String[] P00A43_A641Servico_VincDesc ;
      private bool[] P00A43_n641Servico_VincDesc ;
      private String[] P00A43_A1557Servico_VincSigla ;
      private bool[] P00A43_n1557Servico_VincSigla ;
      private bool[] P00A43_A889Servico_Terceriza ;
      private bool[] P00A43_n889Servico_Terceriza ;
      private String[] P00A43_A1061Servico_Tela ;
      private bool[] P00A43_n1061Servico_Tela ;
      private String[] P00A43_A1072Servico_Atende ;
      private bool[] P00A43_n1072Servico_Atende ;
      private String[] P00A43_A1429Servico_ObrigaValores ;
      private bool[] P00A43_n1429Servico_ObrigaValores ;
      private String[] P00A43_A1436Servico_ObjetoControle ;
      private short[] P00A43_A1530Servico_TipoHierarquia ;
      private bool[] P00A43_n1530Servico_TipoHierarquia ;
      private short[] P00A43_A1534Servico_PercTmp ;
      private bool[] P00A43_n1534Servico_PercTmp ;
      private short[] P00A43_A1535Servico_PercPgm ;
      private bool[] P00A43_n1535Servico_PercPgm ;
      private short[] P00A43_A1536Servico_PercCnc ;
      private bool[] P00A43_n1536Servico_PercCnc ;
      private int[] P00A43_A1545Servico_Anterior ;
      private bool[] P00A43_n1545Servico_Anterior ;
      private int[] P00A43_A1546Servico_Posterior ;
      private bool[] P00A43_n1546Servico_Posterior ;
      private bool[] P00A43_A632Servico_Ativo ;
      private bool[] P00A43_A1635Servico_IsPublico ;
      private int[] P00A43_A2047Servico_LinNegCod ;
      private bool[] P00A43_n2047Servico_LinNegCod ;
      private String[] P00A43_A2048Servico_LinNegDsc ;
      private bool[] P00A43_n2048Servico_LinNegDsc ;
      private bool[] P00A43_A2092Servico_IsOrigemReferencia ;
      private bool[] P00A43_A2131Servico_PausaSLA ;
      private String[] P00A43_A608Servico_Nome ;
      private String[] P00A43_A605Servico_Sigla ;
      private int[] P00A43_A155Servico_Codigo ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A42 ;
          prmP00A42 = new Object[] {
          new Object[] {"@AV16Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A43 ;
          prmP00A43 = new Object[] {
          new Object[] {"@AV16Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00A42", "SELECT T1.[Servico_Descricao], T1.[ServicoGrupo_Codigo], T2.[ServicoGrupo_Descricao], T1.[Servico_UO], T1.[Servico_UORespExclusiva], T1.[Servico_Vinculado] AS Servico_Vinculado, T3.[Servico_Descricao] AS Servico_VincDesc, T3.[Servico_Sigla] AS Servico_VincSigla, T1.[Servico_Terceriza], T1.[Servico_Tela], T1.[Servico_Atende], T1.[Servico_ObrigaValores], T1.[Servico_ObjetoControle], T1.[Servico_TipoHierarquia], T1.[Servico_PercTmp], T1.[Servico_PercPgm], T1.[Servico_PercCnc], T1.[Servico_Anterior], T1.[Servico_Posterior], T1.[Servico_Ativo], T1.[Servico_IsPublico], T1.[Servico_LinNegCod] AS Servico_LinNegCod, T4.[LinhaNegocio_Descricao] AS Servico_LinNegDsc, T1.[Servico_IsOrigemReferencia], T1.[Servico_PausaSLA], T1.[Servico_Nome], T1.[Servico_Sigla], T1.[Servico_Codigo] FROM ((([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[Servico_Vinculado]) LEFT JOIN [LinhaNegocio] T4 WITH (NOLOCK) ON T4.[LinhadeNegocio_Codigo] = T1.[Servico_LinNegCod]) WHERE T1.[Servico_Codigo] = @AV16Servico_Codigo ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A42,1,0,true,true )
             ,new CursorDef("P00A43", "SELECT T1.[Servico_Descricao], T1.[ServicoGrupo_Codigo], T2.[ServicoGrupo_Descricao], T1.[Servico_UO], T1.[Servico_UORespExclusiva], T1.[Servico_Vinculado] AS Servico_Vinculado, T3.[Servico_Descricao] AS Servico_VincDesc, T3.[Servico_Sigla] AS Servico_VincSigla, T1.[Servico_Terceriza], T1.[Servico_Tela], T1.[Servico_Atende], T1.[Servico_ObrigaValores], T1.[Servico_ObjetoControle], T1.[Servico_TipoHierarquia], T1.[Servico_PercTmp], T1.[Servico_PercPgm], T1.[Servico_PercCnc], T1.[Servico_Anterior], T1.[Servico_Posterior], T1.[Servico_Ativo], T1.[Servico_IsPublico], T1.[Servico_LinNegCod] AS Servico_LinNegCod, T4.[LinhaNegocio_Descricao] AS Servico_LinNegDsc, T1.[Servico_IsOrigemReferencia], T1.[Servico_PausaSLA], T1.[Servico_Nome], T1.[Servico_Sigla], T1.[Servico_Codigo] FROM ((([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[Servico_Vinculado]) LEFT JOIN [LinhaNegocio] T4 WITH (NOLOCK) ON T4.[LinhadeNegocio_Codigo] = T1.[Servico_LinNegCod]) WHERE T1.[Servico_Codigo] = @AV16Servico_Codigo ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A43,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 3) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 3) ;
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((short[]) buf[25])[0] = rslt.getShort(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((short[]) buf[27])[0] = rslt.getShort(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((short[]) buf[29])[0] = rslt.getShort(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((int[]) buf[33])[0] = rslt.getInt(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((bool[]) buf[35])[0] = rslt.getBool(20) ;
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((int[]) buf[37])[0] = rslt.getInt(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((bool[]) buf[41])[0] = rslt.getBool(24) ;
                ((bool[]) buf[42])[0] = rslt.getBool(25) ;
                ((String[]) buf[43])[0] = rslt.getString(26, 50) ;
                ((String[]) buf[44])[0] = rslt.getString(27, 15) ;
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 3) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 3) ;
                ((short[]) buf[23])[0] = rslt.getShort(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((short[]) buf[25])[0] = rslt.getShort(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((short[]) buf[27])[0] = rslt.getShort(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((short[]) buf[29])[0] = rslt.getShort(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((int[]) buf[33])[0] = rslt.getInt(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((bool[]) buf[35])[0] = rslt.getBool(20) ;
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((int[]) buf[37])[0] = rslt.getInt(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((bool[]) buf[41])[0] = rslt.getBool(24) ;
                ((bool[]) buf[42])[0] = rslt.getBool(25) ;
                ((String[]) buf[43])[0] = rslt.getString(26, 50) ;
                ((String[]) buf[44])[0] = rslt.getString(27, 15) ;
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
