/*
               File: PRC_DesativarRegistro
        Description: Desativar Registro
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:27.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_desativarregistro : GXProcedure
   {
      public prc_desativarregistro( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_desativarregistro( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_TabelaNome ,
                           int aP1_Codigo ,
                           int aP2_Codigo2 )
      {
         this.AV11TabelaNome = aP0_TabelaNome;
         this.AV9Codigo = aP1_Codigo;
         this.AV12Codigo2 = aP2_Codigo2;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_TabelaNome ,
                                 int aP1_Codigo ,
                                 int aP2_Codigo2 )
      {
         prc_desativarregistro objprc_desativarregistro;
         objprc_desativarregistro = new prc_desativarregistro();
         objprc_desativarregistro.AV11TabelaNome = aP0_TabelaNome;
         objprc_desativarregistro.AV9Codigo = aP1_Codigo;
         objprc_desativarregistro.AV12Codigo2 = aP2_Codigo2;
         objprc_desativarregistro.context.SetSubmitInitialConfig(context);
         objprc_desativarregistro.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_desativarregistro);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_desativarregistro)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV11TabelaNome, "FnD") == 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P005E2 */
            pr_default.execute(0, new Object[] {AV9Codigo});
            pr_default.close(0);
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoDados") ;
            /* End optimized UPDATE. */
         }
         else if ( StringUtil.StrCmp(AV11TabelaNome, "FnT") == 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P005E3 */
            pr_default.execute(1, new Object[] {AV9Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
            dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPF") ;
            /* End optimized UPDATE. */
         }
         else if ( StringUtil.StrCmp(AV11TabelaNome, "Tbl") == 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P005E4 */
            pr_default.execute(2, new Object[] {AV9Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
            dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
            /* End optimized UPDATE. */
         }
         else if ( StringUtil.StrCmp(AV11TabelaNome, "Atr") == 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P005E5 */
            pr_default.execute(3, new Object[] {AV9Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
            dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
            /* End optimized UPDATE. */
         }
         else if ( StringUtil.StrCmp(AV11TabelaNome, "FnTAt") == 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P005E6 */
            pr_default.execute(4, new Object[] {AV12Codigo2, AV9Codigo});
            pr_default.close(4);
            dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
            dsDefault.SmartCacheProvider.SetUpdated("FuncoesAPFAtributos") ;
            /* End optimized UPDATE. */
         }
         else if ( StringUtil.StrCmp(AV11TabelaNome, "FnTEv") == 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P005E7 */
            pr_default.execute(5, new Object[] {AV9Codigo});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
            /* End optimized UPDATE. */
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DesativarRegistro");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_desativarregistro__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Codigo ;
      private int AV12Codigo2 ;
      private String AV11TabelaNome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_desativarregistro__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP005E2 ;
          prmP005E2 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005E3 ;
          prmP005E3 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005E4 ;
          prmP005E4 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005E5 ;
          prmP005E5 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005E6 ;
          prmP005E6 = new Object[] {
          new Object[] {"@AV12Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP005E7 ;
          prmP005E7 = new Object[] {
          new Object[] {"@AV9Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P005E2", "UPDATE [FuncaoDados] SET [FuncaoDados_Ativo]='E'  WHERE [FuncaoDados_Codigo] = @AV9Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005E2)
             ,new CursorDef("P005E3", "UPDATE [FuncoesAPF] SET [FuncaoAPF_Ativo]='E'  WHERE [FuncaoAPF_Codigo] = @AV9Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005E3)
             ,new CursorDef("P005E4", "UPDATE [Tabela] SET [Tabela_Ativo]=CONVERT(BIT, 0)  WHERE [Tabela_Codigo] = @AV9Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005E4)
             ,new CursorDef("P005E5", "UPDATE [Atributos] SET [Atributos_Ativo]=CONVERT(BIT, 0)  WHERE [Atributos_Codigo] = @AV9Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005E5)
             ,new CursorDef("P005E6", "UPDATE [FuncoesAPFAtributos] SET [FuncaoAPFAtributos_Ativo]=CONVERT(BIT, 0)  WHERE [FuncaoAPF_Codigo] = @AV12Codigo2 and [FuncaoAPFAtributos_AtributosCod] = @AV9Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005E6)
             ,new CursorDef("P005E7", "UPDATE [FuncaoAPFEvidencia] SET [FuncaoAPFEvidencia_Ativo]=CONVERT(BIT, 0)  WHERE [FuncaoAPFEvidencia_Codigo] = @AV9Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP005E7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
