/*
               File: GetContratoServicosCustoWCFilterData
        Description: Get Contrato Servicos Custo WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/15/2020 23:16:57.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratoservicoscustowcfilterdata : GXProcedure
   {
      public getcontratoservicoscustowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratoservicoscustowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratoservicoscustowcfilterdata objgetcontratoservicoscustowcfilterdata;
         objgetcontratoservicoscustowcfilterdata = new getcontratoservicoscustowcfilterdata();
         objgetcontratoservicoscustowcfilterdata.AV18DDOName = aP0_DDOName;
         objgetcontratoservicoscustowcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetcontratoservicoscustowcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratoservicoscustowcfilterdata.AV22OptionsJson = "" ;
         objgetcontratoservicoscustowcfilterdata.AV25OptionsDescJson = "" ;
         objgetcontratoservicoscustowcfilterdata.AV27OptionIndexesJson = "" ;
         objgetcontratoservicoscustowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratoservicoscustowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratoservicoscustowcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratoservicoscustowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATOSERVICOSCUSTO_USRPESNOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSCUSTO_USRPESNOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("ContratoServicosCustoWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoServicosCustoWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("ContratoServicosCustoWCGridState"), "");
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_USRPESNOM") == 0 )
            {
               AV10TFContratoServicosCusto_UsrPesNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL") == 0 )
            {
               AV11TFContratoServicosCusto_UsrPesNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
            {
               AV12TFContratoServicosCusto_CstUntPrdNrm = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV13TFContratoServicosCusto_CstUntPrdNrm_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT") == 0 )
            {
               AV14TFContratoServicosCusto_CstUntPrdExt = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFContratoServicosCusto_CstUntPrdExt_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATOSERVICOSCUSTO_CNTSRVCOD") == 0 )
            {
               AV34ContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOSERVICOSCUSTO_USRPESNOMOPTIONS' Routine */
         AV10TFContratoServicosCusto_UsrPesNom = AV16SearchTxt;
         AV11TFContratoServicosCusto_UsrPesNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFContratoServicosCusto_UsrPesNom_Sel ,
                                              AV10TFContratoServicosCusto_UsrPesNom ,
                                              AV12TFContratoServicosCusto_CstUntPrdNrm ,
                                              AV13TFContratoServicosCusto_CstUntPrdNrm_To ,
                                              AV14TFContratoServicosCusto_CstUntPrdExt ,
                                              AV15TFContratoServicosCusto_CstUntPrdExt_To ,
                                              A1477ContratoServicosCusto_UsrPesNom ,
                                              A1474ContratoServicosCusto_CstUntPrdNrm ,
                                              A1475ContratoServicosCusto_CstUntPrdExt ,
                                              AV34ContratoServicosCusto_CntSrvCod ,
                                              A1471ContratoServicosCusto_CntSrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratoServicosCusto_UsrPesNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratoServicosCusto_UsrPesNom), 100, "%");
         /* Using cursor P00K02 */
         pr_default.execute(0, new Object[] {AV34ContratoServicosCusto_CntSrvCod, lV10TFContratoServicosCusto_UsrPesNom, AV11TFContratoServicosCusto_UsrPesNom_Sel, AV12TFContratoServicosCusto_CstUntPrdNrm, AV13TFContratoServicosCusto_CstUntPrdNrm_To, AV14TFContratoServicosCusto_CstUntPrdExt, AV15TFContratoServicosCusto_CstUntPrdExt_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKK02 = false;
            A1472ContratoServicosCusto_UsuarioCod = P00K02_A1472ContratoServicosCusto_UsuarioCod[0];
            A1476ContratoServicosCusto_UsrPesCod = P00K02_A1476ContratoServicosCusto_UsrPesCod[0];
            n1476ContratoServicosCusto_UsrPesCod = P00K02_n1476ContratoServicosCusto_UsrPesCod[0];
            A1471ContratoServicosCusto_CntSrvCod = P00K02_A1471ContratoServicosCusto_CntSrvCod[0];
            A1477ContratoServicosCusto_UsrPesNom = P00K02_A1477ContratoServicosCusto_UsrPesNom[0];
            n1477ContratoServicosCusto_UsrPesNom = P00K02_n1477ContratoServicosCusto_UsrPesNom[0];
            A1475ContratoServicosCusto_CstUntPrdExt = P00K02_A1475ContratoServicosCusto_CstUntPrdExt[0];
            n1475ContratoServicosCusto_CstUntPrdExt = P00K02_n1475ContratoServicosCusto_CstUntPrdExt[0];
            A1474ContratoServicosCusto_CstUntPrdNrm = P00K02_A1474ContratoServicosCusto_CstUntPrdNrm[0];
            A1473ContratoServicosCusto_Codigo = P00K02_A1473ContratoServicosCusto_Codigo[0];
            A1476ContratoServicosCusto_UsrPesCod = P00K02_A1476ContratoServicosCusto_UsrPesCod[0];
            n1476ContratoServicosCusto_UsrPesCod = P00K02_n1476ContratoServicosCusto_UsrPesCod[0];
            A1477ContratoServicosCusto_UsrPesNom = P00K02_A1477ContratoServicosCusto_UsrPesNom[0];
            n1477ContratoServicosCusto_UsrPesNom = P00K02_n1477ContratoServicosCusto_UsrPesNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00K02_A1471ContratoServicosCusto_CntSrvCod[0] == A1471ContratoServicosCusto_CntSrvCod ) && ( StringUtil.StrCmp(P00K02_A1477ContratoServicosCusto_UsrPesNom[0], A1477ContratoServicosCusto_UsrPesNom) == 0 ) )
            {
               BRKK02 = false;
               A1472ContratoServicosCusto_UsuarioCod = P00K02_A1472ContratoServicosCusto_UsuarioCod[0];
               A1476ContratoServicosCusto_UsrPesCod = P00K02_A1476ContratoServicosCusto_UsrPesCod[0];
               n1476ContratoServicosCusto_UsrPesCod = P00K02_n1476ContratoServicosCusto_UsrPesCod[0];
               A1473ContratoServicosCusto_Codigo = P00K02_A1473ContratoServicosCusto_Codigo[0];
               A1476ContratoServicosCusto_UsrPesCod = P00K02_A1476ContratoServicosCusto_UsrPesCod[0];
               n1476ContratoServicosCusto_UsrPesCod = P00K02_n1476ContratoServicosCusto_UsrPesCod[0];
               AV28count = (long)(AV28count+1);
               BRKK02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1477ContratoServicosCusto_UsrPesNom)) )
            {
               AV20Option = A1477ContratoServicosCusto_UsrPesNom;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK02 )
            {
               BRKK02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoServicosCusto_UsrPesNom = "";
         AV11TFContratoServicosCusto_UsrPesNom_Sel = "";
         scmdbuf = "";
         lV10TFContratoServicosCusto_UsrPesNom = "";
         A1477ContratoServicosCusto_UsrPesNom = "";
         P00K02_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         P00K02_A1476ContratoServicosCusto_UsrPesCod = new int[1] ;
         P00K02_n1476ContratoServicosCusto_UsrPesCod = new bool[] {false} ;
         P00K02_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         P00K02_A1477ContratoServicosCusto_UsrPesNom = new String[] {""} ;
         P00K02_n1477ContratoServicosCusto_UsrPesNom = new bool[] {false} ;
         P00K02_A1475ContratoServicosCusto_CstUntPrdExt = new decimal[1] ;
         P00K02_n1475ContratoServicosCusto_CstUntPrdExt = new bool[] {false} ;
         P00K02_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         P00K02_A1473ContratoServicosCusto_Codigo = new int[1] ;
         AV20Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratoservicoscustowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00K02_A1472ContratoServicosCusto_UsuarioCod, P00K02_A1476ContratoServicosCusto_UsrPesCod, P00K02_n1476ContratoServicosCusto_UsrPesCod, P00K02_A1471ContratoServicosCusto_CntSrvCod, P00K02_A1477ContratoServicosCusto_UsrPesNom, P00K02_n1477ContratoServicosCusto_UsrPesNom, P00K02_A1475ContratoServicosCusto_CstUntPrdExt, P00K02_n1475ContratoServicosCusto_CstUntPrdExt, P00K02_A1474ContratoServicosCusto_CstUntPrdNrm, P00K02_A1473ContratoServicosCusto_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV37GXV1 ;
      private int AV34ContratoServicosCusto_CntSrvCod ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int A1472ContratoServicosCusto_UsuarioCod ;
      private int A1476ContratoServicosCusto_UsrPesCod ;
      private int A1473ContratoServicosCusto_Codigo ;
      private long AV28count ;
      private decimal AV12TFContratoServicosCusto_CstUntPrdNrm ;
      private decimal AV13TFContratoServicosCusto_CstUntPrdNrm_To ;
      private decimal AV14TFContratoServicosCusto_CstUntPrdExt ;
      private decimal AV15TFContratoServicosCusto_CstUntPrdExt_To ;
      private decimal A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal A1475ContratoServicosCusto_CstUntPrdExt ;
      private String AV10TFContratoServicosCusto_UsrPesNom ;
      private String AV11TFContratoServicosCusto_UsrPesNom_Sel ;
      private String scmdbuf ;
      private String lV10TFContratoServicosCusto_UsrPesNom ;
      private String A1477ContratoServicosCusto_UsrPesNom ;
      private bool returnInSub ;
      private bool BRKK02 ;
      private bool n1476ContratoServicosCusto_UsrPesCod ;
      private bool n1477ContratoServicosCusto_UsrPesNom ;
      private bool n1475ContratoServicosCusto_CstUntPrdExt ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00K02_A1472ContratoServicosCusto_UsuarioCod ;
      private int[] P00K02_A1476ContratoServicosCusto_UsrPesCod ;
      private bool[] P00K02_n1476ContratoServicosCusto_UsrPesCod ;
      private int[] P00K02_A1471ContratoServicosCusto_CntSrvCod ;
      private String[] P00K02_A1477ContratoServicosCusto_UsrPesNom ;
      private bool[] P00K02_n1477ContratoServicosCusto_UsrPesNom ;
      private decimal[] P00K02_A1475ContratoServicosCusto_CstUntPrdExt ;
      private bool[] P00K02_n1475ContratoServicosCusto_CstUntPrdExt ;
      private decimal[] P00K02_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private int[] P00K02_A1473ContratoServicosCusto_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getcontratoservicoscustowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00K02( IGxContext context ,
                                             String AV11TFContratoServicosCusto_UsrPesNom_Sel ,
                                             String AV10TFContratoServicosCusto_UsrPesNom ,
                                             decimal AV12TFContratoServicosCusto_CstUntPrdNrm ,
                                             decimal AV13TFContratoServicosCusto_CstUntPrdNrm_To ,
                                             decimal AV14TFContratoServicosCusto_CstUntPrdExt ,
                                             decimal AV15TFContratoServicosCusto_CstUntPrdExt_To ,
                                             String A1477ContratoServicosCusto_UsrPesNom ,
                                             decimal A1474ContratoServicosCusto_CstUntPrdNrm ,
                                             decimal A1475ContratoServicosCusto_CstUntPrdExt ,
                                             int AV34ContratoServicosCusto_CntSrvCod ,
                                             int A1471ContratoServicosCusto_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosCusto_UsuarioCod] AS ContratoServicosCusto_UsuarioCod, T2.[Usuario_PessoaCod] AS ContratoServicosCusto_UsrPesCod, T1.[ContratoServicosCusto_CntSrvCod], T3.[Pessoa_Nome] AS ContratoServicosCusto_UsrPesNom, T1.[ContratoServicosCusto_CstUntPrdExt], T1.[ContratoServicosCusto_CstUntPrdNrm], T1.[ContratoServicosCusto_Codigo] FROM (([ContratoServicosCusto] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratoServicosCusto_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosCusto_CntSrvCod] = @AV34ContratoServicosCusto_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoServicosCusto_UsrPesNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratoServicosCusto_UsrPesNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV10TFContratoServicosCusto_UsrPesNom)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratoServicosCusto_UsrPesNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV11TFContratoServicosCusto_UsrPesNom_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV12TFContratoServicosCusto_CstUntPrdNrm) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdNrm] >= @AV12TFContratoServicosCusto_CstUntPrdNrm)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13TFContratoServicosCusto_CstUntPrdNrm_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdNrm] <= @AV13TFContratoServicosCusto_CstUntPrdNrm_To)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV14TFContratoServicosCusto_CstUntPrdExt) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdExt] >= @AV14TFContratoServicosCusto_CstUntPrdExt)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV15TFContratoServicosCusto_CstUntPrdExt_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdExt] <= @AV15TFContratoServicosCusto_CstUntPrdExt_To)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosCusto_CntSrvCod], T3.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00K02(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00K02 ;
          prmP00K02 = new Object[] {
          new Object[] {"@AV34ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratoServicosCusto_UsrPesNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratoServicosCusto_UsrPesNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV12TFContratoServicosCusto_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV13TFContratoServicosCusto_CstUntPrdNrm_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV14TFContratoServicosCusto_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV15TFContratoServicosCusto_CstUntPrdExt_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00K02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K02,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratoservicoscustowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratoservicoscustowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratoservicoscustowcfilterdata") )
          {
             return  ;
          }
          getcontratoservicoscustowcfilterdata worker = new getcontratoservicoscustowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
