/*
               File: REL_RedmineServicos
        Description: Verifica��o de Servicos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:17:23.87
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_redmineservicos : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_redmineservicos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_redmineservicos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         arel_redmineservicos objarel_redmineservicos;
         objarel_redmineservicos = new arel_redmineservicos();
         objarel_redmineservicos.context.SetSubmitInitialConfig(context);
         objarel_redmineservicos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_redmineservicos);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_redmineservicos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV21Titulo = "Servi�os no Redmine vs Meetrika";
            AV15SDT_Issues.FromXml(AV22websession.Get("Issues"), "");
            AV16SDT_Parametros.FromXml(AV22websession.Get("Parametros"), "");
            AV22websession.Remove("Issues");
            AV22websession.Remove("Parametros");
            AV9Area = (int)(AV16SDT_Parametros.gxTpr_Numeric.GetNumeric(1));
            AV11Contrato = (int)(AV16SDT_Parametros.gxTpr_Numeric.GetNumeric(2));
            AV10Campo = StringUtil.Lower( ((String)AV16SDT_Parametros.gxTpr_Character.Item(1)));
            AV31GXV1 = 1;
            while ( AV31GXV1 <= AV15SDT_Issues.gxTpr_Issues.Count )
            {
               AV13Issue = ((SdtSDT_RedmineIssues_issue)AV15SDT_Issues.gxTpr_Issues.Item(AV31GXV1));
               /* Execute user subroutine: 'SIGLA' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               AV17Servico.gxTpr_Codigo = AV13Issue.gxTpr_Id;
               AV17Servico.gxTpr_Descricao = AV19Sigla;
               AV18Servicos.Add(AV17Servico, 0);
               if ( AV20Siglas.IndexOf(AV19Sigla) == 0 )
               {
                  AV20Siglas.Add(AV19Sigla, 0);
               }
               AV31GXV1 = (int)(AV31GXV1+1);
            }
            AV32GXV2 = 1;
            while ( AV32GXV2 <= AV20Siglas.Count )
            {
               AV19Sigla = AV20Siglas.GetString(AV32GXV2);
               AV33GXLvl37 = 0;
               /* Using cursor P00TZ2 */
               pr_default.execute(0, new Object[] {AV11Contrato, AV19Sigla});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A155Servico_Codigo = P00TZ2_A155Servico_Codigo[0];
                  A605Servico_Sigla = P00TZ2_A605Servico_Sigla[0];
                  A74Contrato_Codigo = P00TZ2_A74Contrato_Codigo[0];
                  A160ContratoServicos_Codigo = P00TZ2_A160ContratoServicos_Codigo[0];
                  A605Servico_Sigla = P00TZ2_A605Servico_Sigla[0];
                  AV33GXLvl37 = 1;
                  AV24i = 0;
                  AV34GXV3 = 1;
                  while ( AV34GXV3 <= AV18Servicos.Count )
                  {
                     AV25Item = ((SdtSDT_Codigos)AV18Servicos.Item(AV34GXV3));
                     AV24i = (short)(AV24i+1);
                     if ( StringUtil.StrCmp(AV25Item.gxTpr_Descricao, AV19Sigla) == 0 )
                     {
                        ((SdtSDT_Codigos)AV18Servicos.Item(AV24i)).gxTpr_Descricao = StringUtil.Trim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0));
                     }
                     AV34GXV3 = (int)(AV34GXV3+1);
                  }
                  pr_default.readNext(0);
               }
               pr_default.close(0);
               if ( AV33GXLvl37 == 0 )
               {
                  AV23Linha = AV19Sigla + " n�o identificado";
                  HTZ0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV23Linha, "")), 42, Gx_line+0, 303, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  AV12Erros = (short)(AV12Erros+1);
               }
               AV32GXV2 = (int)(AV32GXV2+1);
            }
            AV22websession.Set("Servicos", AV18Servicos.ToXml(false, true, "SDT_CodigosCollection", "GxEv3Up14_Meetrika"));
            if ( (0==AV12Erros) )
            {
               AV23Linha = "Tudo OK";
               HTZ0( false, 15) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV23Linha, "")), 42, Gx_line+0, 303, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+15);
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HTZ0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'SIGLA' Routine */
         if ( StringUtil.StrCmp(AV10Campo, "project") == 0 )
         {
            AV19Sigla = AV13Issue.gxTpr_Project.gxTpr_Name;
         }
         else if ( StringUtil.StrCmp(AV10Campo, "status") == 0 )
         {
            AV19Sigla = AV13Issue.gxTpr_Status.gxTpr_Name;
         }
         else if ( StringUtil.StrCmp(AV10Campo, "subject") == 0 )
         {
            AV19Sigla = AV13Issue.gxTpr_Subject;
         }
         else if ( StringUtil.StrCmp(AV10Campo, "tracker") == 0 )
         {
            AV19Sigla = AV13Issue.gxTpr_Tracker.gxTpr_Name;
         }
         else if ( StringUtil.StrCmp(AV10Campo, "description") == 0 )
         {
            AV19Sigla = AV13Issue.gxTpr_Description;
         }
         else
         {
            AV35GXV4 = 1;
            while ( AV35GXV4 <= AV13Issue.gxTpr_Custom_fields.gxTpr_Custom_fields.Count )
            {
               AV8Field = ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)AV13Issue.gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV35GXV4));
               AV14Nome = StringUtil.Trim( AV8Field.gxTpr_Name);
               if ( StringUtil.StrCmp(AV14Nome, AV10Campo) == 0 )
               {
                  AV19Sigla = StringUtil.Lower( AV8Field.gxTpr_Value);
               }
               AV35GXV4 = (int)(AV35GXV4+1);
            }
         }
      }

      protected void HTZ0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 667, Gx_line+5, 716, Gx_line+20, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 725, Gx_line+5, 818, Gx_line+20, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV21Titulo, "")), 204, Gx_line+0, 622, Gx_line+26, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+33);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV21Titulo = "";
         AV15SDT_Issues = new SdtSDT_RedmineIssues(context);
         AV22websession = context.GetSession();
         AV16SDT_Parametros = new SdtSDT_Parametros(context);
         AV10Campo = "";
         AV13Issue = new SdtSDT_RedmineIssues_issue(context);
         AV17Servico = new SdtSDT_Codigos(context);
         AV19Sigla = "";
         AV18Servicos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs");
         AV20Siglas = new GxSimpleCollection();
         scmdbuf = "";
         P00TZ2_A155Servico_Codigo = new int[1] ;
         P00TZ2_A605Servico_Sigla = new String[] {""} ;
         P00TZ2_A74Contrato_Codigo = new int[1] ;
         P00TZ2_A160ContratoServicos_Codigo = new int[1] ;
         A605Servico_Sigla = "";
         AV25Item = new SdtSDT_Codigos(context);
         AV23Linha = "";
         AV8Field = new SdtSDT_RedmineIssues_issue_custom_fields_custom_field(context);
         AV14Nome = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_redmineservicos__default(),
            new Object[][] {
                new Object[] {
               P00TZ2_A155Servico_Codigo, P00TZ2_A605Servico_Sigla, P00TZ2_A74Contrato_Codigo, P00TZ2_A160ContratoServicos_Codigo
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV33GXLvl37 ;
      private short AV24i ;
      private short AV12Erros ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV9Area ;
      private int AV11Contrato ;
      private int AV31GXV1 ;
      private int AV32GXV2 ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV34GXV3 ;
      private int Gx_OldLine ;
      private int AV35GXV4 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV21Titulo ;
      private String AV10Campo ;
      private String AV19Sigla ;
      private String scmdbuf ;
      private String A605Servico_Sigla ;
      private String AV23Linha ;
      private String AV14Nome ;
      private String Gx_time ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private IGxSession AV22websession ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Siglas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00TZ2_A155Servico_Codigo ;
      private String[] P00TZ2_A605Servico_Sigla ;
      private int[] P00TZ2_A74Contrato_Codigo ;
      private int[] P00TZ2_A160ContratoServicos_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV18Servicos ;
      private SdtSDT_RedmineIssues AV15SDT_Issues ;
      private SdtSDT_RedmineIssues_issue AV13Issue ;
      private SdtSDT_RedmineIssues_issue_custom_fields_custom_field AV8Field ;
      private SdtSDT_Parametros AV16SDT_Parametros ;
      private SdtSDT_Codigos AV17Servico ;
      private SdtSDT_Codigos AV25Item ;
   }

   public class arel_redmineservicos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TZ2 ;
          prmP00TZ2 = new Object[] {
          new Object[] {"@AV11Contrato",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19Sigla",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TZ2", "SELECT T1.[Servico_Codigo], T2.[Servico_Sigla], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV11Contrato) AND (T2.[Servico_Sigla] = @AV19Sigla) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TZ2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
