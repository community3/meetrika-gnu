/*
               File: ContratoServicosArtefatos
        Description: Artefato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:29:15.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosartefatos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ARTEFATOS_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAARTEFATOS_CODIGO4E194( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A1749Artefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A1749Artefatos_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicos_Codigo), "ZZZZZ9")));
               AV8Artefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Artefatos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vARTEFATOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Artefatos_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynArtefatos_Codigo.Name = "ARTEFATOS_CODIGO";
         dynArtefatos_Codigo.WebTags = "";
         chkContratoServicosArtefato_Obrigatorio.Name = "CONTRATOSERVICOSARTEFATO_OBRIGATORIO";
         chkContratoServicosArtefato_Obrigatorio.WebTags = "";
         chkContratoServicosArtefato_Obrigatorio.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosArtefato_Obrigatorio_Internalname, "TitleCaption", chkContratoServicosArtefato_Obrigatorio.Caption);
         chkContratoServicosArtefato_Obrigatorio.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Artefato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynArtefatos_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicosartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicos_Codigo ,
                           int aP2_Artefatos_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicos_Codigo = aP1_ContratoServicos_Codigo;
         this.AV8Artefatos_Codigo = aP2_Artefatos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynArtefatos_Codigo = new GXCombobox();
         chkContratoServicosArtefato_Obrigatorio = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynArtefatos_Codigo.ItemCount > 0 )
         {
            A1749Artefatos_Codigo = (int)(NumberUtil.Val( dynArtefatos_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4E194( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4E194e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4E194( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4E194( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4E194e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_24_4E194( true) ;
         }
         return  ;
      }

      protected void wb_table3_24_4E194e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_ContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4E194e( true) ;
         }
         else
         {
            wb_table1_2_4E194e( false) ;
         }
      }

      protected void wb_table3_24_4E194( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_24_4E194e( true) ;
         }
         else
         {
            wb_table3_24_4E194e( false) ;
         }
      }

      protected void wb_table2_5_4E194( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_4E194( true) ;
         }
         return  ;
      }

      protected void wb_table4_11_4E194e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4E194e( true) ;
         }
         else
         {
            wb_table2_5_4E194e( false) ;
         }
      }

      protected void wb_table4_11_4E194( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockartefatos_codigo_Internalname, "Descri��o", "", "", lblTextblockartefatos_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynArtefatos_Codigo, dynArtefatos_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)), 1, dynArtefatos_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynArtefatos_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_ContratoServicosArtefatos.htm");
            dynArtefatos_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynArtefatos_Codigo_Internalname, "Values", (String)(dynArtefatos_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosartefato_obrigatorio_Internalname, "Obrigat�rio", "", "", lblTextblockcontratoservicosartefato_obrigatorio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoServicosArtefato_Obrigatorio_Internalname, StringUtil.BoolToStr( A1768ContratoServicosArtefato_Obrigatorio), "", "", 1, chkContratoServicosArtefato_Obrigatorio.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(21, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_4E194e( true) ;
         }
         else
         {
            wb_table4_11_4E194e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114E2 */
         E114E2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynArtefatos_Codigo.CurrentValue = cgiGet( dynArtefatos_Codigo_Internalname);
               A1749Artefatos_Codigo = (int)(NumberUtil.Val( cgiGet( dynArtefatos_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               A1768ContratoServicosArtefato_Obrigatorio = StringUtil.StrToBool( cgiGet( chkContratoServicosArtefato_Obrigatorio_Internalname));
               n1768ContratoServicosArtefato_Obrigatorio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1768ContratoServicosArtefato_Obrigatorio", A1768ContratoServicosArtefato_Obrigatorio);
               n1768ContratoServicosArtefato_Obrigatorio = ((false==A1768ContratoServicosArtefato_Obrigatorio) ? true : false);
               /* Read saved values. */
               Z160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z160ContratoServicos_Codigo"), ",", "."));
               Z1749Artefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1749Artefatos_Codigo"), ",", "."));
               Z1768ContratoServicosArtefato_Obrigatorio = StringUtil.StrToBool( cgiGet( "Z1768ContratoServicosArtefato_Obrigatorio"));
               n1768ContratoServicosArtefato_Obrigatorio = ((false==A1768ContratoServicosArtefato_Obrigatorio) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOS_CODIGO"), ",", "."));
               A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_CODIGO"), ",", "."));
               AV8Artefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vARTEFATOS_CODIGO"), ",", "."));
               A1751Artefatos_Descricao = cgiGet( "ARTEFATOS_DESCRICAO");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosArtefatos";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicosartefatos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoservicosartefatos:[SecurityCheckFailed value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A1749Artefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode194 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode194;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound194 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4E0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "ARTEFATOS_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = dynArtefatos_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114E2 */
                           E114E2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124E2 */
                           E124E2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124E2 */
            E124E2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4E194( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4E194( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4E0( )
      {
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4E194( ) ;
            }
            else
            {
               CheckExtendedTable4E194( ) ;
               CloseExtendedTableCursors4E194( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4E0( )
      {
      }

      protected void E114E2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E124E2( )
      {
         /* After Trn Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4E194( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1768ContratoServicosArtefato_Obrigatorio = T004E3_A1768ContratoServicosArtefato_Obrigatorio[0];
            }
            else
            {
               Z1768ContratoServicosArtefato_Obrigatorio = A1768ContratoServicosArtefato_Obrigatorio;
            }
         }
         if ( GX_JID == -7 )
         {
            Z1768ContratoServicosArtefato_Obrigatorio = A1768ContratoServicosArtefato_Obrigatorio;
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
            Z1751Artefatos_Descricao = A1751Artefatos_Descricao;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAARTEFATOS_CODIGO_html4E194( ) ;
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicos_Codigo) )
         {
            A160ContratoServicos_Codigo = AV7ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         /* Using cursor T004E4 */
         pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( ! (0==AV8Artefatos_Codigo) )
         {
            A1749Artefatos_Codigo = AV8Artefatos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         }
         if ( ! (0==AV8Artefatos_Codigo) )
         {
            dynArtefatos_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynArtefatos_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynArtefatos_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynArtefatos_Codigo.Enabled), 5, 0)));
         }
         if ( ! (0==AV8Artefatos_Codigo) )
         {
            dynArtefatos_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynArtefatos_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T004E5 */
            pr_default.execute(3, new Object[] {A1749Artefatos_Codigo});
            A1751Artefatos_Descricao = T004E5_A1751Artefatos_Descricao[0];
            pr_default.close(3);
         }
      }

      protected void Load4E194( )
      {
         /* Using cursor T004E6 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound194 = 1;
            A1751Artefatos_Descricao = T004E6_A1751Artefatos_Descricao[0];
            A1768ContratoServicosArtefato_Obrigatorio = T004E6_A1768ContratoServicosArtefato_Obrigatorio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1768ContratoServicosArtefato_Obrigatorio", A1768ContratoServicosArtefato_Obrigatorio);
            n1768ContratoServicosArtefato_Obrigatorio = T004E6_n1768ContratoServicosArtefato_Obrigatorio[0];
            ZM4E194( -7) ;
         }
         pr_default.close(4);
         OnLoadActions4E194( ) ;
      }

      protected void OnLoadActions4E194( )
      {
      }

      protected void CheckExtendedTable4E194( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T004E5 */
         pr_default.execute(3, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynArtefatos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1751Artefatos_Descricao = T004E5_A1751Artefatos_Descricao[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4E194( )
      {
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A1749Artefatos_Codigo )
      {
         /* Using cursor T004E7 */
         pr_default.execute(5, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynArtefatos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1751Artefatos_Descricao = T004E7_A1751Artefatos_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1751Artefatos_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void GetKey4E194( )
      {
         /* Using cursor T004E8 */
         pr_default.execute(6, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound194 = 1;
         }
         else
         {
            RcdFound194 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004E3 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4E194( 7) ;
            RcdFound194 = 1;
            A1768ContratoServicosArtefato_Obrigatorio = T004E3_A1768ContratoServicosArtefato_Obrigatorio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1768ContratoServicosArtefato_Obrigatorio", A1768ContratoServicosArtefato_Obrigatorio);
            n1768ContratoServicosArtefato_Obrigatorio = T004E3_n1768ContratoServicosArtefato_Obrigatorio[0];
            A160ContratoServicos_Codigo = T004E3_A160ContratoServicos_Codigo[0];
            A1749Artefatos_Codigo = T004E3_A1749Artefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
            sMode194 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4E194( ) ;
            if ( AnyError == 1 )
            {
               RcdFound194 = 0;
               InitializeNonKey4E194( ) ;
            }
            Gx_mode = sMode194;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound194 = 0;
            InitializeNonKey4E194( ) ;
            sMode194 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode194;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4E194( ) ;
         if ( RcdFound194 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound194 = 0;
         /* Using cursor T004E9 */
         pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T004E9_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T004E9_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T004E9_A1749Artefatos_Codigo[0] < A1749Artefatos_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T004E9_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T004E9_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T004E9_A1749Artefatos_Codigo[0] > A1749Artefatos_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T004E9_A160ContratoServicos_Codigo[0];
               A1749Artefatos_Codigo = T004E9_A1749Artefatos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               RcdFound194 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound194 = 0;
         /* Using cursor T004E10 */
         pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T004E10_A160ContratoServicos_Codigo[0] > A160ContratoServicos_Codigo ) || ( T004E10_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T004E10_A1749Artefatos_Codigo[0] > A1749Artefatos_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T004E10_A160ContratoServicos_Codigo[0] < A160ContratoServicos_Codigo ) || ( T004E10_A160ContratoServicos_Codigo[0] == A160ContratoServicos_Codigo ) && ( T004E10_A1749Artefatos_Codigo[0] < A1749Artefatos_Codigo ) ) )
            {
               A160ContratoServicos_Codigo = T004E10_A160ContratoServicos_Codigo[0];
               A1749Artefatos_Codigo = T004E10_A1749Artefatos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               RcdFound194 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4E194( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynArtefatos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4E194( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound194 == 1 )
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ARTEFATOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = dynArtefatos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynArtefatos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4E194( ) ;
                  GX_FocusControl = dynArtefatos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
               {
                  /* Insert record */
                  GX_FocusControl = dynArtefatos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4E194( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ARTEFATOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = dynArtefatos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynArtefatos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4E194( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) )
         {
            A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ARTEFATOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynArtefatos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynArtefatos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4E194( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004E2 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosArtefatos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1768ContratoServicosArtefato_Obrigatorio != T004E2_A1768ContratoServicosArtefato_Obrigatorio[0] ) )
            {
               if ( Z1768ContratoServicosArtefato_Obrigatorio != T004E2_A1768ContratoServicosArtefato_Obrigatorio[0] )
               {
                  GXUtil.WriteLog("contratoservicosartefatos:[seudo value changed for attri]"+"ContratoServicosArtefato_Obrigatorio");
                  GXUtil.WriteLogRaw("Old: ",Z1768ContratoServicosArtefato_Obrigatorio);
                  GXUtil.WriteLogRaw("Current: ",T004E2_A1768ContratoServicosArtefato_Obrigatorio[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosArtefatos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4E194( )
      {
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4E194( 0) ;
            CheckOptimisticConcurrency4E194( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4E194( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4E194( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004E11 */
                     pr_default.execute(9, new Object[] {n1768ContratoServicosArtefato_Obrigatorio, A1768ContratoServicosArtefato_Obrigatorio, A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
                     if ( (pr_default.getStatus(9) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4E0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4E194( ) ;
            }
            EndLevel4E194( ) ;
         }
         CloseExtendedTableCursors4E194( ) ;
      }

      protected void Update4E194( )
      {
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4E194( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4E194( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4E194( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004E12 */
                     pr_default.execute(10, new Object[] {n1768ContratoServicosArtefato_Obrigatorio, A1768ContratoServicosArtefato_Obrigatorio, A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosArtefatos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4E194( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4E194( ) ;
         }
         CloseExtendedTableCursors4E194( ) ;
      }

      protected void DeferredUpdate4E194( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4E194( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4E194( ) ;
            AfterConfirm4E194( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4E194( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004E13 */
                  pr_default.execute(11, new Object[] {A160ContratoServicos_Codigo, A1749Artefatos_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosArtefatos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode194 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4E194( ) ;
         Gx_mode = sMode194;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4E194( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T004E14 */
            pr_default.execute(12, new Object[] {A1749Artefatos_Codigo});
            A1751Artefatos_Descricao = T004E14_A1751Artefatos_Descricao[0];
            pr_default.close(12);
         }
      }

      protected void EndLevel4E194( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4E194( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores( "ContratoServicosArtefatos");
            if ( AnyError == 0 )
            {
               ConfirmValues4E0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores( "ContratoServicosArtefatos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4E194( )
      {
         /* Scan By routine */
         /* Using cursor T004E15 */
         pr_default.execute(13);
         RcdFound194 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound194 = 1;
            A160ContratoServicos_Codigo = T004E15_A160ContratoServicos_Codigo[0];
            A1749Artefatos_Codigo = T004E15_A1749Artefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4E194( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound194 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound194 = 1;
            A160ContratoServicos_Codigo = T004E15_A160ContratoServicos_Codigo[0];
            A1749Artefatos_Codigo = T004E15_A1749Artefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4E194( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm4E194( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4E194( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4E194( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4E194( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4E194( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4E194( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4E194( )
      {
         dynArtefatos_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynArtefatos_Codigo.Enabled), 5, 0)));
         chkContratoServicosArtefato_Obrigatorio.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoServicosArtefato_Obrigatorio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoServicosArtefato_Obrigatorio.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4E0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120291635");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicos_Codigo) + "," + UrlEncode("" +AV8Artefatos_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1749Artefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1768ContratoServicosArtefato_Obrigatorio", Z1768ContratoServicosArtefato_Obrigatorio);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Artefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_DESCRICAO", A1751Artefatos_Descricao);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vARTEFATOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Artefatos_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosArtefatos";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosartefatos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoservicosartefatos:[SendSecurityCheck value for]"+"ContratoServicos_Codigo:"+context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicosartefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicos_Codigo) + "," + UrlEncode("" +AV8Artefatos_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Artefato" ;
      }

      protected void InitializeNonKey4E194( )
      {
         A1751Artefatos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
         A1768ContratoServicosArtefato_Obrigatorio = false;
         n1768ContratoServicosArtefato_Obrigatorio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1768ContratoServicosArtefato_Obrigatorio", A1768ContratoServicosArtefato_Obrigatorio);
         n1768ContratoServicosArtefato_Obrigatorio = ((false==A1768ContratoServicosArtefato_Obrigatorio) ? true : false);
         Z1768ContratoServicosArtefato_Obrigatorio = false;
      }

      protected void InitAll4E194( )
      {
         A160ContratoServicos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         A1749Artefatos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         InitializeNonKey4E194( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120291638");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicosartefatos.js", "?20203120291638");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockartefatos_codigo_Internalname = "TEXTBLOCKARTEFATOS_CODIGO";
         dynArtefatos_Codigo_Internalname = "ARTEFATOS_CODIGO";
         lblTextblockcontratoservicosartefato_obrigatorio_Internalname = "TEXTBLOCKCONTRATOSERVICOSARTEFATO_OBRIGATORIO";
         chkContratoServicosArtefato_Obrigatorio_Internalname = "CONTRATOSERVICOSARTEFATO_OBRIGATORIO";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Artefato";
         chkContratoServicosArtefato_Obrigatorio.Enabled = 1;
         dynArtefatos_Codigo_Jsonclick = "";
         dynArtefatos_Codigo.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         chkContratoServicosArtefato_Obrigatorio.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAARTEFATOS_CODIGO4E194( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAARTEFATOS_CODIGO_data4E194( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAARTEFATOS_CODIGO_html4E194( )
      {
         int gxdynajaxvalue ;
         GXDLAARTEFATOS_CODIGO_data4E194( ) ;
         gxdynajaxindex = 1;
         dynArtefatos_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynArtefatos_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAARTEFATOS_CODIGO_data4E194( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor T004E16 */
         pr_default.execute(14);
         while ( (pr_default.getStatus(14) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004E16_A1749Artefatos_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T004E16_A1751Artefatos_Descricao[0]);
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      public void Valid_Artefatos_codigo( GXCombobox dynGX_Parm1 ,
                                          String GX_Parm2 )
      {
         dynArtefatos_Codigo = dynGX_Parm1;
         A1749Artefatos_Codigo = (int)(NumberUtil.Val( dynArtefatos_Codigo.CurrentValue, "."));
         A1751Artefatos_Descricao = GX_Parm2;
         /* Using cursor T004E17 */
         pr_default.execute(15, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Artefatos'.", "ForeignKeyNotFound", 1, "ARTEFATOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynArtefatos_Codigo_Internalname;
         }
         A1751Artefatos_Descricao = T004E17_A1751Artefatos_Descricao[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1751Artefatos_Descricao = "";
         }
         isValidOutput.Add(A1751Artefatos_Descricao);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124E2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockartefatos_codigo_Jsonclick = "";
         lblTextblockcontratoservicosartefato_obrigatorio_Jsonclick = "";
         A1751Artefatos_Descricao = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode194 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z1751Artefatos_Descricao = "";
         T004E4_A160ContratoServicos_Codigo = new int[1] ;
         T004E5_A1751Artefatos_Descricao = new String[] {""} ;
         T004E6_A1751Artefatos_Descricao = new String[] {""} ;
         T004E6_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         T004E6_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         T004E6_A160ContratoServicos_Codigo = new int[1] ;
         T004E6_A1749Artefatos_Codigo = new int[1] ;
         T004E7_A1751Artefatos_Descricao = new String[] {""} ;
         T004E8_A160ContratoServicos_Codigo = new int[1] ;
         T004E8_A1749Artefatos_Codigo = new int[1] ;
         T004E3_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         T004E3_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         T004E3_A160ContratoServicos_Codigo = new int[1] ;
         T004E3_A1749Artefatos_Codigo = new int[1] ;
         T004E9_A160ContratoServicos_Codigo = new int[1] ;
         T004E9_A1749Artefatos_Codigo = new int[1] ;
         T004E10_A160ContratoServicos_Codigo = new int[1] ;
         T004E10_A1749Artefatos_Codigo = new int[1] ;
         T004E2_A1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         T004E2_n1768ContratoServicosArtefato_Obrigatorio = new bool[] {false} ;
         T004E2_A160ContratoServicos_Codigo = new int[1] ;
         T004E2_A1749Artefatos_Codigo = new int[1] ;
         T004E14_A1751Artefatos_Descricao = new String[] {""} ;
         T004E15_A160ContratoServicos_Codigo = new int[1] ;
         T004E15_A1749Artefatos_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004E16_A1749Artefatos_Codigo = new int[1] ;
         T004E16_A1751Artefatos_Descricao = new String[] {""} ;
         T004E17_A1751Artefatos_Descricao = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosartefatos__default(),
            new Object[][] {
                new Object[] {
               T004E2_A1768ContratoServicosArtefato_Obrigatorio, T004E2_n1768ContratoServicosArtefato_Obrigatorio, T004E2_A160ContratoServicos_Codigo, T004E2_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004E3_A1768ContratoServicosArtefato_Obrigatorio, T004E3_n1768ContratoServicosArtefato_Obrigatorio, T004E3_A160ContratoServicos_Codigo, T004E3_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004E4_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T004E5_A1751Artefatos_Descricao
               }
               , new Object[] {
               T004E6_A1751Artefatos_Descricao, T004E6_A1768ContratoServicosArtefato_Obrigatorio, T004E6_n1768ContratoServicosArtefato_Obrigatorio, T004E6_A160ContratoServicos_Codigo, T004E6_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004E7_A1751Artefatos_Descricao
               }
               , new Object[] {
               T004E8_A160ContratoServicos_Codigo, T004E8_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004E9_A160ContratoServicos_Codigo, T004E9_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004E10_A160ContratoServicos_Codigo, T004E10_A1749Artefatos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004E14_A1751Artefatos_Descricao
               }
               , new Object[] {
               T004E15_A160ContratoServicos_Codigo, T004E15_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004E16_A1749Artefatos_Codigo, T004E16_A1751Artefatos_Descricao
               }
               , new Object[] {
               T004E17_A1751Artefatos_Descricao
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound194 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoServicos_Codigo ;
      private int wcpOAV8Artefatos_Codigo ;
      private int Z160ContratoServicos_Codigo ;
      private int Z1749Artefatos_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int AV7ContratoServicos_Codigo ;
      private int AV8Artefatos_Codigo ;
      private int trnEnded ;
      private int lblTbjava_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A160ContratoServicos_Codigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkContratoServicosArtefato_Obrigatorio_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynArtefatos_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockartefatos_codigo_Internalname ;
      private String lblTextblockartefatos_codigo_Jsonclick ;
      private String dynArtefatos_Codigo_Jsonclick ;
      private String lblTextblockcontratoservicosartefato_obrigatorio_Internalname ;
      private String lblTextblockcontratoservicosartefato_obrigatorio_Jsonclick ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode194 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool Z1768ContratoServicosArtefato_Obrigatorio ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1768ContratoServicosArtefato_Obrigatorio ;
      private bool n1768ContratoServicosArtefato_Obrigatorio ;
      private bool returnInSub ;
      private String A1751Artefatos_Descricao ;
      private String Z1751Artefatos_Descricao ;
      private IGxSession AV11WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynArtefatos_Codigo ;
      private GXCheckbox chkContratoServicosArtefato_Obrigatorio ;
      private IDataStoreProvider pr_default ;
      private int[] T004E4_A160ContratoServicos_Codigo ;
      private String[] T004E5_A1751Artefatos_Descricao ;
      private String[] T004E6_A1751Artefatos_Descricao ;
      private bool[] T004E6_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] T004E6_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] T004E6_A160ContratoServicos_Codigo ;
      private int[] T004E6_A1749Artefatos_Codigo ;
      private String[] T004E7_A1751Artefatos_Descricao ;
      private int[] T004E8_A160ContratoServicos_Codigo ;
      private int[] T004E8_A1749Artefatos_Codigo ;
      private bool[] T004E3_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] T004E3_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] T004E3_A160ContratoServicos_Codigo ;
      private int[] T004E3_A1749Artefatos_Codigo ;
      private int[] T004E9_A160ContratoServicos_Codigo ;
      private int[] T004E9_A1749Artefatos_Codigo ;
      private int[] T004E10_A160ContratoServicos_Codigo ;
      private int[] T004E10_A1749Artefatos_Codigo ;
      private bool[] T004E2_A1768ContratoServicosArtefato_Obrigatorio ;
      private bool[] T004E2_n1768ContratoServicosArtefato_Obrigatorio ;
      private int[] T004E2_A160ContratoServicos_Codigo ;
      private int[] T004E2_A1749Artefatos_Codigo ;
      private String[] T004E14_A1751Artefatos_Descricao ;
      private int[] T004E15_A160ContratoServicos_Codigo ;
      private int[] T004E15_A1749Artefatos_Codigo ;
      private int[] T004E16_A1749Artefatos_Codigo ;
      private String[] T004E16_A1751Artefatos_Descricao ;
      private String[] T004E17_A1751Artefatos_Descricao ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class contratoservicosartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004E4 ;
          prmT004E4 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E6 ;
          prmT004E6 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E5 ;
          prmT004E5 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E7 ;
          prmT004E7 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E8 ;
          prmT004E8 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E3 ;
          prmT004E3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E9 ;
          prmT004E9 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E10 ;
          prmT004E10 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E2 ;
          prmT004E2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E11 ;
          prmT004E11 = new Object[] {
          new Object[] {"@ContratoServicosArtefato_Obrigatorio",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E12 ;
          prmT004E12 = new Object[] {
          new Object[] {"@ContratoServicosArtefato_Obrigatorio",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E13 ;
          prmT004E13 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E14 ;
          prmT004E14 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004E15 ;
          prmT004E15 = new Object[] {
          } ;
          Object[] prmT004E16 ;
          prmT004E16 = new Object[] {
          } ;
          Object[] prmT004E17 ;
          prmT004E17 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T004E2", "SELECT [ContratoServicosArtefato_Obrigatorio], [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E2,1,0,true,false )
             ,new CursorDef("T004E3", "SELECT [ContratoServicosArtefato_Obrigatorio], [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E3,1,0,true,false )
             ,new CursorDef("T004E4", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E4,1,0,true,false )
             ,new CursorDef("T004E5", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E5,1,0,true,false )
             ,new CursorDef("T004E6", "SELECT T2.[Artefatos_Descricao], TM1.[ContratoServicosArtefato_Obrigatorio], TM1.[ContratoServicos_Codigo], TM1.[Artefatos_Codigo] FROM ([ContratoServicosArtefatos] TM1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = TM1.[Artefatos_Codigo]) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[Artefatos_Codigo] = @Artefatos_Codigo ORDER BY TM1.[ContratoServicos_Codigo], TM1.[Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004E6,100,0,true,false )
             ,new CursorDef("T004E7", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E7,1,0,true,false )
             ,new CursorDef("T004E8", "SELECT [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004E8,1,0,true,false )
             ,new CursorDef("T004E9", "SELECT TOP 1 [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] > @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [Artefatos_Codigo] > @Artefatos_Codigo) ORDER BY [ContratoServicos_Codigo], [Artefatos_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004E9,1,0,true,true )
             ,new CursorDef("T004E10", "SELECT TOP 1 [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE ( [ContratoServicos_Codigo] < @ContratoServicos_Codigo or [ContratoServicos_Codigo] = @ContratoServicos_Codigo and [Artefatos_Codigo] < @Artefatos_Codigo) ORDER BY [ContratoServicos_Codigo] DESC, [Artefatos_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004E10,1,0,true,true )
             ,new CursorDef("T004E11", "INSERT INTO [ContratoServicosArtefatos]([ContratoServicosArtefato_Obrigatorio], [ContratoServicos_Codigo], [Artefatos_Codigo]) VALUES(@ContratoServicosArtefato_Obrigatorio, @ContratoServicos_Codigo, @Artefatos_Codigo)", GxErrorMask.GX_NOMASK,prmT004E11)
             ,new CursorDef("T004E12", "UPDATE [ContratoServicosArtefatos] SET [ContratoServicosArtefato_Obrigatorio]=@ContratoServicosArtefato_Obrigatorio  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmT004E12)
             ,new CursorDef("T004E13", "DELETE FROM [ContratoServicosArtefatos]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmT004E13)
             ,new CursorDef("T004E14", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E14,1,0,true,false )
             ,new CursorDef("T004E15", "SELECT [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) ORDER BY [ContratoServicos_Codigo], [Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004E15,100,0,true,false )
             ,new CursorDef("T004E16", "SELECT [Artefatos_Codigo], [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) ORDER BY [Artefatos_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E16,0,0,true,false )
             ,new CursorDef("T004E17", "SELECT [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004E17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
