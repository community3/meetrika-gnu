/*
               File: Prc_AutorizacaoConsumo_Encerrar
        Description: Prc_Autorizacao Consumo_Encerrar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:4.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_autorizacaoconsumo_encerrar : GXProcedure
   {
      public prc_autorizacaoconsumo_encerrar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_autorizacaoconsumo_encerrar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AutorizacaoConsumo_Codigo )
      {
         this.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         initialize();
         executePrivate();
         aP0_AutorizacaoConsumo_Codigo=this.AV8AutorizacaoConsumo_Codigo;
      }

      public int executeUdp( )
      {
         this.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         initialize();
         executePrivate();
         aP0_AutorizacaoConsumo_Codigo=this.AV8AutorizacaoConsumo_Codigo;
         return AV8AutorizacaoConsumo_Codigo ;
      }

      public void executeSubmit( ref int aP0_AutorizacaoConsumo_Codigo )
      {
         prc_autorizacaoconsumo_encerrar objprc_autorizacaoconsumo_encerrar;
         objprc_autorizacaoconsumo_encerrar = new prc_autorizacaoconsumo_encerrar();
         objprc_autorizacaoconsumo_encerrar.AV8AutorizacaoConsumo_Codigo = aP0_AutorizacaoConsumo_Codigo;
         objprc_autorizacaoconsumo_encerrar.context.SetSubmitInitialConfig(context);
         objprc_autorizacaoconsumo_encerrar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_autorizacaoconsumo_encerrar);
         aP0_AutorizacaoConsumo_Codigo=this.AV8AutorizacaoConsumo_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_autorizacaoconsumo_encerrar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00D72 */
         pr_default.execute(0, new Object[] {AV8AutorizacaoConsumo_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
         dsDefault.SmartCacheProvider.SetUpdated("AutorizacaoConsumo") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_autorizacaoconsumo_encerrar__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8AutorizacaoConsumo_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AutorizacaoConsumo_Codigo ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_autorizacaoconsumo_encerrar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D72 ;
          prmP00D72 = new Object[] {
          new Object[] {"@AV8AutorizacaoConsumo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D72", "UPDATE [AutorizacaoConsumo] SET [AutorizacaoConsumo_Status]='ENC'  WHERE [AutorizacaoConsumo_Codigo] = @AV8AutorizacaoConsumo_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00D72)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
