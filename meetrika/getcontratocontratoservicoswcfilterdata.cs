/*
               File: GetContratoContratoServicosWCFilterData
        Description: Get Contrato Contrato Servicos WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/7/2019 19:2:49.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratoservicoswcfilterdata : GXProcedure
   {
      public getcontratocontratoservicoswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratoservicoswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV29DDOName = aP0_DDOName;
         this.AV27SearchTxt = aP1_SearchTxt;
         this.AV28SearchTxtTo = aP2_SearchTxtTo;
         this.AV33OptionsJson = "" ;
         this.AV36OptionsDescJson = "" ;
         this.AV38OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV29DDOName = aP0_DDOName;
         this.AV27SearchTxt = aP1_SearchTxt;
         this.AV28SearchTxtTo = aP2_SearchTxtTo;
         this.AV33OptionsJson = "" ;
         this.AV36OptionsDescJson = "" ;
         this.AV38OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
         return AV38OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratoservicoswcfilterdata objgetcontratocontratoservicoswcfilterdata;
         objgetcontratocontratoservicoswcfilterdata = new getcontratocontratoservicoswcfilterdata();
         objgetcontratocontratoservicoswcfilterdata.AV29DDOName = aP0_DDOName;
         objgetcontratocontratoservicoswcfilterdata.AV27SearchTxt = aP1_SearchTxt;
         objgetcontratocontratoservicoswcfilterdata.AV28SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratoservicoswcfilterdata.AV33OptionsJson = "" ;
         objgetcontratocontratoservicoswcfilterdata.AV36OptionsDescJson = "" ;
         objgetcontratocontratoservicoswcfilterdata.AV38OptionIndexesJson = "" ;
         objgetcontratocontratoservicoswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratoservicoswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratoservicoswcfilterdata);
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratoservicoswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV32Options = (IGxCollection)(new GxSimpleCollection());
         AV35OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV37OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV29DDOName), "DDO_SERVICO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV29DDOName), "DDO_CONTRATOSERVICOS_ALIAS") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOS_ALIASOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV33OptionsJson = AV32Options.ToJSonString(false);
         AV36OptionsDescJson = AV35OptionsDesc.ToJSonString(false);
         AV38OptionIndexesJson = AV37OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV40Session.Get("ContratoContratoServicosWCGridState"), "") == 0 )
         {
            AV42GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoServicosWCGridState"), "");
         }
         else
         {
            AV42GridState.FromXml(AV40Session.Get("ContratoContratoServicosWCGridState"), "");
         }
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV42GridState.gxTpr_Filtervalues.Count )
         {
            AV43GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV42GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "SERVICO_TIPOHIERARQUIA") == 0 )
            {
               AV45Servico_TipoHierarquia = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "SERVICO_ATIVO") == 0 )
            {
               AV46Servico_Ativo = BooleanUtil.Val( AV43GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV10TFServico_Nome = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV11TFServico_Nome_Sel = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_ALIAS") == 0 )
            {
               AV12TFContratoServicos_Alias = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_ALIAS_SEL") == 0 )
            {
               AV13TFContratoServicos_Alias_Sel = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFSERVICO_VLRUNIDADECONTRATADA") == 0 )
            {
               AV16TFServico_VlrUnidadeContratada = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, ".");
               AV17TFServico_VlrUnidadeContratada_To = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFSERVICO_QTDCONTRATADA") == 0 )
            {
               AV18TFServico_QtdContratada = (long)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
               AV19TFServico_QtdContratada_To = (long)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFSERVICOCONTRATO_FATURAMENTO_SEL") == 0 )
            {
               AV20TFServicoContrato_Faturamento_SelsJson = AV43GridStateFilterValue.gxTpr_Value;
               AV21TFServicoContrato_Faturamento_Sels.FromJSonString(AV20TFServicoContrato_Faturamento_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_HMLSEMCNF_SEL") == 0 )
            {
               AV22TFContratoServicos_HmlSemCnf_Sel = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_PRAZOANALISE") == 0 )
            {
               AV23TFContratoServicos_PrazoAnalise = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
               AV24TFContratoServicos_PrazoAnalise_To = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_INDICADORES") == 0 )
            {
               AV25TFContratoServicos_Indicadores = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
               AV26TFContratoServicos_Indicadores_To = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOS_ATIVO_SEL") == 0 )
            {
               AV48TFContratoServicos_Ativo_Sel = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV47Contrato_Codigo = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICO_NOMEOPTIONS' Routine */
         AV10TFServico_Nome = AV27SearchTxt;
         AV11TFServico_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A607ServicoContrato_Faturamento ,
                                              AV21TFServicoContrato_Faturamento_Sels ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFContratoServicos_Alias_Sel ,
                                              AV12TFContratoServicos_Alias ,
                                              AV16TFServico_VlrUnidadeContratada ,
                                              AV17TFServico_VlrUnidadeContratada_To ,
                                              AV18TFServico_QtdContratada ,
                                              AV19TFServico_QtdContratada_To ,
                                              AV21TFServicoContrato_Faturamento_Sels.Count ,
                                              AV22TFContratoServicos_HmlSemCnf_Sel ,
                                              AV23TFContratoServicos_PrazoAnalise ,
                                              AV24TFContratoServicos_PrazoAnalise_To ,
                                              AV48TFContratoServicos_Ativo_Sel ,
                                              A608Servico_Nome ,
                                              A1858ContratoServicos_Alias ,
                                              A557Servico_VlrUnidadeContratada ,
                                              A555Servico_QtdContratada ,
                                              A888ContratoServicos_HmlSemCnf ,
                                              A1152ContratoServicos_PrazoAnalise ,
                                              A638ContratoServicos_Ativo ,
                                              A632Servico_Ativo ,
                                              AV25TFContratoServicos_Indicadores ,
                                              A1377ContratoServicos_Indicadores ,
                                              AV26TFContratoServicos_Indicadores_To ,
                                              A1530Servico_TipoHierarquia ,
                                              AV47Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFContratoServicos_Alias = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicos_Alias), 15, "%");
         /* Using cursor P00UD3 */
         pr_default.execute(0, new Object[] {AV47Contrato_Codigo, AV25TFContratoServicos_Indicadores, AV25TFContratoServicos_Indicadores, AV26TFContratoServicos_Indicadores_To, AV26TFContratoServicos_Indicadores_To, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFContratoServicos_Alias, AV13TFContratoServicos_Alias_Sel, AV16TFServico_VlrUnidadeContratada, AV17TFServico_VlrUnidadeContratada_To, AV18TFServico_QtdContratada, AV19TFServico_QtdContratada_To, AV23TFContratoServicos_PrazoAnalise, AV24TFContratoServicos_PrazoAnalise_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUD2 = false;
            A74Contrato_Codigo = P00UD3_A74Contrato_Codigo[0];
            A1530Servico_TipoHierarquia = P00UD3_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00UD3_n1530Servico_TipoHierarquia[0];
            A155Servico_Codigo = P00UD3_A155Servico_Codigo[0];
            A638ContratoServicos_Ativo = P00UD3_A638ContratoServicos_Ativo[0];
            A1152ContratoServicos_PrazoAnalise = P00UD3_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = P00UD3_n1152ContratoServicos_PrazoAnalise[0];
            A888ContratoServicos_HmlSemCnf = P00UD3_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = P00UD3_n888ContratoServicos_HmlSemCnf[0];
            A607ServicoContrato_Faturamento = P00UD3_A607ServicoContrato_Faturamento[0];
            A555Servico_QtdContratada = P00UD3_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = P00UD3_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = P00UD3_A557Servico_VlrUnidadeContratada[0];
            A1858ContratoServicos_Alias = P00UD3_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = P00UD3_n1858ContratoServicos_Alias[0];
            A608Servico_Nome = P00UD3_A608Servico_Nome[0];
            A632Servico_Ativo = P00UD3_A632Servico_Ativo[0];
            A160ContratoServicos_Codigo = P00UD3_A160ContratoServicos_Codigo[0];
            A1377ContratoServicos_Indicadores = P00UD3_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00UD3_n1377ContratoServicos_Indicadores[0];
            A1530Servico_TipoHierarquia = P00UD3_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00UD3_n1530Servico_TipoHierarquia[0];
            A608Servico_Nome = P00UD3_A608Servico_Nome[0];
            A632Servico_Ativo = P00UD3_A632Servico_Ativo[0];
            A1377ContratoServicos_Indicadores = P00UD3_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00UD3_n1377ContratoServicos_Indicadores[0];
            AV39count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UD3_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( P00UD3_A155Servico_Codigo[0] == A155Servico_Codigo ) )
            {
               BRKUD2 = false;
               A160ContratoServicos_Codigo = P00UD3_A160ContratoServicos_Codigo[0];
               AV39count = (long)(AV39count+1);
               BRKUD2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
            {
               AV31Option = A608Servico_Nome;
               AV30InsertIndex = 1;
               while ( ( AV30InsertIndex <= AV32Options.Count ) && ( StringUtil.StrCmp(((String)AV32Options.Item(AV30InsertIndex)), AV31Option) < 0 ) )
               {
                  AV30InsertIndex = (int)(AV30InsertIndex+1);
               }
               AV32Options.Add(AV31Option, AV30InsertIndex);
               AV37OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV39count), "Z,ZZZ,ZZZ,ZZ9")), AV30InsertIndex);
            }
            if ( AV32Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUD2 )
            {
               BRKUD2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSERVICOS_ALIASOPTIONS' Routine */
         AV12TFContratoServicos_Alias = AV27SearchTxt;
         AV13TFContratoServicos_Alias_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A607ServicoContrato_Faturamento ,
                                              AV21TFServicoContrato_Faturamento_Sels ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFContratoServicos_Alias_Sel ,
                                              AV12TFContratoServicos_Alias ,
                                              AV16TFServico_VlrUnidadeContratada ,
                                              AV17TFServico_VlrUnidadeContratada_To ,
                                              AV18TFServico_QtdContratada ,
                                              AV19TFServico_QtdContratada_To ,
                                              AV21TFServicoContrato_Faturamento_Sels.Count ,
                                              AV22TFContratoServicos_HmlSemCnf_Sel ,
                                              AV23TFContratoServicos_PrazoAnalise ,
                                              AV24TFContratoServicos_PrazoAnalise_To ,
                                              AV48TFContratoServicos_Ativo_Sel ,
                                              A608Servico_Nome ,
                                              A1858ContratoServicos_Alias ,
                                              A557Servico_VlrUnidadeContratada ,
                                              A555Servico_QtdContratada ,
                                              A888ContratoServicos_HmlSemCnf ,
                                              A1152ContratoServicos_PrazoAnalise ,
                                              A638ContratoServicos_Ativo ,
                                              A632Servico_Ativo ,
                                              AV25TFContratoServicos_Indicadores ,
                                              A1377ContratoServicos_Indicadores ,
                                              AV26TFContratoServicos_Indicadores_To ,
                                              A1530Servico_TipoHierarquia ,
                                              AV47Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.LONG, TypeConstants.LONG, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.LONG, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFContratoServicos_Alias = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicos_Alias), 15, "%");
         /* Using cursor P00UD5 */
         pr_default.execute(1, new Object[] {AV47Contrato_Codigo, AV25TFContratoServicos_Indicadores, AV25TFContratoServicos_Indicadores, AV26TFContratoServicos_Indicadores_To, AV26TFContratoServicos_Indicadores_To, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFContratoServicos_Alias, AV13TFContratoServicos_Alias_Sel, AV16TFServico_VlrUnidadeContratada, AV17TFServico_VlrUnidadeContratada_To, AV18TFServico_QtdContratada, AV19TFServico_QtdContratada_To, AV23TFContratoServicos_PrazoAnalise, AV24TFContratoServicos_PrazoAnalise_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUD4 = false;
            A155Servico_Codigo = P00UD5_A155Servico_Codigo[0];
            A74Contrato_Codigo = P00UD5_A74Contrato_Codigo[0];
            A1530Servico_TipoHierarquia = P00UD5_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00UD5_n1530Servico_TipoHierarquia[0];
            A1858ContratoServicos_Alias = P00UD5_A1858ContratoServicos_Alias[0];
            n1858ContratoServicos_Alias = P00UD5_n1858ContratoServicos_Alias[0];
            A638ContratoServicos_Ativo = P00UD5_A638ContratoServicos_Ativo[0];
            A1152ContratoServicos_PrazoAnalise = P00UD5_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = P00UD5_n1152ContratoServicos_PrazoAnalise[0];
            A888ContratoServicos_HmlSemCnf = P00UD5_A888ContratoServicos_HmlSemCnf[0];
            n888ContratoServicos_HmlSemCnf = P00UD5_n888ContratoServicos_HmlSemCnf[0];
            A607ServicoContrato_Faturamento = P00UD5_A607ServicoContrato_Faturamento[0];
            A555Servico_QtdContratada = P00UD5_A555Servico_QtdContratada[0];
            n555Servico_QtdContratada = P00UD5_n555Servico_QtdContratada[0];
            A557Servico_VlrUnidadeContratada = P00UD5_A557Servico_VlrUnidadeContratada[0];
            A608Servico_Nome = P00UD5_A608Servico_Nome[0];
            A632Servico_Ativo = P00UD5_A632Servico_Ativo[0];
            A160ContratoServicos_Codigo = P00UD5_A160ContratoServicos_Codigo[0];
            A1377ContratoServicos_Indicadores = P00UD5_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00UD5_n1377ContratoServicos_Indicadores[0];
            A1530Servico_TipoHierarquia = P00UD5_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = P00UD5_n1530Servico_TipoHierarquia[0];
            A608Servico_Nome = P00UD5_A608Servico_Nome[0];
            A632Servico_Ativo = P00UD5_A632Servico_Ativo[0];
            A1377ContratoServicos_Indicadores = P00UD5_A1377ContratoServicos_Indicadores[0];
            n1377ContratoServicos_Indicadores = P00UD5_n1377ContratoServicos_Indicadores[0];
            AV39count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00UD5_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( StringUtil.StrCmp(P00UD5_A1858ContratoServicos_Alias[0], A1858ContratoServicos_Alias) == 0 ) )
            {
               BRKUD4 = false;
               A160ContratoServicos_Codigo = P00UD5_A160ContratoServicos_Codigo[0];
               AV39count = (long)(AV39count+1);
               BRKUD4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1858ContratoServicos_Alias)) )
            {
               AV31Option = A1858ContratoServicos_Alias;
               AV32Options.Add(AV31Option, 0);
               AV37OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV39count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV32Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUD4 )
            {
               BRKUD4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV32Options = new GxSimpleCollection();
         AV35OptionsDesc = new GxSimpleCollection();
         AV37OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV40Session = context.GetSession();
         AV42GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV43GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV45Servico_TipoHierarquia = 1;
         AV46Servico_Ativo = true;
         AV10TFServico_Nome = "";
         AV11TFServico_Nome_Sel = "";
         AV12TFContratoServicos_Alias = "";
         AV13TFContratoServicos_Alias_Sel = "";
         AV20TFServicoContrato_Faturamento_SelsJson = "";
         AV21TFServicoContrato_Faturamento_Sels = new GxSimpleCollection();
         scmdbuf = "";
         lV10TFServico_Nome = "";
         lV12TFContratoServicos_Alias = "";
         A607ServicoContrato_Faturamento = "";
         A608Servico_Nome = "";
         A1858ContratoServicos_Alias = "";
         P00UD3_A74Contrato_Codigo = new int[1] ;
         P00UD3_A1530Servico_TipoHierarquia = new short[1] ;
         P00UD3_n1530Servico_TipoHierarquia = new bool[] {false} ;
         P00UD3_A155Servico_Codigo = new int[1] ;
         P00UD3_A638ContratoServicos_Ativo = new bool[] {false} ;
         P00UD3_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00UD3_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00UD3_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00UD3_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00UD3_A607ServicoContrato_Faturamento = new String[] {""} ;
         P00UD3_A555Servico_QtdContratada = new long[1] ;
         P00UD3_n555Servico_QtdContratada = new bool[] {false} ;
         P00UD3_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00UD3_A1858ContratoServicos_Alias = new String[] {""} ;
         P00UD3_n1858ContratoServicos_Alias = new bool[] {false} ;
         P00UD3_A608Servico_Nome = new String[] {""} ;
         P00UD3_A632Servico_Ativo = new bool[] {false} ;
         P00UD3_A160ContratoServicos_Codigo = new int[1] ;
         P00UD3_A1377ContratoServicos_Indicadores = new short[1] ;
         P00UD3_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         AV31Option = "";
         P00UD5_A155Servico_Codigo = new int[1] ;
         P00UD5_A74Contrato_Codigo = new int[1] ;
         P00UD5_A1530Servico_TipoHierarquia = new short[1] ;
         P00UD5_n1530Servico_TipoHierarquia = new bool[] {false} ;
         P00UD5_A1858ContratoServicos_Alias = new String[] {""} ;
         P00UD5_n1858ContratoServicos_Alias = new bool[] {false} ;
         P00UD5_A638ContratoServicos_Ativo = new bool[] {false} ;
         P00UD5_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00UD5_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00UD5_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00UD5_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00UD5_A607ServicoContrato_Faturamento = new String[] {""} ;
         P00UD5_A555Servico_QtdContratada = new long[1] ;
         P00UD5_n555Servico_QtdContratada = new bool[] {false} ;
         P00UD5_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00UD5_A608Servico_Nome = new String[] {""} ;
         P00UD5_A632Servico_Ativo = new bool[] {false} ;
         P00UD5_A160ContratoServicos_Codigo = new int[1] ;
         P00UD5_A1377ContratoServicos_Indicadores = new short[1] ;
         P00UD5_n1377ContratoServicos_Indicadores = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratoservicoswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UD3_A74Contrato_Codigo, P00UD3_A1530Servico_TipoHierarquia, P00UD3_n1530Servico_TipoHierarquia, P00UD3_A155Servico_Codigo, P00UD3_A638ContratoServicos_Ativo, P00UD3_A1152ContratoServicos_PrazoAnalise, P00UD3_n1152ContratoServicos_PrazoAnalise, P00UD3_A888ContratoServicos_HmlSemCnf, P00UD3_n888ContratoServicos_HmlSemCnf, P00UD3_A607ServicoContrato_Faturamento,
               P00UD3_A555Servico_QtdContratada, P00UD3_n555Servico_QtdContratada, P00UD3_A557Servico_VlrUnidadeContratada, P00UD3_A1858ContratoServicos_Alias, P00UD3_n1858ContratoServicos_Alias, P00UD3_A608Servico_Nome, P00UD3_A632Servico_Ativo, P00UD3_A160ContratoServicos_Codigo, P00UD3_A1377ContratoServicos_Indicadores, P00UD3_n1377ContratoServicos_Indicadores
               }
               , new Object[] {
               P00UD5_A155Servico_Codigo, P00UD5_A74Contrato_Codigo, P00UD5_A1530Servico_TipoHierarquia, P00UD5_n1530Servico_TipoHierarquia, P00UD5_A1858ContratoServicos_Alias, P00UD5_n1858ContratoServicos_Alias, P00UD5_A638ContratoServicos_Ativo, P00UD5_A1152ContratoServicos_PrazoAnalise, P00UD5_n1152ContratoServicos_PrazoAnalise, P00UD5_A888ContratoServicos_HmlSemCnf,
               P00UD5_n888ContratoServicos_HmlSemCnf, P00UD5_A607ServicoContrato_Faturamento, P00UD5_A555Servico_QtdContratada, P00UD5_n555Servico_QtdContratada, P00UD5_A557Servico_VlrUnidadeContratada, P00UD5_A608Servico_Nome, P00UD5_A632Servico_Ativo, P00UD5_A160ContratoServicos_Codigo, P00UD5_A1377ContratoServicos_Indicadores, P00UD5_n1377ContratoServicos_Indicadores
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV45Servico_TipoHierarquia ;
      private short AV22TFContratoServicos_HmlSemCnf_Sel ;
      private short AV23TFContratoServicos_PrazoAnalise ;
      private short AV24TFContratoServicos_PrazoAnalise_To ;
      private short AV25TFContratoServicos_Indicadores ;
      private short AV26TFContratoServicos_Indicadores_To ;
      private short AV48TFContratoServicos_Ativo_Sel ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A1377ContratoServicos_Indicadores ;
      private short A1530Servico_TipoHierarquia ;
      private int AV51GXV1 ;
      private int AV47Contrato_Codigo ;
      private int AV21TFServicoContrato_Faturamento_Sels_Count ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int AV30InsertIndex ;
      private long AV18TFServico_QtdContratada ;
      private long AV19TFServico_QtdContratada_To ;
      private long A555Servico_QtdContratada ;
      private long AV39count ;
      private decimal AV16TFServico_VlrUnidadeContratada ;
      private decimal AV17TFServico_VlrUnidadeContratada_To ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private String AV10TFServico_Nome ;
      private String AV11TFServico_Nome_Sel ;
      private String AV12TFContratoServicos_Alias ;
      private String AV13TFContratoServicos_Alias_Sel ;
      private String scmdbuf ;
      private String lV10TFServico_Nome ;
      private String lV12TFContratoServicos_Alias ;
      private String A608Servico_Nome ;
      private String A1858ContratoServicos_Alias ;
      private bool returnInSub ;
      private bool AV46Servico_Ativo ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool A638ContratoServicos_Ativo ;
      private bool A632Servico_Ativo ;
      private bool BRKUD2 ;
      private bool n1530Servico_TipoHierarquia ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private bool n555Servico_QtdContratada ;
      private bool n1858ContratoServicos_Alias ;
      private bool n1377ContratoServicos_Indicadores ;
      private bool BRKUD4 ;
      private String AV38OptionIndexesJson ;
      private String AV33OptionsJson ;
      private String AV36OptionsDescJson ;
      private String AV20TFServicoContrato_Faturamento_SelsJson ;
      private String AV29DDOName ;
      private String AV27SearchTxt ;
      private String AV28SearchTxtTo ;
      private String A607ServicoContrato_Faturamento ;
      private String AV31Option ;
      private IGxSession AV40Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UD3_A74Contrato_Codigo ;
      private short[] P00UD3_A1530Servico_TipoHierarquia ;
      private bool[] P00UD3_n1530Servico_TipoHierarquia ;
      private int[] P00UD3_A155Servico_Codigo ;
      private bool[] P00UD3_A638ContratoServicos_Ativo ;
      private short[] P00UD3_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00UD3_n1152ContratoServicos_PrazoAnalise ;
      private bool[] P00UD3_A888ContratoServicos_HmlSemCnf ;
      private bool[] P00UD3_n888ContratoServicos_HmlSemCnf ;
      private String[] P00UD3_A607ServicoContrato_Faturamento ;
      private long[] P00UD3_A555Servico_QtdContratada ;
      private bool[] P00UD3_n555Servico_QtdContratada ;
      private decimal[] P00UD3_A557Servico_VlrUnidadeContratada ;
      private String[] P00UD3_A1858ContratoServicos_Alias ;
      private bool[] P00UD3_n1858ContratoServicos_Alias ;
      private String[] P00UD3_A608Servico_Nome ;
      private bool[] P00UD3_A632Servico_Ativo ;
      private int[] P00UD3_A160ContratoServicos_Codigo ;
      private short[] P00UD3_A1377ContratoServicos_Indicadores ;
      private bool[] P00UD3_n1377ContratoServicos_Indicadores ;
      private int[] P00UD5_A155Servico_Codigo ;
      private int[] P00UD5_A74Contrato_Codigo ;
      private short[] P00UD5_A1530Servico_TipoHierarquia ;
      private bool[] P00UD5_n1530Servico_TipoHierarquia ;
      private String[] P00UD5_A1858ContratoServicos_Alias ;
      private bool[] P00UD5_n1858ContratoServicos_Alias ;
      private bool[] P00UD5_A638ContratoServicos_Ativo ;
      private short[] P00UD5_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00UD5_n1152ContratoServicos_PrazoAnalise ;
      private bool[] P00UD5_A888ContratoServicos_HmlSemCnf ;
      private bool[] P00UD5_n888ContratoServicos_HmlSemCnf ;
      private String[] P00UD5_A607ServicoContrato_Faturamento ;
      private long[] P00UD5_A555Servico_QtdContratada ;
      private bool[] P00UD5_n555Servico_QtdContratada ;
      private decimal[] P00UD5_A557Servico_VlrUnidadeContratada ;
      private String[] P00UD5_A608Servico_Nome ;
      private bool[] P00UD5_A632Servico_Ativo ;
      private int[] P00UD5_A160ContratoServicos_Codigo ;
      private short[] P00UD5_A1377ContratoServicos_Indicadores ;
      private bool[] P00UD5_n1377ContratoServicos_Indicadores ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV37OptionIndexes ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21TFServicoContrato_Faturamento_Sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV42GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV43GridStateFilterValue ;
   }

   public class getcontratocontratoservicoswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UD3( IGxContext context ,
                                             String A607ServicoContrato_Faturamento ,
                                             IGxCollection AV21TFServicoContrato_Faturamento_Sels ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFContratoServicos_Alias_Sel ,
                                             String AV12TFContratoServicos_Alias ,
                                             decimal AV16TFServico_VlrUnidadeContratada ,
                                             decimal AV17TFServico_VlrUnidadeContratada_To ,
                                             long AV18TFServico_QtdContratada ,
                                             long AV19TFServico_QtdContratada_To ,
                                             int AV21TFServicoContrato_Faturamento_Sels_Count ,
                                             short AV22TFContratoServicos_HmlSemCnf_Sel ,
                                             short AV23TFContratoServicos_PrazoAnalise ,
                                             short AV24TFContratoServicos_PrazoAnalise_To ,
                                             short AV48TFContratoServicos_Ativo_Sel ,
                                             String A608Servico_Nome ,
                                             String A1858ContratoServicos_Alias ,
                                             decimal A557Servico_VlrUnidadeContratada ,
                                             long A555Servico_QtdContratada ,
                                             bool A888ContratoServicos_HmlSemCnf ,
                                             short A1152ContratoServicos_PrazoAnalise ,
                                             bool A638ContratoServicos_Ativo ,
                                             bool A632Servico_Ativo ,
                                             short AV25TFContratoServicos_Indicadores ,
                                             short A1377ContratoServicos_Indicadores ,
                                             short AV26TFContratoServicos_Indicadores_To ,
                                             short A1530Servico_TipoHierarquia ,
                                             int AV47Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [15] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T2.[Servico_TipoHierarquia], T1.[Servico_Codigo], T1.[ContratoServicos_Ativo], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_HmlSemCnf], T1.[ServicoContrato_Faturamento], T1.[Servico_QtdContratada], T1.[Servico_VlrUnidadeContratada], T1.[ContratoServicos_Alias], T2.[Servico_Nome], T2.[Servico_Ativo], T1.[ContratoServicos_Codigo], COALESCE( T3.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T3 ON T3.[ContratoServicosIndicador_CntSrvCod] = T1.[ContratoServicos_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV47Contrato_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Servico_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ((@AV25TFContratoServicos_Indicadores = convert(int, 0)) or ( COALESCE( T3.[ContratoServicos_Indicadores], 0) >= @AV25TFContratoServicos_Indicadores))";
         scmdbuf = scmdbuf + " and ((@AV26TFContratoServicos_Indicadores_To = convert(int, 0)) or ( COALESCE( T3.[ContratoServicos_Indicadores], 0) <= @AV26TFContratoServicos_Indicadores_To))";
         scmdbuf = scmdbuf + " and (T2.[Servico_TipoHierarquia] = 1)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV10TFServico_Nome)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicos_Alias_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicos_Alias)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Alias] like @lV12TFContratoServicos_Alias)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicos_Alias_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Alias] = @AV13TFContratoServicos_Alias_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFServico_VlrUnidadeContratada) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_VlrUnidadeContratada] >= @AV16TFServico_VlrUnidadeContratada)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFServico_VlrUnidadeContratada_To) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_VlrUnidadeContratada] <= @AV17TFServico_VlrUnidadeContratada_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV18TFServico_QtdContratada) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_QtdContratada] >= @AV18TFServico_QtdContratada)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV19TFServico_QtdContratada_To) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_QtdContratada] <= @AV19TFServico_QtdContratada_To)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV21TFServicoContrato_Faturamento_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFServicoContrato_Faturamento_Sels, "T1.[ServicoContrato_Faturamento] IN (", ")") + ")";
         }
         if ( AV22TFContratoServicos_HmlSemCnf_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_HmlSemCnf] = 1)";
         }
         if ( AV22TFContratoServicos_HmlSemCnf_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_HmlSemCnf] = 0)";
         }
         if ( ! (0==AV23TFContratoServicos_PrazoAnalise) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] >= @AV23TFContratoServicos_PrazoAnalise)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV24TFContratoServicos_PrazoAnalise_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] <= @AV24TFContratoServicos_PrazoAnalise_To)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV48TFContratoServicos_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 1)";
         }
         if ( AV48TFContratoServicos_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo], T1.[Servico_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UD5( IGxContext context ,
                                             String A607ServicoContrato_Faturamento ,
                                             IGxCollection AV21TFServicoContrato_Faturamento_Sels ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFContratoServicos_Alias_Sel ,
                                             String AV12TFContratoServicos_Alias ,
                                             decimal AV16TFServico_VlrUnidadeContratada ,
                                             decimal AV17TFServico_VlrUnidadeContratada_To ,
                                             long AV18TFServico_QtdContratada ,
                                             long AV19TFServico_QtdContratada_To ,
                                             int AV21TFServicoContrato_Faturamento_Sels_Count ,
                                             short AV22TFContratoServicos_HmlSemCnf_Sel ,
                                             short AV23TFContratoServicos_PrazoAnalise ,
                                             short AV24TFContratoServicos_PrazoAnalise_To ,
                                             short AV48TFContratoServicos_Ativo_Sel ,
                                             String A608Servico_Nome ,
                                             String A1858ContratoServicos_Alias ,
                                             decimal A557Servico_VlrUnidadeContratada ,
                                             long A555Servico_QtdContratada ,
                                             bool A888ContratoServicos_HmlSemCnf ,
                                             short A1152ContratoServicos_PrazoAnalise ,
                                             bool A638ContratoServicos_Ativo ,
                                             bool A632Servico_Ativo ,
                                             short AV25TFContratoServicos_Indicadores ,
                                             short A1377ContratoServicos_Indicadores ,
                                             short AV26TFContratoServicos_Indicadores_To ,
                                             short A1530Servico_TipoHierarquia ,
                                             int AV47Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [15] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Codigo], T1.[Contrato_Codigo], T2.[Servico_TipoHierarquia], T1.[ContratoServicos_Alias], T1.[ContratoServicos_Ativo], T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_HmlSemCnf], T1.[ServicoContrato_Faturamento], T1.[Servico_QtdContratada], T1.[Servico_VlrUnidadeContratada], T2.[Servico_Nome], T2.[Servico_Ativo], T1.[ContratoServicos_Codigo], COALESCE( T3.[ContratoServicos_Indicadores], 0) AS ContratoServicos_Indicadores FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicos_Indicadores, [ContratoServicosIndicador_CntSrvCod] FROM [ContratoServicosIndicador] WITH (NOLOCK) GROUP BY [ContratoServicosIndicador_CntSrvCod] ) T3 ON T3.[ContratoServicosIndicador_CntSrvCod] = T1.[ContratoServicos_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV47Contrato_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Servico_Ativo] = 1)";
         scmdbuf = scmdbuf + " and ((@AV25TFContratoServicos_Indicadores = convert(int, 0)) or ( COALESCE( T3.[ContratoServicos_Indicadores], 0) >= @AV25TFContratoServicos_Indicadores))";
         scmdbuf = scmdbuf + " and ((@AV26TFContratoServicos_Indicadores_To = convert(int, 0)) or ( COALESCE( T3.[ContratoServicos_Indicadores], 0) <= @AV26TFContratoServicos_Indicadores_To))";
         scmdbuf = scmdbuf + " and (T2.[Servico_TipoHierarquia] = 1)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV10TFServico_Nome)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicos_Alias_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicos_Alias)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Alias] like @lV12TFContratoServicos_Alias)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicos_Alias_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Alias] = @AV13TFContratoServicos_Alias_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFServico_VlrUnidadeContratada) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_VlrUnidadeContratada] >= @AV16TFServico_VlrUnidadeContratada)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFServico_VlrUnidadeContratada_To) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_VlrUnidadeContratada] <= @AV17TFServico_VlrUnidadeContratada_To)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV18TFServico_QtdContratada) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_QtdContratada] >= @AV18TFServico_QtdContratada)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV19TFServico_QtdContratada_To) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_QtdContratada] <= @AV19TFServico_QtdContratada_To)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV21TFServicoContrato_Faturamento_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFServicoContrato_Faturamento_Sels, "T1.[ServicoContrato_Faturamento] IN (", ")") + ")";
         }
         if ( AV22TFContratoServicos_HmlSemCnf_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_HmlSemCnf] = 1)";
         }
         if ( AV22TFContratoServicos_HmlSemCnf_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_HmlSemCnf] = 0)";
         }
         if ( ! (0==AV23TFContratoServicos_PrazoAnalise) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] >= @AV23TFContratoServicos_PrazoAnalise)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV24TFContratoServicos_PrazoAnalise_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_PrazoAnalise] <= @AV24TFContratoServicos_PrazoAnalise_To)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV48TFContratoServicos_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 1)";
         }
         if ( AV48TFContratoServicos_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicos_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo], T1.[ContratoServicos_Alias]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UD3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (long)dynConstraints[8] , (long)dynConstraints[9] , (int)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (decimal)dynConstraints[17] , (long)dynConstraints[18] , (bool)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (bool)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (short)dynConstraints[25] , (short)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
               case 1 :
                     return conditional_P00UD5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (long)dynConstraints[8] , (long)dynConstraints[9] , (int)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (decimal)dynConstraints[17] , (long)dynConstraints[18] , (bool)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (bool)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (short)dynConstraints[25] , (short)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UD3 ;
          prmP00UD3 = new Object[] {
          new Object[] {"@AV47Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV25TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFContratoServicos_Alias_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV16TFServico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17TFServico_VlrUnidadeContratada_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV18TFServico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV19TFServico_QtdContratada_To",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV23TFContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV24TFContratoServicos_PrazoAnalise_To",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00UD5 ;
          prmP00UD5 = new Object[] {
          new Object[] {"@AV47Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV25TFContratoServicos_Indicadores",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26TFContratoServicos_Indicadores_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFContratoServicos_Alias_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV16TFServico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17TFServico_VlrUnidadeContratada_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV18TFServico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV19TFServico_QtdContratada_To",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV23TFContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV24TFContratoServicos_PrazoAnalise_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UD3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UD3,100,0,true,false )
             ,new CursorDef("P00UD5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UD5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((long[]) buf[10])[0] = rslt.getLong(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[16])[0] = rslt.getBool(12) ;
                ((int[]) buf[17])[0] = rslt.getInt(13) ;
                ((short[]) buf[18])[0] = rslt.getShort(14) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(14);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((long[]) buf[12])[0] = rslt.getLong(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((String[]) buf[15])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[16])[0] = rslt.getBool(12) ;
                ((int[]) buf[17])[0] = rslt.getInt(13) ;
                ((short[]) buf[18])[0] = rslt.getShort(14) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(14);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratoservicoswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratoservicoswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratoservicoswcfilterdata") )
          {
             return  ;
          }
          getcontratocontratoservicoswcfilterdata worker = new getcontratocontratoservicoswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
