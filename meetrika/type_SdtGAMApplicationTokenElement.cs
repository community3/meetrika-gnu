/*
               File: type_SdtGAMApplicationTokenElement
        Description: GAMApplicationTokenElement
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:11.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMApplicationTokenElement : GxUserType, IGxExternalObject
   {
      public SdtGAMApplicationTokenElement( )
      {
         initialize();
      }

      public SdtGAMApplicationTokenElement( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMApplicationTokenElement_externalReference == null )
         {
            GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
         }
         returntostring = "";
         returntostring = (String)(GAMApplicationTokenElement_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Name
      {
         get {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            return GAMApplicationTokenElement_externalReference.Name ;
         }

         set {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            GAMApplicationTokenElement_externalReference.Name = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            return GAMApplicationTokenElement_externalReference.Description ;
         }

         set {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            GAMApplicationTokenElement_externalReference.Description = value;
         }

      }

      public short gxTpr_Order
      {
         get {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            return GAMApplicationTokenElement_externalReference.Order ;
         }

         set {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            GAMApplicationTokenElement_externalReference.Order = value;
         }

      }

      public String gxTpr_Webservicehost
      {
         get {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            return GAMApplicationTokenElement_externalReference.WebServiceHost ;
         }

         set {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            GAMApplicationTokenElement_externalReference.WebServiceHost = value;
         }

      }

      public String gxTpr_Webservicename
      {
         get {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            return GAMApplicationTokenElement_externalReference.WebServiceName ;
         }

         set {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            GAMApplicationTokenElement_externalReference.WebServiceName = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMApplicationTokenElement_externalReference == null )
            {
               GAMApplicationTokenElement_externalReference = new Artech.Security.GAMApplicationTokenElement(context);
            }
            return GAMApplicationTokenElement_externalReference ;
         }

         set {
            GAMApplicationTokenElement_externalReference = (Artech.Security.GAMApplicationTokenElement)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMApplicationTokenElement GAMApplicationTokenElement_externalReference=null ;
   }

}
