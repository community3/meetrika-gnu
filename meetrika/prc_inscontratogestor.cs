/*
               File: PRC_INSContratoGestor
        Description: INS Contrato Gestor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:45.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_inscontratogestor : GXProcedure
   {
      public prc_inscontratogestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_inscontratogestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      public int executeUdp( )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         return A74Contrato_Codigo ;
      }

      public void executeSubmit( ref int aP0_Contrato_Codigo )
      {
         prc_inscontratogestor objprc_inscontratogestor;
         objprc_inscontratogestor = new prc_inscontratogestor();
         objprc_inscontratogestor.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_inscontratogestor.context.SetSubmitInitialConfig(context);
         objprc_inscontratogestor.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_inscontratogestor);
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_inscontratogestor)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00XG2 */
         pr_default.execute(0, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = P00XG2_A52Contratada_AreaTrabalhoCod[0];
            A39Contratada_Codigo = P00XG2_A39Contratada_Codigo[0];
            A29Contratante_Codigo = P00XG2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00XG2_n29Contratante_Codigo[0];
            A1013Contrato_PrepostoCod = P00XG2_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P00XG2_n1013Contrato_PrepostoCod[0];
            A52Contratada_AreaTrabalhoCod = P00XG2_A52Contratada_AreaTrabalhoCod[0];
            A29Contratante_Codigo = P00XG2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00XG2_n29Contratante_Codigo[0];
            AV8Contratada_Codigo = A39Contratada_Codigo;
            AV9Contratante_Codigo = A29Contratante_Codigo;
            AV10Usuario_Codigo = A1013Contrato_PrepostoCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         AV14GXLvl9 = 0;
         /* Using cursor P00XG3 */
         pr_default.execute(1, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1078ContratoGestor_ContratoCod = P00XG3_A1078ContratoGestor_ContratoCod[0];
            A1079ContratoGestor_UsuarioCod = P00XG3_A1079ContratoGestor_UsuarioCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00XG3_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00XG3_n1446ContratoGestor_ContratadaAreaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00XG3_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00XG3_n1446ContratoGestor_ContratadaAreaCod[0];
            GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
            if ( A1135ContratoGestor_UsuarioEhContratante )
            {
               AV14GXLvl9 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV14GXLvl9 == 0 )
         {
            /* Using cursor P00XG4 */
            pr_default.execute(2, new Object[] {AV9Contratante_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A54Usuario_Ativo = P00XG4_A54Usuario_Ativo[0];
               n54Usuario_Ativo = P00XG4_n54Usuario_Ativo[0];
               A63ContratanteUsuario_ContratanteCod = P00XG4_A63ContratanteUsuario_ContratanteCod[0];
               A60ContratanteUsuario_UsuarioCod = P00XG4_A60ContratanteUsuario_UsuarioCod[0];
               A54Usuario_Ativo = P00XG4_A54Usuario_Ativo[0];
               n54Usuario_Ativo = P00XG4_n54Usuario_Ativo[0];
               AV10Usuario_Codigo = A60ContratanteUsuario_UsuarioCod;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Execute user subroutine: 'NEWGESTOR' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV16GXLvl22 = 0;
         /* Using cursor P00XG5 */
         pr_default.execute(3, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1078ContratoGestor_ContratoCod = P00XG5_A1078ContratoGestor_ContratoCod[0];
            A1079ContratoGestor_UsuarioCod = P00XG5_A1079ContratoGestor_UsuarioCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00XG5_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00XG5_n1446ContratoGestor_ContratadaAreaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = P00XG5_A1446ContratoGestor_ContratadaAreaCod[0];
            n1446ContratoGestor_ContratadaAreaCod = P00XG5_n1446ContratoGestor_ContratadaAreaCod[0];
            GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
            if ( ! A1135ContratoGestor_UsuarioEhContratante )
            {
               AV16GXLvl22 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( AV16GXLvl22 == 0 )
         {
            if ( (0==AV10Usuario_Codigo) )
            {
               /* Using cursor P00XG6 */
               pr_default.execute(4, new Object[] {AV8Contratada_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1394ContratadaUsuario_UsuarioAtivo = P00XG6_A1394ContratadaUsuario_UsuarioAtivo[0];
                  n1394ContratadaUsuario_UsuarioAtivo = P00XG6_n1394ContratadaUsuario_UsuarioAtivo[0];
                  A66ContratadaUsuario_ContratadaCod = P00XG6_A66ContratadaUsuario_ContratadaCod[0];
                  A69ContratadaUsuario_UsuarioCod = P00XG6_A69ContratadaUsuario_UsuarioCod[0];
                  A1394ContratadaUsuario_UsuarioAtivo = P00XG6_A1394ContratadaUsuario_UsuarioAtivo[0];
                  n1394ContratadaUsuario_UsuarioAtivo = P00XG6_n1394ContratadaUsuario_UsuarioAtivo[0];
                  AV10Usuario_Codigo = A69ContratadaUsuario_UsuarioCod;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
            }
            /* Execute user subroutine: 'NEWGESTOR' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NEWGESTOR' Routine */
         if ( ! (0==AV10Usuario_Codigo) )
         {
            /*
               INSERT RECORD ON TABLE ContratoGestor

            */
            A1078ContratoGestor_ContratoCod = A74Contrato_Codigo;
            A1079ContratoGestor_UsuarioCod = AV10Usuario_Codigo;
            A2009ContratoGestor_Tipo = 1;
            n2009ContratoGestor_Tipo = false;
            /* Using cursor P00XG7 */
            pr_default.execute(5, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod, n2009ContratoGestor_Tipo, A2009ContratoGestor_Tipo});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
            if ( (pr_default.getStatus(5) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_INSContratoGestor");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XG2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00XG2_A74Contrato_Codigo = new int[1] ;
         P00XG2_A39Contratada_Codigo = new int[1] ;
         P00XG2_A29Contratante_Codigo = new int[1] ;
         P00XG2_n29Contratante_Codigo = new bool[] {false} ;
         P00XG2_A1013Contrato_PrepostoCod = new int[1] ;
         P00XG2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00XG3_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00XG3_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00XG3_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00XG3_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00XG4_A54Usuario_Ativo = new bool[] {false} ;
         P00XG4_n54Usuario_Ativo = new bool[] {false} ;
         P00XG4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00XG4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00XG5_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00XG5_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00XG5_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00XG5_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00XG6_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00XG6_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         P00XG6_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00XG6_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_inscontratogestor__default(),
            new Object[][] {
                new Object[] {
               P00XG2_A52Contratada_AreaTrabalhoCod, P00XG2_A74Contrato_Codigo, P00XG2_A39Contratada_Codigo, P00XG2_A29Contratante_Codigo, P00XG2_n29Contratante_Codigo, P00XG2_A1013Contrato_PrepostoCod, P00XG2_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               P00XG3_A1078ContratoGestor_ContratoCod, P00XG3_A1079ContratoGestor_UsuarioCod, P00XG3_A1446ContratoGestor_ContratadaAreaCod, P00XG3_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00XG4_A54Usuario_Ativo, P00XG4_n54Usuario_Ativo, P00XG4_A63ContratanteUsuario_ContratanteCod, P00XG4_A60ContratanteUsuario_UsuarioCod
               }
               , new Object[] {
               P00XG5_A1078ContratoGestor_ContratoCod, P00XG5_A1079ContratoGestor_UsuarioCod, P00XG5_A1446ContratoGestor_ContratadaAreaCod, P00XG5_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00XG6_A1394ContratadaUsuario_UsuarioAtivo, P00XG6_n1394ContratadaUsuario_UsuarioAtivo, P00XG6_A66ContratadaUsuario_ContratadaCod, P00XG6_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14GXLvl9 ;
      private short AV16GXLvl22 ;
      private short A2009ContratoGestor_Tipo ;
      private int A74Contrato_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A29Contratante_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int AV8Contratada_Codigo ;
      private int AV9Contratante_Codigo ;
      private int AV10Usuario_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int GX_INS129 ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private bool n29Contratante_Codigo ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool returnInSub ;
      private bool GXt_boolean1 ;
      private bool A1394ContratadaUsuario_UsuarioAtivo ;
      private bool n1394ContratadaUsuario_UsuarioAtivo ;
      private bool n2009ContratoGestor_Tipo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00XG2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00XG2_A74Contrato_Codigo ;
      private int[] P00XG2_A39Contratada_Codigo ;
      private int[] P00XG2_A29Contratante_Codigo ;
      private bool[] P00XG2_n29Contratante_Codigo ;
      private int[] P00XG2_A1013Contrato_PrepostoCod ;
      private bool[] P00XG2_n1013Contrato_PrepostoCod ;
      private int[] P00XG3_A1078ContratoGestor_ContratoCod ;
      private int[] P00XG3_A1079ContratoGestor_UsuarioCod ;
      private int[] P00XG3_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00XG3_n1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00XG4_A54Usuario_Ativo ;
      private bool[] P00XG4_n54Usuario_Ativo ;
      private int[] P00XG4_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00XG4_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00XG5_A1078ContratoGestor_ContratoCod ;
      private int[] P00XG5_A1079ContratoGestor_UsuarioCod ;
      private int[] P00XG5_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00XG5_n1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00XG6_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] P00XG6_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] P00XG6_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00XG6_A69ContratadaUsuario_UsuarioCod ;
   }

   public class prc_inscontratogestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XG2 ;
          prmP00XG2 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XG3 ;
          prmP00XG3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XG4 ;
          prmP00XG4 = new Object[] {
          new Object[] {"@AV9Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XG5 ;
          prmP00XG5 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XG6 ;
          prmP00XG6 = new Object[] {
          new Object[] {"@AV8Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XG7 ;
          prmP00XG7 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_Tipo",SqlDbType.SmallInt,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XG2", "SELECT TOP 1 T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contrato_Codigo], T1.[Contratada_Codigo], T3.[Contratante_Codigo], T1.[Contrato_PrepostoCod] FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T2.[Contratada_AreaTrabalhoCod]) WHERE T1.[Contrato_Codigo] = @Contrato_Codigo ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XG2,1,0,false,true )
             ,new CursorDef("P00XG3", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XG3,100,0,true,false )
             ,new CursorDef("P00XG4", "SELECT TOP 1 T2.[Usuario_Ativo], T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_ContratanteCod] = @AV9Contratante_Codigo) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XG4,1,0,false,true )
             ,new CursorDef("P00XG5", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XG5,100,0,true,false )
             ,new CursorDef("P00XG6", "SELECT TOP 1 T2.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_ContratadaCod] = @AV8Contratada_Codigo) AND (T2.[Usuario_Ativo] = 1) ORDER BY T1.[ContratadaUsuario_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XG6,1,0,false,true )
             ,new CursorDef("P00XG7", "INSERT INTO [ContratoGestor]([ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod], [ContratoGestor_Tipo]) VALUES(@ContratoGestor_ContratoCod, @ContratoGestor_UsuarioCod, @ContratoGestor_Tipo)", GxErrorMask.GX_NOMASK,prmP00XG7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[3]);
                }
                return;
       }
    }

 }

}
