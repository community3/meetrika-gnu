/*
               File: PRC_SaldoContratoInicializar
        Description: Inicializar Saldo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:39.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saldocontratoinicializar : GXProcedure
   {
      public prc_saldocontratoinicializar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saldocontratoinicializar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           out bool aP1_isErro )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV12isErro = false ;
         initialize();
         executePrivate();
         aP1_isErro=this.AV12isErro;
      }

      public bool executeUdp( int aP0_Contrato_Codigo )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV12isErro = false ;
         initialize();
         executePrivate();
         aP1_isErro=this.AV12isErro;
         return AV12isErro ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 out bool aP1_isErro )
      {
         prc_saldocontratoinicializar objprc_saldocontratoinicializar;
         objprc_saldocontratoinicializar = new prc_saldocontratoinicializar();
         objprc_saldocontratoinicializar.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_saldocontratoinicializar.AV12isErro = false ;
         objprc_saldocontratoinicializar.context.SetSubmitInitialConfig(context);
         objprc_saldocontratoinicializar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saldocontratoinicializar);
         aP1_isErro=this.AV12isErro;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saldocontratoinicializar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12isErro = true;
         AV15GXLvl4 = 0;
         /* Using cursor P00BJ2 */
         pr_default.execute(0, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A74Contrato_Codigo = P00BJ2_A74Contrato_Codigo[0];
            A316ContratoTermoAditivo_DataInicio = P00BJ2_A316ContratoTermoAditivo_DataInicio[0];
            n316ContratoTermoAditivo_DataInicio = P00BJ2_n316ContratoTermoAditivo_DataInicio[0];
            A317ContratoTermoAditivo_DataFim = P00BJ2_A317ContratoTermoAditivo_DataFim[0];
            n317ContratoTermoAditivo_DataFim = P00BJ2_n317ContratoTermoAditivo_DataFim[0];
            A315ContratoTermoAditivo_Codigo = P00BJ2_A315ContratoTermoAditivo_Codigo[0];
            AV15GXLvl4 = 1;
            AV9SaldoContrato_VigenciaInicio = A316ContratoTermoAditivo_DataInicio;
            AV10SaldoContrato_VigenciaFim = A317ContratoTermoAditivo_DataFim;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV15GXLvl4 == 0 )
         {
            /* Using cursor P00BJ3 */
            pr_default.execute(1, new Object[] {AV8Contrato_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = P00BJ3_A74Contrato_Codigo[0];
               A82Contrato_DataVigenciaInicio = P00BJ3_A82Contrato_DataVigenciaInicio[0];
               A83Contrato_DataVigenciaTermino = P00BJ3_A83Contrato_DataVigenciaTermino[0];
               AV9SaldoContrato_VigenciaInicio = A82Contrato_DataVigenciaInicio;
               AV10SaldoContrato_VigenciaFim = A83Contrato_DataVigenciaTermino;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         if ( ! ( (DateTime.MinValue==AV9SaldoContrato_VigenciaInicio) || (DateTime.MinValue==AV10SaldoContrato_VigenciaFim) ) )
         {
            /* Using cursor P00BJ4 */
            pr_default.execute(2, new Object[] {AV8Contrato_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1207ContratoUnidades_ContratoCod = P00BJ4_A1207ContratoUnidades_ContratoCod[0];
               A1204ContratoUnidades_UndMedCod = P00BJ4_A1204ContratoUnidades_UndMedCod[0];
               new prc_saldocontratocriar(context ).execute(  AV8Contrato_Codigo,  A1204ContratoUnidades_UndMedCod,  AV9SaldoContrato_VigenciaInicio,  AV10SaldoContrato_VigenciaFim, out  AV11SaldoContrato_Codigo) ;
               if ( AV11SaldoContrato_Codigo > 0 )
               {
                  AV12isErro = false;
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BJ2_A74Contrato_Codigo = new int[1] ;
         P00BJ2_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         P00BJ2_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         P00BJ2_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00BJ2_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         P00BJ2_A315ContratoTermoAditivo_Codigo = new int[1] ;
         A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         AV9SaldoContrato_VigenciaInicio = DateTime.MinValue;
         AV10SaldoContrato_VigenciaFim = DateTime.MinValue;
         P00BJ3_A74Contrato_Codigo = new int[1] ;
         P00BJ3_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00BJ3_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         P00BJ4_A1207ContratoUnidades_ContratoCod = new int[1] ;
         P00BJ4_A1204ContratoUnidades_UndMedCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saldocontratoinicializar__default(),
            new Object[][] {
                new Object[] {
               P00BJ2_A74Contrato_Codigo, P00BJ2_A316ContratoTermoAditivo_DataInicio, P00BJ2_n316ContratoTermoAditivo_DataInicio, P00BJ2_A317ContratoTermoAditivo_DataFim, P00BJ2_n317ContratoTermoAditivo_DataFim, P00BJ2_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               P00BJ3_A74Contrato_Codigo, P00BJ3_A82Contrato_DataVigenciaInicio, P00BJ3_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               P00BJ4_A1207ContratoUnidades_ContratoCod, P00BJ4_A1204ContratoUnidades_UndMedCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV15GXLvl4 ;
      private int AV8Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A315ContratoTermoAditivo_Codigo ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int AV11SaldoContrato_Codigo ;
      private String scmdbuf ;
      private DateTime A316ContratoTermoAditivo_DataInicio ;
      private DateTime A317ContratoTermoAditivo_DataFim ;
      private DateTime AV9SaldoContrato_VigenciaInicio ;
      private DateTime AV10SaldoContrato_VigenciaFim ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private bool AV12isErro ;
      private bool n316ContratoTermoAditivo_DataInicio ;
      private bool n317ContratoTermoAditivo_DataFim ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BJ2_A74Contrato_Codigo ;
      private DateTime[] P00BJ2_A316ContratoTermoAditivo_DataInicio ;
      private bool[] P00BJ2_n316ContratoTermoAditivo_DataInicio ;
      private DateTime[] P00BJ2_A317ContratoTermoAditivo_DataFim ;
      private bool[] P00BJ2_n317ContratoTermoAditivo_DataFim ;
      private int[] P00BJ2_A315ContratoTermoAditivo_Codigo ;
      private int[] P00BJ3_A74Contrato_Codigo ;
      private DateTime[] P00BJ3_A82Contrato_DataVigenciaInicio ;
      private DateTime[] P00BJ3_A83Contrato_DataVigenciaTermino ;
      private int[] P00BJ4_A1207ContratoUnidades_ContratoCod ;
      private int[] P00BJ4_A1204ContratoUnidades_UndMedCod ;
      private bool aP1_isErro ;
   }

   public class prc_saldocontratoinicializar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BJ2 ;
          prmP00BJ2 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BJ3 ;
          prmP00BJ3 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BJ4 ;
          prmP00BJ4 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BJ2", "SELECT TOP 1 [Contrato_Codigo], [ContratoTermoAditivo_DataInicio], [ContratoTermoAditivo_DataFim], [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [ContratoTermoAditivo_DataFim] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BJ2,1,0,false,true )
             ,new CursorDef("P00BJ3", "SELECT [Contrato_Codigo], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BJ3,1,0,false,true )
             ,new CursorDef("P00BJ4", "SELECT [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @AV8Contrato_Codigo ORDER BY [ContratoUnidades_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BJ4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
