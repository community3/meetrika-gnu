/*
               File: ContagemResultadoNotificacaoGeneral
        Description: Contagem Resultado Notificacao General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:17.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadonotificacaogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadonotificacaogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadonotificacaogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( long aP0_ContagemResultadoNotificacao_Codigo )
      {
         this.A1412ContagemResultadoNotificacao_Codigo = aP0_ContagemResultadoNotificacao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagemResultadoNotificacao_Midia = new GXCombobox();
         cmbContagemResultadoNotificacao_Aut = new GXCombobox();
         cmbContagemResultadoNotificacao_Sec = new GXCombobox();
         cmbContagemResultadoNotificacao_Logged = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1412ContagemResultadoNotificacao_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(long)A1412ContagemResultadoNotificacao_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAQU2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "ContagemResultadoNotificacaoGeneral";
               context.Gx_err = 0;
               WSQU2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Notificacao General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117281726");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadonotificacaogeneral.aspx") + "?" + UrlEncode("" +A1412ContagemResultadoNotificacao_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO", A1415ContagemResultadoNotificacao_Observacao);
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO", GetSecureSignedToken( sPrefix, A1417ContagemResultadoNotificacao_Assunto));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1418ContagemResultadoNotificacao_CorpoEmail, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_PORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( sPrefix, A1959ContagemResultadoNotificacao_Aut));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_USUCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO", GetSecureSignedToken( sPrefix, A1417ContagemResultadoNotificacao_Assunto));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1418ContagemResultadoNotificacao_CorpoEmail, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_PORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( sPrefix, A1959ContagemResultadoNotificacao_Aut));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADONOTIFICACAO_USUCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultadonotificacao_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContagemResultadoNotificacaoGeneral";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_Host:"+StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!")));
         GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_User:"+StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!")));
         GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_Port:"+context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9"));
         GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_Aut:"+StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut));
         GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_Sec:"+context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9"));
         GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SendSecurityCheck value for]"+"ContagemResultadoNotificacao_UsuCod:"+context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormQU2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadonotificacaogeneral.js", "?20203117281732");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoNotificacaoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Notificacao General" ;
      }

      protected void WBQU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadonotificacaogeneral.aspx");
               context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
               context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
            }
            wb_table1_2_QU2( true) ;
         }
         else
         {
            wb_table1_2_QU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_QU2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")), context.localUtil.Format( (decimal)(A1412ContagemResultadoNotificacao_Codigo), "ZZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoNotificacao_Codigo_Visible, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "Codigo10", "right", false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_UsuCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_UsuCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultadoNotificacao_UsuCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTQU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Notificacao General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPQU0( ) ;
            }
         }
      }

      protected void WSQU2( )
      {
         STARTQU2( ) ;
         EVTQU2( ) ;
      }

      protected void EVTQU2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11QU2 */
                                    E11QU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12QU2 */
                                    E12QU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13QU2 */
                                    E13QU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14QU2 */
                                    E14QU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15QU2 */
                                    E15QU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPQU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEQU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormQU2( ) ;
            }
         }
      }

      protected void PAQU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContagemResultadoNotificacao_Midia.Name = "CONTAGEMRESULTADONOTIFICACAO_MIDIA";
            cmbContagemResultadoNotificacao_Midia.WebTags = "";
            cmbContagemResultadoNotificacao_Midia.addItem("EML", "Email", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("SMS", "SMS", 0);
            cmbContagemResultadoNotificacao_Midia.addItem("TEL", "Telefone", 0);
            if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
            {
               A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
               n1420ContagemResultadoNotificacao_Midia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
            }
            cmbContagemResultadoNotificacao_Aut.Name = "CONTAGEMRESULTADONOTIFICACAO_AUT";
            cmbContagemResultadoNotificacao_Aut.WebTags = "";
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbContagemResultadoNotificacao_Aut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
            {
               A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
               n1959ContagemResultadoNotificacao_Aut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( sPrefix, A1959ContagemResultadoNotificacao_Aut));
            }
            cmbContagemResultadoNotificacao_Sec.Name = "CONTAGEMRESULTADONOTIFICACAO_SEC";
            cmbContagemResultadoNotificacao_Sec.WebTags = "";
            cmbContagemResultadoNotificacao_Sec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
            cmbContagemResultadoNotificacao_Sec.addItem("1", "SSL e TLS", 0);
            if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
            {
               A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
               n1960ContagemResultadoNotificacao_Sec = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
            }
            cmbContagemResultadoNotificacao_Logged.Name = "CONTAGEMRESULTADONOTIFICACAO_LOGGED";
            cmbContagemResultadoNotificacao_Logged.WebTags = "";
            cmbContagemResultadoNotificacao_Logged.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Sem dados", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("1", "Com sucesso", 0);
            cmbContagemResultadoNotificacao_Logged.addItem("2", "Mal sucedido", 0);
            if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
            {
               A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
               n1961ContagemResultadoNotificacao_Logged = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagemResultadoNotificacao_Midia.ItemCount > 0 )
         {
            A1420ContagemResultadoNotificacao_Midia = cmbContagemResultadoNotificacao_Midia.getValidValue(A1420ContagemResultadoNotificacao_Midia);
            n1420ContagemResultadoNotificacao_Midia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
         }
         if ( cmbContagemResultadoNotificacao_Aut.ItemCount > 0 )
         {
            A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cmbContagemResultadoNotificacao_Aut.getValidValue(StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut)));
            n1959ContagemResultadoNotificacao_Aut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( sPrefix, A1959ContagemResultadoNotificacao_Aut));
         }
         if ( cmbContagemResultadoNotificacao_Sec.ItemCount > 0 )
         {
            A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Sec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0))), "."));
            n1960ContagemResultadoNotificacao_Sec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
         }
         if ( cmbContagemResultadoNotificacao_Logged.ItemCount > 0 )
         {
            A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cmbContagemResultadoNotificacao_Logged.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0))), "."));
            n1961ContagemResultadoNotificacao_Logged = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFQU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "ContagemResultadoNotificacaoGeneral";
         context.Gx_err = 0;
      }

      protected void RFQU2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00QU2 */
            pr_default.execute(0, new Object[] {A1412ContagemResultadoNotificacao_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1427ContagemResultadoNotificacao_UsuPesCod = H00QU2_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = H00QU2_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1966ContagemResultadoNotificacao_AreaTrabalhoCod = H00QU2_A1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
               n1966ContagemResultadoNotificacao_AreaTrabalhoCod = H00QU2_n1966ContagemResultadoNotificacao_AreaTrabalhoCod[0];
               A29Contratante_Codigo = H00QU2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00QU2_n29Contratante_Codigo[0];
               A1956ContagemResultadoNotificacao_Host = H00QU2_A1956ContagemResultadoNotificacao_Host[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1956ContagemResultadoNotificacao_Host", A1956ContagemResultadoNotificacao_Host);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"))));
               n1956ContagemResultadoNotificacao_Host = H00QU2_n1956ContagemResultadoNotificacao_Host[0];
               A547Contratante_EmailSdaHost = H00QU2_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = H00QU2_n547Contratante_EmailSdaHost[0];
               A1957ContagemResultadoNotificacao_User = H00QU2_A1957ContagemResultadoNotificacao_User[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1957ContagemResultadoNotificacao_User", A1957ContagemResultadoNotificacao_User);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"))));
               n1957ContagemResultadoNotificacao_User = H00QU2_n1957ContagemResultadoNotificacao_User[0];
               A548Contratante_EmailSdaUser = H00QU2_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = H00QU2_n548Contratante_EmailSdaUser[0];
               A1958ContagemResultadoNotificacao_Port = H00QU2_A1958ContagemResultadoNotificacao_Port[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_PORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")));
               n1958ContagemResultadoNotificacao_Port = H00QU2_n1958ContagemResultadoNotificacao_Port[0];
               A552Contratante_EmailSdaPort = H00QU2_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = H00QU2_n552Contratante_EmailSdaPort[0];
               A1959ContagemResultadoNotificacao_Aut = H00QU2_A1959ContagemResultadoNotificacao_Aut[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( sPrefix, A1959ContagemResultadoNotificacao_Aut));
               n1959ContagemResultadoNotificacao_Aut = H00QU2_n1959ContagemResultadoNotificacao_Aut[0];
               A551Contratante_EmailSdaAut = H00QU2_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = H00QU2_n551Contratante_EmailSdaAut[0];
               A1960ContagemResultadoNotificacao_Sec = H00QU2_A1960ContagemResultadoNotificacao_Sec[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
               n1960ContagemResultadoNotificacao_Sec = H00QU2_n1960ContagemResultadoNotificacao_Sec[0];
               A1048Contratante_EmailSdaSec = H00QU2_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = H00QU2_n1048Contratante_EmailSdaSec[0];
               A1413ContagemResultadoNotificacao_UsuCod = H00QU2_A1413ContagemResultadoNotificacao_UsuCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USUCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")));
               A1961ContagemResultadoNotificacao_Logged = H00QU2_A1961ContagemResultadoNotificacao_Logged[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
               n1961ContagemResultadoNotificacao_Logged = H00QU2_n1961ContagemResultadoNotificacao_Logged[0];
               A1415ContagemResultadoNotificacao_Observacao = H00QU2_A1415ContagemResultadoNotificacao_Observacao[0];
               n1415ContagemResultadoNotificacao_Observacao = H00QU2_n1415ContagemResultadoNotificacao_Observacao[0];
               A1420ContagemResultadoNotificacao_Midia = H00QU2_A1420ContagemResultadoNotificacao_Midia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
               n1420ContagemResultadoNotificacao_Midia = H00QU2_n1420ContagemResultadoNotificacao_Midia[0];
               A1418ContagemResultadoNotificacao_CorpoEmail = H00QU2_A1418ContagemResultadoNotificacao_CorpoEmail[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1418ContagemResultadoNotificacao_CorpoEmail", A1418ContagemResultadoNotificacao_CorpoEmail);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1418ContagemResultadoNotificacao_CorpoEmail, ""))));
               n1418ContagemResultadoNotificacao_CorpoEmail = H00QU2_n1418ContagemResultadoNotificacao_CorpoEmail[0];
               A1417ContagemResultadoNotificacao_Assunto = H00QU2_A1417ContagemResultadoNotificacao_Assunto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1417ContagemResultadoNotificacao_Assunto", A1417ContagemResultadoNotificacao_Assunto);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO", GetSecureSignedToken( sPrefix, A1417ContagemResultadoNotificacao_Assunto));
               n1417ContagemResultadoNotificacao_Assunto = H00QU2_n1417ContagemResultadoNotificacao_Assunto[0];
               A1422ContagemResultadoNotificacao_UsuNom = H00QU2_A1422ContagemResultadoNotificacao_UsuNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
               n1422ContagemResultadoNotificacao_UsuNom = H00QU2_n1422ContagemResultadoNotificacao_UsuNom[0];
               A1416ContagemResultadoNotificacao_DataHora = H00QU2_A1416ContagemResultadoNotificacao_DataHora[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99")));
               n1416ContagemResultadoNotificacao_DataHora = H00QU2_n1416ContagemResultadoNotificacao_DataHora[0];
               A29Contratante_Codigo = H00QU2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00QU2_n29Contratante_Codigo[0];
               A547Contratante_EmailSdaHost = H00QU2_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = H00QU2_n547Contratante_EmailSdaHost[0];
               A548Contratante_EmailSdaUser = H00QU2_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = H00QU2_n548Contratante_EmailSdaUser[0];
               A552Contratante_EmailSdaPort = H00QU2_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = H00QU2_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = H00QU2_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = H00QU2_n551Contratante_EmailSdaAut[0];
               A1048Contratante_EmailSdaSec = H00QU2_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = H00QU2_n1048Contratante_EmailSdaSec[0];
               A1427ContagemResultadoNotificacao_UsuPesCod = H00QU2_A1427ContagemResultadoNotificacao_UsuPesCod[0];
               n1427ContagemResultadoNotificacao_UsuPesCod = H00QU2_n1427ContagemResultadoNotificacao_UsuPesCod[0];
               A1422ContagemResultadoNotificacao_UsuNom = H00QU2_A1422ContagemResultadoNotificacao_UsuNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
               n1422ContagemResultadoNotificacao_UsuNom = H00QU2_n1422ContagemResultadoNotificacao_UsuNom[0];
               /* Execute user event: E12QU2 */
               E12QU2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBQU0( ) ;
         }
      }

      protected void STRUPQU0( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "ContagemResultadoNotificacaoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11QU2 */
         E11QU2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1416ContagemResultadoNotificacao_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoNotificacao_DataHora_Internalname), 0);
            n1416ContagemResultadoNotificacao_DataHora = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1416ContagemResultadoNotificacao_DataHora", context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_DATAHORA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99")));
            A1422ContagemResultadoNotificacao_UsuNom = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_UsuNom_Internalname));
            n1422ContagemResultadoNotificacao_UsuNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1422ContagemResultadoNotificacao_UsuNom", A1422ContagemResultadoNotificacao_UsuNom);
            A1417ContagemResultadoNotificacao_Assunto = cgiGet( edtContagemResultadoNotificacao_Assunto_Internalname);
            n1417ContagemResultadoNotificacao_Assunto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1417ContagemResultadoNotificacao_Assunto", A1417ContagemResultadoNotificacao_Assunto);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_ASSUNTO", GetSecureSignedToken( sPrefix, A1417ContagemResultadoNotificacao_Assunto));
            A1418ContagemResultadoNotificacao_CorpoEmail = cgiGet( edtContagemResultadoNotificacao_CorpoEmail_Internalname);
            n1418ContagemResultadoNotificacao_CorpoEmail = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1418ContagemResultadoNotificacao_CorpoEmail", A1418ContagemResultadoNotificacao_CorpoEmail);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1418ContagemResultadoNotificacao_CorpoEmail, ""))));
            cmbContagemResultadoNotificacao_Midia.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Midia_Internalname);
            A1420ContagemResultadoNotificacao_Midia = cgiGet( cmbContagemResultadoNotificacao_Midia_Internalname);
            n1420ContagemResultadoNotificacao_Midia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1420ContagemResultadoNotificacao_Midia", A1420ContagemResultadoNotificacao_Midia);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_MIDIA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1420ContagemResultadoNotificacao_Midia, ""))));
            A1956ContagemResultadoNotificacao_Host = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_Host_Internalname));
            n1956ContagemResultadoNotificacao_Host = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1956ContagemResultadoNotificacao_Host", A1956ContagemResultadoNotificacao_Host);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"))));
            A1957ContagemResultadoNotificacao_User = StringUtil.Upper( cgiGet( edtContagemResultadoNotificacao_User_Internalname));
            n1957ContagemResultadoNotificacao_User = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1957ContagemResultadoNotificacao_User", A1957ContagemResultadoNotificacao_User);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"))));
            A1958ContagemResultadoNotificacao_Port = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Port_Internalname), ",", "."));
            n1958ContagemResultadoNotificacao_Port = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_PORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")));
            cmbContagemResultadoNotificacao_Aut.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname);
            A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname));
            n1959ContagemResultadoNotificacao_Aut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( sPrefix, A1959ContagemResultadoNotificacao_Aut));
            cmbContagemResultadoNotificacao_Sec.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname);
            A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname), "."));
            n1960ContagemResultadoNotificacao_Sec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
            cmbContagemResultadoNotificacao_Logged.CurrentValue = cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname);
            A1961ContagemResultadoNotificacao_Logged = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Logged_Internalname), "."));
            n1961ContagemResultadoNotificacao_Logged = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1961ContagemResultadoNotificacao_Logged", StringUtil.LTrim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_LOGGED", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1961ContagemResultadoNotificacao_Logged), "ZZZ9")));
            A1413ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USUCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")));
            /* Read saved values. */
            A1415ContagemResultadoNotificacao_Observacao = cgiGet( sPrefix+"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO");
            wcpOA1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1412ContagemResultadoNotificacao_Codigo"), ",", "."));
            Contagemresultadonotificacao_observacao_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContagemResultadoNotificacaoGeneral";
            A1956ContagemResultadoNotificacao_Host = cgiGet( edtContagemResultadoNotificacao_Host_Internalname);
            n1956ContagemResultadoNotificacao_Host = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1956ContagemResultadoNotificacao_Host", A1956ContagemResultadoNotificacao_Host);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!"));
            A1957ContagemResultadoNotificacao_User = cgiGet( edtContagemResultadoNotificacao_User_Internalname);
            n1957ContagemResultadoNotificacao_User = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1957ContagemResultadoNotificacao_User", A1957ContagemResultadoNotificacao_User);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!"));
            A1958ContagemResultadoNotificacao_Port = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_Port_Internalname), ",", "."));
            n1958ContagemResultadoNotificacao_Port = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1958ContagemResultadoNotificacao_Port", StringUtil.LTrim( StringUtil.Str( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_PORT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9");
            A1959ContagemResultadoNotificacao_Aut = StringUtil.StrToBool( cgiGet( cmbContagemResultadoNotificacao_Aut_Internalname));
            n1959ContagemResultadoNotificacao_Aut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1959ContagemResultadoNotificacao_Aut", A1959ContagemResultadoNotificacao_Aut);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_AUT", GetSecureSignedToken( sPrefix, A1959ContagemResultadoNotificacao_Aut));
            forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut);
            A1960ContagemResultadoNotificacao_Sec = (short)(NumberUtil.Val( cgiGet( cmbContagemResultadoNotificacao_Sec_Internalname), "."));
            n1960ContagemResultadoNotificacao_Sec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1960ContagemResultadoNotificacao_Sec", StringUtil.LTrim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_SEC", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9");
            A1413ContagemResultadoNotificacao_UsuCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoNotificacao_UsuCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1413ContagemResultadoNotificacao_UsuCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADONOTIFICACAO_USUCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_Host:"+StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!")));
               GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_User:"+StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!")));
               GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_Port:"+context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9"));
               GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_Aut:"+StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut));
               GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_Sec:"+context.localUtil.Format( (decimal)(A1960ContagemResultadoNotificacao_Sec), "ZZZ9"));
               GXUtil.WriteLog("contagemresultadonotificacaogeneral:[SecurityCheckFailed value for]"+"ContagemResultadoNotificacao_UsuCod:"+context.localUtil.Format( (decimal)(A1413ContagemResultadoNotificacao_UsuCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11QU2 */
         E11QU2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11QU2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV12WebSession.Set("Caller", "NtfDds");
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12QU2( )
      {
         /* Load Routine */
         edtContagemResultadoNotificacao_UsuNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1413ContagemResultadoNotificacao_UsuCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_UsuNom_Internalname, "Link", edtContagemResultadoNotificacao_UsuNom_Link);
         edtContagemResultadoNotificacao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_Codigo_Visible), 5, 0)));
         edtContagemResultadoNotificacao_UsuCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_UsuCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoNotificacao_UsuCod_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
         if ( AV6WWPContext.gxTpr_Contratante_codigo > 0 )
         {
            GXt_int1 = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            if ( ! new prc_usuarioehcontratantedaarea(context).executeUdp( ref  GXt_int1, ref  A1413ContagemResultadoNotificacao_UsuCod) )
            {
               edtContagemResultadoNotificacao_UsuNom_Link = "";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_UsuNom_Internalname, "Link", edtContagemResultadoNotificacao_UsuNom_Link);
            }
         }
         else if ( AV6WWPContext.gxTpr_Contratada_codigo > 0 )
         {
            if ( ! new prc_usuarioehcontratada(context).executeUdp(  AV6WWPContext.gxTpr_Contratada_codigo,  A1413ContagemResultadoNotificacao_UsuCod) )
            {
               edtContagemResultadoNotificacao_UsuNom_Link = "";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultadoNotificacao_UsuNom_Internalname, "Link", edtContagemResultadoNotificacao_UsuNom_Link);
            }
         }
         /* Using cursor H00QU3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A5AreaTrabalho_Codigo = H00QU3_A5AreaTrabalho_Codigo[0];
            if ( StringUtil.StrCmp(A547Contratante_EmailSdaHost, A1956ContagemResultadoNotificacao_Host) != 0 )
            {
               imgWh_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWh_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWh_Visible), 5, 0)));
               imgWh_Tooltiptext = "Alterado para "+A547Contratante_EmailSdaHost;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWh_Internalname, "Tooltiptext", imgWh_Tooltiptext);
            }
            else
            {
               imgWh_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWh_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWh_Visible), 5, 0)));
            }
            if ( StringUtil.StrCmp(A548Contratante_EmailSdaUser, A1957ContagemResultadoNotificacao_User) != 0 )
            {
               imgWu_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWu_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWu_Visible), 5, 0)));
               imgWu_Tooltiptext = "Alterado para "+A548Contratante_EmailSdaUser;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWu_Internalname, "Tooltiptext", imgWu_Tooltiptext);
            }
            else
            {
               imgWu_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWu_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWu_Visible), 5, 0)));
            }
            if ( A552Contratante_EmailSdaPort != A1958ContagemResultadoNotificacao_Port )
            {
               imgWp_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWp_Visible), 5, 0)));
               imgWp_Tooltiptext = "Alterada para "+StringUtil.Trim( StringUtil.Str( (decimal)(A552Contratante_EmailSdaPort), 4, 0));
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWp_Internalname, "Tooltiptext", imgWp_Tooltiptext);
            }
            else
            {
               imgWp_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWp_Visible), 5, 0)));
            }
            if ( A551Contratante_EmailSdaAut != A1959ContagemResultadoNotificacao_Aut )
            {
               imgWa_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWa_Visible), 5, 0)));
               imgWa_Tooltiptext = "Alterado para "+(A551Contratante_EmailSdaAut ? "Sim" : "N�o");
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWa_Internalname, "Tooltiptext", imgWa_Tooltiptext);
            }
            else
            {
               imgWa_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWa_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWa_Visible), 5, 0)));
            }
            if ( A1048Contratante_EmailSdaSec != A1960ContagemResultadoNotificacao_Sec )
            {
               imgWs_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWs_Visible), 5, 0)));
               imgWs_Tooltiptext = "Alterada para "+((A1048Contratante_EmailSdaSec==1) ? "SSL e TLS" : "Nenhuma");
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWs_Internalname, "Tooltiptext", imgWs_Tooltiptext);
            }
            else
            {
               imgWs_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgWs_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgWs_Visible), 5, 0)));
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void E13QU2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagemresultadonotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1412ContagemResultadoNotificacao_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14QU2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagemresultadonotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1412ContagemResultadoNotificacao_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15QU2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwcontagemresultadonotificacao.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultadoNotificacao";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContagemResultadoNotificacao_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultadoNotificacao_Codigo), 10, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_QU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_QU2( true) ;
         }
         else
         {
            wb_table2_8_QU2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_QU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_91_QU2( true) ;
         }
         else
         {
            wb_table3_91_QU2( false) ;
         }
         return  ;
      }

      protected void wb_table3_91_QU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_QU2e( true) ;
         }
         else
         {
            wb_table1_2_QU2e( false) ;
         }
      }

      protected void wb_table3_91_QU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_91_QU2e( true) ;
         }
         else
         {
            wb_table3_91_QU2e( false) ;
         }
      }

      protected void wb_table2_8_QU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_datahora_Internalname, "Data", "", "", lblTextblockcontagemresultadonotificacao_datahora_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoNotificacao_DataHora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_DataHora_Internalname, context.localUtil.TToC( A1416ContagemResultadoNotificacao_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1416ContagemResultadoNotificacao_DataHora, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_DataHora_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoNotificacao_DataHora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_usunom_Internalname, "Usuario", "", "", lblTextblockcontagemresultadonotificacao_usunom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_UsuNom_Internalname, StringUtil.RTrim( A1422ContagemResultadoNotificacao_UsuNom), StringUtil.RTrim( context.localUtil.Format( A1422ContagemResultadoNotificacao_UsuNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemResultadoNotificacao_UsuNom_Link, "", "", "", edtContagemResultadoNotificacao_UsuNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_assunto_Internalname, "Assunto", "", "", lblTextblockcontagemresultadonotificacao_assunto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoNotificacao_Assunto_Internalname, A1417ContagemResultadoNotificacao_Assunto, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "1000", -1, "", "", -1, true, "Observacao", "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_corpoemail_Internalname, "Conte�do", "", "", lblTextblockcontagemresultadonotificacao_corpoemail_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoNotificacao_CorpoEmail_Internalname, A1418ContagemResultadoNotificacao_CorpoEmail, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "8000", -1, "", "", -1, true, "", "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_midia_Internalname, "Midia", "", "", lblTextblockcontagemresultadonotificacao_midia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Midia, cmbContagemResultadoNotificacao_Midia_Internalname, StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia), 1, cmbContagemResultadoNotificacao_Midia_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            cmbContagemResultadoNotificacao_Midia.CurrentValue = StringUtil.RTrim( A1420ContagemResultadoNotificacao_Midia);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Midia_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Midia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_observacao_Internalname, "Observa��o", "", "", lblTextblockcontagemresultadonotificacao_observacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"9\"  class='DataContentCellView'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_host_Internalname, "Host", "", "", lblTextblockcontagemresultadonotificacao_host_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_Host_Internalname, StringUtil.RTrim( A1956ContagemResultadoNotificacao_Host), StringUtil.RTrim( context.localUtil.Format( A1956ContagemResultadoNotificacao_Host, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_Host_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWh_Internalname, context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgWh_Visible, 1, "", imgWh_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 7, imgWh_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e16qu1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_user_Internalname, "Usu�rio", "", "", lblTextblockcontagemresultadonotificacao_user_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_User_Internalname, StringUtil.RTrim( A1957ContagemResultadoNotificacao_User), StringUtil.RTrim( context.localUtil.Format( A1957ContagemResultadoNotificacao_User, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_User_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWu_Internalname, context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgWu_Visible, 1, "", imgWu_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 7, imgWu_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e17qu1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_port_Internalname, "Porta", "", "", lblTextblockcontagemresultadonotificacao_port_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoNotificacao_Port_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1958ContagemResultadoNotificacao_Port), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A1958ContagemResultadoNotificacao_Port), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoNotificacao_Port_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWp_Internalname, context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgWp_Visible, 1, "", imgWp_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 7, imgWp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e18qu1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_aut_Internalname, "Autentica��o", "", "", lblTextblockcontagemresultadonotificacao_aut_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Aut, cmbContagemResultadoNotificacao_Aut_Internalname, StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut), 1, cmbContagemResultadoNotificacao_Aut_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            cmbContagemResultadoNotificacao_Aut.CurrentValue = StringUtil.BoolToStr( A1959ContagemResultadoNotificacao_Aut);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Aut_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Aut.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWa_Internalname, context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgWa_Visible, 1, "", imgWa_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 7, imgWa_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e19qu1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_sec_Internalname, "Seguran�a", "", "", lblTextblockcontagemresultadonotificacao_sec_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Sec, cmbContagemResultadoNotificacao_Sec_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0)), 1, cmbContagemResultadoNotificacao_Sec_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            cmbContagemResultadoNotificacao_Sec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1960ContagemResultadoNotificacao_Sec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Sec_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Sec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgWs_Internalname, context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgWs_Visible, 1, "", imgWs_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 7, imgWs_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e20qu1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadonotificacao_logged_Internalname, "Login", "", "", lblTextblockcontagemresultadonotificacao_logged_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoNotificacao_Logged, cmbContagemResultadoNotificacao_Logged_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0)), 1, cmbContagemResultadoNotificacao_Logged_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemResultadoNotificacaoGeneral.htm");
            cmbContagemResultadoNotificacao_Logged.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1961ContagemResultadoNotificacao_Logged), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultadoNotificacao_Logged_Internalname, "Values", (String)(cmbContagemResultadoNotificacao_Logged.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_QU2e( true) ;
         }
         else
         {
            wb_table2_8_QU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1412ContagemResultadoNotificacao_Codigo = Convert.ToInt64(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAQU2( ) ;
         WSQU2( ) ;
         WEQU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1412ContagemResultadoNotificacao_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAQU2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadonotificacaogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAQU2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1412ContagemResultadoNotificacao_Codigo = Convert.ToInt64(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
         }
         wcpOA1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1412ContagemResultadoNotificacao_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1412ContagemResultadoNotificacao_Codigo != wcpOA1412ContagemResultadoNotificacao_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1412ContagemResultadoNotificacao_Codigo = A1412ContagemResultadoNotificacao_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1412ContagemResultadoNotificacao_Codigo = cgiGet( sPrefix+"A1412ContagemResultadoNotificacao_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1412ContagemResultadoNotificacao_Codigo) > 0 )
         {
            A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sCtrlA1412ContagemResultadoNotificacao_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1412ContagemResultadoNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0)));
         }
         else
         {
            A1412ContagemResultadoNotificacao_Codigo = (long)(context.localUtil.CToN( cgiGet( sPrefix+"A1412ContagemResultadoNotificacao_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAQU2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSQU2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSQU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1412ContagemResultadoNotificacao_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1412ContagemResultadoNotificacao_Codigo), 10, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1412ContagemResultadoNotificacao_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1412ContagemResultadoNotificacao_Codigo_CTRL", StringUtil.RTrim( sCtrlA1412ContagemResultadoNotificacao_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEQU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117281826");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadonotificacaogeneral.js", "?20203117281826");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultadonotificacao_datahora_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         edtContagemResultadoNotificacao_DataHora_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_DATAHORA";
         lblTextblockcontagemresultadonotificacao_usunom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_USUNOM";
         edtContagemResultadoNotificacao_UsuNom_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUNOM";
         lblTextblockcontagemresultadonotificacao_assunto_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         edtContagemResultadoNotificacao_Assunto_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_ASSUNTO";
         lblTextblockcontagemresultadonotificacao_corpoemail_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL";
         edtContagemResultadoNotificacao_CorpoEmail_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CORPOEMAIL";
         lblTextblockcontagemresultadonotificacao_midia_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_MIDIA";
         cmbContagemResultadoNotificacao_Midia_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_MIDIA";
         lblTextblockcontagemresultadonotificacao_observacao_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_OBSERVACAO";
         Contagemresultadonotificacao_observacao_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_OBSERVACAO";
         lblTextblockcontagemresultadonotificacao_host_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_HOST";
         edtContagemResultadoNotificacao_Host_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_HOST";
         imgWh_Internalname = sPrefix+"WH";
         lblTextblockcontagemresultadonotificacao_user_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_USER";
         edtContagemResultadoNotificacao_User_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USER";
         imgWu_Internalname = sPrefix+"WU";
         lblTextblockcontagemresultadonotificacao_port_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_PORT";
         edtContagemResultadoNotificacao_Port_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_PORT";
         imgWp_Internalname = sPrefix+"WP";
         lblTextblockcontagemresultadonotificacao_aut_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_AUT";
         cmbContagemResultadoNotificacao_Aut_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_AUT";
         imgWa_Internalname = sPrefix+"WA";
         lblTextblockcontagemresultadonotificacao_sec_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_SEC";
         cmbContagemResultadoNotificacao_Sec_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_SEC";
         imgWs_Internalname = sPrefix+"WS";
         lblTextblockcontagemresultadonotificacao_logged_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADONOTIFICACAO_LOGGED";
         cmbContagemResultadoNotificacao_Logged_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_LOGGED";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagemResultadoNotificacao_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_CODIGO";
         edtContagemResultadoNotificacao_UsuCod_Internalname = sPrefix+"CONTAGEMRESULTADONOTIFICACAO_USUCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContagemResultadoNotificacao_Logged_Jsonclick = "";
         imgWs_Visible = 1;
         cmbContagemResultadoNotificacao_Sec_Jsonclick = "";
         imgWa_Visible = 1;
         cmbContagemResultadoNotificacao_Aut_Jsonclick = "";
         imgWp_Visible = 1;
         edtContagemResultadoNotificacao_Port_Jsonclick = "";
         imgWu_Visible = 1;
         edtContagemResultadoNotificacao_User_Jsonclick = "";
         imgWh_Visible = 1;
         edtContagemResultadoNotificacao_Host_Jsonclick = "";
         Contagemresultadonotificacao_observacao_Enabled = Convert.ToBoolean( 0);
         cmbContagemResultadoNotificacao_Midia_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuNom_Jsonclick = "";
         edtContagemResultadoNotificacao_DataHora_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtndelete_Visible = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnupdate_Visible = 1;
         imgWs_Tooltiptext = "Configura��o alterada";
         imgWa_Tooltiptext = "Configura��o alterada";
         imgWp_Tooltiptext = "Configura��o alterada";
         imgWu_Tooltiptext = "Configura��o alterada";
         imgWh_Tooltiptext = "Configura��o alterada";
         edtContagemResultadoNotificacao_UsuNom_Link = "";
         edtContagemResultadoNotificacao_UsuCod_Jsonclick = "";
         edtContagemResultadoNotificacao_UsuCod_Visible = 1;
         edtContagemResultadoNotificacao_Codigo_Jsonclick = "";
         edtContagemResultadoNotificacao_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13QU2',iparms:[{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14QU2',iparms:[{av:'A1412ContagemResultadoNotificacao_Codigo',fld:'CONTAGEMRESULTADONOTIFICACAO_CODIGO',pic:'ZZZZZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15QU2',iparms:[],oparms:[]}");
         setEventMetadata("'DOWH'","{handler:'E16QU1',iparms:[],oparms:[]}");
         setEventMetadata("'DOWU'","{handler:'E17QU1',iparms:[],oparms:[]}");
         setEventMetadata("'DOWP'","{handler:'E18QU1',iparms:[],oparms:[]}");
         setEventMetadata("'DOWA'","{handler:'E19QU1',iparms:[],oparms:[]}");
         setEventMetadata("'DOWS'","{handler:'E20QU1',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1415ContagemResultadoNotificacao_Observacao = "";
         A1416ContagemResultadoNotificacao_DataHora = (DateTime)(DateTime.MinValue);
         A1417ContagemResultadoNotificacao_Assunto = "";
         A1418ContagemResultadoNotificacao_CorpoEmail = "";
         A1420ContagemResultadoNotificacao_Midia = "";
         A1956ContagemResultadoNotificacao_Host = "";
         A1957ContagemResultadoNotificacao_User = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00QU2_A1427ContagemResultadoNotificacao_UsuPesCod = new int[1] ;
         H00QU2_n1427ContagemResultadoNotificacao_UsuPesCod = new bool[] {false} ;
         H00QU2_A1966ContagemResultadoNotificacao_AreaTrabalhoCod = new int[1] ;
         H00QU2_n1966ContagemResultadoNotificacao_AreaTrabalhoCod = new bool[] {false} ;
         H00QU2_A29Contratante_Codigo = new int[1] ;
         H00QU2_n29Contratante_Codigo = new bool[] {false} ;
         H00QU2_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         H00QU2_A1956ContagemResultadoNotificacao_Host = new String[] {""} ;
         H00QU2_n1956ContagemResultadoNotificacao_Host = new bool[] {false} ;
         H00QU2_A547Contratante_EmailSdaHost = new String[] {""} ;
         H00QU2_n547Contratante_EmailSdaHost = new bool[] {false} ;
         H00QU2_A1957ContagemResultadoNotificacao_User = new String[] {""} ;
         H00QU2_n1957ContagemResultadoNotificacao_User = new bool[] {false} ;
         H00QU2_A548Contratante_EmailSdaUser = new String[] {""} ;
         H00QU2_n548Contratante_EmailSdaUser = new bool[] {false} ;
         H00QU2_A1958ContagemResultadoNotificacao_Port = new short[1] ;
         H00QU2_n1958ContagemResultadoNotificacao_Port = new bool[] {false} ;
         H00QU2_A552Contratante_EmailSdaPort = new short[1] ;
         H00QU2_n552Contratante_EmailSdaPort = new bool[] {false} ;
         H00QU2_A1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         H00QU2_n1959ContagemResultadoNotificacao_Aut = new bool[] {false} ;
         H00QU2_A551Contratante_EmailSdaAut = new bool[] {false} ;
         H00QU2_n551Contratante_EmailSdaAut = new bool[] {false} ;
         H00QU2_A1960ContagemResultadoNotificacao_Sec = new short[1] ;
         H00QU2_n1960ContagemResultadoNotificacao_Sec = new bool[] {false} ;
         H00QU2_A1048Contratante_EmailSdaSec = new short[1] ;
         H00QU2_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         H00QU2_A1413ContagemResultadoNotificacao_UsuCod = new int[1] ;
         H00QU2_A1961ContagemResultadoNotificacao_Logged = new short[1] ;
         H00QU2_n1961ContagemResultadoNotificacao_Logged = new bool[] {false} ;
         H00QU2_A1415ContagemResultadoNotificacao_Observacao = new String[] {""} ;
         H00QU2_n1415ContagemResultadoNotificacao_Observacao = new bool[] {false} ;
         H00QU2_A1420ContagemResultadoNotificacao_Midia = new String[] {""} ;
         H00QU2_n1420ContagemResultadoNotificacao_Midia = new bool[] {false} ;
         H00QU2_A1418ContagemResultadoNotificacao_CorpoEmail = new String[] {""} ;
         H00QU2_n1418ContagemResultadoNotificacao_CorpoEmail = new bool[] {false} ;
         H00QU2_A1417ContagemResultadoNotificacao_Assunto = new String[] {""} ;
         H00QU2_n1417ContagemResultadoNotificacao_Assunto = new bool[] {false} ;
         H00QU2_A1422ContagemResultadoNotificacao_UsuNom = new String[] {""} ;
         H00QU2_n1422ContagemResultadoNotificacao_UsuNom = new bool[] {false} ;
         H00QU2_A1416ContagemResultadoNotificacao_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00QU2_n1416ContagemResultadoNotificacao_DataHora = new bool[] {false} ;
         A547Contratante_EmailSdaHost = "";
         A548Contratante_EmailSdaUser = "";
         A1422ContagemResultadoNotificacao_UsuNom = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12WebSession = context.GetSession();
         H00QU3_A5AreaTrabalho_Codigo = new int[1] ;
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_datahora_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_usunom_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_assunto_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_corpoemail_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_midia_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_observacao_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_host_Jsonclick = "";
         imgWh_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_user_Jsonclick = "";
         imgWu_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_port_Jsonclick = "";
         imgWp_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_aut_Jsonclick = "";
         imgWa_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_sec_Jsonclick = "";
         imgWs_Jsonclick = "";
         lblTextblockcontagemresultadonotificacao_logged_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1412ContagemResultadoNotificacao_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadonotificacaogeneral__default(),
            new Object[][] {
                new Object[] {
               H00QU2_A1427ContagemResultadoNotificacao_UsuPesCod, H00QU2_n1427ContagemResultadoNotificacao_UsuPesCod, H00QU2_A1966ContagemResultadoNotificacao_AreaTrabalhoCod, H00QU2_n1966ContagemResultadoNotificacao_AreaTrabalhoCod, H00QU2_A29Contratante_Codigo, H00QU2_n29Contratante_Codigo, H00QU2_A1412ContagemResultadoNotificacao_Codigo, H00QU2_A1956ContagemResultadoNotificacao_Host, H00QU2_n1956ContagemResultadoNotificacao_Host, H00QU2_A547Contratante_EmailSdaHost,
               H00QU2_n547Contratante_EmailSdaHost, H00QU2_A1957ContagemResultadoNotificacao_User, H00QU2_n1957ContagemResultadoNotificacao_User, H00QU2_A548Contratante_EmailSdaUser, H00QU2_n548Contratante_EmailSdaUser, H00QU2_A1958ContagemResultadoNotificacao_Port, H00QU2_n1958ContagemResultadoNotificacao_Port, H00QU2_A552Contratante_EmailSdaPort, H00QU2_n552Contratante_EmailSdaPort, H00QU2_A1959ContagemResultadoNotificacao_Aut,
               H00QU2_n1959ContagemResultadoNotificacao_Aut, H00QU2_A551Contratante_EmailSdaAut, H00QU2_n551Contratante_EmailSdaAut, H00QU2_A1960ContagemResultadoNotificacao_Sec, H00QU2_n1960ContagemResultadoNotificacao_Sec, H00QU2_A1048Contratante_EmailSdaSec, H00QU2_n1048Contratante_EmailSdaSec, H00QU2_A1413ContagemResultadoNotificacao_UsuCod, H00QU2_A1961ContagemResultadoNotificacao_Logged, H00QU2_n1961ContagemResultadoNotificacao_Logged,
               H00QU2_A1415ContagemResultadoNotificacao_Observacao, H00QU2_n1415ContagemResultadoNotificacao_Observacao, H00QU2_A1420ContagemResultadoNotificacao_Midia, H00QU2_n1420ContagemResultadoNotificacao_Midia, H00QU2_A1418ContagemResultadoNotificacao_CorpoEmail, H00QU2_n1418ContagemResultadoNotificacao_CorpoEmail, H00QU2_A1417ContagemResultadoNotificacao_Assunto, H00QU2_n1417ContagemResultadoNotificacao_Assunto, H00QU2_A1422ContagemResultadoNotificacao_UsuNom, H00QU2_n1422ContagemResultadoNotificacao_UsuNom,
               H00QU2_A1416ContagemResultadoNotificacao_DataHora, H00QU2_n1416ContagemResultadoNotificacao_DataHora
               }
               , new Object[] {
               H00QU3_A5AreaTrabalho_Codigo
               }
            }
         );
         AV16Pgmname = "ContagemResultadoNotificacaoGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "ContagemResultadoNotificacaoGeneral";
         context.Gx_err = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1958ContagemResultadoNotificacao_Port ;
      private short A1960ContagemResultadoNotificacao_Sec ;
      private short A1961ContagemResultadoNotificacao_Logged ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private short nGXWrapped ;
      private int A1413ContagemResultadoNotificacao_UsuCod ;
      private int edtContagemResultadoNotificacao_Codigo_Visible ;
      private int edtContagemResultadoNotificacao_UsuCod_Visible ;
      private int A1427ContagemResultadoNotificacao_UsuPesCod ;
      private int A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int A29Contratante_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int GXt_int1 ;
      private int A5AreaTrabalho_Codigo ;
      private int imgWh_Visible ;
      private int imgWu_Visible ;
      private int imgWp_Visible ;
      private int imgWa_Visible ;
      private int imgWs_Visible ;
      private int idxLst ;
      private long A1412ContagemResultadoNotificacao_Codigo ;
      private long wcpOA1412ContagemResultadoNotificacao_Codigo ;
      private long AV7ContagemResultadoNotificacao_Codigo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1420ContagemResultadoNotificacao_Midia ;
      private String A1956ContagemResultadoNotificacao_Host ;
      private String A1957ContagemResultadoNotificacao_User ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContagemResultadoNotificacao_Codigo_Internalname ;
      private String edtContagemResultadoNotificacao_Codigo_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuCod_Internalname ;
      private String edtContagemResultadoNotificacao_UsuCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A1422ContagemResultadoNotificacao_UsuNom ;
      private String edtContagemResultadoNotificacao_DataHora_Internalname ;
      private String edtContagemResultadoNotificacao_UsuNom_Internalname ;
      private String edtContagemResultadoNotificacao_Assunto_Internalname ;
      private String edtContagemResultadoNotificacao_CorpoEmail_Internalname ;
      private String cmbContagemResultadoNotificacao_Midia_Internalname ;
      private String edtContagemResultadoNotificacao_Host_Internalname ;
      private String edtContagemResultadoNotificacao_User_Internalname ;
      private String edtContagemResultadoNotificacao_Port_Internalname ;
      private String cmbContagemResultadoNotificacao_Aut_Internalname ;
      private String cmbContagemResultadoNotificacao_Sec_Internalname ;
      private String cmbContagemResultadoNotificacao_Logged_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContagemResultadoNotificacao_UsuNom_Link ;
      private String imgWh_Internalname ;
      private String imgWh_Tooltiptext ;
      private String imgWu_Internalname ;
      private String imgWu_Tooltiptext ;
      private String imgWp_Internalname ;
      private String imgWp_Tooltiptext ;
      private String imgWa_Internalname ;
      private String imgWa_Tooltiptext ;
      private String imgWs_Internalname ;
      private String imgWs_Tooltiptext ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_datahora_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_datahora_Jsonclick ;
      private String edtContagemResultadoNotificacao_DataHora_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_usunom_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_usunom_Jsonclick ;
      private String edtContagemResultadoNotificacao_UsuNom_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_assunto_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_assunto_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_corpoemail_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_corpoemail_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_midia_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_midia_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Midia_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_observacao_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_observacao_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_host_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_host_Jsonclick ;
      private String edtContagemResultadoNotificacao_Host_Jsonclick ;
      private String imgWh_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_user_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_user_Jsonclick ;
      private String edtContagemResultadoNotificacao_User_Jsonclick ;
      private String imgWu_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_port_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_port_Jsonclick ;
      private String edtContagemResultadoNotificacao_Port_Jsonclick ;
      private String imgWp_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_aut_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_aut_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Aut_Jsonclick ;
      private String imgWa_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_sec_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_sec_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Sec_Jsonclick ;
      private String imgWs_Jsonclick ;
      private String lblTextblockcontagemresultadonotificacao_logged_Internalname ;
      private String lblTextblockcontagemresultadonotificacao_logged_Jsonclick ;
      private String cmbContagemResultadoNotificacao_Logged_Jsonclick ;
      private String sCtrlA1412ContagemResultadoNotificacao_Codigo ;
      private String Contagemresultadonotificacao_observacao_Internalname ;
      private DateTime A1416ContagemResultadoNotificacao_DataHora ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1959ContagemResultadoNotificacao_Aut ;
      private bool Contagemresultadonotificacao_observacao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1420ContagemResultadoNotificacao_Midia ;
      private bool n1959ContagemResultadoNotificacao_Aut ;
      private bool n1960ContagemResultadoNotificacao_Sec ;
      private bool n1961ContagemResultadoNotificacao_Logged ;
      private bool n1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool n29Contratante_Codigo ;
      private bool n1956ContagemResultadoNotificacao_Host ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n1957ContagemResultadoNotificacao_User ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n1958ContagemResultadoNotificacao_Port ;
      private bool n552Contratante_EmailSdaPort ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private bool n1415ContagemResultadoNotificacao_Observacao ;
      private bool n1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool n1417ContagemResultadoNotificacao_Assunto ;
      private bool n1422ContagemResultadoNotificacao_UsuNom ;
      private bool n1416ContagemResultadoNotificacao_DataHora ;
      private bool returnInSub ;
      private String A1415ContagemResultadoNotificacao_Observacao ;
      private String A1417ContagemResultadoNotificacao_Assunto ;
      private String A1418ContagemResultadoNotificacao_CorpoEmail ;
      private String A547Contratante_EmailSdaHost ;
      private String A548Contratante_EmailSdaUser ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultadoNotificacao_Midia ;
      private GXCombobox cmbContagemResultadoNotificacao_Aut ;
      private GXCombobox cmbContagemResultadoNotificacao_Sec ;
      private GXCombobox cmbContagemResultadoNotificacao_Logged ;
      private IDataStoreProvider pr_default ;
      private int[] H00QU2_A1427ContagemResultadoNotificacao_UsuPesCod ;
      private bool[] H00QU2_n1427ContagemResultadoNotificacao_UsuPesCod ;
      private int[] H00QU2_A1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private bool[] H00QU2_n1966ContagemResultadoNotificacao_AreaTrabalhoCod ;
      private int[] H00QU2_A29Contratante_Codigo ;
      private bool[] H00QU2_n29Contratante_Codigo ;
      private long[] H00QU2_A1412ContagemResultadoNotificacao_Codigo ;
      private String[] H00QU2_A1956ContagemResultadoNotificacao_Host ;
      private bool[] H00QU2_n1956ContagemResultadoNotificacao_Host ;
      private String[] H00QU2_A547Contratante_EmailSdaHost ;
      private bool[] H00QU2_n547Contratante_EmailSdaHost ;
      private String[] H00QU2_A1957ContagemResultadoNotificacao_User ;
      private bool[] H00QU2_n1957ContagemResultadoNotificacao_User ;
      private String[] H00QU2_A548Contratante_EmailSdaUser ;
      private bool[] H00QU2_n548Contratante_EmailSdaUser ;
      private short[] H00QU2_A1958ContagemResultadoNotificacao_Port ;
      private bool[] H00QU2_n1958ContagemResultadoNotificacao_Port ;
      private short[] H00QU2_A552Contratante_EmailSdaPort ;
      private bool[] H00QU2_n552Contratante_EmailSdaPort ;
      private bool[] H00QU2_A1959ContagemResultadoNotificacao_Aut ;
      private bool[] H00QU2_n1959ContagemResultadoNotificacao_Aut ;
      private bool[] H00QU2_A551Contratante_EmailSdaAut ;
      private bool[] H00QU2_n551Contratante_EmailSdaAut ;
      private short[] H00QU2_A1960ContagemResultadoNotificacao_Sec ;
      private bool[] H00QU2_n1960ContagemResultadoNotificacao_Sec ;
      private short[] H00QU2_A1048Contratante_EmailSdaSec ;
      private bool[] H00QU2_n1048Contratante_EmailSdaSec ;
      private int[] H00QU2_A1413ContagemResultadoNotificacao_UsuCod ;
      private short[] H00QU2_A1961ContagemResultadoNotificacao_Logged ;
      private bool[] H00QU2_n1961ContagemResultadoNotificacao_Logged ;
      private String[] H00QU2_A1415ContagemResultadoNotificacao_Observacao ;
      private bool[] H00QU2_n1415ContagemResultadoNotificacao_Observacao ;
      private String[] H00QU2_A1420ContagemResultadoNotificacao_Midia ;
      private bool[] H00QU2_n1420ContagemResultadoNotificacao_Midia ;
      private String[] H00QU2_A1418ContagemResultadoNotificacao_CorpoEmail ;
      private bool[] H00QU2_n1418ContagemResultadoNotificacao_CorpoEmail ;
      private String[] H00QU2_A1417ContagemResultadoNotificacao_Assunto ;
      private bool[] H00QU2_n1417ContagemResultadoNotificacao_Assunto ;
      private String[] H00QU2_A1422ContagemResultadoNotificacao_UsuNom ;
      private bool[] H00QU2_n1422ContagemResultadoNotificacao_UsuNom ;
      private DateTime[] H00QU2_A1416ContagemResultadoNotificacao_DataHora ;
      private bool[] H00QU2_n1416ContagemResultadoNotificacao_DataHora ;
      private int[] H00QU3_A5AreaTrabalho_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV12WebSession ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contagemresultadonotificacaogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00QU2 ;
          prmH00QU2 = new Object[] {
          new Object[] {"@ContagemResultadoNotificacao_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmH00QU3 ;
          prmH00QU3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00QU2", "SELECT T4.[Usuario_PessoaCod] AS ContagemResultadoNotificacao_UsuPesCod, T1.[ContagemResultadoNotificacao_AreaTrabalhoCod] AS ContagemResultadoNotificacao_AreaTrabalhoCod, T2.[Contratante_Codigo], T1.[ContagemResultadoNotificacao_Codigo], T1.[ContagemResultadoNotificacao_Host], T3.[Contratante_EmailSdaHost], T1.[ContagemResultadoNotificacao_User], T3.[Contratante_EmailSdaUser], T1.[ContagemResultadoNotificacao_Port], T3.[Contratante_EmailSdaPort], T1.[ContagemResultadoNotificacao_Aut], T3.[Contratante_EmailSdaAut], T1.[ContagemResultadoNotificacao_Sec], T3.[Contratante_EmailSdaSec], T1.[ContagemResultadoNotificacao_UsuCod] AS ContagemResultadoNotificacao_UsuCod, T1.[ContagemResultadoNotificacao_Logged], T1.[ContagemResultadoNotificacao_Observacao], T1.[ContagemResultadoNotificacao_Midia], T1.[ContagemResultadoNotificacao_CorpoEmail], T1.[ContagemResultadoNotificacao_Assunto], T5.[Pessoa_Nome] AS ContagemResultadoNotificacao_UsuNom, T1.[ContagemResultadoNotificacao_DataHora] FROM (((([ContagemResultadoNotificacao] T1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ContagemResultadoNotificacao_AreaTrabalhoCod]) LEFT JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T2.[Contratante_Codigo]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContagemResultadoNotificacao_UsuCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoNotificacao_Codigo] = @ContagemResultadoNotificacao_Codigo ORDER BY T1.[ContagemResultadoNotificacao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QU2,1,0,true,true )
             ,new CursorDef("H00QU3", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00QU3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((long[]) buf[6])[0] = rslt.getLong(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((bool[]) buf[19])[0] = rslt.getBool(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((bool[]) buf[21])[0] = rslt.getBool(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((short[]) buf[23])[0] = rslt.getShort(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((short[]) buf[25])[0] = rslt.getShort(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((int[]) buf[27])[0] = rslt.getInt(15) ;
                ((short[]) buf[28])[0] = rslt.getShort(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getLongVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getString(18, 3) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((String[]) buf[36])[0] = rslt.getLongVarchar(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((String[]) buf[38])[0] = rslt.getString(21, 100) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((DateTime[]) buf[40])[0] = rslt.getGXDateTime(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
