/*
               File: ServicoArtefatosWC
        Description: Servico Artefatos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:23.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoartefatoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicoartefatoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicoartefatoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Servico_Codigo )
      {
         this.AV7Servico_Codigo = aP0_Servico_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Servico_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
                  AV18TFServicoArtefato_ArtefatoDsc = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFServicoArtefato_ArtefatoDsc", AV18TFServicoArtefato_ArtefatoDsc);
                  AV19TFServicoArtefato_ArtefatoDsc_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoArtefato_ArtefatoDsc_Sel", AV19TFServicoArtefato_ArtefatoDsc_Sel);
                  AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
                  AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace", AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace);
                  AV24Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel, AV7Servico_Codigo, AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, AV24Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAN52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV24Pgmname = "ServicoArtefatosWC";
               context.Gx_err = 0;
               WSN52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Artefatos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311762387");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoartefatoswc.aspx") + "?" + UrlEncode("" +AV7Servico_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOARTEFATO_ARTEFATODSC", AV18TFServicoArtefato_ArtefatoDsc);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSERVICOARTEFATO_ARTEFATODSC_SEL", AV19TFServicoArtefato_ArtefatoDsc_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV21DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV21DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA", AV17ServicoArtefato_ArtefatoDscTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA", AV17ServicoArtefato_ArtefatoDscTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV24Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Caption", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Tooltip", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Cls", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filteredtext_set", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Selectedvalue_set", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includesortasc", StringUtil.BoolToStr( Ddo_servicoartefato_artefatodsc_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includesortdsc", StringUtil.BoolToStr( Ddo_servicoartefato_artefatodsc_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Sortedstatus", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includefilter", StringUtil.BoolToStr( Ddo_servicoartefato_artefatodsc_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filtertype", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filterisrange", StringUtil.BoolToStr( Ddo_servicoartefato_artefatodsc_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includedatalist", StringUtil.BoolToStr( Ddo_servicoartefato_artefatodsc_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Datalisttype", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Datalistproc", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicoartefato_artefatodsc_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Sortasc", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Sortdsc", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Loadingdata", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Cleanfilter", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Noresultsfound", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Searchbuttontext", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Activeeventkey", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filteredtext_get", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Selectedvalue_get", StringUtil.RTrim( Ddo_servicoartefato_artefatodsc_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormN52( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicoartefatoswc.js", "?2020311762418");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoArtefatosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Artefatos WC" ;
      }

      protected void WBN50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicoartefatoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_N52( true) ;
         }
         else
         {
            wb_table1_2_N52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_N52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServico_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoArtefatosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ServicoArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoartefato_artefatodsc_Internalname, AV18TFServicoArtefato_ArtefatoDsc, StringUtil.RTrim( context.localUtil.Format( AV18TFServicoArtefato_ArtefatoDsc, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,12);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoartefato_artefatodsc_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoartefato_artefatodsc_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoArtefatosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoartefato_artefatodsc_sel_Internalname, AV19TFServicoArtefato_ArtefatoDsc_Sel, StringUtil.RTrim( context.localUtil.Format( AV19TFServicoArtefato_ArtefatoDsc_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoartefato_artefatodsc_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoartefato_artefatodsc_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ServicoArtefatosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSCContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Internalname, AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", 0, edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ServicoArtefatosWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTN52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Artefatos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPN50( ) ;
            }
         }
      }

      protected void WSN52( )
      {
         STARTN52( ) ;
         EVTN52( ) ;
      }

      protected void EVTN52( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOARTEFATO_ARTEFATODSC.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11N52 */
                                    E11N52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOMANTERARTEFATOS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12N52 */
                                    E12N52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrdereddsc_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN50( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPN50( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              A1767ServicoArtefato_ArtefatoDsc = StringUtil.Upper( cgiGet( edtServicoArtefato_ArtefatoDsc_Internalname));
                              n1767ServicoArtefato_ArtefatoDsc = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13N52 */
                                          E13N52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14N52 */
                                          E14N52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15N52 */
                                          E15N52 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicoartefato_artefatodsc Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOARTEFATO_ARTEFATODSC"), AV18TFServicoArtefato_ArtefatoDsc) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfservicoartefato_artefatodsc_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOARTEFATO_ARTEFATODSC_SEL"), AV19TFServicoArtefato_ArtefatoDsc_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPN50( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrdereddsc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEN52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormN52( ) ;
            }
         }
      }

      protected void PAN52( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrdereddsc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       bool AV13OrderedDsc ,
                                       String AV18TFServicoArtefato_ArtefatoDsc ,
                                       String AV19TFServicoArtefato_ArtefatoDsc_Sel ,
                                       int AV7Servico_Codigo ,
                                       String AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace ,
                                       String AV24Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFN52( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFN52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV24Pgmname = "ServicoArtefatosWC";
         context.Gx_err = 0;
      }

      protected void RFN52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E14N52 */
         E14N52 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV19TFServicoArtefato_ArtefatoDsc_Sel ,
                                                 AV18TFServicoArtefato_ArtefatoDsc ,
                                                 A1767ServicoArtefato_ArtefatoDsc ,
                                                 AV13OrderedDsc ,
                                                 A155Servico_Codigo ,
                                                 AV7Servico_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV18TFServicoArtefato_ArtefatoDsc = StringUtil.Concat( StringUtil.RTrim( AV18TFServicoArtefato_ArtefatoDsc), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFServicoArtefato_ArtefatoDsc", AV18TFServicoArtefato_ArtefatoDsc);
            /* Using cursor H00N52 */
            pr_default.execute(0, new Object[] {AV7Servico_Codigo, lV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_5_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1766ServicoArtefato_ArtefatoCod = H00N52_A1766ServicoArtefato_ArtefatoCod[0];
               A155Servico_Codigo = H00N52_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               A1767ServicoArtefato_ArtefatoDsc = H00N52_A1767ServicoArtefato_ArtefatoDsc[0];
               n1767ServicoArtefato_ArtefatoDsc = H00N52_n1767ServicoArtefato_ArtefatoDsc[0];
               A1767ServicoArtefato_ArtefatoDsc = H00N52_A1767ServicoArtefato_ArtefatoDsc[0];
               n1767ServicoArtefato_ArtefatoDsc = H00N52_n1767ServicoArtefato_ArtefatoDsc[0];
               /* Execute user event: E15N52 */
               E15N52 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 5;
            WBN50( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV19TFServicoArtefato_ArtefatoDsc_Sel ,
                                              AV18TFServicoArtefato_ArtefatoDsc ,
                                              A1767ServicoArtefato_ArtefatoDsc ,
                                              AV13OrderedDsc ,
                                              A155Servico_Codigo ,
                                              AV7Servico_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV18TFServicoArtefato_ArtefatoDsc = StringUtil.Concat( StringUtil.RTrim( AV18TFServicoArtefato_ArtefatoDsc), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFServicoArtefato_ArtefatoDsc", AV18TFServicoArtefato_ArtefatoDsc);
         /* Using cursor H00N53 */
         pr_default.execute(1, new Object[] {AV7Servico_Codigo, lV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel});
         GRID_nRecordCount = H00N53_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel, AV7Servico_Codigo, AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, AV24Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel, AV7Servico_Codigo, AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, AV24Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel, AV7Servico_Codigo, AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, AV24Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel, AV7Servico_Codigo, AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, AV24Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedDsc, AV18TFServicoArtefato_ArtefatoDsc, AV19TFServicoArtefato_ArtefatoDsc_Sel, AV7Servico_Codigo, AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, AV24Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPN50( )
      {
         /* Before Start, stand alone formulas. */
         AV24Pgmname = "ServicoArtefatosWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13N52 */
         E13N52 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV21DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA"), AV17ServicoArtefato_ArtefatoDscTitleFilterData);
            /* Read variables values. */
            A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            AV18TFServicoArtefato_ArtefatoDsc = StringUtil.Upper( cgiGet( edtavTfservicoartefato_artefatodsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFServicoArtefato_ArtefatoDsc", AV18TFServicoArtefato_ArtefatoDsc);
            AV19TFServicoArtefato_ArtefatoDsc_Sel = StringUtil.Upper( cgiGet( edtavTfservicoartefato_artefatodsc_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoArtefato_ArtefatoDsc_Sel", AV19TFServicoArtefato_ArtefatoDsc_Sel);
            AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace = cgiGet( edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace", AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOAV7Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Servico_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_servicoartefato_artefatodsc_Caption = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Caption");
            Ddo_servicoartefato_artefatodsc_Tooltip = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Tooltip");
            Ddo_servicoartefato_artefatodsc_Cls = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Cls");
            Ddo_servicoartefato_artefatodsc_Filteredtext_set = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filteredtext_set");
            Ddo_servicoartefato_artefatodsc_Selectedvalue_set = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Selectedvalue_set");
            Ddo_servicoartefato_artefatodsc_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Dropdownoptionstype");
            Ddo_servicoartefato_artefatodsc_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Titlecontrolidtoreplace");
            Ddo_servicoartefato_artefatodsc_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includesortasc"));
            Ddo_servicoartefato_artefatodsc_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includesortdsc"));
            Ddo_servicoartefato_artefatodsc_Sortedstatus = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Sortedstatus");
            Ddo_servicoartefato_artefatodsc_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includefilter"));
            Ddo_servicoartefato_artefatodsc_Filtertype = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filtertype");
            Ddo_servicoartefato_artefatodsc_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filterisrange"));
            Ddo_servicoartefato_artefatodsc_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Includedatalist"));
            Ddo_servicoartefato_artefatodsc_Datalisttype = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Datalisttype");
            Ddo_servicoartefato_artefatodsc_Datalistproc = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Datalistproc");
            Ddo_servicoartefato_artefatodsc_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicoartefato_artefatodsc_Sortasc = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Sortasc");
            Ddo_servicoartefato_artefatodsc_Sortdsc = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Sortdsc");
            Ddo_servicoartefato_artefatodsc_Loadingdata = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Loadingdata");
            Ddo_servicoartefato_artefatodsc_Cleanfilter = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Cleanfilter");
            Ddo_servicoartefato_artefatodsc_Noresultsfound = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Noresultsfound");
            Ddo_servicoartefato_artefatodsc_Searchbuttontext = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Searchbuttontext");
            Ddo_servicoartefato_artefatodsc_Activeeventkey = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Activeeventkey");
            Ddo_servicoartefato_artefatodsc_Filteredtext_get = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Filteredtext_get");
            Ddo_servicoartefato_artefatodsc_Selectedvalue_get = cgiGet( sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOARTEFATO_ARTEFATODSC"), AV18TFServicoArtefato_ArtefatoDsc) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSERVICOARTEFATO_ARTEFATODSC_SEL"), AV19TFServicoArtefato_ArtefatoDsc_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13N52 */
         E13N52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13N52( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfservicoartefato_artefatodsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicoartefato_artefatodsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoartefato_artefatodsc_Visible), 5, 0)));
         edtavTfservicoartefato_artefatodsc_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfservicoartefato_artefatodsc_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoartefato_artefatodsc_sel_Visible), 5, 0)));
         Ddo_servicoartefato_artefatodsc_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoArtefato_ArtefatoDsc";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoartefato_artefatodsc_Internalname, "TitleControlIdToReplace", Ddo_servicoartefato_artefatodsc_Titlecontrolidtoreplace);
         AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace = Ddo_servicoartefato_artefatodsc_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace", AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace);
         edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Visible), 5, 0)));
         edtServico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV21DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV21DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E14N52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV17ServicoArtefato_ArtefatoDscTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServicoArtefato_ArtefatoDsc_Titleformat = 2;
         edtServicoArtefato_ArtefatoDsc_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoArtefato_ArtefatoDsc_Internalname, "Title", edtServicoArtefato_ArtefatoDsc_Title);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV17ServicoArtefato_ArtefatoDscTitleFilterData", AV17ServicoArtefato_ArtefatoDscTitleFilterData);
      }

      protected void E11N52( )
      {
         /* Ddo_servicoartefato_artefatodsc_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicoartefato_artefatodsc_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_servicoartefato_artefatodsc_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoartefato_artefatodsc_Internalname, "SortedStatus", Ddo_servicoartefato_artefatodsc_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_servicoartefato_artefatodsc_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_servicoartefato_artefatodsc_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoartefato_artefatodsc_Internalname, "SortedStatus", Ddo_servicoartefato_artefatodsc_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_servicoartefato_artefatodsc_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV18TFServicoArtefato_ArtefatoDsc = Ddo_servicoartefato_artefatodsc_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFServicoArtefato_ArtefatoDsc", AV18TFServicoArtefato_ArtefatoDsc);
            AV19TFServicoArtefato_ArtefatoDsc_Sel = Ddo_servicoartefato_artefatodsc_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoArtefato_ArtefatoDsc_Sel", AV19TFServicoArtefato_ArtefatoDsc_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E15N52( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 5;
         }
         sendrow_52( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void E12N52( )
      {
         /* 'DoManterArtefatos' Routine */
         context.PopUp(formatLink("associationservicoservicoartefatos.aspx") + "?" + UrlEncode("" +AV7Servico_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_servicoartefato_artefatodsc_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoartefato_artefatodsc_Internalname, "SortedStatus", Ddo_servicoartefato_artefatodsc_Sortedstatus);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV24Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV24Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV16Session.Get(AV24Pgmname+"GridState"), "");
         }
         AV13OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25GXV1 = 1;
         while ( AV25GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV25GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOARTEFATO_ARTEFATODSC") == 0 )
            {
               AV18TFServicoArtefato_ArtefatoDsc = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFServicoArtefato_ArtefatoDsc", AV18TFServicoArtefato_ArtefatoDsc);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFServicoArtefato_ArtefatoDsc)) )
               {
                  Ddo_servicoartefato_artefatodsc_Filteredtext_set = AV18TFServicoArtefato_ArtefatoDsc;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoartefato_artefatodsc_Internalname, "FilteredText_set", Ddo_servicoartefato_artefatodsc_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSERVICOARTEFATO_ARTEFATODSC_SEL") == 0 )
            {
               AV19TFServicoArtefato_ArtefatoDsc_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFServicoArtefato_ArtefatoDsc_Sel", AV19TFServicoArtefato_ArtefatoDsc_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoArtefato_ArtefatoDsc_Sel)) )
               {
                  Ddo_servicoartefato_artefatodsc_Selectedvalue_set = AV19TFServicoArtefato_ArtefatoDsc_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_servicoartefato_artefatodsc_Internalname, "SelectedValue_set", Ddo_servicoartefato_artefatodsc_Selectedvalue_set);
               }
            }
            AV25GXV1 = (int)(AV25GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV16Session.Get(AV24Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFServicoArtefato_ArtefatoDsc)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOARTEFATO_ARTEFATODSC";
            AV12GridStateFilterValue.gxTpr_Value = AV18TFServicoArtefato_ArtefatoDsc;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoArtefato_ArtefatoDsc_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSERVICOARTEFATO_ARTEFATODSC_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV19TFServicoArtefato_ArtefatoDsc_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Servico_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&SERVICO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV24Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV24Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ServicoArtefatos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Servico_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV16Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_N52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoArtefato_ArtefatoDsc_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoArtefato_ArtefatoDsc_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoArtefato_ArtefatoDsc_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1767ServicoArtefato_ArtefatoDsc);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoArtefato_ArtefatoDsc_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoArtefato_ArtefatoDsc_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgManterartefatos_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Vincular este servi�o com artefatos", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgManterartefatos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOMANTERARTEFATOS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoArtefatosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_N52e( true) ;
         }
         else
         {
            wb_table1_2_N52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Servico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAN52( ) ;
         WSN52( ) ;
         WEN52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Servico_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAN52( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicoartefatoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAN52( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Servico_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
         }
         wcpOAV7Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Servico_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Servico_Codigo != wcpOAV7Servico_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Servico_Codigo = AV7Servico_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Servico_Codigo = cgiGet( sPrefix+"AV7Servico_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Servico_Codigo) > 0 )
         {
            AV7Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Servico_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
         }
         else
         {
            AV7Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Servico_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAN52( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSN52( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSN52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Servico_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Servico_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Servico_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Servico_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Servico_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEN52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311762527");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("servicoartefatoswc.js", "?2020311762528");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtServicoArtefato_ArtefatoDsc_Internalname = sPrefix+"SERVICOARTEFATO_ARTEFATODSC_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtServicoArtefato_ArtefatoDsc_Internalname = sPrefix+"SERVICOARTEFATO_ARTEFATODSC_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBN50( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_5_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoArtefato_ArtefatoDsc_Internalname,(String)A1767ServicoArtefato_ArtefatoDsc,StringUtil.RTrim( context.localUtil.Format( A1767ServicoArtefato_ArtefatoDsc, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoArtefato_ArtefatoDsc_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GridContainer.AddRow(GridRow);
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtServicoArtefato_ArtefatoDsc_Internalname = sPrefix+"SERVICOARTEFATO_ARTEFATODSC";
         imgManterartefatos_Internalname = sPrefix+"MANTERARTEFATOS";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtServico_Codigo_Internalname = sPrefix+"SERVICO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfservicoartefato_artefatodsc_Internalname = sPrefix+"vTFSERVICOARTEFATO_ARTEFATODSC";
         edtavTfservicoartefato_artefatodsc_sel_Internalname = sPrefix+"vTFSERVICOARTEFATO_ARTEFATODSC_SEL";
         Ddo_servicoartefato_artefatodsc_Internalname = sPrefix+"DDO_SERVICOARTEFATO_ARTEFATODSC";
         edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtServicoArtefato_ArtefatoDsc_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtServicoArtefato_ArtefatoDsc_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtServicoArtefato_ArtefatoDsc_Title = "Descri��o";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Visible = 1;
         edtavTfservicoartefato_artefatodsc_sel_Jsonclick = "";
         edtavTfservicoartefato_artefatodsc_sel_Visible = 1;
         edtavTfservicoartefato_artefatodsc_Jsonclick = "";
         edtavTfservicoartefato_artefatodsc_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtServico_Codigo_Jsonclick = "";
         edtServico_Codigo_Visible = 1;
         Ddo_servicoartefato_artefatodsc_Searchbuttontext = "Pesquisar";
         Ddo_servicoartefato_artefatodsc_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicoartefato_artefatodsc_Cleanfilter = "Limpar pesquisa";
         Ddo_servicoartefato_artefatodsc_Loadingdata = "Carregando dados...";
         Ddo_servicoartefato_artefatodsc_Sortdsc = "Ordenar de Z � A";
         Ddo_servicoartefato_artefatodsc_Sortasc = "Ordenar de A � Z";
         Ddo_servicoartefato_artefatodsc_Datalistupdateminimumcharacters = 0;
         Ddo_servicoartefato_artefatodsc_Datalistproc = "GetServicoArtefatosWCFilterData";
         Ddo_servicoartefato_artefatodsc_Datalisttype = "Dynamic";
         Ddo_servicoartefato_artefatodsc_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicoartefato_artefatodsc_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicoartefato_artefatodsc_Filtertype = "Character";
         Ddo_servicoartefato_artefatodsc_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicoartefato_artefatodsc_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicoartefato_artefatodsc_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicoartefato_artefatodsc_Titlecontrolidtoreplace = "";
         Ddo_servicoartefato_artefatodsc_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicoartefato_artefatodsc_Cls = "ColumnSettings";
         Ddo_servicoartefato_artefatodsc_Tooltip = "Op��es";
         Ddo_servicoartefato_artefatodsc_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace',fld:'vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17ServicoArtefato_ArtefatoDscTitleFilterData',fld:'vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoArtefato_ArtefatoDsc_Titleformat',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Titleformat'},{av:'edtServicoArtefato_ArtefatoDsc_Title',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Title'}]}");
         setEventMetadata("DDO_SERVICOARTEFATO_ARTEFATODSC.ONOPTIONCLICKED","{handler:'E11N52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace',fld:'vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_servicoartefato_artefatodsc_Activeeventkey',ctrl:'DDO_SERVICOARTEFATO_ARTEFATODSC',prop:'ActiveEventKey'},{av:'Ddo_servicoartefato_artefatodsc_Filteredtext_get',ctrl:'DDO_SERVICOARTEFATO_ARTEFATODSC',prop:'FilteredText_get'},{av:'Ddo_servicoartefato_artefatodsc_Selectedvalue_get',ctrl:'DDO_SERVICOARTEFATO_ARTEFATODSC',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicoartefato_artefatodsc_Sortedstatus',ctrl:'DDO_SERVICOARTEFATO_ARTEFATODSC',prop:'SortedStatus'},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E15N52',iparms:[],oparms:[]}");
         setEventMetadata("'DOMANTERARTEFATOS'","{handler:'E12N52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace',fld:'vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace',fld:'vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17ServicoArtefato_ArtefatoDscTitleFilterData',fld:'vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoArtefato_ArtefatoDsc_Titleformat',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Titleformat'},{av:'edtServicoArtefato_ArtefatoDsc_Title',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Title'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace',fld:'vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17ServicoArtefato_ArtefatoDscTitleFilterData',fld:'vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoArtefato_ArtefatoDsc_Titleformat',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Titleformat'},{av:'edtServicoArtefato_ArtefatoDsc_Title',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Title'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace',fld:'vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17ServicoArtefato_ArtefatoDscTitleFilterData',fld:'vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoArtefato_ArtefatoDsc_Titleformat',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Titleformat'},{av:'edtServicoArtefato_ArtefatoDsc_Title',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Title'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace',fld:'vDDO_SERVICOARTEFATO_ARTEFATODSCTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFServicoArtefato_ArtefatoDsc',fld:'vTFSERVICOARTEFATO_ARTEFATODSC',pic:'@!',nv:''},{av:'AV19TFServicoArtefato_ArtefatoDsc_Sel',fld:'vTFSERVICOARTEFATO_ARTEFATODSC_SEL',pic:'@!',nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17ServicoArtefato_ArtefatoDscTitleFilterData',fld:'vSERVICOARTEFATO_ARTEFATODSCTITLEFILTERDATA',pic:'',nv:null},{av:'edtServicoArtefato_ArtefatoDsc_Titleformat',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Titleformat'},{av:'edtServicoArtefato_ArtefatoDsc_Title',ctrl:'SERVICOARTEFATO_ARTEFATODSC',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_servicoartefato_artefatodsc_Activeeventkey = "";
         Ddo_servicoartefato_artefatodsc_Filteredtext_get = "";
         Ddo_servicoartefato_artefatodsc_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV18TFServicoArtefato_ArtefatoDsc = "";
         AV19TFServicoArtefato_ArtefatoDsc_Sel = "";
         AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace = "";
         AV24Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV21DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV17ServicoArtefato_ArtefatoDscTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_servicoartefato_artefatodsc_Filteredtext_set = "";
         Ddo_servicoartefato_artefatodsc_Selectedvalue_set = "";
         Ddo_servicoartefato_artefatodsc_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1767ServicoArtefato_ArtefatoDsc = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18TFServicoArtefato_ArtefatoDsc = "";
         H00N52_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         H00N52_A155Servico_Codigo = new int[1] ;
         H00N52_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         H00N52_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         H00N53_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgManterartefatos_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Servico_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoartefatoswc__default(),
            new Object[][] {
                new Object[] {
               H00N52_A1766ServicoArtefato_ArtefatoCod, H00N52_A155Servico_Codigo, H00N52_A1767ServicoArtefato_ArtefatoDsc, H00N52_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               H00N53_AGRID_nRecordCount
               }
            }
         );
         AV24Pgmname = "ServicoArtefatosWC";
         /* GeneXus formulas. */
         AV24Pgmname = "ServicoArtefatosWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServicoArtefato_ArtefatoDsc_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Servico_Codigo ;
      private int wcpOAV7Servico_Codigo ;
      private int subGrid_Rows ;
      private int Ddo_servicoartefato_artefatodsc_Datalistupdateminimumcharacters ;
      private int A155Servico_Codigo ;
      private int edtServico_Codigo_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfservicoartefato_artefatodsc_Visible ;
      private int edtavTfservicoartefato_artefatodsc_sel_Visible ;
      private int edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A1766ServicoArtefato_ArtefatoCod ;
      private int AV25GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Ddo_servicoartefato_artefatodsc_Activeeventkey ;
      private String Ddo_servicoartefato_artefatodsc_Filteredtext_get ;
      private String Ddo_servicoartefato_artefatodsc_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String AV24Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_servicoartefato_artefatodsc_Caption ;
      private String Ddo_servicoartefato_artefatodsc_Tooltip ;
      private String Ddo_servicoartefato_artefatodsc_Cls ;
      private String Ddo_servicoartefato_artefatodsc_Filteredtext_set ;
      private String Ddo_servicoartefato_artefatodsc_Selectedvalue_set ;
      private String Ddo_servicoartefato_artefatodsc_Dropdownoptionstype ;
      private String Ddo_servicoartefato_artefatodsc_Titlecontrolidtoreplace ;
      private String Ddo_servicoartefato_artefatodsc_Sortedstatus ;
      private String Ddo_servicoartefato_artefatodsc_Filtertype ;
      private String Ddo_servicoartefato_artefatodsc_Datalisttype ;
      private String Ddo_servicoartefato_artefatodsc_Datalistproc ;
      private String Ddo_servicoartefato_artefatodsc_Sortasc ;
      private String Ddo_servicoartefato_artefatodsc_Sortdsc ;
      private String Ddo_servicoartefato_artefatodsc_Loadingdata ;
      private String Ddo_servicoartefato_artefatodsc_Cleanfilter ;
      private String Ddo_servicoartefato_artefatodsc_Noresultsfound ;
      private String Ddo_servicoartefato_artefatodsc_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtServico_Codigo_Internalname ;
      private String edtServico_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfservicoartefato_artefatodsc_Internalname ;
      private String edtavTfservicoartefato_artefatodsc_Jsonclick ;
      private String edtavTfservicoartefato_artefatodsc_sel_Internalname ;
      private String edtavTfservicoartefato_artefatodsc_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_servicoartefato_artefatodsctitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtServicoArtefato_ArtefatoDsc_Internalname ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_servicoartefato_artefatodsc_Internalname ;
      private String edtServicoArtefato_ArtefatoDsc_Title ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgManterartefatos_Internalname ;
      private String imgManterartefatos_Jsonclick ;
      private String sCtrlAV7Servico_Codigo ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServicoArtefato_ArtefatoDsc_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Ddo_servicoartefato_artefatodsc_Includesortasc ;
      private bool Ddo_servicoartefato_artefatodsc_Includesortdsc ;
      private bool Ddo_servicoartefato_artefatodsc_Includefilter ;
      private bool Ddo_servicoartefato_artefatodsc_Filterisrange ;
      private bool Ddo_servicoartefato_artefatodsc_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1767ServicoArtefato_ArtefatoDsc ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV18TFServicoArtefato_ArtefatoDsc ;
      private String AV19TFServicoArtefato_ArtefatoDsc_Sel ;
      private String AV20ddo_ServicoArtefato_ArtefatoDscTitleControlIdToReplace ;
      private String A1767ServicoArtefato_ArtefatoDsc ;
      private String lV18TFServicoArtefato_ArtefatoDsc ;
      private IGxSession AV16Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00N52_A1766ServicoArtefato_ArtefatoCod ;
      private int[] H00N52_A155Servico_Codigo ;
      private String[] H00N52_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] H00N52_n1767ServicoArtefato_ArtefatoDsc ;
      private long[] H00N53_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV17ServicoArtefato_ArtefatoDscTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV21DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class servicoartefatoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00N52( IGxContext context ,
                                             String AV19TFServicoArtefato_ArtefatoDsc_Sel ,
                                             String AV18TFServicoArtefato_ArtefatoDsc ,
                                             String A1767ServicoArtefato_ArtefatoDsc ,
                                             bool AV13OrderedDsc ,
                                             int A155Servico_Codigo ,
                                             int AV7Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [8] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod, T1.[Servico_Codigo], T2.[Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc";
         sFromString = " FROM ([ServicoArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[ServicoArtefato_ArtefatoCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Servico_Codigo] = @AV7Servico_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoArtefato_ArtefatoDsc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFServicoArtefato_ArtefatoDsc)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] like @lV18TFServicoArtefato_ArtefatoDsc)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoArtefato_ArtefatoDsc_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] = @AV19TFServicoArtefato_ArtefatoDsc_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo], T2.[Artefatos_Descricao]";
         }
         else if ( AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo] DESC, T2.[Artefatos_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Servico_Codigo], T1.[ServicoArtefato_ArtefatoCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00N53( IGxContext context ,
                                             String AV19TFServicoArtefato_ArtefatoDsc_Sel ,
                                             String AV18TFServicoArtefato_ArtefatoDsc ,
                                             String A1767ServicoArtefato_ArtefatoDsc ,
                                             bool AV13OrderedDsc ,
                                             int A155Servico_Codigo ,
                                             int AV7Servico_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [3] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ServicoArtefatos] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[ServicoArtefato_ArtefatoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Servico_Codigo] = @AV7Servico_Codigo)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoArtefato_ArtefatoDsc_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFServicoArtefato_ArtefatoDsc)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] like @lV18TFServicoArtefato_ArtefatoDsc)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFServicoArtefato_ArtefatoDsc_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Artefatos_Descricao] = @AV19TFServicoArtefato_ArtefatoDsc_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00N52(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
               case 1 :
                     return conditional_H00N53(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00N52 ;
          prmH00N52 = new Object[] {
          new Object[] {"@AV7Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18TFServicoArtefato_ArtefatoDsc",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV19TFServicoArtefato_ArtefatoDsc_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00N53 ;
          prmH00N53 = new Object[] {
          new Object[] {"@AV7Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18TFServicoArtefato_ArtefatoDsc",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV19TFServicoArtefato_ArtefatoDsc_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00N52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N52,11,0,true,false )
             ,new CursorDef("H00N53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N53,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                return;
       }
    }

 }

}
