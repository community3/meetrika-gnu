/*
               File: type_SdtSDT_ContratoServicoUnidadeMedicao
        Description: SDT_ContratoServicoUnidadeMedicao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:59.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_ContratoServicoUnidadeMedicao" )]
   [XmlType(TypeName =  "SDT_ContratoServicoUnidadeMedicao" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_ContratoServicoUnidadeMedicao : GxUserType
   {
      public SdtSDT_ContratoServicoUnidadeMedicao( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao = "";
         gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl = "";
      }

      public SdtSDT_ContratoServicoUnidadeMedicao( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_ContratoServicoUnidadeMedicao deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_ContratoServicoUnidadeMedicao)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_ContratoServicoUnidadeMedicao obj ;
         obj = this;
         obj.gxTpr_Contratoservicos_unidadecontratada = deserialized.gxTpr_Contratoservicos_unidadecontratada;
         obj.gxTpr_Contratoservicos_unidadecontratadadescricao = deserialized.gxTpr_Contratoservicos_unidadecontratadadescricao;
         obj.gxTpr_Contratoservicos_undcntsgl = deserialized.gxTpr_Contratoservicos_undcntsgl;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UnidadeContratada") )
               {
                  gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratada = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UnidadeContratadaDescricao") )
               {
                  gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoServicos_UndCntSgl") )
               {
                  gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_ContratoServicoUnidadeMedicao";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoServicos_UnidadeContratada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratada), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicos_UnidadeContratadaDescricao", StringUtil.RTrim( gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoServicos_UndCntSgl", StringUtil.RTrim( gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoServicos_UnidadeContratada", gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratada, false);
         AddObjectProperty("ContratoServicos_UnidadeContratadaDescricao", gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao, false);
         AddObjectProperty("ContratoServicos_UndCntSgl", gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl, false);
         return  ;
      }

      [  SoapElement( ElementName = "ContratoServicos_UnidadeContratada" )]
      [  XmlElement( ElementName = "ContratoServicos_UnidadeContratada"   )]
      public int gxTpr_Contratoservicos_unidadecontratada
      {
         get {
            return gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratada ;
         }

         set {
            gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratada = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_UnidadeContratadaDescricao" )]
      [  XmlElement( ElementName = "ContratoServicos_UnidadeContratadaDescricao"   )]
      public String gxTpr_Contratoservicos_unidadecontratadadescricao
      {
         get {
            return gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao ;
         }

         set {
            gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoServicos_UndCntSgl" )]
      [  XmlElement( ElementName = "ContratoServicos_UndCntSgl"   )]
      public String gxTpr_Contratoservicos_undcntsgl
      {
         get {
            return gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl ;
         }

         set {
            gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao = "";
         gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratada ;
      protected String gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_unidadecontratadadescricao ;
      protected String gxTv_SdtSDT_ContratoServicoUnidadeMedicao_Contratoservicos_undcntsgl ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_ContratoServicoUnidadeMedicao", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_ContratoServicoUnidadeMedicao_RESTInterface : GxGenericCollectionItem<SdtSDT_ContratoServicoUnidadeMedicao>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_ContratoServicoUnidadeMedicao_RESTInterface( ) : base()
      {
      }

      public SdtSDT_ContratoServicoUnidadeMedicao_RESTInterface( SdtSDT_ContratoServicoUnidadeMedicao psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoServicos_UnidadeContratada" , Order = 0 )]
      public Nullable<int> gxTpr_Contratoservicos_unidadecontratada
      {
         get {
            return sdt.gxTpr_Contratoservicos_unidadecontratada ;
         }

         set {
            sdt.gxTpr_Contratoservicos_unidadecontratada = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoServicos_UnidadeContratadaDescricao" , Order = 1 )]
      public String gxTpr_Contratoservicos_unidadecontratadadescricao
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_unidadecontratadadescricao) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_unidadecontratadadescricao = (String)(value);
         }

      }

      [DataMember( Name = "ContratoServicos_UndCntSgl" , Order = 2 )]
      public String gxTpr_Contratoservicos_undcntsgl
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratoservicos_undcntsgl) ;
         }

         set {
            sdt.gxTpr_Contratoservicos_undcntsgl = (String)(value);
         }

      }

      public SdtSDT_ContratoServicoUnidadeMedicao sdt
      {
         get {
            return (SdtSDT_ContratoServicoUnidadeMedicao)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_ContratoServicoUnidadeMedicao() ;
         }
      }

   }

}
