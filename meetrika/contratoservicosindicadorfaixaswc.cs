/*
               File: ContratoServicosIndicadorFaixasWC
        Description: Contrato Servicos Indicador Faixas WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:6:28.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosindicadorfaixaswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosindicadorfaixaswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosindicadorfaixaswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosIndicador_Codigo )
      {
         this.AV7ContratoServicosIndicador_Codigo = aP0_ContratoServicosIndicador_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContratoServicosIndicador_Formato = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoServicosIndicador_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_17 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_17_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_17_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV18TFContratoServicosIndicadorFaixa_Numero = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosIndicadorFaixa_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0)));
                  AV19TFContratoServicosIndicadorFaixa_Numero_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratoServicosIndicadorFaixa_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0)));
                  AV22TFContratoServicosIndicadorFaixa_Desde = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosIndicadorFaixa_Desde", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2)));
                  AV23TFContratoServicosIndicadorFaixa_Desde_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosIndicadorFaixa_Desde_To", StringUtil.LTrim( StringUtil.Str( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2)));
                  AV26TFContratoServicosIndicadorFaixa_Ate = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosIndicadorFaixa_Ate", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2)));
                  AV27TFContratoServicosIndicadorFaixa_Ate_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosIndicadorFaixa_Ate_To", StringUtil.LTrim( StringUtil.Str( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2)));
                  AV30TFContratoServicosIndicadorFaixa_Reduz = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicadorFaixa_Reduz", StringUtil.LTrim( StringUtil.Str( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2)));
                  AV31TFContratoServicosIndicadorFaixa_Reduz_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosIndicadorFaixa_Reduz_To", StringUtil.LTrim( StringUtil.Str( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2)));
                  AV7ContratoServicosIndicador_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0)));
                  AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace", AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace);
                  AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace", AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace);
                  AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace", AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace);
                  AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace", AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace);
                  AV39Pgmname = GetNextPar( );
                  A1345ContratoServicosIndicador_CalculoSob = GetNextPar( );
                  n1345ContratoServicosIndicador_CalculoSob = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1345ContratoServicosIndicador_CalculoSob", A1345ContratoServicosIndicador_CalculoSob);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To, AV7ContratoServicosIndicador_Codigo, AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, AV39Pgmname, A1345ContratoServicosIndicador_CalculoSob, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAJU2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV39Pgmname = "ContratoServicosIndicadorFaixasWC";
               context.Gx_err = 0;
               edtavLabel_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLabel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLabel_Enabled), 5, 0)));
               WSJU2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Indicador Faixas WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311762825");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosindicadorfaixaswc.aspx") + "?" + UrlEncode("" +AV7ContratoServicosIndicador_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE", StringUtil.LTrim( StringUtil.NToC( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO", StringUtil.LTrim( StringUtil.NToC( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_ATE", StringUtil.LTrim( StringUtil.NToC( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO", StringUtil.LTrim( StringUtil.NToC( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ", StringUtil.LTrim( StringUtil.NToC( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO", StringUtil.LTrim( StringUtil.NToC( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_17", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_17), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV33DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV33DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLEFILTERDATA", AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLEFILTERDATA", AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_DESDETITLEFILTERDATA", AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_DESDETITLEFILTERDATA", AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_ATETITLEFILTERDATA", AV25ContratoServicosIndicadorFaixa_AteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_ATETITLEFILTERDATA", AV25ContratoServicosIndicadorFaixa_AteTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_REDUZTITLEFILTERDATA", AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_REDUZTITLEFILTERDATA", AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV39Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADOR_CALCULOSOB", StringUtil.RTrim( A1345ContratoServicosIndicador_CalculoSob));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Caption", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Cls", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Caption", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Cls", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_desde_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_desde_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_desde_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_desde_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_desde_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Caption", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Cls", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_ate_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_ate_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_ate_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_ate_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_ate_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Caption", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Tooltip", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Cls", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_reduz_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_reduz_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_reduz_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filtertype", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_reduz_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosindicadorfaixa_reduz_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Sortasc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormJU2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosindicadorfaixaswc.js", "?2020311762954");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosIndicadorFaixasWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Indicador Faixas WC" ;
      }

      protected void WBJU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosindicadorfaixaswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_JU2( true) ;
         }
         else
         {
            wb_table1_2_JU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JU2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosIndicador_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosIndicador_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosIndicador_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,31);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_numero_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_numero_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_numero_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_desde_Internalname, StringUtil.LTrim( StringUtil.NToC( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV22TFContratoServicosIndicadorFaixa_Desde, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,33);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_desde_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_desde_Visible, 1, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV23TFContratoServicosIndicadorFaixa_Desde_To, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,34);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_desde_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_desde_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_ate_Internalname, StringUtil.LTrim( StringUtil.NToC( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV26TFContratoServicosIndicadorFaixa_Ate, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_ate_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_ate_Visible, 1, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV27TFContratoServicosIndicadorFaixa_Ate_To, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_ate_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_ate_to_Visible, 1, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_reduz_Internalname, StringUtil.LTrim( StringUtil.NToC( AV30TFContratoServicosIndicadorFaixa_Reduz, 8, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV30TFContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_reduz_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_reduz_Visible, 1, 0, "text", "", 60, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 8, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV31TFContratoServicosIndicadorFaixa_Reduz_To, "ZZ9.99 %")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosindicadorfaixa_reduz_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosindicadorfaixa_reduz_to_Visible, 1, 0, "text", "", 60, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Internalname, AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", 0, edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Internalname, AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Internalname, AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosIndicadorFaixasWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_17_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Internalname, AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosIndicadorFaixasWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTJU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Indicador Faixas WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPJU0( ) ;
            }
         }
      }

      protected void WSJU2( )
      {
         STARTJU2( ) ;
         EVTJU2( ) ;
      }

      protected void EVTJU2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11JU2 */
                                    E11JU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12JU2 */
                                    E12JU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13JU2 */
                                    E13JU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14JU2 */
                                    E14JU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15JU2 */
                                    E15JU2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPJU0( ) ;
                              }
                              nGXsfl_17_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
                              SubsflControlProps_172( ) ;
                              A1299ContratoServicosIndicadorFaixa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Codigo_Internalname), ",", "."));
                              A1300ContratoServicosIndicadorFaixa_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Numero_Internalname), ",", "."));
                              A1301ContratoServicosIndicadorFaixa_Desde = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Desde_Internalname), ",", ".");
                              AV16Label = cgiGet( edtavLabel_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavLabel_Internalname, AV16Label);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vLABEL"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, StringUtil.RTrim( context.localUtil.Format( AV16Label, ""))));
                              A1302ContratoServicosIndicadorFaixa_Ate = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Ate_Internalname), ",", ".");
                              A1303ContratoServicosIndicadorFaixa_Reduz = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Reduz_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16JU2 */
                                          E16JU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17JU2 */
                                          E17JU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18JU2 */
                                          E18JU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_numero Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO"), ",", ".") != Convert.ToDecimal( AV18TFContratoServicosIndicadorFaixa_Numero )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_numero_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV19TFContratoServicosIndicadorFaixa_Numero_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_desde Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE"), ",", ".") != AV22TFContratoServicosIndicadorFaixa_Desde )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_desde_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO"), ",", ".") != AV23TFContratoServicosIndicadorFaixa_Desde_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_ate Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_ATE"), ",", ".") != AV26TFContratoServicosIndicadorFaixa_Ate )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_ate_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO"), ",", ".") != AV27TFContratoServicosIndicadorFaixa_Ate_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_reduz Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ"), ",", ".") != AV30TFContratoServicosIndicadorFaixa_Reduz )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicosindicadorfaixa_reduz_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO"), ",", ".") != AV31TFContratoServicosIndicadorFaixa_Reduz_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPJU0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJU2( ) ;
            }
         }
      }

      protected void PAJU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContratoServicosIndicador_Formato.Name = "CONTRATOSERVICOSINDICADOR_FORMATO";
            cmbContratoServicosIndicador_Formato.WebTags = "";
            cmbContratoServicosIndicador_Formato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Porcentagem", 0);
            cmbContratoServicosIndicador_Formato.addItem("1", "Numeral", 0);
            if ( cmbContratoServicosIndicador_Formato.ItemCount > 0 )
            {
               A2052ContratoServicosIndicador_Formato = (short)(NumberUtil.Val( cmbContratoServicosIndicador_Formato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_172( ) ;
         while ( nGXsfl_17_idx <= nRC_GXsfl_17 )
         {
            sendrow_172( ) ;
            nGXsfl_17_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_17_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_17_idx+1));
            sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
            SubsflControlProps_172( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       short AV18TFContratoServicosIndicadorFaixa_Numero ,
                                       short AV19TFContratoServicosIndicadorFaixa_Numero_To ,
                                       decimal AV22TFContratoServicosIndicadorFaixa_Desde ,
                                       decimal AV23TFContratoServicosIndicadorFaixa_Desde_To ,
                                       decimal AV26TFContratoServicosIndicadorFaixa_Ate ,
                                       decimal AV27TFContratoServicosIndicadorFaixa_Ate_To ,
                                       decimal AV30TFContratoServicosIndicadorFaixa_Reduz ,
                                       decimal AV31TFContratoServicosIndicadorFaixa_Reduz_To ,
                                       int AV7ContratoServicosIndicador_Codigo ,
                                       String AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace ,
                                       String AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace ,
                                       String AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace ,
                                       String AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace ,
                                       String AV39Pgmname ,
                                       String A1345ContratoServicosIndicador_CalculoSob ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJU2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_DESDE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_DESDE", StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vLABEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV16Label, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vLABEL", StringUtil.RTrim( AV16Label));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_ATE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_ATE", StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_REDUZ", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_REDUZ", StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 6, 2, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicosIndicador_Formato.ItemCount > 0 )
         {
            A2052ContratoServicosIndicador_Formato = (short)(NumberUtil.Val( cmbContratoServicosIndicador_Formato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV39Pgmname = "ContratoServicosIndicadorFaixasWC";
         context.Gx_err = 0;
         edtavLabel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLabel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLabel_Enabled), 5, 0)));
      }

      protected void RFJU2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 17;
         /* Execute user event: E17JU2 */
         E17JU2 ();
         nGXsfl_17_idx = 1;
         sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
         SubsflControlProps_172( ) ;
         nGXsfl_17_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_172( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV18TFContratoServicosIndicadorFaixa_Numero ,
                                                 AV19TFContratoServicosIndicadorFaixa_Numero_To ,
                                                 AV22TFContratoServicosIndicadorFaixa_Desde ,
                                                 AV23TFContratoServicosIndicadorFaixa_Desde_To ,
                                                 AV26TFContratoServicosIndicadorFaixa_Ate ,
                                                 AV27TFContratoServicosIndicadorFaixa_Ate_To ,
                                                 AV30TFContratoServicosIndicadorFaixa_Reduz ,
                                                 AV31TFContratoServicosIndicadorFaixa_Reduz_To ,
                                                 A1300ContratoServicosIndicadorFaixa_Numero ,
                                                 A1301ContratoServicosIndicadorFaixa_Desde ,
                                                 A1302ContratoServicosIndicadorFaixa_Ate ,
                                                 A1303ContratoServicosIndicadorFaixa_Reduz ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1269ContratoServicosIndicador_Codigo ,
                                                 AV7ContratoServicosIndicador_Codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00JU2 */
            pr_default.execute(0, new Object[] {AV7ContratoServicosIndicador_Codigo, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_17_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1345ContratoServicosIndicador_CalculoSob = H00JU2_A1345ContratoServicosIndicador_CalculoSob[0];
               n1345ContratoServicosIndicador_CalculoSob = H00JU2_n1345ContratoServicosIndicador_CalculoSob[0];
               A2052ContratoServicosIndicador_Formato = H00JU2_A2052ContratoServicosIndicador_Formato[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
               A1269ContratoServicosIndicador_Codigo = H00JU2_A1269ContratoServicosIndicador_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
               A1303ContratoServicosIndicadorFaixa_Reduz = H00JU2_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               A1302ContratoServicosIndicadorFaixa_Ate = H00JU2_A1302ContratoServicosIndicadorFaixa_Ate[0];
               A1301ContratoServicosIndicadorFaixa_Desde = H00JU2_A1301ContratoServicosIndicadorFaixa_Desde[0];
               A1300ContratoServicosIndicadorFaixa_Numero = H00JU2_A1300ContratoServicosIndicadorFaixa_Numero[0];
               A1299ContratoServicosIndicadorFaixa_Codigo = H00JU2_A1299ContratoServicosIndicadorFaixa_Codigo[0];
               A1345ContratoServicosIndicador_CalculoSob = H00JU2_A1345ContratoServicosIndicador_CalculoSob[0];
               n1345ContratoServicosIndicador_CalculoSob = H00JU2_n1345ContratoServicosIndicador_CalculoSob[0];
               A2052ContratoServicosIndicador_Formato = H00JU2_A2052ContratoServicosIndicador_Formato[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
               /* Execute user event: E18JU2 */
               E18JU2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 17;
            WBJU0( ) ;
         }
         nGXsfl_17_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV18TFContratoServicosIndicadorFaixa_Numero ,
                                              AV19TFContratoServicosIndicadorFaixa_Numero_To ,
                                              AV22TFContratoServicosIndicadorFaixa_Desde ,
                                              AV23TFContratoServicosIndicadorFaixa_Desde_To ,
                                              AV26TFContratoServicosIndicadorFaixa_Ate ,
                                              AV27TFContratoServicosIndicadorFaixa_Ate_To ,
                                              AV30TFContratoServicosIndicadorFaixa_Reduz ,
                                              AV31TFContratoServicosIndicadorFaixa_Reduz_To ,
                                              A1300ContratoServicosIndicadorFaixa_Numero ,
                                              A1301ContratoServicosIndicadorFaixa_Desde ,
                                              A1302ContratoServicosIndicadorFaixa_Ate ,
                                              A1303ContratoServicosIndicadorFaixa_Reduz ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1269ContratoServicosIndicador_Codigo ,
                                              AV7ContratoServicosIndicador_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00JU3 */
         pr_default.execute(1, new Object[] {AV7ContratoServicosIndicador_Codigo, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To});
         GRID_nRecordCount = H00JU3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To, AV7ContratoServicosIndicador_Codigo, AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, AV39Pgmname, A1345ContratoServicosIndicador_CalculoSob, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To, AV7ContratoServicosIndicador_Codigo, AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, AV39Pgmname, A1345ContratoServicosIndicador_CalculoSob, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To, AV7ContratoServicosIndicador_Codigo, AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, AV39Pgmname, A1345ContratoServicosIndicador_CalculoSob, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To, AV7ContratoServicosIndicador_Codigo, AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, AV39Pgmname, A1345ContratoServicosIndicador_CalculoSob, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV18TFContratoServicosIndicadorFaixa_Numero, AV19TFContratoServicosIndicadorFaixa_Numero_To, AV22TFContratoServicosIndicadorFaixa_Desde, AV23TFContratoServicosIndicadorFaixa_Desde_To, AV26TFContratoServicosIndicadorFaixa_Ate, AV27TFContratoServicosIndicadorFaixa_Ate_To, AV30TFContratoServicosIndicadorFaixa_Reduz, AV31TFContratoServicosIndicadorFaixa_Reduz_To, AV7ContratoServicosIndicador_Codigo, AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, AV39Pgmname, A1345ContratoServicosIndicador_CalculoSob, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJU0( )
      {
         /* Before Start, stand alone formulas. */
         AV39Pgmname = "ContratoServicosIndicadorFaixasWC";
         context.Gx_err = 0;
         edtavLabel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavLabel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLabel_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16JU2 */
         E16JU2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV33DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLEFILTERDATA"), AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_DESDETITLEFILTERDATA"), AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_ATETITLEFILTERDATA"), AV25ContratoServicosIndicadorFaixa_AteTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSINDICADORFAIXA_REDUZTITLEFILTERDATA"), AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData);
            /* Read variables values. */
            cmbContratoServicosIndicador_Formato.Name = cmbContratoServicosIndicador_Formato_Internalname;
            cmbContratoServicosIndicador_Formato.CurrentValue = cgiGet( cmbContratoServicosIndicador_Formato_Internalname);
            A2052ContratoServicosIndicador_Formato = (short)(NumberUtil.Val( cgiGet( cmbContratoServicosIndicador_Formato_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2052ContratoServicosIndicador_Formato", StringUtil.LTrim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)));
            A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1269ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_numero_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_numero_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_numero_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18TFContratoServicosIndicadorFaixa_Numero = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosIndicadorFaixa_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0)));
            }
            else
            {
               AV18TFContratoServicosIndicadorFaixa_Numero = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_numero_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosIndicadorFaixa_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19TFContratoServicosIndicadorFaixa_Numero_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratoServicosIndicadorFaixa_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0)));
            }
            else
            {
               AV19TFContratoServicosIndicadorFaixa_Numero_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratoServicosIndicadorFaixa_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_desde_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_desde_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_desde_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFContratoServicosIndicadorFaixa_Desde = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosIndicadorFaixa_Desde", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2)));
            }
            else
            {
               AV22TFContratoServicosIndicadorFaixa_Desde = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_desde_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosIndicadorFaixa_Desde", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFContratoServicosIndicadorFaixa_Desde_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosIndicadorFaixa_Desde_To", StringUtil.LTrim( StringUtil.Str( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2)));
            }
            else
            {
               AV23TFContratoServicosIndicadorFaixa_Desde_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosIndicadorFaixa_Desde_To", StringUtil.LTrim( StringUtil.Str( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_ate_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_ate_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_ATE");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_ate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26TFContratoServicosIndicadorFaixa_Ate = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosIndicadorFaixa_Ate", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2)));
            }
            else
            {
               AV26TFContratoServicosIndicadorFaixa_Ate = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_ate_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosIndicadorFaixa_Ate", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27TFContratoServicosIndicadorFaixa_Ate_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosIndicadorFaixa_Ate_To", StringUtil.LTrim( StringUtil.Str( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2)));
            }
            else
            {
               AV27TFContratoServicosIndicadorFaixa_Ate_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosIndicadorFaixa_Ate_To", StringUtil.LTrim( StringUtil.Str( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_reduz_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_reduz_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_reduz_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30TFContratoServicosIndicadorFaixa_Reduz = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicadorFaixa_Reduz", StringUtil.LTrim( StringUtil.Str( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2)));
            }
            else
            {
               AV30TFContratoServicosIndicadorFaixa_Reduz = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_reduz_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicadorFaixa_Reduz", StringUtil.LTrim( StringUtil.Str( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO");
               GX_FocusControl = edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFContratoServicosIndicadorFaixa_Reduz_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosIndicadorFaixa_Reduz_To", StringUtil.LTrim( StringUtil.Str( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2)));
            }
            else
            {
               AV31TFContratoServicosIndicadorFaixa_Reduz_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosIndicadorFaixa_Reduz_To", StringUtil.LTrim( StringUtil.Str( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2)));
            }
            AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace", AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace);
            AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace", AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace);
            AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace", AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace);
            AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace", AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_17 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_17"), ",", "."));
            AV35GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV36GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosIndicador_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosindicadorfaixa_numero_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Caption");
            Ddo_contratoservicosindicadorfaixa_numero_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Tooltip");
            Ddo_contratoservicosindicadorfaixa_numero_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Cls");
            Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtext_set");
            Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtextto_set");
            Ddo_contratoservicosindicadorfaixa_numero_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Dropdownoptionstype");
            Ddo_contratoservicosindicadorfaixa_numero_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicadorfaixa_numero_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includesortasc"));
            Ddo_contratoservicosindicadorfaixa_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includesortdsc"));
            Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Sortedstatus");
            Ddo_contratoservicosindicadorfaixa_numero_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includefilter"));
            Ddo_contratoservicosindicadorfaixa_numero_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filtertype");
            Ddo_contratoservicosindicadorfaixa_numero_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filterisrange"));
            Ddo_contratoservicosindicadorfaixa_numero_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Includedatalist"));
            Ddo_contratoservicosindicadorfaixa_numero_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Sortasc");
            Ddo_contratoservicosindicadorfaixa_numero_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Sortdsc");
            Ddo_contratoservicosindicadorfaixa_numero_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Cleanfilter");
            Ddo_contratoservicosindicadorfaixa_numero_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Rangefilterfrom");
            Ddo_contratoservicosindicadorfaixa_numero_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Rangefilterto");
            Ddo_contratoservicosindicadorfaixa_numero_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Searchbuttontext");
            Ddo_contratoservicosindicadorfaixa_desde_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Caption");
            Ddo_contratoservicosindicadorfaixa_desde_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Tooltip");
            Ddo_contratoservicosindicadorfaixa_desde_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Cls");
            Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtext_set");
            Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtextto_set");
            Ddo_contratoservicosindicadorfaixa_desde_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Dropdownoptionstype");
            Ddo_contratoservicosindicadorfaixa_desde_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicadorfaixa_desde_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includesortasc"));
            Ddo_contratoservicosindicadorfaixa_desde_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includesortdsc"));
            Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Sortedstatus");
            Ddo_contratoservicosindicadorfaixa_desde_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includefilter"));
            Ddo_contratoservicosindicadorfaixa_desde_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filtertype");
            Ddo_contratoservicosindicadorfaixa_desde_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filterisrange"));
            Ddo_contratoservicosindicadorfaixa_desde_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Includedatalist"));
            Ddo_contratoservicosindicadorfaixa_desde_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Sortasc");
            Ddo_contratoservicosindicadorfaixa_desde_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Sortdsc");
            Ddo_contratoservicosindicadorfaixa_desde_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Cleanfilter");
            Ddo_contratoservicosindicadorfaixa_desde_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Rangefilterfrom");
            Ddo_contratoservicosindicadorfaixa_desde_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Rangefilterto");
            Ddo_contratoservicosindicadorfaixa_desde_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Searchbuttontext");
            Ddo_contratoservicosindicadorfaixa_ate_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Caption");
            Ddo_contratoservicosindicadorfaixa_ate_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Tooltip");
            Ddo_contratoservicosindicadorfaixa_ate_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Cls");
            Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtext_set");
            Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtextto_set");
            Ddo_contratoservicosindicadorfaixa_ate_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Dropdownoptionstype");
            Ddo_contratoservicosindicadorfaixa_ate_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicadorfaixa_ate_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includesortasc"));
            Ddo_contratoservicosindicadorfaixa_ate_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includesortdsc"));
            Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Sortedstatus");
            Ddo_contratoservicosindicadorfaixa_ate_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includefilter"));
            Ddo_contratoservicosindicadorfaixa_ate_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filtertype");
            Ddo_contratoservicosindicadorfaixa_ate_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filterisrange"));
            Ddo_contratoservicosindicadorfaixa_ate_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Includedatalist"));
            Ddo_contratoservicosindicadorfaixa_ate_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Sortasc");
            Ddo_contratoservicosindicadorfaixa_ate_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Sortdsc");
            Ddo_contratoservicosindicadorfaixa_ate_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Cleanfilter");
            Ddo_contratoservicosindicadorfaixa_ate_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Rangefilterfrom");
            Ddo_contratoservicosindicadorfaixa_ate_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Rangefilterto");
            Ddo_contratoservicosindicadorfaixa_ate_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Searchbuttontext");
            Ddo_contratoservicosindicadorfaixa_reduz_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Caption");
            Ddo_contratoservicosindicadorfaixa_reduz_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Tooltip");
            Ddo_contratoservicosindicadorfaixa_reduz_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Cls");
            Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtext_set");
            Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtextto_set");
            Ddo_contratoservicosindicadorfaixa_reduz_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Dropdownoptionstype");
            Ddo_contratoservicosindicadorfaixa_reduz_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Titlecontrolidtoreplace");
            Ddo_contratoservicosindicadorfaixa_reduz_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includesortasc"));
            Ddo_contratoservicosindicadorfaixa_reduz_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includesortdsc"));
            Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Sortedstatus");
            Ddo_contratoservicosindicadorfaixa_reduz_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includefilter"));
            Ddo_contratoservicosindicadorfaixa_reduz_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filtertype");
            Ddo_contratoservicosindicadorfaixa_reduz_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filterisrange"));
            Ddo_contratoservicosindicadorfaixa_reduz_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Includedatalist"));
            Ddo_contratoservicosindicadorfaixa_reduz_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Sortasc");
            Ddo_contratoservicosindicadorfaixa_reduz_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Sortdsc");
            Ddo_contratoservicosindicadorfaixa_reduz_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Cleanfilter");
            Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Rangefilterfrom");
            Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Rangefilterto");
            Ddo_contratoservicosindicadorfaixa_reduz_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Activeeventkey");
            Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtext_get");
            Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO_Filteredtextto_get");
            Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Activeeventkey");
            Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtext_get");
            Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE_Filteredtextto_get");
            Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Activeeventkey");
            Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtext_get");
            Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE_Filteredtextto_get");
            Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Activeeventkey");
            Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtext_get");
            Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO"), ",", ".") != Convert.ToDecimal( AV18TFContratoServicosIndicadorFaixa_Numero )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO"), ",", ".") != Convert.ToDecimal( AV19TFContratoServicosIndicadorFaixa_Numero_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE"), ",", ".") != AV22TFContratoServicosIndicadorFaixa_Desde )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO"), ",", ".") != AV23TFContratoServicosIndicadorFaixa_Desde_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_ATE"), ",", ".") != AV26TFContratoServicosIndicadorFaixa_Ate )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO"), ",", ".") != AV27TFContratoServicosIndicadorFaixa_Ate_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ"), ",", ".") != AV30TFContratoServicosIndicadorFaixa_Reduz )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO"), ",", ".") != AV31TFContratoServicosIndicadorFaixa_Reduz_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16JU2 */
         E16JU2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16JU2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratoservicosindicadorfaixa_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_numero_Visible), 5, 0)));
         edtavTfcontratoservicosindicadorfaixa_numero_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_numero_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicadorfaixa_desde_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_desde_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_desde_Visible), 5, 0)));
         edtavTfcontratoservicosindicadorfaixa_desde_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_desde_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicadorfaixa_ate_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_ate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_ate_Visible), 5, 0)));
         edtavTfcontratoservicosindicadorfaixa_ate_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_ate_to_Visible), 5, 0)));
         edtavTfcontratoservicosindicadorfaixa_reduz_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_reduz_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_reduz_Visible), 5, 0)));
         edtavTfcontratoservicosindicadorfaixa_reduz_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosindicadorfaixa_reduz_to_Visible), 5, 0)));
         Ddo_contratoservicosindicadorfaixa_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicadorFaixa_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_numero_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicadorfaixa_numero_Titlecontrolidtoreplace);
         AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace = Ddo_contratoservicosindicadorfaixa_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace", AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace);
         edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicadorfaixa_desde_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicadorFaixa_Desde";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_desde_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicadorfaixa_desde_Titlecontrolidtoreplace);
         AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace = Ddo_contratoservicosindicadorfaixa_desde_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace", AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace);
         edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicadorfaixa_ate_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicadorFaixa_Ate";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_ate_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicadorfaixa_ate_Titlecontrolidtoreplace);
         AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace = Ddo_contratoservicosindicadorfaixa_ate_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace", AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace);
         edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosindicadorfaixa_reduz_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosIndicadorFaixa_Reduz";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_reduz_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosindicadorfaixa_reduz_Titlecontrolidtoreplace);
         AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace = Ddo_contratoservicosindicadorfaixa_reduz_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace", AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace);
         edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Visible), 5, 0)));
         edtContratoServicosIndicador_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicador_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosIndicador_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV33DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV33DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E17JU2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25ContratoServicosIndicadorFaixa_AteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosIndicadorFaixa_Numero_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero", AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicadorFaixa_Numero_Internalname, "Title", edtContratoServicosIndicadorFaixa_Numero_Title);
         edtContratoServicosIndicadorFaixa_Desde_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Desde_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dias", AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Title", edtContratoServicosIndicadorFaixa_Desde_Title);
         edtContratoServicosIndicadorFaixa_Ate_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Ate_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "", AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicadorFaixa_Ate_Internalname, "Title", edtContratoServicosIndicadorFaixa_Ate_Title);
         edtContratoServicosIndicadorFaixa_Reduz_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Reduz_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Reduz", AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosIndicadorFaixa_Reduz_Internalname, "Title", edtContratoServicosIndicadorFaixa_Reduz_Title);
         AV35GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridCurrentPage), 10, 0)));
         AV36GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36GridPageCount), 10, 0)));
         AV16Label = " at� ";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavLabel_Internalname, AV16Label);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vLABEL"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, StringUtil.RTrim( context.localUtil.Format( AV16Label, ""))));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData", AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData", AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV25ContratoServicosIndicadorFaixa_AteTitleFilterData", AV25ContratoServicosIndicadorFaixa_AteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData", AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData);
      }

      protected void E11JU2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV34PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV34PageToGo) ;
         }
      }

      protected void E12JU2( )
      {
         /* Ddo_contratoservicosindicadorfaixa_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV18TFContratoServicosIndicadorFaixa_Numero = (short)(NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosIndicadorFaixa_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0)));
            AV19TFContratoServicosIndicadorFaixa_Numero_To = (short)(NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratoServicosIndicadorFaixa_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13JU2( )
      {
         /* Ddo_contratoservicosindicadorfaixa_desde_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_desde_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_desde_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV22TFContratoServicosIndicadorFaixa_Desde = NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosIndicadorFaixa_Desde", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2)));
            AV23TFContratoServicosIndicadorFaixa_Desde_To = NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosIndicadorFaixa_Desde_To", StringUtil.LTrim( StringUtil.Str( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E14JU2( )
      {
         /* Ddo_contratoservicosindicadorfaixa_ate_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_ate_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_ate_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV26TFContratoServicosIndicadorFaixa_Ate = NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosIndicadorFaixa_Ate", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2)));
            AV27TFContratoServicosIndicadorFaixa_Ate_To = NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosIndicadorFaixa_Ate_To", StringUtil.LTrim( StringUtil.Str( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E15JU2( )
      {
         /* Ddo_contratoservicosindicadorfaixa_reduz_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_reduz_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_reduz_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV30TFContratoServicosIndicadorFaixa_Reduz = NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicadorFaixa_Reduz", StringUtil.LTrim( StringUtil.Str( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2)));
            AV31TFContratoServicosIndicadorFaixa_Reduz_To = NumberUtil.Val( Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosIndicadorFaixa_Reduz_To", StringUtil.LTrim( StringUtil.Str( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
      }

      private void E18JU2( )
      {
         /* Grid_Load Routine */
         edtContratoServicosIndicadorFaixa_Desde_Title = ((StringUtil.StrCmp(A1345ContratoServicosIndicador_CalculoSob, "D")==0) ? "Dias" : "Percentuais");
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 17;
         }
         sendrow_172( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_17_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(17, GridRow);
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus);
         Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_desde_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus);
         Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_ate_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus);
         Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_reduz_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_numero_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_desde_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_ate_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_reduz_Internalname, "SortedStatus", Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV39Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV39Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV39Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV40GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 )
            {
               AV18TFContratoServicosIndicadorFaixa_Numero = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFContratoServicosIndicadorFaixa_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0)));
               AV19TFContratoServicosIndicadorFaixa_Numero_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19TFContratoServicosIndicadorFaixa_Numero_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0)));
               if ( ! (0==AV18TFContratoServicosIndicadorFaixa_Numero) )
               {
                  Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_set = StringUtil.Str( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_numero_Internalname, "FilteredText_set", Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_set);
               }
               if ( ! (0==AV19TFContratoServicosIndicadorFaixa_Numero_To) )
               {
                  Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_set = StringUtil.Str( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_numero_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADORFAIXA_DESDE") == 0 )
            {
               AV22TFContratoServicosIndicadorFaixa_Desde = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22TFContratoServicosIndicadorFaixa_Desde", StringUtil.LTrim( StringUtil.Str( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2)));
               AV23TFContratoServicosIndicadorFaixa_Desde_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFContratoServicosIndicadorFaixa_Desde_To", StringUtil.LTrim( StringUtil.Str( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosIndicadorFaixa_Desde) )
               {
                  Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_set = StringUtil.Str( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_desde_Internalname, "FilteredText_set", Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV23TFContratoServicosIndicadorFaixa_Desde_To) )
               {
                  Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_set = StringUtil.Str( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_desde_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADORFAIXA_ATE") == 0 )
            {
               AV26TFContratoServicosIndicadorFaixa_Ate = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26TFContratoServicosIndicadorFaixa_Ate", StringUtil.LTrim( StringUtil.Str( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2)));
               AV27TFContratoServicosIndicadorFaixa_Ate_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFContratoServicosIndicadorFaixa_Ate_To", StringUtil.LTrim( StringUtil.Str( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosIndicadorFaixa_Ate) )
               {
                  Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_set = StringUtil.Str( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_ate_Internalname, "FilteredText_set", Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV27TFContratoServicosIndicadorFaixa_Ate_To) )
               {
                  Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_set = StringUtil.Str( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_ate_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSINDICADORFAIXA_REDUZ") == 0 )
            {
               AV30TFContratoServicosIndicadorFaixa_Reduz = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30TFContratoServicosIndicadorFaixa_Reduz", StringUtil.LTrim( StringUtil.Str( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2)));
               AV31TFContratoServicosIndicadorFaixa_Reduz_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31TFContratoServicosIndicadorFaixa_Reduz_To", StringUtil.LTrim( StringUtil.Str( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV30TFContratoServicosIndicadorFaixa_Reduz) )
               {
                  Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_set = StringUtil.Str( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_reduz_Internalname, "FilteredText_set", Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV31TFContratoServicosIndicadorFaixa_Reduz_To) )
               {
                  Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_set = StringUtil.Str( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicosindicadorfaixa_reduz_Internalname, "FilteredTextTo_set", Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_set);
               }
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV39Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV18TFContratoServicosIndicadorFaixa_Numero) && (0==AV19TFContratoServicosIndicadorFaixa_Numero_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADORFAIXA_NUMERO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV18TFContratoServicosIndicadorFaixa_Numero), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV19TFContratoServicosIndicadorFaixa_Numero_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV22TFContratoServicosIndicadorFaixa_Desde) && (Convert.ToDecimal(0)==AV23TFContratoServicosIndicadorFaixa_Desde_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADORFAIXA_DESDE";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV22TFContratoServicosIndicadorFaixa_Desde, 6, 2);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV23TFContratoServicosIndicadorFaixa_Desde_To, 6, 2);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV26TFContratoServicosIndicadorFaixa_Ate) && (Convert.ToDecimal(0)==AV27TFContratoServicosIndicadorFaixa_Ate_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADORFAIXA_ATE";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV26TFContratoServicosIndicadorFaixa_Ate, 6, 2);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV27TFContratoServicosIndicadorFaixa_Ate_To, 6, 2);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV30TFContratoServicosIndicadorFaixa_Reduz) && (Convert.ToDecimal(0)==AV31TFContratoServicosIndicadorFaixa_Reduz_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSINDICADORFAIXA_REDUZ";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV30TFContratoServicosIndicadorFaixa_Reduz, 6, 2);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV31TFContratoServicosIndicadorFaixa_Reduz_To, 6, 2);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV39Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV39Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicosIndicador";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicosIndicador_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_JU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosindicador_formato_Internalname, "Formato", "", "", lblTextblockcontratoservicosindicador_formato_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosIndicador_Formato, cmbContratoServicosIndicador_Formato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0)), 1, cmbContratoServicosIndicador_Formato_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ContratoServicosIndicadorFaixasWC.htm");
            cmbContratoServicosIndicador_Formato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2052ContratoServicosIndicador_Formato), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratoServicosIndicador_Formato_Internalname, "Values", (String)(cmbContratoServicosIndicador_Formato.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_14_JU2( true) ;
         }
         else
         {
            wb_table2_14_JU2( false) ;
         }
         return  ;
      }

      protected void wb_table2_14_JU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JU2e( true) ;
         }
         else
         {
            wb_table1_2_JU2e( false) ;
         }
      }

      protected void wb_table2_14_JU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"17\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(51), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Desde_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Desde_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Desde_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Ate_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Ate_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Ate_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Reduz_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Reduz_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Reduz_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Desde_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV16Label));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLabel_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Ate_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Reduz_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 17 )
         {
            wbEnd = 0;
            nRC_GXsfl_17 = (short)(nGXsfl_17_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_14_JU2e( true) ;
         }
         else
         {
            wb_table2_14_JU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoServicosIndicador_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJU2( ) ;
         WSJU2( ) ;
         WEJU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoServicosIndicador_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAJU2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosindicadorfaixaswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAJU2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoServicosIndicador_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0)));
         }
         wcpOAV7ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosIndicador_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoServicosIndicador_Codigo != wcpOAV7ContratoServicosIndicador_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoServicosIndicador_Codigo = AV7ContratoServicosIndicador_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoServicosIndicador_Codigo = cgiGet( sPrefix+"AV7ContratoServicosIndicador_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoServicosIndicador_Codigo) > 0 )
         {
            AV7ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoServicosIndicador_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosIndicador_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0)));
         }
         else
         {
            AV7ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoServicosIndicador_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAJU2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSJU2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSJU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosIndicador_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosIndicador_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoServicosIndicador_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosIndicador_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContratoServicosIndicador_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEJU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311763355");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosindicadorfaixaswc.js", "?2020311763355");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_172( )
      {
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_17_idx;
         edtContratoServicosIndicadorFaixa_Numero_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_17_idx;
         edtContratoServicosIndicadorFaixa_Desde_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_17_idx;
         edtavLabel_Internalname = sPrefix+"vLABEL_"+sGXsfl_17_idx;
         edtContratoServicosIndicadorFaixa_Ate_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_17_idx;
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_17_idx;
      }

      protected void SubsflControlProps_fel_172( )
      {
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_17_fel_idx;
         edtContratoServicosIndicadorFaixa_Numero_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_17_fel_idx;
         edtContratoServicosIndicadorFaixa_Desde_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_17_fel_idx;
         edtavLabel_Internalname = sPrefix+"vLABEL_"+sGXsfl_17_fel_idx;
         edtContratoServicosIndicadorFaixa_Ate_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_17_fel_idx;
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_17_fel_idx;
      }

      protected void sendrow_172( )
      {
         SubsflControlProps_172( ) ;
         WBJU0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_17_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_17_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_17_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Numero_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)51,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Desde_Internalname,StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", "")),context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Desde_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLabel_Internalname,StringUtil.RTrim( AV16Label),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLabel_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavLabel_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Ate_Internalname,StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", "")),context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Ate_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)46,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Reduz_Internalname,StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ",", "")),context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Reduz_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_CODIGO"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_NUMERO"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_DESDE"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vLABEL"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, StringUtil.RTrim( context.localUtil.Format( AV16Label, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_ATE"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSINDICADORFAIXA_REDUZ"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sPrefix+sGXsfl_17_idx, context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")));
            GridContainer.AddRow(GridRow);
            nGXsfl_17_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_17_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_17_idx+1));
            sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
            SubsflControlProps_172( ) ;
         }
         /* End function sendrow_172 */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicosindicador_formato_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSINDICADOR_FORMATO";
         cmbContratoServicosIndicador_Formato_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_FORMATO";
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_CODIGO";
         edtContratoServicosIndicadorFaixa_Numero_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         edtContratoServicosIndicadorFaixa_Desde_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_DESDE";
         edtavLabel_Internalname = sPrefix+"vLABEL";
         edtContratoServicosIndicadorFaixa_Ate_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_ATE";
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = sPrefix+"CONTRATOSERVICOSINDICADORFAIXA_REDUZ";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContratoServicosIndicador_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSINDICADOR_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratoservicosindicadorfaixa_numero_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO";
         edtavTfcontratoservicosindicadorfaixa_desde_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE";
         edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO";
         edtavTfcontratoservicosindicadorfaixa_ate_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_ATE";
         edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO";
         edtavTfcontratoservicosindicadorfaixa_reduz_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ";
         edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO";
         Ddo_contratoservicosindicadorfaixa_numero_Internalname = sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicadorfaixa_desde_Internalname = sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE";
         edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSINDICADORFAIXA_DESDETITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicadorfaixa_ate_Internalname = sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE";
         edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSINDICADORFAIXA_ATETITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosindicadorfaixa_reduz_Internalname = sPrefix+"DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ";
         edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosIndicadorFaixa_Reduz_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Ate_Jsonclick = "";
         edtavLabel_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Desde_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Numero_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavLabel_Enabled = 0;
         edtContratoServicosIndicadorFaixa_Reduz_Titleformat = 0;
         edtContratoServicosIndicadorFaixa_Ate_Titleformat = 0;
         edtContratoServicosIndicadorFaixa_Desde_Titleformat = 0;
         edtContratoServicosIndicadorFaixa_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbContratoServicosIndicador_Formato_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Reduz_Title = "Reduz";
         edtContratoServicosIndicadorFaixa_Ate_Title = "";
         edtContratoServicosIndicadorFaixa_Desde_Title = "Dias";
         edtContratoServicosIndicadorFaixa_Numero_Title = "N�mero";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_reduz_to_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_reduz_to_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_reduz_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_reduz_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_ate_to_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_ate_to_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_ate_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_ate_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_desde_to_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_desde_to_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_desde_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_desde_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_numero_to_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_numero_to_Visible = 1;
         edtavTfcontratoservicosindicadorfaixa_numero_Jsonclick = "";
         edtavTfcontratoservicosindicadorfaixa_numero_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContratoServicosIndicador_Codigo_Jsonclick = "";
         edtContratoServicosIndicador_Codigo_Visible = 1;
         Ddo_contratoservicosindicadorfaixa_reduz_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterto = "At�";
         Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicadorfaixa_reduz_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicadorfaixa_reduz_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicadorfaixa_reduz_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicadorfaixa_reduz_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicadorfaixa_reduz_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_reduz_Filtertype = "Numeric";
         Ddo_contratoservicosindicadorfaixa_reduz_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_reduz_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_reduz_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_reduz_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicadorfaixa_reduz_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicadorfaixa_reduz_Cls = "ColumnSettings";
         Ddo_contratoservicosindicadorfaixa_reduz_Tooltip = "Op��es";
         Ddo_contratoservicosindicadorfaixa_reduz_Caption = "";
         Ddo_contratoservicosindicadorfaixa_ate_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicadorfaixa_ate_Rangefilterto = "At�";
         Ddo_contratoservicosindicadorfaixa_ate_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicadorfaixa_ate_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicadorfaixa_ate_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicadorfaixa_ate_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicadorfaixa_ate_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicadorfaixa_ate_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_ate_Filtertype = "Numeric";
         Ddo_contratoservicosindicadorfaixa_ate_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_ate_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_ate_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_ate_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicadorfaixa_ate_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicadorfaixa_ate_Cls = "ColumnSettings";
         Ddo_contratoservicosindicadorfaixa_ate_Tooltip = "Op��es";
         Ddo_contratoservicosindicadorfaixa_ate_Caption = "";
         Ddo_contratoservicosindicadorfaixa_desde_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicadorfaixa_desde_Rangefilterto = "At�";
         Ddo_contratoservicosindicadorfaixa_desde_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicadorfaixa_desde_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicadorfaixa_desde_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicadorfaixa_desde_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicadorfaixa_desde_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicadorfaixa_desde_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_desde_Filtertype = "Numeric";
         Ddo_contratoservicosindicadorfaixa_desde_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_desde_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_desde_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_desde_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicadorfaixa_desde_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicadorfaixa_desde_Cls = "ColumnSettings";
         Ddo_contratoservicosindicadorfaixa_desde_Tooltip = "Op��es";
         Ddo_contratoservicosindicadorfaixa_desde_Caption = "";
         Ddo_contratoservicosindicadorfaixa_numero_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosindicadorfaixa_numero_Rangefilterto = "At�";
         Ddo_contratoservicosindicadorfaixa_numero_Rangefilterfrom = "Desde";
         Ddo_contratoservicosindicadorfaixa_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosindicadorfaixa_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosindicadorfaixa_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosindicadorfaixa_numero_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosindicadorfaixa_numero_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_numero_Filtertype = "Numeric";
         Ddo_contratoservicosindicadorfaixa_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosindicadorfaixa_numero_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosindicadorfaixa_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosindicadorfaixa_numero_Cls = "ColumnSettings";
         Ddo_contratoservicosindicadorfaixa_numero_Tooltip = "Op��es";
         Ddo_contratoservicosindicadorfaixa_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_DESDETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_ATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFContratoServicosIndicadorFaixa_Numero',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO',pic:'ZZZ9',nv:0},{av:'AV19TFContratoServicosIndicadorFaixa_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV22TFContratoServicosIndicadorFaixa_Desde',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE',pic:'ZZ9.99',nv:0.0},{av:'AV23TFContratoServicosIndicadorFaixa_Desde_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV26TFContratoServicosIndicadorFaixa_Ate',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE',pic:'ZZ9.99',nv:0.0},{av:'AV27TFContratoServicosIndicadorFaixa_Ate_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV30TFContratoServicosIndicadorFaixa_Reduz',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ',pic:'ZZ9.99 %',nv:0.0},{av:'AV31TFContratoServicosIndicadorFaixa_Reduz_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO',pic:'ZZ9.99 %',nv:0.0}],oparms:[{av:'AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData',fld:'vCONTRATOSERVICOSINDICADORFAIXA_DESDETITLEFILTERDATA',pic:'',nv:null},{av:'AV25ContratoServicosIndicadorFaixa_AteTitleFilterData',fld:'vCONTRATOSERVICOSINDICADORFAIXA_ATETITLEFILTERDATA',pic:'',nv:null},{av:'AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData',fld:'vCONTRATOSERVICOSINDICADORFAIXA_REDUZTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosIndicadorFaixa_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Desde_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Ate_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Ate_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Title'},{av:'AV35GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV36GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV16Label',fld:'vLABEL',pic:'',hsh:true,nv:''}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11JU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFContratoServicosIndicadorFaixa_Numero',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO',pic:'ZZZ9',nv:0},{av:'AV19TFContratoServicosIndicadorFaixa_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV22TFContratoServicosIndicadorFaixa_Desde',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE',pic:'ZZ9.99',nv:0.0},{av:'AV23TFContratoServicosIndicadorFaixa_Desde_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV26TFContratoServicosIndicadorFaixa_Ate',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE',pic:'ZZ9.99',nv:0.0},{av:'AV27TFContratoServicosIndicadorFaixa_Ate_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV30TFContratoServicosIndicadorFaixa_Reduz',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ',pic:'ZZ9.99 %',nv:0.0},{av:'AV31TFContratoServicosIndicadorFaixa_Reduz_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV7ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_DESDETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_ATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO.ONOPTIONCLICKED","{handler:'E12JU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFContratoServicosIndicadorFaixa_Numero',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO',pic:'ZZZ9',nv:0},{av:'AV19TFContratoServicosIndicadorFaixa_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV22TFContratoServicosIndicadorFaixa_Desde',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE',pic:'ZZ9.99',nv:0.0},{av:'AV23TFContratoServicosIndicadorFaixa_Desde_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV26TFContratoServicosIndicadorFaixa_Ate',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE',pic:'ZZ9.99',nv:0.0},{av:'AV27TFContratoServicosIndicadorFaixa_Ate_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV30TFContratoServicosIndicadorFaixa_Reduz',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ',pic:'ZZ9.99 %',nv:0.0},{av:'AV31TFContratoServicosIndicadorFaixa_Reduz_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV7ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_DESDETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_ATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'SortedStatus'},{av:'AV18TFContratoServicosIndicadorFaixa_Numero',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO',pic:'ZZZ9',nv:0},{av:'AV19TFContratoServicosIndicadorFaixa_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE.ONOPTIONCLICKED","{handler:'E13JU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFContratoServicosIndicadorFaixa_Numero',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO',pic:'ZZZ9',nv:0},{av:'AV19TFContratoServicosIndicadorFaixa_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV22TFContratoServicosIndicadorFaixa_Desde',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE',pic:'ZZ9.99',nv:0.0},{av:'AV23TFContratoServicosIndicadorFaixa_Desde_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV26TFContratoServicosIndicadorFaixa_Ate',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE',pic:'ZZ9.99',nv:0.0},{av:'AV27TFContratoServicosIndicadorFaixa_Ate_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV30TFContratoServicosIndicadorFaixa_Reduz',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ',pic:'ZZ9.99 %',nv:0.0},{av:'AV31TFContratoServicosIndicadorFaixa_Reduz_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV7ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_DESDETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_ATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'SortedStatus'},{av:'AV22TFContratoServicosIndicadorFaixa_Desde',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE',pic:'ZZ9.99',nv:0.0},{av:'AV23TFContratoServicosIndicadorFaixa_Desde_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE.ONOPTIONCLICKED","{handler:'E14JU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFContratoServicosIndicadorFaixa_Numero',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO',pic:'ZZZ9',nv:0},{av:'AV19TFContratoServicosIndicadorFaixa_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV22TFContratoServicosIndicadorFaixa_Desde',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE',pic:'ZZ9.99',nv:0.0},{av:'AV23TFContratoServicosIndicadorFaixa_Desde_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV26TFContratoServicosIndicadorFaixa_Ate',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE',pic:'ZZ9.99',nv:0.0},{av:'AV27TFContratoServicosIndicadorFaixa_Ate_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV30TFContratoServicosIndicadorFaixa_Reduz',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ',pic:'ZZ9.99 %',nv:0.0},{av:'AV31TFContratoServicosIndicadorFaixa_Reduz_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV7ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_DESDETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_ATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'SortedStatus'},{av:'AV26TFContratoServicosIndicadorFaixa_Ate',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE',pic:'ZZ9.99',nv:0.0},{av:'AV27TFContratoServicosIndicadorFaixa_Ate_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ.ONOPTIONCLICKED","{handler:'E15JU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV18TFContratoServicosIndicadorFaixa_Numero',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO',pic:'ZZZ9',nv:0},{av:'AV19TFContratoServicosIndicadorFaixa_Numero_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_NUMERO_TO',pic:'ZZZ9',nv:0},{av:'AV22TFContratoServicosIndicadorFaixa_Desde',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE',pic:'ZZ9.99',nv:0.0},{av:'AV23TFContratoServicosIndicadorFaixa_Desde_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_DESDE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV26TFContratoServicosIndicadorFaixa_Ate',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE',pic:'ZZ9.99',nv:0.0},{av:'AV27TFContratoServicosIndicadorFaixa_Ate_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_ATE_TO',pic:'ZZ9.99',nv:0.0},{av:'AV30TFContratoServicosIndicadorFaixa_Reduz',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ',pic:'ZZ9.99 %',nv:0.0},{av:'AV31TFContratoServicosIndicadorFaixa_Reduz_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO',pic:'ZZ9.99 %',nv:0.0},{av:'AV7ContratoServicosIndicador_Codigo',fld:'vCONTRATOSERVICOSINDICADOR_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_DESDETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_ATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'FilteredText_get'},{av:'Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'SortedStatus'},{av:'AV30TFContratoServicosIndicadorFaixa_Reduz',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ',pic:'ZZ9.99 %',nv:0.0},{av:'AV31TFContratoServicosIndicadorFaixa_Reduz_To',fld:'vTFCONTRATOSERVICOSINDICADORFAIXA_REDUZ_TO',pic:'ZZ9.99 %',nv:0.0},{av:'Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'SortedStatus'},{av:'Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E18JU2',iparms:[{av:'A1345ContratoServicosIndicador_CalculoSob',fld:'CONTRATOSERVICOSINDICADOR_CALCULOSOB',pic:'',nv:''}],oparms:[{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey = "";
         Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_get = "";
         Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_get = "";
         Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey = "";
         Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_get = "";
         Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_get = "";
         Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey = "";
         Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_get = "";
         Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_get = "";
         Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey = "";
         Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_get = "";
         Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV18TFContratoServicosIndicadorFaixa_Numero = 0;
         AV19TFContratoServicosIndicadorFaixa_Numero_To = 0;
         AV22TFContratoServicosIndicadorFaixa_Desde = 0;
         AV23TFContratoServicosIndicadorFaixa_Desde_To = 0;
         AV26TFContratoServicosIndicadorFaixa_Ate = 0;
         AV27TFContratoServicosIndicadorFaixa_Ate_To = 0;
         AV30TFContratoServicosIndicadorFaixa_Reduz = 0;
         AV31TFContratoServicosIndicadorFaixa_Reduz_To = 0;
         AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace = "";
         AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace = "";
         AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace = "";
         AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace = "";
         AV39Pgmname = "";
         A1345ContratoServicosIndicador_CalculoSob = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV33DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25ContratoServicosIndicadorFaixa_AteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_set = "";
         Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_set = "";
         Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus = "";
         Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_set = "";
         Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_set = "";
         Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus = "";
         Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_set = "";
         Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_set = "";
         Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus = "";
         Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_set = "";
         Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_set = "";
         Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Label = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00JU2_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         H00JU2_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         H00JU2_A2052ContratoServicosIndicador_Formato = new short[1] ;
         H00JU2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00JU2_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         H00JU2_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         H00JU2_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         H00JU2_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         H00JU2_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         H00JU3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         lblTextblockcontratoservicosindicador_formato_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoServicosIndicador_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosindicadorfaixaswc__default(),
            new Object[][] {
                new Object[] {
               H00JU2_A1345ContratoServicosIndicador_CalculoSob, H00JU2_n1345ContratoServicosIndicador_CalculoSob, H00JU2_A2052ContratoServicosIndicador_Formato, H00JU2_A1269ContratoServicosIndicador_Codigo, H00JU2_A1303ContratoServicosIndicadorFaixa_Reduz, H00JU2_A1302ContratoServicosIndicadorFaixa_Ate, H00JU2_A1301ContratoServicosIndicadorFaixa_Desde, H00JU2_A1300ContratoServicosIndicadorFaixa_Numero, H00JU2_A1299ContratoServicosIndicadorFaixa_Codigo
               }
               , new Object[] {
               H00JU3_AGRID_nRecordCount
               }
            }
         );
         AV39Pgmname = "ContratoServicosIndicadorFaixasWC";
         /* GeneXus formulas. */
         AV39Pgmname = "ContratoServicosIndicadorFaixasWC";
         context.Gx_err = 0;
         edtavLabel_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_17 ;
      private short nGXsfl_17_idx=1 ;
      private short AV13OrderedBy ;
      private short AV18TFContratoServicosIndicadorFaixa_Numero ;
      private short AV19TFContratoServicosIndicadorFaixa_Numero_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A2052ContratoServicosIndicador_Formato ;
      private short nGXsfl_17_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosIndicadorFaixa_Numero_Titleformat ;
      private short edtContratoServicosIndicadorFaixa_Desde_Titleformat ;
      private short edtContratoServicosIndicadorFaixa_Ate_Titleformat ;
      private short edtContratoServicosIndicadorFaixa_Reduz_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoServicosIndicador_Codigo ;
      private int wcpOAV7ContratoServicosIndicador_Codigo ;
      private int subGrid_Rows ;
      private int edtavLabel_Enabled ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int edtContratoServicosIndicador_Codigo_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_numero_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_numero_to_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_desde_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_desde_to_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_ate_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_ate_to_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_reduz_Visible ;
      private int edtavTfcontratoservicosindicadorfaixa_reduz_to_Visible ;
      private int edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Visible ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV34PageToGo ;
      private int AV40GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV35GridCurrentPage ;
      private long AV36GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV22TFContratoServicosIndicadorFaixa_Desde ;
      private decimal AV23TFContratoServicosIndicadorFaixa_Desde_To ;
      private decimal AV26TFContratoServicosIndicadorFaixa_Ate ;
      private decimal AV27TFContratoServicosIndicadorFaixa_Ate_To ;
      private decimal AV30TFContratoServicosIndicadorFaixa_Reduz ;
      private decimal AV31TFContratoServicosIndicadorFaixa_Reduz_To ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Activeeventkey ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_get ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_get ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Activeeventkey ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_get ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_get ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Activeeventkey ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_get ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_get ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Activeeventkey ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_get ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_17_idx="0001" ;
      private String AV39Pgmname ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String GXKey ;
      private String edtavLabel_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Caption ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Tooltip ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Cls ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Filteredtext_set ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Filteredtextto_set ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Sortedstatus ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Filtertype ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Sortasc ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Sortdsc ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Cleanfilter ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Rangefilterfrom ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Rangefilterto ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Searchbuttontext ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Caption ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Tooltip ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Cls ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Filteredtext_set ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Filteredtextto_set ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Sortedstatus ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Filtertype ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Sortasc ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Sortdsc ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Cleanfilter ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Rangefilterfrom ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Rangefilterto ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Searchbuttontext ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Caption ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Tooltip ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Cls ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Filteredtext_set ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Filteredtextto_set ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Sortedstatus ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Filtertype ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Sortasc ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Sortdsc ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Cleanfilter ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Rangefilterfrom ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Rangefilterto ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Searchbuttontext ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Caption ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Tooltip ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Cls ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Filteredtext_set ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Filteredtextto_set ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Dropdownoptionstype ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Sortedstatus ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Filtertype ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Sortasc ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Sortdsc ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Cleanfilter ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterfrom ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Rangefilterto ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContratoServicosIndicador_Codigo_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_numero_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_numero_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_numero_to_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_numero_to_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_desde_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_desde_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_desde_to_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_desde_to_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_ate_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_ate_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_ate_to_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_ate_to_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_reduz_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_reduz_Jsonclick ;
      private String edtavTfcontratoservicosindicadorfaixa_reduz_to_Internalname ;
      private String edtavTfcontratoservicosindicadorfaixa_reduz_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratoservicosindicadorfaixa_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicadorfaixa_desdetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicadorfaixa_atetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosindicadorfaixa_reduztitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContratoServicosIndicadorFaixa_Codigo_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Numero_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Desde_Internalname ;
      private String AV16Label ;
      private String edtContratoServicosIndicadorFaixa_Ate_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Internalname ;
      private String scmdbuf ;
      private String cmbContratoServicosIndicador_Formato_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosindicadorfaixa_numero_Internalname ;
      private String Ddo_contratoservicosindicadorfaixa_desde_Internalname ;
      private String Ddo_contratoservicosindicadorfaixa_ate_Internalname ;
      private String Ddo_contratoservicosindicadorfaixa_reduz_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Numero_Title ;
      private String edtContratoServicosIndicadorFaixa_Desde_Title ;
      private String edtContratoServicosIndicadorFaixa_Ate_Title ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Title ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String lblTextblockcontratoservicosindicador_formato_Internalname ;
      private String lblTextblockcontratoservicosindicador_formato_Jsonclick ;
      private String cmbContratoServicosIndicador_Formato_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV7ContratoServicosIndicador_Codigo ;
      private String sGXsfl_17_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosIndicadorFaixa_Codigo_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Numero_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Desde_Jsonclick ;
      private String edtavLabel_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Ate_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosindicadorfaixa_numero_Includesortasc ;
      private bool Ddo_contratoservicosindicadorfaixa_numero_Includesortdsc ;
      private bool Ddo_contratoservicosindicadorfaixa_numero_Includefilter ;
      private bool Ddo_contratoservicosindicadorfaixa_numero_Filterisrange ;
      private bool Ddo_contratoservicosindicadorfaixa_numero_Includedatalist ;
      private bool Ddo_contratoservicosindicadorfaixa_desde_Includesortasc ;
      private bool Ddo_contratoservicosindicadorfaixa_desde_Includesortdsc ;
      private bool Ddo_contratoservicosindicadorfaixa_desde_Includefilter ;
      private bool Ddo_contratoservicosindicadorfaixa_desde_Filterisrange ;
      private bool Ddo_contratoservicosindicadorfaixa_desde_Includedatalist ;
      private bool Ddo_contratoservicosindicadorfaixa_ate_Includesortasc ;
      private bool Ddo_contratoservicosindicadorfaixa_ate_Includesortdsc ;
      private bool Ddo_contratoservicosindicadorfaixa_ate_Includefilter ;
      private bool Ddo_contratoservicosindicadorfaixa_ate_Filterisrange ;
      private bool Ddo_contratoservicosindicadorfaixa_ate_Includedatalist ;
      private bool Ddo_contratoservicosindicadorfaixa_reduz_Includesortasc ;
      private bool Ddo_contratoservicosindicadorfaixa_reduz_Includesortdsc ;
      private bool Ddo_contratoservicosindicadorfaixa_reduz_Includefilter ;
      private bool Ddo_contratoservicosindicadorfaixa_reduz_Filterisrange ;
      private bool Ddo_contratoservicosindicadorfaixa_reduz_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV20ddo_ContratoServicosIndicadorFaixa_NumeroTitleControlIdToReplace ;
      private String AV24ddo_ContratoServicosIndicadorFaixa_DesdeTitleControlIdToReplace ;
      private String AV28ddo_ContratoServicosIndicadorFaixa_AteTitleControlIdToReplace ;
      private String AV32ddo_ContratoServicosIndicadorFaixa_ReduzTitleControlIdToReplace ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicosIndicador_Formato ;
      private IDataStoreProvider pr_default ;
      private String[] H00JU2_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] H00JU2_n1345ContratoServicosIndicador_CalculoSob ;
      private short[] H00JU2_A2052ContratoServicosIndicador_Formato ;
      private int[] H00JU2_A1269ContratoServicosIndicador_Codigo ;
      private decimal[] H00JU2_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal[] H00JU2_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] H00JU2_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] H00JU2_A1300ContratoServicosIndicadorFaixa_Numero ;
      private int[] H00JU2_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private long[] H00JU3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV17ContratoServicosIndicadorFaixa_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV21ContratoServicosIndicadorFaixa_DesdeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV25ContratoServicosIndicadorFaixa_AteTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV29ContratoServicosIndicadorFaixa_ReduzTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV33DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoservicosindicadorfaixaswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00JU2( IGxContext context ,
                                             short AV18TFContratoServicosIndicadorFaixa_Numero ,
                                             short AV19TFContratoServicosIndicadorFaixa_Numero_To ,
                                             decimal AV22TFContratoServicosIndicadorFaixa_Desde ,
                                             decimal AV23TFContratoServicosIndicadorFaixa_Desde_To ,
                                             decimal AV26TFContratoServicosIndicadorFaixa_Ate ,
                                             decimal AV27TFContratoServicosIndicadorFaixa_Ate_To ,
                                             decimal AV30TFContratoServicosIndicadorFaixa_Reduz ,
                                             decimal AV31TFContratoServicosIndicadorFaixa_Reduz_To ,
                                             short A1300ContratoServicosIndicadorFaixa_Numero ,
                                             decimal A1301ContratoServicosIndicadorFaixa_Desde ,
                                             decimal A1302ContratoServicosIndicadorFaixa_Ate ,
                                             decimal A1303ContratoServicosIndicadorFaixa_Reduz ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int AV7ContratoServicosIndicador_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [14] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[ContratoServicosIndicador_CalculoSob], T2.[ContratoServicosIndicador_Formato], T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicadorFaixa_Reduz], T1.[ContratoServicosIndicadorFaixa_Ate], T1.[ContratoServicosIndicadorFaixa_Desde], T1.[ContratoServicosIndicadorFaixa_Numero], T1.[ContratoServicosIndicadorFaixa_Codigo]";
         sFromString = " FROM ([ContratoServicosIndicadorFaixas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicosIndicador] T2 WITH (NOLOCK) ON T2.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoServicosIndicador_Codigo] = @AV7ContratoServicosIndicador_Codigo)";
         if ( ! (0==AV18TFContratoServicosIndicadorFaixa_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Numero] >= @AV18TFContratoServicosIndicadorFaixa_Numero)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV19TFContratoServicosIndicadorFaixa_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Numero] <= @AV19TFContratoServicosIndicadorFaixa_Numero_To)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosIndicadorFaixa_Desde) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Desde] >= @AV22TFContratoServicosIndicadorFaixa_Desde)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFContratoServicosIndicadorFaixa_Desde_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Desde] <= @AV23TFContratoServicosIndicadorFaixa_Desde_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosIndicadorFaixa_Ate) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Ate] >= @AV26TFContratoServicosIndicadorFaixa_Ate)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFContratoServicosIndicadorFaixa_Ate_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Ate] <= @AV27TFContratoServicosIndicadorFaixa_Ate_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFContratoServicosIndicadorFaixa_Reduz) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Reduz] >= @AV30TFContratoServicosIndicadorFaixa_Reduz)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV31TFContratoServicosIndicadorFaixa_Reduz_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Reduz] <= @AV31TFContratoServicosIndicadorFaixa_Reduz_To)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicadorFaixa_Numero]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo] DESC, T1.[ContratoServicosIndicadorFaixa_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicadorFaixa_Desde]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo] DESC, T1.[ContratoServicosIndicadorFaixa_Desde] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicadorFaixa_Ate]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo] DESC, T1.[ContratoServicosIndicadorFaixa_Ate] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicadorFaixa_Reduz]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo] DESC, T1.[ContratoServicosIndicadorFaixa_Reduz] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosIndicador_Codigo], T1.[ContratoServicosIndicadorFaixa_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00JU3( IGxContext context ,
                                             short AV18TFContratoServicosIndicadorFaixa_Numero ,
                                             short AV19TFContratoServicosIndicadorFaixa_Numero_To ,
                                             decimal AV22TFContratoServicosIndicadorFaixa_Desde ,
                                             decimal AV23TFContratoServicosIndicadorFaixa_Desde_To ,
                                             decimal AV26TFContratoServicosIndicadorFaixa_Ate ,
                                             decimal AV27TFContratoServicosIndicadorFaixa_Ate_To ,
                                             decimal AV30TFContratoServicosIndicadorFaixa_Reduz ,
                                             decimal AV31TFContratoServicosIndicadorFaixa_Reduz_To ,
                                             short A1300ContratoServicosIndicadorFaixa_Numero ,
                                             decimal A1301ContratoServicosIndicadorFaixa_Desde ,
                                             decimal A1302ContratoServicosIndicadorFaixa_Ate ,
                                             decimal A1303ContratoServicosIndicadorFaixa_Reduz ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1269ContratoServicosIndicador_Codigo ,
                                             int AV7ContratoServicosIndicador_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [9] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoServicosIndicadorFaixas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicosIndicador] T2 WITH (NOLOCK) ON T2.[ContratoServicosIndicador_Codigo] = T1.[ContratoServicosIndicador_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosIndicador_Codigo] = @AV7ContratoServicosIndicador_Codigo)";
         if ( ! (0==AV18TFContratoServicosIndicadorFaixa_Numero) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Numero] >= @AV18TFContratoServicosIndicadorFaixa_Numero)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV19TFContratoServicosIndicadorFaixa_Numero_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Numero] <= @AV19TFContratoServicosIndicadorFaixa_Numero_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFContratoServicosIndicadorFaixa_Desde) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Desde] >= @AV22TFContratoServicosIndicadorFaixa_Desde)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFContratoServicosIndicadorFaixa_Desde_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Desde] <= @AV23TFContratoServicosIndicadorFaixa_Desde_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoServicosIndicadorFaixa_Ate) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Ate] >= @AV26TFContratoServicosIndicadorFaixa_Ate)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFContratoServicosIndicadorFaixa_Ate_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Ate] <= @AV27TFContratoServicosIndicadorFaixa_Ate_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFContratoServicosIndicadorFaixa_Reduz) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Reduz] >= @AV30TFContratoServicosIndicadorFaixa_Reduz)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV31TFContratoServicosIndicadorFaixa_Reduz_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosIndicadorFaixa_Reduz] <= @AV31TFContratoServicosIndicadorFaixa_Reduz_To)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00JU2(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (short)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
               case 1 :
                     return conditional_H00JU3(context, (short)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (short)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JU2 ;
          prmH00JU2 = new Object[] {
          new Object[] {"@AV7ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratoServicosIndicadorFaixa_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFContratoServicosIndicadorFaixa_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV22TFContratoServicosIndicadorFaixa_Desde",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV23TFContratoServicosIndicadorFaixa_Desde_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV26TFContratoServicosIndicadorFaixa_Ate",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV27TFContratoServicosIndicadorFaixa_Ate_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV30TFContratoServicosIndicadorFaixa_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV31TFContratoServicosIndicadorFaixa_Reduz_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00JU3 ;
          prmH00JU3 = new Object[] {
          new Object[] {"@AV7ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratoServicosIndicadorFaixa_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV19TFContratoServicosIndicadorFaixa_Numero_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV22TFContratoServicosIndicadorFaixa_Desde",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV23TFContratoServicosIndicadorFaixa_Desde_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV26TFContratoServicosIndicadorFaixa_Ate",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV27TFContratoServicosIndicadorFaixa_Ate_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV30TFContratoServicosIndicadorFaixa_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV31TFContratoServicosIndicadorFaixa_Reduz_To",SqlDbType.Decimal,6,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JU2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JU2,11,0,true,false )
             ,new CursorDef("H00JU3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JU3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((short[]) buf[7])[0] = rslt.getShort(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                return;
       }
    }

 }

}
