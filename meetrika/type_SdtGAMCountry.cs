/*
               File: type_SdtGAMCountry
        Description: GAMCountry
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:11.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMCountry : GxUserType, IGxExternalObject
   {
      public SdtGAMCountry( )
      {
         initialize();
      }

      public SdtGAMCountry( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMCountry_externalReference == null )
         {
            GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
         }
         returntostring = "";
         returntostring = (String)(GAMCountry_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Id
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference.Id ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            GAMCountry_externalReference.Id = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference.Name ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            GAMCountry_externalReference.Name = value;
         }

      }

      public String gxTpr_Iso_3
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference.ISO_3 ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            GAMCountry_externalReference.ISO_3 = value;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference.DateCreated ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            GAMCountry_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference.UserCreated ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            GAMCountry_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference.DateUpdated ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            GAMCountry_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference.UserUpdated ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            GAMCountry_externalReference.UserUpdated = value;
         }

      }

      public IGxCollection gxTpr_Languages
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMCountryLanguages", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMCountryLanguages> externalParm0 ;
            externalParm0 = GAMCountry_externalReference.Languages;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMCountryLanguages>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMCountryLanguages> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMCountryLanguages>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMCountryLanguages>), intValue.ExternalInstance);
            GAMCountry_externalReference.Languages = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMCountry_externalReference == null )
            {
               GAMCountry_externalReference = new Artech.Security.GAMCountry(context);
            }
            return GAMCountry_externalReference ;
         }

         set {
            GAMCountry_externalReference = (Artech.Security.GAMCountry)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMCountry GAMCountry_externalReference=null ;
   }

}
