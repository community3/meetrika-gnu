/*
               File: GIT_GeraListaDll
        Description: Gera a Lista de DLL a serem enviadas para o GIT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:49.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class git_geralistadll : GXProcedure
   {
      public git_geralistadll( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public git_geralistadll( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         git_geralistadll objgit_geralistadll;
         objgit_geralistadll = new git_geralistadll();
         objgit_geralistadll.context.SetSubmitInitialConfig(context);
         objgit_geralistadll.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgit_geralistadll);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((git_geralistadll)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new geralog(context ).execute( ref  AV13Pgmname) ;
         AV9Directory.Source = "D:\\ProjetosGenexus\\GxEv3Up14_Meetrika\\Hom\\Web\\bin\\";
         AV15GXV2 = 1;
         AV14GXV1 = AV9Directory.GetFiles("");
         while ( AV15GXV2 <= AV14GXV1.ItemCount )
         {
            AV8File = AV14GXV1.Item(AV15GXV2);
            if ( StringUtil.StrCmp(AV8File.GetName(), "client.exe.config") != 0 )
            {
               GXt_char1 = "git add /d/ProjetosGenexus/GxEv3Up14_Meetrika/Hom/web/bin/" + AV8File.GetName() + " -f";
               new geralog(context ).execute( ref  GXt_char1) ;
            }
            AV15GXV2 = (int)(AV15GXV2+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13Pgmname = "";
         AV9Directory = new GxDirectory(context.GetPhysicalPath());
         AV14GXV1 = new GxFileCollection();
         AV8File = new GxFile(context.GetPhysicalPath());
         GXt_char1 = "";
         AV13Pgmname = "GIT_GeraListaDll";
         /* GeneXus formulas. */
         AV13Pgmname = "GIT_GeraListaDll";
         context.Gx_err = 0;
      }

      private int AV15GXV2 ;
      private String AV13Pgmname ;
      private String GXt_char1 ;
      private GxFile AV8File ;
      private GxDirectory AV9Directory ;
      private GxFileCollection AV14GXV1 ;
   }

}
