/*
               File: PRC_UpdContagem_Deflator
        Description: Update Contagem_Deflator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:29.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updcontagem_deflator : GXProcedure
   {
      public prc_updcontagem_deflator( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updcontagem_deflator( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public int executeUdp( )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         return A456ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo )
      {
         prc_updcontagem_deflator objprc_updcontagem_deflator;
         objprc_updcontagem_deflator = new prc_updcontagem_deflator();
         objprc_updcontagem_deflator.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_updcontagem_deflator.context.SetSubmitInitialConfig(context);
         objprc_updcontagem_deflator.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updcontagem_deflator);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updcontagem_deflator)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00602 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00602_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00602_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = P00602_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00602_n1603ContagemResultado_CntCod[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00602_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00602_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A512ContagemResultado_ValorPF = P00602_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00602_n512ContagemResultado_ValorPF[0];
            A1616ContagemResultado_CntVlrUndCnt = P00602_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00602_n1616ContagemResultado_CntVlrUndCnt[0];
            A1596ContagemResultado_CntSrvPrc = P00602_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00602_n1596ContagemResultado_CntSrvPrc[0];
            A1603ContagemResultado_CntCod = P00602_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00602_n1603ContagemResultado_CntCod[0];
            A1597ContagemResultado_CntSrvVlrUndCnt = P00602_A1597ContagemResultado_CntSrvVlrUndCnt[0];
            n1597ContagemResultado_CntSrvVlrUndCnt = P00602_n1597ContagemResultado_CntSrvVlrUndCnt[0];
            A1596ContagemResultado_CntSrvPrc = P00602_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00602_n1596ContagemResultado_CntSrvPrc[0];
            A1616ContagemResultado_CntVlrUndCnt = P00602_A1616ContagemResultado_CntVlrUndCnt[0];
            n1616ContagemResultado_CntVlrUndCnt = P00602_n1616ContagemResultado_CntVlrUndCnt[0];
            if ( ( A1597ContagemResultado_CntSrvVlrUndCnt > Convert.ToDecimal( 0 )) )
            {
               A512ContagemResultado_ValorPF = A1597ContagemResultado_CntSrvVlrUndCnt;
               n512ContagemResultado_ValorPF = false;
            }
            else
            {
               A512ContagemResultado_ValorPF = A1616ContagemResultado_CntVlrUndCnt;
               n512ContagemResultado_ValorPF = false;
            }
            if ( ( A1596ContagemResultado_CntSrvPrc > Convert.ToDecimal( 0 )) )
            {
               AV12Servico_Percentual = A1596ContagemResultado_CntSrvPrc;
            }
            else
            {
               AV12Servico_Percentual = (decimal)(1);
            }
            /* Using cursor P00603 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A800ContagemResultado_Deflator = P00603_A800ContagemResultado_Deflator[0];
               n800ContagemResultado_Deflator = P00603_n800ContagemResultado_Deflator[0];
               A473ContagemResultado_DataCnt = P00603_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P00603_A511ContagemResultado_HoraCnt[0];
               A800ContagemResultado_Deflator = AV12Servico_Percentual;
               n800ContagemResultado_Deflator = false;
               BatchSize = 10;
               pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp00604");
               /* Using cursor P00604 */
               pr_default.addRecord(2, new Object[] {n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
               {
                  Executebatchp00604( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(1);
            }
            if ( pr_default.getBatchSize(2) > 0 )
            {
               Executebatchp00604( ) ;
            }
            pr_default.close(1);
            /* Using cursor P00605 */
            pr_default.execute(3, new Object[] {n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, A456ContagemResultado_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void Executebatchp00604( )
      {
         /* Using cursor P00604 */
         pr_default.executeBatch(2);
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00602_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00602_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00602_A1603ContagemResultado_CntCod = new int[1] ;
         P00602_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00602_A456ContagemResultado_Codigo = new int[1] ;
         P00602_A1597ContagemResultado_CntSrvVlrUndCnt = new decimal[1] ;
         P00602_n1597ContagemResultado_CntSrvVlrUndCnt = new bool[] {false} ;
         P00602_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00602_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00602_A1616ContagemResultado_CntVlrUndCnt = new decimal[1] ;
         P00602_n1616ContagemResultado_CntVlrUndCnt = new bool[] {false} ;
         P00602_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P00602_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P00603_A456ContagemResultado_Codigo = new int[1] ;
         P00603_A800ContagemResultado_Deflator = new decimal[1] ;
         P00603_n800ContagemResultado_Deflator = new bool[] {false} ;
         P00603_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00603_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         P00604_A800ContagemResultado_Deflator = new decimal[1] ;
         P00604_n800ContagemResultado_Deflator = new bool[] {false} ;
         P00604_A456ContagemResultado_Codigo = new int[1] ;
         P00604_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00604_A511ContagemResultado_HoraCnt = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updcontagem_deflator__default(),
            new Object[][] {
                new Object[] {
               P00602_A1553ContagemResultado_CntSrvCod, P00602_n1553ContagemResultado_CntSrvCod, P00602_A1603ContagemResultado_CntCod, P00602_n1603ContagemResultado_CntCod, P00602_A456ContagemResultado_Codigo, P00602_A1597ContagemResultado_CntSrvVlrUndCnt, P00602_n1597ContagemResultado_CntSrvVlrUndCnt, P00602_A512ContagemResultado_ValorPF, P00602_n512ContagemResultado_ValorPF, P00602_A1616ContagemResultado_CntVlrUndCnt,
               P00602_n1616ContagemResultado_CntVlrUndCnt, P00602_A1596ContagemResultado_CntSrvPrc, P00602_n1596ContagemResultado_CntSrvPrc
               }
               , new Object[] {
               P00603_A456ContagemResultado_Codigo, P00603_A800ContagemResultado_Deflator, P00603_n800ContagemResultado_Deflator, P00603_A473ContagemResultado_DataCnt, P00603_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int BatchSize ;
      private decimal A1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1616ContagemResultado_CntVlrUndCnt ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal AV12Servico_Percentual ;
      private decimal A800ContagemResultado_Deflator ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1616ContagemResultado_CntVlrUndCnt ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool n800ContagemResultado_Deflator ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00602_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00602_n1553ContagemResultado_CntSrvCod ;
      private int[] P00602_A1603ContagemResultado_CntCod ;
      private bool[] P00602_n1603ContagemResultado_CntCod ;
      private int[] P00602_A456ContagemResultado_Codigo ;
      private decimal[] P00602_A1597ContagemResultado_CntSrvVlrUndCnt ;
      private bool[] P00602_n1597ContagemResultado_CntSrvVlrUndCnt ;
      private decimal[] P00602_A512ContagemResultado_ValorPF ;
      private bool[] P00602_n512ContagemResultado_ValorPF ;
      private decimal[] P00602_A1616ContagemResultado_CntVlrUndCnt ;
      private bool[] P00602_n1616ContagemResultado_CntVlrUndCnt ;
      private decimal[] P00602_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P00602_n1596ContagemResultado_CntSrvPrc ;
      private int[] P00603_A456ContagemResultado_Codigo ;
      private decimal[] P00603_A800ContagemResultado_Deflator ;
      private bool[] P00603_n800ContagemResultado_Deflator ;
      private DateTime[] P00603_A473ContagemResultado_DataCnt ;
      private String[] P00603_A511ContagemResultado_HoraCnt ;
      private decimal[] P00604_A800ContagemResultado_Deflator ;
      private bool[] P00604_n800ContagemResultado_Deflator ;
      private int[] P00604_A456ContagemResultado_Codigo ;
      private DateTime[] P00604_A473ContagemResultado_DataCnt ;
      private String[] P00604_A511ContagemResultado_HoraCnt ;
   }

   public class prc_updcontagem_deflator__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00602 ;
          prmP00602 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00603 ;
          prmP00603 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00604 ;
          prmP00604 = new Object[] {
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP00605 ;
          prmP00605 = new Object[] {
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00602", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T2.[Servico_VlrUnidadeContratada] AS ContagemResultado_CntSrvVlrUndCnt, T1.[ContagemResultado_ValorPF], T3.[Contrato_ValorUnidadeContratacao] AS ContagemResultado_CntVlrUndCnt, T2.[Servico_Percentual] AS ContagemResultado_CntSrvPrc FROM (([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00602,1,0,true,true )
             ,new CursorDef("P00603", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Deflator], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00603,1,0,true,false )
             ,new CursorDef("P00604", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Deflator]=@ContagemResultado_Deflator  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00604)
             ,new CursorDef("P00605", "UPDATE [ContagemResultado] SET [ContagemResultado_ValorPF]=@ContagemResultado_ValorPF  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00605)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (DateTime)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
