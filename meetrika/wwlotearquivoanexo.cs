/*
               File: WWLoteArquivoAnexo
        Description:  Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:47:59.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwlotearquivoanexo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwlotearquivoanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwlotearquivoanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_91 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_91_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_91_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17LoteArquivoAnexo_NomeArq1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LoteArquivoAnexo_NomeArq1", AV17LoteArquivoAnexo_NomeArq1);
               AV18TipoDocumento_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TipoDocumento_Nome1", AV18TipoDocumento_Nome1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22LoteArquivoAnexo_NomeArq2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LoteArquivoAnexo_NomeArq2", AV22LoteArquivoAnexo_NomeArq2);
               AV23TipoDocumento_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TipoDocumento_Nome2", AV23TipoDocumento_Nome2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27LoteArquivoAnexo_NomeArq3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LoteArquivoAnexo_NomeArq3", AV27LoteArquivoAnexo_NomeArq3);
               AV28TipoDocumento_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TipoDocumento_Nome3", AV28TipoDocumento_Nome3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV37TFLoteArquivoAnexo_NomeArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLoteArquivoAnexo_NomeArq", AV37TFLoteArquivoAnexo_NomeArq);
               AV38TFLoteArquivoAnexo_NomeArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLoteArquivoAnexo_NomeArq_Sel", AV38TFLoteArquivoAnexo_NomeArq_Sel);
               AV41TFTipoDocumento_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoDocumento_Nome", AV41TFTipoDocumento_Nome);
               AV42TFTipoDocumento_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFTipoDocumento_Nome_Sel", AV42TFTipoDocumento_Nome_Sel);
               AV45TFLoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               AV46TFLoteArquivoAnexo_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace", AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace);
               AV43ddo_TipoDocumento_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_TipoDocumento_NomeTitleControlIdToReplace", AV43ddo_TipoDocumento_NomeTitleControlIdToReplace);
               AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace", AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace);
               AV78Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A836LoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n645TipoDocumento_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEW2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEW2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117475993");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwlotearquivoanexo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTEARQUIVOANEXO_NOMEARQ1", StringUtil.RTrim( AV17LoteArquivoAnexo_NomeArq1));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME1", StringUtil.RTrim( AV18TipoDocumento_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTEARQUIVOANEXO_NOMEARQ2", StringUtil.RTrim( AV22LoteArquivoAnexo_NomeArq2));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME2", StringUtil.RTrim( AV23TipoDocumento_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTEARQUIVOANEXO_NOMEARQ3", StringUtil.RTrim( AV27LoteArquivoAnexo_NomeArq3));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME3", StringUtil.RTrim( AV28TipoDocumento_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ", StringUtil.RTrim( AV37TFLoteArquivoAnexo_NomeArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ_SEL", StringUtil.RTrim( AV38TFLoteArquivoAnexo_NomeArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODOCUMENTO_NOME", StringUtil.RTrim( AV41TFTipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODOCUMENTO_NOME_SEL", StringUtil.RTrim( AV42TFTipoDocumento_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_DATA", context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_DATA_TO", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_91", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_91), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV50DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA", AV36LoteArquivoAnexo_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA", AV36LoteArquivoAnexo_NomeArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODOCUMENTO_NOMETITLEFILTERDATA", AV40TipoDocumento_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODOCUMENTO_NOMETITLEFILTERDATA", AV40TipoDocumento_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTEARQUIVOANEXO_DATATITLEFILTERDATA", AV44LoteArquivoAnexo_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTEARQUIVOANEXO_DATATITLEFILTERDATA", AV44LoteArquivoAnexo_DataTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV78Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Caption", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cls", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortedstatus", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortasc", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortdsc", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Caption", StringUtil.RTrim( Ddo_tipodocumento_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Tooltip", StringUtil.RTrim( Ddo_tipodocumento_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Cls", StringUtil.RTrim( Ddo_tipodocumento_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tipodocumento_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodocumento_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodocumento_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodocumento_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filtertype", StringUtil.RTrim( Ddo_tipodocumento_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalisttype", StringUtil.RTrim( Ddo_tipodocumento_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalistproc", StringUtil.RTrim( Ddo_tipodocumento_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tipodocumento_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortasc", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortdsc", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Loadingdata", StringUtil.RTrim( Ddo_tipodocumento_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tipodocumento_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tipodocumento_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tipodocumento_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Caption", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Tooltip", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Cls", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filtertype", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Sortasc", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Sortdsc", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tipodocumento_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tipodocumento_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodocumento_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEW2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEW2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwlotearquivoanexo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWLoteArquivoAnexo" ;
      }

      public override String GetPgmdesc( )
      {
         return " Arquivos Anexos" ;
      }

      protected void WBEW0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_EW2( true) ;
         }
         else
         {
            wb_table1_2_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(103, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(104, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_nomearq_Internalname, StringUtil.RTrim( AV37TFLoteArquivoAnexo_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV37TFLoteArquivoAnexo_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_nomearq_sel_Internalname, StringUtil.RTrim( AV38TFLoteArquivoAnexo_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV38TFLoteArquivoAnexo_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodocumento_nome_Internalname, StringUtil.RTrim( AV41TFTipoDocumento_Nome), StringUtil.RTrim( context.localUtil.Format( AV41TFTipoDocumento_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodocumento_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodocumento_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodocumento_nome_sel_Internalname, StringUtil.RTrim( AV42TFTipoDocumento_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV42TFTipoDocumento_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodocumento_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodocumento_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflotearquivoanexo_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_data_Internalname, context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV45TFLoteArquivoAnexo_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_data_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavTflotearquivoanexo_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflotearquivoanexo_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflotearquivoanexo_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_data_to_Internalname, context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV46TFLoteArquivoAnexo_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavTflotearquivoanexo_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflotearquivoanexo_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_lotearquivoanexo_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lotearquivoanexo_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lotearquivoanexo_dataauxdate_Internalname, context.localUtil.Format(AV47DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"), context.localUtil.Format( AV47DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lotearquivoanexo_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lotearquivoanexo_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_91_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lotearquivoanexo_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lotearquivoanexo_dataauxdateto_Internalname, context.localUtil.Format(AV48DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV48DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lotearquivoanexo_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lotearquivoanexo_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTEARQUIVOANEXO_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODOCUMENTO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTEARQUIVOANEXO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_91_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWLoteArquivoAnexo.htm");
         }
         wbLoad = true;
      }

      protected void STARTEW2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Arquivos Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEW0( ) ;
      }

      protected void WSEW2( )
      {
         STARTEW2( ) ;
         EVTEW2( ) ;
      }

      protected void EVTEW2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11EW2 */
                              E11EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOTEARQUIVOANEXO_NOMEARQ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12EW2 */
                              E12EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODOCUMENTO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13EW2 */
                              E13EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOTEARQUIVOANEXO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14EW2 */
                              E14EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15EW2 */
                              E15EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16EW2 */
                              E16EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17EW2 */
                              E17EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18EW2 */
                              E18EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19EW2 */
                              E19EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20EW2 */
                              E20EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21EW2 */
                              E21EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22EW2 */
                              E22EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23EW2 */
                              E23EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24EW2 */
                              E24EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25EW2 */
                              E25EW2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_91_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
                              SubsflControlProps_912( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV76Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV77Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              A841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( edtLoteArquivoAnexo_LoteCod_Internalname), ",", "."));
                              A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
                              A838LoteArquivoAnexo_Arquivo = cgiGet( edtLoteArquivoAnexo_Arquivo_Internalname);
                              n838LoteArquivoAnexo_Arquivo = false;
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
                              A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtLoteArquivoAnexo_Data_Internalname), 0);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26EW2 */
                                    E26EW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27EW2 */
                                    E27EW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28EW2 */
                                    E28EW2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lotearquivoanexo_nomearq1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ1"), AV17LoteArquivoAnexo_NomeArq1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodocumento_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME1"), AV18TipoDocumento_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lotearquivoanexo_nomearq2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ2"), AV22LoteArquivoAnexo_NomeArq2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodocumento_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME2"), AV23TipoDocumento_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Lotearquivoanexo_nomearq3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ3"), AV27LoteArquivoAnexo_NomeArq3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tipodocumento_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME3"), AV28TipoDocumento_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflotearquivoanexo_nomearq Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ"), AV37TFLoteArquivoAnexo_NomeArq) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflotearquivoanexo_nomearq_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ_SEL"), AV38TFLoteArquivoAnexo_NomeArq_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodocumento_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME"), AV41TFTipoDocumento_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftipodocumento_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME_SEL"), AV42TFTipoDocumento_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflotearquivoanexo_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA"), 0) != AV45TFLoteArquivoAnexo_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tflotearquivoanexo_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA_TO"), 0) != AV46TFLoteArquivoAnexo_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEW2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEW2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("LOTEARQUIVOANEXO_NOMEARQ", "do Arquivo", 0);
            cmbavDynamicfiltersselector1.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("LOTEARQUIVOANEXO_NOMEARQ", "do Arquivo", 0);
            cmbavDynamicfiltersselector2.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("LOTEARQUIVOANEXO_NOMEARQ", "do Arquivo", 0);
            cmbavDynamicfiltersselector3.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_912( ) ;
         while ( nGXsfl_91_idx <= nRC_GXsfl_91 )
         {
            sendrow_912( ) ;
            nGXsfl_91_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_91_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17LoteArquivoAnexo_NomeArq1 ,
                                       String AV18TipoDocumento_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22LoteArquivoAnexo_NomeArq2 ,
                                       String AV23TipoDocumento_Nome2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27LoteArquivoAnexo_NomeArq3 ,
                                       String AV28TipoDocumento_Nome3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV37TFLoteArquivoAnexo_NomeArq ,
                                       String AV38TFLoteArquivoAnexo_NomeArq_Sel ,
                                       String AV41TFTipoDocumento_Nome ,
                                       String AV42TFTipoDocumento_Nome_Sel ,
                                       DateTime AV45TFLoteArquivoAnexo_Data ,
                                       DateTime AV46TFLoteArquivoAnexo_Data_To ,
                                       String AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace ,
                                       String AV43ddo_TipoDocumento_NomeTitleControlIdToReplace ,
                                       String AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace ,
                                       String AV78Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A841LoteArquivoAnexo_LoteCod ,
                                       DateTime A836LoteArquivoAnexo_Data ,
                                       int A645TipoDocumento_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFEW2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_DATA", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEW2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV78Pgmname = "WWLoteArquivoAnexo";
         context.Gx_err = 0;
      }

      protected void RFEW2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 91;
         /* Execute user event: E27EW2 */
         E27EW2 ();
         nGXsfl_91_idx = 1;
         sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
         SubsflControlProps_912( ) ;
         nGXsfl_91_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_912( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                                 AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                                 AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                                 AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                                 AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                                 AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                                 AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                                 AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                                 AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                                 AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                                 AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                                 AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                                 AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                                 AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                                 AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                                 AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                                 AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                                 AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                                 AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                                 AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                                 A839LoteArquivoAnexo_NomeArq ,
                                                 A646TipoDocumento_Nome ,
                                                 A836LoteArquivoAnexo_Data ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
            lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
            lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
            lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
            lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
            lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
            lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
            lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
            lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
            lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
            lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
            lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
            lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = StringUtil.PadR( StringUtil.RTrim( AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq), 50, "%");
            lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = StringUtil.PadR( StringUtil.RTrim( AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome), 50, "%");
            /* Using cursor H00EW2 */
            pr_default.execute(0, new Object[] {lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq, AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel, lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome, AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel, AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data, AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_91_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A839LoteArquivoAnexo_NomeArq = H00EW2_A839LoteArquivoAnexo_NomeArq[0];
               n839LoteArquivoAnexo_NomeArq = H00EW2_n839LoteArquivoAnexo_NomeArq[0];
               edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
               A840LoteArquivoAnexo_TipoArq = H00EW2_A840LoteArquivoAnexo_TipoArq[0];
               n840LoteArquivoAnexo_TipoArq = H00EW2_n840LoteArquivoAnexo_TipoArq[0];
               edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               A645TipoDocumento_Codigo = H00EW2_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = H00EW2_n645TipoDocumento_Codigo[0];
               A836LoteArquivoAnexo_Data = H00EW2_A836LoteArquivoAnexo_Data[0];
               A646TipoDocumento_Nome = H00EW2_A646TipoDocumento_Nome[0];
               A841LoteArquivoAnexo_LoteCod = H00EW2_A841LoteArquivoAnexo_LoteCod[0];
               A838LoteArquivoAnexo_Arquivo = H00EW2_A838LoteArquivoAnexo_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
               n838LoteArquivoAnexo_Arquivo = H00EW2_n838LoteArquivoAnexo_Arquivo[0];
               A646TipoDocumento_Nome = H00EW2_A646TipoDocumento_Nome[0];
               /* Execute user event: E28EW2 */
               E28EW2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 91;
            WBEW0( ) ;
         }
         nGXsfl_91_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV17LoteArquivoAnexo_NomeArq1;
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV18TipoDocumento_Nome1;
         AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV22LoteArquivoAnexo_NomeArq2;
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV23TipoDocumento_Nome2;
         AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV27LoteArquivoAnexo_NomeArq3;
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV28TipoDocumento_Nome3;
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV37TFLoteArquivoAnexo_NomeArq;
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV38TFLoteArquivoAnexo_NomeArq_Sel;
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV41TFTipoDocumento_Nome;
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV42TFTipoDocumento_Nome_Sel;
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV45TFLoteArquivoAnexo_Data;
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV46TFLoteArquivoAnexo_Data_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                              AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                              AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                              AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                              AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                              AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                              AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                              AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                              AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                              AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                              AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                              AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                              AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                              AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                              AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                              AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                              AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                              AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                              AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                              AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A836LoteArquivoAnexo_Data ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
         lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = StringUtil.PadR( StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1), 50, "%");
         lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
         lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1), 50, "%");
         lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
         lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = StringUtil.PadR( StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2), 50, "%");
         lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
         lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = StringUtil.PadR( StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2), 50, "%");
         lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
         lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = StringUtil.PadR( StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3), 50, "%");
         lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
         lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = StringUtil.PadR( StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3), 50, "%");
         lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = StringUtil.PadR( StringUtil.RTrim( AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq), 50, "%");
         lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = StringUtil.PadR( StringUtil.RTrim( AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome), 50, "%");
         /* Using cursor H00EW3 */
         pr_default.execute(1, new Object[] {lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1, lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1, lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2, lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2, lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3, lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3, lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq, AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel, lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome, AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel, AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data, AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to});
         GRID_nRecordCount = H00EW3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV17LoteArquivoAnexo_NomeArq1;
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV18TipoDocumento_Nome1;
         AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV22LoteArquivoAnexo_NomeArq2;
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV23TipoDocumento_Nome2;
         AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV27LoteArquivoAnexo_NomeArq3;
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV28TipoDocumento_Nome3;
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV37TFLoteArquivoAnexo_NomeArq;
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV38TFLoteArquivoAnexo_NomeArq_Sel;
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV41TFTipoDocumento_Nome;
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV42TFTipoDocumento_Nome_Sel;
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV45TFLoteArquivoAnexo_Data;
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV46TFLoteArquivoAnexo_Data_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV17LoteArquivoAnexo_NomeArq1;
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV18TipoDocumento_Nome1;
         AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV22LoteArquivoAnexo_NomeArq2;
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV23TipoDocumento_Nome2;
         AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV27LoteArquivoAnexo_NomeArq3;
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV28TipoDocumento_Nome3;
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV37TFLoteArquivoAnexo_NomeArq;
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV38TFLoteArquivoAnexo_NomeArq_Sel;
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV41TFTipoDocumento_Nome;
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV42TFTipoDocumento_Nome_Sel;
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV45TFLoteArquivoAnexo_Data;
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV46TFLoteArquivoAnexo_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV17LoteArquivoAnexo_NomeArq1;
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV18TipoDocumento_Nome1;
         AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV22LoteArquivoAnexo_NomeArq2;
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV23TipoDocumento_Nome2;
         AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV27LoteArquivoAnexo_NomeArq3;
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV28TipoDocumento_Nome3;
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV37TFLoteArquivoAnexo_NomeArq;
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV38TFLoteArquivoAnexo_NomeArq_Sel;
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV41TFTipoDocumento_Nome;
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV42TFTipoDocumento_Nome_Sel;
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV45TFLoteArquivoAnexo_Data;
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV46TFLoteArquivoAnexo_Data_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV17LoteArquivoAnexo_NomeArq1;
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV18TipoDocumento_Nome1;
         AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV22LoteArquivoAnexo_NomeArq2;
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV23TipoDocumento_Nome2;
         AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV27LoteArquivoAnexo_NomeArq3;
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV28TipoDocumento_Nome3;
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV37TFLoteArquivoAnexo_NomeArq;
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV38TFLoteArquivoAnexo_NomeArq_Sel;
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV41TFTipoDocumento_Nome;
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV42TFTipoDocumento_Nome_Sel;
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV45TFLoteArquivoAnexo_Data;
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV46TFLoteArquivoAnexo_Data_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV17LoteArquivoAnexo_NomeArq1;
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV18TipoDocumento_Nome1;
         AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV22LoteArquivoAnexo_NomeArq2;
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV23TipoDocumento_Nome2;
         AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV27LoteArquivoAnexo_NomeArq3;
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV28TipoDocumento_Nome3;
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV37TFLoteArquivoAnexo_NomeArq;
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV38TFLoteArquivoAnexo_NomeArq_Sel;
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV41TFTipoDocumento_Nome;
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV42TFTipoDocumento_Nome_Sel;
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV45TFLoteArquivoAnexo_Data;
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV46TFLoteArquivoAnexo_Data_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPEW0( )
      {
         /* Before Start, stand alone formulas. */
         AV78Pgmname = "WWLoteArquivoAnexo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26EW2 */
         E26EW2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV50DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA"), AV36LoteArquivoAnexo_NomeArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODOCUMENTO_NOMETITLEFILTERDATA"), AV40TipoDocumento_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTEARQUIVOANEXO_DATATITLEFILTERDATA"), AV44LoteArquivoAnexo_DataTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17LoteArquivoAnexo_NomeArq1 = cgiGet( edtavLotearquivoanexo_nomearq1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LoteArquivoAnexo_NomeArq1", AV17LoteArquivoAnexo_NomeArq1);
            AV18TipoDocumento_Nome1 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TipoDocumento_Nome1", AV18TipoDocumento_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22LoteArquivoAnexo_NomeArq2 = cgiGet( edtavLotearquivoanexo_nomearq2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LoteArquivoAnexo_NomeArq2", AV22LoteArquivoAnexo_NomeArq2);
            AV23TipoDocumento_Nome2 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TipoDocumento_Nome2", AV23TipoDocumento_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27LoteArquivoAnexo_NomeArq3 = cgiGet( edtavLotearquivoanexo_nomearq3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LoteArquivoAnexo_NomeArq3", AV27LoteArquivoAnexo_NomeArq3);
            AV28TipoDocumento_Nome3 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TipoDocumento_Nome3", AV28TipoDocumento_Nome3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV37TFLoteArquivoAnexo_NomeArq = cgiGet( edtavTflotearquivoanexo_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLoteArquivoAnexo_NomeArq", AV37TFLoteArquivoAnexo_NomeArq);
            AV38TFLoteArquivoAnexo_NomeArq_Sel = cgiGet( edtavTflotearquivoanexo_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLoteArquivoAnexo_NomeArq_Sel", AV38TFLoteArquivoAnexo_NomeArq_Sel);
            AV41TFTipoDocumento_Nome = StringUtil.Upper( cgiGet( edtavTftipodocumento_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoDocumento_Nome", AV41TFTipoDocumento_Nome);
            AV42TFTipoDocumento_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftipodocumento_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFTipoDocumento_Nome_Sel", AV42TFTipoDocumento_Nome_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflotearquivoanexo_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLote Arquivo Anexo_Data"}), 1, "vTFLOTEARQUIVOANEXO_DATA");
               GX_FocusControl = edtavTflotearquivoanexo_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFLoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV45TFLoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtavTflotearquivoanexo_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflotearquivoanexo_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLote Arquivo Anexo_Data_To"}), 1, "vTFLOTEARQUIVOANEXO_DATA_TO");
               GX_FocusControl = edtavTflotearquivoanexo_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFLoteArquivoAnexo_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV46TFLoteArquivoAnexo_Data_To = context.localUtil.CToT( cgiGet( edtavTflotearquivoanexo_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lotearquivoanexo_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote Arquivo Anexo_Data Aux Date"}), 1, "vDDO_LOTEARQUIVOANEXO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_lotearquivoanexo_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47DDO_LoteArquivoAnexo_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_LoteArquivoAnexo_DataAuxDate", context.localUtil.Format(AV47DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV47DDO_LoteArquivoAnexo_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lotearquivoanexo_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_LoteArquivoAnexo_DataAuxDate", context.localUtil.Format(AV47DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lotearquivoanexo_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote Arquivo Anexo_Data Aux Date To"}), 1, "vDDO_LOTEARQUIVOANEXO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_lotearquivoanexo_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48DDO_LoteArquivoAnexo_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_LoteArquivoAnexo_DataAuxDateTo", context.localUtil.Format(AV48DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV48DDO_LoteArquivoAnexo_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lotearquivoanexo_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_LoteArquivoAnexo_DataAuxDateTo", context.localUtil.Format(AV48DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"));
            }
            AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace", AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace);
            AV43ddo_TipoDocumento_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_TipoDocumento_NomeTitleControlIdToReplace", AV43ddo_TipoDocumento_NomeTitleControlIdToReplace);
            AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = cgiGet( edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace", AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_91 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_91"), ",", "."));
            AV52GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV53GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_lotearquivoanexo_nomearq_Caption = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Caption");
            Ddo_lotearquivoanexo_nomearq_Tooltip = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Tooltip");
            Ddo_lotearquivoanexo_nomearq_Cls = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cls");
            Ddo_lotearquivoanexo_nomearq_Filteredtext_set = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_set");
            Ddo_lotearquivoanexo_nomearq_Selectedvalue_set = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_set");
            Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Dropdownoptionstype");
            Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_lotearquivoanexo_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortasc"));
            Ddo_lotearquivoanexo_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortdsc"));
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortedstatus");
            Ddo_lotearquivoanexo_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includefilter"));
            Ddo_lotearquivoanexo_nomearq_Filtertype = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filtertype");
            Ddo_lotearquivoanexo_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filterisrange"));
            Ddo_lotearquivoanexo_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includedatalist"));
            Ddo_lotearquivoanexo_nomearq_Datalisttype = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalisttype");
            Ddo_lotearquivoanexo_nomearq_Datalistproc = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistproc");
            Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lotearquivoanexo_nomearq_Sortasc = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortasc");
            Ddo_lotearquivoanexo_nomearq_Sortdsc = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortdsc");
            Ddo_lotearquivoanexo_nomearq_Loadingdata = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Loadingdata");
            Ddo_lotearquivoanexo_nomearq_Cleanfilter = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cleanfilter");
            Ddo_lotearquivoanexo_nomearq_Noresultsfound = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Noresultsfound");
            Ddo_lotearquivoanexo_nomearq_Searchbuttontext = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Searchbuttontext");
            Ddo_tipodocumento_nome_Caption = cgiGet( "DDO_TIPODOCUMENTO_NOME_Caption");
            Ddo_tipodocumento_nome_Tooltip = cgiGet( "DDO_TIPODOCUMENTO_NOME_Tooltip");
            Ddo_tipodocumento_nome_Cls = cgiGet( "DDO_TIPODOCUMENTO_NOME_Cls");
            Ddo_tipodocumento_nome_Filteredtext_set = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filteredtext_set");
            Ddo_tipodocumento_nome_Selectedvalue_set = cgiGet( "DDO_TIPODOCUMENTO_NOME_Selectedvalue_set");
            Ddo_tipodocumento_nome_Dropdownoptionstype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Dropdownoptionstype");
            Ddo_tipodocumento_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODOCUMENTO_NOME_Titlecontrolidtoreplace");
            Ddo_tipodocumento_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includesortasc"));
            Ddo_tipodocumento_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includesortdsc"));
            Ddo_tipodocumento_nome_Sortedstatus = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortedstatus");
            Ddo_tipodocumento_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includefilter"));
            Ddo_tipodocumento_nome_Filtertype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filtertype");
            Ddo_tipodocumento_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Filterisrange"));
            Ddo_tipodocumento_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includedatalist"));
            Ddo_tipodocumento_nome_Datalisttype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalisttype");
            Ddo_tipodocumento_nome_Datalistproc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalistproc");
            Ddo_tipodocumento_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tipodocumento_nome_Sortasc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortasc");
            Ddo_tipodocumento_nome_Sortdsc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortdsc");
            Ddo_tipodocumento_nome_Loadingdata = cgiGet( "DDO_TIPODOCUMENTO_NOME_Loadingdata");
            Ddo_tipodocumento_nome_Cleanfilter = cgiGet( "DDO_TIPODOCUMENTO_NOME_Cleanfilter");
            Ddo_tipodocumento_nome_Noresultsfound = cgiGet( "DDO_TIPODOCUMENTO_NOME_Noresultsfound");
            Ddo_tipodocumento_nome_Searchbuttontext = cgiGet( "DDO_TIPODOCUMENTO_NOME_Searchbuttontext");
            Ddo_lotearquivoanexo_data_Caption = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Caption");
            Ddo_lotearquivoanexo_data_Tooltip = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Tooltip");
            Ddo_lotearquivoanexo_data_Cls = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Cls");
            Ddo_lotearquivoanexo_data_Filteredtext_set = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_set");
            Ddo_lotearquivoanexo_data_Filteredtextto_set = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_set");
            Ddo_lotearquivoanexo_data_Dropdownoptionstype = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Dropdownoptionstype");
            Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Titlecontrolidtoreplace");
            Ddo_lotearquivoanexo_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includesortasc"));
            Ddo_lotearquivoanexo_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includesortdsc"));
            Ddo_lotearquivoanexo_data_Sortedstatus = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Sortedstatus");
            Ddo_lotearquivoanexo_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includefilter"));
            Ddo_lotearquivoanexo_data_Filtertype = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filtertype");
            Ddo_lotearquivoanexo_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filterisrange"));
            Ddo_lotearquivoanexo_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includedatalist"));
            Ddo_lotearquivoanexo_data_Sortasc = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Sortasc");
            Ddo_lotearquivoanexo_data_Sortdsc = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Sortdsc");
            Ddo_lotearquivoanexo_data_Cleanfilter = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Cleanfilter");
            Ddo_lotearquivoanexo_data_Rangefilterfrom = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterfrom");
            Ddo_lotearquivoanexo_data_Rangefilterto = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterto");
            Ddo_lotearquivoanexo_data_Searchbuttontext = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_lotearquivoanexo_nomearq_Activeeventkey = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Activeeventkey");
            Ddo_lotearquivoanexo_nomearq_Filteredtext_get = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_get");
            Ddo_lotearquivoanexo_nomearq_Selectedvalue_get = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_get");
            Ddo_tipodocumento_nome_Activeeventkey = cgiGet( "DDO_TIPODOCUMENTO_NOME_Activeeventkey");
            Ddo_tipodocumento_nome_Filteredtext_get = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filteredtext_get");
            Ddo_tipodocumento_nome_Selectedvalue_get = cgiGet( "DDO_TIPODOCUMENTO_NOME_Selectedvalue_get");
            Ddo_lotearquivoanexo_data_Activeeventkey = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Activeeventkey");
            Ddo_lotearquivoanexo_data_Filteredtext_get = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_get");
            Ddo_lotearquivoanexo_data_Filteredtextto_get = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ1"), AV17LoteArquivoAnexo_NomeArq1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME1"), AV18TipoDocumento_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ2"), AV22LoteArquivoAnexo_NomeArq2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME2"), AV23TipoDocumento_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ3"), AV27LoteArquivoAnexo_NomeArq3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME3"), AV28TipoDocumento_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ"), AV37TFLoteArquivoAnexo_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ_SEL"), AV38TFLoteArquivoAnexo_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME"), AV41TFTipoDocumento_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME_SEL"), AV42TFTipoDocumento_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA"), 0) != AV45TFLoteArquivoAnexo_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA_TO"), 0) != AV46TFLoteArquivoAnexo_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26EW2 */
         E26EW2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26EW2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTflotearquivoanexo_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_nomearq_Visible), 5, 0)));
         edtavTflotearquivoanexo_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_nomearq_sel_Visible), 5, 0)));
         edtavTftipodocumento_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodocumento_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodocumento_nome_Visible), 5, 0)));
         edtavTftipodocumento_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodocumento_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodocumento_nome_sel_Visible), 5, 0)));
         edtavTflotearquivoanexo_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_data_Visible), 5, 0)));
         edtavTflotearquivoanexo_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_data_to_Visible), 5, 0)));
         Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_LoteArquivoAnexo_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "TitleControlIdToReplace", Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace);
         AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace", AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace);
         edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipodocumento_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoDocumento_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "TitleControlIdToReplace", Ddo_tipodocumento_nome_Titlecontrolidtoreplace);
         AV43ddo_TipoDocumento_NomeTitleControlIdToReplace = Ddo_tipodocumento_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_TipoDocumento_NomeTitleControlIdToReplace", AV43ddo_TipoDocumento_NomeTitleControlIdToReplace);
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace = subGrid_Internalname+"_LoteArquivoAnexo_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "TitleControlIdToReplace", Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace);
         AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace", AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace);
         edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Arquivos Anexos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "do Arquivo", 0);
         cmbavOrderedby.addItem("2", "Tipo de Documento", 0);
         cmbavOrderedby.addItem("3", "Hora Upload", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV50DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV50DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27EW2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV36LoteArquivoAnexo_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40TipoDocumento_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44LoteArquivoAnexo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLoteArquivoAnexo_NomeArq_Titleformat = 2;
         edtLoteArquivoAnexo_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome do Arquivo", AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_NomeArq_Internalname, "Title", edtLoteArquivoAnexo_NomeArq_Title);
         edtTipoDocumento_Nome_Titleformat = 2;
         edtTipoDocumento_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo de Documento", AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Title", edtTipoDocumento_Nome_Title);
         edtLoteArquivoAnexo_Data_Titleformat = 2;
         edtLoteArquivoAnexo_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Hora Upload", AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Data_Internalname, "Title", edtLoteArquivoAnexo_Data_Title);
         AV52GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52GridCurrentPage), 10, 0)));
         AV53GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53GridPageCount), 10, 0)));
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = AV17LoteArquivoAnexo_NomeArq1;
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = AV18TipoDocumento_Nome1;
         AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = AV22LoteArquivoAnexo_NomeArq2;
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = AV23TipoDocumento_Nome2;
         AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = AV27LoteArquivoAnexo_NomeArq3;
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = AV28TipoDocumento_Nome3;
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = AV37TFLoteArquivoAnexo_NomeArq;
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = AV38TFLoteArquivoAnexo_NomeArq_Sel;
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = AV41TFTipoDocumento_Nome;
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = AV42TFTipoDocumento_Nome_Sel;
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = AV45TFLoteArquivoAnexo_Data;
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = AV46TFLoteArquivoAnexo_Data_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36LoteArquivoAnexo_NomeArqTitleFilterData", AV36LoteArquivoAnexo_NomeArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40TipoDocumento_NomeTitleFilterData", AV40TipoDocumento_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44LoteArquivoAnexo_DataTitleFilterData", AV44LoteArquivoAnexo_DataTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11EW2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV51PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV51PageToGo) ;
         }
      }

      protected void E12EW2( )
      {
         /* Ddo_lotearquivoanexo_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_nomearq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_nomearq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFLoteArquivoAnexo_NomeArq = Ddo_lotearquivoanexo_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLoteArquivoAnexo_NomeArq", AV37TFLoteArquivoAnexo_NomeArq);
            AV38TFLoteArquivoAnexo_NomeArq_Sel = Ddo_lotearquivoanexo_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLoteArquivoAnexo_NomeArq_Sel", AV38TFLoteArquivoAnexo_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13EW2( )
      {
         /* Ddo_tipodocumento_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodocumento_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tipodocumento_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFTipoDocumento_Nome = Ddo_tipodocumento_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoDocumento_Nome", AV41TFTipoDocumento_Nome);
            AV42TFTipoDocumento_Nome_Sel = Ddo_tipodocumento_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFTipoDocumento_Nome_Sel", AV42TFTipoDocumento_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14EW2( )
      {
         /* Ddo_lotearquivoanexo_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lotearquivoanexo_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lotearquivoanexo_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFLoteArquivoAnexo_Data = context.localUtil.CToT( Ddo_lotearquivoanexo_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            AV46TFLoteArquivoAnexo_Data_To = context.localUtil.CToT( Ddo_lotearquivoanexo_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV46TFLoteArquivoAnexo_Data_To) )
            {
               AV46TFLoteArquivoAnexo_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV46TFLoteArquivoAnexo_Data_To)), (short)(DateTimeUtil.Month( AV46TFLoteArquivoAnexo_Data_To)), (short)(DateTimeUtil.Day( AV46TFLoteArquivoAnexo_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E28EW2( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV76Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(A836LoteArquivoAnexo_Data));
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV77Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(A836LoteArquivoAnexo_Data));
         edtLoteArquivoAnexo_NomeArq_Link = formatLink("viewlotearquivoanexo.aspx") + "?" + UrlEncode("" +A841LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(A836LoteArquivoAnexo_Data)) + "," + UrlEncode(StringUtil.RTrim(""));
         edtTipoDocumento_Nome_Link = formatLink("viewtipodocumento.aspx") + "?" + UrlEncode("" +A645TipoDocumento_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 91;
         }
         sendrow_912( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_91_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(91, GridRow);
         }
      }

      protected void E15EW2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21EW2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
      }

      protected void E16EW2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22EW2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23EW2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
      }

      protected void E17EW2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24EW2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E18EW2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17LoteArquivoAnexo_NomeArq1, AV18TipoDocumento_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22LoteArquivoAnexo_NomeArq2, AV23TipoDocumento_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27LoteArquivoAnexo_NomeArq3, AV28TipoDocumento_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV37TFLoteArquivoAnexo_NomeArq, AV38TFLoteArquivoAnexo_NomeArq_Sel, AV41TFTipoDocumento_Nome, AV42TFTipoDocumento_Nome_Sel, AV45TFLoteArquivoAnexo_Data, AV46TFLoteArquivoAnexo_Data_To, AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV43ddo_TipoDocumento_NomeTitleControlIdToReplace, AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, A645TipoDocumento_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25EW2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E19EW2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E20EW2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_lotearquivoanexo_nomearq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
         Ddo_tipodocumento_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
         Ddo_lotearquivoanexo_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tipodocumento_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_lotearquivoanexo_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavLotearquivoanexo_nomearq1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq1_Visible), 5, 0)));
         edtavTipodocumento_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            edtavLotearquivoanexo_nomearq1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavLotearquivoanexo_nomearq2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq2_Visible), 5, 0)));
         edtavTipodocumento_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            edtavLotearquivoanexo_nomearq2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavLotearquivoanexo_nomearq3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq3_Visible), 5, 0)));
         edtavTipodocumento_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            edtavLotearquivoanexo_nomearq3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22LoteArquivoAnexo_NomeArq2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LoteArquivoAnexo_NomeArq2", AV22LoteArquivoAnexo_NomeArq2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27LoteArquivoAnexo_NomeArq3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LoteArquivoAnexo_NomeArq3", AV27LoteArquivoAnexo_NomeArq3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37TFLoteArquivoAnexo_NomeArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLoteArquivoAnexo_NomeArq", AV37TFLoteArquivoAnexo_NomeArq);
         Ddo_lotearquivoanexo_nomearq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_nomearq_Filteredtext_set);
         AV38TFLoteArquivoAnexo_NomeArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLoteArquivoAnexo_NomeArq_Sel", AV38TFLoteArquivoAnexo_NomeArq_Sel);
         Ddo_lotearquivoanexo_nomearq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SelectedValue_set", Ddo_lotearquivoanexo_nomearq_Selectedvalue_set);
         AV41TFTipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoDocumento_Nome", AV41TFTipoDocumento_Nome);
         Ddo_tipodocumento_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "FilteredText_set", Ddo_tipodocumento_nome_Filteredtext_set);
         AV42TFTipoDocumento_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFTipoDocumento_Nome_Sel", AV42TFTipoDocumento_Nome_Sel);
         Ddo_tipodocumento_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SelectedValue_set", Ddo_tipodocumento_nome_Selectedvalue_set);
         AV45TFLoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_lotearquivoanexo_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_data_Filteredtext_set);
         AV46TFLoteArquivoAnexo_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_lotearquivoanexo_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "FilteredTextTo_set", Ddo_lotearquivoanexo_data_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17LoteArquivoAnexo_NomeArq1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LoteArquivoAnexo_NomeArq1", AV17LoteArquivoAnexo_NomeArq1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV78Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV78Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV78Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV79GXV1 = 1;
         while ( AV79GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV79GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               AV37TFLoteArquivoAnexo_NomeArq = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFLoteArquivoAnexo_NomeArq", AV37TFLoteArquivoAnexo_NomeArq);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFLoteArquivoAnexo_NomeArq)) )
               {
                  Ddo_lotearquivoanexo_nomearq_Filteredtext_set = AV37TFLoteArquivoAnexo_NomeArq;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_nomearq_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_NOMEARQ_SEL") == 0 )
            {
               AV38TFLoteArquivoAnexo_NomeArq_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLoteArquivoAnexo_NomeArq_Sel", AV38TFLoteArquivoAnexo_NomeArq_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFLoteArquivoAnexo_NomeArq_Sel)) )
               {
                  Ddo_lotearquivoanexo_nomearq_Selectedvalue_set = AV38TFLoteArquivoAnexo_NomeArq_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SelectedValue_set", Ddo_lotearquivoanexo_nomearq_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME") == 0 )
            {
               AV41TFTipoDocumento_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFTipoDocumento_Nome", AV41TFTipoDocumento_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFTipoDocumento_Nome)) )
               {
                  Ddo_tipodocumento_nome_Filteredtext_set = AV41TFTipoDocumento_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "FilteredText_set", Ddo_tipodocumento_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME_SEL") == 0 )
            {
               AV42TFTipoDocumento_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFTipoDocumento_Nome_Sel", AV42TFTipoDocumento_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFTipoDocumento_Nome_Sel)) )
               {
                  Ddo_tipodocumento_nome_Selectedvalue_set = AV42TFTipoDocumento_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SelectedValue_set", Ddo_tipodocumento_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFLOTEARQUIVOANEXO_DATA") == 0 )
            {
               AV45TFLoteArquivoAnexo_Data = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               AV46TFLoteArquivoAnexo_Data_To = context.localUtil.CToT( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV45TFLoteArquivoAnexo_Data) )
               {
                  AV47DDO_LoteArquivoAnexo_DataAuxDate = DateTimeUtil.ResetTime(AV45TFLoteArquivoAnexo_Data);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_LoteArquivoAnexo_DataAuxDate", context.localUtil.Format(AV47DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"));
                  Ddo_lotearquivoanexo_data_Filteredtext_set = context.localUtil.DToC( AV47DDO_LoteArquivoAnexo_DataAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV46TFLoteArquivoAnexo_Data_To) )
               {
                  AV48DDO_LoteArquivoAnexo_DataAuxDateTo = DateTimeUtil.ResetTime(AV46TFLoteArquivoAnexo_Data_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_LoteArquivoAnexo_DataAuxDateTo", context.localUtil.Format(AV48DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"));
                  Ddo_lotearquivoanexo_data_Filteredtextto_set = context.localUtil.DToC( AV48DDO_LoteArquivoAnexo_DataAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "FilteredTextTo_set", Ddo_lotearquivoanexo_data_Filteredtextto_set);
               }
            }
            AV79GXV1 = (int)(AV79GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17LoteArquivoAnexo_NomeArq1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LoteArquivoAnexo_NomeArq1", AV17LoteArquivoAnexo_NomeArq1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18TipoDocumento_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18TipoDocumento_Nome1", AV18TipoDocumento_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22LoteArquivoAnexo_NomeArq2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22LoteArquivoAnexo_NomeArq2", AV22LoteArquivoAnexo_NomeArq2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23TipoDocumento_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23TipoDocumento_Nome2", AV23TipoDocumento_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27LoteArquivoAnexo_NomeArq3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LoteArquivoAnexo_NomeArq3", AV27LoteArquivoAnexo_NomeArq3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV28TipoDocumento_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28TipoDocumento_Nome3", AV28TipoDocumento_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV78Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFLoteArquivoAnexo_NomeArq)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_NOMEARQ";
            AV11GridStateFilterValue.gxTpr_Value = AV37TFLoteArquivoAnexo_NomeArq;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_NOMEARQ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFLoteArquivoAnexo_NomeArq_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFTipoDocumento_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODOCUMENTO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFTipoDocumento_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFTipoDocumento_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTIPODOCUMENTO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFTipoDocumento_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV45TFLoteArquivoAnexo_Data) && (DateTime.MinValue==AV46TFLoteArquivoAnexo_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV45TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV46TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV78Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17LoteArquivoAnexo_NomeArq1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17LoteArquivoAnexo_NomeArq1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TipoDocumento_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18TipoDocumento_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22LoteArquivoAnexo_NomeArq2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22LoteArquivoAnexo_NomeArq2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TipoDocumento_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23TipoDocumento_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27LoteArquivoAnexo_NomeArq3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27LoteArquivoAnexo_NomeArq3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TipoDocumento_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28TipoDocumento_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV78Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "LoteArquivoAnexo";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_EW2( true) ;
         }
         else
         {
            wb_table2_8_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_85_EW2( true) ;
         }
         else
         {
            wb_table3_85_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table3_85_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EW2e( true) ;
         }
         else
         {
            wb_table1_2_EW2e( false) ;
         }
      }

      protected void wb_table3_85_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_88_EW2( true) ;
         }
         else
         {
            wb_table4_88_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table4_88_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_85_EW2e( true) ;
         }
         else
         {
            wb_table3_85_EW2e( false) ;
         }
      }

      protected void wb_table4_88_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"91\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Anexo_Lote Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoDocumento_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoDocumento_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoDocumento_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_NomeArq_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtLoteArquivoAnexo_NomeArq_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A646TipoDocumento_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoDocumento_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTipoDocumento_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A838LoteArquivoAnexo_Arquivo);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 91 )
         {
            wbEnd = 0;
            nRC_GXsfl_91 = (short)(nGXsfl_91_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_88_EW2e( true) ;
         }
         else
         {
            wb_table4_88_EW2e( false) ;
         }
      }

      protected void wb_table2_8_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLotearquivoanexotitle_Internalname, "Arquivos Anexos", "", "", lblLotearquivoanexotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_EW2( true) ;
         }
         else
         {
            wb_table5_13_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWLoteArquivoAnexo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_EW2( true) ;
         }
         else
         {
            wb_table6_23_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_EW2e( true) ;
         }
         else
         {
            wb_table2_8_EW2e( false) ;
         }
      }

      protected void wb_table6_23_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_EW2( true) ;
         }
         else
         {
            wb_table7_28_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_EW2e( true) ;
         }
         else
         {
            wb_table6_23_EW2e( false) ;
         }
      }

      protected void wb_table7_28_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWLoteArquivoAnexo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_EW2( true) ;
         }
         else
         {
            wb_table8_37_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWLoteArquivoAnexo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_55_EW2( true) ;
         }
         else
         {
            wb_table9_55_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table9_55_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WWLoteArquivoAnexo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_73_EW2( true) ;
         }
         else
         {
            wb_table10_73_EW2( false) ;
         }
         return  ;
      }

      protected void wb_table10_73_EW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_EW2e( true) ;
         }
         else
         {
            wb_table7_28_EW2e( false) ;
         }
      }

      protected void wb_table10_73_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_WWLoteArquivoAnexo.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_nomearq3_Internalname, StringUtil.RTrim( AV27LoteArquivoAnexo_NomeArq3), StringUtil.RTrim( context.localUtil.Format( AV27LoteArquivoAnexo_NomeArq3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_nomearq3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLotearquivoanexo_nomearq3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome3_Internalname, StringUtil.RTrim( AV28TipoDocumento_Nome3), StringUtil.RTrim( context.localUtil.Format( AV28TipoDocumento_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_73_EW2e( true) ;
         }
         else
         {
            wb_table10_73_EW2e( false) ;
         }
      }

      protected void wb_table9_55_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWLoteArquivoAnexo.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_nomearq2_Internalname, StringUtil.RTrim( AV22LoteArquivoAnexo_NomeArq2), StringUtil.RTrim( context.localUtil.Format( AV22LoteArquivoAnexo_NomeArq2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_nomearq2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLotearquivoanexo_nomearq2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome2_Internalname, StringUtil.RTrim( AV23TipoDocumento_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23TipoDocumento_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_55_EW2e( true) ;
         }
         else
         {
            wb_table9_55_EW2e( false) ;
         }
      }

      protected void wb_table8_37_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWLoteArquivoAnexo.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_nomearq1_Internalname, StringUtil.RTrim( AV17LoteArquivoAnexo_NomeArq1), StringUtil.RTrim( context.localUtil.Format( AV17LoteArquivoAnexo_NomeArq1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_nomearq1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLotearquivoanexo_nomearq1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome1_Internalname, StringUtil.RTrim( AV18TipoDocumento_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18TipoDocumento_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_EW2e( true) ;
         }
         else
         {
            wb_table8_37_EW2e( false) ;
         }
      }

      protected void wb_table5_13_EW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_EW2e( true) ;
         }
         else
         {
            wb_table5_13_EW2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEW2( ) ;
         WSEW2( ) ;
         WEEW2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311748622");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwlotearquivoanexo.js", "?2020311748622");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_912( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_91_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_91_idx;
         edtLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_91_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_91_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_91_idx;
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO_"+sGXsfl_91_idx;
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA_"+sGXsfl_91_idx;
      }

      protected void SubsflControlProps_fel_912( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_91_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_91_fel_idx;
         edtLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_91_fel_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_91_fel_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_91_fel_idx;
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO_"+sGXsfl_91_fel_idx;
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA_"+sGXsfl_91_fel_idx;
      }

      protected void sendrow_912( )
      {
         SubsflControlProps_912( ) ;
         WBEW0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_91_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_91_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_91_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV76Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV76Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV77Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_LoteCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_LoteCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_NomeArq_Internalname,StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtLoteArquivoAnexo_NomeArq_Link,(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Nome_Internalname,StringUtil.RTrim( A646TipoDocumento_Nome),StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTipoDocumento_Nome_Link,(String)"",(String)"",(String)"",(String)edtTipoDocumento_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            ClassString = "Image";
            StyleString = "";
            edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            edtLoteArquivoAnexo_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) )
            {
               gxblobfileaux.Source = A838LoteArquivoAnexo_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtLoteArquivoAnexo_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtLoteArquivoAnexo_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A838LoteArquivoAnexo_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n838LoteArquivoAnexo_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
                  edtLoteArquivoAnexo_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
            }
            GridRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Arquivo_Internalname,StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo),context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filetype)) ? A838LoteArquivoAnexo_Arquivo : edtLoteArquivoAnexo_Arquivo_Filetype)) : edtLoteArquivoAnexo_Arquivo_Contenttype),(bool)true,(String)"",(String)edtLoteArquivoAnexo_Arquivo_Parameters,(short)0,(short)0,(short)-1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtLoteArquivoAnexo_Arquivo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",(String)""+"",(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Data_Internalname,context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_LOTECOD"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DATA"+"_"+sGXsfl_91_idx, GetSecureSignedToken( sGXsfl_91_idx, context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_91_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_91_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         /* End function sendrow_912 */
      }

      protected void init_default_properties( )
      {
         lblLotearquivoanexotitle_Internalname = "LOTEARQUIVOANEXOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavLotearquivoanexo_nomearq1_Internalname = "vLOTEARQUIVOANEXO_NOMEARQ1";
         edtavTipodocumento_nome1_Internalname = "vTIPODOCUMENTO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavLotearquivoanexo_nomearq2_Internalname = "vLOTEARQUIVOANEXO_NOMEARQ2";
         edtavTipodocumento_nome2_Internalname = "vTIPODOCUMENTO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavLotearquivoanexo_nomearq3_Internalname = "vLOTEARQUIVOANEXO_NOMEARQ3";
         edtavTipodocumento_nome3_Internalname = "vTIPODOCUMENTO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD";
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ";
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME";
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO";
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTflotearquivoanexo_nomearq_Internalname = "vTFLOTEARQUIVOANEXO_NOMEARQ";
         edtavTflotearquivoanexo_nomearq_sel_Internalname = "vTFLOTEARQUIVOANEXO_NOMEARQ_SEL";
         edtavTftipodocumento_nome_Internalname = "vTFTIPODOCUMENTO_NOME";
         edtavTftipodocumento_nome_sel_Internalname = "vTFTIPODOCUMENTO_NOME_SEL";
         edtavTflotearquivoanexo_data_Internalname = "vTFLOTEARQUIVOANEXO_DATA";
         edtavTflotearquivoanexo_data_to_Internalname = "vTFLOTEARQUIVOANEXO_DATA_TO";
         edtavDdo_lotearquivoanexo_dataauxdate_Internalname = "vDDO_LOTEARQUIVOANEXO_DATAAUXDATE";
         edtavDdo_lotearquivoanexo_dataauxdateto_Internalname = "vDDO_LOTEARQUIVOANEXO_DATAAUXDATETO";
         divDdo_lotearquivoanexo_dataauxdates_Internalname = "DDO_LOTEARQUIVOANEXO_DATAAUXDATES";
         Ddo_lotearquivoanexo_nomearq_Internalname = "DDO_LOTEARQUIVOANEXO_NOMEARQ";
         edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname = "vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE";
         Ddo_tipodocumento_nome_Internalname = "DDO_TIPODOCUMENTO_NOME";
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname = "vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_lotearquivoanexo_data_Internalname = "DDO_LOTEARQUIVOANEXO_DATA";
         edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname = "vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtLoteArquivoAnexo_Data_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Parameters = "";
         edtLoteArquivoAnexo_Arquivo_Contenttype = "";
         edtTipoDocumento_Nome_Jsonclick = "";
         edtLoteArquivoAnexo_NomeArq_Jsonclick = "";
         edtLoteArquivoAnexo_LoteCod_Jsonclick = "";
         edtavTipodocumento_nome1_Jsonclick = "";
         edtavLotearquivoanexo_nomearq1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavTipodocumento_nome2_Jsonclick = "";
         edtavLotearquivoanexo_nomearq2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavTipodocumento_nome3_Jsonclick = "";
         edtavLotearquivoanexo_nomearq3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtTipoDocumento_Nome_Link = "";
         edtLoteArquivoAnexo_NomeArq_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtLoteArquivoAnexo_Data_Titleformat = 0;
         edtTipoDocumento_Nome_Titleformat = 0;
         edtLoteArquivoAnexo_NomeArq_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavTipodocumento_nome3_Visible = 1;
         edtavLotearquivoanexo_nomearq3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavTipodocumento_nome2_Visible = 1;
         edtavLotearquivoanexo_nomearq2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavTipodocumento_nome1_Visible = 1;
         edtavLotearquivoanexo_nomearq1_Visible = 1;
         edtLoteArquivoAnexo_Data_Title = "Hora Upload";
         edtTipoDocumento_Nome_Title = "Tipo de Documento";
         edtLoteArquivoAnexo_NomeArq_Title = "Nome do Arquivo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         edtLoteArquivoAnexo_Arquivo_Filetype = "";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lotearquivoanexo_dataauxdateto_Jsonclick = "";
         edtavDdo_lotearquivoanexo_dataauxdate_Jsonclick = "";
         edtavTflotearquivoanexo_data_to_Jsonclick = "";
         edtavTflotearquivoanexo_data_to_Visible = 1;
         edtavTflotearquivoanexo_data_Jsonclick = "";
         edtavTflotearquivoanexo_data_Visible = 1;
         edtavTftipodocumento_nome_sel_Jsonclick = "";
         edtavTftipodocumento_nome_sel_Visible = 1;
         edtavTftipodocumento_nome_Jsonclick = "";
         edtavTftipodocumento_nome_Visible = 1;
         edtavTflotearquivoanexo_nomearq_sel_Jsonclick = "";
         edtavTflotearquivoanexo_nomearq_sel_Visible = 1;
         edtavTflotearquivoanexo_nomearq_Jsonclick = "";
         edtavTflotearquivoanexo_nomearq_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_lotearquivoanexo_data_Searchbuttontext = "Pesquisar";
         Ddo_lotearquivoanexo_data_Rangefilterto = "At�";
         Ddo_lotearquivoanexo_data_Rangefilterfrom = "Desde";
         Ddo_lotearquivoanexo_data_Cleanfilter = "Limpar pesquisa";
         Ddo_lotearquivoanexo_data_Sortdsc = "Ordenar de Z � A";
         Ddo_lotearquivoanexo_data_Sortasc = "Ordenar de A � Z";
         Ddo_lotearquivoanexo_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lotearquivoanexo_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Filtertype = "Date";
         Ddo_lotearquivoanexo_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace = "";
         Ddo_lotearquivoanexo_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lotearquivoanexo_data_Cls = "ColumnSettings";
         Ddo_lotearquivoanexo_data_Tooltip = "Op��es";
         Ddo_lotearquivoanexo_data_Caption = "";
         Ddo_tipodocumento_nome_Searchbuttontext = "Pesquisar";
         Ddo_tipodocumento_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tipodocumento_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodocumento_nome_Loadingdata = "Carregando dados...";
         Ddo_tipodocumento_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodocumento_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tipodocumento_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tipodocumento_nome_Datalistproc = "GetWWLoteArquivoAnexoFilterData";
         Ddo_tipodocumento_nome_Datalisttype = "Dynamic";
         Ddo_tipodocumento_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tipodocumento_nome_Filtertype = "Character";
         Ddo_tipodocumento_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Titlecontrolidtoreplace = "";
         Ddo_tipodocumento_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodocumento_nome_Cls = "ColumnSettings";
         Ddo_tipodocumento_nome_Tooltip = "Op��es";
         Ddo_tipodocumento_nome_Caption = "";
         Ddo_lotearquivoanexo_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_lotearquivoanexo_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lotearquivoanexo_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_lotearquivoanexo_nomearq_Loadingdata = "Carregando dados...";
         Ddo_lotearquivoanexo_nomearq_Sortdsc = "Ordenar de Z � A";
         Ddo_lotearquivoanexo_nomearq_Sortasc = "Ordenar de A � Z";
         Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_lotearquivoanexo_nomearq_Datalistproc = "GetWWLoteArquivoAnexoFilterData";
         Ddo_lotearquivoanexo_nomearq_Datalisttype = "Dynamic";
         Ddo_lotearquivoanexo_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_lotearquivoanexo_nomearq_Filtertype = "Character";
         Ddo_lotearquivoanexo_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace = "";
         Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lotearquivoanexo_nomearq_Cls = "ColumnSettings";
         Ddo_lotearquivoanexo_nomearq_Tooltip = "Op��es";
         Ddo_lotearquivoanexo_nomearq_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Arquivos Anexos";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36LoteArquivoAnexo_NomeArqTitleFilterData',fld:'vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV40TipoDocumento_NomeTitleFilterData',fld:'vTIPODOCUMENTO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV44LoteArquivoAnexo_DataTitleFilterData',fld:'vLOTEARQUIVOANEXO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtLoteArquivoAnexo_NomeArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_NomeArq_Title',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Title'},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'edtLoteArquivoAnexo_Data_Titleformat',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Data_Title',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Title'},{av:'AV52GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV53GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_LOTEARQUIVOANEXO_NOMEARQ.ONOPTIONCLICKED","{handler:'E12EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lotearquivoanexo_nomearq_Activeeventkey',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_lotearquivoanexo_nomearq_Filteredtext_get',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_lotearquivoanexo_nomearq_Selectedvalue_get',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPODOCUMENTO_NOME.ONOPTIONCLICKED","{handler:'E13EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_tipodocumento_nome_Activeeventkey',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'ActiveEventKey'},{av:'Ddo_tipodocumento_nome_Filteredtext_get',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'FilteredText_get'},{av:'Ddo_tipodocumento_nome_Selectedvalue_get',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTEARQUIVOANEXO_DATA.ONOPTIONCLICKED","{handler:'E14EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lotearquivoanexo_data_Activeeventkey',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'ActiveEventKey'},{av:'Ddo_lotearquivoanexo_data_Filteredtext_get',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredText_get'},{av:'Ddo_lotearquivoanexo_data_Filteredtextto_get',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28EW2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtLoteArquivoAnexo_NomeArq_Link',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Link'},{av:'edtTipoDocumento_Nome_Link',ctrl:'TIPODOCUMENTO_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22EW2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24EW2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25EW2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19EW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV37TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_nomearq_Filteredtext_set',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'FilteredText_set'},{av:'AV38TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_nomearq_Selectedvalue_set',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SelectedValue_set'},{av:'AV41TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'Ddo_tipodocumento_nome_Filteredtext_set',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'FilteredText_set'},{av:'AV42TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipodocumento_nome_Selectedvalue_set',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SelectedValue_set'},{av:'AV45TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lotearquivoanexo_data_Filteredtext_set',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredText_set'},{av:'AV46TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lotearquivoanexo_data_Filteredtextto_set',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV28TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20EW2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_lotearquivoanexo_nomearq_Activeeventkey = "";
         Ddo_lotearquivoanexo_nomearq_Filteredtext_get = "";
         Ddo_lotearquivoanexo_nomearq_Selectedvalue_get = "";
         Ddo_tipodocumento_nome_Activeeventkey = "";
         Ddo_tipodocumento_nome_Filteredtext_get = "";
         Ddo_tipodocumento_nome_Selectedvalue_get = "";
         Ddo_lotearquivoanexo_data_Activeeventkey = "";
         Ddo_lotearquivoanexo_data_Filteredtext_get = "";
         Ddo_lotearquivoanexo_data_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17LoteArquivoAnexo_NomeArq1 = "";
         AV18TipoDocumento_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22LoteArquivoAnexo_NomeArq2 = "";
         AV23TipoDocumento_Nome2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27LoteArquivoAnexo_NomeArq3 = "";
         AV28TipoDocumento_Nome3 = "";
         AV37TFLoteArquivoAnexo_NomeArq = "";
         AV38TFLoteArquivoAnexo_NomeArq_Sel = "";
         AV41TFTipoDocumento_Nome = "";
         AV42TFTipoDocumento_Nome_Sel = "";
         AV45TFLoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV46TFLoteArquivoAnexo_Data_To = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = "";
         AV43ddo_TipoDocumento_NomeTitleControlIdToReplace = "";
         AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = "";
         AV78Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV50DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36LoteArquivoAnexo_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40TipoDocumento_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44LoteArquivoAnexo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_lotearquivoanexo_nomearq_Filteredtext_set = "";
         Ddo_lotearquivoanexo_nomearq_Selectedvalue_set = "";
         Ddo_lotearquivoanexo_nomearq_Sortedstatus = "";
         Ddo_tipodocumento_nome_Filteredtext_set = "";
         Ddo_tipodocumento_nome_Selectedvalue_set = "";
         Ddo_tipodocumento_nome_Sortedstatus = "";
         Ddo_lotearquivoanexo_data_Filteredtext_set = "";
         Ddo_lotearquivoanexo_data_Filteredtextto_set = "";
         Ddo_lotearquivoanexo_data_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV47DDO_LoteArquivoAnexo_DataAuxDate = DateTime.MinValue;
         AV48DDO_LoteArquivoAnexo_DataAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV76Update_GXI = "";
         AV32Delete = "";
         AV77Delete_GXI = "";
         A646TipoDocumento_Nome = "";
         A838LoteArquivoAnexo_Arquivo = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = "";
         lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = "";
         lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = "";
         lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = "";
         lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = "";
         lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = "";
         lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = "";
         lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = "";
         AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 = "";
         AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 = "";
         AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 = "";
         AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 = "";
         AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 = "";
         AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 = "";
         AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 = "";
         AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 = "";
         AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 = "";
         AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel = "";
         AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq = "";
         AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel = "";
         AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome = "";
         AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data = (DateTime)(DateTime.MinValue);
         AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to = (DateTime)(DateTime.MinValue);
         A839LoteArquivoAnexo_NomeArq = "";
         H00EW2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         H00EW2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         H00EW2_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         H00EW2_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         H00EW2_A645TipoDocumento_Codigo = new int[1] ;
         H00EW2_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00EW2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00EW2_A646TipoDocumento_Nome = new String[] {""} ;
         H00EW2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         H00EW2_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         H00EW2_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         edtLoteArquivoAnexo_Arquivo_Filename = "";
         A840LoteArquivoAnexo_TipoArq = "";
         H00EW3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblLotearquivoanexotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwlotearquivoanexo__default(),
            new Object[][] {
                new Object[] {
               H00EW2_A839LoteArquivoAnexo_NomeArq, H00EW2_n839LoteArquivoAnexo_NomeArq, H00EW2_A840LoteArquivoAnexo_TipoArq, H00EW2_n840LoteArquivoAnexo_TipoArq, H00EW2_A645TipoDocumento_Codigo, H00EW2_n645TipoDocumento_Codigo, H00EW2_A836LoteArquivoAnexo_Data, H00EW2_A646TipoDocumento_Nome, H00EW2_A841LoteArquivoAnexo_LoteCod, H00EW2_A838LoteArquivoAnexo_Arquivo,
               H00EW2_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               H00EW3_AGRID_nRecordCount
               }
            }
         );
         AV78Pgmname = "WWLoteArquivoAnexo";
         /* GeneXus formulas. */
         AV78Pgmname = "WWLoteArquivoAnexo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_91 ;
      private short nGXsfl_91_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_91_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ;
      private short AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ;
      private short AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ;
      private short edtLoteArquivoAnexo_NomeArq_Titleformat ;
      private short edtTipoDocumento_Nome_Titleformat ;
      private short edtLoteArquivoAnexo_Data_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int A645TipoDocumento_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters ;
      private int Ddo_tipodocumento_nome_Datalistupdateminimumcharacters ;
      private int edtavTflotearquivoanexo_nomearq_Visible ;
      private int edtavTflotearquivoanexo_nomearq_sel_Visible ;
      private int edtavTftipodocumento_nome_Visible ;
      private int edtavTftipodocumento_nome_sel_Visible ;
      private int edtavTflotearquivoanexo_data_Visible ;
      private int edtavTflotearquivoanexo_data_to_Visible ;
      private int edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV51PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavLotearquivoanexo_nomearq1_Visible ;
      private int edtavTipodocumento_nome1_Visible ;
      private int edtavLotearquivoanexo_nomearq2_Visible ;
      private int edtavTipodocumento_nome2_Visible ;
      private int edtavLotearquivoanexo_nomearq3_Visible ;
      private int edtavTipodocumento_nome3_Visible ;
      private int AV79GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV52GridCurrentPage ;
      private long AV53GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_lotearquivoanexo_nomearq_Activeeventkey ;
      private String Ddo_lotearquivoanexo_nomearq_Filteredtext_get ;
      private String Ddo_lotearquivoanexo_nomearq_Selectedvalue_get ;
      private String Ddo_tipodocumento_nome_Activeeventkey ;
      private String Ddo_tipodocumento_nome_Filteredtext_get ;
      private String Ddo_tipodocumento_nome_Selectedvalue_get ;
      private String Ddo_lotearquivoanexo_data_Activeeventkey ;
      private String Ddo_lotearquivoanexo_data_Filteredtext_get ;
      private String Ddo_lotearquivoanexo_data_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_91_idx="0001" ;
      private String AV17LoteArquivoAnexo_NomeArq1 ;
      private String AV18TipoDocumento_Nome1 ;
      private String AV22LoteArquivoAnexo_NomeArq2 ;
      private String AV23TipoDocumento_Nome2 ;
      private String AV27LoteArquivoAnexo_NomeArq3 ;
      private String AV28TipoDocumento_Nome3 ;
      private String AV37TFLoteArquivoAnexo_NomeArq ;
      private String AV38TFLoteArquivoAnexo_NomeArq_Sel ;
      private String AV41TFTipoDocumento_Nome ;
      private String AV42TFTipoDocumento_Nome_Sel ;
      private String AV78Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_lotearquivoanexo_nomearq_Caption ;
      private String Ddo_lotearquivoanexo_nomearq_Tooltip ;
      private String Ddo_lotearquivoanexo_nomearq_Cls ;
      private String Ddo_lotearquivoanexo_nomearq_Filteredtext_set ;
      private String Ddo_lotearquivoanexo_nomearq_Selectedvalue_set ;
      private String Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype ;
      private String Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_lotearquivoanexo_nomearq_Sortedstatus ;
      private String Ddo_lotearquivoanexo_nomearq_Filtertype ;
      private String Ddo_lotearquivoanexo_nomearq_Datalisttype ;
      private String Ddo_lotearquivoanexo_nomearq_Datalistproc ;
      private String Ddo_lotearquivoanexo_nomearq_Sortasc ;
      private String Ddo_lotearquivoanexo_nomearq_Sortdsc ;
      private String Ddo_lotearquivoanexo_nomearq_Loadingdata ;
      private String Ddo_lotearquivoanexo_nomearq_Cleanfilter ;
      private String Ddo_lotearquivoanexo_nomearq_Noresultsfound ;
      private String Ddo_lotearquivoanexo_nomearq_Searchbuttontext ;
      private String Ddo_tipodocumento_nome_Caption ;
      private String Ddo_tipodocumento_nome_Tooltip ;
      private String Ddo_tipodocumento_nome_Cls ;
      private String Ddo_tipodocumento_nome_Filteredtext_set ;
      private String Ddo_tipodocumento_nome_Selectedvalue_set ;
      private String Ddo_tipodocumento_nome_Dropdownoptionstype ;
      private String Ddo_tipodocumento_nome_Titlecontrolidtoreplace ;
      private String Ddo_tipodocumento_nome_Sortedstatus ;
      private String Ddo_tipodocumento_nome_Filtertype ;
      private String Ddo_tipodocumento_nome_Datalisttype ;
      private String Ddo_tipodocumento_nome_Datalistproc ;
      private String Ddo_tipodocumento_nome_Sortasc ;
      private String Ddo_tipodocumento_nome_Sortdsc ;
      private String Ddo_tipodocumento_nome_Loadingdata ;
      private String Ddo_tipodocumento_nome_Cleanfilter ;
      private String Ddo_tipodocumento_nome_Noresultsfound ;
      private String Ddo_tipodocumento_nome_Searchbuttontext ;
      private String Ddo_lotearquivoanexo_data_Caption ;
      private String Ddo_lotearquivoanexo_data_Tooltip ;
      private String Ddo_lotearquivoanexo_data_Cls ;
      private String Ddo_lotearquivoanexo_data_Filteredtext_set ;
      private String Ddo_lotearquivoanexo_data_Filteredtextto_set ;
      private String Ddo_lotearquivoanexo_data_Dropdownoptionstype ;
      private String Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace ;
      private String Ddo_lotearquivoanexo_data_Sortedstatus ;
      private String Ddo_lotearquivoanexo_data_Filtertype ;
      private String Ddo_lotearquivoanexo_data_Sortasc ;
      private String Ddo_lotearquivoanexo_data_Sortdsc ;
      private String Ddo_lotearquivoanexo_data_Cleanfilter ;
      private String Ddo_lotearquivoanexo_data_Rangefilterfrom ;
      private String Ddo_lotearquivoanexo_data_Rangefilterto ;
      private String Ddo_lotearquivoanexo_data_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTflotearquivoanexo_nomearq_Internalname ;
      private String edtavTflotearquivoanexo_nomearq_Jsonclick ;
      private String edtavTflotearquivoanexo_nomearq_sel_Internalname ;
      private String edtavTflotearquivoanexo_nomearq_sel_Jsonclick ;
      private String edtavTftipodocumento_nome_Internalname ;
      private String edtavTftipodocumento_nome_Jsonclick ;
      private String edtavTftipodocumento_nome_sel_Internalname ;
      private String edtavTftipodocumento_nome_sel_Jsonclick ;
      private String edtavTflotearquivoanexo_data_Internalname ;
      private String edtavTflotearquivoanexo_data_Jsonclick ;
      private String edtavTflotearquivoanexo_data_to_Internalname ;
      private String edtavTflotearquivoanexo_data_to_Jsonclick ;
      private String divDdo_lotearquivoanexo_dataauxdates_Internalname ;
      private String edtavDdo_lotearquivoanexo_dataauxdate_Internalname ;
      private String edtavDdo_lotearquivoanexo_dataauxdate_Jsonclick ;
      private String edtavDdo_lotearquivoanexo_dataauxdateto_Internalname ;
      private String edtavDdo_lotearquivoanexo_dataauxdateto_Jsonclick ;
      private String edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtLoteArquivoAnexo_LoteCod_Internalname ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String edtLoteArquivoAnexo_Arquivo_Internalname ;
      private String edtLoteArquivoAnexo_Data_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ;
      private String lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ;
      private String lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ;
      private String lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ;
      private String lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ;
      private String lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ;
      private String lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ;
      private String lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ;
      private String AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ;
      private String AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ;
      private String AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ;
      private String AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ;
      private String AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ;
      private String AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ;
      private String AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ;
      private String AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ;
      private String AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ;
      private String AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String edtLoteArquivoAnexo_Arquivo_Filename ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String edtLoteArquivoAnexo_Arquivo_Filetype ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavLotearquivoanexo_nomearq1_Internalname ;
      private String edtavTipodocumento_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavLotearquivoanexo_nomearq2_Internalname ;
      private String edtavTipodocumento_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavLotearquivoanexo_nomearq3_Internalname ;
      private String edtavTipodocumento_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_lotearquivoanexo_nomearq_Internalname ;
      private String Ddo_tipodocumento_nome_Internalname ;
      private String Ddo_lotearquivoanexo_data_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtLoteArquivoAnexo_NomeArq_Title ;
      private String edtLoteArquivoAnexo_NomeArq_Internalname ;
      private String edtTipoDocumento_Nome_Title ;
      private String edtLoteArquivoAnexo_Data_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtLoteArquivoAnexo_NomeArq_Link ;
      private String edtTipoDocumento_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblLotearquivoanexotitle_Internalname ;
      private String lblLotearquivoanexotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavLotearquivoanexo_nomearq3_Jsonclick ;
      private String edtavTipodocumento_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavLotearquivoanexo_nomearq2_Jsonclick ;
      private String edtavTipodocumento_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavLotearquivoanexo_nomearq1_Jsonclick ;
      private String edtavTipodocumento_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_91_fel_idx="0001" ;
      private String ROClassString ;
      private String edtLoteArquivoAnexo_LoteCod_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String edtLoteArquivoAnexo_Arquivo_Contenttype ;
      private String edtLoteArquivoAnexo_Arquivo_Parameters ;
      private String edtLoteArquivoAnexo_Arquivo_Jsonclick ;
      private String edtLoteArquivoAnexo_Data_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV45TFLoteArquivoAnexo_Data ;
      private DateTime AV46TFLoteArquivoAnexo_Data_To ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private DateTime AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ;
      private DateTime AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ;
      private DateTime AV47DDO_LoteArquivoAnexo_DataAuxDate ;
      private DateTime AV48DDO_LoteArquivoAnexo_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool n645TipoDocumento_Codigo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_lotearquivoanexo_nomearq_Includesortasc ;
      private bool Ddo_lotearquivoanexo_nomearq_Includesortdsc ;
      private bool Ddo_lotearquivoanexo_nomearq_Includefilter ;
      private bool Ddo_lotearquivoanexo_nomearq_Filterisrange ;
      private bool Ddo_lotearquivoanexo_nomearq_Includedatalist ;
      private bool Ddo_tipodocumento_nome_Includesortasc ;
      private bool Ddo_tipodocumento_nome_Includesortdsc ;
      private bool Ddo_tipodocumento_nome_Includefilter ;
      private bool Ddo_tipodocumento_nome_Filterisrange ;
      private bool Ddo_tipodocumento_nome_Includedatalist ;
      private bool Ddo_lotearquivoanexo_data_Includesortasc ;
      private bool Ddo_lotearquivoanexo_data_Includesortdsc ;
      private bool Ddo_lotearquivoanexo_data_Includefilter ;
      private bool Ddo_lotearquivoanexo_data_Filterisrange ;
      private bool Ddo_lotearquivoanexo_data_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private bool AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ;
      private bool AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV39ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace ;
      private String AV43ddo_TipoDocumento_NomeTitleControlIdToReplace ;
      private String AV49ddo_LoteArquivoAnexo_DataTitleControlIdToReplace ;
      private String AV76Update_GXI ;
      private String AV77Delete_GXI ;
      private String AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ;
      private String AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ;
      private String AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ;
      private String AV31Update ;
      private String AV32Delete ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private IGxSession AV33Session ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00EW2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] H00EW2_n839LoteArquivoAnexo_NomeArq ;
      private String[] H00EW2_A840LoteArquivoAnexo_TipoArq ;
      private bool[] H00EW2_n840LoteArquivoAnexo_TipoArq ;
      private int[] H00EW2_A645TipoDocumento_Codigo ;
      private bool[] H00EW2_n645TipoDocumento_Codigo ;
      private DateTime[] H00EW2_A836LoteArquivoAnexo_Data ;
      private String[] H00EW2_A646TipoDocumento_Nome ;
      private int[] H00EW2_A841LoteArquivoAnexo_LoteCod ;
      private String[] H00EW2_A838LoteArquivoAnexo_Arquivo ;
      private bool[] H00EW2_n838LoteArquivoAnexo_Arquivo ;
      private long[] H00EW3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36LoteArquivoAnexo_NomeArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40TipoDocumento_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44LoteArquivoAnexo_DataTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV50DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwlotearquivoanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00EW2( IGxContext context ,
                                             String AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                             short AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                             String AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                             String AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                             bool AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                             String AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                             short AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                             String AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                             String AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                             bool AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                             String AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                             short AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                             String AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                             String AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                             String AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                             String AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                             String AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                             String AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                             DateTime AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                             DateTime AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [23] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[LoteArquivoAnexo_NomeArq], T1.[LoteArquivoAnexo_TipoArq], T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_Data], T2.[TipoDocumento_Nome], T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Arquivo]";
         sFromString = " FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_NomeArq]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_NomeArq] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[TipoDocumento_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[TipoDocumento_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_Data]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Data]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00EW3( IGxContext context ,
                                             String AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1 ,
                                             short AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 ,
                                             String AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1 ,
                                             String AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1 ,
                                             bool AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 ,
                                             String AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2 ,
                                             short AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 ,
                                             String AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2 ,
                                             String AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2 ,
                                             bool AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 ,
                                             String AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3 ,
                                             short AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 ,
                                             String AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3 ,
                                             String AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3 ,
                                             String AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel ,
                                             String AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq ,
                                             String AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel ,
                                             String AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome ,
                                             DateTime AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data ,
                                             DateTime AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [18] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56WWLoteArquivoAnexoDS_1_Dynamicfiltersselector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV57WWLoteArquivoAnexoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV60WWLoteArquivoAnexoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWLoteArquivoAnexoDS_6_Dynamicfiltersselector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV62WWLoteArquivoAnexoDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV65WWLoteArquivoAnexoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWLoteArquivoAnexoDS_11_Dynamicfiltersselector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV67WWLoteArquivoAnexoDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00EW2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
               case 1 :
                     return conditional_H00EW3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EW2 ;
          prmH00EW2 = new Object[] {
          new Object[] {"@lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00EW3 ;
          prmH00EW3 = new Object[] {
          new Object[] {"@lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWLoteArquivoAnexoDS_3_Lotearquivoanexo_nomearq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWLoteArquivoAnexoDS_4_Tipodocumento_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWLoteArquivoAnexoDS_8_Lotearquivoanexo_nomearq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64WWLoteArquivoAnexoDS_9_Tipodocumento_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWLoteArquivoAnexoDS_13_Lotearquivoanexo_nomearq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWLoteArquivoAnexoDS_14_Tipodocumento_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWLoteArquivoAnexoDS_15_Tflotearquivoanexo_nomearq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWLoteArquivoAnexoDS_16_Tflotearquivoanexo_nomearq_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWLoteArquivoAnexoDS_17_Tftipodocumento_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWLoteArquivoAnexoDS_18_Tftipodocumento_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWLoteArquivoAnexoDS_19_Tflotearquivoanexo_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV75WWLoteArquivoAnexoDS_20_Tflotearquivoanexo_data_to",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EW2,11,0,true,false )
             ,new CursorDef("H00EW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EW3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((String[]) buf[9])[0] = rslt.getBLOBFile(7, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                return;
       }
    }

 }

}
