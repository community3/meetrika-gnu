/*
               File: gpxreCAPTCHAValid
        Description: gpxre CAPTCHAValid
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:32.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gpxrecaptchavalid : GXProcedure
   {
      public gpxrecaptchavalid( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gpxrecaptchavalid( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_PrivateKey ,
                           String aP1_Response ,
                           out String aP2_CaptchaMessage ,
                           out bool aP3_CaptchaValid )
      {
         this.AV11PrivateKey = aP0_PrivateKey;
         this.AV12Response = aP1_Response;
         this.AV8CaptchaMessage = "" ;
         this.AV9CaptchaValid = false ;
         initialize();
         executePrivate();
         aP2_CaptchaMessage=this.AV8CaptchaMessage;
         aP3_CaptchaValid=this.AV9CaptchaValid;
      }

      public bool executeUdp( String aP0_PrivateKey ,
                              String aP1_Response ,
                              out String aP2_CaptchaMessage )
      {
         this.AV11PrivateKey = aP0_PrivateKey;
         this.AV12Response = aP1_Response;
         this.AV8CaptchaMessage = "" ;
         this.AV9CaptchaValid = false ;
         initialize();
         executePrivate();
         aP2_CaptchaMessage=this.AV8CaptchaMessage;
         aP3_CaptchaValid=this.AV9CaptchaValid;
         return AV9CaptchaValid ;
      }

      public void executeSubmit( String aP0_PrivateKey ,
                                 String aP1_Response ,
                                 out String aP2_CaptchaMessage ,
                                 out bool aP3_CaptchaValid )
      {
         gpxrecaptchavalid objgpxrecaptchavalid;
         objgpxrecaptchavalid = new gpxrecaptchavalid();
         objgpxrecaptchavalid.AV11PrivateKey = aP0_PrivateKey;
         objgpxrecaptchavalid.AV12Response = aP1_Response;
         objgpxrecaptchavalid.AV8CaptchaMessage = "" ;
         objgpxrecaptchavalid.AV9CaptchaValid = false ;
         objgpxrecaptchavalid.context.SetSubmitInitialConfig(context);
         objgpxrecaptchavalid.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgpxrecaptchavalid);
         aP2_CaptchaMessage=this.AV8CaptchaMessage;
         aP3_CaptchaValid=this.AV9CaptchaValid;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gpxrecaptchavalid)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9CaptchaValid = false;
         AV10httpClient.AddVariable("secret", AV11PrivateKey);
         AV10httpClient.AddVariable("response", AV12Response);
         AV10httpClient.AddVariable("remoteip", context.GetRemoteAddress( ));
         AV10httpClient.Execute("POST", "https://www.google.com/recaptcha/api/siteverify");
         if ( AV10httpClient.ErrCode == 0 )
         {
            Gx_msg = AV10httpClient.ToString();
            if ( StringUtil.StringSearch( Gx_msg, "\"success\": true", 1) > 0 )
            {
               AV9CaptchaValid = true;
               AV8CaptchaMessage = "success";
            }
            else
            {
               AV8CaptchaMessage = "fail";
            }
         }
         else
         {
            AV8CaptchaMessage = AV10httpClient.ErrDescription;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10httpClient = new GxHttpClient( context);
         Gx_msg = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV11PrivateKey ;
      private String AV12Response ;
      private String Gx_msg ;
      private String AV8CaptchaMessage ;
      private bool AV9CaptchaValid ;
      private String aP2_CaptchaMessage ;
      private bool aP3_CaptchaValid ;
      private GxHttpClient AV10httpClient ;
   }

}
