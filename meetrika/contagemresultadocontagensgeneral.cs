/*
               File: ContagemResultadoContagensGeneral
        Description: Contagem Resultado Contagens General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:20:31.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadocontagensgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadocontagensgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadocontagensgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_ContagemResultado_DataCnt ,
                           String aP2_ContagemResultado_HoraCnt )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.A473ContagemResultado_DataCnt = aP1_ContagemResultado_DataCnt;
         this.A511ContagemResultado_HoraCnt = aP2_ContagemResultado_HoraCnt;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagemResultado_StatusCnt = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A473ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
                  A511ContagemResultado_HoraCnt = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A456ContagemResultado_Codigo,(DateTime)A473ContagemResultado_DataCnt,(String)A511ContagemResultado_HoraCnt});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PABA2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV16Pgmname = "ContagemResultadoContagensGeneral";
               context.Gx_err = 0;
               /* Using cursor H00BA2 */
               pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
               A484ContagemResultado_StatusDmn = H00BA2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00BA2_n484ContagemResultado_StatusDmn[0];
               pr_default.close(0);
               WSBA2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado Contagens General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120203197");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadocontagensgeneral.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A473ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(A511ContagemResultado_HoraCnt))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA473ContagemResultado_DataCnt", context.localUtil.DToC( wcpOA473ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA511ContagemResultado_HoraCnt", StringUtil.RTrim( wcpOA511ContagemResultado_HoraCnt));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PARECERTCN", GetSecureSignedToken( sPrefix, A463ContagemResultado_ParecerTcn));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_NOMEPLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A853ContagemResultado_NomePla, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_TIPOPLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A854ContagemResultado_TipoPla, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCONTAGENS_ESFORCO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContagemResultadoContagensGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemresultadocontagensgeneral:[SendSecurityCheck value for]"+"ContagemResultado_StatusCnt:"+context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9"));
      }

      protected void RenderHtmlCloseFormBA2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadocontagensgeneral.js", "?2020312020320");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoContagensGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Contagens General" ;
      }

      protected void WBBA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadocontagensgeneral.aspx");
            }
            wb_table1_2_BA2( true) ;
         }
         else
         {
            wb_table1_2_BA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultado_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTBA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado Contagens General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPBA0( ) ;
            }
         }
      }

      protected void WSBA2( )
      {
         STARTBA2( ) ;
         EVTBA2( ) ;
      }

      protected void EVTBA2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11BA2 */
                                    E11BA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12BA2 */
                                    E12BA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13BA2 */
                                    E13BA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14BA2 */
                                    E14BA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15BA2 */
                                    E15BA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16BA2 */
                                    E16BA2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBA0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormBA2( ) ;
            }
         }
      }

      protected void PABA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbContagemResultado_StatusCnt.Name = "CONTAGEMRESULTADO_STATUSCNT";
            cmbContagemResultado_StatusCnt.WebTags = "";
            cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
            cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
            cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
            cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
            cmbContagemResultado_StatusCnt.addItem("5", "Aprovada", 0);
            cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
            cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
            if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
            {
               A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
         {
            A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV16Pgmname = "ContagemResultadoContagensGeneral";
         context.Gx_err = 0;
      }

      protected void RFBA2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E16BA2 */
         E16BA2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00BA3 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A479ContagemResultado_CrFMPessoaCod = H00BA3_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BA3_n479ContagemResultado_CrFMPessoaCod[0];
               A470ContagemResultado_ContadorFMCod = H00BA3_A470ContagemResultado_ContadorFMCod[0];
               A469ContagemResultado_NaoCnfCntCod = H00BA3_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = H00BA3_n469ContagemResultado_NaoCnfCntCod[0];
               A482ContagemResultadoContagens_Esforco = H00BA3_A482ContagemResultadoContagens_Esforco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOCONTAGENS_ESFORCO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")));
               A483ContagemResultado_StatusCnt = H00BA3_A483ContagemResultado_StatusCnt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
               A462ContagemResultado_Divergencia = H00BA3_A462ContagemResultado_Divergencia[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A462ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( A462ContagemResultado_Divergencia, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
               A854ContagemResultado_TipoPla = H00BA3_A854ContagemResultado_TipoPla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_TIPOPLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A854ContagemResultado_TipoPla, ""))));
               n854ContagemResultado_TipoPla = H00BA3_n854ContagemResultado_TipoPla[0];
               A853ContagemResultado_NomePla = H00BA3_A853ContagemResultado_NomePla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_NOMEPLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A853ContagemResultado_NomePla, ""))));
               n853ContagemResultado_NomePla = H00BA3_n853ContagemResultado_NomePla[0];
               A478ContagemResultado_NaoCnfCntNom = H00BA3_A478ContagemResultado_NaoCnfCntNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A478ContagemResultado_NaoCnfCntNom", A478ContagemResultado_NaoCnfCntNom);
               n478ContagemResultado_NaoCnfCntNom = H00BA3_n478ContagemResultado_NaoCnfCntNom[0];
               A463ContagemResultado_ParecerTcn = H00BA3_A463ContagemResultado_ParecerTcn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A463ContagemResultado_ParecerTcn", A463ContagemResultado_ParecerTcn);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PARECERTCN", GetSecureSignedToken( sPrefix, A463ContagemResultado_ParecerTcn));
               n463ContagemResultado_ParecerTcn = H00BA3_n463ContagemResultado_ParecerTcn[0];
               A461ContagemResultado_PFLFM = H00BA3_A461ContagemResultado_PFLFM[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
               n461ContagemResultado_PFLFM = H00BA3_n461ContagemResultado_PFLFM[0];
               A460ContagemResultado_PFBFM = H00BA3_A460ContagemResultado_PFBFM[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
               n460ContagemResultado_PFBFM = H00BA3_n460ContagemResultado_PFBFM[0];
               A459ContagemResultado_PFLFS = H00BA3_A459ContagemResultado_PFLFS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
               n459ContagemResultado_PFLFS = H00BA3_n459ContagemResultado_PFLFS[0];
               A458ContagemResultado_PFBFS = H00BA3_A458ContagemResultado_PFBFS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
               n458ContagemResultado_PFBFS = H00BA3_n458ContagemResultado_PFBFS[0];
               A474ContagemResultado_ContadorFMNom = H00BA3_A474ContagemResultado_ContadorFMNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A474ContagemResultado_ContadorFMNom", A474ContagemResultado_ContadorFMNom);
               n474ContagemResultado_ContadorFMNom = H00BA3_n474ContagemResultado_ContadorFMNom[0];
               A479ContagemResultado_CrFMPessoaCod = H00BA3_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00BA3_n479ContagemResultado_CrFMPessoaCod[0];
               A474ContagemResultado_ContadorFMNom = H00BA3_A474ContagemResultado_ContadorFMNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A474ContagemResultado_ContadorFMNom", A474ContagemResultado_ContadorFMNom);
               n474ContagemResultado_ContadorFMNom = H00BA3_n474ContagemResultado_ContadorFMNom[0];
               A478ContagemResultado_NaoCnfCntNom = H00BA3_A478ContagemResultado_NaoCnfCntNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A478ContagemResultado_NaoCnfCntNom", A478ContagemResultado_NaoCnfCntNom);
               n478ContagemResultado_NaoCnfCntNom = H00BA3_n478ContagemResultado_NaoCnfCntNom[0];
               /* Execute user event: E12BA2 */
               E12BA2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBBA0( ) ;
         }
      }

      protected void STRUPBA0( )
      {
         /* Before Start, stand alone formulas. */
         AV16Pgmname = "ContagemResultadoContagensGeneral";
         context.Gx_err = 0;
         /* Using cursor H00BA4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         A484ContagemResultado_StatusDmn = H00BA4_A484ContagemResultado_StatusDmn[0];
         n484ContagemResultado_StatusDmn = H00BA4_n484ContagemResultado_StatusDmn[0];
         pr_default.close(2);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11BA2 */
         E11BA2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A474ContagemResultado_ContadorFMNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContadorFMNom_Internalname));
            n474ContagemResultado_ContadorFMNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A474ContagemResultado_ContadorFMNom", A474ContagemResultado_ContadorFMNom);
            A458ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFS_Internalname), ",", ".");
            n458ContagemResultado_PFBFS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
            A459ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFS_Internalname), ",", ".");
            n459ContagemResultado_PFLFS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A459ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
            A460ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFM_Internalname), ",", ".");
            n460ContagemResultado_PFBFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A460ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
            A461ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFM_Internalname), ",", ".");
            n461ContagemResultado_PFLFM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A461ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
            A463ContagemResultado_ParecerTcn = cgiGet( edtContagemResultado_ParecerTcn_Internalname);
            n463ContagemResultado_ParecerTcn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A463ContagemResultado_ParecerTcn", A463ContagemResultado_ParecerTcn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_PARECERTCN", GetSecureSignedToken( sPrefix, A463ContagemResultado_ParecerTcn));
            A478ContagemResultado_NaoCnfCntNom = StringUtil.Upper( cgiGet( edtContagemResultado_NaoCnfCntNom_Internalname));
            n478ContagemResultado_NaoCnfCntNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A478ContagemResultado_NaoCnfCntNom", A478ContagemResultado_NaoCnfCntNom);
            A853ContagemResultado_NomePla = cgiGet( edtContagemResultado_NomePla_Internalname);
            n853ContagemResultado_NomePla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A853ContagemResultado_NomePla", A853ContagemResultado_NomePla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_NOMEPLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A853ContagemResultado_NomePla, ""))));
            A854ContagemResultado_TipoPla = cgiGet( edtContagemResultado_TipoPla_Internalname);
            n854ContagemResultado_TipoPla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A854ContagemResultado_TipoPla", A854ContagemResultado_TipoPla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_TIPOPLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A854ContagemResultado_TipoPla, ""))));
            A462ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( edtContagemResultado_Divergencia_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A462ContagemResultado_Divergencia", StringUtil.LTrim( StringUtil.Str( A462ContagemResultado_Divergencia, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
            cmbContagemResultado_StatusCnt.CurrentValue = cgiGet( cmbContagemResultado_StatusCnt_Internalname);
            A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_StatusCnt_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
            A482ContagemResultadoContagens_Esforco = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoContagens_Esforco_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A482ContagemResultadoContagens_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADOCONTAGENS_ESFORCO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")));
            /* Read saved values. */
            wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
            wcpOA473ContagemResultado_DataCnt = context.localUtil.CToD( cgiGet( sPrefix+"wcpOA473ContagemResultado_DataCnt"), 0);
            wcpOA511ContagemResultado_HoraCnt = cgiGet( sPrefix+"wcpOA511ContagemResultado_HoraCnt");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContagemResultadoContagensGeneral";
            A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_StatusCnt_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A483ContagemResultado_StatusCnt", StringUtil.LTrim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contagemresultadocontagensgeneral:[SecurityCheckFailed value for]"+"ContagemResultado_StatusCnt:"+context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11BA2 */
         E11BA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11BA2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12BA2( )
      {
         /* Load Routine */
         edtContagemResultado_ContadorFMNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A470ContagemResultado_ContadorFMCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_ContadorFMNom_Internalname, "Link", edtContagemResultado_ContadorFMNom_Link);
         edtContagemResultado_NaoCnfCntNom_Link = formatLink("viewnaoconformidade.aspx") + "?" + UrlEncode("" +A469ContagemResultado_NaoCnfCntCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_NaoCnfCntNom_Internalname, "Link", edtContagemResultado_NaoCnfCntNom_Link);
         edtContagemResultado_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Visible), 5, 0)));
         if ( ! ( A483ContagemResultado_StatusCnt == 7 ) )
         {
            bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Userehgestor&&AV6WWPContext.gxTpr_Update ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
            bttBtndelete_Visible = (AV6WWPContext.gxTpr_Userehgestor&&AV6WWPContext.gxTpr_Delete ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
         }
      }

      protected void E13BA2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagemresultadocontagens.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A473ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(A511ContagemResultado_HoraCnt));
         context.wjLocDisableFrm = 1;
      }

      protected void E14BA2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagemresultadocontagens.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(A473ContagemResultado_DataCnt)) + "," + UrlEncode(StringUtil.RTrim(A511ContagemResultado_HoraCnt));
         context.wjLocDisableFrm = 1;
      }

      protected void E15BA2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV16Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContagemResultadoContagens";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultado_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultado_DataCnt";
         AV10TrnContextAtt.gxTpr_Attributevalue = context.localUtil.DToC( AV8ContagemResultado_DataCnt, 2, "/");
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContagemResultado_HoraCnt";
         AV10TrnContextAtt.gxTpr_Attributevalue = AV13ContagemResultado_HoraCnt;
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E16BA2( )
      {
         /* Refresh Routine */
         bttBtnupdate_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam||!((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         bttBtndelete_Enabled = (AV6WWPContext.gxTpr_Userehadministradorgam||!((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         edtContagemResultado_ContadorFMNom_Visible = (!AV6WWPContext.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_ContadorFMNom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_ContadorFMNom_Visible), 5, 0)));
         lblTextblockcontagemresultado_contadorfmnom_Visible = (!AV6WWPContext.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTextblockcontagemresultado_contadorfmnom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontagemresultado_contadorfmnom_Visible), 5, 0)));
      }

      protected void wb_table1_2_BA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_BA2( true) ;
         }
         else
         {
            wb_table2_8_BA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_BA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_87_BA2( true) ;
         }
         else
         {
            wb_table3_87_BA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_87_BA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BA2e( true) ;
         }
         else
         {
            wb_table1_2_BA2e( false) ;
         }
      }

      protected void wb_table3_87_BA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_87_BA2e( true) ;
         }
         else
         {
            wb_table3_87_BA2e( false) ;
         }
      }

      protected void wb_table2_8_BA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datacnt_Internalname, "Data", "", "", lblTextblockcontagemresultado_datacnt_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table4_13_BA2( true) ;
         }
         else
         {
            wb_table4_13_BA2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_BA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contadorfmnom_Internalname, "Contador FM", "", "", lblTextblockcontagemresultado_contadorfmnom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontagemresultado_contadorfmnom_Visible, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_ContadorFMNom_Internalname, StringUtil.RTrim( A474ContagemResultado_ContadorFMNom), StringUtil.RTrim( context.localUtil.Format( A474ContagemResultado_ContadorFMNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemResultado_ContadorFMNom_Link, "", "", "", edtContagemResultado_ContadorFMNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtContagemResultado_ContadorFMNom_Visible, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfs_Internalname, "PF Bruto FS", "", "", lblTextblockcontagemresultado_pfbfs_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFBFS_Internalname, StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")), context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFBFS_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfs_Internalname, "PF L�quido FS", "", "", lblTextblockcontagemresultado_pflfs_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFLFS_Internalname, StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ",", "")), context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFLFS_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfm_Internalname, "PF Bruto FM", "", "", lblTextblockcontagemresultado_pfbfm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFBFM_Internalname, StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")), context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFBFM_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfm_Internalname, "PF L�quido FM", "", "", lblTextblockcontagemresultado_pflfm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFLFM_Internalname, StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ",", "")), context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFLFM_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_parecertcn_Internalname, "Parecer", "", "", lblTextblockcontagemresultado_parecertcn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultado_ParecerTcn_Internalname, A463ContagemResultado_ParecerTcn, "", "", 0, 1, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_naocnfcntnom_Internalname, "N�o conformidade", "", "", lblTextblockcontagemresultado_naocnfcntnom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_NaoCnfCntNom_Internalname, StringUtil.RTrim( A478ContagemResultado_NaoCnfCntNom), StringUtil.RTrim( context.localUtil.Format( A478ContagemResultado_NaoCnfCntNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemResultado_NaoCnfCntNom_Link, "", "", "", edtContagemResultado_NaoCnfCntNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_nomepla_Internalname, "Nome da Planilha", "", "", lblTextblockcontagemresultado_nomepla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_NomePla_Internalname, StringUtil.RTrim( A853ContagemResultado_NomePla), StringUtil.RTrim( context.localUtil.Format( A853ContagemResultado_NomePla, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_NomePla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_tipopla_Internalname, "Tipo de Arquivo", "", "", lblTextblockcontagemresultado_tipopla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_TipoPla_Internalname, StringUtil.RTrim( A854ContagemResultado_TipoPla), StringUtil.RTrim( context.localUtil.Format( A854ContagemResultado_TipoPla, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_TipoPla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_divergencia_Internalname, "Diverg�ncia", "", "", lblTextblockcontagemresultado_divergencia_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table5_64_BA2( true) ;
         }
         else
         {
            wb_table5_64_BA2( false) ;
         }
         return  ;
      }

      protected void wb_table5_64_BA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_statuscnt_Internalname, "Status da Cnt", "", "", lblTextblockcontagemresultado_statuscnt_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultado_StatusCnt, cmbContagemResultado_StatusCnt_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)), 1, cmbContagemResultado_StatusCnt_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemResultadoContagensGeneral.htm");
            cmbContagemResultado_StatusCnt.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultado_StatusCnt_Internalname, "Values", (String)(cmbContagemResultado_StatusCnt.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadocontagens_esforco_Internalname, "Esfor�o", "", "", lblTextblockcontagemresultadocontagens_esforco_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table6_77_BA2( true) ;
         }
         else
         {
            wb_table6_77_BA2( false) ;
         }
         return  ;
      }

      protected void wb_table6_77_BA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_BA2e( true) ;
         }
         else
         {
            wb_table2_8_BA2e( false) ;
         }
      }

      protected void wb_table6_77_BA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultadocontagens_esforco_Internalname, tblTablemergedcontagemresultadocontagens_esforco_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoContagens_Esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoContagens_Esforco_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadocontagens_esforco_righttext_Internalname, "min.", "", "", lblContagemresultadocontagens_esforco_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_77_BA2e( true) ;
         }
         else
         {
            wb_table6_77_BA2e( false) ;
         }
      }

      protected void wb_table5_64_BA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_divergencia_Internalname, tblTablemergedcontagemresultado_divergencia_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Divergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ",", "")), context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Divergencia_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_divergencia_righttext_Internalname, "%", "", "", lblContagemresultado_divergencia_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_64_BA2e( true) ;
         }
         else
         {
            wb_table5_64_BA2e( false) ;
         }
      }

      protected void wb_table4_13_BA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_datacnt_Internalname, tblTablemergedcontagemresultado_datacnt_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataCnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataCnt_Internalname, context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"), context.localUtil.Format( A473ContagemResultado_DataCnt, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataCnt_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoContagensGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataCnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_HoraCnt_Internalname, StringUtil.RTrim( A511ContagemResultado_HoraCnt), StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_HoraCnt_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoContagensGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_BA2e( true) ;
         }
         else
         {
            wb_table4_13_BA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         A473ContagemResultado_DataCnt = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
         A511ContagemResultado_HoraCnt = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABA2( ) ;
         WSBA2( ) ;
         WEBA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA456ContagemResultado_Codigo = (String)((String)getParm(obj,0));
         sCtrlA473ContagemResultado_DataCnt = (String)((String)getParm(obj,1));
         sCtrlA511ContagemResultado_HoraCnt = (String)((String)getParm(obj,2));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PABA2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadocontagensgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PABA2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A473ContagemResultado_DataCnt = (DateTime)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
            A511ContagemResultado_HoraCnt = (String)getParm(obj,4);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
         }
         wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
         wcpOA473ContagemResultado_DataCnt = context.localUtil.CToD( cgiGet( sPrefix+"wcpOA473ContagemResultado_DataCnt"), 0);
         wcpOA511ContagemResultado_HoraCnt = cgiGet( sPrefix+"wcpOA511ContagemResultado_HoraCnt");
         if ( ! GetJustCreated( ) && ( ( A456ContagemResultado_Codigo != wcpOA456ContagemResultado_Codigo ) || ( A473ContagemResultado_DataCnt != wcpOA473ContagemResultado_DataCnt ) || ( StringUtil.StrCmp(A511ContagemResultado_HoraCnt, wcpOA511ContagemResultado_HoraCnt) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOA456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
         wcpOA473ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
         wcpOA511ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA456ContagemResultado_Codigo = cgiGet( sPrefix+"A456ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA456ContagemResultado_Codigo) > 0 )
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA456ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A456ContagemResultado_Codigo_PARM"), ",", "."));
         }
         sCtrlA473ContagemResultado_DataCnt = cgiGet( sPrefix+"A473ContagemResultado_DataCnt_CTRL");
         if ( StringUtil.Len( sCtrlA473ContagemResultado_DataCnt) > 0 )
         {
            A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( sCtrlA473ContagemResultado_DataCnt), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
         }
         else
         {
            A473ContagemResultado_DataCnt = context.localUtil.CToD( cgiGet( sPrefix+"A473ContagemResultado_DataCnt_PARM"), 0);
         }
         sCtrlA511ContagemResultado_HoraCnt = cgiGet( sPrefix+"A511ContagemResultado_HoraCnt_CTRL");
         if ( StringUtil.Len( sCtrlA511ContagemResultado_HoraCnt) > 0 )
         {
            A511ContagemResultado_HoraCnt = cgiGet( sCtrlA511ContagemResultado_HoraCnt);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
         }
         else
         {
            A511ContagemResultado_HoraCnt = cgiGet( sPrefix+"A511ContagemResultado_HoraCnt_PARM");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PABA2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSBA2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSBA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A473ContagemResultado_DataCnt_PARM", context.localUtil.DToC( A473ContagemResultado_DataCnt, 0, "/"));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA473ContagemResultado_DataCnt)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A473ContagemResultado_DataCnt_CTRL", StringUtil.RTrim( sCtrlA473ContagemResultado_DataCnt));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A511ContagemResultado_HoraCnt_PARM", StringUtil.RTrim( A511ContagemResultado_HoraCnt));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA511ContagemResultado_HoraCnt)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A511ContagemResultado_HoraCnt_CTRL", StringUtil.RTrim( sCtrlA511ContagemResultado_HoraCnt));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEBA2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120203279");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadocontagensgeneral.js", "?20203120203280");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_datacnt_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_DATACNT";
         edtContagemResultado_DataCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACNT";
         edtContagemResultado_HoraCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_HORACNT";
         tblTablemergedcontagemresultado_datacnt_Internalname = sPrefix+"TABLEMERGEDCONTAGEMRESULTADO_DATACNT";
         lblTextblockcontagemresultado_contadorfmnom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_CONTADORFMNOM";
         edtContagemResultado_ContadorFMNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFMNOM";
         lblTextblockcontagemresultado_pfbfs_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_PFBFS";
         edtContagemResultado_PFBFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFS";
         lblTextblockcontagemresultado_pflfs_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_PFLFS";
         edtContagemResultado_PFLFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFS";
         lblTextblockcontagemresultado_pfbfm_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_PFBFM";
         edtContagemResultado_PFBFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFM";
         lblTextblockcontagemresultado_pflfm_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_PFLFM";
         edtContagemResultado_PFLFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFM";
         lblTextblockcontagemresultado_parecertcn_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_PARECERTCN";
         edtContagemResultado_ParecerTcn_Internalname = sPrefix+"CONTAGEMRESULTADO_PARECERTCN";
         lblTextblockcontagemresultado_naocnfcntnom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_NAOCNFCNTNOM";
         edtContagemResultado_NaoCnfCntNom_Internalname = sPrefix+"CONTAGEMRESULTADO_NAOCNFCNTNOM";
         lblTextblockcontagemresultado_nomepla_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_NOMEPLA";
         edtContagemResultado_NomePla_Internalname = sPrefix+"CONTAGEMRESULTADO_NOMEPLA";
         lblTextblockcontagemresultado_tipopla_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_TIPOPLA";
         edtContagemResultado_TipoPla_Internalname = sPrefix+"CONTAGEMRESULTADO_TIPOPLA";
         lblTextblockcontagemresultado_divergencia_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_DIVERGENCIA";
         edtContagemResultado_Divergencia_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA";
         lblContagemresultado_divergencia_righttext_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA_RIGHTTEXT";
         tblTablemergedcontagemresultado_divergencia_Internalname = sPrefix+"TABLEMERGEDCONTAGEMRESULTADO_DIVERGENCIA";
         lblTextblockcontagemresultado_statuscnt_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_STATUSCNT";
         cmbContagemResultado_StatusCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSCNT";
         lblTextblockcontagemresultadocontagens_esforco_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADOCONTAGENS_ESFORCO";
         edtContagemResultadoContagens_Esforco_Internalname = sPrefix+"CONTAGEMRESULTADOCONTAGENS_ESFORCO";
         lblContagemresultadocontagens_esforco_righttext_Internalname = sPrefix+"CONTAGEMRESULTADOCONTAGENS_ESFORCO_RIGHTTEXT";
         tblTablemergedcontagemresultadocontagens_esforco_Internalname = sPrefix+"TABLEMERGEDCONTAGEMRESULTADOCONTAGENS_ESFORCO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultado_HoraCnt_Jsonclick = "";
         edtContagemResultado_DataCnt_Jsonclick = "";
         edtContagemResultado_Divergencia_Jsonclick = "";
         edtContagemResultadoContagens_Esforco_Jsonclick = "";
         cmbContagemResultado_StatusCnt_Jsonclick = "";
         edtContagemResultado_TipoPla_Jsonclick = "";
         edtContagemResultado_NomePla_Jsonclick = "";
         edtContagemResultado_NaoCnfCntNom_Jsonclick = "";
         edtContagemResultado_PFLFM_Jsonclick = "";
         edtContagemResultado_PFBFM_Jsonclick = "";
         edtContagemResultado_PFLFS_Jsonclick = "";
         edtContagemResultado_PFBFS_Jsonclick = "";
         edtContagemResultado_ContadorFMNom_Jsonclick = "";
         lblTextblockcontagemresultado_contadorfmnom_Visible = 1;
         bttBtndelete_Enabled = 1;
         bttBtndelete_Visible = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnupdate_Visible = 1;
         edtContagemResultado_ContadorFMNom_Visible = 1;
         edtContagemResultado_NaoCnfCntNom_Link = "";
         edtContagemResultado_ContadorFMNom_Link = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''}],oparms:[{ctrl:'BTNUPDATE',prop:'Enabled'},{ctrl:'BTNDELETE',prop:'Enabled'},{av:'edtContagemResultado_ContadorFMNom_Visible',ctrl:'CONTAGEMRESULTADO_CONTADORFMNOM',prop:'Visible'},{av:'lblTextblockcontagemresultado_contadorfmnom_Visible',ctrl:'TEXTBLOCKCONTAGEMRESULTADO_CONTADORFMNOM',prop:'Visible'}]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13BA2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'A511ContagemResultado_HoraCnt',fld:'CONTAGEMRESULTADO_HORACNT',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14BA2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'A511ContagemResultado_HoraCnt',fld:'CONTAGEMRESULTADO_HORACNT',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15BA2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOA473ContagemResultado_DataCnt = DateTime.MinValue;
         wcpOA511ContagemResultado_HoraCnt = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16Pgmname = "";
         scmdbuf = "";
         H00BA2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00BA2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A463ContagemResultado_ParecerTcn = "";
         A853ContagemResultado_NomePla = "";
         A854ContagemResultado_TipoPla = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00BA3_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00BA3_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00BA3_A456ContagemResultado_Codigo = new int[1] ;
         H00BA3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00BA3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00BA3_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00BA3_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         H00BA3_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         H00BA3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00BA3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00BA3_A482ContagemResultadoContagens_Esforco = new short[1] ;
         H00BA3_A483ContagemResultado_StatusCnt = new short[1] ;
         H00BA3_A462ContagemResultado_Divergencia = new decimal[1] ;
         H00BA3_A854ContagemResultado_TipoPla = new String[] {""} ;
         H00BA3_n854ContagemResultado_TipoPla = new bool[] {false} ;
         H00BA3_A853ContagemResultado_NomePla = new String[] {""} ;
         H00BA3_n853ContagemResultado_NomePla = new bool[] {false} ;
         H00BA3_A478ContagemResultado_NaoCnfCntNom = new String[] {""} ;
         H00BA3_n478ContagemResultado_NaoCnfCntNom = new bool[] {false} ;
         H00BA3_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         H00BA3_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         H00BA3_A461ContagemResultado_PFLFM = new decimal[1] ;
         H00BA3_n461ContagemResultado_PFLFM = new bool[] {false} ;
         H00BA3_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00BA3_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00BA3_A459ContagemResultado_PFLFS = new decimal[1] ;
         H00BA3_n459ContagemResultado_PFLFS = new bool[] {false} ;
         H00BA3_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00BA3_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00BA3_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00BA3_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         A478ContagemResultado_NaoCnfCntNom = "";
         A474ContagemResultado_ContadorFMNom = "";
         H00BA4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00BA4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         hsh = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV8ContagemResultado_DataCnt = DateTime.MinValue;
         AV13ContagemResultado_HoraCnt = context.localUtil.Time( );
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontagemresultado_datacnt_Jsonclick = "";
         lblTextblockcontagemresultado_contadorfmnom_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfs_Jsonclick = "";
         lblTextblockcontagemresultado_pflfs_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfm_Jsonclick = "";
         lblTextblockcontagemresultado_pflfm_Jsonclick = "";
         lblTextblockcontagemresultado_parecertcn_Jsonclick = "";
         lblTextblockcontagemresultado_naocnfcntnom_Jsonclick = "";
         lblTextblockcontagemresultado_nomepla_Jsonclick = "";
         lblTextblockcontagemresultado_tipopla_Jsonclick = "";
         lblTextblockcontagemresultado_divergencia_Jsonclick = "";
         lblTextblockcontagemresultado_statuscnt_Jsonclick = "";
         lblTextblockcontagemresultadocontagens_esforco_Jsonclick = "";
         lblContagemresultadocontagens_esforco_righttext_Jsonclick = "";
         lblContagemresultado_divergencia_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA456ContagemResultado_Codigo = "";
         sCtrlA473ContagemResultado_DataCnt = "";
         sCtrlA511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadocontagensgeneral__default(),
            new Object[][] {
                new Object[] {
               H00BA2_A484ContagemResultado_StatusDmn, H00BA2_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               H00BA3_A479ContagemResultado_CrFMPessoaCod, H00BA3_n479ContagemResultado_CrFMPessoaCod, H00BA3_A456ContagemResultado_Codigo, H00BA3_A473ContagemResultado_DataCnt, H00BA3_A511ContagemResultado_HoraCnt, H00BA3_A470ContagemResultado_ContadorFMCod, H00BA3_A469ContagemResultado_NaoCnfCntCod, H00BA3_n469ContagemResultado_NaoCnfCntCod, H00BA3_A484ContagemResultado_StatusDmn, H00BA3_n484ContagemResultado_StatusDmn,
               H00BA3_A482ContagemResultadoContagens_Esforco, H00BA3_A483ContagemResultado_StatusCnt, H00BA3_A462ContagemResultado_Divergencia, H00BA3_A854ContagemResultado_TipoPla, H00BA3_n854ContagemResultado_TipoPla, H00BA3_A853ContagemResultado_NomePla, H00BA3_n853ContagemResultado_NomePla, H00BA3_A478ContagemResultado_NaoCnfCntNom, H00BA3_n478ContagemResultado_NaoCnfCntNom, H00BA3_A463ContagemResultado_ParecerTcn,
               H00BA3_n463ContagemResultado_ParecerTcn, H00BA3_A461ContagemResultado_PFLFM, H00BA3_n461ContagemResultado_PFLFM, H00BA3_A460ContagemResultado_PFBFM, H00BA3_n460ContagemResultado_PFBFM, H00BA3_A459ContagemResultado_PFLFS, H00BA3_n459ContagemResultado_PFLFS, H00BA3_A458ContagemResultado_PFBFS, H00BA3_n458ContagemResultado_PFBFS, H00BA3_A474ContagemResultado_ContadorFMNom,
               H00BA3_n474ContagemResultado_ContadorFMNom
               }
               , new Object[] {
               H00BA4_A484ContagemResultado_StatusDmn, H00BA4_n484ContagemResultado_StatusDmn
               }
            }
         );
         AV16Pgmname = "ContagemResultadoContagensGeneral";
         /* GeneXus formulas. */
         AV16Pgmname = "ContagemResultadoContagensGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A456ContagemResultado_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int edtContagemResultado_Codigo_Visible ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContagemResultado_Codigo ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int edtContagemResultado_ContadorFMNom_Visible ;
      private int lblTextblockcontagemresultado_contadorfmnom_Visible ;
      private int idxLst ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private String A511ContagemResultado_HoraCnt ;
      private String wcpOA511ContagemResultado_HoraCnt ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV16Pgmname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A853ContagemResultado_NomePla ;
      private String A854ContagemResultado_TipoPla ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A478ContagemResultado_NaoCnfCntNom ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String edtContagemResultado_ContadorFMNom_Internalname ;
      private String edtContagemResultado_PFBFS_Internalname ;
      private String edtContagemResultado_PFLFS_Internalname ;
      private String edtContagemResultado_PFBFM_Internalname ;
      private String edtContagemResultado_PFLFM_Internalname ;
      private String edtContagemResultado_ParecerTcn_Internalname ;
      private String edtContagemResultado_NaoCnfCntNom_Internalname ;
      private String edtContagemResultado_NomePla_Internalname ;
      private String edtContagemResultado_TipoPla_Internalname ;
      private String edtContagemResultado_Divergencia_Internalname ;
      private String cmbContagemResultado_StatusCnt_Internalname ;
      private String edtContagemResultadoContagens_Esforco_Internalname ;
      private String hsh ;
      private String edtContagemResultado_ContadorFMNom_Link ;
      private String edtContagemResultado_NaoCnfCntNom_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String AV13ContagemResultado_HoraCnt ;
      private String lblTextblockcontagemresultado_contadorfmnom_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultado_datacnt_Internalname ;
      private String lblTextblockcontagemresultado_datacnt_Jsonclick ;
      private String lblTextblockcontagemresultado_contadorfmnom_Jsonclick ;
      private String edtContagemResultado_ContadorFMNom_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfs_Internalname ;
      private String lblTextblockcontagemresultado_pfbfs_Jsonclick ;
      private String edtContagemResultado_PFBFS_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfs_Internalname ;
      private String lblTextblockcontagemresultado_pflfs_Jsonclick ;
      private String edtContagemResultado_PFLFS_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfm_Internalname ;
      private String lblTextblockcontagemresultado_pfbfm_Jsonclick ;
      private String edtContagemResultado_PFBFM_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfm_Internalname ;
      private String lblTextblockcontagemresultado_pflfm_Jsonclick ;
      private String edtContagemResultado_PFLFM_Jsonclick ;
      private String lblTextblockcontagemresultado_parecertcn_Internalname ;
      private String lblTextblockcontagemresultado_parecertcn_Jsonclick ;
      private String lblTextblockcontagemresultado_naocnfcntnom_Internalname ;
      private String lblTextblockcontagemresultado_naocnfcntnom_Jsonclick ;
      private String edtContagemResultado_NaoCnfCntNom_Jsonclick ;
      private String lblTextblockcontagemresultado_nomepla_Internalname ;
      private String lblTextblockcontagemresultado_nomepla_Jsonclick ;
      private String edtContagemResultado_NomePla_Jsonclick ;
      private String lblTextblockcontagemresultado_tipopla_Internalname ;
      private String lblTextblockcontagemresultado_tipopla_Jsonclick ;
      private String edtContagemResultado_TipoPla_Jsonclick ;
      private String lblTextblockcontagemresultado_divergencia_Internalname ;
      private String lblTextblockcontagemresultado_divergencia_Jsonclick ;
      private String lblTextblockcontagemresultado_statuscnt_Internalname ;
      private String lblTextblockcontagemresultado_statuscnt_Jsonclick ;
      private String cmbContagemResultado_StatusCnt_Jsonclick ;
      private String lblTextblockcontagemresultadocontagens_esforco_Internalname ;
      private String lblTextblockcontagemresultadocontagens_esforco_Jsonclick ;
      private String tblTablemergedcontagemresultadocontagens_esforco_Internalname ;
      private String edtContagemResultadoContagens_Esforco_Jsonclick ;
      private String lblContagemresultadocontagens_esforco_righttext_Internalname ;
      private String lblContagemresultadocontagens_esforco_righttext_Jsonclick ;
      private String tblTablemergedcontagemresultado_divergencia_Internalname ;
      private String edtContagemResultado_Divergencia_Jsonclick ;
      private String lblContagemresultado_divergencia_righttext_Internalname ;
      private String lblContagemresultado_divergencia_righttext_Jsonclick ;
      private String tblTablemergedcontagemresultado_datacnt_Internalname ;
      private String edtContagemResultado_DataCnt_Internalname ;
      private String edtContagemResultado_DataCnt_Jsonclick ;
      private String edtContagemResultado_HoraCnt_Internalname ;
      private String edtContagemResultado_HoraCnt_Jsonclick ;
      private String sCtrlA456ContagemResultado_Codigo ;
      private String sCtrlA473ContagemResultado_DataCnt ;
      private String sCtrlA511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime wcpOA473ContagemResultado_DataCnt ;
      private DateTime AV8ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n854ContagemResultado_TipoPla ;
      private bool n853ContagemResultado_NomePla ;
      private bool n478ContagemResultado_NaoCnfCntNom ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool returnInSub ;
      private String A463ContagemResultado_ParecerTcn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultado_StatusCnt ;
      private IDataStoreProvider pr_default ;
      private String[] H00BA2_A484ContagemResultado_StatusDmn ;
      private bool[] H00BA2_n484ContagemResultado_StatusDmn ;
      private int[] H00BA3_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00BA3_n479ContagemResultado_CrFMPessoaCod ;
      private int[] H00BA3_A456ContagemResultado_Codigo ;
      private DateTime[] H00BA3_A473ContagemResultado_DataCnt ;
      private String[] H00BA3_A511ContagemResultado_HoraCnt ;
      private int[] H00BA3_A470ContagemResultado_ContadorFMCod ;
      private int[] H00BA3_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] H00BA3_n469ContagemResultado_NaoCnfCntCod ;
      private String[] H00BA3_A484ContagemResultado_StatusDmn ;
      private bool[] H00BA3_n484ContagemResultado_StatusDmn ;
      private short[] H00BA3_A482ContagemResultadoContagens_Esforco ;
      private short[] H00BA3_A483ContagemResultado_StatusCnt ;
      private decimal[] H00BA3_A462ContagemResultado_Divergencia ;
      private String[] H00BA3_A854ContagemResultado_TipoPla ;
      private bool[] H00BA3_n854ContagemResultado_TipoPla ;
      private String[] H00BA3_A853ContagemResultado_NomePla ;
      private bool[] H00BA3_n853ContagemResultado_NomePla ;
      private String[] H00BA3_A478ContagemResultado_NaoCnfCntNom ;
      private bool[] H00BA3_n478ContagemResultado_NaoCnfCntNom ;
      private String[] H00BA3_A463ContagemResultado_ParecerTcn ;
      private bool[] H00BA3_n463ContagemResultado_ParecerTcn ;
      private decimal[] H00BA3_A461ContagemResultado_PFLFM ;
      private bool[] H00BA3_n461ContagemResultado_PFLFM ;
      private decimal[] H00BA3_A460ContagemResultado_PFBFM ;
      private bool[] H00BA3_n460ContagemResultado_PFBFM ;
      private decimal[] H00BA3_A459ContagemResultado_PFLFS ;
      private bool[] H00BA3_n459ContagemResultado_PFLFS ;
      private decimal[] H00BA3_A458ContagemResultado_PFBFS ;
      private bool[] H00BA3_n458ContagemResultado_PFBFS ;
      private String[] H00BA3_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00BA3_n474ContagemResultado_ContadorFMNom ;
      private String[] H00BA4_A484ContagemResultado_StatusDmn ;
      private bool[] H00BA4_n484ContagemResultado_StatusDmn ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class contagemresultadocontagensgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BA2 ;
          prmH00BA2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BA3 ;
          prmH00BA3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmH00BA4 ;
          prmH00BA4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BA2", "SELECT [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BA2,1,0,true,true )
             ,new CursorDef("H00BA3", "SELECT T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T1.[ContagemResultado_NaoCnfCntCod] AS ContagemResultado_NaoCnfCntCod, T5.[ContagemResultado_StatusDmn], T1.[ContagemResultadoContagens_Esforco], T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_TipoPla], T1.[ContagemResultado_NomePla], T4.[NaoConformidade_Nome] AS ContagemResultado_NaoCnfCntNom, T1.[ContagemResultado_ParecerTcn], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM (((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN [NaoConformidade] T4 WITH (NOLOCK) ON T4.[NaoConformidade_Codigo] = T1.[ContagemResultado_NaoCnfCntCod]) INNER JOIN [ContagemResultado] T5 WITH (NOLOCK) ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo and T1.[ContagemResultado_DataCnt] = @ContagemResultado_DataCnt and T1.[ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt ORDER BY T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BA3,1,0,true,true )
             ,new CursorDef("H00BA4", "SELECT [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BA4,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((short[]) buf[10])[0] = rslt.getShort(8) ;
                ((short[]) buf[11])[0] = rslt.getShort(9) ;
                ((decimal[]) buf[12])[0] = rslt.getDecimal(10) ;
                ((String[]) buf[13])[0] = rslt.getString(11, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((String[]) buf[15])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                ((String[]) buf[17])[0] = rslt.getString(13, 50) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(13);
                ((String[]) buf[19])[0] = rslt.getLongVarchar(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(17);
                ((decimal[]) buf[27])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(18);
                ((String[]) buf[29])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
