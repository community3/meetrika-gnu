/*
               File: type_SdtSDT_RedmineStatus_issue_status
        Description: SDT_RedmineStatus
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:5.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RedmineStatus.issue_status" )]
   [XmlType(TypeName =  "SDT_RedmineStatus.issue_status" , Namespace = "" )]
   [Serializable]
   public class SdtSDT_RedmineStatus_issue_status : GxUserType
   {
      public SdtSDT_RedmineStatus_issue_status( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineStatus_issue_status_Name = "";
         gxTv_SdtSDT_RedmineStatus_issue_status_Is_default = "";
         gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed = "";
      }

      public SdtSDT_RedmineStatus_issue_status( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineStatus_issue_status deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineStatus_issue_status)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineStatus_issue_status obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Is_default = deserialized.gxTpr_Is_default;
         obj.gxTpr_Is_closed = deserialized.gxTpr_Is_closed;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineStatus_issue_status_Id = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "name") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineStatus_issue_status_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "is_default") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineStatus_issue_status_Is_default = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "is_closed") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RedmineStatus.issue_status";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineStatus_issue_status_Id), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("name", StringUtil.RTrim( gxTv_SdtSDT_RedmineStatus_issue_status_Name));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("is_default", StringUtil.RTrim( gxTv_SdtSDT_RedmineStatus_issue_status_Is_default));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("is_closed", StringUtil.RTrim( gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_RedmineStatus_issue_status_Id, false);
         AddObjectProperty("name", gxTv_SdtSDT_RedmineStatus_issue_status_Name, false);
         AddObjectProperty("is_default", gxTv_SdtSDT_RedmineStatus_issue_status_Is_default, false);
         AddObjectProperty("is_closed", gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed, false);
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id" , Namespace = ""  )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_RedmineStatus_issue_status_Id ;
         }

         set {
            gxTv_SdtSDT_RedmineStatus_issue_status_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "name" )]
      [  XmlElement( ElementName = "name" , Namespace = ""  )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtSDT_RedmineStatus_issue_status_Name ;
         }

         set {
            gxTv_SdtSDT_RedmineStatus_issue_status_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "is_default" )]
      [  XmlElement( ElementName = "is_default" , Namespace = ""  )]
      public String gxTpr_Is_default
      {
         get {
            return gxTv_SdtSDT_RedmineStatus_issue_status_Is_default ;
         }

         set {
            gxTv_SdtSDT_RedmineStatus_issue_status_Is_default = (String)(value);
         }

      }

      [  SoapElement( ElementName = "is_closed" )]
      [  XmlElement( ElementName = "is_closed" , Namespace = ""  )]
      public String gxTpr_Is_closed
      {
         get {
            return gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed ;
         }

         set {
            gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineStatus_issue_status_Name = "";
         gxTv_SdtSDT_RedmineStatus_issue_status_Is_default = "";
         gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_RedmineStatus_issue_status_Id ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineStatus_issue_status_Name ;
      protected String gxTv_SdtSDT_RedmineStatus_issue_status_Is_default ;
      protected String gxTv_SdtSDT_RedmineStatus_issue_status_Is_closed ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_RedmineStatus.issue_status", Namespace = "")]
   public class SdtSDT_RedmineStatus_issue_status_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineStatus_issue_status>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineStatus_issue_status_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineStatus_issue_status_RESTInterface( SdtSDT_RedmineStatus_issue_status psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "name" , Order = 1 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "is_default" , Order = 2 )]
      public String gxTpr_Is_default
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Is_default) ;
         }

         set {
            sdt.gxTpr_Is_default = (String)(value);
         }

      }

      [DataMember( Name = "is_closed" , Order = 3 )]
      public String gxTpr_Is_closed
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Is_closed) ;
         }

         set {
            sdt.gxTpr_Is_closed = (String)(value);
         }

      }

      public SdtSDT_RedmineStatus_issue_status sdt
      {
         get {
            return (SdtSDT_RedmineStatus_issue_status)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineStatus_issue_status() ;
         }
      }

   }

}
