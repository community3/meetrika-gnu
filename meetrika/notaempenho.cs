/*
               File: NotaEmpenho
        Description: Nota Empenho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:42:49.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class notaempenho : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action18") == 0 )
         {
            A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            A1560NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
            A1566NotaEmpenho_Valor = NumberUtil.Val( GetNextPar( ), ".");
            n1566NotaEmpenho_Valor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_18_3Y178( A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo, A1566NotaEmpenho_Valor) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action19") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_19_3Y178( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action20") == 0 )
         {
            A1560NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
            A1566NotaEmpenho_Valor = NumberUtil.Val( GetNextPar( ), ".");
            n1566NotaEmpenho_Valor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_20_3Y178( A1560NotaEmpenho_Codigo, A1566NotaEmpenho_Valor) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SALDOCONTRATO_CODIGO") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDSASALDOCONTRATO_CODIGO3Y178( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_22") == 0 )
         {
            A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_22( A1561SaldoContrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7NotaEmpenho_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNOTAEMPENHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7NotaEmpenho_Codigo), "ZZZZZ9")));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynSaldoContrato_Codigo.Name = "SALDOCONTRATO_CODIGO";
         dynSaldoContrato_Codigo.WebTags = "";
         chkNotaEmpenho_Ativo.Name = "NOTAEMPENHO_ATIVO";
         chkNotaEmpenho_Ativo.WebTags = "";
         chkNotaEmpenho_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNotaEmpenho_Ativo_Internalname, "TitleCaption", chkNotaEmpenho_Ativo.Caption);
         chkNotaEmpenho_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Nota Empenho", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public notaempenho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public notaempenho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_NotaEmpenho_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7NotaEmpenho_Codigo = aP1_NotaEmpenho_Codigo;
         this.A74Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.A74Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynSaldoContrato_Codigo = new GXCombobox();
         chkNotaEmpenho_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynSaldoContrato_Codigo.ItemCount > 0 )
         {
            A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( dynSaldoContrato_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3Y178( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3Y178e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3Y178( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3Y178( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3Y178e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_69_3Y178( true) ;
         }
         return  ;
      }

      protected void wb_table3_69_3Y178e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3Y178e( true) ;
         }
         else
         {
            wb_table1_2_3Y178e( false) ;
         }
      }

      protected void wb_table3_69_3Y178( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_69_3Y178e( true) ;
         }
         else
         {
            wb_table3_69_3Y178e( false) ;
         }
      }

      protected void wb_table2_5_3Y178( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_3Y178( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_3Y178e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLESALDOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLESALDOContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table5_57_3Y178( true) ;
         }
         return  ;
      }

      protected void wb_table5_57_3Y178e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3Y178e( true) ;
         }
         else
         {
            wb_table2_5_3Y178e( false) ;
         }
      }

      protected void wb_table5_57_3Y178( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesaldo_Internalname, tblTablesaldo_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_saldoant_Internalname, "Saldo Anterior", "", "", lblTextblocknotaempenho_saldoant_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavNotaempenho_saldoant_Internalname, StringUtil.LTrim( StringUtil.NToC( AV13NotaEmpenho_SaldoAnt, 18, 5, ",", "")), ((edtavNotaempenho_saldoant_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV13NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV13NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNotaempenho_saldoant_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavNotaempenho_saldoant_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_saldopos_Internalname, "Saldo Posterior", "", "", lblTextblocknotaempenho_saldopos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavNotaempenho_saldopos_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14NotaEmpenho_SaldoPos, 18, 5, ",", "")), ((edtavNotaempenho_saldopos_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV14NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNotaempenho_saldopos_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavNotaempenho_saldopos_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_57_3Y178e( true) ;
         }
         else
         {
            wb_table5_57_3Y178e( false) ;
         }
      }

      protected void wb_table4_13_3Y178( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_codigo_Internalname, "C�digo", "", "", lblTextblocknotaempenho_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ",", "")), ((edtNotaEmpenho_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtNotaEmpenho_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_codigo_Internalname, "Saldo Contrato", "", "", lblTextblocksaldocontrato_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynSaldoContrato_Codigo, dynSaldoContrato_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)), 1, dynSaldoContrato_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynSaldoContrato_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_NotaEmpenho.htm");
            dynSaldoContrato_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSaldoContrato_Codigo_Internalname, "Values", (String)(dynSaldoContrato_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_itentificador_Internalname, "Identificador", "", "", lblTextblocknotaempenho_itentificador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Itentificador_Internalname, StringUtil.RTrim( A1564NotaEmpenho_Itentificador), StringUtil.RTrim( context.localUtil.Format( A1564NotaEmpenho_Itentificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Itentificador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtNotaEmpenho_Itentificador_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_demissao_Internalname, "Emiss�o", "", "", lblTextblocknotaempenho_demissao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtNotaEmpenho_DEmissao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_DEmissao_Internalname, context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1565NotaEmpenho_DEmissao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_DEmissao_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtNotaEmpenho_DEmissao_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_NotaEmpenho.htm");
            GxWebStd.gx_bitmap( context, edtNotaEmpenho_DEmissao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtNotaEmpenho_DEmissao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_NotaEmpenho.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_qtd_Internalname, "Quantidade", "", "", lblTextblocknotaempenho_qtd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Qtd_Internalname, StringUtil.LTrim( StringUtil.NToC( A1567NotaEmpenho_Qtd, 14, 5, ",", "")), ((edtNotaEmpenho_Qtd_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1567NotaEmpenho_Qtd, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Qtd_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtNotaEmpenho_Qtd_Enabled, 0, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_valor_Internalname, "Valor", "", "", lblTextblocknotaempenho_valor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtNotaEmpenho_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A1566NotaEmpenho_Valor, 18, 5, ",", "")), ((edtNotaEmpenho_Valor_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1566NotaEmpenho_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtNotaEmpenho_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtNotaEmpenho_Valor_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknotaempenho_ativo_Internalname, "Ativo", "", "", lblTextblocknotaempenho_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_NotaEmpenho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkNotaEmpenho_Ativo_Internalname, StringUtil.BoolToStr( A1570NotaEmpenho_Ativo), "", "", 1, chkNotaEmpenho_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(50, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_3Y178e( true) ;
         }
         else
         {
            wb_table4_13_3Y178e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113Y2 */
         E113Y2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNotaEmpenho_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
               dynSaldoContrato_Codigo.CurrentValue = cgiGet( dynSaldoContrato_Codigo_Internalname);
               A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( cgiGet( dynSaldoContrato_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
               A1564NotaEmpenho_Itentificador = cgiGet( edtNotaEmpenho_Itentificador_Internalname);
               n1564NotaEmpenho_Itentificador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1564NotaEmpenho_Itentificador", A1564NotaEmpenho_Itentificador);
               n1564NotaEmpenho_Itentificador = (String.IsNullOrEmpty(StringUtil.RTrim( A1564NotaEmpenho_Itentificador)) ? true : false);
               if ( context.localUtil.VCDateTime( cgiGet( edtNotaEmpenho_DEmissao_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data Emiss�o"}), 1, "NOTAEMPENHO_DEMISSAO");
                  AnyError = 1;
                  GX_FocusControl = edtNotaEmpenho_DEmissao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
                  n1565NotaEmpenho_DEmissao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1565NotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( edtNotaEmpenho_DEmissao_Internalname));
                  n1565NotaEmpenho_DEmissao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
               }
               n1565NotaEmpenho_DEmissao = ((DateTime.MinValue==A1565NotaEmpenho_DEmissao) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtNotaEmpenho_Qtd_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtNotaEmpenho_Qtd_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "NOTAEMPENHO_QTD");
                  AnyError = 1;
                  GX_FocusControl = edtNotaEmpenho_Qtd_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1567NotaEmpenho_Qtd = 0;
                  n1567NotaEmpenho_Qtd = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( A1567NotaEmpenho_Qtd, 14, 5)));
               }
               else
               {
                  A1567NotaEmpenho_Qtd = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Qtd_Internalname), ",", ".");
                  n1567NotaEmpenho_Qtd = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( A1567NotaEmpenho_Qtd, 14, 5)));
               }
               n1567NotaEmpenho_Qtd = ((Convert.ToDecimal(0)==A1567NotaEmpenho_Qtd) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtNotaEmpenho_Valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtNotaEmpenho_Valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "NOTAEMPENHO_VALOR");
                  AnyError = 1;
                  GX_FocusControl = edtNotaEmpenho_Valor_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1566NotaEmpenho_Valor = 0;
                  n1566NotaEmpenho_Valor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
               }
               else
               {
                  A1566NotaEmpenho_Valor = context.localUtil.CToN( cgiGet( edtNotaEmpenho_Valor_Internalname), ",", ".");
                  n1566NotaEmpenho_Valor = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
               }
               n1566NotaEmpenho_Valor = ((Convert.ToDecimal(0)==A1566NotaEmpenho_Valor) ? true : false);
               A1570NotaEmpenho_Ativo = StringUtil.StrToBool( cgiGet( chkNotaEmpenho_Ativo_Internalname));
               n1570NotaEmpenho_Ativo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
               n1570NotaEmpenho_Ativo = ((false==A1570NotaEmpenho_Ativo) ? true : false);
               AV13NotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( edtavNotaempenho_saldoant_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV13NotaEmpenho_SaldoAnt, 18, 5)));
               AV14NotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( edtavNotaempenho_saldopos_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV14NotaEmpenho_SaldoPos, 18, 5)));
               /* Read saved values. */
               Z1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1560NotaEmpenho_Codigo"), ",", "."));
               Z1568NotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( "Z1568NotaEmpenho_SaldoAnt"), ",", ".");
               n1568NotaEmpenho_SaldoAnt = ((Convert.ToDecimal(0)==A1568NotaEmpenho_SaldoAnt) ? true : false);
               Z1569NotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( "Z1569NotaEmpenho_SaldoPos"), ",", ".");
               n1569NotaEmpenho_SaldoPos = ((Convert.ToDecimal(0)==A1569NotaEmpenho_SaldoPos) ? true : false);
               Z1564NotaEmpenho_Itentificador = cgiGet( "Z1564NotaEmpenho_Itentificador");
               n1564NotaEmpenho_Itentificador = (String.IsNullOrEmpty(StringUtil.RTrim( A1564NotaEmpenho_Itentificador)) ? true : false);
               Z1565NotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( "Z1565NotaEmpenho_DEmissao"), 0);
               n1565NotaEmpenho_DEmissao = ((DateTime.MinValue==A1565NotaEmpenho_DEmissao) ? true : false);
               Z1566NotaEmpenho_Valor = context.localUtil.CToN( cgiGet( "Z1566NotaEmpenho_Valor"), ",", ".");
               n1566NotaEmpenho_Valor = ((Convert.ToDecimal(0)==A1566NotaEmpenho_Valor) ? true : false);
               Z1567NotaEmpenho_Qtd = context.localUtil.CToN( cgiGet( "Z1567NotaEmpenho_Qtd"), ",", ".");
               n1567NotaEmpenho_Qtd = ((Convert.ToDecimal(0)==A1567NotaEmpenho_Qtd) ? true : false);
               Z1570NotaEmpenho_Ativo = StringUtil.StrToBool( cgiGet( "Z1570NotaEmpenho_Ativo"));
               n1570NotaEmpenho_Ativo = ((false==A1570NotaEmpenho_Ativo) ? true : false);
               Z1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1561SaldoContrato_Codigo"), ",", "."));
               A1568NotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( "Z1568NotaEmpenho_SaldoAnt"), ",", ".");
               n1568NotaEmpenho_SaldoAnt = false;
               n1568NotaEmpenho_SaldoAnt = ((Convert.ToDecimal(0)==A1568NotaEmpenho_SaldoAnt) ? true : false);
               A1569NotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( "Z1569NotaEmpenho_SaldoPos"), ",", ".");
               n1569NotaEmpenho_SaldoPos = false;
               n1569NotaEmpenho_SaldoPos = ((Convert.ToDecimal(0)==A1569NotaEmpenho_SaldoPos) ? true : false);
               O1566NotaEmpenho_Valor = context.localUtil.CToN( cgiGet( "O1566NotaEmpenho_Valor"), ",", ".");
               n1566NotaEmpenho_Valor = ((Convert.ToDecimal(0)==A1566NotaEmpenho_Valor) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1561SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N1561SaldoContrato_Codigo"), ",", "."));
               AV7NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( "vNOTAEMPENHO_CODIGO"), ",", "."));
               AV11Insert_SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SALDOCONTRATO_CODIGO"), ",", "."));
               A1568NotaEmpenho_SaldoAnt = context.localUtil.CToN( cgiGet( "NOTAEMPENHO_SALDOANT"), ",", ".");
               n1568NotaEmpenho_SaldoAnt = ((Convert.ToDecimal(0)==A1568NotaEmpenho_SaldoAnt) ? true : false);
               A1569NotaEmpenho_SaldoPos = context.localUtil.CToN( cgiGet( "NOTAEMPENHO_SALDOPOS"), ",", ".");
               n1569NotaEmpenho_SaldoPos = ((Convert.ToDecimal(0)==A1569NotaEmpenho_SaldoPos) ? true : false);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               A1576SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( "SALDOCONTRATO_SALDO"), ",", ".");
               AV16Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               Dvpanel_tablesaldo_Width = cgiGet( "DVPANEL_TABLESALDO_Width");
               Dvpanel_tablesaldo_Height = cgiGet( "DVPANEL_TABLESALDO_Height");
               Dvpanel_tablesaldo_Cls = cgiGet( "DVPANEL_TABLESALDO_Cls");
               Dvpanel_tablesaldo_Title = cgiGet( "DVPANEL_TABLESALDO_Title");
               Dvpanel_tablesaldo_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Collapsible"));
               Dvpanel_tablesaldo_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Collapsed"));
               Dvpanel_tablesaldo_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Enabled"));
               Dvpanel_tablesaldo_Class = cgiGet( "DVPANEL_TABLESALDO_Class");
               Dvpanel_tablesaldo_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Autowidth"));
               Dvpanel_tablesaldo_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Autoheight"));
               Dvpanel_tablesaldo_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Showheader"));
               Dvpanel_tablesaldo_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Showcollapseicon"));
               Dvpanel_tablesaldo_Iconposition = cgiGet( "DVPANEL_TABLESALDO_Iconposition");
               Dvpanel_tablesaldo_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Autoscroll"));
               Dvpanel_tablesaldo_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLESALDO_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "NotaEmpenho";
               A1560NotaEmpenho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtNotaEmpenho_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1560NotaEmpenho_Codigo != Z1560NotaEmpenho_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("notaempenho:[SecurityCheckFailed value for]"+"NotaEmpenho_Codigo:"+context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("notaempenho:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("notaempenho:[SecurityCheckFailed value for]"+"NotaEmpenho_SaldoAnt:"+context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GXUtil.WriteLog("notaempenho:[SecurityCheckFailed value for]"+"NotaEmpenho_SaldoPos:"+context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1560NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode178 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode178;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound178 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3Y0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "NOTAEMPENHO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtNotaEmpenho_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113Y2 */
                           E113Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123Y2 */
                           E123Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SALDOCONTRATO_CODIGO.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E133Y2 */
                           E133Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NOTAEMPENHO_VALOR.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E143Y2 */
                           E143Y2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123Y2 */
            E123Y2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3Y178( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3Y178( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaempenho_saldoant_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_saldoant_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaempenho_saldopos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_saldopos_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3Y0( )
      {
         BeforeValidate3Y178( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3Y178( ) ;
            }
            else
            {
               CheckExtendedTable3Y178( ) ;
               CloseExtendedTableCursors3Y178( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3Y0( )
      {
      }

      protected void E113Y2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV16Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV17GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            while ( AV17GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV17GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "SaldoContrato_Codigo") == 0 )
               {
                  AV11Insert_SaldoContrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_SaldoContrato_Codigo), 6, 0)));
               }
               AV17GXV1 = (int)(AV17GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GXV1), 8, 0)));
            }
         }
         GXt_decimal1 = AV13NotaEmpenho_SaldoAnt;
         new prc_notaempenhosaldo(context ).execute(  A1560NotaEmpenho_Codigo,  A1561SaldoContrato_Codigo, out  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         AV13NotaEmpenho_SaldoAnt = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV13NotaEmpenho_SaldoAnt, 18, 5)));
         AV14NotaEmpenho_SaldoPos = A1569NotaEmpenho_SaldoPos;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV14NotaEmpenho_SaldoPos, 18, 5)));
      }

      protected void E123Y2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwnotaempenho.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A74Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E133Y2( )
      {
         /* SaldoContrato_Codigo_Isvalid Routine */
         GXt_decimal1 = AV13NotaEmpenho_SaldoAnt;
         new prc_notaempenhosaldo(context ).execute(  A1560NotaEmpenho_Codigo,  A1561SaldoContrato_Codigo, out  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         AV13NotaEmpenho_SaldoAnt = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV13NotaEmpenho_SaldoAnt, 18, 5)));
         AV14NotaEmpenho_SaldoPos = (decimal)(AV13NotaEmpenho_SaldoAnt+A1566NotaEmpenho_Valor);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV14NotaEmpenho_SaldoPos, 18, 5)));
      }

      protected void E143Y2( )
      {
         /* NotaEmpenho_Valor_Isvalid Routine */
         GXt_decimal1 = AV13NotaEmpenho_SaldoAnt;
         new prc_notaempenhosaldo(context ).execute(  A1560NotaEmpenho_Codigo,  A1561SaldoContrato_Codigo, out  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         AV13NotaEmpenho_SaldoAnt = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( AV13NotaEmpenho_SaldoAnt, 18, 5)));
         AV14NotaEmpenho_SaldoPos = (decimal)(AV13NotaEmpenho_SaldoAnt+A1566NotaEmpenho_Valor);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( AV14NotaEmpenho_SaldoPos, 18, 5)));
      }

      protected void ZM3Y178( short GX_JID )
      {
         if ( ( GX_JID == 21 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1568NotaEmpenho_SaldoAnt = T003Y3_A1568NotaEmpenho_SaldoAnt[0];
               Z1569NotaEmpenho_SaldoPos = T003Y3_A1569NotaEmpenho_SaldoPos[0];
               Z1564NotaEmpenho_Itentificador = T003Y3_A1564NotaEmpenho_Itentificador[0];
               Z1565NotaEmpenho_DEmissao = T003Y3_A1565NotaEmpenho_DEmissao[0];
               Z1566NotaEmpenho_Valor = T003Y3_A1566NotaEmpenho_Valor[0];
               Z1567NotaEmpenho_Qtd = T003Y3_A1567NotaEmpenho_Qtd[0];
               Z1570NotaEmpenho_Ativo = T003Y3_A1570NotaEmpenho_Ativo[0];
               Z1561SaldoContrato_Codigo = T003Y3_A1561SaldoContrato_Codigo[0];
            }
            else
            {
               Z1568NotaEmpenho_SaldoAnt = A1568NotaEmpenho_SaldoAnt;
               Z1569NotaEmpenho_SaldoPos = A1569NotaEmpenho_SaldoPos;
               Z1564NotaEmpenho_Itentificador = A1564NotaEmpenho_Itentificador;
               Z1565NotaEmpenho_DEmissao = A1565NotaEmpenho_DEmissao;
               Z1566NotaEmpenho_Valor = A1566NotaEmpenho_Valor;
               Z1567NotaEmpenho_Qtd = A1567NotaEmpenho_Qtd;
               Z1570NotaEmpenho_Ativo = A1570NotaEmpenho_Ativo;
               Z1561SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            }
         }
         if ( GX_JID == -21 )
         {
            Z1560NotaEmpenho_Codigo = A1560NotaEmpenho_Codigo;
            Z1568NotaEmpenho_SaldoAnt = A1568NotaEmpenho_SaldoAnt;
            Z1569NotaEmpenho_SaldoPos = A1569NotaEmpenho_SaldoPos;
            Z1564NotaEmpenho_Itentificador = A1564NotaEmpenho_Itentificador;
            Z1565NotaEmpenho_DEmissao = A1565NotaEmpenho_DEmissao;
            Z1566NotaEmpenho_Valor = A1566NotaEmpenho_Valor;
            Z1567NotaEmpenho_Qtd = A1567NotaEmpenho_Qtd;
            Z1570NotaEmpenho_Ativo = A1570NotaEmpenho_Ativo;
            Z1561SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z1576SaldoContrato_Saldo = A1576SaldoContrato_Saldo;
         }
      }

      protected void standaloneNotModal( )
      {
         edtNotaEmpenho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNotaEmpenho_Codigo_Enabled), 5, 0)));
         AV16Pgmname = "NotaEmpenho";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Pgmname", AV16Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtNotaEmpenho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNotaEmpenho_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7NotaEmpenho_Codigo) )
         {
            A1560NotaEmpenho_Codigo = AV7NotaEmpenho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         }
         GXASALDOCONTRATO_CODIGO_html3Y178( A74Contrato_Codigo) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_SaldoContrato_Codigo) )
         {
            dynSaldoContrato_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSaldoContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSaldoContrato_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynSaldoContrato_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSaldoContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSaldoContrato_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            A1569NotaEmpenho_SaldoPos = AV14NotaEmpenho_SaldoPos;
            n1569NotaEmpenho_SaldoPos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1569NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( A1569NotaEmpenho_SaldoPos, 18, 5)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            A1568NotaEmpenho_SaldoAnt = AV13NotaEmpenho_SaldoAnt;
            n1568NotaEmpenho_SaldoAnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1568NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( A1568NotaEmpenho_SaldoAnt, 18, 5)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_SaldoContrato_Codigo) )
         {
            A1561SaldoContrato_Codigo = AV11Insert_SaldoContrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1570NotaEmpenho_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A1570NotaEmpenho_Ativo = true;
            n1570NotaEmpenho_Ativo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003Y4 */
            pr_default.execute(2, new Object[] {A1561SaldoContrato_Codigo});
            A1576SaldoContrato_Saldo = T003Y4_A1576SaldoContrato_Saldo[0];
            pr_default.close(2);
         }
      }

      protected void Load3Y178( )
      {
         /* Using cursor T003Y5 */
         pr_default.execute(3, new Object[] {A1560NotaEmpenho_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound178 = 1;
            A1568NotaEmpenho_SaldoAnt = T003Y5_A1568NotaEmpenho_SaldoAnt[0];
            n1568NotaEmpenho_SaldoAnt = T003Y5_n1568NotaEmpenho_SaldoAnt[0];
            A1569NotaEmpenho_SaldoPos = T003Y5_A1569NotaEmpenho_SaldoPos[0];
            n1569NotaEmpenho_SaldoPos = T003Y5_n1569NotaEmpenho_SaldoPos[0];
            A1576SaldoContrato_Saldo = T003Y5_A1576SaldoContrato_Saldo[0];
            A1564NotaEmpenho_Itentificador = T003Y5_A1564NotaEmpenho_Itentificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1564NotaEmpenho_Itentificador", A1564NotaEmpenho_Itentificador);
            n1564NotaEmpenho_Itentificador = T003Y5_n1564NotaEmpenho_Itentificador[0];
            A1565NotaEmpenho_DEmissao = T003Y5_A1565NotaEmpenho_DEmissao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            n1565NotaEmpenho_DEmissao = T003Y5_n1565NotaEmpenho_DEmissao[0];
            A1566NotaEmpenho_Valor = T003Y5_A1566NotaEmpenho_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
            n1566NotaEmpenho_Valor = T003Y5_n1566NotaEmpenho_Valor[0];
            A1567NotaEmpenho_Qtd = T003Y5_A1567NotaEmpenho_Qtd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( A1567NotaEmpenho_Qtd, 14, 5)));
            n1567NotaEmpenho_Qtd = T003Y5_n1567NotaEmpenho_Qtd[0];
            A1570NotaEmpenho_Ativo = T003Y5_A1570NotaEmpenho_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
            n1570NotaEmpenho_Ativo = T003Y5_n1570NotaEmpenho_Ativo[0];
            A1561SaldoContrato_Codigo = T003Y5_A1561SaldoContrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            ZM3Y178( -21) ;
         }
         pr_default.close(3);
         OnLoadActions3Y178( ) ;
      }

      protected void OnLoadActions3Y178( )
      {
      }

      protected void CheckExtendedTable3Y178( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T003Y4 */
         pr_default.execute(2, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T179'.", "ForeignKeyNotFound", 1, "SALDOCONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1576SaldoContrato_Saldo = T003Y4_A1576SaldoContrato_Saldo[0];
         pr_default.close(2);
         if ( (0==A1561SaldoContrato_Codigo) )
         {
            GX_msglist.addItem("Saldo Contrato � obrigat�rio.", 1, "SALDOCONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1564NotaEmpenho_Itentificador)) )
         {
            GX_msglist.addItem("Identificador � obrigat�rio.", 1, "NOTAEMPENHO_ITENTIFICADOR");
            AnyError = 1;
            GX_FocusControl = edtNotaEmpenho_Itentificador_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1565NotaEmpenho_DEmissao) || ( A1565NotaEmpenho_DEmissao >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data Emiss�o fora do intervalo", "OutOfRange", 1, "NOTAEMPENHO_DEMISSAO");
            AnyError = 1;
            GX_FocusControl = edtNotaEmpenho_DEmissao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A1565NotaEmpenho_DEmissao) )
         {
            GX_msglist.addItem("Data Emiss�o � obrigat�rio.", 1, "NOTAEMPENHO_DEMISSAO");
            AnyError = 1;
            GX_FocusControl = edtNotaEmpenho_DEmissao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (Convert.ToDecimal(0)==A1566NotaEmpenho_Valor) )
         {
            GX_msglist.addItem("Valor � obrigat�rio.", 1, "NOTAEMPENHO_VALOR");
            AnyError = 1;
            GX_FocusControl = edtNotaEmpenho_Valor_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (false==A1570NotaEmpenho_Ativo) )
         {
            GX_msglist.addItem("Ativo � obrigat�rio.", 1, "NOTAEMPENHO_ATIVO");
            AnyError = 1;
            GX_FocusControl = chkNotaEmpenho_Ativo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3Y178( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_22( int A1561SaldoContrato_Codigo )
      {
         /* Using cursor T003Y6 */
         pr_default.execute(4, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T179'.", "ForeignKeyNotFound", 1, "SALDOCONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A74Contrato_Codigo = T003Y6_A74Contrato_Codigo[0];
         A1576SaldoContrato_Saldo = T003Y6_A1576SaldoContrato_Saldo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3Y178( )
      {
         /* Using cursor T003Y7 */
         pr_default.execute(5, new Object[] {A1560NotaEmpenho_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound178 = 1;
         }
         else
         {
            RcdFound178 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003Y3 */
         pr_default.execute(1, new Object[] {A1560NotaEmpenho_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3Y178( 21) ;
            RcdFound178 = 1;
            A1560NotaEmpenho_Codigo = T003Y3_A1560NotaEmpenho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
            A1568NotaEmpenho_SaldoAnt = T003Y3_A1568NotaEmpenho_SaldoAnt[0];
            n1568NotaEmpenho_SaldoAnt = T003Y3_n1568NotaEmpenho_SaldoAnt[0];
            A1569NotaEmpenho_SaldoPos = T003Y3_A1569NotaEmpenho_SaldoPos[0];
            n1569NotaEmpenho_SaldoPos = T003Y3_n1569NotaEmpenho_SaldoPos[0];
            A1564NotaEmpenho_Itentificador = T003Y3_A1564NotaEmpenho_Itentificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1564NotaEmpenho_Itentificador", A1564NotaEmpenho_Itentificador);
            n1564NotaEmpenho_Itentificador = T003Y3_n1564NotaEmpenho_Itentificador[0];
            A1565NotaEmpenho_DEmissao = T003Y3_A1565NotaEmpenho_DEmissao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            n1565NotaEmpenho_DEmissao = T003Y3_n1565NotaEmpenho_DEmissao[0];
            A1566NotaEmpenho_Valor = T003Y3_A1566NotaEmpenho_Valor[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
            n1566NotaEmpenho_Valor = T003Y3_n1566NotaEmpenho_Valor[0];
            A1567NotaEmpenho_Qtd = T003Y3_A1567NotaEmpenho_Qtd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( A1567NotaEmpenho_Qtd, 14, 5)));
            n1567NotaEmpenho_Qtd = T003Y3_n1567NotaEmpenho_Qtd[0];
            A1570NotaEmpenho_Ativo = T003Y3_A1570NotaEmpenho_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
            n1570NotaEmpenho_Ativo = T003Y3_n1570NotaEmpenho_Ativo[0];
            A1561SaldoContrato_Codigo = T003Y3_A1561SaldoContrato_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            O1566NotaEmpenho_Valor = A1566NotaEmpenho_Valor;
            n1566NotaEmpenho_Valor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
            Z1560NotaEmpenho_Codigo = A1560NotaEmpenho_Codigo;
            sMode178 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3Y178( ) ;
            if ( AnyError == 1 )
            {
               RcdFound178 = 0;
               InitializeNonKey3Y178( ) ;
            }
            Gx_mode = sMode178;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound178 = 0;
            InitializeNonKey3Y178( ) ;
            sMode178 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode178;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3Y178( ) ;
         if ( RcdFound178 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound178 = 0;
         /* Using cursor T003Y8 */
         pr_default.execute(6, new Object[] {A1560NotaEmpenho_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003Y8_A1560NotaEmpenho_Codigo[0] < A1560NotaEmpenho_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003Y8_A1560NotaEmpenho_Codigo[0] > A1560NotaEmpenho_Codigo ) ) )
            {
               A1560NotaEmpenho_Codigo = T003Y8_A1560NotaEmpenho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
               RcdFound178 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound178 = 0;
         /* Using cursor T003Y9 */
         pr_default.execute(7, new Object[] {A1560NotaEmpenho_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003Y9_A1560NotaEmpenho_Codigo[0] > A1560NotaEmpenho_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003Y9_A1560NotaEmpenho_Codigo[0] < A1560NotaEmpenho_Codigo ) ) )
            {
               A1560NotaEmpenho_Codigo = T003Y9_A1560NotaEmpenho_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
               RcdFound178 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3Y178( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3Y178( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound178 == 1 )
            {
               if ( A1560NotaEmpenho_Codigo != Z1560NotaEmpenho_Codigo )
               {
                  A1560NotaEmpenho_Codigo = Z1560NotaEmpenho_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "NOTAEMPENHO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtNotaEmpenho_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3Y178( ) ;
                  GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1560NotaEmpenho_Codigo != Z1560NotaEmpenho_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3Y178( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "NOTAEMPENHO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtNotaEmpenho_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3Y178( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1560NotaEmpenho_Codigo != Z1560NotaEmpenho_Codigo )
         {
            A1560NotaEmpenho_Codigo = Z1560NotaEmpenho_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "NOTAEMPENHO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtNotaEmpenho_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3Y178( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003Y2 */
            pr_default.execute(0, new Object[] {A1560NotaEmpenho_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"NotaEmpenho"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1568NotaEmpenho_SaldoAnt != T003Y2_A1568NotaEmpenho_SaldoAnt[0] ) || ( Z1569NotaEmpenho_SaldoPos != T003Y2_A1569NotaEmpenho_SaldoPos[0] ) || ( StringUtil.StrCmp(Z1564NotaEmpenho_Itentificador, T003Y2_A1564NotaEmpenho_Itentificador[0]) != 0 ) || ( Z1565NotaEmpenho_DEmissao != T003Y2_A1565NotaEmpenho_DEmissao[0] ) || ( Z1566NotaEmpenho_Valor != T003Y2_A1566NotaEmpenho_Valor[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1567NotaEmpenho_Qtd != T003Y2_A1567NotaEmpenho_Qtd[0] ) || ( Z1570NotaEmpenho_Ativo != T003Y2_A1570NotaEmpenho_Ativo[0] ) || ( Z1561SaldoContrato_Codigo != T003Y2_A1561SaldoContrato_Codigo[0] ) )
            {
               if ( Z1568NotaEmpenho_SaldoAnt != T003Y2_A1568NotaEmpenho_SaldoAnt[0] )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"NotaEmpenho_SaldoAnt");
                  GXUtil.WriteLogRaw("Old: ",Z1568NotaEmpenho_SaldoAnt);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1568NotaEmpenho_SaldoAnt[0]);
               }
               if ( Z1569NotaEmpenho_SaldoPos != T003Y2_A1569NotaEmpenho_SaldoPos[0] )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"NotaEmpenho_SaldoPos");
                  GXUtil.WriteLogRaw("Old: ",Z1569NotaEmpenho_SaldoPos);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1569NotaEmpenho_SaldoPos[0]);
               }
               if ( StringUtil.StrCmp(Z1564NotaEmpenho_Itentificador, T003Y2_A1564NotaEmpenho_Itentificador[0]) != 0 )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"NotaEmpenho_Itentificador");
                  GXUtil.WriteLogRaw("Old: ",Z1564NotaEmpenho_Itentificador);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1564NotaEmpenho_Itentificador[0]);
               }
               if ( Z1565NotaEmpenho_DEmissao != T003Y2_A1565NotaEmpenho_DEmissao[0] )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"NotaEmpenho_DEmissao");
                  GXUtil.WriteLogRaw("Old: ",Z1565NotaEmpenho_DEmissao);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1565NotaEmpenho_DEmissao[0]);
               }
               if ( Z1566NotaEmpenho_Valor != T003Y2_A1566NotaEmpenho_Valor[0] )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"NotaEmpenho_Valor");
                  GXUtil.WriteLogRaw("Old: ",Z1566NotaEmpenho_Valor);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1566NotaEmpenho_Valor[0]);
               }
               if ( Z1567NotaEmpenho_Qtd != T003Y2_A1567NotaEmpenho_Qtd[0] )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"NotaEmpenho_Qtd");
                  GXUtil.WriteLogRaw("Old: ",Z1567NotaEmpenho_Qtd);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1567NotaEmpenho_Qtd[0]);
               }
               if ( Z1570NotaEmpenho_Ativo != T003Y2_A1570NotaEmpenho_Ativo[0] )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"NotaEmpenho_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z1570NotaEmpenho_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1570NotaEmpenho_Ativo[0]);
               }
               if ( Z1561SaldoContrato_Codigo != T003Y2_A1561SaldoContrato_Codigo[0] )
               {
                  GXUtil.WriteLog("notaempenho:[seudo value changed for attri]"+"SaldoContrato_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z1561SaldoContrato_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T003Y2_A1561SaldoContrato_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"NotaEmpenho"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3Y178( )
      {
         BeforeValidate3Y178( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3Y178( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3Y178( 0) ;
            CheckOptimisticConcurrency3Y178( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3Y178( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3Y178( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003Y10 */
                     pr_default.execute(8, new Object[] {n1568NotaEmpenho_SaldoAnt, A1568NotaEmpenho_SaldoAnt, n1569NotaEmpenho_SaldoPos, A1569NotaEmpenho_SaldoPos, n1564NotaEmpenho_Itentificador, A1564NotaEmpenho_Itentificador, n1565NotaEmpenho_DEmissao, A1565NotaEmpenho_DEmissao, n1566NotaEmpenho_Valor, A1566NotaEmpenho_Valor, n1567NotaEmpenho_Qtd, A1567NotaEmpenho_Qtd, n1570NotaEmpenho_Ativo, A1570NotaEmpenho_Ativo, A1561SaldoContrato_Codigo});
                     A1560NotaEmpenho_Codigo = T003Y10_A1560NotaEmpenho_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("NotaEmpenho") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_notaempenhosaldocontrato(context ).execute(  A1561SaldoContrato_Codigo,  "CRI",  A1560NotaEmpenho_Codigo,  (decimal)(0),  A1566NotaEmpenho_Valor) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3Y0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3Y178( ) ;
            }
            EndLevel3Y178( ) ;
         }
         CloseExtendedTableCursors3Y178( ) ;
      }

      protected void Update3Y178( )
      {
         BeforeValidate3Y178( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3Y178( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3Y178( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3Y178( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3Y178( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003Y11 */
                     pr_default.execute(9, new Object[] {n1568NotaEmpenho_SaldoAnt, A1568NotaEmpenho_SaldoAnt, n1569NotaEmpenho_SaldoPos, A1569NotaEmpenho_SaldoPos, n1564NotaEmpenho_Itentificador, A1564NotaEmpenho_Itentificador, n1565NotaEmpenho_DEmissao, A1565NotaEmpenho_DEmissao, n1566NotaEmpenho_Valor, A1566NotaEmpenho_Valor, n1567NotaEmpenho_Qtd, A1567NotaEmpenho_Qtd, n1570NotaEmpenho_Ativo, A1570NotaEmpenho_Ativo, A1561SaldoContrato_Codigo, A1560NotaEmpenho_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("NotaEmpenho") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"NotaEmpenho"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3Y178( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3Y178( ) ;
         }
         CloseExtendedTableCursors3Y178( ) ;
      }

      protected void DeferredUpdate3Y178( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3Y178( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3Y178( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3Y178( ) ;
            AfterConfirm3Y178( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3Y178( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003Y12 */
                  pr_default.execute(10, new Object[] {A1560NotaEmpenho_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("NotaEmpenho") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode178 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3Y178( ) ;
         Gx_mode = sMode178;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3Y178( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003Y13 */
            pr_default.execute(11, new Object[] {A1561SaldoContrato_Codigo});
            A1576SaldoContrato_Saldo = T003Y13_A1576SaldoContrato_Saldo[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel3Y178( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3Y178( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "NotaEmpenho");
            if ( AnyError == 0 )
            {
               ConfirmValues3Y0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "NotaEmpenho");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3Y178( )
      {
         /* Scan By routine */
         /* Using cursor T003Y14 */
         pr_default.execute(12);
         RcdFound178 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound178 = 1;
            A1560NotaEmpenho_Codigo = T003Y14_A1560NotaEmpenho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3Y178( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound178 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound178 = 1;
            A1560NotaEmpenho_Codigo = T003Y14_A1560NotaEmpenho_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3Y178( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm3Y178( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3Y178( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3Y178( )
      {
         /* Before Update Rules */
         if ( A1566NotaEmpenho_Valor != O1566NotaEmpenho_Valor )
         {
            new prc_notaempenhosaldocontrato(context ).execute(  A1561SaldoContrato_Codigo,  "ALT",  A1560NotaEmpenho_Codigo,  O1566NotaEmpenho_Valor,  A1566NotaEmpenho_Valor) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
         }
         if ( A1566NotaEmpenho_Valor != O1566NotaEmpenho_Valor )
         {
            new prc_notaempenhonotificargestor(context ).execute(  A1560NotaEmpenho_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         }
      }

      protected void BeforeDelete3Y178( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3Y178( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3Y178( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3Y178( )
      {
         edtNotaEmpenho_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNotaEmpenho_Codigo_Enabled), 5, 0)));
         dynSaldoContrato_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynSaldoContrato_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynSaldoContrato_Codigo.Enabled), 5, 0)));
         edtNotaEmpenho_Itentificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Itentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNotaEmpenho_Itentificador_Enabled), 5, 0)));
         edtNotaEmpenho_DEmissao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_DEmissao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNotaEmpenho_DEmissao_Enabled), 5, 0)));
         edtNotaEmpenho_Qtd_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Qtd_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNotaEmpenho_Qtd_Enabled), 5, 0)));
         edtNotaEmpenho_Valor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtNotaEmpenho_Valor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtNotaEmpenho_Valor_Enabled), 5, 0)));
         chkNotaEmpenho_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkNotaEmpenho_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkNotaEmpenho_Ativo.Enabled), 5, 0)));
         edtavNotaempenho_saldoant_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaempenho_saldoant_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_saldoant_Enabled), 5, 0)));
         edtavNotaempenho_saldopos_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaempenho_saldopos_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_saldopos_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3Y0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120425020");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("notaempenho.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7NotaEmpenho_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1560NotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1568NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.NToC( Z1568NotaEmpenho_SaldoAnt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1569NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.NToC( Z1569NotaEmpenho_SaldoPos, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1564NotaEmpenho_Itentificador", StringUtil.RTrim( Z1564NotaEmpenho_Itentificador));
         GxWebStd.gx_hidden_field( context, "Z1565NotaEmpenho_DEmissao", context.localUtil.TToC( Z1565NotaEmpenho_DEmissao, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.NToC( Z1566NotaEmpenho_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.NToC( Z1567NotaEmpenho_Qtd, 14, 5, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1570NotaEmpenho_Ativo", Z1570NotaEmpenho_Ativo);
         GxWebStd.gx_hidden_field( context, "Z1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1561SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.NToC( O1566NotaEmpenho_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1561SaldoContrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vNOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7NotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_SALDOANT", StringUtil.LTrim( StringUtil.NToC( A1568NotaEmpenho_SaldoAnt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "NOTAEMPENHO_SALDOPOS", StringUtil.LTrim( StringUtil.NToC( A1569NotaEmpenho_SaldoPos, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SALDOCONTRATO_SALDO", StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV16Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vNOTAEMPENHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7NotaEmpenho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Width", StringUtil.RTrim( Dvpanel_tablesaldo_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Cls", StringUtil.RTrim( Dvpanel_tablesaldo_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Title", StringUtil.RTrim( Dvpanel_tablesaldo_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Collapsible", StringUtil.BoolToStr( Dvpanel_tablesaldo_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Collapsed", StringUtil.BoolToStr( Dvpanel_tablesaldo_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Enabled", StringUtil.BoolToStr( Dvpanel_tablesaldo_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Autowidth", StringUtil.BoolToStr( Dvpanel_tablesaldo_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Autoheight", StringUtil.BoolToStr( Dvpanel_tablesaldo_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tablesaldo_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Iconposition", StringUtil.RTrim( Dvpanel_tablesaldo_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLESALDO_Autoscroll", StringUtil.BoolToStr( Dvpanel_tablesaldo_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "NotaEmpenho";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("notaempenho:[SendSecurityCheck value for]"+"NotaEmpenho_Codigo:"+context.localUtil.Format( (decimal)(A1560NotaEmpenho_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("notaempenho:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("notaempenho:[SendSecurityCheck value for]"+"NotaEmpenho_SaldoAnt:"+context.localUtil.Format( A1568NotaEmpenho_SaldoAnt, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
         GXUtil.WriteLog("notaempenho:[SendSecurityCheck value for]"+"NotaEmpenho_SaldoPos:"+context.localUtil.Format( A1569NotaEmpenho_SaldoPos, "ZZZ,ZZZ,ZZZ,ZZ9.99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("notaempenho.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7NotaEmpenho_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "NotaEmpenho" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nota Empenho" ;
      }

      protected void InitializeNonKey3Y178( )
      {
         A1561SaldoContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         A1568NotaEmpenho_SaldoAnt = 0;
         n1568NotaEmpenho_SaldoAnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1568NotaEmpenho_SaldoAnt", StringUtil.LTrim( StringUtil.Str( A1568NotaEmpenho_SaldoAnt, 18, 5)));
         A1569NotaEmpenho_SaldoPos = 0;
         n1569NotaEmpenho_SaldoPos = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1569NotaEmpenho_SaldoPos", StringUtil.LTrim( StringUtil.Str( A1569NotaEmpenho_SaldoPos, 18, 5)));
         A1576SaldoContrato_Saldo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1576SaldoContrato_Saldo", StringUtil.LTrim( StringUtil.Str( A1576SaldoContrato_Saldo, 18, 5)));
         A1564NotaEmpenho_Itentificador = "";
         n1564NotaEmpenho_Itentificador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1564NotaEmpenho_Itentificador", A1564NotaEmpenho_Itentificador);
         n1564NotaEmpenho_Itentificador = (String.IsNullOrEmpty(StringUtil.RTrim( A1564NotaEmpenho_Itentificador)) ? true : false);
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         n1565NotaEmpenho_DEmissao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
         n1565NotaEmpenho_DEmissao = ((DateTime.MinValue==A1565NotaEmpenho_DEmissao) ? true : false);
         A1566NotaEmpenho_Valor = 0;
         n1566NotaEmpenho_Valor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
         n1566NotaEmpenho_Valor = ((Convert.ToDecimal(0)==A1566NotaEmpenho_Valor) ? true : false);
         A1567NotaEmpenho_Qtd = 0;
         n1567NotaEmpenho_Qtd = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1567NotaEmpenho_Qtd", StringUtil.LTrim( StringUtil.Str( A1567NotaEmpenho_Qtd, 14, 5)));
         n1567NotaEmpenho_Qtd = ((Convert.ToDecimal(0)==A1567NotaEmpenho_Qtd) ? true : false);
         A1570NotaEmpenho_Ativo = true;
         n1570NotaEmpenho_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
         O1566NotaEmpenho_Valor = A1566NotaEmpenho_Valor;
         n1566NotaEmpenho_Valor = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
         Z1568NotaEmpenho_SaldoAnt = 0;
         Z1569NotaEmpenho_SaldoPos = 0;
         Z1564NotaEmpenho_Itentificador = "";
         Z1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         Z1566NotaEmpenho_Valor = 0;
         Z1567NotaEmpenho_Qtd = 0;
         Z1570NotaEmpenho_Ativo = false;
         Z1561SaldoContrato_Codigo = 0;
      }

      protected void InitAll3Y178( )
      {
         A1560NotaEmpenho_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         InitializeNonKey3Y178( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1570NotaEmpenho_Ativo = i1570NotaEmpenho_Ativo;
         n1570NotaEmpenho_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1570NotaEmpenho_Ativo", A1570NotaEmpenho_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120425043");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("notaempenho.js", "?20203120425043");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocknotaempenho_codigo_Internalname = "TEXTBLOCKNOTAEMPENHO_CODIGO";
         edtNotaEmpenho_Codigo_Internalname = "NOTAEMPENHO_CODIGO";
         lblTextblocksaldocontrato_codigo_Internalname = "TEXTBLOCKSALDOCONTRATO_CODIGO";
         dynSaldoContrato_Codigo_Internalname = "SALDOCONTRATO_CODIGO";
         lblTextblocknotaempenho_itentificador_Internalname = "TEXTBLOCKNOTAEMPENHO_ITENTIFICADOR";
         edtNotaEmpenho_Itentificador_Internalname = "NOTAEMPENHO_ITENTIFICADOR";
         lblTextblocknotaempenho_demissao_Internalname = "TEXTBLOCKNOTAEMPENHO_DEMISSAO";
         edtNotaEmpenho_DEmissao_Internalname = "NOTAEMPENHO_DEMISSAO";
         lblTextblocknotaempenho_qtd_Internalname = "TEXTBLOCKNOTAEMPENHO_QTD";
         edtNotaEmpenho_Qtd_Internalname = "NOTAEMPENHO_QTD";
         lblTextblocknotaempenho_valor_Internalname = "TEXTBLOCKNOTAEMPENHO_VALOR";
         edtNotaEmpenho_Valor_Internalname = "NOTAEMPENHO_VALOR";
         lblTextblocknotaempenho_ativo_Internalname = "TEXTBLOCKNOTAEMPENHO_ATIVO";
         chkNotaEmpenho_Ativo_Internalname = "NOTAEMPENHO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         lblTextblocknotaempenho_saldoant_Internalname = "TEXTBLOCKNOTAEMPENHO_SALDOANT";
         edtavNotaempenho_saldoant_Internalname = "vNOTAEMPENHO_SALDOANT";
         lblTextblocknotaempenho_saldopos_Internalname = "TEXTBLOCKNOTAEMPENHO_SALDOPOS";
         edtavNotaempenho_saldopos_Internalname = "vNOTAEMPENHO_SALDOPOS";
         tblTablesaldo_Internalname = "TABLESALDO";
         Dvpanel_tablesaldo_Internalname = "DVPANEL_TABLESALDO";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tablesaldo_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tablesaldo_Iconposition = "left";
         Dvpanel_tablesaldo_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tablesaldo_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tablesaldo_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tablesaldo_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tablesaldo_Collapsible = Convert.ToBoolean( -1);
         Dvpanel_tablesaldo_Title = "Saldo";
         Dvpanel_tablesaldo_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tablesaldo_Width = "100%";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Nota Empenho";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Nota Empenho";
         chkNotaEmpenho_Ativo.Enabled = 1;
         edtNotaEmpenho_Valor_Jsonclick = "";
         edtNotaEmpenho_Valor_Enabled = 1;
         edtNotaEmpenho_Qtd_Jsonclick = "";
         edtNotaEmpenho_Qtd_Enabled = 1;
         edtNotaEmpenho_DEmissao_Jsonclick = "";
         edtNotaEmpenho_DEmissao_Enabled = 1;
         edtNotaEmpenho_Itentificador_Jsonclick = "";
         edtNotaEmpenho_Itentificador_Enabled = 1;
         dynSaldoContrato_Codigo_Jsonclick = "";
         dynSaldoContrato_Codigo.Enabled = 1;
         edtNotaEmpenho_Codigo_Jsonclick = "";
         edtNotaEmpenho_Codigo_Enabled = 0;
         edtavNotaempenho_saldopos_Jsonclick = "";
         edtavNotaempenho_saldopos_Enabled = 0;
         edtavNotaempenho_saldoant_Jsonclick = "";
         edtavNotaempenho_saldoant_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         chkNotaEmpenho_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDSASALDOCONTRATO_CODIGO3Y178( int A74Contrato_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSASALDOCONTRATO_CODIGO_data3Y178( A74Contrato_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASALDOCONTRATO_CODIGO_html3Y178( int A74Contrato_Codigo )
      {
         int gxdynajaxvalue ;
         GXDSASALDOCONTRATO_CODIGO_data3Y178( A74Contrato_Codigo) ;
         gxdynajaxindex = 1;
         dynSaldoContrato_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynSaldoContrato_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDSASALDOCONTRATO_CODIGO_data3Y178( int A74Contrato_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         IGxCollection gxcolSALDOCONTRATO_CODIGO ;
         SdtSDT_SaldoContratoCB gxcolitemSALDOCONTRATO_CODIGO ;
         new dp_saldocontratocb(context ).execute(  A74Contrato_Codigo, out  gxcolSALDOCONTRATO_CODIGO) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         gxcolSALDOCONTRATO_CODIGO.Sort("Saldocontrato_descricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolSALDOCONTRATO_CODIGO.Count )
         {
            gxcolitemSALDOCONTRATO_CODIGO = ((SdtSDT_SaldoContratoCB)gxcolSALDOCONTRATO_CODIGO.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemSALDOCONTRATO_CODIGO.gxTpr_Saldocontrato_codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemSALDOCONTRATO_CODIGO.gxTpr_Saldocontrato_descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void XC_18_3Y178( int A1561SaldoContrato_Codigo ,
                                  int A1560NotaEmpenho_Codigo ,
                                  decimal A1566NotaEmpenho_Valor )
      {
         new prc_notaempenhosaldocontrato(context ).execute(  A1561SaldoContrato_Codigo,  "CRI",  A1560NotaEmpenho_Codigo,  (decimal)(0),  A1566NotaEmpenho_Valor) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_19_3Y178( )
      {
         if ( A1566NotaEmpenho_Valor != O1566NotaEmpenho_Valor )
         {
            new prc_notaempenhosaldocontrato(context ).execute(  A1561SaldoContrato_Codigo,  "ALT",  A1560NotaEmpenho_Codigo,  O1566NotaEmpenho_Valor,  A1566NotaEmpenho_Valor) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1561SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1561SaldoContrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1566NotaEmpenho_Valor", StringUtil.LTrim( StringUtil.Str( A1566NotaEmpenho_Valor, 18, 5)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_20_3Y178( int A1560NotaEmpenho_Codigo ,
                                  decimal A1566NotaEmpenho_Valor )
      {
         if ( A1566NotaEmpenho_Valor != O1566NotaEmpenho_Valor )
         {
            new prc_notaempenhonotificargestor(context ).execute(  A1560NotaEmpenho_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Saldocontrato_codigo( GXCombobox dynGX_Parm1 ,
                                              int GX_Parm2 ,
                                              decimal GX_Parm3 )
      {
         dynSaldoContrato_Codigo = dynGX_Parm1;
         A1561SaldoContrato_Codigo = (int)(NumberUtil.Val( dynSaldoContrato_Codigo.CurrentValue, "."));
         A74Contrato_Codigo = GX_Parm2;
         A1576SaldoContrato_Saldo = GX_Parm3;
         /* Using cursor T003Y13 */
         pr_default.execute(11, new Object[] {A1561SaldoContrato_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T179'.", "ForeignKeyNotFound", 1, "SALDOCONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
         }
         A1576SaldoContrato_Saldo = T003Y13_A1576SaldoContrato_Saldo[0];
         pr_default.close(11);
         if ( (0==A1561SaldoContrato_Codigo) )
         {
            GX_msglist.addItem("Saldo Contrato � obrigat�rio.", 1, "SALDOCONTRATO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynSaldoContrato_Codigo_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A74Contrato_Codigo = 0;
            A1576SaldoContrato_Saldo = 0;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A1576SaldoContrato_Saldo, 18, 5, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7NotaEmpenho_Codigo',fld:'vNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123Y2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("SALDOCONTRATO_CODIGO.ISVALID","{handler:'E133Y2',iparms:[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1566NotaEmpenho_Valor',fld:'NOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV13NotaEmpenho_SaldoAnt',fld:'vNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV14NotaEmpenho_SaldoPos',fld:'vNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         setEventMetadata("NOTAEMPENHO_VALOR.ISVALID","{handler:'E143Y2',iparms:[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1561SaldoContrato_Codigo',fld:'SALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1566NotaEmpenho_Valor',fld:'NOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],oparms:[{av:'AV13NotaEmpenho_SaldoAnt',fld:'vNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV14NotaEmpenho_SaldoPos',fld:'vNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1564NotaEmpenho_Itentificador = "";
         Z1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocknotaempenho_saldoant_Jsonclick = "";
         lblTextblocknotaempenho_saldopos_Jsonclick = "";
         lblTextblocknotaempenho_codigo_Jsonclick = "";
         lblTextblocksaldocontrato_codigo_Jsonclick = "";
         lblTextblocknotaempenho_itentificador_Jsonclick = "";
         A1564NotaEmpenho_Itentificador = "";
         lblTextblocknotaempenho_demissao_Jsonclick = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         lblTextblocknotaempenho_qtd_Jsonclick = "";
         lblTextblocknotaempenho_valor_Jsonclick = "";
         lblTextblocknotaempenho_ativo_Jsonclick = "";
         AV16Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         Dvpanel_tablesaldo_Height = "";
         Dvpanel_tablesaldo_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode178 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T003Y4_A74Contrato_Codigo = new int[1] ;
         T003Y4_A1576SaldoContrato_Saldo = new decimal[1] ;
         T003Y5_A74Contrato_Codigo = new int[1] ;
         T003Y5_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Y5_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         T003Y5_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         T003Y5_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         T003Y5_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         T003Y5_A1576SaldoContrato_Saldo = new decimal[1] ;
         T003Y5_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         T003Y5_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         T003Y5_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         T003Y5_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         T003Y5_A1566NotaEmpenho_Valor = new decimal[1] ;
         T003Y5_n1566NotaEmpenho_Valor = new bool[] {false} ;
         T003Y5_A1567NotaEmpenho_Qtd = new decimal[1] ;
         T003Y5_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         T003Y5_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         T003Y5_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         T003Y5_A1561SaldoContrato_Codigo = new int[1] ;
         T003Y6_A74Contrato_Codigo = new int[1] ;
         T003Y6_A1576SaldoContrato_Saldo = new decimal[1] ;
         T003Y7_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Y3_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Y3_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         T003Y3_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         T003Y3_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         T003Y3_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         T003Y3_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         T003Y3_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         T003Y3_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         T003Y3_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         T003Y3_A1566NotaEmpenho_Valor = new decimal[1] ;
         T003Y3_n1566NotaEmpenho_Valor = new bool[] {false} ;
         T003Y3_A1567NotaEmpenho_Qtd = new decimal[1] ;
         T003Y3_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         T003Y3_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         T003Y3_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         T003Y3_A1561SaldoContrato_Codigo = new int[1] ;
         T003Y8_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Y9_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Y2_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Y2_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         T003Y2_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         T003Y2_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         T003Y2_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         T003Y2_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         T003Y2_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         T003Y2_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         T003Y2_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         T003Y2_A1566NotaEmpenho_Valor = new decimal[1] ;
         T003Y2_n1566NotaEmpenho_Valor = new bool[] {false} ;
         T003Y2_A1567NotaEmpenho_Qtd = new decimal[1] ;
         T003Y2_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         T003Y2_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         T003Y2_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         T003Y2_A1561SaldoContrato_Codigo = new int[1] ;
         T003Y10_A1560NotaEmpenho_Codigo = new int[1] ;
         T003Y13_A74Contrato_Codigo = new int[1] ;
         T003Y13_A1576SaldoContrato_Saldo = new decimal[1] ;
         T003Y14_A1560NotaEmpenho_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.notaempenho__default(),
            new Object[][] {
                new Object[] {
               T003Y2_A1560NotaEmpenho_Codigo, T003Y2_A1568NotaEmpenho_SaldoAnt, T003Y2_n1568NotaEmpenho_SaldoAnt, T003Y2_A1569NotaEmpenho_SaldoPos, T003Y2_n1569NotaEmpenho_SaldoPos, T003Y2_A1564NotaEmpenho_Itentificador, T003Y2_n1564NotaEmpenho_Itentificador, T003Y2_A1565NotaEmpenho_DEmissao, T003Y2_n1565NotaEmpenho_DEmissao, T003Y2_A1566NotaEmpenho_Valor,
               T003Y2_n1566NotaEmpenho_Valor, T003Y2_A1567NotaEmpenho_Qtd, T003Y2_n1567NotaEmpenho_Qtd, T003Y2_A1570NotaEmpenho_Ativo, T003Y2_n1570NotaEmpenho_Ativo, T003Y2_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003Y3_A1560NotaEmpenho_Codigo, T003Y3_A1568NotaEmpenho_SaldoAnt, T003Y3_n1568NotaEmpenho_SaldoAnt, T003Y3_A1569NotaEmpenho_SaldoPos, T003Y3_n1569NotaEmpenho_SaldoPos, T003Y3_A1564NotaEmpenho_Itentificador, T003Y3_n1564NotaEmpenho_Itentificador, T003Y3_A1565NotaEmpenho_DEmissao, T003Y3_n1565NotaEmpenho_DEmissao, T003Y3_A1566NotaEmpenho_Valor,
               T003Y3_n1566NotaEmpenho_Valor, T003Y3_A1567NotaEmpenho_Qtd, T003Y3_n1567NotaEmpenho_Qtd, T003Y3_A1570NotaEmpenho_Ativo, T003Y3_n1570NotaEmpenho_Ativo, T003Y3_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003Y4_A74Contrato_Codigo, T003Y4_A1576SaldoContrato_Saldo
               }
               , new Object[] {
               T003Y5_A74Contrato_Codigo, T003Y5_A1560NotaEmpenho_Codigo, T003Y5_A1568NotaEmpenho_SaldoAnt, T003Y5_n1568NotaEmpenho_SaldoAnt, T003Y5_A1569NotaEmpenho_SaldoPos, T003Y5_n1569NotaEmpenho_SaldoPos, T003Y5_A1576SaldoContrato_Saldo, T003Y5_A1564NotaEmpenho_Itentificador, T003Y5_n1564NotaEmpenho_Itentificador, T003Y5_A1565NotaEmpenho_DEmissao,
               T003Y5_n1565NotaEmpenho_DEmissao, T003Y5_A1566NotaEmpenho_Valor, T003Y5_n1566NotaEmpenho_Valor, T003Y5_A1567NotaEmpenho_Qtd, T003Y5_n1567NotaEmpenho_Qtd, T003Y5_A1570NotaEmpenho_Ativo, T003Y5_n1570NotaEmpenho_Ativo, T003Y5_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               T003Y6_A74Contrato_Codigo, T003Y6_A1576SaldoContrato_Saldo
               }
               , new Object[] {
               T003Y7_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               T003Y8_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               T003Y9_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               T003Y10_A1560NotaEmpenho_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003Y13_A74Contrato_Codigo, T003Y13_A1576SaldoContrato_Saldo
               }
               , new Object[] {
               T003Y14_A1560NotaEmpenho_Codigo
               }
            }
         );
         Z74Contrato_Codigo = 0;
         A74Contrato_Codigo = 0;
         AV16Pgmname = "NotaEmpenho";
         Z1570NotaEmpenho_Ativo = true;
         n1570NotaEmpenho_Ativo = false;
         A1570NotaEmpenho_Ativo = true;
         n1570NotaEmpenho_Ativo = false;
         i1570NotaEmpenho_Ativo = true;
         n1570NotaEmpenho_Ativo = false;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound178 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7NotaEmpenho_Codigo ;
      private int wcpOA74Contrato_Codigo ;
      private int Z1560NotaEmpenho_Codigo ;
      private int Z1561SaldoContrato_Codigo ;
      private int N1561SaldoContrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int A1560NotaEmpenho_Codigo ;
      private int A74Contrato_Codigo ;
      private int AV7NotaEmpenho_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtavNotaempenho_saldoant_Enabled ;
      private int edtavNotaempenho_saldopos_Enabled ;
      private int edtNotaEmpenho_Codigo_Enabled ;
      private int edtNotaEmpenho_Itentificador_Enabled ;
      private int edtNotaEmpenho_DEmissao_Enabled ;
      private int edtNotaEmpenho_Qtd_Enabled ;
      private int edtNotaEmpenho_Valor_Enabled ;
      private int AV11Insert_SaldoContrato_Codigo ;
      private int AV17GXV1 ;
      private int Z74Contrato_Codigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1568NotaEmpenho_SaldoAnt ;
      private decimal Z1569NotaEmpenho_SaldoPos ;
      private decimal Z1566NotaEmpenho_Valor ;
      private decimal Z1567NotaEmpenho_Qtd ;
      private decimal O1566NotaEmpenho_Valor ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal AV13NotaEmpenho_SaldoAnt ;
      private decimal AV14NotaEmpenho_SaldoPos ;
      private decimal A1567NotaEmpenho_Qtd ;
      private decimal A1568NotaEmpenho_SaldoAnt ;
      private decimal A1569NotaEmpenho_SaldoPos ;
      private decimal A1576SaldoContrato_Saldo ;
      private decimal GXt_decimal1 ;
      private decimal Z1576SaldoContrato_Saldo ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1564NotaEmpenho_Itentificador ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkNotaEmpenho_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynSaldoContrato_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTablesaldo_Internalname ;
      private String lblTextblocknotaempenho_saldoant_Internalname ;
      private String lblTextblocknotaempenho_saldoant_Jsonclick ;
      private String edtavNotaempenho_saldoant_Internalname ;
      private String edtavNotaempenho_saldoant_Jsonclick ;
      private String lblTextblocknotaempenho_saldopos_Internalname ;
      private String lblTextblocknotaempenho_saldopos_Jsonclick ;
      private String edtavNotaempenho_saldopos_Internalname ;
      private String edtavNotaempenho_saldopos_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocknotaempenho_codigo_Internalname ;
      private String lblTextblocknotaempenho_codigo_Jsonclick ;
      private String edtNotaEmpenho_Codigo_Internalname ;
      private String edtNotaEmpenho_Codigo_Jsonclick ;
      private String lblTextblocksaldocontrato_codigo_Internalname ;
      private String lblTextblocksaldocontrato_codigo_Jsonclick ;
      private String dynSaldoContrato_Codigo_Jsonclick ;
      private String lblTextblocknotaempenho_itentificador_Internalname ;
      private String lblTextblocknotaempenho_itentificador_Jsonclick ;
      private String edtNotaEmpenho_Itentificador_Internalname ;
      private String A1564NotaEmpenho_Itentificador ;
      private String edtNotaEmpenho_Itentificador_Jsonclick ;
      private String lblTextblocknotaempenho_demissao_Internalname ;
      private String lblTextblocknotaempenho_demissao_Jsonclick ;
      private String edtNotaEmpenho_DEmissao_Internalname ;
      private String edtNotaEmpenho_DEmissao_Jsonclick ;
      private String lblTextblocknotaempenho_qtd_Internalname ;
      private String lblTextblocknotaempenho_qtd_Jsonclick ;
      private String edtNotaEmpenho_Qtd_Internalname ;
      private String edtNotaEmpenho_Qtd_Jsonclick ;
      private String lblTextblocknotaempenho_valor_Internalname ;
      private String lblTextblocknotaempenho_valor_Jsonclick ;
      private String edtNotaEmpenho_Valor_Internalname ;
      private String edtNotaEmpenho_Valor_Jsonclick ;
      private String lblTextblocknotaempenho_ativo_Internalname ;
      private String lblTextblocknotaempenho_ativo_Jsonclick ;
      private String AV16Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Dvpanel_tablesaldo_Width ;
      private String Dvpanel_tablesaldo_Height ;
      private String Dvpanel_tablesaldo_Cls ;
      private String Dvpanel_tablesaldo_Title ;
      private String Dvpanel_tablesaldo_Class ;
      private String Dvpanel_tablesaldo_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode178 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Dvpanel_tablesaldo_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1565NotaEmpenho_DEmissao ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private bool Z1570NotaEmpenho_Ativo ;
      private bool entryPointCalled ;
      private bool n1566NotaEmpenho_Valor ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1570NotaEmpenho_Ativo ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool n1565NotaEmpenho_DEmissao ;
      private bool n1567NotaEmpenho_Qtd ;
      private bool n1570NotaEmpenho_Ativo ;
      private bool n1568NotaEmpenho_SaldoAnt ;
      private bool n1569NotaEmpenho_SaldoPos ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool Dvpanel_tablesaldo_Collapsible ;
      private bool Dvpanel_tablesaldo_Collapsed ;
      private bool Dvpanel_tablesaldo_Enabled ;
      private bool Dvpanel_tablesaldo_Autowidth ;
      private bool Dvpanel_tablesaldo_Autoheight ;
      private bool Dvpanel_tablesaldo_Showheader ;
      private bool Dvpanel_tablesaldo_Showcollapseicon ;
      private bool Dvpanel_tablesaldo_Autoscroll ;
      private bool Dvpanel_tablesaldo_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i1570NotaEmpenho_Ativo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private GXCombobox dynSaldoContrato_Codigo ;
      private GXCheckbox chkNotaEmpenho_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] T003Y4_A74Contrato_Codigo ;
      private decimal[] T003Y4_A1576SaldoContrato_Saldo ;
      private int[] T003Y5_A74Contrato_Codigo ;
      private int[] T003Y5_A1560NotaEmpenho_Codigo ;
      private decimal[] T003Y5_A1568NotaEmpenho_SaldoAnt ;
      private bool[] T003Y5_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] T003Y5_A1569NotaEmpenho_SaldoPos ;
      private bool[] T003Y5_n1569NotaEmpenho_SaldoPos ;
      private decimal[] T003Y5_A1576SaldoContrato_Saldo ;
      private String[] T003Y5_A1564NotaEmpenho_Itentificador ;
      private bool[] T003Y5_n1564NotaEmpenho_Itentificador ;
      private DateTime[] T003Y5_A1565NotaEmpenho_DEmissao ;
      private bool[] T003Y5_n1565NotaEmpenho_DEmissao ;
      private decimal[] T003Y5_A1566NotaEmpenho_Valor ;
      private bool[] T003Y5_n1566NotaEmpenho_Valor ;
      private decimal[] T003Y5_A1567NotaEmpenho_Qtd ;
      private bool[] T003Y5_n1567NotaEmpenho_Qtd ;
      private bool[] T003Y5_A1570NotaEmpenho_Ativo ;
      private bool[] T003Y5_n1570NotaEmpenho_Ativo ;
      private int[] T003Y5_A1561SaldoContrato_Codigo ;
      private int[] T003Y6_A74Contrato_Codigo ;
      private decimal[] T003Y6_A1576SaldoContrato_Saldo ;
      private int[] T003Y7_A1560NotaEmpenho_Codigo ;
      private int[] T003Y3_A1560NotaEmpenho_Codigo ;
      private decimal[] T003Y3_A1568NotaEmpenho_SaldoAnt ;
      private bool[] T003Y3_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] T003Y3_A1569NotaEmpenho_SaldoPos ;
      private bool[] T003Y3_n1569NotaEmpenho_SaldoPos ;
      private String[] T003Y3_A1564NotaEmpenho_Itentificador ;
      private bool[] T003Y3_n1564NotaEmpenho_Itentificador ;
      private DateTime[] T003Y3_A1565NotaEmpenho_DEmissao ;
      private bool[] T003Y3_n1565NotaEmpenho_DEmissao ;
      private decimal[] T003Y3_A1566NotaEmpenho_Valor ;
      private bool[] T003Y3_n1566NotaEmpenho_Valor ;
      private decimal[] T003Y3_A1567NotaEmpenho_Qtd ;
      private bool[] T003Y3_n1567NotaEmpenho_Qtd ;
      private bool[] T003Y3_A1570NotaEmpenho_Ativo ;
      private bool[] T003Y3_n1570NotaEmpenho_Ativo ;
      private int[] T003Y3_A1561SaldoContrato_Codigo ;
      private int[] T003Y8_A1560NotaEmpenho_Codigo ;
      private int[] T003Y9_A1560NotaEmpenho_Codigo ;
      private int[] T003Y2_A1560NotaEmpenho_Codigo ;
      private decimal[] T003Y2_A1568NotaEmpenho_SaldoAnt ;
      private bool[] T003Y2_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] T003Y2_A1569NotaEmpenho_SaldoPos ;
      private bool[] T003Y2_n1569NotaEmpenho_SaldoPos ;
      private String[] T003Y2_A1564NotaEmpenho_Itentificador ;
      private bool[] T003Y2_n1564NotaEmpenho_Itentificador ;
      private DateTime[] T003Y2_A1565NotaEmpenho_DEmissao ;
      private bool[] T003Y2_n1565NotaEmpenho_DEmissao ;
      private decimal[] T003Y2_A1566NotaEmpenho_Valor ;
      private bool[] T003Y2_n1566NotaEmpenho_Valor ;
      private decimal[] T003Y2_A1567NotaEmpenho_Qtd ;
      private bool[] T003Y2_n1567NotaEmpenho_Qtd ;
      private bool[] T003Y2_A1570NotaEmpenho_Ativo ;
      private bool[] T003Y2_n1570NotaEmpenho_Ativo ;
      private int[] T003Y2_A1561SaldoContrato_Codigo ;
      private int[] T003Y10_A1560NotaEmpenho_Codigo ;
      private int[] T003Y13_A74Contrato_Codigo ;
      private decimal[] T003Y13_A1576SaldoContrato_Saldo ;
      private int[] T003Y14_A1560NotaEmpenho_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class notaempenho__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003Y5 ;
          prmT003Y5 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y4 ;
          prmT003Y4 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y6 ;
          prmT003Y6 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y7 ;
          prmT003Y7 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y3 ;
          prmT003Y3 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y8 ;
          prmT003Y8 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y9 ;
          prmT003Y9 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y2 ;
          prmT003Y2 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y10 ;
          prmT003Y10 = new Object[] {
          new Object[] {"@NotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@NotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@NotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@NotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@NotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@NotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@NotaEmpenho_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y11 ;
          prmT003Y11 = new Object[] {
          new Object[] {"@NotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@NotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@NotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@NotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@NotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@NotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@NotaEmpenho_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y12 ;
          prmT003Y12 = new Object[] {
          new Object[] {"@NotaEmpenho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003Y14 ;
          prmT003Y14 = new Object[] {
          } ;
          Object[] prmT003Y13 ;
          prmT003Y13 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003Y2", "SELECT [NotaEmpenho_Codigo], [NotaEmpenho_SaldoAnt], [NotaEmpenho_SaldoPos], [NotaEmpenho_Itentificador], [NotaEmpenho_DEmissao], [NotaEmpenho_Valor], [NotaEmpenho_Qtd], [NotaEmpenho_Ativo], [SaldoContrato_Codigo] FROM [NotaEmpenho] WITH (UPDLOCK) WHERE [NotaEmpenho_Codigo] = @NotaEmpenho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y2,1,0,true,false )
             ,new CursorDef("T003Y3", "SELECT [NotaEmpenho_Codigo], [NotaEmpenho_SaldoAnt], [NotaEmpenho_SaldoPos], [NotaEmpenho_Itentificador], [NotaEmpenho_DEmissao], [NotaEmpenho_Valor], [NotaEmpenho_Qtd], [NotaEmpenho_Ativo], [SaldoContrato_Codigo] FROM [NotaEmpenho] WITH (NOLOCK) WHERE [NotaEmpenho_Codigo] = @NotaEmpenho_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y3,1,0,true,false )
             ,new CursorDef("T003Y4", "SELECT [Contrato_Codigo], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y4,1,0,true,false )
             ,new CursorDef("T003Y5", "SELECT T2.[Contrato_Codigo], TM1.[NotaEmpenho_Codigo], TM1.[NotaEmpenho_SaldoAnt], TM1.[NotaEmpenho_SaldoPos], T2.[SaldoContrato_Saldo], TM1.[NotaEmpenho_Itentificador], TM1.[NotaEmpenho_DEmissao], TM1.[NotaEmpenho_Valor], TM1.[NotaEmpenho_Qtd], TM1.[NotaEmpenho_Ativo], TM1.[SaldoContrato_Codigo] FROM ([NotaEmpenho] TM1 WITH (NOLOCK) INNER JOIN [SaldoContrato] T2 WITH (NOLOCK) ON T2.[SaldoContrato_Codigo] = TM1.[SaldoContrato_Codigo]) WHERE TM1.[NotaEmpenho_Codigo] = @NotaEmpenho_Codigo ORDER BY TM1.[NotaEmpenho_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y5,100,0,true,false )
             ,new CursorDef("T003Y6", "SELECT [Contrato_Codigo], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y6,1,0,true,false )
             ,new CursorDef("T003Y7", "SELECT [NotaEmpenho_Codigo] FROM [NotaEmpenho] WITH (NOLOCK) WHERE [NotaEmpenho_Codigo] = @NotaEmpenho_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y7,1,0,true,false )
             ,new CursorDef("T003Y8", "SELECT TOP 1 [NotaEmpenho_Codigo] FROM [NotaEmpenho] WITH (NOLOCK) WHERE ( [NotaEmpenho_Codigo] > @NotaEmpenho_Codigo) ORDER BY [NotaEmpenho_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y8,1,0,true,true )
             ,new CursorDef("T003Y9", "SELECT TOP 1 [NotaEmpenho_Codigo] FROM [NotaEmpenho] WITH (NOLOCK) WHERE ( [NotaEmpenho_Codigo] < @NotaEmpenho_Codigo) ORDER BY [NotaEmpenho_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y9,1,0,true,true )
             ,new CursorDef("T003Y10", "INSERT INTO [NotaEmpenho]([NotaEmpenho_SaldoAnt], [NotaEmpenho_SaldoPos], [NotaEmpenho_Itentificador], [NotaEmpenho_DEmissao], [NotaEmpenho_Valor], [NotaEmpenho_Qtd], [NotaEmpenho_Ativo], [SaldoContrato_Codigo]) VALUES(@NotaEmpenho_SaldoAnt, @NotaEmpenho_SaldoPos, @NotaEmpenho_Itentificador, @NotaEmpenho_DEmissao, @NotaEmpenho_Valor, @NotaEmpenho_Qtd, @NotaEmpenho_Ativo, @SaldoContrato_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003Y10)
             ,new CursorDef("T003Y11", "UPDATE [NotaEmpenho] SET [NotaEmpenho_SaldoAnt]=@NotaEmpenho_SaldoAnt, [NotaEmpenho_SaldoPos]=@NotaEmpenho_SaldoPos, [NotaEmpenho_Itentificador]=@NotaEmpenho_Itentificador, [NotaEmpenho_DEmissao]=@NotaEmpenho_DEmissao, [NotaEmpenho_Valor]=@NotaEmpenho_Valor, [NotaEmpenho_Qtd]=@NotaEmpenho_Qtd, [NotaEmpenho_Ativo]=@NotaEmpenho_Ativo, [SaldoContrato_Codigo]=@SaldoContrato_Codigo  WHERE [NotaEmpenho_Codigo] = @NotaEmpenho_Codigo", GxErrorMask.GX_NOMASK,prmT003Y11)
             ,new CursorDef("T003Y12", "DELETE FROM [NotaEmpenho]  WHERE [NotaEmpenho_Codigo] = @NotaEmpenho_Codigo", GxErrorMask.GX_NOMASK,prmT003Y12)
             ,new CursorDef("T003Y13", "SELECT [Contrato_Codigo], [SaldoContrato_Saldo] FROM [SaldoContrato] WITH (NOLOCK) WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y13,1,0,true,false )
             ,new CursorDef("T003Y14", "SELECT [NotaEmpenho_Codigo] FROM [NotaEmpenho] WITH (NOLOCK) ORDER BY [NotaEmpenho_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003Y14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[13]);
                }
                stmt.SetParameter(8, (int)parms[14]);
                stmt.SetParameter(9, (int)parms[15]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
