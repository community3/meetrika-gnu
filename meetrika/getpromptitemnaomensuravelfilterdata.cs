/*
               File: GetPromptItemNaoMensuravelFilterData
        Description: Get Prompt Item Nao Mensuravel Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:22.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptitemnaomensuravelfilterdata : GXProcedure
   {
      public getpromptitemnaomensuravelfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptitemnaomensuravelfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV29DDOName = aP0_DDOName;
         this.AV27SearchTxt = aP1_SearchTxt;
         this.AV28SearchTxtTo = aP2_SearchTxtTo;
         this.AV33OptionsJson = "" ;
         this.AV36OptionsDescJson = "" ;
         this.AV38OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV29DDOName = aP0_DDOName;
         this.AV27SearchTxt = aP1_SearchTxt;
         this.AV28SearchTxtTo = aP2_SearchTxtTo;
         this.AV33OptionsJson = "" ;
         this.AV36OptionsDescJson = "" ;
         this.AV38OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
         return AV38OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptitemnaomensuravelfilterdata objgetpromptitemnaomensuravelfilterdata;
         objgetpromptitemnaomensuravelfilterdata = new getpromptitemnaomensuravelfilterdata();
         objgetpromptitemnaomensuravelfilterdata.AV29DDOName = aP0_DDOName;
         objgetpromptitemnaomensuravelfilterdata.AV27SearchTxt = aP1_SearchTxt;
         objgetpromptitemnaomensuravelfilterdata.AV28SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptitemnaomensuravelfilterdata.AV33OptionsJson = "" ;
         objgetpromptitemnaomensuravelfilterdata.AV36OptionsDescJson = "" ;
         objgetpromptitemnaomensuravelfilterdata.AV38OptionIndexesJson = "" ;
         objgetpromptitemnaomensuravelfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptitemnaomensuravelfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptitemnaomensuravelfilterdata);
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptitemnaomensuravelfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV32Options = (IGxCollection)(new GxSimpleCollection());
         AV35OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV37OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV29DDOName), "DDO_ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_AREATRABALHODESOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV29DDOName), "DDO_ITEMNAOMENSURAVEL_CODIGO") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_CODIGOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV29DDOName), "DDO_ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADITEMNAOMENSURAVEL_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV29DDOName), "DDO_REFERENCIAINM_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIAINM_DESCRICAOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV33OptionsJson = AV32Options.ToJSonString(false);
         AV36OptionsDescJson = AV35OptionsDesc.ToJSonString(false);
         AV38OptionIndexesJson = AV37OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV40Session.Get("PromptItemNaoMensuravelGridState"), "") == 0 )
         {
            AV42GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptItemNaoMensuravelGridState"), "");
         }
         else
         {
            AV42GridState.FromXml(AV40Session.Get("PromptItemNaoMensuravelGridState"), "");
         }
         AV64GXV1 = 1;
         while ( AV64GXV1 <= AV42GridState.gxTpr_Filtervalues.Count )
         {
            AV43GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV42GridState.gxTpr_Filtervalues.Item(AV64GXV1));
            if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_AREATRABALHOCOD") == 0 )
            {
               AV10TFItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
               AV11TFItemNaoMensuravel_AreaTrabalhoCod_To = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
            {
               AV12TFItemNaoMensuravel_AreaTrabalhoDes = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_AREATRABALHODES_SEL") == 0 )
            {
               AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO") == 0 )
            {
               AV14TFItemNaoMensuravel_Codigo = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_CODIGO_SEL") == 0 )
            {
               AV15TFItemNaoMensuravel_Codigo_Sel = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV16TFItemNaoMensuravel_Descricao = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_DESCRICAO_SEL") == 0 )
            {
               AV17TFItemNaoMensuravel_Descricao_Sel = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_VALOR") == 0 )
            {
               AV18TFItemNaoMensuravel_Valor = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, ".");
               AV19TFItemNaoMensuravel_Valor_To = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_CODIGO") == 0 )
            {
               AV20TFReferenciaINM_Codigo = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
               AV21TFReferenciaINM_Codigo_To = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO") == 0 )
            {
               AV22TFReferenciaINM_Descricao = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFREFERENCIAINM_DESCRICAO_SEL") == 0 )
            {
               AV23TFReferenciaINM_Descricao_Sel = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_TIPO_SEL") == 0 )
            {
               AV24TFItemNaoMensuravel_Tipo_SelsJson = AV43GridStateFilterValue.gxTpr_Value;
               AV25TFItemNaoMensuravel_Tipo_Sels.FromJSonString(AV24TFItemNaoMensuravel_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFITEMNAOMENSURAVEL_ATIVO_SEL") == 0 )
            {
               AV26TFItemNaoMensuravel_Ativo_Sel = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
            }
            AV64GXV1 = (int)(AV64GXV1+1);
         }
         if ( AV42GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV42GridState.gxTpr_Dynamicfilters.Item(1));
            AV45DynamicFiltersSelector1 = AV44GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
            {
               AV46DynamicFiltersOperator1 = AV44GridStateDynamicFilter.gxTpr_Operator;
               AV47ItemNaoMensuravel_Descricao1 = AV44GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
            {
               AV46DynamicFiltersOperator1 = AV44GridStateDynamicFilter.gxTpr_Operator;
               AV48ItemNaoMensuravel_AreaTrabalhoDes1 = AV44GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
            {
               AV46DynamicFiltersOperator1 = AV44GridStateDynamicFilter.gxTpr_Operator;
               AV49ReferenciaINM_Descricao1 = AV44GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV42GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV50DynamicFiltersEnabled2 = true;
               AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV42GridState.gxTpr_Dynamicfilters.Item(2));
               AV51DynamicFiltersSelector2 = AV44GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV44GridStateDynamicFilter.gxTpr_Operator;
                  AV53ItemNaoMensuravel_Descricao2 = AV44GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV44GridStateDynamicFilter.gxTpr_Operator;
                  AV54ItemNaoMensuravel_AreaTrabalhoDes2 = AV44GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV44GridStateDynamicFilter.gxTpr_Operator;
                  AV55ReferenciaINM_Descricao2 = AV44GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV42GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV56DynamicFiltersEnabled3 = true;
                  AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV42GridState.gxTpr_Dynamicfilters.Item(3));
                  AV57DynamicFiltersSelector3 = AV44GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 )
                  {
                     AV58DynamicFiltersOperator3 = AV44GridStateDynamicFilter.gxTpr_Operator;
                     AV59ItemNaoMensuravel_Descricao3 = AV44GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 )
                  {
                     AV58DynamicFiltersOperator3 = AV44GridStateDynamicFilter.gxTpr_Operator;
                     AV60ItemNaoMensuravel_AreaTrabalhoDes3 = AV44GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
                  {
                     AV58DynamicFiltersOperator3 = AV44GridStateDynamicFilter.gxTpr_Operator;
                     AV61ReferenciaINM_Descricao3 = AV44GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADITEMNAOMENSURAVEL_AREATRABALHODESOPTIONS' Routine */
         AV12TFItemNaoMensuravel_AreaTrabalhoDes = AV27SearchTxt;
         AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels ,
                                              AV45DynamicFiltersSelector1 ,
                                              AV46DynamicFiltersOperator1 ,
                                              AV47ItemNaoMensuravel_Descricao1 ,
                                              AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                              AV49ReferenciaINM_Descricao1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ItemNaoMensuravel_Descricao2 ,
                                              AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                              AV55ReferenciaINM_Descricao2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV59ItemNaoMensuravel_Descricao3 ,
                                              AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                              AV61ReferenciaINM_Descricao3 ,
                                              AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                              AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                              AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                              AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                              AV15TFItemNaoMensuravel_Codigo_Sel ,
                                              AV14TFItemNaoMensuravel_Codigo ,
                                              AV17TFItemNaoMensuravel_Descricao_Sel ,
                                              AV16TFItemNaoMensuravel_Descricao ,
                                              AV18TFItemNaoMensuravel_Valor ,
                                              AV19TFItemNaoMensuravel_Valor_To ,
                                              AV20TFReferenciaINM_Codigo ,
                                              AV21TFReferenciaINM_Codigo_To ,
                                              AV23TFReferenciaINM_Descricao_Sel ,
                                              AV22TFReferenciaINM_Descricao ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV26TFItemNaoMensuravel_Ativo_Sel ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A709ReferenciaINM_Codigo ,
                                              A716ItemNaoMensuravel_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV12TFItemNaoMensuravel_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes), "%", "");
         lV14TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo), 20, "%");
         lV16TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao), "%", "");
         lV22TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFReferenciaINM_Descricao), "%", "");
         /* Using cursor P00OG2 */
         pr_default.execute(0, new Object[] {lV47ItemNaoMensuravel_Descricao1, lV47ItemNaoMensuravel_Descricao1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV49ReferenciaINM_Descricao1, lV49ReferenciaINM_Descricao1, lV53ItemNaoMensuravel_Descricao2, lV53ItemNaoMensuravel_Descricao2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV55ReferenciaINM_Descricao2, lV55ReferenciaINM_Descricao2, lV59ItemNaoMensuravel_Descricao3, lV59ItemNaoMensuravel_Descricao3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV61ReferenciaINM_Descricao3, lV61ReferenciaINM_Descricao3, AV10TFItemNaoMensuravel_AreaTrabalhoCod, AV11TFItemNaoMensuravel_AreaTrabalhoCod_To, lV12TFItemNaoMensuravel_AreaTrabalhoDes, AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel, lV14TFItemNaoMensuravel_Codigo, AV15TFItemNaoMensuravel_Codigo_Sel, lV16TFItemNaoMensuravel_Descricao, AV17TFItemNaoMensuravel_Descricao_Sel, AV18TFItemNaoMensuravel_Valor, AV19TFItemNaoMensuravel_Valor_To, AV20TFReferenciaINM_Codigo, AV21TFReferenciaINM_Codigo_To, lV22TFReferenciaINM_Descricao, AV23TFReferenciaINM_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKOG2 = false;
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00OG2_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A716ItemNaoMensuravel_Ativo = P00OG2_A716ItemNaoMensuravel_Ativo[0];
            A717ItemNaoMensuravel_Tipo = P00OG2_A717ItemNaoMensuravel_Tipo[0];
            A709ReferenciaINM_Codigo = P00OG2_A709ReferenciaINM_Codigo[0];
            A719ItemNaoMensuravel_Valor = P00OG2_A719ItemNaoMensuravel_Valor[0];
            A715ItemNaoMensuravel_Codigo = P00OG2_A715ItemNaoMensuravel_Codigo[0];
            A710ReferenciaINM_Descricao = P00OG2_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG2_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG2_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A714ItemNaoMensuravel_Descricao = P00OG2_A714ItemNaoMensuravel_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG2_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG2_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A710ReferenciaINM_Descricao = P00OG2_A710ReferenciaINM_Descricao[0];
            AV39count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00OG2_A718ItemNaoMensuravel_AreaTrabalhoCod[0] == A718ItemNaoMensuravel_AreaTrabalhoCod ) )
            {
               BRKOG2 = false;
               A715ItemNaoMensuravel_Codigo = P00OG2_A715ItemNaoMensuravel_Codigo[0];
               AV39count = (long)(AV39count+1);
               BRKOG2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A720ItemNaoMensuravel_AreaTrabalhoDes)) )
            {
               AV31Option = A720ItemNaoMensuravel_AreaTrabalhoDes;
               AV30InsertIndex = 1;
               while ( ( AV30InsertIndex <= AV32Options.Count ) && ( StringUtil.StrCmp(((String)AV32Options.Item(AV30InsertIndex)), AV31Option) < 0 ) )
               {
                  AV30InsertIndex = (int)(AV30InsertIndex+1);
               }
               AV32Options.Add(AV31Option, AV30InsertIndex);
               AV37OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV39count), "Z,ZZZ,ZZZ,ZZ9")), AV30InsertIndex);
            }
            if ( AV32Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOG2 )
            {
               BRKOG2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADITEMNAOMENSURAVEL_CODIGOOPTIONS' Routine */
         AV14TFItemNaoMensuravel_Codigo = AV27SearchTxt;
         AV15TFItemNaoMensuravel_Codigo_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels ,
                                              AV45DynamicFiltersSelector1 ,
                                              AV46DynamicFiltersOperator1 ,
                                              AV47ItemNaoMensuravel_Descricao1 ,
                                              AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                              AV49ReferenciaINM_Descricao1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ItemNaoMensuravel_Descricao2 ,
                                              AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                              AV55ReferenciaINM_Descricao2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV59ItemNaoMensuravel_Descricao3 ,
                                              AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                              AV61ReferenciaINM_Descricao3 ,
                                              AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                              AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                              AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                              AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                              AV15TFItemNaoMensuravel_Codigo_Sel ,
                                              AV14TFItemNaoMensuravel_Codigo ,
                                              AV17TFItemNaoMensuravel_Descricao_Sel ,
                                              AV16TFItemNaoMensuravel_Descricao ,
                                              AV18TFItemNaoMensuravel_Valor ,
                                              AV19TFItemNaoMensuravel_Valor_To ,
                                              AV20TFReferenciaINM_Codigo ,
                                              AV21TFReferenciaINM_Codigo_To ,
                                              AV23TFReferenciaINM_Descricao_Sel ,
                                              AV22TFReferenciaINM_Descricao ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV26TFItemNaoMensuravel_Ativo_Sel ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A709ReferenciaINM_Codigo ,
                                              A716ItemNaoMensuravel_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV12TFItemNaoMensuravel_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes), "%", "");
         lV14TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo), 20, "%");
         lV16TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao), "%", "");
         lV22TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFReferenciaINM_Descricao), "%", "");
         /* Using cursor P00OG3 */
         pr_default.execute(1, new Object[] {lV47ItemNaoMensuravel_Descricao1, lV47ItemNaoMensuravel_Descricao1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV49ReferenciaINM_Descricao1, lV49ReferenciaINM_Descricao1, lV53ItemNaoMensuravel_Descricao2, lV53ItemNaoMensuravel_Descricao2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV55ReferenciaINM_Descricao2, lV55ReferenciaINM_Descricao2, lV59ItemNaoMensuravel_Descricao3, lV59ItemNaoMensuravel_Descricao3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV61ReferenciaINM_Descricao3, lV61ReferenciaINM_Descricao3, AV10TFItemNaoMensuravel_AreaTrabalhoCod, AV11TFItemNaoMensuravel_AreaTrabalhoCod_To, lV12TFItemNaoMensuravel_AreaTrabalhoDes, AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel, lV14TFItemNaoMensuravel_Codigo, AV15TFItemNaoMensuravel_Codigo_Sel, lV16TFItemNaoMensuravel_Descricao, AV17TFItemNaoMensuravel_Descricao_Sel, AV18TFItemNaoMensuravel_Valor, AV19TFItemNaoMensuravel_Valor_To, AV20TFReferenciaINM_Codigo, AV21TFReferenciaINM_Codigo_To, lV22TFReferenciaINM_Descricao, AV23TFReferenciaINM_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOG4 = false;
            A715ItemNaoMensuravel_Codigo = P00OG3_A715ItemNaoMensuravel_Codigo[0];
            A716ItemNaoMensuravel_Ativo = P00OG3_A716ItemNaoMensuravel_Ativo[0];
            A717ItemNaoMensuravel_Tipo = P00OG3_A717ItemNaoMensuravel_Tipo[0];
            A709ReferenciaINM_Codigo = P00OG3_A709ReferenciaINM_Codigo[0];
            A719ItemNaoMensuravel_Valor = P00OG3_A719ItemNaoMensuravel_Valor[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00OG3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A710ReferenciaINM_Descricao = P00OG3_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG3_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG3_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A714ItemNaoMensuravel_Descricao = P00OG3_A714ItemNaoMensuravel_Descricao[0];
            A710ReferenciaINM_Descricao = P00OG3_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG3_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG3_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            AV39count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OG3_A715ItemNaoMensuravel_Codigo[0], A715ItemNaoMensuravel_Codigo) == 0 ) )
            {
               BRKOG4 = false;
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00OG3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               AV39count = (long)(AV39count+1);
               BRKOG4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A715ItemNaoMensuravel_Codigo)) )
            {
               AV31Option = A715ItemNaoMensuravel_Codigo;
               AV32Options.Add(AV31Option, 0);
               AV37OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV39count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV32Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOG4 )
            {
               BRKOG4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADITEMNAOMENSURAVEL_DESCRICAOOPTIONS' Routine */
         AV16TFItemNaoMensuravel_Descricao = AV27SearchTxt;
         AV17TFItemNaoMensuravel_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels ,
                                              AV45DynamicFiltersSelector1 ,
                                              AV46DynamicFiltersOperator1 ,
                                              AV47ItemNaoMensuravel_Descricao1 ,
                                              AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                              AV49ReferenciaINM_Descricao1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ItemNaoMensuravel_Descricao2 ,
                                              AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                              AV55ReferenciaINM_Descricao2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV59ItemNaoMensuravel_Descricao3 ,
                                              AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                              AV61ReferenciaINM_Descricao3 ,
                                              AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                              AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                              AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                              AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                              AV15TFItemNaoMensuravel_Codigo_Sel ,
                                              AV14TFItemNaoMensuravel_Codigo ,
                                              AV17TFItemNaoMensuravel_Descricao_Sel ,
                                              AV16TFItemNaoMensuravel_Descricao ,
                                              AV18TFItemNaoMensuravel_Valor ,
                                              AV19TFItemNaoMensuravel_Valor_To ,
                                              AV20TFReferenciaINM_Codigo ,
                                              AV21TFReferenciaINM_Codigo_To ,
                                              AV23TFReferenciaINM_Descricao_Sel ,
                                              AV22TFReferenciaINM_Descricao ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV26TFItemNaoMensuravel_Ativo_Sel ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A709ReferenciaINM_Codigo ,
                                              A716ItemNaoMensuravel_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV12TFItemNaoMensuravel_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes), "%", "");
         lV14TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo), 20, "%");
         lV16TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao), "%", "");
         lV22TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFReferenciaINM_Descricao), "%", "");
         /* Using cursor P00OG4 */
         pr_default.execute(2, new Object[] {lV47ItemNaoMensuravel_Descricao1, lV47ItemNaoMensuravel_Descricao1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV49ReferenciaINM_Descricao1, lV49ReferenciaINM_Descricao1, lV53ItemNaoMensuravel_Descricao2, lV53ItemNaoMensuravel_Descricao2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV55ReferenciaINM_Descricao2, lV55ReferenciaINM_Descricao2, lV59ItemNaoMensuravel_Descricao3, lV59ItemNaoMensuravel_Descricao3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV61ReferenciaINM_Descricao3, lV61ReferenciaINM_Descricao3, AV10TFItemNaoMensuravel_AreaTrabalhoCod, AV11TFItemNaoMensuravel_AreaTrabalhoCod_To, lV12TFItemNaoMensuravel_AreaTrabalhoDes, AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel, lV14TFItemNaoMensuravel_Codigo, AV15TFItemNaoMensuravel_Codigo_Sel, lV16TFItemNaoMensuravel_Descricao, AV17TFItemNaoMensuravel_Descricao_Sel, AV18TFItemNaoMensuravel_Valor, AV19TFItemNaoMensuravel_Valor_To, AV20TFReferenciaINM_Codigo, AV21TFReferenciaINM_Codigo_To, lV22TFReferenciaINM_Descricao, AV23TFReferenciaINM_Descricao_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKOG6 = false;
            A714ItemNaoMensuravel_Descricao = P00OG4_A714ItemNaoMensuravel_Descricao[0];
            A716ItemNaoMensuravel_Ativo = P00OG4_A716ItemNaoMensuravel_Ativo[0];
            A717ItemNaoMensuravel_Tipo = P00OG4_A717ItemNaoMensuravel_Tipo[0];
            A709ReferenciaINM_Codigo = P00OG4_A709ReferenciaINM_Codigo[0];
            A719ItemNaoMensuravel_Valor = P00OG4_A719ItemNaoMensuravel_Valor[0];
            A715ItemNaoMensuravel_Codigo = P00OG4_A715ItemNaoMensuravel_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00OG4_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A710ReferenciaINM_Descricao = P00OG4_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG4_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG4_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A710ReferenciaINM_Descricao = P00OG4_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG4_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG4_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            AV39count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00OG4_A714ItemNaoMensuravel_Descricao[0], A714ItemNaoMensuravel_Descricao) == 0 ) )
            {
               BRKOG6 = false;
               A715ItemNaoMensuravel_Codigo = P00OG4_A715ItemNaoMensuravel_Codigo[0];
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00OG4_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               AV39count = (long)(AV39count+1);
               BRKOG6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A714ItemNaoMensuravel_Descricao)) )
            {
               AV31Option = A714ItemNaoMensuravel_Descricao;
               AV32Options.Add(AV31Option, 0);
               AV37OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV39count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV32Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOG6 )
            {
               BRKOG6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADREFERENCIAINM_DESCRICAOOPTIONS' Routine */
         AV22TFReferenciaINM_Descricao = AV27SearchTxt;
         AV23TFReferenciaINM_Descricao_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A717ItemNaoMensuravel_Tipo ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels ,
                                              AV45DynamicFiltersSelector1 ,
                                              AV46DynamicFiltersOperator1 ,
                                              AV47ItemNaoMensuravel_Descricao1 ,
                                              AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                              AV49ReferenciaINM_Descricao1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ItemNaoMensuravel_Descricao2 ,
                                              AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                              AV55ReferenciaINM_Descricao2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV59ItemNaoMensuravel_Descricao3 ,
                                              AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                              AV61ReferenciaINM_Descricao3 ,
                                              AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                              AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                              AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                              AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                              AV15TFItemNaoMensuravel_Codigo_Sel ,
                                              AV14TFItemNaoMensuravel_Codigo ,
                                              AV17TFItemNaoMensuravel_Descricao_Sel ,
                                              AV16TFItemNaoMensuravel_Descricao ,
                                              AV18TFItemNaoMensuravel_Valor ,
                                              AV19TFItemNaoMensuravel_Valor_To ,
                                              AV20TFReferenciaINM_Codigo ,
                                              AV21TFReferenciaINM_Codigo_To ,
                                              AV23TFReferenciaINM_Descricao_Sel ,
                                              AV22TFReferenciaINM_Descricao ,
                                              AV25TFItemNaoMensuravel_Tipo_Sels.Count ,
                                              AV26TFItemNaoMensuravel_Ativo_Sel ,
                                              A714ItemNaoMensuravel_Descricao ,
                                              A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                              A710ReferenciaINM_Descricao ,
                                              A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                              A715ItemNaoMensuravel_Codigo ,
                                              A719ItemNaoMensuravel_Valor ,
                                              A709ReferenciaINM_Codigo ,
                                              A716ItemNaoMensuravel_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV47ItemNaoMensuravel_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV49ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV49ReferenciaINM_Descricao1), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV53ItemNaoMensuravel_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV55ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV55ReferenciaINM_Descricao2), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV59ItemNaoMensuravel_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV61ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV61ReferenciaINM_Descricao3), "%", "");
         lV12TFItemNaoMensuravel_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes), "%", "");
         lV14TFItemNaoMensuravel_Codigo = StringUtil.PadR( StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo), 20, "%");
         lV16TFItemNaoMensuravel_Descricao = StringUtil.Concat( StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao), "%", "");
         lV22TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFReferenciaINM_Descricao), "%", "");
         /* Using cursor P00OG5 */
         pr_default.execute(3, new Object[] {lV47ItemNaoMensuravel_Descricao1, lV47ItemNaoMensuravel_Descricao1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV48ItemNaoMensuravel_AreaTrabalhoDes1, lV49ReferenciaINM_Descricao1, lV49ReferenciaINM_Descricao1, lV53ItemNaoMensuravel_Descricao2, lV53ItemNaoMensuravel_Descricao2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV54ItemNaoMensuravel_AreaTrabalhoDes2, lV55ReferenciaINM_Descricao2, lV55ReferenciaINM_Descricao2, lV59ItemNaoMensuravel_Descricao3, lV59ItemNaoMensuravel_Descricao3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV60ItemNaoMensuravel_AreaTrabalhoDes3, lV61ReferenciaINM_Descricao3, lV61ReferenciaINM_Descricao3, AV10TFItemNaoMensuravel_AreaTrabalhoCod, AV11TFItemNaoMensuravel_AreaTrabalhoCod_To, lV12TFItemNaoMensuravel_AreaTrabalhoDes, AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel, lV14TFItemNaoMensuravel_Codigo, AV15TFItemNaoMensuravel_Codigo_Sel, lV16TFItemNaoMensuravel_Descricao, AV17TFItemNaoMensuravel_Descricao_Sel, AV18TFItemNaoMensuravel_Valor, AV19TFItemNaoMensuravel_Valor_To, AV20TFReferenciaINM_Codigo, AV21TFReferenciaINM_Codigo_To, lV22TFReferenciaINM_Descricao, AV23TFReferenciaINM_Descricao_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKOG8 = false;
            A709ReferenciaINM_Codigo = P00OG5_A709ReferenciaINM_Codigo[0];
            A716ItemNaoMensuravel_Ativo = P00OG5_A716ItemNaoMensuravel_Ativo[0];
            A717ItemNaoMensuravel_Tipo = P00OG5_A717ItemNaoMensuravel_Tipo[0];
            A719ItemNaoMensuravel_Valor = P00OG5_A719ItemNaoMensuravel_Valor[0];
            A715ItemNaoMensuravel_Codigo = P00OG5_A715ItemNaoMensuravel_Codigo[0];
            A718ItemNaoMensuravel_AreaTrabalhoCod = P00OG5_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A710ReferenciaINM_Descricao = P00OG5_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG5_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG5_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            A714ItemNaoMensuravel_Descricao = P00OG5_A714ItemNaoMensuravel_Descricao[0];
            A710ReferenciaINM_Descricao = P00OG5_A710ReferenciaINM_Descricao[0];
            A720ItemNaoMensuravel_AreaTrabalhoDes = P00OG5_A720ItemNaoMensuravel_AreaTrabalhoDes[0];
            n720ItemNaoMensuravel_AreaTrabalhoDes = P00OG5_n720ItemNaoMensuravel_AreaTrabalhoDes[0];
            AV39count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00OG5_A709ReferenciaINM_Codigo[0] == A709ReferenciaINM_Codigo ) )
            {
               BRKOG8 = false;
               A715ItemNaoMensuravel_Codigo = P00OG5_A715ItemNaoMensuravel_Codigo[0];
               A718ItemNaoMensuravel_AreaTrabalhoCod = P00OG5_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               AV39count = (long)(AV39count+1);
               BRKOG8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A710ReferenciaINM_Descricao)) )
            {
               AV31Option = A710ReferenciaINM_Descricao;
               AV30InsertIndex = 1;
               while ( ( AV30InsertIndex <= AV32Options.Count ) && ( StringUtil.StrCmp(((String)AV32Options.Item(AV30InsertIndex)), AV31Option) < 0 ) )
               {
                  AV30InsertIndex = (int)(AV30InsertIndex+1);
               }
               AV32Options.Add(AV31Option, AV30InsertIndex);
               AV37OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV39count), "Z,ZZZ,ZZZ,ZZ9")), AV30InsertIndex);
            }
            if ( AV32Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOG8 )
            {
               BRKOG8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV32Options = new GxSimpleCollection();
         AV35OptionsDesc = new GxSimpleCollection();
         AV37OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV40Session = context.GetSession();
         AV42GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV43GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFItemNaoMensuravel_AreaTrabalhoDes = "";
         AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel = "";
         AV14TFItemNaoMensuravel_Codigo = "";
         AV15TFItemNaoMensuravel_Codigo_Sel = "";
         AV16TFItemNaoMensuravel_Descricao = "";
         AV17TFItemNaoMensuravel_Descricao_Sel = "";
         AV22TFReferenciaINM_Descricao = "";
         AV23TFReferenciaINM_Descricao_Sel = "";
         AV24TFItemNaoMensuravel_Tipo_SelsJson = "";
         AV25TFItemNaoMensuravel_Tipo_Sels = new GxSimpleCollection();
         AV44GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV45DynamicFiltersSelector1 = "";
         AV47ItemNaoMensuravel_Descricao1 = "";
         AV48ItemNaoMensuravel_AreaTrabalhoDes1 = "";
         AV49ReferenciaINM_Descricao1 = "";
         AV51DynamicFiltersSelector2 = "";
         AV53ItemNaoMensuravel_Descricao2 = "";
         AV54ItemNaoMensuravel_AreaTrabalhoDes2 = "";
         AV55ReferenciaINM_Descricao2 = "";
         AV57DynamicFiltersSelector3 = "";
         AV59ItemNaoMensuravel_Descricao3 = "";
         AV60ItemNaoMensuravel_AreaTrabalhoDes3 = "";
         AV61ReferenciaINM_Descricao3 = "";
         scmdbuf = "";
         lV47ItemNaoMensuravel_Descricao1 = "";
         lV48ItemNaoMensuravel_AreaTrabalhoDes1 = "";
         lV49ReferenciaINM_Descricao1 = "";
         lV53ItemNaoMensuravel_Descricao2 = "";
         lV54ItemNaoMensuravel_AreaTrabalhoDes2 = "";
         lV55ReferenciaINM_Descricao2 = "";
         lV59ItemNaoMensuravel_Descricao3 = "";
         lV60ItemNaoMensuravel_AreaTrabalhoDes3 = "";
         lV61ReferenciaINM_Descricao3 = "";
         lV12TFItemNaoMensuravel_AreaTrabalhoDes = "";
         lV14TFItemNaoMensuravel_Codigo = "";
         lV16TFItemNaoMensuravel_Descricao = "";
         lV22TFReferenciaINM_Descricao = "";
         A714ItemNaoMensuravel_Descricao = "";
         A720ItemNaoMensuravel_AreaTrabalhoDes = "";
         A710ReferenciaINM_Descricao = "";
         A715ItemNaoMensuravel_Codigo = "";
         P00OG2_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00OG2_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00OG2_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00OG2_A709ReferenciaINM_Codigo = new int[1] ;
         P00OG2_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00OG2_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00OG2_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00OG2_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         P00OG2_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         P00OG2_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         AV31Option = "";
         P00OG3_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00OG3_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00OG3_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00OG3_A709ReferenciaINM_Codigo = new int[1] ;
         P00OG3_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00OG3_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00OG3_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00OG3_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         P00OG3_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         P00OG3_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00OG4_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         P00OG4_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00OG4_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00OG4_A709ReferenciaINM_Codigo = new int[1] ;
         P00OG4_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00OG4_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00OG4_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00OG4_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00OG4_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         P00OG4_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         P00OG5_A709ReferenciaINM_Codigo = new int[1] ;
         P00OG5_A716ItemNaoMensuravel_Ativo = new bool[] {false} ;
         P00OG5_A717ItemNaoMensuravel_Tipo = new short[1] ;
         P00OG5_A719ItemNaoMensuravel_Valor = new decimal[1] ;
         P00OG5_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         P00OG5_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         P00OG5_A710ReferenciaINM_Descricao = new String[] {""} ;
         P00OG5_A720ItemNaoMensuravel_AreaTrabalhoDes = new String[] {""} ;
         P00OG5_n720ItemNaoMensuravel_AreaTrabalhoDes = new bool[] {false} ;
         P00OG5_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptitemnaomensuravelfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OG2_A718ItemNaoMensuravel_AreaTrabalhoCod, P00OG2_A716ItemNaoMensuravel_Ativo, P00OG2_A717ItemNaoMensuravel_Tipo, P00OG2_A709ReferenciaINM_Codigo, P00OG2_A719ItemNaoMensuravel_Valor, P00OG2_A715ItemNaoMensuravel_Codigo, P00OG2_A710ReferenciaINM_Descricao, P00OG2_A720ItemNaoMensuravel_AreaTrabalhoDes, P00OG2_n720ItemNaoMensuravel_AreaTrabalhoDes, P00OG2_A714ItemNaoMensuravel_Descricao
               }
               , new Object[] {
               P00OG3_A715ItemNaoMensuravel_Codigo, P00OG3_A716ItemNaoMensuravel_Ativo, P00OG3_A717ItemNaoMensuravel_Tipo, P00OG3_A709ReferenciaINM_Codigo, P00OG3_A719ItemNaoMensuravel_Valor, P00OG3_A718ItemNaoMensuravel_AreaTrabalhoCod, P00OG3_A710ReferenciaINM_Descricao, P00OG3_A720ItemNaoMensuravel_AreaTrabalhoDes, P00OG3_n720ItemNaoMensuravel_AreaTrabalhoDes, P00OG3_A714ItemNaoMensuravel_Descricao
               }
               , new Object[] {
               P00OG4_A714ItemNaoMensuravel_Descricao, P00OG4_A716ItemNaoMensuravel_Ativo, P00OG4_A717ItemNaoMensuravel_Tipo, P00OG4_A709ReferenciaINM_Codigo, P00OG4_A719ItemNaoMensuravel_Valor, P00OG4_A715ItemNaoMensuravel_Codigo, P00OG4_A718ItemNaoMensuravel_AreaTrabalhoCod, P00OG4_A710ReferenciaINM_Descricao, P00OG4_A720ItemNaoMensuravel_AreaTrabalhoDes, P00OG4_n720ItemNaoMensuravel_AreaTrabalhoDes
               }
               , new Object[] {
               P00OG5_A709ReferenciaINM_Codigo, P00OG5_A716ItemNaoMensuravel_Ativo, P00OG5_A717ItemNaoMensuravel_Tipo, P00OG5_A719ItemNaoMensuravel_Valor, P00OG5_A715ItemNaoMensuravel_Codigo, P00OG5_A718ItemNaoMensuravel_AreaTrabalhoCod, P00OG5_A710ReferenciaINM_Descricao, P00OG5_A720ItemNaoMensuravel_AreaTrabalhoDes, P00OG5_n720ItemNaoMensuravel_AreaTrabalhoDes, P00OG5_A714ItemNaoMensuravel_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV26TFItemNaoMensuravel_Ativo_Sel ;
      private short AV46DynamicFiltersOperator1 ;
      private short AV52DynamicFiltersOperator2 ;
      private short AV58DynamicFiltersOperator3 ;
      private short A717ItemNaoMensuravel_Tipo ;
      private int AV64GXV1 ;
      private int AV10TFItemNaoMensuravel_AreaTrabalhoCod ;
      private int AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ;
      private int AV20TFReferenciaINM_Codigo ;
      private int AV21TFReferenciaINM_Codigo_To ;
      private int AV25TFItemNaoMensuravel_Tipo_Sels_Count ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int A709ReferenciaINM_Codigo ;
      private int AV30InsertIndex ;
      private long AV39count ;
      private decimal AV18TFItemNaoMensuravel_Valor ;
      private decimal AV19TFItemNaoMensuravel_Valor_To ;
      private decimal A719ItemNaoMensuravel_Valor ;
      private String AV14TFItemNaoMensuravel_Codigo ;
      private String AV15TFItemNaoMensuravel_Codigo_Sel ;
      private String scmdbuf ;
      private String lV14TFItemNaoMensuravel_Codigo ;
      private String A715ItemNaoMensuravel_Codigo ;
      private bool returnInSub ;
      private bool AV50DynamicFiltersEnabled2 ;
      private bool AV56DynamicFiltersEnabled3 ;
      private bool A716ItemNaoMensuravel_Ativo ;
      private bool BRKOG2 ;
      private bool n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool BRKOG4 ;
      private bool BRKOG6 ;
      private bool BRKOG8 ;
      private String AV38OptionIndexesJson ;
      private String AV33OptionsJson ;
      private String AV36OptionsDescJson ;
      private String AV24TFItemNaoMensuravel_Tipo_SelsJson ;
      private String AV47ItemNaoMensuravel_Descricao1 ;
      private String AV53ItemNaoMensuravel_Descricao2 ;
      private String AV59ItemNaoMensuravel_Descricao3 ;
      private String lV47ItemNaoMensuravel_Descricao1 ;
      private String lV53ItemNaoMensuravel_Descricao2 ;
      private String lV59ItemNaoMensuravel_Descricao3 ;
      private String A714ItemNaoMensuravel_Descricao ;
      private String AV29DDOName ;
      private String AV27SearchTxt ;
      private String AV28SearchTxtTo ;
      private String AV12TFItemNaoMensuravel_AreaTrabalhoDes ;
      private String AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ;
      private String AV16TFItemNaoMensuravel_Descricao ;
      private String AV17TFItemNaoMensuravel_Descricao_Sel ;
      private String AV22TFReferenciaINM_Descricao ;
      private String AV23TFReferenciaINM_Descricao_Sel ;
      private String AV45DynamicFiltersSelector1 ;
      private String AV48ItemNaoMensuravel_AreaTrabalhoDes1 ;
      private String AV49ReferenciaINM_Descricao1 ;
      private String AV51DynamicFiltersSelector2 ;
      private String AV54ItemNaoMensuravel_AreaTrabalhoDes2 ;
      private String AV55ReferenciaINM_Descricao2 ;
      private String AV57DynamicFiltersSelector3 ;
      private String AV60ItemNaoMensuravel_AreaTrabalhoDes3 ;
      private String AV61ReferenciaINM_Descricao3 ;
      private String lV48ItemNaoMensuravel_AreaTrabalhoDes1 ;
      private String lV49ReferenciaINM_Descricao1 ;
      private String lV54ItemNaoMensuravel_AreaTrabalhoDes2 ;
      private String lV55ReferenciaINM_Descricao2 ;
      private String lV60ItemNaoMensuravel_AreaTrabalhoDes3 ;
      private String lV61ReferenciaINM_Descricao3 ;
      private String lV12TFItemNaoMensuravel_AreaTrabalhoDes ;
      private String lV16TFItemNaoMensuravel_Descricao ;
      private String lV22TFReferenciaINM_Descricao ;
      private String A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String A710ReferenciaINM_Descricao ;
      private String AV31Option ;
      private IGxSession AV40Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00OG2_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private bool[] P00OG2_A716ItemNaoMensuravel_Ativo ;
      private short[] P00OG2_A717ItemNaoMensuravel_Tipo ;
      private int[] P00OG2_A709ReferenciaINM_Codigo ;
      private decimal[] P00OG2_A719ItemNaoMensuravel_Valor ;
      private String[] P00OG2_A715ItemNaoMensuravel_Codigo ;
      private String[] P00OG2_A710ReferenciaINM_Descricao ;
      private String[] P00OG2_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] P00OG2_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] P00OG2_A714ItemNaoMensuravel_Descricao ;
      private String[] P00OG3_A715ItemNaoMensuravel_Codigo ;
      private bool[] P00OG3_A716ItemNaoMensuravel_Ativo ;
      private short[] P00OG3_A717ItemNaoMensuravel_Tipo ;
      private int[] P00OG3_A709ReferenciaINM_Codigo ;
      private decimal[] P00OG3_A719ItemNaoMensuravel_Valor ;
      private int[] P00OG3_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] P00OG3_A710ReferenciaINM_Descricao ;
      private String[] P00OG3_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] P00OG3_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] P00OG3_A714ItemNaoMensuravel_Descricao ;
      private String[] P00OG4_A714ItemNaoMensuravel_Descricao ;
      private bool[] P00OG4_A716ItemNaoMensuravel_Ativo ;
      private short[] P00OG4_A717ItemNaoMensuravel_Tipo ;
      private int[] P00OG4_A709ReferenciaINM_Codigo ;
      private decimal[] P00OG4_A719ItemNaoMensuravel_Valor ;
      private String[] P00OG4_A715ItemNaoMensuravel_Codigo ;
      private int[] P00OG4_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] P00OG4_A710ReferenciaINM_Descricao ;
      private String[] P00OG4_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] P00OG4_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private int[] P00OG5_A709ReferenciaINM_Codigo ;
      private bool[] P00OG5_A716ItemNaoMensuravel_Ativo ;
      private short[] P00OG5_A717ItemNaoMensuravel_Tipo ;
      private decimal[] P00OG5_A719ItemNaoMensuravel_Valor ;
      private String[] P00OG5_A715ItemNaoMensuravel_Codigo ;
      private int[] P00OG5_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] P00OG5_A710ReferenciaINM_Descricao ;
      private String[] P00OG5_A720ItemNaoMensuravel_AreaTrabalhoDes ;
      private bool[] P00OG5_n720ItemNaoMensuravel_AreaTrabalhoDes ;
      private String[] P00OG5_A714ItemNaoMensuravel_Descricao ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV25TFItemNaoMensuravel_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV37OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV42GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV43GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV44GridStateDynamicFilter ;
   }

   public class getpromptitemnaomensuravelfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OG2( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV25TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV45DynamicFiltersSelector1 ,
                                             short AV46DynamicFiltersOperator1 ,
                                             String AV47ItemNaoMensuravel_Descricao1 ,
                                             String AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                             String AV49ReferenciaINM_Descricao1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ItemNaoMensuravel_Descricao2 ,
                                             String AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                             String AV55ReferenciaINM_Descricao2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV59ItemNaoMensuravel_Descricao3 ,
                                             String AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                             String AV61ReferenciaINM_Descricao3 ,
                                             int AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                             String AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                             String AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                             String AV15TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV14TFItemNaoMensuravel_Codigo ,
                                             String AV17TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV16TFItemNaoMensuravel_Descricao ,
                                             decimal AV18TFItemNaoMensuravel_Valor ,
                                             decimal AV19TFItemNaoMensuravel_Valor_To ,
                                             int AV20TFReferenciaINM_Codigo ,
                                             int AV21TFReferenciaINM_Codigo_To ,
                                             String AV23TFReferenciaINM_Descricao_Sel ,
                                             String AV22TFReferenciaINM_Descricao ,
                                             int AV25TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV26TFItemNaoMensuravel_Ativo_Sel ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             int A709ReferenciaINM_Codigo ,
                                             bool A716ItemNaoMensuravel_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [32] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_Codigo], T3.[ReferenciaINM_Descricao], T2.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes, T1.[ItemNaoMensuravel_Descricao] FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod]) INNER JOIN [ReferenciaINM] T3 WITH (NOLOCK) ON T3.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo])";
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV10TFItemNaoMensuravel_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV11TFItemNaoMensuravel_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFItemNaoMensuravel_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFItemNaoMensuravel_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! (0==AV20TFReferenciaINM_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! (0==AV21TFReferenciaINM_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFReferenciaINM_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( AV25TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ItemNaoMensuravel_AreaTrabalhoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OG3( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV25TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV45DynamicFiltersSelector1 ,
                                             short AV46DynamicFiltersOperator1 ,
                                             String AV47ItemNaoMensuravel_Descricao1 ,
                                             String AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                             String AV49ReferenciaINM_Descricao1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ItemNaoMensuravel_Descricao2 ,
                                             String AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                             String AV55ReferenciaINM_Descricao2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV59ItemNaoMensuravel_Descricao3 ,
                                             String AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                             String AV61ReferenciaINM_Descricao3 ,
                                             int AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                             String AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                             String AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                             String AV15TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV14TFItemNaoMensuravel_Codigo ,
                                             String AV17TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV16TFItemNaoMensuravel_Descricao ,
                                             decimal AV18TFItemNaoMensuravel_Valor ,
                                             decimal AV19TFItemNaoMensuravel_Valor_To ,
                                             int AV20TFReferenciaINM_Codigo ,
                                             int AV21TFReferenciaINM_Codigo_To ,
                                             String AV23TFReferenciaINM_Descricao_Sel ,
                                             String AV22TFReferenciaINM_Descricao ,
                                             int AV25TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV26TFItemNaoMensuravel_Ativo_Sel ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             int A709ReferenciaINM_Codigo ,
                                             bool A716ItemNaoMensuravel_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [32] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ItemNaoMensuravel_Codigo], T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, T2.[ReferenciaINM_Descricao], T3.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes, T1.[ItemNaoMensuravel_Descricao] FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV10TFItemNaoMensuravel_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV11TFItemNaoMensuravel_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFItemNaoMensuravel_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFItemNaoMensuravel_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (0==AV20TFReferenciaINM_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! (0==AV21TFReferenciaINM_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFReferenciaINM_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( AV25TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ItemNaoMensuravel_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00OG4( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV25TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV45DynamicFiltersSelector1 ,
                                             short AV46DynamicFiltersOperator1 ,
                                             String AV47ItemNaoMensuravel_Descricao1 ,
                                             String AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                             String AV49ReferenciaINM_Descricao1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ItemNaoMensuravel_Descricao2 ,
                                             String AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                             String AV55ReferenciaINM_Descricao2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV59ItemNaoMensuravel_Descricao3 ,
                                             String AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                             String AV61ReferenciaINM_Descricao3 ,
                                             int AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                             String AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                             String AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                             String AV15TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV14TFItemNaoMensuravel_Codigo ,
                                             String AV17TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV16TFItemNaoMensuravel_Descricao ,
                                             decimal AV18TFItemNaoMensuravel_Valor ,
                                             decimal AV19TFItemNaoMensuravel_Valor_To ,
                                             int AV20TFReferenciaINM_Codigo ,
                                             int AV21TFReferenciaINM_Codigo_To ,
                                             String AV23TFReferenciaINM_Descricao_Sel ,
                                             String AV22TFReferenciaINM_Descricao ,
                                             int AV25TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV26TFItemNaoMensuravel_Ativo_Sel ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             int A709ReferenciaINM_Codigo ,
                                             bool A716ItemNaoMensuravel_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [32] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ItemNaoMensuravel_Descricao], T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_Codigo], T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, T2.[ReferenciaINM_Descricao], T3.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV10TFItemNaoMensuravel_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV11TFItemNaoMensuravel_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFItemNaoMensuravel_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFItemNaoMensuravel_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! (0==AV20TFReferenciaINM_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! (0==AV21TFReferenciaINM_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFReferenciaINM_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( AV25TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ItemNaoMensuravel_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00OG5( IGxContext context ,
                                             short A717ItemNaoMensuravel_Tipo ,
                                             IGxCollection AV25TFItemNaoMensuravel_Tipo_Sels ,
                                             String AV45DynamicFiltersSelector1 ,
                                             short AV46DynamicFiltersOperator1 ,
                                             String AV47ItemNaoMensuravel_Descricao1 ,
                                             String AV48ItemNaoMensuravel_AreaTrabalhoDes1 ,
                                             String AV49ReferenciaINM_Descricao1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ItemNaoMensuravel_Descricao2 ,
                                             String AV54ItemNaoMensuravel_AreaTrabalhoDes2 ,
                                             String AV55ReferenciaINM_Descricao2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV59ItemNaoMensuravel_Descricao3 ,
                                             String AV60ItemNaoMensuravel_AreaTrabalhoDes3 ,
                                             String AV61ReferenciaINM_Descricao3 ,
                                             int AV10TFItemNaoMensuravel_AreaTrabalhoCod ,
                                             int AV11TFItemNaoMensuravel_AreaTrabalhoCod_To ,
                                             String AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel ,
                                             String AV12TFItemNaoMensuravel_AreaTrabalhoDes ,
                                             String AV15TFItemNaoMensuravel_Codigo_Sel ,
                                             String AV14TFItemNaoMensuravel_Codigo ,
                                             String AV17TFItemNaoMensuravel_Descricao_Sel ,
                                             String AV16TFItemNaoMensuravel_Descricao ,
                                             decimal AV18TFItemNaoMensuravel_Valor ,
                                             decimal AV19TFItemNaoMensuravel_Valor_To ,
                                             int AV20TFReferenciaINM_Codigo ,
                                             int AV21TFReferenciaINM_Codigo_To ,
                                             String AV23TFReferenciaINM_Descricao_Sel ,
                                             String AV22TFReferenciaINM_Descricao ,
                                             int AV25TFItemNaoMensuravel_Tipo_Sels_Count ,
                                             short AV26TFItemNaoMensuravel_Ativo_Sel ,
                                             String A714ItemNaoMensuravel_Descricao ,
                                             String A720ItemNaoMensuravel_AreaTrabalhoDes ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A718ItemNaoMensuravel_AreaTrabalhoCod ,
                                             String A715ItemNaoMensuravel_Codigo ,
                                             decimal A719ItemNaoMensuravel_Valor ,
                                             int A709ReferenciaINM_Codigo ,
                                             bool A716ItemNaoMensuravel_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [32] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ReferenciaINM_Codigo], T1.[ItemNaoMensuravel_Ativo], T1.[ItemNaoMensuravel_Tipo], T1.[ItemNaoMensuravel_Valor], T1.[ItemNaoMensuravel_Codigo], T1.[ItemNaoMensuravel_AreaTrabalhoCod] AS ItemNaoMensuravel_AreaTrabalhoCod, T2.[ReferenciaINM_Descricao], T3.[AreaTrabalho_Descricao] AS ItemNaoMensuravel_AreaTrabalhoDes, T1.[ItemNaoMensuravel_Descricao] FROM (([ItemNaoMensuravel] T1 WITH (NOLOCK) INNER JOIN [ReferenciaINM] T2 WITH (NOLOCK) ON T2.[ReferenciaINM_Codigo] = T1.[ReferenciaINM_Codigo]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[ItemNaoMensuravel_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ItemNaoMensuravel_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV47ItemNaoMensuravel_Descricao1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ItemNaoMensuravel_AreaTrabalhoDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV48ItemNaoMensuravel_AreaTrabalhoDes1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ReferenciaINM_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV49ReferenciaINM_Descricao1)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ItemNaoMensuravel_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV53ItemNaoMensuravel_Descricao2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54ItemNaoMensuravel_AreaTrabalhoDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV54ItemNaoMensuravel_AreaTrabalhoDes2)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ReferenciaINM_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV55ReferenciaINM_Descricao2)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ItemNaoMensuravel_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like '%' + @lV59ItemNaoMensuravel_Descricao3)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "ITEMNAOMENSURAVEL_AREATRABALHODES") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ItemNaoMensuravel_AreaTrabalhoDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like '%' + @lV60ItemNaoMensuravel_AreaTrabalhoDes3)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ReferenciaINM_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like '%' + @lV61ReferenciaINM_Descricao3)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! (0==AV10TFItemNaoMensuravel_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] >= @AV10TFItemNaoMensuravel_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! (0==AV11TFItemNaoMensuravel_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_AreaTrabalhoCod] <= @AV11TFItemNaoMensuravel_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFItemNaoMensuravel_AreaTrabalhoDes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] like @lV12TFItemNaoMensuravel_AreaTrabalhoDes)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[AreaTrabalho_Descricao] = @AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFItemNaoMensuravel_Codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] like @lV14TFItemNaoMensuravel_Codigo)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFItemNaoMensuravel_Codigo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Codigo] = @AV15TFItemNaoMensuravel_Codigo_Sel)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFItemNaoMensuravel_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] like @lV16TFItemNaoMensuravel_Descricao)";
            }
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFItemNaoMensuravel_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Descricao] = @AV17TFItemNaoMensuravel_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFItemNaoMensuravel_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] >= @AV18TFItemNaoMensuravel_Valor)";
            }
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFItemNaoMensuravel_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Valor] <= @AV19TFItemNaoMensuravel_Valor_To)";
            }
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( ! (0==AV20TFReferenciaINM_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] >= @AV20TFReferenciaINM_Codigo)";
            }
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( ! (0==AV21TFReferenciaINM_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaINM_Codigo] <= @AV21TFReferenciaINM_Codigo_To)";
            }
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFReferenciaINM_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] like @lV22TFReferenciaINM_Descricao)";
            }
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFReferenciaINM_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ReferenciaINM_Descricao] = @AV23TFReferenciaINM_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( AV25TFItemNaoMensuravel_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV25TFItemNaoMensuravel_Tipo_Sels, "T1.[ItemNaoMensuravel_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 1)";
            }
         }
         if ( AV26TFItemNaoMensuravel_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ItemNaoMensuravel_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaINM_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OG2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (short)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (bool)dynConstraints[42] );
               case 1 :
                     return conditional_P00OG3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (short)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (bool)dynConstraints[42] );
               case 2 :
                     return conditional_P00OG4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (short)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (bool)dynConstraints[42] );
               case 3 :
                     return conditional_P00OG5(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (short)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (String)dynConstraints[39] , (decimal)dynConstraints[40] , (int)dynConstraints[41] , (bool)dynConstraints[42] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OG2 ;
          prmP00OG2 = new Object[] {
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFItemNaoMensuravel_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFItemNaoMensuravel_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV14TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV16TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00OG3 ;
          prmP00OG3 = new Object[] {
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFItemNaoMensuravel_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFItemNaoMensuravel_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV14TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV16TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00OG4 ;
          prmP00OG4 = new Object[] {
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFItemNaoMensuravel_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFItemNaoMensuravel_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV14TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV16TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00OG5 ;
          prmP00OG5 = new Object[] {
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV47ItemNaoMensuravel_Descricao1",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV48ItemNaoMensuravel_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV49ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV53ItemNaoMensuravel_Descricao2",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54ItemNaoMensuravel_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV59ItemNaoMensuravel_Descricao3",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV60ItemNaoMensuravel_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV61ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV10TFItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFItemNaoMensuravel_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFItemNaoMensuravel_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13TFItemNaoMensuravel_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV14TFItemNaoMensuravel_Codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFItemNaoMensuravel_Codigo_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV16TFItemNaoMensuravel_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFItemNaoMensuravel_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV18TFItemNaoMensuravel_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFItemNaoMensuravel_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OG2,100,0,true,false )
             ,new CursorDef("P00OG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OG3,100,0,true,false )
             ,new CursorDef("P00OG4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OG4,100,0,true,false )
             ,new CursorDef("P00OG5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OG5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(9) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(9) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 20) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(9);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(8);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptitemnaomensuravelfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptitemnaomensuravelfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptitemnaomensuravelfilterdata") )
          {
             return  ;
          }
          getpromptitemnaomensuravelfilterdata worker = new getpromptitemnaomensuravelfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
