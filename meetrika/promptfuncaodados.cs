/*
               File: PromptFuncaoDados
        Description: Selecione Fun��o de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:38:31.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptfuncaodados : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptfuncaodados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptfuncaodados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutFuncaoDados_Codigo ,
                           ref String aP1_InOutFuncaoDados_Nome ,
                           ref String aP2_InOutFuncaoDados_Descricao ,
                           ref String aP3_FuncaoDados_Tipo ,
                           int aP4_FuncaoDados_SistemaCod )
      {
         this.AV7InOutFuncaoDados_Codigo = aP0_InOutFuncaoDados_Codigo;
         this.AV8InOutFuncaoDados_Nome = aP1_InOutFuncaoDados_Nome;
         this.AV34InOutFuncaoDados_Descricao = aP2_InOutFuncaoDados_Descricao;
         this.AV37FuncaoDados_Tipo = aP3_FuncaoDados_Tipo;
         this.AV33FuncaoDados_SistemaCod = aP4_FuncaoDados_SistemaCod;
         executePrivate();
         aP0_InOutFuncaoDados_Codigo=this.AV7InOutFuncaoDados_Codigo;
         aP1_InOutFuncaoDados_Nome=this.AV8InOutFuncaoDados_Nome;
         aP2_InOutFuncaoDados_Descricao=this.AV34InOutFuncaoDados_Descricao;
         aP3_FuncaoDados_Tipo=this.AV37FuncaoDados_Tipo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbFuncaoDados_Tipo = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
         cmbFuncaoDados_Ativo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_29 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_29_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_29_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV32OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV36FuncaoDados_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36FuncaoDados_Nome", AV36FuncaoDados_Nome);
               AV39TFFuncaoDados_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
               AV40TFFuncaoDados_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
               AV51TFFuncaoDados_DER = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
               AV52TFFuncaoDados_DER_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
               AV55TFFuncaoDados_RLR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
               AV56TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
               AV59TFFuncaoDados_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
               AV60TFFuncaoDados_PF_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
               AV33FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33FuncaoDados_SistemaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV33FuncaoDados_SistemaCod), "ZZZZZ9")));
               AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoDados_NomeTitleControlIdToReplace", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace);
               AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoDados_TipoTitleControlIdToReplace", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace);
               AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
               AV53ddo_FuncaoDados_DERTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoDados_DERTitleControlIdToReplace", AV53ddo_FuncaoDados_DERTitleControlIdToReplace);
               AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoDados_RLRTitleControlIdToReplace", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace);
               AV61ddo_FuncaoDados_PFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoDados_PFTitleControlIdToReplace", AV61ddo_FuncaoDados_PFTitleControlIdToReplace);
               AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV44TFFuncaoDados_Tipo_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV48TFFuncaoDados_Complexidade_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV64TFFuncaoDados_Ativo_Sels);
               AV73Pgmname = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV13OrderedDsc, AV36FuncaoDados_Nome, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV33FuncaoDados_SistemaCod, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV64TFFuncaoDados_Ativo_Sels, AV73Pgmname) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptFuncaoDados";
               forbiddenHiddens = forbiddenHiddens + A397FuncaoDados_Descricao;
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptfuncaodados:[SendSecurityCheck value for]"+"FuncaoDados_Descricao:"+A397FuncaoDados_Descricao);
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutFuncaoDados_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoDados_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutFuncaoDados_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoDados_Nome", AV8InOutFuncaoDados_Nome);
                  AV34InOutFuncaoDados_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34InOutFuncaoDados_Descricao", AV34InOutFuncaoDados_Descricao);
                  AV37FuncaoDados_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoDados_Tipo", AV37FuncaoDados_Tipo);
                  AV33FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33FuncaoDados_SistemaCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV33FuncaoDados_SistemaCod), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA9C2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV73Pgmname = "PromptFuncaoDados";
               context.Gx_err = 0;
               WS9C2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE9C2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117383159");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptfuncaodados.aspx") + "?" + UrlEncode("" +AV7InOutFuncaoDados_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutFuncaoDados_Nome)) + "," + UrlEncode(StringUtil.RTrim(AV34InOutFuncaoDados_Descricao)) + "," + UrlEncode(StringUtil.RTrim(AV37FuncaoDados_Tipo)) + "," + UrlEncode("" +AV33FuncaoDados_SistemaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_NOME", AV36FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_NOME", AV39TFFuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_NOME_SEL", AV40TFFuncaoDados_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFFuncaoDados_DER), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_DER_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFFuncaoDados_RLR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_RLR_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( AV59TFFuncaoDados_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_PF_TO", StringUtil.LTrim( StringUtil.NToC( AV60TFFuncaoDados_PF_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_29", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_29), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_NOMETITLEFILTERDATA", AV38FuncaoDados_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_NOMETITLEFILTERDATA", AV38FuncaoDados_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_TIPOTITLEFILTERDATA", AV42FuncaoDados_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_TIPOTITLEFILTERDATA", AV42FuncaoDados_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV46FuncaoDados_ComplexidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV46FuncaoDados_ComplexidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_DERTITLEFILTERDATA", AV50FuncaoDados_DERTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_DERTITLEFILTERDATA", AV50FuncaoDados_DERTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_RLRTITLEFILTERDATA", AV54FuncaoDados_RLRTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_RLRTITLEFILTERDATA", AV54FuncaoDados_RLRTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_PFTITLEFILTERDATA", AV58FuncaoDados_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_PFTITLEFILTERDATA", AV58FuncaoDados_PFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_ATIVOTITLEFILTERDATA", AV62FuncaoDados_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_ATIVOTITLEFILTERDATA", AV62FuncaoDados_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vFUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33FuncaoDados_SistemaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAODADOS_TIPO_SELS", AV44TFFuncaoDados_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAODADOS_TIPO_SELS", AV44TFFuncaoDados_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV48TFFuncaoDados_Complexidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV48TFFuncaoDados_Complexidade_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAODADOS_ATIVO_SELS", AV64TFFuncaoDados_Ativo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAODADOS_ATIVO_SELS", AV64TFFuncaoDados_Ativo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV73Pgmname));
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutFuncaoDados_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAODADOS_NOME", AV8InOutFuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAODADOS_DESCRICAO", AV34InOutFuncaoDados_Descricao);
         GxWebStd.gx_hidden_field( context, "vFUNCAODADOS_TIPO", StringUtil.RTrim( AV37FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV33FuncaoDados_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV33FuncaoDados_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Caption", StringUtil.RTrim( Ddo_funcaodados_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaodados_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Cls", StringUtil.RTrim( Ddo_funcaodados_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaodados_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaodados_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Caption", StringUtil.RTrim( Ddo_funcaodados_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Tooltip", StringUtil.RTrim( Ddo_funcaodados_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Cls", StringUtil.RTrim( Ddo_funcaodados_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Sortasc", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Caption", StringUtil.RTrim( Ddo_funcaodados_complexidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip", StringUtil.RTrim( Ddo_funcaodados_complexidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Cls", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_complexidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_complexidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Caption", StringUtil.RTrim( Ddo_funcaodados_der_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Tooltip", StringUtil.RTrim( Ddo_funcaodados_der_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Cls", StringUtil.RTrim( Ddo_funcaodados_der_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_der_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_der_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_der_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filtertype", StringUtil.RTrim( Ddo_funcaodados_der_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_der_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_der_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_der_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_der_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Caption", StringUtil.RTrim( Ddo_funcaodados_rlr_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Tooltip", StringUtil.RTrim( Ddo_funcaodados_rlr_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Cls", StringUtil.RTrim( Ddo_funcaodados_rlr_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_rlr_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_rlr_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filtertype", StringUtil.RTrim( Ddo_funcaodados_rlr_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_rlr_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_rlr_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Caption", StringUtil.RTrim( Ddo_funcaodados_pf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Tooltip", StringUtil.RTrim( Ddo_funcaodados_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Cls", StringUtil.RTrim( Ddo_funcaodados_pf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filtertype", StringUtil.RTrim( Ddo_funcaodados_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Caption", StringUtil.RTrim( Ddo_funcaodados_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Tooltip", StringUtil.RTrim( Ddo_funcaodados_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Cls", StringUtil.RTrim( Ddo_funcaodados_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Sortasc", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_complexidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_der_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_rlr_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptFuncaoDados";
         forbiddenHiddens = forbiddenHiddens + A397FuncaoDados_Descricao;
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptfuncaodados:[SendSecurityCheck value for]"+"FuncaoDados_Descricao:"+A397FuncaoDados_Descricao);
      }

      protected void RenderHtmlCloseForm9C2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptFuncaoDados" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Fun��o de Dados" ;
      }

      protected void WB9C0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_9C2( true) ;
         }
         else
         {
            wb_table1_2_9C2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoDados_Descricao_Internalname, A397FuncaoDados_Descricao, "", "", 0, edtFuncaoDados_Descricao_Visible, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_Internalname, AV39TFFuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", 0, edtavTffuncaodados_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_sel_Internalname, AV40TFFuncaoDados_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, edtavTffuncaodados_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFFuncaoDados_DER), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFFuncaoDados_DER), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFFuncaoDados_DER_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFFuncaoDados_RLR), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFFuncaoDados_RLR), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFFuncaoDados_RLR_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV59TFFuncaoDados_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV59TFFuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV60TFFuncaoDados_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV60TFFuncaoDados_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_COMPLEXIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_DERContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", 0, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_RLRContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", 0, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", 0, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
         }
         wbLoad = true;
      }

      protected void START9C2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Fun��o de Dados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9C0( ) ;
      }

      protected void WS9C2( )
      {
         START9C2( ) ;
         EVT9C2( ) ;
      }

      protected void EVT9C2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E119C2 */
                           E119C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E129C2 */
                           E129C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_TIPO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E139C2 */
                           E139C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E149C2 */
                           E149C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_DER.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E159C2 */
                           E159C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E169C2 */
                           E169C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_PF.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E179C2 */
                           E179C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E189C2 */
                           E189C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E199C2 */
                           E199C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E209C2 */
                           E209C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_29_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
                           SubsflControlProps_292( ) ;
                           AV30Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)) ? AV72Select_GXI : context.convertURL( context.PathToRelativeUrl( AV30Select))));
                           A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
                           A370FuncaoDados_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_SistemaCod_Internalname), ",", "."));
                           A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
                           cmbFuncaoDados_Tipo.Name = cmbFuncaoDados_Tipo_Internalname;
                           cmbFuncaoDados_Tipo.CurrentValue = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                           A373FuncaoDados_Tipo = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                           cmbFuncaoDados_Complexidade.Name = cmbFuncaoDados_Complexidade_Internalname;
                           cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                           A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                           A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
                           A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
                           A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
                           cmbFuncaoDados_Ativo.Name = cmbFuncaoDados_Ativo_Internalname;
                           cmbFuncaoDados_Ativo.CurrentValue = cgiGet( cmbFuncaoDados_Ativo_Internalname);
                           A394FuncaoDados_Ativo = cgiGet( cmbFuncaoDados_Ativo_Internalname);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E219C2 */
                                 E219C2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E229C2 */
                                 E229C2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E239C2 */
                                 E239C2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV32OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaodados_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME"), AV36FuncaoDados_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME"), AV39TFFuncaoDados_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME_SEL"), AV40TFFuncaoDados_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_der Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_DER"), ",", ".") != Convert.ToDecimal( AV51TFFuncaoDados_DER )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_der_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_DER_TO"), ",", ".") != Convert.ToDecimal( AV52TFFuncaoDados_DER_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_rlr Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_RLR"), ",", ".") != Convert.ToDecimal( AV55TFFuncaoDados_RLR )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_rlr_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_RLR_TO"), ",", ".") != Convert.ToDecimal( AV56TFFuncaoDados_RLR_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_pf Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_PF"), ",", ".") != AV59TFFuncaoDados_PF )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaodados_pf_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_PF_TO"), ",", ".") != AV60TFFuncaoDados_PF_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E249C2 */
                                       E249C2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE9C2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9C2( ) ;
            }
         }
      }

      protected void PA9C2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV32OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            }
            GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_29_idx;
            cmbFuncaoDados_Tipo.Name = GXCCtl;
            cmbFuncaoDados_Tipo.WebTags = "";
            cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
            cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
            cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
            if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
            {
               A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_29_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            GXCCtl = "FUNCAODADOS_ATIVO_" + sGXsfl_29_idx;
            cmbFuncaoDados_Ativo.Name = GXCCtl;
            cmbFuncaoDados_Ativo.WebTags = "";
            cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
            {
               A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_292( ) ;
         while ( nGXsfl_29_idx <= nRC_GXsfl_29 )
         {
            sendrow_292( ) ;
            nGXsfl_29_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_29_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_29_idx+1));
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_292( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV32OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       String AV36FuncaoDados_Nome ,
                                       String AV39TFFuncaoDados_Nome ,
                                       String AV40TFFuncaoDados_Nome_Sel ,
                                       short AV51TFFuncaoDados_DER ,
                                       short AV52TFFuncaoDados_DER_To ,
                                       short AV55TFFuncaoDados_RLR ,
                                       short AV56TFFuncaoDados_RLR_To ,
                                       decimal AV59TFFuncaoDados_PF ,
                                       decimal AV60TFFuncaoDados_PF_To ,
                                       int AV33FuncaoDados_SistemaCod ,
                                       String AV41ddo_FuncaoDados_NomeTitleControlIdToReplace ,
                                       String AV45ddo_FuncaoDados_TipoTitleControlIdToReplace ,
                                       String AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ,
                                       String AV53ddo_FuncaoDados_DERTitleControlIdToReplace ,
                                       String AV57ddo_FuncaoDados_RLRTitleControlIdToReplace ,
                                       String AV61ddo_FuncaoDados_PFTitleControlIdToReplace ,
                                       String AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV44TFFuncaoDados_Tipo_Sels ,
                                       IGxCollection AV48TFFuncaoDados_Complexidade_Sels ,
                                       IGxCollection AV64TFFuncaoDados_Ativo_Sels ,
                                       String AV73Pgmname )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9C2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_NOME", A369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_COMPLEXIDADE", StringUtil.RTrim( A376FuncaoDados_Complexidade));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_DER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_PF", GetSecureSignedToken( "", context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_ATIVO", StringUtil.RTrim( A394FuncaoDados_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV32OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9C2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV73Pgmname = "PromptFuncaoDados";
         context.Gx_err = 0;
      }

      protected void RF9C2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 29;
         /* Execute user event: E229C2 */
         E229C2 ();
         nGXsfl_29_idx = 1;
         sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
         SubsflControlProps_292( ) ;
         nGXsfl_29_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_292( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A376FuncaoDados_Complexidade ,
                                                 AV48TFFuncaoDados_Complexidade_Sels ,
                                                 A373FuncaoDados_Tipo ,
                                                 AV44TFFuncaoDados_Tipo_Sels ,
                                                 A394FuncaoDados_Ativo ,
                                                 AV64TFFuncaoDados_Ativo_Sels ,
                                                 AV36FuncaoDados_Nome ,
                                                 AV40TFFuncaoDados_Nome_Sel ,
                                                 AV39TFFuncaoDados_Nome ,
                                                 AV44TFFuncaoDados_Tipo_Sels.Count ,
                                                 AV64TFFuncaoDados_Ativo_Sels.Count ,
                                                 AV33FuncaoDados_SistemaCod ,
                                                 A369FuncaoDados_Nome ,
                                                 A370FuncaoDados_SistemaCod ,
                                                 AV32OrderedBy ,
                                                 AV13OrderedDsc ,
                                                 AV48TFFuncaoDados_Complexidade_Sels.Count ,
                                                 AV51TFFuncaoDados_DER ,
                                                 A374FuncaoDados_DER ,
                                                 AV52TFFuncaoDados_DER_To ,
                                                 AV55TFFuncaoDados_RLR ,
                                                 A375FuncaoDados_RLR ,
                                                 AV56TFFuncaoDados_RLR_To ,
                                                 AV59TFFuncaoDados_PF ,
                                                 A377FuncaoDados_PF ,
                                                 AV60TFFuncaoDados_PF_To },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                                 }
            });
            lV36FuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV36FuncaoDados_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36FuncaoDados_Nome", AV36FuncaoDados_Nome);
            lV39TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV39TFFuncaoDados_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
            /* Using cursor H009C2 */
            pr_default.execute(0, new Object[] {AV48TFFuncaoDados_Complexidade_Sels.Count, lV36FuncaoDados_Nome, lV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV33FuncaoDados_SistemaCod});
            nGXsfl_29_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A397FuncaoDados_Descricao = H009C2_A397FuncaoDados_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
               n397FuncaoDados_Descricao = H009C2_n397FuncaoDados_Descricao[0];
               A394FuncaoDados_Ativo = H009C2_A394FuncaoDados_Ativo[0];
               A373FuncaoDados_Tipo = H009C2_A373FuncaoDados_Tipo[0];
               A369FuncaoDados_Nome = H009C2_A369FuncaoDados_Nome[0];
               A370FuncaoDados_SistemaCod = H009C2_A370FuncaoDados_SistemaCod[0];
               A755FuncaoDados_Contar = H009C2_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H009C2_n755FuncaoDados_Contar[0];
               A368FuncaoDados_Codigo = H009C2_A368FuncaoDados_Codigo[0];
               GXt_char1 = A376FuncaoDados_Complexidade;
               new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
               A376FuncaoDados_Complexidade = GXt_char1;
               if ( ( AV48TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV48TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
               {
                  GXt_int2 = A374FuncaoDados_DER;
                  new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                  A374FuncaoDados_DER = GXt_int2;
                  if ( (0==AV51TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV51TFFuncaoDados_DER ) ) )
                  {
                     if ( (0==AV52TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV52TFFuncaoDados_DER_To ) ) )
                     {
                        GXt_int2 = A375FuncaoDados_RLR;
                        new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                        A375FuncaoDados_RLR = GXt_int2;
                        if ( (0==AV55TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV55TFFuncaoDados_RLR ) ) )
                        {
                           if ( (0==AV56TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV56TFFuncaoDados_RLR_To ) ) )
                           {
                              if ( A755FuncaoDados_Contar )
                              {
                                 GXt_int2 = (short)(A377FuncaoDados_PF);
                                 new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                                 A377FuncaoDados_PF = (decimal)(GXt_int2);
                              }
                              else
                              {
                                 A377FuncaoDados_PF = 0;
                              }
                              if ( (Convert.ToDecimal(0)==AV59TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV59TFFuncaoDados_PF ) ) )
                              {
                                 if ( (Convert.ToDecimal(0)==AV60TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV60TFFuncaoDados_PF_To ) ) )
                                 {
                                    /* Execute user event: E239C2 */
                                    E239C2 ();
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 29;
            WB9C0( ) ;
         }
         nGXsfl_29_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV13OrderedDsc, AV36FuncaoDados_Nome, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV33FuncaoDados_SistemaCod, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV64TFFuncaoDados_Ativo_Sels, AV73Pgmname) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV13OrderedDsc, AV36FuncaoDados_Nome, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV33FuncaoDados_SistemaCod, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV64TFFuncaoDados_Ativo_Sels, AV73Pgmname) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV13OrderedDsc, AV36FuncaoDados_Nome, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV33FuncaoDados_SistemaCod, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV64TFFuncaoDados_Ativo_Sels, AV73Pgmname) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV13OrderedDsc, AV36FuncaoDados_Nome, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV33FuncaoDados_SistemaCod, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV64TFFuncaoDados_Ativo_Sels, AV73Pgmname) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV32OrderedBy, AV13OrderedDsc, AV36FuncaoDados_Nome, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV33FuncaoDados_SistemaCod, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV64TFFuncaoDados_Ativo_Sels, AV73Pgmname) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9C0( )
      {
         /* Before Start, stand alone formulas. */
         AV73Pgmname = "PromptFuncaoDados";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E219C2 */
         E219C2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV66DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_NOMETITLEFILTERDATA"), AV38FuncaoDados_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_TIPOTITLEFILTERDATA"), AV42FuncaoDados_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA"), AV46FuncaoDados_ComplexidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_DERTITLEFILTERDATA"), AV50FuncaoDados_DERTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_RLRTITLEFILTERDATA"), AV54FuncaoDados_RLRTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_PFTITLEFILTERDATA"), AV58FuncaoDados_PFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_ATIVOTITLEFILTERDATA"), AV62FuncaoDados_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV32OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            AV36FuncaoDados_Nome = cgiGet( edtavFuncaodados_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36FuncaoDados_Nome", AV36FuncaoDados_Nome);
            A397FuncaoDados_Descricao = cgiGet( edtFuncaoDados_Descricao_Internalname);
            n397FuncaoDados_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
            AV39TFFuncaoDados_Nome = cgiGet( edtavTffuncaodados_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
            AV40TFFuncaoDados_Nome_Sel = cgiGet( edtavTffuncaodados_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER");
               GX_FocusControl = edtavTffuncaodados_der_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFFuncaoDados_DER = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
            }
            else
            {
               AV51TFFuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER_TO");
               GX_FocusControl = edtavTffuncaodados_der_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFFuncaoDados_DER_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
            }
            else
            {
               AV52TFFuncaoDados_DER_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR");
               GX_FocusControl = edtavTffuncaodados_rlr_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFFuncaoDados_RLR = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
            }
            else
            {
               AV55TFFuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR_TO");
               GX_FocusControl = edtavTffuncaodados_rlr_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFFuncaoDados_RLR_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
            }
            else
            {
               AV56TFFuncaoDados_RLR_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF");
               GX_FocusControl = edtavTffuncaodados_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFFuncaoDados_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
            }
            else
            {
               AV59TFFuncaoDados_PF = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF_TO");
               GX_FocusControl = edtavTffuncaodados_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFFuncaoDados_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
            }
            else
            {
               AV60TFFuncaoDados_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
            }
            AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoDados_NomeTitleControlIdToReplace", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace);
            AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoDados_TipoTitleControlIdToReplace", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace);
            AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
            AV53ddo_FuncaoDados_DERTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoDados_DERTitleControlIdToReplace", AV53ddo_FuncaoDados_DERTitleControlIdToReplace);
            AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoDados_RLRTitleControlIdToReplace", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace);
            AV61ddo_FuncaoDados_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoDados_PFTitleControlIdToReplace", AV61ddo_FuncaoDados_PFTitleControlIdToReplace);
            AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_29 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_29"), ",", "."));
            AV68GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV69GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaodados_nome_Caption = cgiGet( "DDO_FUNCAODADOS_NOME_Caption");
            Ddo_funcaodados_nome_Tooltip = cgiGet( "DDO_FUNCAODADOS_NOME_Tooltip");
            Ddo_funcaodados_nome_Cls = cgiGet( "DDO_FUNCAODADOS_NOME_Cls");
            Ddo_funcaodados_nome_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_NOME_Filteredtext_set");
            Ddo_funcaodados_nome_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_NOME_Selectedvalue_set");
            Ddo_funcaodados_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_NOME_Dropdownoptionstype");
            Ddo_funcaodados_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace");
            Ddo_funcaodados_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includesortasc"));
            Ddo_funcaodados_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includesortdsc"));
            Ddo_funcaodados_nome_Sortedstatus = cgiGet( "DDO_FUNCAODADOS_NOME_Sortedstatus");
            Ddo_funcaodados_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includefilter"));
            Ddo_funcaodados_nome_Filtertype = cgiGet( "DDO_FUNCAODADOS_NOME_Filtertype");
            Ddo_funcaodados_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Filterisrange"));
            Ddo_funcaodados_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includedatalist"));
            Ddo_funcaodados_nome_Datalisttype = cgiGet( "DDO_FUNCAODADOS_NOME_Datalisttype");
            Ddo_funcaodados_nome_Datalistproc = cgiGet( "DDO_FUNCAODADOS_NOME_Datalistproc");
            Ddo_funcaodados_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_nome_Sortasc = cgiGet( "DDO_FUNCAODADOS_NOME_Sortasc");
            Ddo_funcaodados_nome_Sortdsc = cgiGet( "DDO_FUNCAODADOS_NOME_Sortdsc");
            Ddo_funcaodados_nome_Loadingdata = cgiGet( "DDO_FUNCAODADOS_NOME_Loadingdata");
            Ddo_funcaodados_nome_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_NOME_Cleanfilter");
            Ddo_funcaodados_nome_Noresultsfound = cgiGet( "DDO_FUNCAODADOS_NOME_Noresultsfound");
            Ddo_funcaodados_nome_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_NOME_Searchbuttontext");
            Ddo_funcaodados_tipo_Caption = cgiGet( "DDO_FUNCAODADOS_TIPO_Caption");
            Ddo_funcaodados_tipo_Tooltip = cgiGet( "DDO_FUNCAODADOS_TIPO_Tooltip");
            Ddo_funcaodados_tipo_Cls = cgiGet( "DDO_FUNCAODADOS_TIPO_Cls");
            Ddo_funcaodados_tipo_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_TIPO_Selectedvalue_set");
            Ddo_funcaodados_tipo_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_TIPO_Dropdownoptionstype");
            Ddo_funcaodados_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_TIPO_Titlecontrolidtoreplace");
            Ddo_funcaodados_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includesortasc"));
            Ddo_funcaodados_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includesortdsc"));
            Ddo_funcaodados_tipo_Sortedstatus = cgiGet( "DDO_FUNCAODADOS_TIPO_Sortedstatus");
            Ddo_funcaodados_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includefilter"));
            Ddo_funcaodados_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includedatalist"));
            Ddo_funcaodados_tipo_Datalisttype = cgiGet( "DDO_FUNCAODADOS_TIPO_Datalisttype");
            Ddo_funcaodados_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Allowmultipleselection"));
            Ddo_funcaodados_tipo_Datalistfixedvalues = cgiGet( "DDO_FUNCAODADOS_TIPO_Datalistfixedvalues");
            Ddo_funcaodados_tipo_Sortasc = cgiGet( "DDO_FUNCAODADOS_TIPO_Sortasc");
            Ddo_funcaodados_tipo_Sortdsc = cgiGet( "DDO_FUNCAODADOS_TIPO_Sortdsc");
            Ddo_funcaodados_tipo_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_TIPO_Cleanfilter");
            Ddo_funcaodados_tipo_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_TIPO_Searchbuttontext");
            Ddo_funcaodados_complexidade_Caption = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Caption");
            Ddo_funcaodados_complexidade_Tooltip = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip");
            Ddo_funcaodados_complexidade_Cls = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Cls");
            Ddo_funcaodados_complexidade_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set");
            Ddo_funcaodados_complexidade_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype");
            Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace");
            Ddo_funcaodados_complexidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc"));
            Ddo_funcaodados_complexidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc"));
            Ddo_funcaodados_complexidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter"));
            Ddo_funcaodados_complexidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist"));
            Ddo_funcaodados_complexidade_Datalisttype = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype");
            Ddo_funcaodados_complexidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection"));
            Ddo_funcaodados_complexidade_Datalistfixedvalues = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues");
            Ddo_funcaodados_complexidade_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter");
            Ddo_funcaodados_complexidade_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext");
            Ddo_funcaodados_der_Caption = cgiGet( "DDO_FUNCAODADOS_DER_Caption");
            Ddo_funcaodados_der_Tooltip = cgiGet( "DDO_FUNCAODADOS_DER_Tooltip");
            Ddo_funcaodados_der_Cls = cgiGet( "DDO_FUNCAODADOS_DER_Cls");
            Ddo_funcaodados_der_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtext_set");
            Ddo_funcaodados_der_Filteredtextto_set = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtextto_set");
            Ddo_funcaodados_der_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_DER_Dropdownoptionstype");
            Ddo_funcaodados_der_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace");
            Ddo_funcaodados_der_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includesortasc"));
            Ddo_funcaodados_der_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includesortdsc"));
            Ddo_funcaodados_der_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includefilter"));
            Ddo_funcaodados_der_Filtertype = cgiGet( "DDO_FUNCAODADOS_DER_Filtertype");
            Ddo_funcaodados_der_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Filterisrange"));
            Ddo_funcaodados_der_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includedatalist"));
            Ddo_funcaodados_der_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_DER_Cleanfilter");
            Ddo_funcaodados_der_Rangefilterfrom = cgiGet( "DDO_FUNCAODADOS_DER_Rangefilterfrom");
            Ddo_funcaodados_der_Rangefilterto = cgiGet( "DDO_FUNCAODADOS_DER_Rangefilterto");
            Ddo_funcaodados_der_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_DER_Searchbuttontext");
            Ddo_funcaodados_rlr_Caption = cgiGet( "DDO_FUNCAODADOS_RLR_Caption");
            Ddo_funcaodados_rlr_Tooltip = cgiGet( "DDO_FUNCAODADOS_RLR_Tooltip");
            Ddo_funcaodados_rlr_Cls = cgiGet( "DDO_FUNCAODADOS_RLR_Cls");
            Ddo_funcaodados_rlr_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtext_set");
            Ddo_funcaodados_rlr_Filteredtextto_set = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtextto_set");
            Ddo_funcaodados_rlr_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_RLR_Dropdownoptionstype");
            Ddo_funcaodados_rlr_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace");
            Ddo_funcaodados_rlr_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includesortasc"));
            Ddo_funcaodados_rlr_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includesortdsc"));
            Ddo_funcaodados_rlr_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includefilter"));
            Ddo_funcaodados_rlr_Filtertype = cgiGet( "DDO_FUNCAODADOS_RLR_Filtertype");
            Ddo_funcaodados_rlr_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Filterisrange"));
            Ddo_funcaodados_rlr_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includedatalist"));
            Ddo_funcaodados_rlr_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_RLR_Cleanfilter");
            Ddo_funcaodados_rlr_Rangefilterfrom = cgiGet( "DDO_FUNCAODADOS_RLR_Rangefilterfrom");
            Ddo_funcaodados_rlr_Rangefilterto = cgiGet( "DDO_FUNCAODADOS_RLR_Rangefilterto");
            Ddo_funcaodados_rlr_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_RLR_Searchbuttontext");
            Ddo_funcaodados_pf_Caption = cgiGet( "DDO_FUNCAODADOS_PF_Caption");
            Ddo_funcaodados_pf_Tooltip = cgiGet( "DDO_FUNCAODADOS_PF_Tooltip");
            Ddo_funcaodados_pf_Cls = cgiGet( "DDO_FUNCAODADOS_PF_Cls");
            Ddo_funcaodados_pf_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtext_set");
            Ddo_funcaodados_pf_Filteredtextto_set = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtextto_set");
            Ddo_funcaodados_pf_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_PF_Dropdownoptionstype");
            Ddo_funcaodados_pf_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace");
            Ddo_funcaodados_pf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includesortasc"));
            Ddo_funcaodados_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includesortdsc"));
            Ddo_funcaodados_pf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includefilter"));
            Ddo_funcaodados_pf_Filtertype = cgiGet( "DDO_FUNCAODADOS_PF_Filtertype");
            Ddo_funcaodados_pf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Filterisrange"));
            Ddo_funcaodados_pf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includedatalist"));
            Ddo_funcaodados_pf_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_PF_Cleanfilter");
            Ddo_funcaodados_pf_Rangefilterfrom = cgiGet( "DDO_FUNCAODADOS_PF_Rangefilterfrom");
            Ddo_funcaodados_pf_Rangefilterto = cgiGet( "DDO_FUNCAODADOS_PF_Rangefilterto");
            Ddo_funcaodados_pf_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_PF_Searchbuttontext");
            Ddo_funcaodados_ativo_Caption = cgiGet( "DDO_FUNCAODADOS_ATIVO_Caption");
            Ddo_funcaodados_ativo_Tooltip = cgiGet( "DDO_FUNCAODADOS_ATIVO_Tooltip");
            Ddo_funcaodados_ativo_Cls = cgiGet( "DDO_FUNCAODADOS_ATIVO_Cls");
            Ddo_funcaodados_ativo_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_ATIVO_Selectedvalue_set");
            Ddo_funcaodados_ativo_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_ATIVO_Dropdownoptionstype");
            Ddo_funcaodados_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_ATIVO_Titlecontrolidtoreplace");
            Ddo_funcaodados_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includesortasc"));
            Ddo_funcaodados_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includesortdsc"));
            Ddo_funcaodados_ativo_Sortedstatus = cgiGet( "DDO_FUNCAODADOS_ATIVO_Sortedstatus");
            Ddo_funcaodados_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includefilter"));
            Ddo_funcaodados_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includedatalist"));
            Ddo_funcaodados_ativo_Datalisttype = cgiGet( "DDO_FUNCAODADOS_ATIVO_Datalisttype");
            Ddo_funcaodados_ativo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Allowmultipleselection"));
            Ddo_funcaodados_ativo_Datalistfixedvalues = cgiGet( "DDO_FUNCAODADOS_ATIVO_Datalistfixedvalues");
            Ddo_funcaodados_ativo_Sortasc = cgiGet( "DDO_FUNCAODADOS_ATIVO_Sortasc");
            Ddo_funcaodados_ativo_Sortdsc = cgiGet( "DDO_FUNCAODADOS_ATIVO_Sortdsc");
            Ddo_funcaodados_ativo_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_ATIVO_Cleanfilter");
            Ddo_funcaodados_ativo_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaodados_nome_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_NOME_Activeeventkey");
            Ddo_funcaodados_nome_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_NOME_Filteredtext_get");
            Ddo_funcaodados_nome_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_NOME_Selectedvalue_get");
            Ddo_funcaodados_tipo_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_TIPO_Activeeventkey");
            Ddo_funcaodados_tipo_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_TIPO_Selectedvalue_get");
            Ddo_funcaodados_complexidade_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey");
            Ddo_funcaodados_complexidade_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get");
            Ddo_funcaodados_der_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_DER_Activeeventkey");
            Ddo_funcaodados_der_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtext_get");
            Ddo_funcaodados_der_Filteredtextto_get = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtextto_get");
            Ddo_funcaodados_rlr_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_RLR_Activeeventkey");
            Ddo_funcaodados_rlr_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtext_get");
            Ddo_funcaodados_rlr_Filteredtextto_get = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtextto_get");
            Ddo_funcaodados_pf_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_PF_Activeeventkey");
            Ddo_funcaodados_pf_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtext_get");
            Ddo_funcaodados_pf_Filteredtextto_get = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtextto_get");
            Ddo_funcaodados_ativo_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_ATIVO_Activeeventkey");
            Ddo_funcaodados_ativo_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptFuncaoDados";
            A397FuncaoDados_Descricao = cgiGet( edtFuncaoDados_Descricao_Internalname);
            n397FuncaoDados_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A397FuncaoDados_Descricao", A397FuncaoDados_Descricao);
            forbiddenHiddens = forbiddenHiddens + A397FuncaoDados_Descricao;
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptfuncaodados:[SecurityCheckFailed value for]"+"FuncaoDados_Descricao:"+A397FuncaoDados_Descricao);
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV32OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME"), AV36FuncaoDados_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME"), AV39TFFuncaoDados_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME_SEL"), AV40TFFuncaoDados_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_DER"), ",", ".") != Convert.ToDecimal( AV51TFFuncaoDados_DER )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_DER_TO"), ",", ".") != Convert.ToDecimal( AV52TFFuncaoDados_DER_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_RLR"), ",", ".") != Convert.ToDecimal( AV55TFFuncaoDados_RLR )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_RLR_TO"), ",", ".") != Convert.ToDecimal( AV56TFFuncaoDados_RLR_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_PF"), ",", ".") != AV59TFFuncaoDados_PF )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAODADOS_PF_TO"), ",", ".") != AV60TFFuncaoDados_PF_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E219C2 */
         E219C2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E219C2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTffuncaodados_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_Visible), 5, 0)));
         edtavTffuncaodados_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_sel_Visible), 5, 0)));
         edtavTffuncaodados_der_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_der_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_Visible), 5, 0)));
         edtavTffuncaodados_der_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_der_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_to_Visible), 5, 0)));
         edtavTffuncaodados_rlr_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_rlr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_Visible), 5, 0)));
         edtavTffuncaodados_rlr_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_rlr_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_to_Visible), 5, 0)));
         edtavTffuncaodados_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_Visible), 5, 0)));
         edtavTffuncaodados_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_to_Visible), 5, 0)));
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_nome_Titlecontrolidtoreplace);
         AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = Ddo_funcaodados_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoDados_NomeTitleControlIdToReplace", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace);
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_tipo_Titlecontrolidtoreplace);
         AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = Ddo_funcaodados_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoDados_TipoTitleControlIdToReplace", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace);
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Complexidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_complexidade_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_complexidade_Titlecontrolidtoreplace);
         AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = Ddo_funcaodados_complexidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_der_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_DER";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_der_Titlecontrolidtoreplace);
         AV53ddo_FuncaoDados_DERTitleControlIdToReplace = Ddo_funcaodados_der_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoDados_DERTitleControlIdToReplace", AV53ddo_FuncaoDados_DERTitleControlIdToReplace);
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_RLR";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_rlr_Titlecontrolidtoreplace);
         AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = Ddo_funcaodados_rlr_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoDados_RLRTitleControlIdToReplace", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace);
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_pf_Titlecontrolidtoreplace);
         AV61ddo_FuncaoDados_PFTitleControlIdToReplace = Ddo_funcaodados_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoDados_PFTitleControlIdToReplace", AV61ddo_FuncaoDados_PFTitleControlIdToReplace);
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_ativo_Titlecontrolidtoreplace);
         AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = Ddo_funcaodados_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace);
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione a Fun��o de Dados:";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtFuncaoDados_Descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoDados_Descricao_Visible), 5, 0)));
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Dados", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "Status", 0);
         if ( AV32OrderedBy < 1 )
         {
            AV32OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = AV66DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3) ;
         AV66DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3;
      }

      protected void E229C2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV38FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoDados_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62FuncaoDados_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoDados_Nome_Titleformat = 2;
         edtFuncaoDados_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Nome_Internalname, "Title", edtFuncaoDados_Nome_Title);
         cmbFuncaoDados_Tipo_Titleformat = 2;
         cmbFuncaoDados_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Tipo_Internalname, "Title", cmbFuncaoDados_Tipo.Title.Text);
         cmbFuncaoDados_Complexidade_Titleformat = 2;
         cmbFuncaoDados_Complexidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Complexidade", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Title", cmbFuncaoDados_Complexidade.Title.Text);
         edtFuncaoDados_DER_Titleformat = 2;
         edtFuncaoDados_DER_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "DER", AV53ddo_FuncaoDados_DERTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_DER_Internalname, "Title", edtFuncaoDados_DER_Title);
         edtFuncaoDados_RLR_Titleformat = 2;
         edtFuncaoDados_RLR_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "RLR", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_RLR_Internalname, "Title", edtFuncaoDados_RLR_Title);
         edtFuncaoDados_PF_Titleformat = 2;
         edtFuncaoDados_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV61ddo_FuncaoDados_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_PF_Internalname, "Title", edtFuncaoDados_PF_Title);
         cmbFuncaoDados_Ativo_Titleformat = 2;
         cmbFuncaoDados_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Title", cmbFuncaoDados_Ativo.Title.Text);
         AV68GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68GridCurrentPage), 10, 0)));
         AV69GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridPageCount), 10, 0)));
         edtavFuncaodados_nome_Height = 25;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Height", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Height), 9, 0)));
         edtavFuncaodados_nome_Width = 150;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome_Width), 9, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38FuncaoDados_NomeTitleFilterData", AV38FuncaoDados_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42FuncaoDados_TipoTitleFilterData", AV42FuncaoDados_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46FuncaoDados_ComplexidadeTitleFilterData", AV46FuncaoDados_ComplexidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50FuncaoDados_DERTitleFilterData", AV50FuncaoDados_DERTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54FuncaoDados_RLRTitleFilterData", AV54FuncaoDados_RLRTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58FuncaoDados_PFTitleFilterData", AV58FuncaoDados_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62FuncaoDados_AtivoTitleFilterData", AV62FuncaoDados_AtivoTitleFilterData);
      }

      protected void E119C2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV67PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV67PageToGo) ;
         }
      }

      protected void E129C2( )
      {
         /* Ddo_funcaodados_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV32OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV32OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFFuncaoDados_Nome = Ddo_funcaodados_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
            AV40TFFuncaoDados_Nome_Sel = Ddo_funcaodados_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E139C2( )
      {
         /* Ddo_funcaodados_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV32OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV32OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFFuncaoDados_Tipo_SelsJson = Ddo_funcaodados_tipo_Selectedvalue_get;
            AV44TFFuncaoDados_Tipo_Sels.FromJSonString(AV43TFFuncaoDados_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44TFFuncaoDados_Tipo_Sels", AV44TFFuncaoDados_Tipo_Sels);
      }

      protected void E149C2( )
      {
         /* Ddo_funcaodados_complexidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_complexidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFFuncaoDados_Complexidade_SelsJson = Ddo_funcaodados_complexidade_Selectedvalue_get;
            AV48TFFuncaoDados_Complexidade_Sels.FromJSonString(AV47TFFuncaoDados_Complexidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFFuncaoDados_Complexidade_Sels", AV48TFFuncaoDados_Complexidade_Sels);
      }

      protected void E159C2( )
      {
         /* Ddo_funcaodados_der_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_der_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFFuncaoDados_DER = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
            AV52TFFuncaoDados_DER_To = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E169C2( )
      {
         /* Ddo_funcaodados_rlr_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_rlr_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFFuncaoDados_RLR = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
            AV56TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E179C2( )
      {
         /* Ddo_funcaodados_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFFuncaoDados_PF = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
            AV60TFFuncaoDados_PF_To = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E189C2( )
      {
         /* Ddo_funcaodados_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV32OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV32OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFFuncaoDados_Ativo_SelsJson = Ddo_funcaodados_ativo_Selectedvalue_get;
            AV64TFFuncaoDados_Ativo_Sels.FromJSonString(AV63TFFuncaoDados_Ativo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64TFFuncaoDados_Ativo_Sels", AV64TFFuncaoDados_Ativo_Sels);
      }

      private void E239C2( )
      {
         /* Grid_Load Routine */
         AV30Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV30Select);
         AV72Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 29;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_292( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_29_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(29, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E249C2 */
         E249C2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E249C2( )
      {
         /* Enter Routine */
         AV37FuncaoDados_Tipo = A373FuncaoDados_Tipo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoDados_Tipo", AV37FuncaoDados_Tipo);
         AV7InOutFuncaoDados_Codigo = A368FuncaoDados_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoDados_Codigo), 6, 0)));
         AV8InOutFuncaoDados_Nome = A369FuncaoDados_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoDados_Nome", AV8InOutFuncaoDados_Nome);
         AV34InOutFuncaoDados_Descricao = A397FuncaoDados_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34InOutFuncaoDados_Descricao", AV34InOutFuncaoDados_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutFuncaoDados_Codigo,(String)AV8InOutFuncaoDados_Nome,(String)AV34InOutFuncaoDados_Descricao,(String)AV37FuncaoDados_Tipo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E199C2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E209C2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44TFFuncaoDados_Tipo_Sels", AV44TFFuncaoDados_Tipo_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFFuncaoDados_Complexidade_Sels", AV48TFFuncaoDados_Complexidade_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64TFFuncaoDados_Ativo_Sels", AV64TFFuncaoDados_Ativo_Sels);
      }

      protected void S132( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaodados_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
         Ddo_funcaodados_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
         Ddo_funcaodados_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV32OrderedBy == 1 )
         {
            Ddo_funcaodados_nome_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
         }
         else if ( AV32OrderedBy == 2 )
         {
            Ddo_funcaodados_tipo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
         }
         else if ( AV32OrderedBy == 3 )
         {
            Ddo_funcaodados_ativo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CLEANFILTERS' Routine */
         AV36FuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36FuncaoDados_Nome", AV36FuncaoDados_Nome);
         AV39TFFuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
         Ddo_funcaodados_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "FilteredText_set", Ddo_funcaodados_nome_Filteredtext_set);
         AV40TFFuncaoDados_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SelectedValue_set", Ddo_funcaodados_nome_Selectedvalue_set);
         AV44TFFuncaoDados_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SelectedValue_set", Ddo_funcaodados_tipo_Selectedvalue_set);
         AV48TFFuncaoDados_Complexidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_complexidade_Internalname, "SelectedValue_set", Ddo_funcaodados_complexidade_Selectedvalue_set);
         AV51TFFuncaoDados_DER = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
         Ddo_funcaodados_der_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "FilteredText_set", Ddo_funcaodados_der_Filteredtext_set);
         AV52TFFuncaoDados_DER_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
         Ddo_funcaodados_der_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "FilteredTextTo_set", Ddo_funcaodados_der_Filteredtextto_set);
         AV55TFFuncaoDados_RLR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "FilteredText_set", Ddo_funcaodados_rlr_Filteredtext_set);
         AV56TFFuncaoDados_RLR_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "FilteredTextTo_set", Ddo_funcaodados_rlr_Filteredtextto_set);
         AV59TFFuncaoDados_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
         Ddo_funcaodados_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "FilteredText_set", Ddo_funcaodados_pf_Filteredtext_set);
         AV60TFFuncaoDados_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "FilteredTextTo_set", Ddo_funcaodados_pf_Filteredtextto_set);
         AV64TFFuncaoDados_Ativo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SelectedValue_set", Ddo_funcaodados_ativo_Selectedvalue_set);
      }

      protected void S122( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV32OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36FuncaoDados_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "FUNCAODADOS_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV36FuncaoDados_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoDados_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFFuncaoDados_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoDados_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFFuncaoDados_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV44TFFuncaoDados_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFFuncaoDados_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV48TFFuncaoDados_Complexidade_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_COMPLEXIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFFuncaoDados_Complexidade_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV51TFFuncaoDados_DER) && (0==AV52TFFuncaoDados_DER_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_DER";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV55TFFuncaoDados_RLR) && (0==AV56TFFuncaoDados_RLR_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_RLR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV59TFFuncaoDados_PF) && (Convert.ToDecimal(0)==AV60TFFuncaoDados_PF_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_PF";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV64TFFuncaoDados_Ativo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFFuncaoDados_Ativo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV33FuncaoDados_SistemaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAODADOS_SISTEMACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV33FuncaoDados_SistemaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV73Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void wb_table1_2_9C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_9C2( true) ;
         }
         else
         {
            wb_table2_5_9C2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_9C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_23_9C2( true) ;
         }
         else
         {
            wb_table3_23_9C2( false) ;
         }
         return  ;
      }

      protected void wb_table3_23_9C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9C2e( true) ;
         }
         else
         {
            wb_table1_2_9C2e( false) ;
         }
      }

      protected void wb_table3_23_9C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_26_9C2( true) ;
         }
         else
         {
            wb_table4_26_9C2( false) ;
         }
         return  ;
      }

      protected void wb_table4_26_9C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_23_9C2e( true) ;
         }
         else
         {
            wb_table3_23_9C2e( false) ;
         }
      }

      protected void wb_table4_26_9C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"29\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Complexidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_DER_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_DER_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_DER_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_RLR_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_RLR_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_RLR_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A369FuncaoDados_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A373FuncaoDados_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A376FuncaoDados_Complexidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Complexidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Complexidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_DER_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_DER_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_RLR_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_RLR_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A394FuncaoDados_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 29 )
         {
            wbEnd = 0;
            nRC_GXsfl_29 = (short)(nGXsfl_29_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_26_9C2e( true) ;
         }
         else
         {
            wb_table4_26_9C2e( false) ;
         }
      }

      protected void wb_table2_5_9C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_29_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "ImageAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptFuncaoDados.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_29_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_13_9C2( true) ;
         }
         else
         {
            wb_table5_13_9C2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_9C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_9C2e( true) ;
         }
         else
         {
            wb_table2_5_9C2e( false) ;
         }
      }

      protected void wb_table5_13_9C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextfuncaodados_nome_Internalname, "Nome", "", "", lblFiltertextfuncaodados_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_PromptFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_29_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome_Internalname, AV36FuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", 0, 1, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_9C2e( true) ;
         }
         else
         {
            wb_table5_13_9C2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutFuncaoDados_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoDados_Codigo), 6, 0)));
         AV8InOutFuncaoDados_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoDados_Nome", AV8InOutFuncaoDados_Nome);
         AV34InOutFuncaoDados_Descricao = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34InOutFuncaoDados_Descricao", AV34InOutFuncaoDados_Descricao);
         AV37FuncaoDados_Tipo = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoDados_Tipo", AV37FuncaoDados_Tipo);
         AV33FuncaoDados_SistemaCod = Convert.ToInt32(getParm(obj,4));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33FuncaoDados_SistemaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFUNCAODADOS_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV33FuncaoDados_SistemaCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9C2( ) ;
         WS9C2( ) ;
         WE9C2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311738384");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptfuncaodados.js", "?2020311738384");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_292( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_29_idx;
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO_"+sGXsfl_29_idx;
         edtFuncaoDados_SistemaCod_Internalname = "FUNCAODADOS_SISTEMACOD_"+sGXsfl_29_idx;
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME_"+sGXsfl_29_idx;
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO_"+sGXsfl_29_idx;
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_29_idx;
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER_"+sGXsfl_29_idx;
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR_"+sGXsfl_29_idx;
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF_"+sGXsfl_29_idx;
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO_"+sGXsfl_29_idx;
      }

      protected void SubsflControlProps_fel_292( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_29_fel_idx;
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO_"+sGXsfl_29_fel_idx;
         edtFuncaoDados_SistemaCod_Internalname = "FUNCAODADOS_SISTEMACOD_"+sGXsfl_29_fel_idx;
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME_"+sGXsfl_29_fel_idx;
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO_"+sGXsfl_29_fel_idx;
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_29_fel_idx;
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER_"+sGXsfl_29_fel_idx;
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR_"+sGXsfl_29_fel_idx;
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF_"+sGXsfl_29_fel_idx;
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO_"+sGXsfl_29_fel_idx;
      }

      protected void sendrow_292( )
      {
         SubsflControlProps_292( ) ;
         WB9C0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_29_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_29_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_29_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 30,'',false,'',29)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV30Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV72Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Select)) ? AV72Select_GXI : context.PathToRelativeUrl( AV30Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_29_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV30Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Nome_Internalname,(String)A369FuncaoDados_Nome,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_29_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_29_idx;
               cmbFuncaoDados_Tipo.Name = GXCCtl;
               cmbFuncaoDados_Tipo.WebTags = "";
               cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
               cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
               cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
               cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
               if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
               {
                  A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Tipo,(String)cmbFuncaoDados_Tipo_Internalname,StringUtil.RTrim( A373FuncaoDados_Tipo),(short)1,(String)cmbFuncaoDados_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Tipo.CurrentValue = StringUtil.RTrim( A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Tipo_Internalname, "Values", (String)(cmbFuncaoDados_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_29_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Complexidade,(String)cmbFuncaoDados_Complexidade_Internalname,StringUtil.RTrim( A376FuncaoDados_Complexidade),(short)1,(String)cmbFuncaoDados_Complexidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_DER_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_DER_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_RLR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_RLR_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")),context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)29,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_29_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAODADOS_ATIVO_" + sGXsfl_29_idx;
               cmbFuncaoDados_Ativo.Name = GXCCtl;
               cmbFuncaoDados_Ativo.WebTags = "";
               cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
               cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
               cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
               if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
               {
                  A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Ativo,(String)cmbFuncaoDados_Ativo_Internalname,StringUtil.RTrim( A394FuncaoDados_Ativo),(short)1,(String)cmbFuncaoDados_Ativo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Ativo.CurrentValue = StringUtil.RTrim( A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Values", (String)(cmbFuncaoDados_Ativo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_CODIGO"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_SISTEMACOD"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, context.localUtil.Format( (decimal)(A370FuncaoDados_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_NOME"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_TIPO"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_COMPLEXIDADE"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_DER"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_RLR"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_PF"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_ATIVO"+"_"+sGXsfl_29_idx, GetSecureSignedToken( sGXsfl_29_idx, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_29_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_29_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_29_idx+1));
            sGXsfl_29_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_29_idx), 4, 0)), 4, "0");
            SubsflControlProps_292( ) ;
         }
         /* End function sendrow_292 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextfuncaodados_nome_Internalname = "FILTERTEXTFUNCAODADOS_NOME";
         edtavFuncaodados_nome_Internalname = "vFUNCAODADOS_NOME";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO";
         edtFuncaoDados_SistemaCod_Internalname = "FUNCAODADOS_SISTEMACOD";
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME";
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO";
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE";
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER";
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR";
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF";
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtFuncaoDados_Descricao_Internalname = "FUNCAODADOS_DESCRICAO";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTffuncaodados_nome_Internalname = "vTFFUNCAODADOS_NOME";
         edtavTffuncaodados_nome_sel_Internalname = "vTFFUNCAODADOS_NOME_SEL";
         edtavTffuncaodados_der_Internalname = "vTFFUNCAODADOS_DER";
         edtavTffuncaodados_der_to_Internalname = "vTFFUNCAODADOS_DER_TO";
         edtavTffuncaodados_rlr_Internalname = "vTFFUNCAODADOS_RLR";
         edtavTffuncaodados_rlr_to_Internalname = "vTFFUNCAODADOS_RLR_TO";
         edtavTffuncaodados_pf_Internalname = "vTFFUNCAODADOS_PF";
         edtavTffuncaodados_pf_to_Internalname = "vTFFUNCAODADOS_PF_TO";
         Ddo_funcaodados_nome_Internalname = "DDO_FUNCAODADOS_NOME";
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_tipo_Internalname = "DDO_FUNCAODADOS_TIPO";
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_complexidade_Internalname = "DDO_FUNCAODADOS_COMPLEXIDADE";
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_der_Internalname = "DDO_FUNCAODADOS_DER";
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_rlr_Internalname = "DDO_FUNCAODADOS_RLR";
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_pf_Internalname = "DDO_FUNCAODADOS_PF";
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_ativo_Internalname = "DDO_FUNCAODADOS_ATIVO";
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbFuncaoDados_Ativo_Jsonclick = "";
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_DER_Jsonclick = "";
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         cmbFuncaoDados_Tipo_Jsonclick = "";
         edtFuncaoDados_Nome_Jsonclick = "";
         edtFuncaoDados_SistemaCod_Jsonclick = "";
         edtFuncaoDados_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbFuncaoDados_Ativo_Titleformat = 0;
         edtFuncaoDados_PF_Titleformat = 0;
         edtFuncaoDados_RLR_Titleformat = 0;
         edtFuncaoDados_DER_Titleformat = 0;
         cmbFuncaoDados_Complexidade_Titleformat = 0;
         cmbFuncaoDados_Tipo_Titleformat = 0;
         edtFuncaoDados_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavFuncaodados_nome_Width = 80;
         edtavFuncaodados_nome_Height = 3;
         cmbFuncaoDados_Ativo.Title.Text = "Status";
         edtFuncaoDados_PF_Title = "PF";
         edtFuncaoDados_RLR_Title = "RLR";
         edtFuncaoDados_DER_Title = "DER";
         cmbFuncaoDados_Complexidade.Title.Text = "Complexidade";
         cmbFuncaoDados_Tipo.Title.Text = "Tipo";
         edtFuncaoDados_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaodados_pf_to_Jsonclick = "";
         edtavTffuncaodados_pf_to_Visible = 1;
         edtavTffuncaodados_pf_Jsonclick = "";
         edtavTffuncaodados_pf_Visible = 1;
         edtavTffuncaodados_rlr_to_Jsonclick = "";
         edtavTffuncaodados_rlr_to_Visible = 1;
         edtavTffuncaodados_rlr_Jsonclick = "";
         edtavTffuncaodados_rlr_Visible = 1;
         edtavTffuncaodados_der_to_Jsonclick = "";
         edtavTffuncaodados_der_to_Visible = 1;
         edtavTffuncaodados_der_Jsonclick = "";
         edtavTffuncaodados_der_Visible = 1;
         edtavTffuncaodados_nome_sel_Visible = 1;
         edtavTffuncaodados_nome_Visible = 1;
         edtFuncaoDados_Descricao_Visible = 1;
         Ddo_funcaodados_ativo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_ativo_Datalistfixedvalues = "A:Ativa,E:Exclu�da,R:Rejeitada";
         Ddo_funcaodados_ativo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Datalisttype = "FixedValues";
         Ddo_funcaodados_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_ativo_Cls = "ColumnSettings";
         Ddo_funcaodados_ativo_Tooltip = "Op��es";
         Ddo_funcaodados_ativo_Caption = "";
         Ddo_funcaodados_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_pf_Rangefilterto = "At�";
         Ddo_funcaodados_pf_Rangefilterfrom = "Desde";
         Ddo_funcaodados_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Filtertype = "Numeric";
         Ddo_funcaodados_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_pf_Cls = "ColumnSettings";
         Ddo_funcaodados_pf_Tooltip = "Op��es";
         Ddo_funcaodados_pf_Caption = "";
         Ddo_funcaodados_rlr_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_rlr_Rangefilterto = "At�";
         Ddo_funcaodados_rlr_Rangefilterfrom = "Desde";
         Ddo_funcaodados_rlr_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_rlr_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Filtertype = "Numeric";
         Ddo_funcaodados_rlr_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_rlr_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_rlr_Cls = "ColumnSettings";
         Ddo_funcaodados_rlr_Tooltip = "Op��es";
         Ddo_funcaodados_rlr_Caption = "";
         Ddo_funcaodados_der_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_der_Rangefilterto = "At�";
         Ddo_funcaodados_der_Rangefilterfrom = "Desde";
         Ddo_funcaodados_der_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_der_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Filtertype = "Numeric";
         Ddo_funcaodados_der_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_der_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_der_Cls = "ColumnSettings";
         Ddo_funcaodados_der_Tooltip = "Op��es";
         Ddo_funcaodados_der_Caption = "";
         Ddo_funcaodados_complexidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_complexidade_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_complexidade_Datalistfixedvalues = "E:,B:Baixa,M:M�dia,A:Alta";
         Ddo_funcaodados_complexidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Datalisttype = "FixedValues";
         Ddo_funcaodados_complexidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_complexidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_complexidade_Cls = "ColumnSettings";
         Ddo_funcaodados_complexidade_Tooltip = "Op��es";
         Ddo_funcaodados_complexidade_Caption = "";
         Ddo_funcaodados_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_tipo_Datalistfixedvalues = "ALI:ALI,AIE:AIE,DC:DC";
         Ddo_funcaodados_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Datalisttype = "FixedValues";
         Ddo_funcaodados_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_tipo_Cls = "ColumnSettings";
         Ddo_funcaodados_tipo_Tooltip = "Op��es";
         Ddo_funcaodados_tipo_Caption = "";
         Ddo_funcaodados_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_nome_Datalistproc = "GetPromptFuncaoDadosFilterData";
         Ddo_funcaodados_nome_Datalisttype = "Dynamic";
         Ddo_funcaodados_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaodados_nome_Filtertype = "Character";
         Ddo_funcaodados_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_nome_Cls = "ColumnSettings";
         Ddo_funcaodados_nome_Tooltip = "Op��es";
         Ddo_funcaodados_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Fun��o de Dados";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV38FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV42FuncaoDados_TipoTitleFilterData',fld:'vFUNCAODADOS_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV50FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV54FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV58FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV62FuncaoDados_AtivoTitleFilterData',fld:'vFUNCAODADOS_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Tipo'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'cmbFuncaoDados_Ativo'},{av:'AV68GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV69GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavFuncaodados_nome_Height',ctrl:'vFUNCAODADOS_NOME',prop:'Height'},{av:'edtavFuncaodados_nome_Width',ctrl:'vFUNCAODADOS_NOME',prop:'Width'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E119C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED","{handler:'E129C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Activeeventkey',ctrl:'DDO_FUNCAODADOS_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_nome_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaodados_nome_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_TIPO.ONOPTIONCLICKED","{handler:'E139C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_funcaodados_tipo_Activeeventkey',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_tipo_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED","{handler:'E149C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_funcaodados_complexidade_Activeeventkey',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_complexidade_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAODADOS_DER.ONOPTIONCLICKED","{handler:'E159C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_funcaodados_der_Activeeventkey',ctrl:'DDO_FUNCAODADOS_DER',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_der_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_get'},{av:'Ddo_funcaodados_der_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_get'}],oparms:[{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED","{handler:'E169C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_funcaodados_rlr_Activeeventkey',ctrl:'DDO_FUNCAODADOS_RLR',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_rlr_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_get'},{av:'Ddo_funcaodados_rlr_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_PF.ONOPTIONCLICKED","{handler:'E179C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_funcaodados_pf_Activeeventkey',ctrl:'DDO_FUNCAODADOS_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_pf_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_get'},{av:'Ddo_funcaodados_pf_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_FUNCAODADOS_ATIVO.ONOPTIONCLICKED","{handler:'E189C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Ddo_funcaodados_ativo_Activeeventkey',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_ativo_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E239C2',iparms:[],oparms:[{av:'AV30Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E249C2',iparms:[{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',hsh:true,nv:''},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A369FuncaoDados_Nome',fld:'FUNCAODADOS_NOME',pic:'',hsh:true,nv:''},{av:'A397FuncaoDados_Descricao',fld:'FUNCAODADOS_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV37FuncaoDados_Tipo',fld:'vFUNCAODADOS_TIPO',pic:'',nv:''},{av:'AV7InOutFuncaoDados_Codigo',fld:'vINOUTFUNCAODADOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutFuncaoDados_Nome',fld:'vINOUTFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV34InOutFuncaoDados_Descricao',fld:'vINOUTFUNCAODADOS_DESCRICAO',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E199C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E209C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV32OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV33FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV73Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV36FuncaoDados_Nome',fld:'vFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_set'},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_set'},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_tipo_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SelectedValue_set'},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_complexidade_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_set'},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_set'},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_set'},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_set'},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_set'},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_set'},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_set'},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_ativo_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SelectedValue_set'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutFuncaoDados_Nome = "";
         wcpOAV34InOutFuncaoDados_Descricao = "";
         wcpOAV37FuncaoDados_Tipo = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaodados_nome_Activeeventkey = "";
         Ddo_funcaodados_nome_Filteredtext_get = "";
         Ddo_funcaodados_nome_Selectedvalue_get = "";
         Ddo_funcaodados_tipo_Activeeventkey = "";
         Ddo_funcaodados_tipo_Selectedvalue_get = "";
         Ddo_funcaodados_complexidade_Activeeventkey = "";
         Ddo_funcaodados_complexidade_Selectedvalue_get = "";
         Ddo_funcaodados_der_Activeeventkey = "";
         Ddo_funcaodados_der_Filteredtext_get = "";
         Ddo_funcaodados_der_Filteredtextto_get = "";
         Ddo_funcaodados_rlr_Activeeventkey = "";
         Ddo_funcaodados_rlr_Filteredtext_get = "";
         Ddo_funcaodados_rlr_Filteredtextto_get = "";
         Ddo_funcaodados_pf_Activeeventkey = "";
         Ddo_funcaodados_pf_Filteredtext_get = "";
         Ddo_funcaodados_pf_Filteredtextto_get = "";
         Ddo_funcaodados_ativo_Activeeventkey = "";
         Ddo_funcaodados_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV36FuncaoDados_Nome = "";
         AV39TFFuncaoDados_Nome = "";
         AV40TFFuncaoDados_Nome_Sel = "";
         AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = "";
         AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = "";
         AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = "";
         AV53ddo_FuncaoDados_DERTitleControlIdToReplace = "";
         AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = "";
         AV61ddo_FuncaoDados_PFTitleControlIdToReplace = "";
         AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = "";
         AV44TFFuncaoDados_Tipo_Sels = new GxSimpleCollection();
         AV48TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV64TFFuncaoDados_Ativo_Sels = new GxSimpleCollection();
         AV73Pgmname = "";
         GXKey = "";
         forbiddenHiddens = "";
         A397FuncaoDados_Descricao = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV66DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV38FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoDados_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62FuncaoDados_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaodados_nome_Filteredtext_set = "";
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         Ddo_funcaodados_nome_Sortedstatus = "";
         Ddo_funcaodados_tipo_Selectedvalue_set = "";
         Ddo_funcaodados_tipo_Sortedstatus = "";
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         Ddo_funcaodados_der_Filteredtext_set = "";
         Ddo_funcaodados_der_Filteredtextto_set = "";
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         Ddo_funcaodados_pf_Filteredtext_set = "";
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         Ddo_funcaodados_ativo_Selectedvalue_set = "";
         Ddo_funcaodados_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30Select = "";
         AV72Select_GXI = "";
         A369FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         A376FuncaoDados_Complexidade = "";
         A394FuncaoDados_Ativo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV36FuncaoDados_Nome = "";
         lV39TFFuncaoDados_Nome = "";
         H009C2_A397FuncaoDados_Descricao = new String[] {""} ;
         H009C2_n397FuncaoDados_Descricao = new bool[] {false} ;
         H009C2_A394FuncaoDados_Ativo = new String[] {""} ;
         H009C2_A373FuncaoDados_Tipo = new String[] {""} ;
         H009C2_A369FuncaoDados_Nome = new String[] {""} ;
         H009C2_A370FuncaoDados_SistemaCod = new int[1] ;
         H009C2_A755FuncaoDados_Contar = new bool[] {false} ;
         H009C2_n755FuncaoDados_Contar = new bool[] {false} ;
         H009C2_A368FuncaoDados_Codigo = new int[1] ;
         GXt_char1 = "";
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV43TFFuncaoDados_Tipo_SelsJson = "";
         AV47TFFuncaoDados_Complexidade_SelsJson = "";
         AV63TFFuncaoDados_Ativo_SelsJson = "";
         GridRow = new GXWebRow();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblFiltertextfuncaodados_nome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptfuncaodados__default(),
            new Object[][] {
                new Object[] {
               H009C2_A397FuncaoDados_Descricao, H009C2_n397FuncaoDados_Descricao, H009C2_A394FuncaoDados_Ativo, H009C2_A373FuncaoDados_Tipo, H009C2_A369FuncaoDados_Nome, H009C2_A370FuncaoDados_SistemaCod, H009C2_A755FuncaoDados_Contar, H009C2_n755FuncaoDados_Contar, H009C2_A368FuncaoDados_Codigo
               }
            }
         );
         AV73Pgmname = "PromptFuncaoDados";
         /* GeneXus formulas. */
         AV73Pgmname = "PromptFuncaoDados";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_29 ;
      private short nGXsfl_29_idx=1 ;
      private short AV32OrderedBy ;
      private short AV51TFFuncaoDados_DER ;
      private short AV52TFFuncaoDados_DER_To ;
      private short AV55TFFuncaoDados_RLR ;
      private short AV56TFFuncaoDados_RLR_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_29_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int2 ;
      private short edtFuncaoDados_Nome_Titleformat ;
      private short cmbFuncaoDados_Tipo_Titleformat ;
      private short cmbFuncaoDados_Complexidade_Titleformat ;
      private short edtFuncaoDados_DER_Titleformat ;
      private short edtFuncaoDados_RLR_Titleformat ;
      private short edtFuncaoDados_PF_Titleformat ;
      private short cmbFuncaoDados_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutFuncaoDados_Codigo ;
      private int AV33FuncaoDados_SistemaCod ;
      private int wcpOAV7InOutFuncaoDados_Codigo ;
      private int wcpOAV33FuncaoDados_SistemaCod ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaodados_nome_Datalistupdateminimumcharacters ;
      private int edtFuncaoDados_Descricao_Visible ;
      private int edtavTffuncaodados_nome_Visible ;
      private int edtavTffuncaodados_nome_sel_Visible ;
      private int edtavTffuncaodados_der_Visible ;
      private int edtavTffuncaodados_der_to_Visible ;
      private int edtavTffuncaodados_rlr_Visible ;
      private int edtavTffuncaodados_rlr_to_Visible ;
      private int edtavTffuncaodados_pf_Visible ;
      private int edtavTffuncaodados_pf_to_Visible ;
      private int edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible ;
      private int A368FuncaoDados_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int subGrid_Islastpage ;
      private int AV44TFFuncaoDados_Tipo_Sels_Count ;
      private int AV64TFFuncaoDados_Ativo_Sels_Count ;
      private int AV48TFFuncaoDados_Complexidade_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int edtavFuncaodados_nome_Height ;
      private int edtavFuncaodados_nome_Width ;
      private int AV67PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV68GridCurrentPage ;
      private long AV69GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV59TFFuncaoDados_PF ;
      private decimal AV60TFFuncaoDados_PF_To ;
      private decimal A377FuncaoDados_PF ;
      private String AV37FuncaoDados_Tipo ;
      private String wcpOAV37FuncaoDados_Tipo ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaodados_nome_Activeeventkey ;
      private String Ddo_funcaodados_nome_Filteredtext_get ;
      private String Ddo_funcaodados_nome_Selectedvalue_get ;
      private String Ddo_funcaodados_tipo_Activeeventkey ;
      private String Ddo_funcaodados_tipo_Selectedvalue_get ;
      private String Ddo_funcaodados_complexidade_Activeeventkey ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_get ;
      private String Ddo_funcaodados_der_Activeeventkey ;
      private String Ddo_funcaodados_der_Filteredtext_get ;
      private String Ddo_funcaodados_der_Filteredtextto_get ;
      private String Ddo_funcaodados_rlr_Activeeventkey ;
      private String Ddo_funcaodados_rlr_Filteredtext_get ;
      private String Ddo_funcaodados_rlr_Filteredtextto_get ;
      private String Ddo_funcaodados_pf_Activeeventkey ;
      private String Ddo_funcaodados_pf_Filteredtext_get ;
      private String Ddo_funcaodados_pf_Filteredtextto_get ;
      private String Ddo_funcaodados_ativo_Activeeventkey ;
      private String Ddo_funcaodados_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_29_idx="0001" ;
      private String AV73Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaodados_nome_Caption ;
      private String Ddo_funcaodados_nome_Tooltip ;
      private String Ddo_funcaodados_nome_Cls ;
      private String Ddo_funcaodados_nome_Filteredtext_set ;
      private String Ddo_funcaodados_nome_Selectedvalue_set ;
      private String Ddo_funcaodados_nome_Dropdownoptionstype ;
      private String Ddo_funcaodados_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_nome_Sortedstatus ;
      private String Ddo_funcaodados_nome_Filtertype ;
      private String Ddo_funcaodados_nome_Datalisttype ;
      private String Ddo_funcaodados_nome_Datalistproc ;
      private String Ddo_funcaodados_nome_Sortasc ;
      private String Ddo_funcaodados_nome_Sortdsc ;
      private String Ddo_funcaodados_nome_Loadingdata ;
      private String Ddo_funcaodados_nome_Cleanfilter ;
      private String Ddo_funcaodados_nome_Noresultsfound ;
      private String Ddo_funcaodados_nome_Searchbuttontext ;
      private String Ddo_funcaodados_tipo_Caption ;
      private String Ddo_funcaodados_tipo_Tooltip ;
      private String Ddo_funcaodados_tipo_Cls ;
      private String Ddo_funcaodados_tipo_Selectedvalue_set ;
      private String Ddo_funcaodados_tipo_Dropdownoptionstype ;
      private String Ddo_funcaodados_tipo_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_tipo_Sortedstatus ;
      private String Ddo_funcaodados_tipo_Datalisttype ;
      private String Ddo_funcaodados_tipo_Datalistfixedvalues ;
      private String Ddo_funcaodados_tipo_Sortasc ;
      private String Ddo_funcaodados_tipo_Sortdsc ;
      private String Ddo_funcaodados_tipo_Cleanfilter ;
      private String Ddo_funcaodados_tipo_Searchbuttontext ;
      private String Ddo_funcaodados_complexidade_Caption ;
      private String Ddo_funcaodados_complexidade_Tooltip ;
      private String Ddo_funcaodados_complexidade_Cls ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_set ;
      private String Ddo_funcaodados_complexidade_Dropdownoptionstype ;
      private String Ddo_funcaodados_complexidade_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_complexidade_Datalisttype ;
      private String Ddo_funcaodados_complexidade_Datalistfixedvalues ;
      private String Ddo_funcaodados_complexidade_Cleanfilter ;
      private String Ddo_funcaodados_complexidade_Searchbuttontext ;
      private String Ddo_funcaodados_der_Caption ;
      private String Ddo_funcaodados_der_Tooltip ;
      private String Ddo_funcaodados_der_Cls ;
      private String Ddo_funcaodados_der_Filteredtext_set ;
      private String Ddo_funcaodados_der_Filteredtextto_set ;
      private String Ddo_funcaodados_der_Dropdownoptionstype ;
      private String Ddo_funcaodados_der_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_der_Filtertype ;
      private String Ddo_funcaodados_der_Cleanfilter ;
      private String Ddo_funcaodados_der_Rangefilterfrom ;
      private String Ddo_funcaodados_der_Rangefilterto ;
      private String Ddo_funcaodados_der_Searchbuttontext ;
      private String Ddo_funcaodados_rlr_Caption ;
      private String Ddo_funcaodados_rlr_Tooltip ;
      private String Ddo_funcaodados_rlr_Cls ;
      private String Ddo_funcaodados_rlr_Filteredtext_set ;
      private String Ddo_funcaodados_rlr_Filteredtextto_set ;
      private String Ddo_funcaodados_rlr_Dropdownoptionstype ;
      private String Ddo_funcaodados_rlr_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_rlr_Filtertype ;
      private String Ddo_funcaodados_rlr_Cleanfilter ;
      private String Ddo_funcaodados_rlr_Rangefilterfrom ;
      private String Ddo_funcaodados_rlr_Rangefilterto ;
      private String Ddo_funcaodados_rlr_Searchbuttontext ;
      private String Ddo_funcaodados_pf_Caption ;
      private String Ddo_funcaodados_pf_Tooltip ;
      private String Ddo_funcaodados_pf_Cls ;
      private String Ddo_funcaodados_pf_Filteredtext_set ;
      private String Ddo_funcaodados_pf_Filteredtextto_set ;
      private String Ddo_funcaodados_pf_Dropdownoptionstype ;
      private String Ddo_funcaodados_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_pf_Filtertype ;
      private String Ddo_funcaodados_pf_Cleanfilter ;
      private String Ddo_funcaodados_pf_Rangefilterfrom ;
      private String Ddo_funcaodados_pf_Rangefilterto ;
      private String Ddo_funcaodados_pf_Searchbuttontext ;
      private String Ddo_funcaodados_ativo_Caption ;
      private String Ddo_funcaodados_ativo_Tooltip ;
      private String Ddo_funcaodados_ativo_Cls ;
      private String Ddo_funcaodados_ativo_Selectedvalue_set ;
      private String Ddo_funcaodados_ativo_Dropdownoptionstype ;
      private String Ddo_funcaodados_ativo_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_ativo_Sortedstatus ;
      private String Ddo_funcaodados_ativo_Datalisttype ;
      private String Ddo_funcaodados_ativo_Datalistfixedvalues ;
      private String Ddo_funcaodados_ativo_Sortasc ;
      private String Ddo_funcaodados_ativo_Sortdsc ;
      private String Ddo_funcaodados_ativo_Cleanfilter ;
      private String Ddo_funcaodados_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String edtFuncaoDados_Descricao_Internalname ;
      private String TempTags ;
      private String edtavTffuncaodados_nome_Internalname ;
      private String edtavTffuncaodados_nome_sel_Internalname ;
      private String edtavTffuncaodados_der_Internalname ;
      private String edtavTffuncaodados_der_Jsonclick ;
      private String edtavTffuncaodados_der_to_Internalname ;
      private String edtavTffuncaodados_der_to_Jsonclick ;
      private String edtavTffuncaodados_rlr_Internalname ;
      private String edtavTffuncaodados_rlr_Jsonclick ;
      private String edtavTffuncaodados_rlr_to_Internalname ;
      private String edtavTffuncaodados_rlr_to_Jsonclick ;
      private String edtavTffuncaodados_pf_Internalname ;
      private String edtavTffuncaodados_pf_Jsonclick ;
      private String edtavTffuncaodados_pf_to_Internalname ;
      private String edtavTffuncaodados_pf_to_Jsonclick ;
      private String edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String edtFuncaoDados_SistemaCod_Internalname ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String cmbFuncaoDados_Tipo_Internalname ;
      private String A373FuncaoDados_Tipo ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String A376FuncaoDados_Complexidade ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_PF_Internalname ;
      private String cmbFuncaoDados_Ativo_Internalname ;
      private String A394FuncaoDados_Ativo ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavFuncaodados_nome_Internalname ;
      private String hsh ;
      private String subGrid_Internalname ;
      private String Ddo_funcaodados_nome_Internalname ;
      private String Ddo_funcaodados_tipo_Internalname ;
      private String Ddo_funcaodados_complexidade_Internalname ;
      private String Ddo_funcaodados_der_Internalname ;
      private String Ddo_funcaodados_rlr_Internalname ;
      private String Ddo_funcaodados_pf_Internalname ;
      private String Ddo_funcaodados_ativo_Internalname ;
      private String edtFuncaoDados_Nome_Title ;
      private String edtFuncaoDados_DER_Title ;
      private String edtFuncaoDados_RLR_Title ;
      private String edtFuncaoDados_PF_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextfuncaodados_nome_Internalname ;
      private String lblFiltertextfuncaodados_nome_Jsonclick ;
      private String sGXsfl_29_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String edtFuncaoDados_SistemaCod_Jsonclick ;
      private String edtFuncaoDados_Nome_Jsonclick ;
      private String cmbFuncaoDados_Tipo_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String cmbFuncaoDados_Ativo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaodados_nome_Includesortasc ;
      private bool Ddo_funcaodados_nome_Includesortdsc ;
      private bool Ddo_funcaodados_nome_Includefilter ;
      private bool Ddo_funcaodados_nome_Filterisrange ;
      private bool Ddo_funcaodados_nome_Includedatalist ;
      private bool Ddo_funcaodados_tipo_Includesortasc ;
      private bool Ddo_funcaodados_tipo_Includesortdsc ;
      private bool Ddo_funcaodados_tipo_Includefilter ;
      private bool Ddo_funcaodados_tipo_Includedatalist ;
      private bool Ddo_funcaodados_tipo_Allowmultipleselection ;
      private bool Ddo_funcaodados_complexidade_Includesortasc ;
      private bool Ddo_funcaodados_complexidade_Includesortdsc ;
      private bool Ddo_funcaodados_complexidade_Includefilter ;
      private bool Ddo_funcaodados_complexidade_Includedatalist ;
      private bool Ddo_funcaodados_complexidade_Allowmultipleselection ;
      private bool Ddo_funcaodados_der_Includesortasc ;
      private bool Ddo_funcaodados_der_Includesortdsc ;
      private bool Ddo_funcaodados_der_Includefilter ;
      private bool Ddo_funcaodados_der_Filterisrange ;
      private bool Ddo_funcaodados_der_Includedatalist ;
      private bool Ddo_funcaodados_rlr_Includesortasc ;
      private bool Ddo_funcaodados_rlr_Includesortdsc ;
      private bool Ddo_funcaodados_rlr_Includefilter ;
      private bool Ddo_funcaodados_rlr_Filterisrange ;
      private bool Ddo_funcaodados_rlr_Includedatalist ;
      private bool Ddo_funcaodados_pf_Includesortasc ;
      private bool Ddo_funcaodados_pf_Includesortdsc ;
      private bool Ddo_funcaodados_pf_Includefilter ;
      private bool Ddo_funcaodados_pf_Filterisrange ;
      private bool Ddo_funcaodados_pf_Includedatalist ;
      private bool Ddo_funcaodados_ativo_Includesortasc ;
      private bool Ddo_funcaodados_ativo_Includesortdsc ;
      private bool Ddo_funcaodados_ativo_Includefilter ;
      private bool Ddo_funcaodados_ativo_Includedatalist ;
      private bool Ddo_funcaodados_ativo_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n397FuncaoDados_Descricao ;
      private bool n755FuncaoDados_Contar ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV30Select_IsBlob ;
      private String AV34InOutFuncaoDados_Descricao ;
      private String wcpOAV34InOutFuncaoDados_Descricao ;
      private String A397FuncaoDados_Descricao ;
      private String AV43TFFuncaoDados_Tipo_SelsJson ;
      private String AV47TFFuncaoDados_Complexidade_SelsJson ;
      private String AV63TFFuncaoDados_Ativo_SelsJson ;
      private String AV8InOutFuncaoDados_Nome ;
      private String wcpOAV8InOutFuncaoDados_Nome ;
      private String AV36FuncaoDados_Nome ;
      private String AV39TFFuncaoDados_Nome ;
      private String AV40TFFuncaoDados_Nome_Sel ;
      private String AV41ddo_FuncaoDados_NomeTitleControlIdToReplace ;
      private String AV45ddo_FuncaoDados_TipoTitleControlIdToReplace ;
      private String AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ;
      private String AV53ddo_FuncaoDados_DERTitleControlIdToReplace ;
      private String AV57ddo_FuncaoDados_RLRTitleControlIdToReplace ;
      private String AV61ddo_FuncaoDados_PFTitleControlIdToReplace ;
      private String AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace ;
      private String AV72Select_GXI ;
      private String A369FuncaoDados_Nome ;
      private String lV36FuncaoDados_Nome ;
      private String lV39TFFuncaoDados_Nome ;
      private String AV30Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutFuncaoDados_Codigo ;
      private String aP1_InOutFuncaoDados_Nome ;
      private String aP2_InOutFuncaoDados_Descricao ;
      private String aP3_FuncaoDados_Tipo ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbFuncaoDados_Tipo ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private GXCombobox cmbFuncaoDados_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] H009C2_A397FuncaoDados_Descricao ;
      private bool[] H009C2_n397FuncaoDados_Descricao ;
      private String[] H009C2_A394FuncaoDados_Ativo ;
      private String[] H009C2_A373FuncaoDados_Tipo ;
      private String[] H009C2_A369FuncaoDados_Nome ;
      private int[] H009C2_A370FuncaoDados_SistemaCod ;
      private bool[] H009C2_A755FuncaoDados_Contar ;
      private bool[] H009C2_n755FuncaoDados_Contar ;
      private int[] H009C2_A368FuncaoDados_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44TFFuncaoDados_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV48TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV64TFFuncaoDados_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38FuncaoDados_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42FuncaoDados_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46FuncaoDados_ComplexidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50FuncaoDados_DERTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54FuncaoDados_RLRTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58FuncaoDados_PFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62FuncaoDados_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV66DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 ;
   }

   public class promptfuncaodados__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009C2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV48TFFuncaoDados_Complexidade_Sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV44TFFuncaoDados_Tipo_Sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV64TFFuncaoDados_Ativo_Sels ,
                                             String AV36FuncaoDados_Nome ,
                                             String AV40TFFuncaoDados_Nome_Sel ,
                                             String AV39TFFuncaoDados_Nome ,
                                             int AV44TFFuncaoDados_Tipo_Sels_Count ,
                                             int AV64TFFuncaoDados_Ativo_Sels_Count ,
                                             int AV33FuncaoDados_SistemaCod ,
                                             String A369FuncaoDados_Nome ,
                                             int A370FuncaoDados_SistemaCod ,
                                             short AV32OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int AV48TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV51TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV52TFFuncaoDados_DER_To ,
                                             short AV55TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV56TFFuncaoDados_RLR_To ,
                                             decimal AV59TFFuncaoDados_PF ,
                                             decimal A377FuncaoDados_PF ,
                                             decimal AV60TFFuncaoDados_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [5] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoDados_Descricao], [FuncaoDados_Ativo], [FuncaoDados_Tipo], [FuncaoDados_Nome], [FuncaoDados_SistemaCod], [FuncaoDados_Contar], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36FuncaoDados_Nome)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_Nome] like '%' + @lV36FuncaoDados_Nome + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_Nome] like '%' + @lV36FuncaoDados_Nome + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoDados_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_Nome] like @lV39TFFuncaoDados_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_Nome] like @lV39TFFuncaoDados_Nome)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoDados_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_Nome] = @AV40TFFuncaoDados_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_Nome] = @AV40TFFuncaoDados_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV44TFFuncaoDados_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44TFFuncaoDados_Tipo_Sels, "[FuncaoDados_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV44TFFuncaoDados_Tipo_Sels, "[FuncaoDados_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV64TFFuncaoDados_Ativo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64TFFuncaoDados_Ativo_Sels, "[FuncaoDados_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV64TFFuncaoDados_Ativo_Sels, "[FuncaoDados_Ativo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV33FuncaoDados_SistemaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_SistemaCod] = @AV33FuncaoDados_SistemaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_SistemaCod] = @AV33FuncaoDados_SistemaCod)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV32OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Nome]";
         }
         else if ( ( AV32OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Nome] DESC";
         }
         else if ( ( AV32OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Tipo]";
         }
         else if ( ( AV32OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Tipo] DESC";
         }
         else if ( ( AV32OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Ativo]";
         }
         else if ( ( AV32OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Ativo] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H009C2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (short)dynConstraints[14] , (bool)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009C2 ;
          prmH009C2 = new Object[] {
          new Object[] {"@AV48TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV36FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV39TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV40TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV33FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009C2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009C2,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                return;
       }
    }

 }

}
