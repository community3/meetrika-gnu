/*
               File: WWEstado
        Description:  Estado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:31:7.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwestado : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwestado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwestado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavEstado_uf1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavEstado_uf2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavEstado_uf3 = new GXCombobox();
         chkEstado_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvESTADO_UF1172( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF2") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvESTADO_UF2172( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF3") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvESTADO_UF3172( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_76 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_76_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_76_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV55Estado_UF1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Estado_UF1", AV55Estado_UF1);
               AV17Estado_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Estado_Nome1", AV17Estado_Nome1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV56Estado_UF2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Estado_UF2", AV56Estado_UF2);
               AV21Estado_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Estado_Nome2", AV21Estado_Nome2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV57Estado_UF3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Estado_UF3", AV57Estado_UF3);
               AV25Estado_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Estado_Nome3", AV25Estado_Nome3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV63TFEstado_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFEstado_Nome", AV63TFEstado_Nome);
               AV64TFEstado_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFEstado_Nome_Sel", AV64TFEstado_Nome_Sel);
               AV67TFEstado_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFEstado_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0));
               AV73ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV73ManageFiltersExecutionStep), 1, 0));
               AV65ddo_Estado_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Estado_NomeTitleControlIdToReplace", AV65ddo_Estado_NomeTitleControlIdToReplace);
               AV68ddo_Estado_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Estado_AtivoTitleControlIdToReplace", AV68ddo_Estado_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV98Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A23Estado_UF = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA172( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START172( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311731816");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwestado.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_UF1", StringUtil.RTrim( AV55Estado_UF1));
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_NOME1", StringUtil.RTrim( AV17Estado_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_UF2", StringUtil.RTrim( AV56Estado_UF2));
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_NOME2", StringUtil.RTrim( AV21Estado_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_UF3", StringUtil.RTrim( AV57Estado_UF3));
         GxWebStd.gx_hidden_field( context, "GXH_vESTADO_NOME3", StringUtil.RTrim( AV25Estado_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_NOME", StringUtil.RTrim( AV63TFEstado_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_NOME_SEL", StringUtil.RTrim( AV64TFEstado_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_76", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_76), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV77ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV77ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV69DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV69DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vESTADO_NOMETITLEFILTERDATA", AV62Estado_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vESTADO_NOMETITLEFILTERDATA", AV62Estado_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vESTADO_ATIVOTITLEFILTERDATA", AV66Estado_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vESTADO_ATIVOTITLEFILTERDATA", AV66Estado_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV98Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Caption", StringUtil.RTrim( Ddo_estado_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Tooltip", StringUtil.RTrim( Ddo_estado_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Cls", StringUtil.RTrim( Ddo_estado_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_estado_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_estado_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_estado_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_estado_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filtertype", StringUtil.RTrim( Ddo_estado_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_estado_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_estado_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Datalisttype", StringUtil.RTrim( Ddo_estado_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Datalistproc", StringUtil.RTrim( Ddo_estado_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_estado_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Sortasc", StringUtil.RTrim( Ddo_estado_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Sortdsc", StringUtil.RTrim( Ddo_estado_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Loadingdata", StringUtil.RTrim( Ddo_estado_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_estado_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_estado_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_estado_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Caption", StringUtil.RTrim( Ddo_estado_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_estado_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Cls", StringUtil.RTrim( Ddo_estado_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_estado_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_estado_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_estado_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_estado_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_estado_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_estado_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_estado_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_estado_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_estado_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_estado_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_estado_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_estado_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_estado_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE172( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT172( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwestado.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWEstado" ;
      }

      public override String GetPgmdesc( )
      {
         return " Estado" ;
      }

      protected void WB170( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_172( true) ;
         }
         else
         {
            wb_table1_2_172( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_172e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(87, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(88, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV73ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWEstado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_nome_Internalname, StringUtil.RTrim( AV63TFEstado_Nome), StringUtil.RTrim( context.localUtil.Format( AV63TFEstado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEstado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_nome_sel_Internalname, StringUtil.RTrim( AV64TFEstado_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV64TFEstado_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEstado.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV67TFEstado_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWEstado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ESTADO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_nometitlecontrolidtoreplace_Internalname, AV65ddo_Estado_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_estado_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWEstado.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ESTADO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_ativotitlecontrolidtoreplace_Internalname, AV68ddo_Estado_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_estado_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWEstado.htm");
         }
         wbLoad = true;
      }

      protected void START172( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Estado", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP170( ) ;
      }

      protected void WS172( )
      {
         START172( ) ;
         EVT172( ) ;
      }

      protected void EVT172( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11172 */
                              E11172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12172 */
                              E12172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13172 */
                              E13172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14172 */
                              E14172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15172 */
                              E15172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16172 */
                              E16172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17172 */
                              E17172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18172 */
                              E18172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19172 */
                              E19172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20172 */
                              E20172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21172 */
                              E21172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22172 */
                              E22172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23172 */
                              E23172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24172 */
                              E24172 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_76_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
                              SubsflControlProps_762( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV95Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV96Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV54Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV54Display)) ? AV97Display_GXI : context.convertURL( context.PathToRelativeUrl( AV54Display))));
                              A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                              A24Estado_Nome = StringUtil.Upper( cgiGet( edtEstado_Nome_Internalname));
                              A634Estado_Ativo = StringUtil.StrToBool( cgiGet( chkEstado_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25172 */
                                    E25172 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26172 */
                                    E26172 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27172 */
                                    E27172 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_uf1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_UF1"), AV55Estado_UF1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME1"), AV17Estado_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_uf2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_UF2"), AV56Estado_UF2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME2"), AV21Estado_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_uf3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_UF3"), AV57Estado_UF3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Estado_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME3"), AV25Estado_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME"), AV63TFEstado_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME_SEL"), AV64TFEstado_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFESTADO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV67TFEstado_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE172( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA172( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ESTADO_UF", "UF", 0);
            cmbavDynamicfiltersselector1.addItem("ESTADO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            dynavEstado_uf1.Name = "vESTADO_UF1";
            dynavEstado_uf1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ESTADO_UF", "UF", 0);
            cmbavDynamicfiltersselector2.addItem("ESTADO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            dynavEstado_uf2.Name = "vESTADO_UF2";
            dynavEstado_uf2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("ESTADO_UF", "UF", 0);
            cmbavDynamicfiltersselector3.addItem("ESTADO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            dynavEstado_uf3.Name = "vESTADO_UF3";
            dynavEstado_uf3.WebTags = "";
            GXCCtl = "ESTADO_ATIVO_" + sGXsfl_76_idx;
            chkEstado_Ativo.Name = GXCCtl;
            chkEstado_Ativo.WebTags = "";
            chkEstado_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkEstado_Ativo_Internalname, "TitleCaption", chkEstado_Ativo.Caption);
            chkEstado_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvESTADO_UF1172( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF1_data172( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF1_html172( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF1_data172( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf1.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavEstado_uf1.ItemCount > 0 )
         {
            AV55Estado_UF1 = dynavEstado_uf1.getValidValue(AV55Estado_UF1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Estado_UF1", AV55Estado_UF1);
         }
      }

      protected void GXDLVvESTADO_UF1_data172( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00172 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00172_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00172_A24Estado_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvESTADO_UF2172( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF2_data172( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF2_html172( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF2_data172( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf2.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavEstado_uf2.ItemCount > 0 )
         {
            AV56Estado_UF2 = dynavEstado_uf2.getValidValue(AV56Estado_UF2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Estado_UF2", AV56Estado_UF2);
         }
      }

      protected void GXDLVvESTADO_UF2_data172( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00173 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00173_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00173_A24Estado_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvESTADO_UF3172( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF3_data172( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF3_html172( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF3_data172( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf3.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavEstado_uf3.ItemCount > 0 )
         {
            AV57Estado_UF3 = dynavEstado_uf3.getValidValue(AV57Estado_UF3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Estado_UF3", AV57Estado_UF3);
         }
      }

      protected void GXDLVvESTADO_UF3_data172( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00174 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00174_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00174_A24Estado_Nome[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_762( ) ;
         while ( nGXsfl_76_idx <= nRC_GXsfl_76 )
         {
            sendrow_762( ) ;
            nGXsfl_76_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_76_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_76_idx+1));
            sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
            SubsflControlProps_762( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV55Estado_UF1 ,
                                       String AV17Estado_Nome1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV56Estado_UF2 ,
                                       String AV21Estado_Nome2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV57Estado_UF3 ,
                                       String AV25Estado_Nome3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV63TFEstado_Nome ,
                                       String AV64TFEstado_Nome_Sel ,
                                       short AV67TFEstado_Ativo_Sel ,
                                       short AV73ManageFiltersExecutionStep ,
                                       String AV65ddo_Estado_NomeTitleControlIdToReplace ,
                                       String AV68ddo_Estado_AtivoTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV98Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       String A23Estado_UF )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF172( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_UF", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
         GxWebStd.gx_hidden_field( context, "ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A24Estado_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "ESTADO_NOME", StringUtil.RTrim( A24Estado_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_ATIVO", GetSecureSignedToken( "", A634Estado_Ativo));
         GxWebStd.gx_hidden_field( context, "ESTADO_ATIVO", StringUtil.BoolToStr( A634Estado_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( dynavEstado_uf1.ItemCount > 0 )
         {
            AV55Estado_UF1 = dynavEstado_uf1.getValidValue(AV55Estado_UF1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Estado_UF1", AV55Estado_UF1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( dynavEstado_uf2.ItemCount > 0 )
         {
            AV56Estado_UF2 = dynavEstado_uf2.getValidValue(AV56Estado_UF2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Estado_UF2", AV56Estado_UF2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( dynavEstado_uf3.ItemCount > 0 )
         {
            AV57Estado_UF3 = dynavEstado_uf3.getValidValue(AV57Estado_UF3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Estado_UF3", AV57Estado_UF3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF172( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV98Pgmname = "WWEstado";
         context.Gx_err = 0;
      }

      protected void RF172( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 76;
         /* Execute user event: E26172 */
         E26172 ();
         nGXsfl_76_idx = 1;
         sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
         SubsflControlProps_762( ) ;
         nGXsfl_76_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_762( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 AV81WWEstadoDS_1_Dynamicfiltersselector1 ,
                                                 AV82WWEstadoDS_2_Estado_uf1 ,
                                                 AV83WWEstadoDS_3_Estado_nome1 ,
                                                 AV84WWEstadoDS_4_Dynamicfiltersenabled2 ,
                                                 AV85WWEstadoDS_5_Dynamicfiltersselector2 ,
                                                 AV86WWEstadoDS_6_Estado_uf2 ,
                                                 AV87WWEstadoDS_7_Estado_nome2 ,
                                                 AV88WWEstadoDS_8_Dynamicfiltersenabled3 ,
                                                 AV89WWEstadoDS_9_Dynamicfiltersselector3 ,
                                                 AV90WWEstadoDS_10_Estado_uf3 ,
                                                 AV91WWEstadoDS_11_Estado_nome3 ,
                                                 AV93WWEstadoDS_13_Tfestado_nome_sel ,
                                                 AV92WWEstadoDS_12_Tfestado_nome ,
                                                 AV94WWEstadoDS_14_Tfestado_ativo_sel ,
                                                 A23Estado_UF ,
                                                 A24Estado_Nome ,
                                                 A634Estado_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV82WWEstadoDS_2_Estado_uf1 = StringUtil.PadR( StringUtil.RTrim( AV82WWEstadoDS_2_Estado_uf1), 2, "%");
            lV83WWEstadoDS_3_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV83WWEstadoDS_3_Estado_nome1), 50, "%");
            lV86WWEstadoDS_6_Estado_uf2 = StringUtil.PadR( StringUtil.RTrim( AV86WWEstadoDS_6_Estado_uf2), 2, "%");
            lV87WWEstadoDS_7_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV87WWEstadoDS_7_Estado_nome2), 50, "%");
            lV90WWEstadoDS_10_Estado_uf3 = StringUtil.PadR( StringUtil.RTrim( AV90WWEstadoDS_10_Estado_uf3), 2, "%");
            lV91WWEstadoDS_11_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV91WWEstadoDS_11_Estado_nome3), 50, "%");
            lV92WWEstadoDS_12_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV92WWEstadoDS_12_Tfestado_nome), 50, "%");
            /* Using cursor H00175 */
            pr_default.execute(3, new Object[] {lV82WWEstadoDS_2_Estado_uf1, lV83WWEstadoDS_3_Estado_nome1, lV86WWEstadoDS_6_Estado_uf2, lV87WWEstadoDS_7_Estado_nome2, lV90WWEstadoDS_10_Estado_uf3, lV91WWEstadoDS_11_Estado_nome3, lV92WWEstadoDS_12_Tfestado_nome, AV93WWEstadoDS_13_Tfestado_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_76_idx = 1;
            while ( ( (pr_default.getStatus(3) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A634Estado_Ativo = H00175_A634Estado_Ativo[0];
               A24Estado_Nome = H00175_A24Estado_Nome[0];
               A23Estado_UF = H00175_A23Estado_UF[0];
               /* Execute user event: E27172 */
               E27172 ();
               pr_default.readNext(3);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(3) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(3);
            wbEnd = 76;
            WB170( ) ;
         }
         nGXsfl_76_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWEstadoDS_2_Estado_uf1 = AV55Estado_UF1;
         AV83WWEstadoDS_3_Estado_nome1 = AV17Estado_Nome1;
         AV84WWEstadoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV86WWEstadoDS_6_Estado_uf2 = AV56Estado_UF2;
         AV87WWEstadoDS_7_Estado_nome2 = AV21Estado_Nome2;
         AV88WWEstadoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV90WWEstadoDS_10_Estado_uf3 = AV57Estado_UF3;
         AV91WWEstadoDS_11_Estado_nome3 = AV25Estado_Nome3;
         AV92WWEstadoDS_12_Tfestado_nome = AV63TFEstado_Nome;
         AV93WWEstadoDS_13_Tfestado_nome_sel = AV64TFEstado_Nome_Sel;
         AV94WWEstadoDS_14_Tfestado_ativo_sel = AV67TFEstado_Ativo_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV81WWEstadoDS_1_Dynamicfiltersselector1 ,
                                              AV82WWEstadoDS_2_Estado_uf1 ,
                                              AV83WWEstadoDS_3_Estado_nome1 ,
                                              AV84WWEstadoDS_4_Dynamicfiltersenabled2 ,
                                              AV85WWEstadoDS_5_Dynamicfiltersselector2 ,
                                              AV86WWEstadoDS_6_Estado_uf2 ,
                                              AV87WWEstadoDS_7_Estado_nome2 ,
                                              AV88WWEstadoDS_8_Dynamicfiltersenabled3 ,
                                              AV89WWEstadoDS_9_Dynamicfiltersselector3 ,
                                              AV90WWEstadoDS_10_Estado_uf3 ,
                                              AV91WWEstadoDS_11_Estado_nome3 ,
                                              AV93WWEstadoDS_13_Tfestado_nome_sel ,
                                              AV92WWEstadoDS_12_Tfestado_nome ,
                                              AV94WWEstadoDS_14_Tfestado_ativo_sel ,
                                              A23Estado_UF ,
                                              A24Estado_Nome ,
                                              A634Estado_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV82WWEstadoDS_2_Estado_uf1 = StringUtil.PadR( StringUtil.RTrim( AV82WWEstadoDS_2_Estado_uf1), 2, "%");
         lV83WWEstadoDS_3_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV83WWEstadoDS_3_Estado_nome1), 50, "%");
         lV86WWEstadoDS_6_Estado_uf2 = StringUtil.PadR( StringUtil.RTrim( AV86WWEstadoDS_6_Estado_uf2), 2, "%");
         lV87WWEstadoDS_7_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV87WWEstadoDS_7_Estado_nome2), 50, "%");
         lV90WWEstadoDS_10_Estado_uf3 = StringUtil.PadR( StringUtil.RTrim( AV90WWEstadoDS_10_Estado_uf3), 2, "%");
         lV91WWEstadoDS_11_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV91WWEstadoDS_11_Estado_nome3), 50, "%");
         lV92WWEstadoDS_12_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV92WWEstadoDS_12_Tfestado_nome), 50, "%");
         /* Using cursor H00176 */
         pr_default.execute(4, new Object[] {lV82WWEstadoDS_2_Estado_uf1, lV83WWEstadoDS_3_Estado_nome1, lV86WWEstadoDS_6_Estado_uf2, lV87WWEstadoDS_7_Estado_nome2, lV90WWEstadoDS_10_Estado_uf3, lV91WWEstadoDS_11_Estado_nome3, lV92WWEstadoDS_12_Tfestado_nome, AV93WWEstadoDS_13_Tfestado_nome_sel});
         GRID_nRecordCount = H00176_AGRID_nRecordCount[0];
         pr_default.close(4);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWEstadoDS_2_Estado_uf1 = AV55Estado_UF1;
         AV83WWEstadoDS_3_Estado_nome1 = AV17Estado_Nome1;
         AV84WWEstadoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV86WWEstadoDS_6_Estado_uf2 = AV56Estado_UF2;
         AV87WWEstadoDS_7_Estado_nome2 = AV21Estado_Nome2;
         AV88WWEstadoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV90WWEstadoDS_10_Estado_uf3 = AV57Estado_UF3;
         AV91WWEstadoDS_11_Estado_nome3 = AV25Estado_Nome3;
         AV92WWEstadoDS_12_Tfestado_nome = AV63TFEstado_Nome;
         AV93WWEstadoDS_13_Tfestado_nome_sel = AV64TFEstado_Nome_Sel;
         AV94WWEstadoDS_14_Tfestado_ativo_sel = AV67TFEstado_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWEstadoDS_2_Estado_uf1 = AV55Estado_UF1;
         AV83WWEstadoDS_3_Estado_nome1 = AV17Estado_Nome1;
         AV84WWEstadoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV86WWEstadoDS_6_Estado_uf2 = AV56Estado_UF2;
         AV87WWEstadoDS_7_Estado_nome2 = AV21Estado_Nome2;
         AV88WWEstadoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV90WWEstadoDS_10_Estado_uf3 = AV57Estado_UF3;
         AV91WWEstadoDS_11_Estado_nome3 = AV25Estado_Nome3;
         AV92WWEstadoDS_12_Tfestado_nome = AV63TFEstado_Nome;
         AV93WWEstadoDS_13_Tfestado_nome_sel = AV64TFEstado_Nome_Sel;
         AV94WWEstadoDS_14_Tfestado_ativo_sel = AV67TFEstado_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWEstadoDS_2_Estado_uf1 = AV55Estado_UF1;
         AV83WWEstadoDS_3_Estado_nome1 = AV17Estado_Nome1;
         AV84WWEstadoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV86WWEstadoDS_6_Estado_uf2 = AV56Estado_UF2;
         AV87WWEstadoDS_7_Estado_nome2 = AV21Estado_Nome2;
         AV88WWEstadoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV90WWEstadoDS_10_Estado_uf3 = AV57Estado_UF3;
         AV91WWEstadoDS_11_Estado_nome3 = AV25Estado_Nome3;
         AV92WWEstadoDS_12_Tfestado_nome = AV63TFEstado_Nome;
         AV93WWEstadoDS_13_Tfestado_nome_sel = AV64TFEstado_Nome_Sel;
         AV94WWEstadoDS_14_Tfestado_ativo_sel = AV67TFEstado_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWEstadoDS_2_Estado_uf1 = AV55Estado_UF1;
         AV83WWEstadoDS_3_Estado_nome1 = AV17Estado_Nome1;
         AV84WWEstadoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV86WWEstadoDS_6_Estado_uf2 = AV56Estado_UF2;
         AV87WWEstadoDS_7_Estado_nome2 = AV21Estado_Nome2;
         AV88WWEstadoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV90WWEstadoDS_10_Estado_uf3 = AV57Estado_UF3;
         AV91WWEstadoDS_11_Estado_nome3 = AV25Estado_Nome3;
         AV92WWEstadoDS_12_Tfestado_nome = AV63TFEstado_Nome;
         AV93WWEstadoDS_13_Tfestado_nome_sel = AV64TFEstado_Nome_Sel;
         AV94WWEstadoDS_14_Tfestado_ativo_sel = AV67TFEstado_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWEstadoDS_2_Estado_uf1 = AV55Estado_UF1;
         AV83WWEstadoDS_3_Estado_nome1 = AV17Estado_Nome1;
         AV84WWEstadoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV86WWEstadoDS_6_Estado_uf2 = AV56Estado_UF2;
         AV87WWEstadoDS_7_Estado_nome2 = AV21Estado_Nome2;
         AV88WWEstadoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV90WWEstadoDS_10_Estado_uf3 = AV57Estado_UF3;
         AV91WWEstadoDS_11_Estado_nome3 = AV25Estado_Nome3;
         AV92WWEstadoDS_12_Tfestado_nome = AV63TFEstado_Nome;
         AV93WWEstadoDS_13_Tfestado_nome_sel = AV64TFEstado_Nome_Sel;
         AV94WWEstadoDS_14_Tfestado_ativo_sel = AV67TFEstado_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         }
         return (int)(0) ;
      }

      protected void STRUP170( )
      {
         /* Before Start, stand alone formulas. */
         AV98Pgmname = "WWEstado";
         context.Gx_err = 0;
         GXVvESTADO_UF1_html172( ) ;
         GXVvESTADO_UF2_html172( ) ;
         GXVvESTADO_UF3_html172( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25172 */
         E25172 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV77ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV69DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vESTADO_NOMETITLEFILTERDATA"), AV62Estado_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vESTADO_ATIVOTITLEFILTERDATA"), AV66Estado_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            dynavEstado_uf1.Name = dynavEstado_uf1_Internalname;
            dynavEstado_uf1.CurrentValue = cgiGet( dynavEstado_uf1_Internalname);
            AV55Estado_UF1 = cgiGet( dynavEstado_uf1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Estado_UF1", AV55Estado_UF1);
            AV17Estado_Nome1 = StringUtil.Upper( cgiGet( edtavEstado_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Estado_Nome1", AV17Estado_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            dynavEstado_uf2.Name = dynavEstado_uf2_Internalname;
            dynavEstado_uf2.CurrentValue = cgiGet( dynavEstado_uf2_Internalname);
            AV56Estado_UF2 = cgiGet( dynavEstado_uf2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Estado_UF2", AV56Estado_UF2);
            AV21Estado_Nome2 = StringUtil.Upper( cgiGet( edtavEstado_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Estado_Nome2", AV21Estado_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            dynavEstado_uf3.Name = dynavEstado_uf3_Internalname;
            dynavEstado_uf3.CurrentValue = cgiGet( dynavEstado_uf3_Internalname);
            AV57Estado_UF3 = cgiGet( dynavEstado_uf3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Estado_UF3", AV57Estado_UF3);
            AV25Estado_Nome3 = StringUtil.Upper( cgiGet( edtavEstado_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Estado_Nome3", AV25Estado_Nome3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV73ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV73ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV73ManageFiltersExecutionStep), 1, 0));
            }
            AV63TFEstado_Nome = StringUtil.Upper( cgiGet( edtavTfestado_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFEstado_Nome", AV63TFEstado_Nome);
            AV64TFEstado_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfestado_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFEstado_Nome_Sel", AV64TFEstado_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfestado_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfestado_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFESTADO_ATIVO_SEL");
               GX_FocusControl = edtavTfestado_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFEstado_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFEstado_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0));
            }
            else
            {
               AV67TFEstado_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfestado_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFEstado_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0));
            }
            AV65ddo_Estado_NomeTitleControlIdToReplace = cgiGet( edtavDdo_estado_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Estado_NomeTitleControlIdToReplace", AV65ddo_Estado_NomeTitleControlIdToReplace);
            AV68ddo_Estado_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_estado_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Estado_AtivoTitleControlIdToReplace", AV68ddo_Estado_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_76 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_76"), ",", "."));
            AV71GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV72GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_estado_nome_Caption = cgiGet( "DDO_ESTADO_NOME_Caption");
            Ddo_estado_nome_Tooltip = cgiGet( "DDO_ESTADO_NOME_Tooltip");
            Ddo_estado_nome_Cls = cgiGet( "DDO_ESTADO_NOME_Cls");
            Ddo_estado_nome_Filteredtext_set = cgiGet( "DDO_ESTADO_NOME_Filteredtext_set");
            Ddo_estado_nome_Selectedvalue_set = cgiGet( "DDO_ESTADO_NOME_Selectedvalue_set");
            Ddo_estado_nome_Dropdownoptionstype = cgiGet( "DDO_ESTADO_NOME_Dropdownoptionstype");
            Ddo_estado_nome_Titlecontrolidtoreplace = cgiGet( "DDO_ESTADO_NOME_Titlecontrolidtoreplace");
            Ddo_estado_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includesortasc"));
            Ddo_estado_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includesortdsc"));
            Ddo_estado_nome_Sortedstatus = cgiGet( "DDO_ESTADO_NOME_Sortedstatus");
            Ddo_estado_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includefilter"));
            Ddo_estado_nome_Filtertype = cgiGet( "DDO_ESTADO_NOME_Filtertype");
            Ddo_estado_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Filterisrange"));
            Ddo_estado_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_NOME_Includedatalist"));
            Ddo_estado_nome_Datalisttype = cgiGet( "DDO_ESTADO_NOME_Datalisttype");
            Ddo_estado_nome_Datalistproc = cgiGet( "DDO_ESTADO_NOME_Datalistproc");
            Ddo_estado_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ESTADO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_estado_nome_Sortasc = cgiGet( "DDO_ESTADO_NOME_Sortasc");
            Ddo_estado_nome_Sortdsc = cgiGet( "DDO_ESTADO_NOME_Sortdsc");
            Ddo_estado_nome_Loadingdata = cgiGet( "DDO_ESTADO_NOME_Loadingdata");
            Ddo_estado_nome_Cleanfilter = cgiGet( "DDO_ESTADO_NOME_Cleanfilter");
            Ddo_estado_nome_Noresultsfound = cgiGet( "DDO_ESTADO_NOME_Noresultsfound");
            Ddo_estado_nome_Searchbuttontext = cgiGet( "DDO_ESTADO_NOME_Searchbuttontext");
            Ddo_estado_ativo_Caption = cgiGet( "DDO_ESTADO_ATIVO_Caption");
            Ddo_estado_ativo_Tooltip = cgiGet( "DDO_ESTADO_ATIVO_Tooltip");
            Ddo_estado_ativo_Cls = cgiGet( "DDO_ESTADO_ATIVO_Cls");
            Ddo_estado_ativo_Selectedvalue_set = cgiGet( "DDO_ESTADO_ATIVO_Selectedvalue_set");
            Ddo_estado_ativo_Dropdownoptionstype = cgiGet( "DDO_ESTADO_ATIVO_Dropdownoptionstype");
            Ddo_estado_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_ESTADO_ATIVO_Titlecontrolidtoreplace");
            Ddo_estado_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_ATIVO_Includesortasc"));
            Ddo_estado_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_ATIVO_Includesortdsc"));
            Ddo_estado_ativo_Sortedstatus = cgiGet( "DDO_ESTADO_ATIVO_Sortedstatus");
            Ddo_estado_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_ATIVO_Includefilter"));
            Ddo_estado_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_ATIVO_Includedatalist"));
            Ddo_estado_ativo_Datalisttype = cgiGet( "DDO_ESTADO_ATIVO_Datalisttype");
            Ddo_estado_ativo_Datalistfixedvalues = cgiGet( "DDO_ESTADO_ATIVO_Datalistfixedvalues");
            Ddo_estado_ativo_Sortasc = cgiGet( "DDO_ESTADO_ATIVO_Sortasc");
            Ddo_estado_ativo_Sortdsc = cgiGet( "DDO_ESTADO_ATIVO_Sortdsc");
            Ddo_estado_ativo_Cleanfilter = cgiGet( "DDO_ESTADO_ATIVO_Cleanfilter");
            Ddo_estado_ativo_Searchbuttontext = cgiGet( "DDO_ESTADO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_estado_nome_Activeeventkey = cgiGet( "DDO_ESTADO_NOME_Activeeventkey");
            Ddo_estado_nome_Filteredtext_get = cgiGet( "DDO_ESTADO_NOME_Filteredtext_get");
            Ddo_estado_nome_Selectedvalue_get = cgiGet( "DDO_ESTADO_NOME_Selectedvalue_get");
            Ddo_estado_ativo_Activeeventkey = cgiGet( "DDO_ESTADO_ATIVO_Activeeventkey");
            Ddo_estado_ativo_Selectedvalue_get = cgiGet( "DDO_ESTADO_ATIVO_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_UF1"), AV55Estado_UF1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME1"), AV17Estado_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_UF2"), AV56Estado_UF2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME2"), AV21Estado_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_UF3"), AV57Estado_UF3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vESTADO_NOME3"), AV25Estado_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME"), AV63TFEstado_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_NOME_SEL"), AV64TFEstado_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFESTADO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV67TFEstado_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25172 */
         E25172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25172( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "ESTADO_UF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "ESTADO_UF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "ESTADO_UF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfestado_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_nome_Visible), 5, 0)));
         edtavTfestado_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_nome_sel_Visible), 5, 0)));
         edtavTfestado_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_ativo_sel_Visible), 5, 0)));
         Ddo_estado_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "TitleControlIdToReplace", Ddo_estado_nome_Titlecontrolidtoreplace);
         AV65ddo_Estado_NomeTitleControlIdToReplace = Ddo_estado_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Estado_NomeTitleControlIdToReplace", AV65ddo_Estado_NomeTitleControlIdToReplace);
         edtavDdo_estado_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_estado_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_estado_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_ativo_Internalname, "TitleControlIdToReplace", Ddo_estado_ativo_Titlecontrolidtoreplace);
         AV68ddo_Estado_AtivoTitleControlIdToReplace = Ddo_estado_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Estado_AtivoTitleControlIdToReplace", AV68ddo_Estado_AtivoTitleControlIdToReplace);
         edtavDdo_estado_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_estado_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Estado";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "UF", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV69DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV69DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26172( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV62Estado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Estado_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV73ManageFiltersExecutionStep == 1 )
         {
            AV73ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV73ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV73ManageFiltersExecutionStep == 2 )
         {
            AV73ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV73ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtEstado_Nome_Titleformat = 2;
         edtEstado_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV65ddo_Estado_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstado_Nome_Internalname, "Title", edtEstado_Nome_Title);
         chkEstado_Ativo_Titleformat = 2;
         chkEstado_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV68ddo_Estado_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkEstado_Ativo_Internalname, "Title", chkEstado_Ativo.Title.Text);
         AV71GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71GridCurrentPage), 10, 0)));
         AV72GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72GridPageCount), 10, 0)));
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV82WWEstadoDS_2_Estado_uf1 = AV55Estado_UF1;
         AV83WWEstadoDS_3_Estado_nome1 = AV17Estado_Nome1;
         AV84WWEstadoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV86WWEstadoDS_6_Estado_uf2 = AV56Estado_UF2;
         AV87WWEstadoDS_7_Estado_nome2 = AV21Estado_Nome2;
         AV88WWEstadoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV90WWEstadoDS_10_Estado_uf3 = AV57Estado_UF3;
         AV91WWEstadoDS_11_Estado_nome3 = AV25Estado_Nome3;
         AV92WWEstadoDS_12_Tfestado_nome = AV63TFEstado_Nome;
         AV93WWEstadoDS_13_Tfestado_nome_sel = AV64TFEstado_Nome_Sel;
         AV94WWEstadoDS_14_Tfestado_ativo_sel = AV67TFEstado_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62Estado_NomeTitleFilterData", AV62Estado_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66Estado_AtivoTitleFilterData", AV66Estado_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV77ManageFiltersData", AV77ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12172( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV70PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV70PageToGo) ;
         }
      }

      protected void E13172( )
      {
         /* Ddo_estado_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFEstado_Nome = Ddo_estado_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFEstado_Nome", AV63TFEstado_Nome);
            AV64TFEstado_Nome_Sel = Ddo_estado_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFEstado_Nome_Sel", AV64TFEstado_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14172( )
      {
         /* Ddo_estado_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_ativo_Internalname, "SortedStatus", Ddo_estado_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_ativo_Internalname, "SortedStatus", Ddo_estado_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFEstado_Ativo_Sel = (short)(NumberUtil.Val( Ddo_estado_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFEstado_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27172( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("estado.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(StringUtil.RTrim(A23Estado_UF));
            AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV95Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV28Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV95Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("estado.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(StringUtil.RTrim(A23Estado_UF));
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV96Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV96Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Municipios";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewestado.aspx") + "?" + UrlEncode(StringUtil.RTrim(A23Estado_UF)) + "," + UrlEncode(StringUtil.RTrim(""));
            AV54Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV54Display);
            AV97Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV54Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV54Display);
            AV97Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtEstado_Nome_Link = formatLink("viewestado.aspx") + "?" + UrlEncode(StringUtil.RTrim(A23Estado_UF)) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 76;
         }
         sendrow_762( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_76_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(76, GridRow);
         }
      }

      protected void E15172( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20172( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16172( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV56Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV57Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV55Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
      }

      protected void E21172( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22172( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17172( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV56Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV57Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV55Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
      }

      protected void E23172( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18172( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV55Estado_UF1, AV17Estado_Nome1, AV19DynamicFiltersSelector2, AV56Estado_UF2, AV21Estado_Nome2, AV23DynamicFiltersSelector3, AV57Estado_UF3, AV25Estado_Nome3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV63TFEstado_Nome, AV64TFEstado_Nome_Sel, AV67TFEstado_Ativo_Sel, AV73ManageFiltersExecutionStep, AV65ddo_Estado_NomeTitleControlIdToReplace, AV68ddo_Estado_AtivoTitleControlIdToReplace, AV6WWPContext, AV98Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A23Estado_UF) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV56Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV57Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV55Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
      }

      protected void E24172( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11172( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWEstadoFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV73ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV73ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWEstadoFilters")), new Object[] {});
            AV73ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV73ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV74ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWEstadoFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV74ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV98Pgmname+"GridState",  AV74ManageFiltersXml) ;
               AV10GridState.FromXml(AV74ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S252 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV55Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV56Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV57Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
      }

      protected void E19172( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("estado.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void S202( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_estado_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
         Ddo_estado_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_ativo_Internalname, "SortedStatus", Ddo_estado_ativo_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_estado_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SortedStatus", Ddo_estado_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_estado_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_ativo_Internalname, "SortedStatus", Ddo_estado_ativo_Sortedstatus);
         }
      }

      protected void S182( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         dynavEstado_uf1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf1.Visible), 5, 0)));
         edtavEstado_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_UF") == 0 )
         {
            dynavEstado_uf1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_NOME") == 0 )
         {
            edtavEstado_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome1_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         dynavEstado_uf2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf2.Visible), 5, 0)));
         edtavEstado_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ESTADO_UF") == 0 )
         {
            dynavEstado_uf2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ESTADO_NOME") == 0 )
         {
            edtavEstado_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome2_Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         dynavEstado_uf3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf3.Visible), 5, 0)));
         edtavEstado_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ESTADO_UF") == 0 )
         {
            dynavEstado_uf3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ESTADO_NOME") == 0 )
         {
            edtavEstado_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEstado_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEstado_nome3_Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "ESTADO_UF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV56Estado_UF2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Estado_UF2", AV56Estado_UF2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "ESTADO_UF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV57Estado_UF3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Estado_UF3", AV57Estado_UF3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV77ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV78ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV78ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV78ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV78ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV78ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV77ManageFiltersData.Add(AV78ManageFiltersDataItem, 0);
         AV78ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV78ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV78ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV78ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV78ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV77ManageFiltersData.Add(AV78ManageFiltersDataItem, 0);
         AV78ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV78ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV77ManageFiltersData.Add(AV78ManageFiltersDataItem, 0);
         AV75ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWEstadoFilters"), "");
         AV99GXV1 = 1;
         while ( AV99GXV1 <= AV75ManageFiltersItems.Count )
         {
            AV76ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV75ManageFiltersItems.Item(AV99GXV1));
            AV78ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV78ManageFiltersDataItem.gxTpr_Title = AV76ManageFiltersItem.gxTpr_Title;
            AV78ManageFiltersDataItem.gxTpr_Eventkey = AV76ManageFiltersItem.gxTpr_Title;
            AV78ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV78ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV77ManageFiltersData.Add(AV78ManageFiltersDataItem, 0);
            if ( AV77ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV99GXV1 = (int)(AV99GXV1+1);
         }
         if ( AV77ManageFiltersData.Count > 3 )
         {
            AV78ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV78ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV77ManageFiltersData.Add(AV78ManageFiltersDataItem, 0);
            AV78ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV78ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV78ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV78ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV78ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV78ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV77ManageFiltersData.Add(AV78ManageFiltersDataItem, 0);
         }
      }

      protected void S242( )
      {
         /* 'CLEANFILTERS' Routine */
         AV63TFEstado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFEstado_Nome", AV63TFEstado_Nome);
         Ddo_estado_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "FilteredText_set", Ddo_estado_nome_Filteredtext_set);
         AV64TFEstado_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFEstado_Nome_Sel", AV64TFEstado_Nome_Sel);
         Ddo_estado_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SelectedValue_set", Ddo_estado_nome_Selectedvalue_set);
         AV67TFEstado_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFEstado_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0));
         Ddo_estado_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_ativo_Internalname, "SelectedValue_set", Ddo_estado_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "ESTADO_UF";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV55Estado_UF1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Estado_UF1", AV55Estado_UF1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get(AV98Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV98Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV32Session.Get(AV98Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV100GXV2 = 1;
         while ( AV100GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV100GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME") == 0 )
            {
               AV63TFEstado_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFEstado_Nome", AV63TFEstado_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFEstado_Nome)) )
               {
                  Ddo_estado_nome_Filteredtext_set = AV63TFEstado_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "FilteredText_set", Ddo_estado_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME_SEL") == 0 )
            {
               AV64TFEstado_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFEstado_Nome_Sel", AV64TFEstado_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFEstado_Nome_Sel)) )
               {
                  Ddo_estado_nome_Selectedvalue_set = AV64TFEstado_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_nome_Internalname, "SelectedValue_set", Ddo_estado_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_ATIVO_SEL") == 0 )
            {
               AV67TFEstado_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFEstado_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0));
               if ( ! (0==AV67TFEstado_Ativo_Sel) )
               {
                  Ddo_estado_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_ativo_Internalname, "SelectedValue_set", Ddo_estado_ativo_Selectedvalue_set);
               }
            }
            AV100GXV2 = (int)(AV100GXV2+1);
         }
      }

      protected void S232( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_UF") == 0 )
            {
               AV55Estado_UF1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55Estado_UF1", AV55Estado_UF1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_NOME") == 0 )
            {
               AV17Estado_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Estado_Nome1", AV17Estado_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ESTADO_UF") == 0 )
               {
                  AV56Estado_UF2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Estado_UF2", AV56Estado_UF2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ESTADO_NOME") == 0 )
               {
                  AV21Estado_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Estado_Nome2", AV21Estado_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ESTADO_UF") == 0 )
                  {
                     AV57Estado_UF3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57Estado_UF3", AV57Estado_UF3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ESTADO_NOME") == 0 )
                  {
                     AV25Estado_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Estado_Nome3", AV25Estado_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S192( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV32Session.Get(AV98Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFEstado_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFEstado_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFEstado_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFEstado_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV67TFEstado_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV67TFEstado_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV98Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_UF") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Estado_UF1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV55Estado_UF1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ESTADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Estado_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Estado_Nome1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ESTADO_UF") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56Estado_UF2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56Estado_UF2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ESTADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Estado_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Estado_Nome2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ESTADO_UF") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV57Estado_UF3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV57Estado_UF3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ESTADO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Estado_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Estado_Nome3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV98Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Estado";
         AV32Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_172( true) ;
         }
         else
         {
            wb_table2_8_172( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_172e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_70_172( true) ;
         }
         else
         {
            wb_table3_70_172( false) ;
         }
         return  ;
      }

      protected void wb_table3_70_172e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_172e( true) ;
         }
         else
         {
            wb_table1_2_172e( false) ;
         }
      }

      protected void wb_table3_70_172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_73_172( true) ;
         }
         else
         {
            wb_table4_73_172( false) ;
         }
         return  ;
      }

      protected void wb_table4_73_172e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_70_172e( true) ;
         }
         else
         {
            wb_table3_70_172e( false) ;
         }
      }

      protected void wb_table4_73_172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"76\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "UF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkEstado_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkEstado_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkEstado_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV54Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A24Estado_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtEstado_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A634Estado_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkEstado_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkEstado_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 76 )
         {
            wbEnd = 0;
            nRC_GXsfl_76 = (short)(nGXsfl_76_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_73_172e( true) ;
         }
         else
         {
            wb_table4_73_172e( false) ;
         }
      }

      protected void wb_table2_8_172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_11_172( true) ;
         }
         else
         {
            wb_table5_11_172( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_172e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_172( true) ;
         }
         else
         {
            wb_table6_23_172( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_172e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_172e( true) ;
         }
         else
         {
            wb_table2_8_172e( false) ;
         }
      }

      protected void wb_table6_23_172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_172( true) ;
         }
         else
         {
            wb_table7_28_172( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_172e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_172e( true) ;
         }
         else
         {
            wb_table6_23_172e( false) ;
         }
      }

      protected void wb_table7_28_172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWEstado.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf1, dynavEstado_uf1_Internalname, StringUtil.RTrim( AV55Estado_UF1), 1, dynavEstado_uf1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavEstado_uf1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWEstado.htm");
            dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV55Estado_UF1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf1_Internalname, "Values", (String)(dynavEstado_uf1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEstado_nome1_Internalname, StringUtil.RTrim( AV17Estado_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Estado_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEstado_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEstado_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEstado.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWEstado.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf2, dynavEstado_uf2_Internalname, StringUtil.RTrim( AV56Estado_UF2), 1, dynavEstado_uf2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavEstado_uf2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWEstado.htm");
            dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV56Estado_UF2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf2_Internalname, "Values", (String)(dynavEstado_uf2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEstado_nome2_Internalname, StringUtil.RTrim( AV21Estado_Nome2), StringUtil.RTrim( context.localUtil.Format( AV21Estado_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEstado_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEstado_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEstado.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WWEstado.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf3, dynavEstado_uf3_Internalname, StringUtil.RTrim( AV57Estado_UF3), 1, dynavEstado_uf3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", dynavEstado_uf3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "", true, "HLP_WWEstado.htm");
            dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV57Estado_UF3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavEstado_uf3_Internalname, "Values", (String)(dynavEstado_uf3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEstado_nome3_Internalname, StringUtil.RTrim( AV25Estado_Nome3), StringUtil.RTrim( context.localUtil.Format( AV25Estado_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEstado_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEstado_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_172e( true) ;
         }
         else
         {
            wb_table7_28_172e( false) ;
         }
      }

      protected void wb_table5_11_172( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblEstadotitle_Internalname, "Estados", "", "", lblEstadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWEstado.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWEstado.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_172e( true) ;
         }
         else
         {
            wb_table5_11_172e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA172( ) ;
         WS172( ) ;
         WE172( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117311297");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwestado.js", "?20203117311297");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_762( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_76_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_76_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_76_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_76_idx;
         edtEstado_Nome_Internalname = "ESTADO_NOME_"+sGXsfl_76_idx;
         chkEstado_Ativo_Internalname = "ESTADO_ATIVO_"+sGXsfl_76_idx;
      }

      protected void SubsflControlProps_fel_762( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_76_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_76_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_76_fel_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_76_fel_idx;
         edtEstado_Nome_Internalname = "ESTADO_NOME_"+sGXsfl_76_fel_idx;
         chkEstado_Ativo_Internalname = "ESTADO_ATIVO_"+sGXsfl_76_fel_idx;
      }

      protected void sendrow_762( )
      {
         SubsflControlProps_762( ) ;
         WB170( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_76_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_76_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_76_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV95Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV95Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV96Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV96Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV54Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV54Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV97Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV54Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV54Display)) ? AV97Display_GXI : context.PathToRelativeUrl( AV54Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV54Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)76,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_Nome_Internalname,StringUtil.RTrim( A24Estado_Nome),StringUtil.RTrim( context.localUtil.Format( A24Estado_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtEstado_Nome_Link,(String)"",(String)"",(String)"",(String)edtEstado_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)76,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkEstado_Ativo_Internalname,StringUtil.BoolToStr( A634Estado_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_UF"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_NOME"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, StringUtil.RTrim( context.localUtil.Format( A24Estado_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_ESTADO_ATIVO"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, A634Estado_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_76_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_76_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_76_idx+1));
            sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
            SubsflControlProps_762( ) ;
         }
         /* End function sendrow_762 */
      }

      protected void init_default_properties( )
      {
         lblEstadotitle_Internalname = "ESTADOTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         dynavEstado_uf1_Internalname = "vESTADO_UF1";
         edtavEstado_nome1_Internalname = "vESTADO_NOME1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         dynavEstado_uf2_Internalname = "vESTADO_UF2";
         edtavEstado_nome2_Internalname = "vESTADO_NOME2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         dynavEstado_uf3_Internalname = "vESTADO_UF3";
         edtavEstado_nome3_Internalname = "vESTADO_NOME3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtEstado_UF_Internalname = "ESTADO_UF";
         edtEstado_Nome_Internalname = "ESTADO_NOME";
         chkEstado_Ativo_Internalname = "ESTADO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfestado_nome_Internalname = "vTFESTADO_NOME";
         edtavTfestado_nome_sel_Internalname = "vTFESTADO_NOME_SEL";
         edtavTfestado_ativo_sel_Internalname = "vTFESTADO_ATIVO_SEL";
         Ddo_estado_nome_Internalname = "DDO_ESTADO_NOME";
         edtavDdo_estado_nometitlecontrolidtoreplace_Internalname = "vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_estado_ativo_Internalname = "DDO_ESTADO_ATIVO";
         edtavDdo_estado_ativotitlecontrolidtoreplace_Internalname = "vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtEstado_Nome_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavEstado_nome3_Jsonclick = "";
         dynavEstado_uf3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavEstado_nome2_Jsonclick = "";
         dynavEstado_uf2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavEstado_nome1_Jsonclick = "";
         dynavEstado_uf1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtEstado_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Municipios";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkEstado_Ativo_Titleformat = 0;
         edtEstado_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavEstado_nome3_Visible = 1;
         dynavEstado_uf3.Visible = 1;
         edtavEstado_nome2_Visible = 1;
         dynavEstado_uf2.Visible = 1;
         edtavEstado_nome1_Visible = 1;
         dynavEstado_uf1.Visible = 1;
         chkEstado_Ativo.Title.Text = "Ativo?";
         edtEstado_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkEstado_Ativo.Caption = "";
         edtavDdo_estado_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_estado_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfestado_ativo_sel_Jsonclick = "";
         edtavTfestado_ativo_sel_Visible = 1;
         edtavTfestado_nome_sel_Jsonclick = "";
         edtavTfestado_nome_sel_Visible = 1;
         edtavTfestado_nome_Jsonclick = "";
         edtavTfestado_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_estado_ativo_Searchbuttontext = "Pesquisar";
         Ddo_estado_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_estado_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_estado_ativo_Datalisttype = "FixedValues";
         Ddo_estado_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_estado_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_ativo_Titlecontrolidtoreplace = "";
         Ddo_estado_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_ativo_Cls = "ColumnSettings";
         Ddo_estado_ativo_Tooltip = "Op��es";
         Ddo_estado_ativo_Caption = "";
         Ddo_estado_nome_Searchbuttontext = "Pesquisar";
         Ddo_estado_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_estado_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_nome_Loadingdata = "Carregando dados...";
         Ddo_estado_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_nome_Sortasc = "Ordenar de A � Z";
         Ddo_estado_nome_Datalistupdateminimumcharacters = 0;
         Ddo_estado_nome_Datalistproc = "GetWWEstadoFilterData";
         Ddo_estado_nome_Datalisttype = "Dynamic";
         Ddo_estado_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_estado_nome_Filtertype = "Character";
         Ddo_estado_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_estado_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_nome_Titlecontrolidtoreplace = "";
         Ddo_estado_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_nome_Cls = "ColumnSettings";
         Ddo_estado_nome_Tooltip = "Op��es";
         Ddo_estado_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Estado";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV62Estado_NomeTitleFilterData',fld:'vESTADO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV66Estado_AtivoTitleFilterData',fld:'vESTADO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtEstado_Nome_Titleformat',ctrl:'ESTADO_NOME',prop:'Titleformat'},{av:'edtEstado_Nome_Title',ctrl:'ESTADO_NOME',prop:'Title'},{av:'chkEstado_Ativo_Titleformat',ctrl:'ESTADO_ATIVO',prop:'Titleformat'},{av:'chkEstado_Ativo.Title.Text',ctrl:'ESTADO_ATIVO',prop:'Title'},{av:'AV71GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV72GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV77ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_ESTADO_NOME.ONOPTIONCLICKED","{handler:'E13172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'Ddo_estado_nome_Activeeventkey',ctrl:'DDO_ESTADO_NOME',prop:'ActiveEventKey'},{av:'Ddo_estado_nome_Filteredtext_get',ctrl:'DDO_ESTADO_NOME',prop:'FilteredText_get'},{av:'Ddo_estado_nome_Selectedvalue_get',ctrl:'DDO_ESTADO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_nome_Sortedstatus',ctrl:'DDO_ESTADO_NOME',prop:'SortedStatus'},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_estado_ativo_Sortedstatus',ctrl:'DDO_ESTADO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ESTADO_ATIVO.ONOPTIONCLICKED","{handler:'E14172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'Ddo_estado_ativo_Activeeventkey',ctrl:'DDO_ESTADO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_estado_ativo_Selectedvalue_get',ctrl:'DDO_ESTADO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_ativo_Sortedstatus',ctrl:'DDO_ESTADO_ATIVO',prop:'SortedStatus'},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_estado_nome_Sortedstatus',ctrl:'DDO_ESTADO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27172',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV54Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtEstado_Nome_Link',ctrl:'ESTADO_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20172',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'dynavEstado_uf2'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21172',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'dynavEstado_uf1'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22172',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'dynavEstado_uf2'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23172',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'dynavEstado_uf2'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'dynavEstado_uf2'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24172',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'dynavEstado_uf3'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11172',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV65ddo_Estado_NomeTitleControlIdToReplace',fld:'vDDO_ESTADO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Estado_AtivoTitleControlIdToReplace',fld:'vDDO_ESTADO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV98Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV73ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV63TFEstado_Nome',fld:'vTFESTADO_NOME',pic:'@!',nv:''},{av:'Ddo_estado_nome_Filteredtext_set',ctrl:'DDO_ESTADO_NOME',prop:'FilteredText_set'},{av:'AV64TFEstado_Nome_Sel',fld:'vTFESTADO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_estado_nome_Selectedvalue_set',ctrl:'DDO_ESTADO_NOME',prop:'SelectedValue_set'},{av:'AV67TFEstado_Ativo_Sel',fld:'vTFESTADO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_estado_ativo_Selectedvalue_set',ctrl:'DDO_ESTADO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV55Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'Ddo_estado_nome_Sortedstatus',ctrl:'DDO_ESTADO_NOME',prop:'SortedStatus'},{av:'Ddo_estado_ativo_Sortedstatus',ctrl:'DDO_ESTADO_ATIVO',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17Estado_Nome1',fld:'vESTADO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV56Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV21Estado_Nome2',fld:'vESTADO_NOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV57Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV25Estado_Nome3',fld:'vESTADO_NOME3',pic:'@!',nv:''},{av:'dynavEstado_uf1'},{av:'edtavEstado_nome1_Visible',ctrl:'vESTADO_NOME1',prop:'Visible'},{av:'dynavEstado_uf2'},{av:'edtavEstado_nome2_Visible',ctrl:'vESTADO_NOME2',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'edtavEstado_nome3_Visible',ctrl:'vESTADO_NOME3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E19172',iparms:[{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_estado_nome_Activeeventkey = "";
         Ddo_estado_nome_Filteredtext_get = "";
         Ddo_estado_nome_Selectedvalue_get = "";
         Ddo_estado_ativo_Activeeventkey = "";
         Ddo_estado_ativo_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV55Estado_UF1 = "";
         AV17Estado_Nome1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV56Estado_UF2 = "";
         AV21Estado_Nome2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV57Estado_UF3 = "";
         AV25Estado_Nome3 = "";
         AV63TFEstado_Nome = "";
         AV64TFEstado_Nome_Sel = "";
         AV65ddo_Estado_NomeTitleControlIdToReplace = "";
         AV68ddo_Estado_AtivoTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV98Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A23Estado_UF = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV77ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV62Estado_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Estado_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_estado_nome_Filteredtext_set = "";
         Ddo_estado_nome_Selectedvalue_set = "";
         Ddo_estado_nome_Sortedstatus = "";
         Ddo_estado_ativo_Selectedvalue_set = "";
         Ddo_estado_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV95Update_GXI = "";
         AV29Delete = "";
         AV96Delete_GXI = "";
         AV54Display = "";
         AV97Display_GXI = "";
         A24Estado_Nome = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00172_A23Estado_UF = new String[] {""} ;
         H00172_A24Estado_Nome = new String[] {""} ;
         H00173_A23Estado_UF = new String[] {""} ;
         H00173_A24Estado_Nome = new String[] {""} ;
         H00174_A23Estado_UF = new String[] {""} ;
         H00174_A24Estado_Nome = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV82WWEstadoDS_2_Estado_uf1 = "";
         lV83WWEstadoDS_3_Estado_nome1 = "";
         lV86WWEstadoDS_6_Estado_uf2 = "";
         lV87WWEstadoDS_7_Estado_nome2 = "";
         lV90WWEstadoDS_10_Estado_uf3 = "";
         lV91WWEstadoDS_11_Estado_nome3 = "";
         lV92WWEstadoDS_12_Tfestado_nome = "";
         AV81WWEstadoDS_1_Dynamicfiltersselector1 = "";
         AV82WWEstadoDS_2_Estado_uf1 = "";
         AV83WWEstadoDS_3_Estado_nome1 = "";
         AV85WWEstadoDS_5_Dynamicfiltersselector2 = "";
         AV86WWEstadoDS_6_Estado_uf2 = "";
         AV87WWEstadoDS_7_Estado_nome2 = "";
         AV89WWEstadoDS_9_Dynamicfiltersselector3 = "";
         AV90WWEstadoDS_10_Estado_uf3 = "";
         AV91WWEstadoDS_11_Estado_nome3 = "";
         AV93WWEstadoDS_13_Tfestado_nome_sel = "";
         AV92WWEstadoDS_12_Tfestado_nome = "";
         H00175_A634Estado_Ativo = new bool[] {false} ;
         H00175_A24Estado_Nome = new String[] {""} ;
         H00175_A23Estado_UF = new String[] {""} ;
         H00176_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV74ManageFiltersXml = "";
         GXt_char2 = "";
         imgInsert_Link = "";
         AV78ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV75ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV76ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV32Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblEstadotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwestado__default(),
            new Object[][] {
                new Object[] {
               H00172_A23Estado_UF, H00172_A24Estado_Nome
               }
               , new Object[] {
               H00173_A23Estado_UF, H00173_A24Estado_Nome
               }
               , new Object[] {
               H00174_A23Estado_UF, H00174_A24Estado_Nome
               }
               , new Object[] {
               H00175_A634Estado_Ativo, H00175_A24Estado_Nome, H00175_A23Estado_UF
               }
               , new Object[] {
               H00176_AGRID_nRecordCount
               }
            }
         );
         AV98Pgmname = "WWEstado";
         /* GeneXus formulas. */
         AV98Pgmname = "WWEstado";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_76 ;
      private short nGXsfl_76_idx=1 ;
      private short AV13OrderedBy ;
      private short AV67TFEstado_Ativo_Sel ;
      private short AV73ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_76_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV94WWEstadoDS_14_Tfestado_ativo_sel ;
      private short edtEstado_Nome_Titleformat ;
      private short chkEstado_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_estado_nome_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfestado_nome_Visible ;
      private int edtavTfestado_nome_sel_Visible ;
      private int edtavTfestado_ativo_sel_Visible ;
      private int edtavDdo_estado_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_estado_ativotitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV70PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavEstado_nome1_Visible ;
      private int edtavEstado_nome2_Visible ;
      private int edtavEstado_nome3_Visible ;
      private int AV99GXV1 ;
      private int AV100GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV71GridCurrentPage ;
      private long AV72GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_estado_nome_Activeeventkey ;
      private String Ddo_estado_nome_Filteredtext_get ;
      private String Ddo_estado_nome_Selectedvalue_get ;
      private String Ddo_estado_ativo_Activeeventkey ;
      private String Ddo_estado_ativo_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_76_idx="0001" ;
      private String AV55Estado_UF1 ;
      private String AV17Estado_Nome1 ;
      private String AV56Estado_UF2 ;
      private String AV21Estado_Nome2 ;
      private String AV57Estado_UF3 ;
      private String AV25Estado_Nome3 ;
      private String AV63TFEstado_Nome ;
      private String AV64TFEstado_Nome_Sel ;
      private String AV98Pgmname ;
      private String A23Estado_UF ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_estado_nome_Caption ;
      private String Ddo_estado_nome_Tooltip ;
      private String Ddo_estado_nome_Cls ;
      private String Ddo_estado_nome_Filteredtext_set ;
      private String Ddo_estado_nome_Selectedvalue_set ;
      private String Ddo_estado_nome_Dropdownoptionstype ;
      private String Ddo_estado_nome_Titlecontrolidtoreplace ;
      private String Ddo_estado_nome_Sortedstatus ;
      private String Ddo_estado_nome_Filtertype ;
      private String Ddo_estado_nome_Datalisttype ;
      private String Ddo_estado_nome_Datalistproc ;
      private String Ddo_estado_nome_Sortasc ;
      private String Ddo_estado_nome_Sortdsc ;
      private String Ddo_estado_nome_Loadingdata ;
      private String Ddo_estado_nome_Cleanfilter ;
      private String Ddo_estado_nome_Noresultsfound ;
      private String Ddo_estado_nome_Searchbuttontext ;
      private String Ddo_estado_ativo_Caption ;
      private String Ddo_estado_ativo_Tooltip ;
      private String Ddo_estado_ativo_Cls ;
      private String Ddo_estado_ativo_Selectedvalue_set ;
      private String Ddo_estado_ativo_Dropdownoptionstype ;
      private String Ddo_estado_ativo_Titlecontrolidtoreplace ;
      private String Ddo_estado_ativo_Sortedstatus ;
      private String Ddo_estado_ativo_Datalisttype ;
      private String Ddo_estado_ativo_Datalistfixedvalues ;
      private String Ddo_estado_ativo_Sortasc ;
      private String Ddo_estado_ativo_Sortdsc ;
      private String Ddo_estado_ativo_Cleanfilter ;
      private String Ddo_estado_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfestado_nome_Internalname ;
      private String edtavTfestado_nome_Jsonclick ;
      private String edtavTfestado_nome_sel_Internalname ;
      private String edtavTfestado_nome_sel_Jsonclick ;
      private String edtavTfestado_ativo_sel_Internalname ;
      private String edtavTfestado_ativo_sel_Jsonclick ;
      private String edtavDdo_estado_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_estado_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtEstado_UF_Internalname ;
      private String A24Estado_Nome ;
      private String edtEstado_Nome_Internalname ;
      private String chkEstado_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV82WWEstadoDS_2_Estado_uf1 ;
      private String lV83WWEstadoDS_3_Estado_nome1 ;
      private String lV86WWEstadoDS_6_Estado_uf2 ;
      private String lV87WWEstadoDS_7_Estado_nome2 ;
      private String lV90WWEstadoDS_10_Estado_uf3 ;
      private String lV91WWEstadoDS_11_Estado_nome3 ;
      private String lV92WWEstadoDS_12_Tfestado_nome ;
      private String AV82WWEstadoDS_2_Estado_uf1 ;
      private String AV83WWEstadoDS_3_Estado_nome1 ;
      private String AV86WWEstadoDS_6_Estado_uf2 ;
      private String AV87WWEstadoDS_7_Estado_nome2 ;
      private String AV90WWEstadoDS_10_Estado_uf3 ;
      private String AV91WWEstadoDS_11_Estado_nome3 ;
      private String AV93WWEstadoDS_13_Tfestado_nome_sel ;
      private String AV92WWEstadoDS_12_Tfestado_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String dynavEstado_uf1_Internalname ;
      private String edtavEstado_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String dynavEstado_uf2_Internalname ;
      private String edtavEstado_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String dynavEstado_uf3_Internalname ;
      private String edtavEstado_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_estado_nome_Internalname ;
      private String Ddo_estado_ativo_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtEstado_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtEstado_Nome_Link ;
      private String GXt_char2 ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String dynavEstado_uf1_Jsonclick ;
      private String edtavEstado_nome1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String dynavEstado_uf2_Jsonclick ;
      private String edtavEstado_nome2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String dynavEstado_uf3_Jsonclick ;
      private String edtavEstado_nome3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblEstadotitle_Internalname ;
      private String lblEstadotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_76_fel_idx="0001" ;
      private String ROClassString ;
      private String edtEstado_UF_Jsonclick ;
      private String edtEstado_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_estado_nome_Includesortasc ;
      private bool Ddo_estado_nome_Includesortdsc ;
      private bool Ddo_estado_nome_Includefilter ;
      private bool Ddo_estado_nome_Filterisrange ;
      private bool Ddo_estado_nome_Includedatalist ;
      private bool Ddo_estado_ativo_Includesortasc ;
      private bool Ddo_estado_ativo_Includesortdsc ;
      private bool Ddo_estado_ativo_Includefilter ;
      private bool Ddo_estado_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A634Estado_Ativo ;
      private bool AV84WWEstadoDS_4_Dynamicfiltersenabled2 ;
      private bool AV88WWEstadoDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV54Display_IsBlob ;
      private String AV74ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV65ddo_Estado_NomeTitleControlIdToReplace ;
      private String AV68ddo_Estado_AtivoTitleControlIdToReplace ;
      private String AV95Update_GXI ;
      private String AV96Delete_GXI ;
      private String AV97Display_GXI ;
      private String AV81WWEstadoDS_1_Dynamicfiltersselector1 ;
      private String AV85WWEstadoDS_5_Dynamicfiltersselector2 ;
      private String AV89WWEstadoDS_9_Dynamicfiltersselector3 ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV54Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV32Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavEstado_uf1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavEstado_uf2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavEstado_uf3 ;
      private GXCheckbox chkEstado_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00172_A23Estado_UF ;
      private String[] H00172_A24Estado_Nome ;
      private String[] H00173_A23Estado_UF ;
      private String[] H00173_A24Estado_Nome ;
      private String[] H00174_A23Estado_UF ;
      private String[] H00174_A24Estado_Nome ;
      private bool[] H00175_A634Estado_Ativo ;
      private String[] H00175_A24Estado_Nome ;
      private String[] H00175_A23Estado_UF ;
      private long[] H00176_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV77ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62Estado_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66Estado_AtivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV75ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV78ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV69DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV76ManageFiltersItem ;
   }

   public class wwestado__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00175( IGxContext context ,
                                             String AV81WWEstadoDS_1_Dynamicfiltersselector1 ,
                                             String AV82WWEstadoDS_2_Estado_uf1 ,
                                             String AV83WWEstadoDS_3_Estado_nome1 ,
                                             bool AV84WWEstadoDS_4_Dynamicfiltersenabled2 ,
                                             String AV85WWEstadoDS_5_Dynamicfiltersselector2 ,
                                             String AV86WWEstadoDS_6_Estado_uf2 ,
                                             String AV87WWEstadoDS_7_Estado_nome2 ,
                                             bool AV88WWEstadoDS_8_Dynamicfiltersenabled3 ,
                                             String AV89WWEstadoDS_9_Dynamicfiltersselector3 ,
                                             String AV90WWEstadoDS_10_Estado_uf3 ,
                                             String AV91WWEstadoDS_11_Estado_nome3 ,
                                             String AV93WWEstadoDS_13_Tfestado_nome_sel ,
                                             String AV92WWEstadoDS_12_Tfestado_nome ,
                                             short AV94WWEstadoDS_14_Tfestado_ativo_sel ,
                                             String A23Estado_UF ,
                                             String A24Estado_Nome ,
                                             bool A634Estado_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Estado_Ativo], [Estado_Nome], [Estado_UF]";
         sFromString = " FROM [Estado] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV81WWEstadoDS_1_Dynamicfiltersselector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWEstadoDS_2_Estado_uf1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV82WWEstadoDS_2_Estado_uf1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV82WWEstadoDS_2_Estado_uf1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWEstadoDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWEstadoDS_3_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV83WWEstadoDS_3_Estado_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV83WWEstadoDS_3_Estado_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV84WWEstadoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV85WWEstadoDS_5_Dynamicfiltersselector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWEstadoDS_6_Estado_uf2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV86WWEstadoDS_6_Estado_uf2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV86WWEstadoDS_6_Estado_uf2 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV84WWEstadoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV85WWEstadoDS_5_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWEstadoDS_7_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV87WWEstadoDS_7_Estado_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV87WWEstadoDS_7_Estado_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV88WWEstadoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV89WWEstadoDS_9_Dynamicfiltersselector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWEstadoDS_10_Estado_uf3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV90WWEstadoDS_10_Estado_uf3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV90WWEstadoDS_10_Estado_uf3 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV88WWEstadoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV89WWEstadoDS_9_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWEstadoDS_11_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV91WWEstadoDS_11_Estado_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV91WWEstadoDS_11_Estado_nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWEstadoDS_13_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWEstadoDS_12_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like @lV92WWEstadoDS_12_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like @lV92WWEstadoDS_12_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWEstadoDS_13_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] = @AV93WWEstadoDS_13_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] = @AV93WWEstadoDS_13_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV94WWEstadoDS_14_Tfestado_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Ativo] = 1)";
            }
         }
         if ( AV94WWEstadoDS_14_Tfestado_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY [Estado_UF]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Estado_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Estado_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Estado_Ativo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Estado_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Estado_UF]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00176( IGxContext context ,
                                             String AV81WWEstadoDS_1_Dynamicfiltersselector1 ,
                                             String AV82WWEstadoDS_2_Estado_uf1 ,
                                             String AV83WWEstadoDS_3_Estado_nome1 ,
                                             bool AV84WWEstadoDS_4_Dynamicfiltersenabled2 ,
                                             String AV85WWEstadoDS_5_Dynamicfiltersselector2 ,
                                             String AV86WWEstadoDS_6_Estado_uf2 ,
                                             String AV87WWEstadoDS_7_Estado_nome2 ,
                                             bool AV88WWEstadoDS_8_Dynamicfiltersenabled3 ,
                                             String AV89WWEstadoDS_9_Dynamicfiltersselector3 ,
                                             String AV90WWEstadoDS_10_Estado_uf3 ,
                                             String AV91WWEstadoDS_11_Estado_nome3 ,
                                             String AV93WWEstadoDS_13_Tfestado_nome_sel ,
                                             String AV92WWEstadoDS_12_Tfestado_nome ,
                                             short AV94WWEstadoDS_14_Tfestado_ativo_sel ,
                                             String A23Estado_UF ,
                                             String A24Estado_Nome ,
                                             bool A634Estado_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [8] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Estado] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV81WWEstadoDS_1_Dynamicfiltersselector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWEstadoDS_2_Estado_uf1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV82WWEstadoDS_2_Estado_uf1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV82WWEstadoDS_2_Estado_uf1 + '%')";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV81WWEstadoDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWEstadoDS_3_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV83WWEstadoDS_3_Estado_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV83WWEstadoDS_3_Estado_nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV84WWEstadoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV85WWEstadoDS_5_Dynamicfiltersselector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWEstadoDS_6_Estado_uf2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV86WWEstadoDS_6_Estado_uf2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV86WWEstadoDS_6_Estado_uf2 + '%')";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV84WWEstadoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV85WWEstadoDS_5_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWEstadoDS_7_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV87WWEstadoDS_7_Estado_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV87WWEstadoDS_7_Estado_nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV88WWEstadoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV89WWEstadoDS_9_Dynamicfiltersselector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWEstadoDS_10_Estado_uf3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV90WWEstadoDS_10_Estado_uf3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV90WWEstadoDS_10_Estado_uf3 + '%')";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV88WWEstadoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV89WWEstadoDS_9_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWEstadoDS_11_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV91WWEstadoDS_11_Estado_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV91WWEstadoDS_11_Estado_nome3 + '%')";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV93WWEstadoDS_13_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWEstadoDS_12_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like @lV92WWEstadoDS_12_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like @lV92WWEstadoDS_12_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWEstadoDS_13_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] = @AV93WWEstadoDS_13_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] = @AV93WWEstadoDS_13_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV94WWEstadoDS_14_Tfestado_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Ativo] = 1)";
            }
         }
         if ( AV94WWEstadoDS_14_Tfestado_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_H00175(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
               case 4 :
                     return conditional_H00176(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00172 ;
          prmH00172 = new Object[] {
          } ;
          Object[] prmH00173 ;
          prmH00173 = new Object[] {
          } ;
          Object[] prmH00174 ;
          prmH00174 = new Object[] {
          } ;
          Object[] prmH00175 ;
          prmH00175 = new Object[] {
          new Object[] {"@lV82WWEstadoDS_2_Estado_uf1",SqlDbType.Char,2,0} ,
          new Object[] {"@lV83WWEstadoDS_3_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWEstadoDS_6_Estado_uf2",SqlDbType.Char,2,0} ,
          new Object[] {"@lV87WWEstadoDS_7_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV90WWEstadoDS_10_Estado_uf3",SqlDbType.Char,2,0} ,
          new Object[] {"@lV91WWEstadoDS_11_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWEstadoDS_12_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV93WWEstadoDS_13_Tfestado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00176 ;
          prmH00176 = new Object[] {
          new Object[] {"@lV82WWEstadoDS_2_Estado_uf1",SqlDbType.Char,2,0} ,
          new Object[] {"@lV83WWEstadoDS_3_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWEstadoDS_6_Estado_uf2",SqlDbType.Char,2,0} ,
          new Object[] {"@lV87WWEstadoDS_7_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV90WWEstadoDS_10_Estado_uf3",SqlDbType.Char,2,0} ,
          new Object[] {"@lV91WWEstadoDS_11_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV92WWEstadoDS_12_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV93WWEstadoDS_13_Tfestado_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00172", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00172,0,0,true,false )
             ,new CursorDef("H00173", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00173,0,0,true,false )
             ,new CursorDef("H00174", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00174,0,0,true,false )
             ,new CursorDef("H00175", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00175,11,0,true,false )
             ,new CursorDef("H00176", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00176,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                return;
             case 4 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

}
