/*
               File: ExtraWWContagemResultadoPgtoFnc
        Description: Pagamento Funcion�rios
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:46:42.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class extrawwcontagemresultadopgtofnc : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public extrawwcontagemresultadopgtofnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public extrawwcontagemresultadopgtofnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavContagemresultado_statusdmn = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         dynavContagemresultado_contadorfm1 = new GXCombobox();
         dynavContagemresultado_cntcod1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         dynavContagemresultado_contadorfm2 = new GXCombobox();
         dynavContagemresultado_cntcod2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         dynavContagemresultado_contadorfm3 = new GXCombobox();
         dynavContagemresultado_cntcod3 = new GXCombobox();
         chkavTodasasareas = new GXCheckbox();
         chkavLiquidadas = new GXCheckbox();
         chkavSelected = new GXCheckbox();
         cmbContagemResultado_StatusDmn = new GXCombobox();
         dynContagemResultado_ContadorFM = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vCONTAGEMRESULTADO_AGRUPADOR1") == 0 )
            {
               AV45TodasAsAreas = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TodasAsAreas", AV45TodasAsAreas);
               AV58AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58AreaTrabalho_Codigo), 6, 0)));
               A1046ContagemResultado_Agrupador = GetNextPar( );
               n1046ContagemResultado_Agrupador = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvCONTAGEMRESULTADO_AGRUPADOR1K70( AV45TodasAsAreas, AV58AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vCONTAGEMRESULTADO_AGRUPADOR2") == 0 )
            {
               AV45TodasAsAreas = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TodasAsAreas", AV45TodasAsAreas);
               AV58AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58AreaTrabalho_Codigo), 6, 0)));
               A1046ContagemResultado_Agrupador = GetNextPar( );
               n1046ContagemResultado_Agrupador = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvCONTAGEMRESULTADO_AGRUPADOR2K70( AV45TodasAsAreas, AV58AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxSuggest"+"_"+"vCONTAGEMRESULTADO_AGRUPADOR3") == 0 )
            {
               AV45TodasAsAreas = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TodasAsAreas", AV45TodasAsAreas);
               AV58AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58AreaTrabalho_Codigo), 6, 0)));
               A1046ContagemResultado_Agrupador = GetNextPar( );
               n1046ContagemResultado_Agrupador = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXSGVvCONTAGEMRESULTADO_AGRUPADOR3K70( AV45TodasAsAreas, AV58AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFM1") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFM1K72( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CNTCOD1") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CNTCOD1K72( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFM2") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFM2K72( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CNTCOD2") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CNTCOD2K72( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTADORFM3") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTADORFM3K72( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CNTCOD3") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CNTCOD3K72( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLACONTAGEMRESULTADO_CONTADORFMK72( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_195 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_195_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_195_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0)));
               AV18DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
               AV89DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0)));
               AV77ContagemResultado_Demanda1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ContagemResultado_Demanda1", AV77ContagemResultado_Demanda1);
               AV164ContagemResultadoLiqLog_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV164ContagemResultadoLiqLog_Data1", context.localUtil.Format(AV164ContagemResultadoLiqLog_Data1, "99/99/99"));
               AV165ContagemResultadoLiqLog_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV165ContagemResultadoLiqLog_Data_To1", context.localUtil.Format(AV165ContagemResultadoLiqLog_Data_To1, "99/99/99"));
               AV55ContagemResultado_Agrupador1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
               AV152ContagemResultado_CntCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV152ContagemResultado_CntCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0)));
               AV23DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
               AV90DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0)));
               AV78ContagemResultado_Demanda2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ContagemResultado_Demanda2", AV78ContagemResultado_Demanda2);
               AV166ContagemResultadoLiqLog_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166ContagemResultadoLiqLog_Data2", context.localUtil.Format(AV166ContagemResultadoLiqLog_Data2, "99/99/99"));
               AV167ContagemResultadoLiqLog_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV167ContagemResultadoLiqLog_Data_To2", context.localUtil.Format(AV167ContagemResultadoLiqLog_Data_To2, "99/99/99"));
               AV56ContagemResultado_Agrupador2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_Agrupador2", AV56ContagemResultado_Agrupador2);
               AV153ContagemResultado_CntCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153ContagemResultado_CntCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0)));
               AV28DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
               AV91DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0)));
               AV79ContagemResultado_Demanda3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ContagemResultado_Demanda3", AV79ContagemResultado_Demanda3);
               AV168ContagemResultadoLiqLog_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168ContagemResultadoLiqLog_Data3", context.localUtil.Format(AV168ContagemResultadoLiqLog_Data3, "99/99/99"));
               AV169ContagemResultadoLiqLog_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV169ContagemResultadoLiqLog_Data_To3", context.localUtil.Format(AV169ContagemResultadoLiqLog_Data_To3, "99/99/99"));
               AV57ContagemResultado_Agrupador3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_Agrupador3", AV57ContagemResultado_Agrupador3);
               AV154ContagemResultado_CntCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154ContagemResultado_CntCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0)));
               AV87Liquidadas = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Liquidadas", AV87Liquidadas);
               AV22DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
               AV27DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV147PaginaAtual = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV147PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV147PaginaAtual), 10, 0)));
               AV45TodasAsAreas = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TodasAsAreas", AV45TodasAsAreas);
               AV85TudoClicked = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TudoClicked", AV85TudoClicked);
               AV84PreView = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84PreView", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84PreView), 4, 0)));
               AV148ContagemResultado_ContratadaPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), 6, 0)));
               AV16ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
               AV102ContagemResultado_StatusDmn = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ContagemResultado_StatusDmn", AV102ContagemResultado_StatusDmn);
               AV19ContagemResultado_DataUltCnt1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataUltCnt1", context.localUtil.Format(AV19ContagemResultado_DataUltCnt1, "99/99/99"));
               AV20ContagemResultado_DataUltCnt_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataUltCnt_To1", context.localUtil.Format(AV20ContagemResultado_DataUltCnt_To1, "99/99/99"));
               AV21ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFM1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0)));
               AV24ContagemResultado_DataUltCnt2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataUltCnt2", context.localUtil.Format(AV24ContagemResultado_DataUltCnt2, "99/99/99"));
               AV25ContagemResultado_DataUltCnt_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_DataUltCnt_To2", context.localUtil.Format(AV25ContagemResultado_DataUltCnt_To2, "99/99/99"));
               AV26ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ContadorFM2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0)));
               AV29ContagemResultado_DataUltCnt3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataUltCnt3", context.localUtil.Format(AV29ContagemResultado_DataUltCnt3, "99/99/99"));
               AV30ContagemResultado_DataUltCnt_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataUltCnt_To3", context.localUtil.Format(AV30ContagemResultado_DataUltCnt_To3, "99/99/99"));
               AV31ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_ContadorFM3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0)));
               AV216Pgmname = GetNextPar( );
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A584ContagemResultado_ContadorFM = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1583ContagemResultado_TipoRegistro = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
               AV83LimiteRegistros = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83LimiteRegistros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83LimiteRegistros), 4, 0)));
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n602ContagemResultado_OSVinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               A603ContagemResultado_DmnVinculada = GetNextPar( );
               n603ContagemResultado_DmnVinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A603ContagemResultado_DmnVinculada", A603ContagemResultado_DmnVinculada);
               A1590ContagemResultado_SiglaSrvVnc = GetNextPar( );
               n1590ContagemResultado_SiglaSrvVnc = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1590ContagemResultado_SiglaSrvVnc", A1590ContagemResultado_SiglaSrvVnc);
               AV35Quantidade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Quantidade), 4, 0)));
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n490ContagemResultado_ContratadaCod = false;
               A1480ContagemResultado_CstUntUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1480ContagemResultado_CstUntUltima", StringUtil.LTrim( StringUtil.Str( A1480ContagemResultado_CstUntUltima, 18, 5)));
               A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1553ContagemResultado_CntSrvCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
               AV69CstUntNrm = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69CstUntNrm", StringUtil.LTrim( StringUtil.Str( AV69CstUntNrm, 18, 5)));
               AV70CalculoPFinal = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70CalculoPFinal", AV70CalculoPFinal);
               A682ContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A682ContagemResultado_PFBFMUltima", StringUtil.LTrim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5)));
               A684ContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A684ContagemResultado_PFBFSUltima", StringUtil.LTrim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5)));
               A1033ContagemResultadoLiqLog_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1033ContagemResultadoLiqLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0)));
               A1043ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1043ContagemResultado_LiqLogCod = false;
               A1371ContagemResultadoLiqLogOS_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1371ContagemResultadoLiqLogOS_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), 6, 0)));
               A1375ContagemResultadoLiqLogOS_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1375ContagemResultadoLiqLogOS_Valor", StringUtil.LTrim( StringUtil.Str( A1375ContagemResultadoLiqLogOS_Valor, 18, 5)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV46Selecionadas);
               AV48QtdeSelecionadas = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
               AV62PFTotalB = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PFTotalB", StringUtil.LTrim( StringUtil.Str( AV62PFTotalB, 14, 5)));
               AV64TotalBruto = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
               AV61ValorB = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorb_Internalname, StringUtil.LTrim( StringUtil.Str( AV61ValorB, 18, 5)));
               AV63GlsTotal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
               A1051ContagemResultado_GlsValor = NumberUtil.Val( GetNextPar( ), ".");
               n1051ContagemResultado_GlsValor = false;
               AV65TotalLiquido = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
               AV81Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Codigo), 6, 0)));
               A1373ContagemResultadoLiqLogOS_UserNom = GetNextPar( );
               n1373ContagemResultadoLiqLogOS_UserNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1373ContagemResultadoLiqLogOS_UserNom", A1373ContagemResultadoLiqLogOS_UserNom);
               A1034ContagemResultadoLiqLog_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               n1034ContagemResultadoLiqLog_Data = false;
               A1372ContagemResultadoLiqLogOS_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1372ContagemResultadoLiqLogOS_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), 6, 0)));
               AV76ContadorFM = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76ContadorFM), 6, 0)));
               AV67ValorL = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorl_Internalname, StringUtil.LTrim( StringUtil.Str( AV67ValorL, 18, 5)));
               AV86ReAbertas = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ReAbertas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ReAbertas), 4, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ExtraWWContagemResultadoPgtoFnc";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("extrawwcontagemresultadopgtofnc:[SendSecurityCheck value for]"+"Registros:"+context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAK72( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTK72( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423464397");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("extrawwcontagemresultadopgtofnc.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV18DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA1", AV77ContagemResultado_Demanda1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOLIQLOG_DATA1", context.localUtil.Format(AV164ContagemResultadoLiqLog_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO1", context.localUtil.Format(AV165ContagemResultadoLiqLog_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_AGRUPADOR1", StringUtil.RTrim( AV55ContagemResultado_Agrupador1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CNTCOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV152ContagemResultado_CntCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV23DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA2", AV78ContagemResultado_Demanda2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOLIQLOG_DATA2", context.localUtil.Format(AV166ContagemResultadoLiqLog_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO2", context.localUtil.Format(AV167ContagemResultadoLiqLog_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_AGRUPADOR2", StringUtil.RTrim( AV56ContagemResultado_Agrupador2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CNTCOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV153ContagemResultado_CntCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV28DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV91DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA3", AV79ContagemResultado_Demanda3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOLIQLOG_DATA3", context.localUtil.Format(AV168ContagemResultadoLiqLog_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO3", context.localUtil.Format(AV169ContagemResultadoLiqLog_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_AGRUPADOR3", StringUtil.RTrim( AV57ContagemResultado_Agrupador3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CNTCOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV154ContagemResultado_CntCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLIQUIDADAS", StringUtil.BoolToStr( AV87Liquidadas));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV22DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV27DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_195", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_195), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV145GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV146GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPAGINAATUAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV147PaginaAtual), 10, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vTUDOCLICKED", AV85TudoClicked);
         GxWebStd.gx_hidden_field( context, "vPREVIEW", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84PreView), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV216Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_TIPOREGISTRO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vLIMITEREGISTROS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83LimiteRegistros), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DMNVINCULADA", A603ContagemResultado_DmnVinculada);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SIGLASRVVNC", StringUtil.RTrim( A1590ContagemResultado_SiglaSrvVnc));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CSTUNTULTIMA", StringUtil.LTrim( StringUtil.NToC( A1480ContagemResultado_CstUntUltima, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCSTUNTNRM", StringUtil.LTrim( StringUtil.NToC( AV69CstUntNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCALCULOPFINAL", StringUtil.RTrim( AV70CalculoPFinal));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOG_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1033ContagemResultadoLiqLog_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1371ContagemResultadoLiqLogOS_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_VALOR", StringUtil.LTrim( StringUtil.NToC( A1375ContagemResultadoLiqLogOS_Valor, 18, 5, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSELECIONADAS", AV46Selecionadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSELECIONADAS", AV46Selecionadas);
         }
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_USERNOM", StringUtil.RTrim( A1373ContagemResultadoLiqLogOS_UserNom));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOLIQLOGOS_USERCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1372ContagemResultadoLiqLogOS_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTADORFM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76ContadorFM), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vREABERTAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86ReAbertas), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vOBSERVACAO", AV59Observacao);
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV156Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVSTTPGMFNC", StringUtil.RTrim( A1600ContagemResultado_CntSrvSttPgmFnc));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_VLRCNC", StringUtil.LTrim( StringUtil.NToC( A1854ContagemResultado_VlrCnc, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vREGISTROS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Width", StringUtil.RTrim( Confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Height", StringUtil.RTrim( Confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Icon", StringUtil.RTrim( Confirmpanel_Icon));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtext", StringUtil.RTrim( Confirmpanel_Confirmtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonyestext", StringUtil.RTrim( Confirmpanel_Buttonyestext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttonnotext", StringUtil.RTrim( Confirmpanel_Buttonnotext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Buttoncanceltext", StringUtil.RTrim( Confirmpanel_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Confirmpanel_Confirmtype));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Draggeable", StringUtil.BoolToStr( Confirmpanel_Draggeable));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ExtraWWContagemResultadoPgtoFnc";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("extrawwcontagemresultadopgtofnc:[SendSecurityCheck value for]"+"Registros:"+context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEK72( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTK72( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("extrawwcontagemresultadopgtofnc.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ExtraWWContagemResultadoPgtoFnc" ;
      }

      public override String GetPgmdesc( )
      {
         return "Pagamento Funcion�rios" ;
      }

      protected void WBK70( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_K72( true) ;
         }
         else
         {
            wb_table1_2_K72( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 274,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,274);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavAreatrabalho_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 275,'',false,'" + sGXsfl_195_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(275, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,275);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 276,'',false,'" + sGXsfl_195_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV27DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(276, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,276);\"");
         }
         wbLoad = true;
      }

      protected void STARTK72( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Pagamento Funcion�rios", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPK70( ) ;
      }

      protected void WSK72( )
      {
         STARTK72( ) ;
         EVTK72( ) ;
      }

      protected void EVTK72( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11K72 */
                              E11K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12K72 */
                              E12K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13K72 */
                              E13K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14K72 */
                              E14K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15K72 */
                              E15K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16K72 */
                              E16K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17K72 */
                              E17K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOLIQUIDAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18K72 */
                              E18K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOATUALIZAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19K72 */
                              E19K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOMOSTRARTUDO'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20K72 */
                              E20K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOEXPORTPDF'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21K72 */
                              E21K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22K72 */
                              E22K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23K72 */
                              E23K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24K72 */
                              E24K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25K72 */
                              E25K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26K72 */
                              E26K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VLIQUIDADAS.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27K72 */
                              E27K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CONTADORFM1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28K72 */
                              E28K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CONTADORFM2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E29K72 */
                              E29K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CONTADORFM3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E30K72 */
                              E30K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CNTCOD1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E31K72 */
                              E31K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CNTCOD2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E32K72 */
                              E32K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_CNTCOD3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E33K72 */
                              E33K72 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) )
                           {
                              nGXsfl_195_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_195_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_195_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1952( ) ;
                              AV41Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV41Selected);
                              A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                              A490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_ContratadaCod_Internalname), ",", "."));
                              n490ContagemResultado_ContratadaCod = false;
                              A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_AreaTrabalhoCod_Internalname), ",", "."));
                              n52Contratada_AreaTrabalhoCod = false;
                              A574ContagemResultado_PFFinal = context.localUtil.CToN( cgiGet( edtContagemResultado_PFFinal_Internalname), ",", ".");
                              A825ContagemResultado_HoraUltCnt = cgiGet( edtContagemResultado_HoraUltCnt_Internalname);
                              A1043ContagemResultado_LiqLogCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_LiqLogCod_Internalname), ",", "."));
                              n1043ContagemResultado_LiqLogCod = false;
                              A53Contratada_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContratada_AreaTrabalhoDes_Internalname));
                              n53Contratada_AreaTrabalhoDes = false;
                              A1046ContagemResultado_Agrupador = StringUtil.Upper( cgiGet( edtContagemResultado_Agrupador_Internalname));
                              n1046ContagemResultado_Agrupador = false;
                              A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
                              n457ContagemResultado_Demanda = false;
                              A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                              n493ContagemResultado_DemandaFM = false;
                              AV42VinculadaCom = cgiGet( edtavVinculadacom_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVinculadacom_Internalname, AV42VinculadaCom);
                              A566ContagemResultado_DataUltCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataUltCnt_Internalname), 0));
                              cmbContagemResultado_StatusDmn.Name = cmbContagemResultado_StatusDmn_Internalname;
                              cmbContagemResultado_StatusDmn.CurrentValue = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                              A484ContagemResultado_StatusDmn = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                              n484ContagemResultado_StatusDmn = false;
                              A1034ContagemResultadoLiqLog_Data = context.localUtil.CToT( cgiGet( edtContagemResultadoLiqLog_Data_Internalname), 0);
                              n1034ContagemResultadoLiqLog_Data = false;
                              dynContagemResultado_ContadorFM.Name = dynContagemResultado_ContadorFM_Internalname;
                              dynContagemResultado_ContadorFM.CurrentValue = cgiGet( dynContagemResultado_ContadorFM_Internalname);
                              A584ContagemResultado_ContadorFM = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_ContadorFM_Internalname), "."));
                              A801ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ServicoSigla_Internalname));
                              n801ContagemResultado_ServicoSigla = false;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFB");
                                 GX_FocusControl = edtavPfb_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV66PFB = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV66PFB, 14, 5)));
                              }
                              else
                              {
                                 AV66PFB = context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV66PFB, 14, 5)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavValorb_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavValorb_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVALORB");
                                 GX_FocusControl = edtavValorb_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV61ValorB = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorb_Internalname, StringUtil.LTrim( StringUtil.Str( AV61ValorB, 18, 5)));
                              }
                              else
                              {
                                 AV61ValorB = context.localUtil.CToN( cgiGet( edtavValorb_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorb_Internalname, StringUtil.LTrim( StringUtil.Str( AV61ValorB, 18, 5)));
                              }
                              A1051ContagemResultado_GlsValor = context.localUtil.CToN( cgiGet( edtContagemResultado_GlsValor_Internalname), ",", ".");
                              n1051ContagemResultado_GlsValor = false;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavValorl_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavValorl_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vVALORL");
                                 GX_FocusControl = edtavValorl_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV67ValorL = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorl_Internalname, StringUtil.LTrim( StringUtil.Str( AV67ValorL, 18, 5)));
                              }
                              else
                              {
                                 AV67ValorL = context.localUtil.CToN( cgiGet( edtavValorl_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorl_Internalname, StringUtil.LTrim( StringUtil.Str( AV67ValorL, 18, 5)));
                              }
                              AV80Warning = cgiGet( edtavWarning_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV80Warning)) ? AV214Warning_GXI : context.convertURL( context.PathToRelativeUrl( AV80Warning))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E34K72 */
                                    E34K72 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E35K72 */
                                    E35K72 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E36K72 */
                                    E36K72 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECTED.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E37K72 */
                                    E37K72 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_areatrabalhocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATADA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV15Contratada_AreaTrabalhoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV18DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV89DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demanda1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA1"), AV77ContagemResultado_Demanda1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoliqlog_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA1"), 0) != AV164ContagemResultadoLiqLog_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoliqlog_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO1"), 0) != AV165ContagemResultadoLiqLog_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_agrupador1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR1"), AV55ContagemResultado_Agrupador1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_cntcod1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTCOD1"), ",", ".") != Convert.ToDecimal( AV152ContagemResultado_CntCod1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV90DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demanda2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA2"), AV78ContagemResultado_Demanda2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoliqlog_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA2"), 0) != AV166ContagemResultadoLiqLog_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoliqlog_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO2"), 0) != AV167ContagemResultadoLiqLog_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_agrupador2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR2"), AV56ContagemResultado_Agrupador2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_cntcod2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTCOD2"), ",", ".") != Convert.ToDecimal( AV153ContagemResultado_CntCod2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV28DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV91DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demanda3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA3"), AV79ContagemResultado_Demanda3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoliqlog_data3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA3"), 0) != AV168ContagemResultadoLiqLog_Data3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultadoliqlog_data_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO3"), 0) != AV169ContagemResultadoLiqLog_Data_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_agrupador3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR3"), AV57ContagemResultado_Agrupador3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_cntcod3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTCOD3"), ",", ".") != Convert.ToDecimal( AV154ContagemResultado_CntCod3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Liquidadas Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vLIQUIDADAS")) != AV87Liquidadas )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV27DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEK72( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAK72( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavContagemresultado_statusdmn.Name = "vCONTAGEMRESULTADO_STATUSDMN";
            cmbavContagemresultado_statusdmn.WebTags = "";
            cmbavContagemresultado_statusdmn.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn.addItem("D", "Rejeitada", 0);
            cmbavContagemresultado_statusdmn.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn.ItemCount > 0 )
            {
               AV102ContagemResultado_StatusDmn = cmbavContagemresultado_statusdmn.getValidValue(AV102ContagemResultado_StatusDmn);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ContagemResultado_StatusDmn", AV102ContagemResultado_StatusDmn);
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DATAULTCNT", "Data de execu��o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DEMANDA", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_CONTADORFM", "Funcion�rio", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOLIQLOG_DATA", "Data de liquidada", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_CNTCOD", "Contrato", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV18DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV89DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0)));
            }
            dynavContagemresultado_contadorfm1.Name = "vCONTAGEMRESULTADO_CONTADORFM1";
            dynavContagemresultado_contadorfm1.WebTags = "";
            dynavContagemresultado_cntcod1.Name = "vCONTAGEMRESULTADO_CNTCOD1";
            dynavContagemresultado_cntcod1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DATAULTCNT", "Data de execu��o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DEMANDA", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_CONTADORFM", "Funcion�rio", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOLIQLOG_DATA", "Data de liquidada", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_CNTCOD", "Contrato", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV90DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0)));
            }
            dynavContagemresultado_contadorfm2.Name = "vCONTAGEMRESULTADO_CONTADORFM2";
            dynavContagemresultado_contadorfm2.WebTags = "";
            dynavContagemresultado_cntcod2.Name = "vCONTAGEMRESULTADO_CNTCOD2";
            dynavContagemresultado_cntcod2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DATAULTCNT", "Data de execu��o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DEMANDA", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_CONTADORFM", "Funcion�rio", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOLIQLOG_DATA", "Data de liquidada", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_CNTCOD", "Contrato", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV28DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV28DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV91DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0)));
            }
            dynavContagemresultado_contadorfm3.Name = "vCONTAGEMRESULTADO_CONTADORFM3";
            dynavContagemresultado_contadorfm3.WebTags = "";
            dynavContagemresultado_cntcod3.Name = "vCONTAGEMRESULTADO_CNTCOD3";
            dynavContagemresultado_cntcod3.WebTags = "";
            chkavTodasasareas.Name = "vTODASASAREAS";
            chkavTodasasareas.WebTags = "";
            chkavTodasasareas.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavTodasasareas_Internalname, "TitleCaption", chkavTodasasareas.Caption);
            chkavTodasasareas.CheckedValue = "false";
            chkavLiquidadas.Name = "vLIQUIDADAS";
            chkavLiquidadas.WebTags = "";
            chkavLiquidadas.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavLiquidadas_Internalname, "TitleCaption", chkavLiquidadas.Caption);
            chkavLiquidadas.CheckedValue = "False";
            GXCCtl = "vSELECTED_" + sGXsfl_195_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption);
            chkavSelected.CheckedValue = "false";
            GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_195_idx;
            cmbContagemResultado_StatusDmn.Name = GXCCtl;
            cmbContagemResultado_StatusDmn.WebTags = "";
            cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
            cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
            cmbContagemResultado_StatusDmn.addItem("D", "Rejeitada", 0);
            cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
            cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
            cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
            {
               A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = false;
            }
            GXCCtl = "CONTAGEMRESULTADO_CONTADORFM_" + sGXsfl_195_idx;
            dynContagemResultado_ContadorFM.Name = GXCCtl;
            dynContagemResultado_ContadorFM.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXSGVvCONTAGEMRESULTADO_AGRUPADOR1K70( bool AV45TodasAsAreas ,
                                                            int AV58AreaTrabalho_Codigo ,
                                                            String A1046ContagemResultado_Agrupador )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvCONTAGEMRESULTADO_AGRUPADOR1_dataK70( AV45TodasAsAreas, AV58AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvCONTAGEMRESULTADO_AGRUPADOR1_dataK70( bool AV45TodasAsAreas ,
                                                                 int AV58AreaTrabalho_Codigo ,
                                                                 String A1046ContagemResultado_Agrupador )
      {
         l1046ContagemResultado_Agrupador = StringUtil.PadR( StringUtil.RTrim( A1046ContagemResultado_Agrupador), 15, "%");
         n1046ContagemResultado_Agrupador = false;
         /* Using cursor H00K72 */
         pr_default.execute(0, new Object[] {l1046ContagemResultado_Agrupador, AV45TodasAsAreas, AV58AreaTrabalho_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00K72_A1046ContagemResultado_Agrupador[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K72_A1046ContagemResultado_Agrupador[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXSGVvCONTAGEMRESULTADO_AGRUPADOR2K70( bool AV45TodasAsAreas ,
                                                            int AV58AreaTrabalho_Codigo ,
                                                            String A1046ContagemResultado_Agrupador )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvCONTAGEMRESULTADO_AGRUPADOR2_dataK70( AV45TodasAsAreas, AV58AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvCONTAGEMRESULTADO_AGRUPADOR2_dataK70( bool AV45TodasAsAreas ,
                                                                 int AV58AreaTrabalho_Codigo ,
                                                                 String A1046ContagemResultado_Agrupador )
      {
         l1046ContagemResultado_Agrupador = StringUtil.PadR( StringUtil.RTrim( A1046ContagemResultado_Agrupador), 15, "%");
         n1046ContagemResultado_Agrupador = false;
         /* Using cursor H00K73 */
         pr_default.execute(1, new Object[] {l1046ContagemResultado_Agrupador, AV45TodasAsAreas, AV58AreaTrabalho_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00K73_A1046ContagemResultado_Agrupador[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K73_A1046ContagemResultado_Agrupador[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXSGVvCONTAGEMRESULTADO_AGRUPADOR3K70( bool AV45TodasAsAreas ,
                                                            int AV58AreaTrabalho_Codigo ,
                                                            String A1046ContagemResultado_Agrupador )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXSGVvCONTAGEMRESULTADO_AGRUPADOR3_dataK70( AV45TodasAsAreas, AV58AreaTrabalho_Codigo, A1046ContagemResultado_Agrupador) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXSGVvCONTAGEMRESULTADO_AGRUPADOR3_dataK70( bool AV45TodasAsAreas ,
                                                                 int AV58AreaTrabalho_Codigo ,
                                                                 String A1046ContagemResultado_Agrupador )
      {
         l1046ContagemResultado_Agrupador = StringUtil.PadR( StringUtil.RTrim( A1046ContagemResultado_Agrupador), 15, "%");
         n1046ContagemResultado_Agrupador = false;
         /* Using cursor H00K74 */
         pr_default.execute(2, new Object[] {l1046ContagemResultado_Agrupador, AV45TodasAsAreas, AV58AreaTrabalho_Codigo});
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00K74_A1046ContagemResultado_Agrupador[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K74_A1046ContagemResultado_Agrupador[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFM1K72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFM1_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFM1_htmlK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFM1_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfm1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfm1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfm1.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( dynavContagemresultado_contadorfm1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFM1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFM1_dataK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* * Property Userehcontratanre not supported in */
         /* * Property Userehcontratanre not supported in */
         /* Using cursor H00K75 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Userehcontratada, AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            /* * Property Userehcontratanre not supported in */
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K75_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K75_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTCOD1K72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CNTCOD1_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CNTCOD1_htmlK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CNTCOD1_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_cntcod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_cntcod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_cntcod1.ItemCount > 0 )
         {
            AV152ContagemResultado_CntCod1 = (int)(NumberUtil.Val( dynavContagemresultado_cntcod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV152ContagemResultado_CntCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTCOD1_dataK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* Using cursor H00K76 */
         pr_default.execute(4, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K76_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K76_A77Contrato_Numero[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFM2K72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFM2_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFM2_htmlK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFM2_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfm2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfm2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfm2.ItemCount > 0 )
         {
            AV26ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( dynavContagemresultado_contadorfm2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ContadorFM2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFM2_dataK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* * Property Userehcontratanre not supported in */
         /* * Property Userehcontratanre not supported in */
         /* Using cursor H00K77 */
         pr_default.execute(5, new Object[] {AV6WWPContext.gxTpr_Userehcontratada, AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            /* * Property Userehcontratanre not supported in */
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K77_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K77_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTCOD2K72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CNTCOD2_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CNTCOD2_htmlK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CNTCOD2_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_cntcod2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_cntcod2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_cntcod2.ItemCount > 0 )
         {
            AV153ContagemResultado_CntCod2 = (int)(NumberUtil.Val( dynavContagemresultado_cntcod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153ContagemResultado_CntCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTCOD2_dataK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* Using cursor H00K78 */
         pr_default.execute(6, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K78_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K78_A77Contrato_Numero[0]));
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFM3K72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTADORFM3_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTADORFM3_htmlK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTADORFM3_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contadorfm3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contadorfm3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contadorfm3.ItemCount > 0 )
         {
            AV31ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( dynavContagemresultado_contadorfm3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_ContadorFM3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTADORFM3_dataK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* * Property Userehcontratanre not supported in */
         /* * Property Userehcontratanre not supported in */
         /* Using cursor H00K79 */
         pr_default.execute(7, new Object[] {AV6WWPContext.gxTpr_Userehcontratada, AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            /* * Property Userehcontratanre not supported in */
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K79_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K79_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTCOD3K72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CNTCOD3_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CNTCOD3_htmlK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CNTCOD3_dataK72( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_cntcod3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_cntcod3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_cntcod3.ItemCount > 0 )
         {
            AV154ContagemResultado_CntCod3 = (int)(NumberUtil.Val( dynavContagemresultado_cntcod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154ContagemResultado_CntCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTCOD3_dataK72( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Todos)");
         /* Using cursor H00K710 */
         pr_default.execute(8, new Object[] {AV6WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K710_A74Contrato_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K710_A77Contrato_Numero[0]));
            pr_default.readNext(8);
         }
         pr_default.close(8);
      }

      protected void GXDLACONTAGEMRESULTADO_CONTADORFMK72( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_CONTADORFM_dataK72( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_CONTADORFM_htmlK72( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_CONTADORFM_dataK72( ) ;
         gxdynajaxindex = 1;
         dynContagemResultado_ContadorFM.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_ContadorFM.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_CONTADORFM_dataK72( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00K711 */
         pr_default.execute(9);
         while ( (pr_default.getStatus(9) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00K711_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00K711_A58Usuario_PessoaNom[0]));
            pr_default.readNext(9);
         }
         pr_default.close(9);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1952( ) ;
         while ( nGXsfl_195_idx <= nRC_GXsfl_195 )
         {
            sendrow_1952( ) ;
            nGXsfl_195_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_195_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_195_idx+1));
            sGXsfl_195_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_195_idx), 4, 0)), 4, "0");
            SubsflControlProps_1952( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV15Contratada_AreaTrabalhoCod ,
                                       String AV18DynamicFiltersSelector1 ,
                                       short AV89DynamicFiltersOperator1 ,
                                       String AV77ContagemResultado_Demanda1 ,
                                       DateTime AV164ContagemResultadoLiqLog_Data1 ,
                                       DateTime AV165ContagemResultadoLiqLog_Data_To1 ,
                                       String AV55ContagemResultado_Agrupador1 ,
                                       int AV152ContagemResultado_CntCod1 ,
                                       String AV23DynamicFiltersSelector2 ,
                                       short AV90DynamicFiltersOperator2 ,
                                       String AV78ContagemResultado_Demanda2 ,
                                       DateTime AV166ContagemResultadoLiqLog_Data2 ,
                                       DateTime AV167ContagemResultadoLiqLog_Data_To2 ,
                                       String AV56ContagemResultado_Agrupador2 ,
                                       int AV153ContagemResultado_CntCod2 ,
                                       String AV28DynamicFiltersSelector3 ,
                                       short AV91DynamicFiltersOperator3 ,
                                       String AV79ContagemResultado_Demanda3 ,
                                       DateTime AV168ContagemResultadoLiqLog_Data3 ,
                                       DateTime AV169ContagemResultadoLiqLog_Data_To3 ,
                                       String AV57ContagemResultado_Agrupador3 ,
                                       int AV154ContagemResultado_CntCod3 ,
                                       bool AV87Liquidadas ,
                                       bool AV22DynamicFiltersEnabled2 ,
                                       bool AV27DynamicFiltersEnabled3 ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       long AV147PaginaAtual ,
                                       bool AV45TodasAsAreas ,
                                       bool AV85TudoClicked ,
                                       short AV84PreView ,
                                       int AV148ContagemResultado_ContratadaPessoaCod ,
                                       int AV16ContagemResultado_LiqLogCod ,
                                       String AV102ContagemResultado_StatusDmn ,
                                       DateTime AV19ContagemResultado_DataUltCnt1 ,
                                       DateTime AV20ContagemResultado_DataUltCnt_To1 ,
                                       int AV21ContagemResultado_ContadorFM1 ,
                                       DateTime AV24ContagemResultado_DataUltCnt2 ,
                                       DateTime AV25ContagemResultado_DataUltCnt_To2 ,
                                       int AV26ContagemResultado_ContadorFM2 ,
                                       DateTime AV29ContagemResultado_DataUltCnt3 ,
                                       DateTime AV30ContagemResultado_DataUltCnt_To3 ,
                                       int AV31ContagemResultado_ContadorFM3 ,
                                       String AV216Pgmname ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       int A456ContagemResultado_Codigo ,
                                       int A584ContagemResultado_ContadorFM ,
                                       short A1583ContagemResultado_TipoRegistro ,
                                       short AV83LimiteRegistros ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A602ContagemResultado_OSVinculada ,
                                       String A603ContagemResultado_DmnVinculada ,
                                       String A1590ContagemResultado_SiglaSrvVnc ,
                                       short AV35Quantidade ,
                                       int A490ContagemResultado_ContratadaCod ,
                                       decimal A1480ContagemResultado_CstUntUltima ,
                                       int A1553ContagemResultado_CntSrvCod ,
                                       decimal AV69CstUntNrm ,
                                       String AV70CalculoPFinal ,
                                       decimal A682ContagemResultado_PFBFMUltima ,
                                       decimal A684ContagemResultado_PFBFSUltima ,
                                       int A1033ContagemResultadoLiqLog_Codigo ,
                                       int A1043ContagemResultado_LiqLogCod ,
                                       int A1371ContagemResultadoLiqLogOS_OSCod ,
                                       decimal A1375ContagemResultadoLiqLogOS_Valor ,
                                       IGxCollection AV46Selecionadas ,
                                       short AV48QtdeSelecionadas ,
                                       decimal AV62PFTotalB ,
                                       decimal AV64TotalBruto ,
                                       decimal AV61ValorB ,
                                       decimal AV63GlsTotal ,
                                       decimal A1051ContagemResultado_GlsValor ,
                                       decimal AV65TotalLiquido ,
                                       int AV81Codigo ,
                                       String A1373ContagemResultadoLiqLogOS_UserNom ,
                                       DateTime A1034ContagemResultadoLiqLog_Data ,
                                       int A1372ContagemResultadoLiqLogOS_UserCod ,
                                       int AV76ContadorFM ,
                                       decimal AV67ValorL ,
                                       short AV86ReAbertas )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFK72( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CONTRATADACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A490ContagemResultado_ContratadaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFFINAL", GetSecureSignedToken( "", context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_LIQLOGCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1043ContagemResultado_LiqLogCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_LIQLOGCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_AGRUPADOR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1046ContagemResultado_Agrupador, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_AGRUPADOR", StringUtil.RTrim( A1046ContagemResultado_Agrupador));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSDMN", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_GLSVALOR", GetSecureSignedToken( "", context.localUtil.Format( A1051ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_GLSVALOR", StringUtil.LTrim( StringUtil.NToC( A1051ContagemResultado_GlsValor, 12, 2, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn.ItemCount > 0 )
         {
            AV102ContagemResultado_StatusDmn = cmbavContagemresultado_statusdmn.getValidValue(AV102ContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ContagemResultado_StatusDmn", AV102ContagemResultado_StatusDmn);
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV18DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV89DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0)));
         }
         if ( dynavContagemresultado_contadorfm1.ItemCount > 0 )
         {
            AV21ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( dynavContagemresultado_contadorfm1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFM1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0)));
         }
         if ( dynavContagemresultado_cntcod1.ItemCount > 0 )
         {
            AV152ContagemResultado_CntCod1 = (int)(NumberUtil.Val( dynavContagemresultado_cntcod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV152ContagemResultado_CntCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV90DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0)));
         }
         if ( dynavContagemresultado_contadorfm2.ItemCount > 0 )
         {
            AV26ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( dynavContagemresultado_contadorfm2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ContadorFM2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0)));
         }
         if ( dynavContagemresultado_cntcod2.ItemCount > 0 )
         {
            AV153ContagemResultado_CntCod2 = (int)(NumberUtil.Val( dynavContagemresultado_cntcod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153ContagemResultado_CntCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV28DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV28DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV91DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0)));
         }
         if ( dynavContagemresultado_contadorfm3.ItemCount > 0 )
         {
            AV31ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( dynavContagemresultado_contadorfm3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_ContadorFM3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0)));
         }
         if ( dynavContagemresultado_cntcod3.ItemCount > 0 )
         {
            AV154ContagemResultado_CntCod3 = (int)(NumberUtil.Val( dynavContagemresultado_cntcod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154ContagemResultado_CntCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFK72( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV216Pgmname = "ExtraWWContagemResultadoPgtoFnc";
         context.Gx_err = 0;
         edtavVinculadacom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVinculadacom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVinculadacom_Enabled), 5, 0)));
         edtavPfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfb_Enabled), 5, 0)));
         edtavValorb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValorb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorb_Enabled), 5, 0)));
         edtavValorl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValorl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorl_Enabled), 5, 0)));
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavRegistros_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegistros_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegistros_Enabled), 5, 0)));
         edtavQtdeselecionadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdeselecionadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdeselecionadas_Enabled), 5, 0)));
         edtavPftotalb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPftotalb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPftotalb_Enabled), 5, 0)));
         edtavTotalbruto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalbruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalbruto_Enabled), 5, 0)));
         edtavGlstotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlstotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlstotal_Enabled), 5, 0)));
         edtavTotalliquido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalliquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalliquido_Enabled), 5, 0)));
      }

      protected void RFK72( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 195;
         /* Execute user event: E35K72 */
         E35K72 ();
         nGXsfl_195_idx = 1;
         sGXsfl_195_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_195_idx), 4, 0)), 4, "0");
         SubsflControlProps_1952( ) ;
         nGXsfl_195_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1952( ) ;
            pr_default.dynParam(10, new Object[]{ new Object[]{
                                                 AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod ,
                                                 AV87Liquidadas ,
                                                 AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 ,
                                                 AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 ,
                                                 AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 ,
                                                 AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 ,
                                                 AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 ,
                                                 AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 ,
                                                 AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 ,
                                                 AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 ,
                                                 AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 ,
                                                 AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 ,
                                                 AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 ,
                                                 AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 ,
                                                 AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 ,
                                                 AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 ,
                                                 AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 ,
                                                 AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 ,
                                                 AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 ,
                                                 AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 ,
                                                 AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 ,
                                                 AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 ,
                                                 AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 ,
                                                 AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 ,
                                                 AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 ,
                                                 AV10GridState.gxTpr_Dynamicfilters.Count ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 A1043ContagemResultado_LiqLogCod ,
                                                 A457ContagemResultado_Demanda ,
                                                 A1034ContagemResultadoLiqLog_Data ,
                                                 A1046ContagemResultado_Agrupador ,
                                                 A1603ContagemResultado_CntCod ,
                                                 A456ContagemResultado_Codigo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A484ContagemResultado_StatusDmn ,
                                                 A1600ContagemResultado_CntSrvSttPgmFnc ,
                                                 A1854ContagemResultado_VlrCnc ,
                                                 AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 ,
                                                 A566ContagemResultado_DataUltCnt ,
                                                 AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 ,
                                                 AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 ,
                                                 A584ContagemResultado_ContadorFM ,
                                                 A508ContagemResultado_Owner ,
                                                 AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 ,
                                                 AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 ,
                                                 AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 ,
                                                 AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 ,
                                                 AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 ,
                                                 AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 ,
                                                 A499ContagemResultado_ContratadaPessoaCod ,
                                                 AV6WWPContext.gxTpr_Contratada_pessoacod ,
                                                 AV6WWPContext.gxTpr_Userehfinanceiro ,
                                                 AV6WWPContext.gxTpr_Userid ,
                                                 A1583ContagemResultado_TipoRegistro },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT
                                                 }
            });
            lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1), "%", "");
            lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1), "%", "");
            lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1), 15, "%");
            lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2), "%", "");
            lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2), "%", "");
            lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2), 15, "%");
            lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3), "%", "");
            lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3), "%", "");
            lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3), 15, "%");
            /* Using cursor H00K713 */
            pr_default.execute(10, new Object[] {AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1, AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1, AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1, AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1, AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1, AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1, AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1, AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2, AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2, AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2, AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2, AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2, AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2, AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2, AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2, AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2, AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2, AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3, AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3, AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3, AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3, AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3, AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3, AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3, AV6WWPContext.gxTpr_Contratada_pessoacod, AV6WWPContext.gxTpr_Userehfinanceiro, AV6WWPContext.gxTpr_Userid, AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod, AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1, lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1, lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1, AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1, AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1, lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1, AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1, AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2, lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2, lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2, AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2, AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2, lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2, AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2, AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3, lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3, lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3, AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3, AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3, lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3, AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3});
            nGXsfl_195_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(10) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A146Modulo_Codigo = H00K713_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00K713_n146Modulo_Codigo[0];
               A127Sistema_Codigo = H00K713_A127Sistema_Codigo[0];
               A1399Sistema_ImpUserCod = H00K713_A1399Sistema_ImpUserCod[0];
               n1399Sistema_ImpUserCod = H00K713_n1399Sistema_ImpUserCod[0];
               A601ContagemResultado_Servico = H00K713_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00K713_n601ContagemResultado_Servico[0];
               A1627ContagemResultado_CntSrvVncCod = H00K713_A1627ContagemResultado_CntSrvVncCod[0];
               n1627ContagemResultado_CntSrvVncCod = H00K713_n1627ContagemResultado_CntSrvVncCod[0];
               A1591ContagemResultado_CodSrvVnc = H00K713_A1591ContagemResultado_CodSrvVnc[0];
               n1591ContagemResultado_CodSrvVnc = H00K713_n1591ContagemResultado_CodSrvVnc[0];
               A1043ContagemResultado_LiqLogCod = H00K713_A1043ContagemResultado_LiqLogCod[0];
               n1043ContagemResultado_LiqLogCod = H00K713_n1043ContagemResultado_LiqLogCod[0];
               A52Contratada_AreaTrabalhoCod = H00K713_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00K713_n52Contratada_AreaTrabalhoCod[0];
               A292Usuario_EhContratante = H00K713_A292Usuario_EhContratante[0];
               n292Usuario_EhContratante = H00K713_n292Usuario_EhContratante[0];
               A1583ContagemResultado_TipoRegistro = H00K713_A1583ContagemResultado_TipoRegistro[0];
               A1603ContagemResultado_CntCod = H00K713_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00K713_n1603ContagemResultado_CntCod[0];
               A508ContagemResultado_Owner = H00K713_A508ContagemResultado_Owner[0];
               A1854ContagemResultado_VlrCnc = H00K713_A1854ContagemResultado_VlrCnc[0];
               n1854ContagemResultado_VlrCnc = H00K713_n1854ContagemResultado_VlrCnc[0];
               A1600ContagemResultado_CntSrvSttPgmFnc = H00K713_A1600ContagemResultado_CntSrvSttPgmFnc[0];
               n1600ContagemResultado_CntSrvSttPgmFnc = H00K713_n1600ContagemResultado_CntSrvSttPgmFnc[0];
               A499ContagemResultado_ContratadaPessoaCod = H00K713_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = H00K713_n499ContagemResultado_ContratadaPessoaCod[0];
               A602ContagemResultado_OSVinculada = H00K713_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00K713_n602ContagemResultado_OSVinculada[0];
               A1590ContagemResultado_SiglaSrvVnc = H00K713_A1590ContagemResultado_SiglaSrvVnc[0];
               n1590ContagemResultado_SiglaSrvVnc = H00K713_n1590ContagemResultado_SiglaSrvVnc[0];
               A603ContagemResultado_DmnVinculada = H00K713_A603ContagemResultado_DmnVinculada[0];
               n603ContagemResultado_DmnVinculada = H00K713_n603ContagemResultado_DmnVinculada[0];
               A1553ContagemResultado_CntSrvCod = H00K713_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00K713_n1553ContagemResultado_CntSrvCod[0];
               A1051ContagemResultado_GlsValor = H00K713_A1051ContagemResultado_GlsValor[0];
               n1051ContagemResultado_GlsValor = H00K713_n1051ContagemResultado_GlsValor[0];
               A801ContagemResultado_ServicoSigla = H00K713_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00K713_n801ContagemResultado_ServicoSigla[0];
               A1034ContagemResultadoLiqLog_Data = H00K713_A1034ContagemResultadoLiqLog_Data[0];
               n1034ContagemResultadoLiqLog_Data = H00K713_n1034ContagemResultadoLiqLog_Data[0];
               A484ContagemResultado_StatusDmn = H00K713_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00K713_n484ContagemResultado_StatusDmn[0];
               A493ContagemResultado_DemandaFM = H00K713_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00K713_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00K713_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00K713_n457ContagemResultado_Demanda[0];
               A1046ContagemResultado_Agrupador = H00K713_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = H00K713_n1046ContagemResultado_Agrupador[0];
               A53Contratada_AreaTrabalhoDes = H00K713_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = H00K713_n53Contratada_AreaTrabalhoDes[0];
               A490ContagemResultado_ContratadaCod = H00K713_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00K713_n490ContagemResultado_ContratadaCod[0];
               A1480ContagemResultado_CstUntUltima = H00K713_A1480ContagemResultado_CstUntUltima[0];
               A682ContagemResultado_PFBFMUltima = H00K713_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00K713_A684ContagemResultado_PFBFSUltima[0];
               A584ContagemResultado_ContadorFM = H00K713_A584ContagemResultado_ContadorFM[0];
               A566ContagemResultado_DataUltCnt = H00K713_A566ContagemResultado_DataUltCnt[0];
               A825ContagemResultado_HoraUltCnt = H00K713_A825ContagemResultado_HoraUltCnt[0];
               A456ContagemResultado_Codigo = H00K713_A456ContagemResultado_Codigo[0];
               A127Sistema_Codigo = H00K713_A127Sistema_Codigo[0];
               A1399Sistema_ImpUserCod = H00K713_A1399Sistema_ImpUserCod[0];
               n1399Sistema_ImpUserCod = H00K713_n1399Sistema_ImpUserCod[0];
               A292Usuario_EhContratante = H00K713_A292Usuario_EhContratante[0];
               n292Usuario_EhContratante = H00K713_n292Usuario_EhContratante[0];
               A1034ContagemResultadoLiqLog_Data = H00K713_A1034ContagemResultadoLiqLog_Data[0];
               n1034ContagemResultadoLiqLog_Data = H00K713_n1034ContagemResultadoLiqLog_Data[0];
               A1627ContagemResultado_CntSrvVncCod = H00K713_A1627ContagemResultado_CntSrvVncCod[0];
               n1627ContagemResultado_CntSrvVncCod = H00K713_n1627ContagemResultado_CntSrvVncCod[0];
               A603ContagemResultado_DmnVinculada = H00K713_A603ContagemResultado_DmnVinculada[0];
               n603ContagemResultado_DmnVinculada = H00K713_n603ContagemResultado_DmnVinculada[0];
               A1591ContagemResultado_CodSrvVnc = H00K713_A1591ContagemResultado_CodSrvVnc[0];
               n1591ContagemResultado_CodSrvVnc = H00K713_n1591ContagemResultado_CodSrvVnc[0];
               A1590ContagemResultado_SiglaSrvVnc = H00K713_A1590ContagemResultado_SiglaSrvVnc[0];
               n1590ContagemResultado_SiglaSrvVnc = H00K713_n1590ContagemResultado_SiglaSrvVnc[0];
               A601ContagemResultado_Servico = H00K713_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00K713_n601ContagemResultado_Servico[0];
               A1603ContagemResultado_CntCod = H00K713_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = H00K713_n1603ContagemResultado_CntCod[0];
               A1600ContagemResultado_CntSrvSttPgmFnc = H00K713_A1600ContagemResultado_CntSrvSttPgmFnc[0];
               n1600ContagemResultado_CntSrvSttPgmFnc = H00K713_n1600ContagemResultado_CntSrvSttPgmFnc[0];
               A801ContagemResultado_ServicoSigla = H00K713_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00K713_n801ContagemResultado_ServicoSigla[0];
               A52Contratada_AreaTrabalhoCod = H00K713_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00K713_n52Contratada_AreaTrabalhoCod[0];
               A499ContagemResultado_ContratadaPessoaCod = H00K713_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = H00K713_n499ContagemResultado_ContratadaPessoaCod[0];
               A53Contratada_AreaTrabalhoDes = H00K713_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = H00K713_n53Contratada_AreaTrabalhoDes[0];
               A1480ContagemResultado_CstUntUltima = H00K713_A1480ContagemResultado_CstUntUltima[0];
               A682ContagemResultado_PFBFMUltima = H00K713_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00K713_A684ContagemResultado_PFBFSUltima[0];
               A584ContagemResultado_ContadorFM = H00K713_A584ContagemResultado_ContadorFM[0];
               A566ContagemResultado_DataUltCnt = H00K713_A566ContagemResultado_DataUltCnt[0];
               A825ContagemResultado_HoraUltCnt = H00K713_A825ContagemResultado_HoraUltCnt[0];
               if ( ! ( ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADOLIQLOG_DATA") != 0 ) && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADOLIQLOG_DATA") != 0 ) && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADOLIQLOG_DATA") != 0 ) ) || ( (DateTime.MinValue==AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1) && (DateTime.MinValue==AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1) && (DateTime.MinValue==AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2) && (DateTime.MinValue==AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2) && (DateTime.MinValue==AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3) && (DateTime.MinValue==AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3) ) ) || ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, A1600ContagemResultado_CntSrvSttPgmFnc) == 0 ) || ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) && ( A1854ContagemResultado_VlrCnc > Convert.ToDecimal( 0 )) ) || ( new prc_prontoparaliqfnc(context).executeUdp(  A484ContagemResultado_StatusDmn,  A1600ContagemResultado_CntSrvSttPgmFnc) ) ) )
               {
                  GXt_decimal1 = A574ContagemResultado_PFFinal;
                  new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
                  A574ContagemResultado_PFFinal = GXt_decimal1;
                  /* Execute user event: E36K72 */
                  E36K72 ();
               }
               pr_default.readNext(10);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(10) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(10);
            wbEnd = 195;
            WBK70( ) ;
         }
         nGXsfl_195_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod = AV15Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod = AV148ContagemResultado_ContratadaPessoaCod;
         AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod = AV16ContagemResultado_LiqLogCod;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = AV102ContagemResultado_StatusDmn;
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 = AV89DynamicFiltersOperator1;
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = AV19ContagemResultado_DataUltCnt1;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = AV20ContagemResultado_DataUltCnt_To1;
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = AV77ContagemResultado_Demanda1;
         AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = AV21ContagemResultado_ContadorFM1;
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = AV164ContagemResultadoLiqLog_Data1;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = AV165ContagemResultadoLiqLog_Data_To1;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = AV55ContagemResultado_Agrupador1;
         AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 = AV152ContagemResultado_CntCod1;
         AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = AV24ContagemResultado_DataUltCnt2;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = AV25ContagemResultado_DataUltCnt_To2;
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = AV78ContagemResultado_Demanda2;
         AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = AV26ContagemResultado_ContadorFM2;
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = AV166ContagemResultadoLiqLog_Data2;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = AV167ContagemResultadoLiqLog_Data_To2;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = AV56ContagemResultado_Agrupador2;
         AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 = AV153ContagemResultado_CntCod2;
         AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = AV27DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = AV28DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = AV29ContagemResultado_DataUltCnt3;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = AV30ContagemResultado_DataUltCnt_To3;
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = AV79ContagemResultado_Demanda3;
         AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = AV31ContagemResultado_ContadorFM3;
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = AV168ContagemResultadoLiqLog_Data3;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = AV169ContagemResultadoLiqLog_Data_To3;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = AV57ContagemResultado_Agrupador3;
         AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 = AV154ContagemResultado_CntCod3;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod = AV15Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod = AV148ContagemResultado_ContratadaPessoaCod;
         AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod = AV16ContagemResultado_LiqLogCod;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = AV102ContagemResultado_StatusDmn;
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 = AV89DynamicFiltersOperator1;
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = AV19ContagemResultado_DataUltCnt1;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = AV20ContagemResultado_DataUltCnt_To1;
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = AV77ContagemResultado_Demanda1;
         AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = AV21ContagemResultado_ContadorFM1;
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = AV164ContagemResultadoLiqLog_Data1;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = AV165ContagemResultadoLiqLog_Data_To1;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = AV55ContagemResultado_Agrupador1;
         AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 = AV152ContagemResultado_CntCod1;
         AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = AV24ContagemResultado_DataUltCnt2;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = AV25ContagemResultado_DataUltCnt_To2;
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = AV78ContagemResultado_Demanda2;
         AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = AV26ContagemResultado_ContadorFM2;
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = AV166ContagemResultadoLiqLog_Data2;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = AV167ContagemResultadoLiqLog_Data_To2;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = AV56ContagemResultado_Agrupador2;
         AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 = AV153ContagemResultado_CntCod2;
         AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = AV27DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = AV28DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = AV29ContagemResultado_DataUltCnt3;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = AV30ContagemResultado_DataUltCnt_To3;
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = AV79ContagemResultado_Demanda3;
         AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = AV31ContagemResultado_ContadorFM3;
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = AV168ContagemResultadoLiqLog_Data3;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = AV169ContagemResultadoLiqLog_Data_To3;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = AV57ContagemResultado_Agrupador3;
         AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 = AV154ContagemResultado_CntCod3;
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod = AV15Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod = AV148ContagemResultado_ContratadaPessoaCod;
         AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod = AV16ContagemResultado_LiqLogCod;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = AV102ContagemResultado_StatusDmn;
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 = AV89DynamicFiltersOperator1;
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = AV19ContagemResultado_DataUltCnt1;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = AV20ContagemResultado_DataUltCnt_To1;
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = AV77ContagemResultado_Demanda1;
         AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = AV21ContagemResultado_ContadorFM1;
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = AV164ContagemResultadoLiqLog_Data1;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = AV165ContagemResultadoLiqLog_Data_To1;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = AV55ContagemResultado_Agrupador1;
         AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 = AV152ContagemResultado_CntCod1;
         AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = AV24ContagemResultado_DataUltCnt2;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = AV25ContagemResultado_DataUltCnt_To2;
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = AV78ContagemResultado_Demanda2;
         AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = AV26ContagemResultado_ContadorFM2;
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = AV166ContagemResultadoLiqLog_Data2;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = AV167ContagemResultadoLiqLog_Data_To2;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = AV56ContagemResultado_Agrupador2;
         AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 = AV153ContagemResultado_CntCod2;
         AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = AV27DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = AV28DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = AV29ContagemResultado_DataUltCnt3;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = AV30ContagemResultado_DataUltCnt_To3;
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = AV79ContagemResultado_Demanda3;
         AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = AV31ContagemResultado_ContadorFM3;
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = AV168ContagemResultadoLiqLog_Data3;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = AV169ContagemResultadoLiqLog_Data_To3;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = AV57ContagemResultado_Agrupador3;
         AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 = AV154ContagemResultado_CntCod3;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod = AV15Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod = AV148ContagemResultado_ContratadaPessoaCod;
         AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod = AV16ContagemResultado_LiqLogCod;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = AV102ContagemResultado_StatusDmn;
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 = AV89DynamicFiltersOperator1;
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = AV19ContagemResultado_DataUltCnt1;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = AV20ContagemResultado_DataUltCnt_To1;
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = AV77ContagemResultado_Demanda1;
         AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = AV21ContagemResultado_ContadorFM1;
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = AV164ContagemResultadoLiqLog_Data1;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = AV165ContagemResultadoLiqLog_Data_To1;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = AV55ContagemResultado_Agrupador1;
         AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 = AV152ContagemResultado_CntCod1;
         AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = AV24ContagemResultado_DataUltCnt2;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = AV25ContagemResultado_DataUltCnt_To2;
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = AV78ContagemResultado_Demanda2;
         AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = AV26ContagemResultado_ContadorFM2;
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = AV166ContagemResultadoLiqLog_Data2;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = AV167ContagemResultadoLiqLog_Data_To2;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = AV56ContagemResultado_Agrupador2;
         AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 = AV153ContagemResultado_CntCod2;
         AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = AV27DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = AV28DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = AV29ContagemResultado_DataUltCnt3;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = AV30ContagemResultado_DataUltCnt_To3;
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = AV79ContagemResultado_Demanda3;
         AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = AV31ContagemResultado_ContadorFM3;
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = AV168ContagemResultadoLiqLog_Data3;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = AV169ContagemResultadoLiqLog_Data_To3;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = AV57ContagemResultado_Agrupador3;
         AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 = AV154ContagemResultado_CntCod3;
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod = AV15Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod = AV148ContagemResultado_ContratadaPessoaCod;
         AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod = AV16ContagemResultado_LiqLogCod;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = AV102ContagemResultado_StatusDmn;
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 = AV89DynamicFiltersOperator1;
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = AV19ContagemResultado_DataUltCnt1;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = AV20ContagemResultado_DataUltCnt_To1;
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = AV77ContagemResultado_Demanda1;
         AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = AV21ContagemResultado_ContadorFM1;
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = AV164ContagemResultadoLiqLog_Data1;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = AV165ContagemResultadoLiqLog_Data_To1;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = AV55ContagemResultado_Agrupador1;
         AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 = AV152ContagemResultado_CntCod1;
         AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = AV24ContagemResultado_DataUltCnt2;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = AV25ContagemResultado_DataUltCnt_To2;
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = AV78ContagemResultado_Demanda2;
         AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = AV26ContagemResultado_ContadorFM2;
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = AV166ContagemResultadoLiqLog_Data2;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = AV167ContagemResultadoLiqLog_Data_To2;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = AV56ContagemResultado_Agrupador2;
         AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 = AV153ContagemResultado_CntCod2;
         AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = AV27DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = AV28DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = AV29ContagemResultado_DataUltCnt3;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = AV30ContagemResultado_DataUltCnt_To3;
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = AV79ContagemResultado_Demanda3;
         AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = AV31ContagemResultado_ContadorFM3;
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = AV168ContagemResultadoLiqLog_Data3;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = AV169ContagemResultadoLiqLog_Data_To3;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = AV57ContagemResultado_Agrupador3;
         AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 = AV154ContagemResultado_CntCod3;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         }
         return (int)(0) ;
      }

      protected void STRUPK70( )
      {
         /* Before Start, stand alone formulas. */
         AV216Pgmname = "ExtraWWContagemResultadoPgtoFnc";
         context.Gx_err = 0;
         edtavVinculadacom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVinculadacom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVinculadacom_Enabled), 5, 0)));
         edtavPfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfb_Enabled), 5, 0)));
         edtavValorb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValorb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorb_Enabled), 5, 0)));
         edtavValorl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavValorl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavValorl_Enabled), 5, 0)));
         edtavQuantidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuantidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuantidade_Enabled), 5, 0)));
         edtavRegistros_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegistros_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegistros_Enabled), 5, 0)));
         edtavQtdeselecionadas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdeselecionadas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdeselecionadas_Enabled), 5, 0)));
         edtavPftotalb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPftotalb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPftotalb_Enabled), 5, 0)));
         edtavTotalbruto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalbruto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalbruto_Enabled), 5, 0)));
         edtavGlstotal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGlstotal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGlstotal_Enabled), 5, 0)));
         edtavTotalliquido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTotalliquido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTotalliquido_Enabled), 5, 0)));
         GXACONTAGEMRESULTADO_CONTADORFM_htmlK72( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E34K72 */
         E34K72 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_CONTADORFM1_htmlK72( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CNTCOD1_htmlK72( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTADORFM2_htmlK72( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CNTCOD2_htmlK72( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CONTADORFM3_htmlK72( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CNTCOD3_htmlK72( AV6WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_AREATRABALHOCOD");
               GX_FocusControl = edtavContratada_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15Contratada_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV15Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contratadapessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contratadapessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_CONTRATADAPESSOACOD");
               GX_FocusControl = edtavContagemresultado_contratadapessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV148ContagemResultado_ContratadaPessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), 6, 0)));
            }
            else
            {
               AV148ContagemResultado_ContratadaPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_contratadapessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_liqlogcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_liqlogcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_LIQLOGCOD");
               GX_FocusControl = edtavContagemresultado_liqlogcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContagemResultado_LiqLogCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
            }
            else
            {
               AV16ContagemResultado_LiqLogCod = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_liqlogcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
            }
            cmbavContagemresultado_statusdmn.Name = cmbavContagemresultado_statusdmn_Internalname;
            cmbavContagemresultado_statusdmn.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn_Internalname);
            AV102ContagemResultado_StatusDmn = cgiGet( cmbavContagemresultado_statusdmn_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ContagemResultado_StatusDmn", AV102ContagemResultado_StatusDmn);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV18DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV89DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt1"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT1");
               GX_FocusControl = edtavContagemresultado_dataultcnt1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19ContagemResultado_DataUltCnt1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataUltCnt1", context.localUtil.Format(AV19ContagemResultado_DataUltCnt1, "99/99/99"));
            }
            else
            {
               AV19ContagemResultado_DataUltCnt1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataUltCnt1", context.localUtil.Format(AV19ContagemResultado_DataUltCnt1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt_To1"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT_TO1");
               GX_FocusControl = edtavContagemresultado_dataultcnt_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContagemResultado_DataUltCnt_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataUltCnt_To1", context.localUtil.Format(AV20ContagemResultado_DataUltCnt_To1, "99/99/99"));
            }
            else
            {
               AV20ContagemResultado_DataUltCnt_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataUltCnt_To1", context.localUtil.Format(AV20ContagemResultado_DataUltCnt_To1, "99/99/99"));
            }
            AV77ContagemResultado_Demanda1 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ContagemResultado_Demanda1", AV77ContagemResultado_Demanda1);
            dynavContagemresultado_contadorfm1.Name = dynavContagemresultado_contadorfm1_Internalname;
            dynavContagemresultado_contadorfm1.CurrentValue = cgiGet( dynavContagemresultado_contadorfm1_Internalname);
            AV21ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfm1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFM1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoliqlog_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado Liq Log_Data1"}), 1, "vCONTAGEMRESULTADOLIQLOG_DATA1");
               GX_FocusControl = edtavContagemresultadoliqlog_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV164ContagemResultadoLiqLog_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV164ContagemResultadoLiqLog_Data1", context.localUtil.Format(AV164ContagemResultadoLiqLog_Data1, "99/99/99"));
            }
            else
            {
               AV164ContagemResultadoLiqLog_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultadoliqlog_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV164ContagemResultadoLiqLog_Data1", context.localUtil.Format(AV164ContagemResultadoLiqLog_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoliqlog_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado Liq Log_Data_To1"}), 1, "vCONTAGEMRESULTADOLIQLOG_DATA_TO1");
               GX_FocusControl = edtavContagemresultadoliqlog_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV165ContagemResultadoLiqLog_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV165ContagemResultadoLiqLog_Data_To1", context.localUtil.Format(AV165ContagemResultadoLiqLog_Data_To1, "99/99/99"));
            }
            else
            {
               AV165ContagemResultadoLiqLog_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultadoliqlog_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV165ContagemResultadoLiqLog_Data_To1", context.localUtil.Format(AV165ContagemResultadoLiqLog_Data_To1, "99/99/99"));
            }
            AV55ContagemResultado_Agrupador1 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
            dynavContagemresultado_cntcod1.Name = dynavContagemresultado_cntcod1_Internalname;
            dynavContagemresultado_cntcod1.CurrentValue = cgiGet( dynavContagemresultado_cntcod1_Internalname);
            AV152ContagemResultado_CntCod1 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_cntcod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV152ContagemResultado_CntCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV23DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV90DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt2"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT2");
               GX_FocusControl = edtavContagemresultado_dataultcnt2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContagemResultado_DataUltCnt2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataUltCnt2", context.localUtil.Format(AV24ContagemResultado_DataUltCnt2, "99/99/99"));
            }
            else
            {
               AV24ContagemResultado_DataUltCnt2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataUltCnt2", context.localUtil.Format(AV24ContagemResultado_DataUltCnt2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt_To2"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT_TO2");
               GX_FocusControl = edtavContagemresultado_dataultcnt_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContagemResultado_DataUltCnt_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_DataUltCnt_To2", context.localUtil.Format(AV25ContagemResultado_DataUltCnt_To2, "99/99/99"));
            }
            else
            {
               AV25ContagemResultado_DataUltCnt_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_DataUltCnt_To2", context.localUtil.Format(AV25ContagemResultado_DataUltCnt_To2, "99/99/99"));
            }
            AV78ContagemResultado_Demanda2 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ContagemResultado_Demanda2", AV78ContagemResultado_Demanda2);
            dynavContagemresultado_contadorfm2.Name = dynavContagemresultado_contadorfm2_Internalname;
            dynavContagemresultado_contadorfm2.CurrentValue = cgiGet( dynavContagemresultado_contadorfm2_Internalname);
            AV26ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfm2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ContadorFM2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoliqlog_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado Liq Log_Data2"}), 1, "vCONTAGEMRESULTADOLIQLOG_DATA2");
               GX_FocusControl = edtavContagemresultadoliqlog_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV166ContagemResultadoLiqLog_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166ContagemResultadoLiqLog_Data2", context.localUtil.Format(AV166ContagemResultadoLiqLog_Data2, "99/99/99"));
            }
            else
            {
               AV166ContagemResultadoLiqLog_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultadoliqlog_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166ContagemResultadoLiqLog_Data2", context.localUtil.Format(AV166ContagemResultadoLiqLog_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoliqlog_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado Liq Log_Data_To2"}), 1, "vCONTAGEMRESULTADOLIQLOG_DATA_TO2");
               GX_FocusControl = edtavContagemresultadoliqlog_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV167ContagemResultadoLiqLog_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV167ContagemResultadoLiqLog_Data_To2", context.localUtil.Format(AV167ContagemResultadoLiqLog_Data_To2, "99/99/99"));
            }
            else
            {
               AV167ContagemResultadoLiqLog_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultadoliqlog_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV167ContagemResultadoLiqLog_Data_To2", context.localUtil.Format(AV167ContagemResultadoLiqLog_Data_To2, "99/99/99"));
            }
            AV56ContagemResultado_Agrupador2 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_Agrupador2", AV56ContagemResultado_Agrupador2);
            dynavContagemresultado_cntcod2.Name = dynavContagemresultado_cntcod2_Internalname;
            dynavContagemresultado_cntcod2.CurrentValue = cgiGet( dynavContagemresultado_cntcod2_Internalname);
            AV153ContagemResultado_CntCod2 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_cntcod2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153ContagemResultado_CntCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV28DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV91DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt3"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT3");
               GX_FocusControl = edtavContagemresultado_dataultcnt3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29ContagemResultado_DataUltCnt3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataUltCnt3", context.localUtil.Format(AV29ContagemResultado_DataUltCnt3, "99/99/99"));
            }
            else
            {
               AV29ContagemResultado_DataUltCnt3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataUltCnt3", context.localUtil.Format(AV29ContagemResultado_DataUltCnt3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataultcnt_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Ult Cnt_To3"}), 1, "vCONTAGEMRESULTADO_DATAULTCNT_TO3");
               GX_FocusControl = edtavContagemresultado_dataultcnt_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30ContagemResultado_DataUltCnt_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataUltCnt_To3", context.localUtil.Format(AV30ContagemResultado_DataUltCnt_To3, "99/99/99"));
            }
            else
            {
               AV30ContagemResultado_DataUltCnt_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_dataultcnt_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataUltCnt_To3", context.localUtil.Format(AV30ContagemResultado_DataUltCnt_To3, "99/99/99"));
            }
            AV79ContagemResultado_Demanda3 = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ContagemResultado_Demanda3", AV79ContagemResultado_Demanda3);
            dynavContagemresultado_contadorfm3.Name = dynavContagemresultado_contadorfm3_Internalname;
            dynavContagemresultado_contadorfm3.CurrentValue = cgiGet( dynavContagemresultado_contadorfm3_Internalname);
            AV31ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contadorfm3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_ContadorFM3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoliqlog_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado Liq Log_Data3"}), 1, "vCONTAGEMRESULTADOLIQLOG_DATA3");
               GX_FocusControl = edtavContagemresultadoliqlog_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV168ContagemResultadoLiqLog_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168ContagemResultadoLiqLog_Data3", context.localUtil.Format(AV168ContagemResultadoLiqLog_Data3, "99/99/99"));
            }
            else
            {
               AV168ContagemResultadoLiqLog_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultadoliqlog_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168ContagemResultadoLiqLog_Data3", context.localUtil.Format(AV168ContagemResultadoLiqLog_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoliqlog_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado Liq Log_Data_To3"}), 1, "vCONTAGEMRESULTADOLIQLOG_DATA_TO3");
               GX_FocusControl = edtavContagemresultadoliqlog_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV169ContagemResultadoLiqLog_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV169ContagemResultadoLiqLog_Data_To3", context.localUtil.Format(AV169ContagemResultadoLiqLog_Data_To3, "99/99/99"));
            }
            else
            {
               AV169ContagemResultadoLiqLog_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultadoliqlog_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV169ContagemResultadoLiqLog_Data_To3", context.localUtil.Format(AV169ContagemResultadoLiqLog_Data_To3, "99/99/99"));
            }
            AV57ContagemResultado_Agrupador3 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_Agrupador3", AV57ContagemResultado_Agrupador3);
            dynavContagemresultado_cntcod3.Name = dynavContagemresultado_cntcod3_Internalname;
            dynavContagemresultado_cntcod3.CurrentValue = cgiGet( dynavContagemresultado_cntcod3_Internalname);
            AV154ContagemResultado_CntCod3 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_cntcod3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154ContagemResultado_CntCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0)));
            AV45TodasAsAreas = StringUtil.StrToBool( cgiGet( chkavTodasasareas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TodasAsAreas", AV45TodasAsAreas);
            AV87Liquidadas = StringUtil.StrToBool( cgiGet( chkavLiquidadas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Liquidadas", AV87Liquidadas);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQUANTIDADE");
               GX_FocusControl = edtavQuantidade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35Quantidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Quantidade), 4, 0)));
            }
            else
            {
               AV35Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavQuantidade_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Quantidade), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRegistros_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRegistros_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREGISTROS");
               GX_FocusControl = edtavRegistros_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Registros = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Registros), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREGISTROS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")));
            }
            else
            {
               AV36Registros = (int)(context.localUtil.CToN( cgiGet( edtavRegistros_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Registros), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREGISTROS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdeselecionadas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdeselecionadas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDESELECIONADAS");
               GX_FocusControl = edtavQtdeselecionadas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48QtdeSelecionadas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
            }
            else
            {
               AV48QtdeSelecionadas = (short)(context.localUtil.CToN( cgiGet( edtavQtdeselecionadas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPftotalb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPftotalb_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFTOTALB");
               GX_FocusControl = edtavPftotalb_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62PFTotalB = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PFTotalB", StringUtil.LTrim( StringUtil.Str( AV62PFTotalB, 14, 5)));
            }
            else
            {
               AV62PFTotalB = context.localUtil.CToN( cgiGet( edtavPftotalb_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PFTotalB", StringUtil.LTrim( StringUtil.Str( AV62PFTotalB, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTotalbruto_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTotalbruto_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTOTALBRUTO");
               GX_FocusControl = edtavTotalbruto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TotalBruto = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
            }
            else
            {
               AV64TotalBruto = context.localUtil.CToN( cgiGet( edtavTotalbruto_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGlstotal_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavGlstotal_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGLSTOTAL");
               GX_FocusControl = edtavGlstotal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63GlsTotal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
            }
            else
            {
               AV63GlsTotal = context.localUtil.CToN( cgiGet( edtavGlstotal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTotalliquido_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTotalliquido_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTOTALLIQUIDO");
               GX_FocusControl = edtavTotalliquido_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TotalLiquido = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
            }
            else
            {
               AV65TotalLiquido = context.localUtil.CToN( cgiGet( edtavTotalliquido_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV58AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58AreaTrabalho_Codigo), 6, 0)));
            }
            AV22DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
            AV27DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_195 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_195"), ",", "."));
            AV145GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV146GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            Confirmpanel_Width = cgiGet( "CONFIRMPANEL_Width");
            Confirmpanel_Height = cgiGet( "CONFIRMPANEL_Height");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            Confirmpanel_Icon = cgiGet( "CONFIRMPANEL_Icon");
            Confirmpanel_Confirmtext = cgiGet( "CONFIRMPANEL_Confirmtext");
            Confirmpanel_Buttonyestext = cgiGet( "CONFIRMPANEL_Buttonyestext");
            Confirmpanel_Buttonnotext = cgiGet( "CONFIRMPANEL_Buttonnotext");
            Confirmpanel_Buttoncanceltext = cgiGet( "CONFIRMPANEL_Buttoncanceltext");
            Confirmpanel_Confirmtype = cgiGet( "CONFIRMPANEL_Confirmtype");
            Confirmpanel_Draggeable = StringUtil.StrToBool( cgiGet( "CONFIRMPANEL_Draggeable"));
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Confirmpanel_Title = cgiGet( "CONFIRMPANEL_Title");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ExtraWWContagemResultadoPgtoFnc";
            AV36Registros = (int)(context.localUtil.CToN( cgiGet( edtavRegistros_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Registros), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREGISTROS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("extrawwcontagemresultadopgtofnc:[SecurityCheckFailed value for]"+"Registros:"+context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATADA_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV15Contratada_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV18DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV89DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA1"), AV77ContagemResultado_Demanda1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA1"), 0) != AV164ContagemResultadoLiqLog_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO1"), 0) != AV165ContagemResultadoLiqLog_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR1"), AV55ContagemResultado_Agrupador1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTCOD1"), ",", ".") != Convert.ToDecimal( AV152ContagemResultado_CntCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV23DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV90DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA2"), AV78ContagemResultado_Demanda2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA2"), 0) != AV166ContagemResultadoLiqLog_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO2"), 0) != AV167ContagemResultadoLiqLog_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR2"), AV56ContagemResultado_Agrupador2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTCOD2"), ",", ".") != Convert.ToDecimal( AV153ContagemResultado_CntCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV28DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV91DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA3"), AV79ContagemResultado_Demanda3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA3"), 0) != AV168ContagemResultadoLiqLog_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOLIQLOG_DATA_TO3"), 0) != AV169ContagemResultadoLiqLog_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR3"), AV57ContagemResultado_Agrupador3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTCOD3"), ",", ".") != Convert.ToDecimal( AV154ContagemResultado_CntCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vLIQUIDADAS")) != AV87Liquidadas )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV22DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV27DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E34K72 */
         E34K72 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E34K72( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( ! AV6WWPContext.gxTpr_Userehadministradorgam && AV6WWPContext.gxTpr_Userehcontratante )
         {
            this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
         AV84PreView = 25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84PreView", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84PreView), 4, 0)));
         AV83LimiteRegistros = 500;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83LimiteRegistros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83LimiteRegistros), 4, 0)));
         bttBtnmostrartudo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnmostrartudo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnmostrartudo_Visible), 5, 0)));
         edtavAreatrabalho_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_codigo_Visible), 5, 0)));
         subGrid_Rows = AV84PreView;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV18DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DATAULTCNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DATAULTCNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV28DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DATAULTCNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         AV102ContagemResultado_StatusDmn = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ContagemResultado_StatusDmn", AV102ContagemResultado_StatusDmn);
         Form.Caption = "Pagamento Funcion�rios";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "�rea de Trabalho", 0);
         cmbavOrderedby.addItem("2", "OS", 0);
         cmbavOrderedby.addItem("3", "Status", 0);
         cmbavOrderedby.addItem("4", "Liquidada", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         AV147PaginaAtual = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV147PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV147PaginaAtual), 10, 0)));
         if ( ! AV6WWPContext.gxTpr_Userehfinanceiro )
         {
            cmbavDynamicfiltersselector1.removeItem("CONTAGEMRESULTADO_CONTADORFM");
            cmbavDynamicfiltersselector2.removeItem("CONTAGEMRESULTADO_CONTADORFM");
            cmbavDynamicfiltersselector3.removeItem("CONTAGEMRESULTADO_CONTADORFM");
         }
      }

      protected void E35K72( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbContagemResultado_StatusDmn_Titleformat = 2;
         cmbContagemResultado_StatusDmn.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Status", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusDmn_Internalname, "Title", cmbContagemResultado_StatusDmn.Title.Text);
         edtContagemResultadoLiqLog_Data_Titleformat = 2;
         edtContagemResultadoLiqLog_Data_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Liquidada", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Data_Internalname, "Title", edtContagemResultadoLiqLog_Data_Title);
         AV145GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV145GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV145GridCurrentPage), 10, 0)));
         AV146GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV146GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV146GridPageCount), 10, 0)));
         AV145GridCurrentPage = AV147PaginaAtual;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV145GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV145GridCurrentPage), 10, 0)));
         edtContratada_AreaTrabalhoDes_Visible = (AV45TodasAsAreas ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_AreaTrabalhoDes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_AreaTrabalhoDes_Visible), 5, 0)));
         edtContagemResultadoLiqLog_Data_Visible = (AV87Liquidadas ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoLiqLog_Data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoLiqLog_Data_Visible), 5, 0)));
         chkavSelected.Visible = (!AV87Liquidadas ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSelected.Visible), 5, 0)));
         AV35Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Quantidade), 4, 0)));
         AV86ReAbertas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ReAbertas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ReAbertas), 4, 0)));
         AV48QtdeSelecionadas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
         AV62PFTotalB = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PFTotalB", StringUtil.LTrim( StringUtil.Str( AV62PFTotalB, 14, 5)));
         AV63GlsTotal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
         AV64TotalBruto = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
         AV48QtdeSelecionadas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
         AV65TotalLiquido = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
         AV46Selecionadas.Clear();
         if ( (0==AV6WWPContext.gxTpr_Contratada_codigo) )
         {
            GX_msglist.addItem("Seu usu�rio n�o pertence a nenhuma Contratada, requerida em regras desta tela!");
         }
         if ( AV45TodasAsAreas )
         {
            AV15Contratada_AreaTrabalhoCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            AV15Contratada_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0)));
         }
         if ( AV85TudoClicked )
         {
            bttBtnliquidar_Tooltiptext = "Liquidar as OS selecionadas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnliquidar_Internalname, "Tooltiptext", bttBtnliquidar_Tooltiptext);
            bttBtnliquidar_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnliquidar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnliquidar_Enabled), 5, 0)));
            bttBtnmostrartudo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnmostrartudo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnmostrartudo_Visible), 5, 0)));
            AV85TudoClicked = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TudoClicked", AV85TudoClicked);
         }
         else
         {
            if ( AV84PreView > 0 )
            {
               /* Execute user subroutine: 'PRECONSULTA' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
         AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod = AV15Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod = AV148ContagemResultado_ContratadaPessoaCod;
         AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod = AV16ContagemResultado_LiqLogCod;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = AV102ContagemResultado_StatusDmn;
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 = AV89DynamicFiltersOperator1;
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = AV19ContagemResultado_DataUltCnt1;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = AV20ContagemResultado_DataUltCnt_To1;
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = AV77ContagemResultado_Demanda1;
         AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = AV21ContagemResultado_ContadorFM1;
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = AV164ContagemResultadoLiqLog_Data1;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = AV165ContagemResultadoLiqLog_Data_To1;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = AV55ContagemResultado_Agrupador1;
         AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 = AV152ContagemResultado_CntCod1;
         AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = AV24ContagemResultado_DataUltCnt2;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = AV25ContagemResultado_DataUltCnt_To2;
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = AV78ContagemResultado_Demanda2;
         AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = AV26ContagemResultado_ContadorFM2;
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = AV166ContagemResultadoLiqLog_Data2;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = AV167ContagemResultadoLiqLog_Data_To2;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = AV56ContagemResultado_Agrupador2;
         AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 = AV153ContagemResultado_CntCod2;
         AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = AV27DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = AV28DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = AV29ContagemResultado_DataUltCnt3;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = AV30ContagemResultado_DataUltCnt_To3;
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = AV79ContagemResultado_Demanda3;
         AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = AV31ContagemResultado_ContadorFM3;
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = AV168ContagemResultadoLiqLog_Data3;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = AV169ContagemResultadoLiqLog_Data_To3;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = AV57ContagemResultado_Agrupador3;
         AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 = AV154ContagemResultado_CntCod3;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Selecionadas", AV46Selecionadas);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11K72( )
      {
         /* Gridpaginationbar_Changepage Routine */
         GXt_int2 = AV147PaginaAtual;
         new prc_currentpage(context ).execute(  Gridpaginationbar_Selectedpage, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gridpaginationbar_Internalname, "SelectedPage", Gridpaginationbar_Selectedpage);
         AV147PaginaAtual = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV147PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV147PaginaAtual), 10, 0)));
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV144PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV144PageToGo) ;
         }
      }

      private void E36K72( )
      {
         /* Grid_Load Routine */
         AV80Warning = context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavWarning_Internalname, AV80Warning);
         AV214Warning_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "4041cec4-85fe-4bd8-aaa2-a8a2026c306c", "", context.GetTheme( )));
         edtavWarning_Tooltiptext = "";
         imgExportpdf_Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Gerar PDF";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
         if ( A602ContagemResultado_OSVinculada > 0 )
         {
            AV42VinculadaCom = "*";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVinculadacom_Internalname, AV42VinculadaCom);
            edtavVinculadacom_Tooltiptext = "Vinculada � OS "+A603ContagemResultado_DmnVinculada+" "+A1590ContagemResultado_SiglaSrvVnc;
            edtavVinculadacom_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A602ContagemResultado_OSVinculada) + "," + UrlEncode(StringUtil.RTrim(""));
         }
         else
         {
            AV42VinculadaCom = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVinculadacom_Internalname, AV42VinculadaCom);
            edtavVinculadacom_Tooltiptext = "";
            edtavVinculadacom_Link = "";
         }
         edtContagemResultado_Demanda_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContagemResultado_Demanda_Linktarget = "_blank";
         AV35Quantidade = (short)(AV35Quantidade+1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Quantidade), 4, 0)));
         AV75ContratadaCod = A490ContagemResultado_ContratadaCod;
         AV76ContadorFM = A584ContagemResultado_ContadorFM;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76ContadorFM), 6, 0)));
         if ( ( A1480ContagemResultado_CstUntUltima > Convert.ToDecimal( 0 )) )
         {
            AV69CstUntNrm = A1480ContagemResultado_CstUntUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69CstUntNrm", StringUtil.LTrim( StringUtil.Str( AV69CstUntNrm, 18, 5)));
         }
         else
         {
            new prc_cstuntprdnrm(context ).execute( ref  A1553ContagemResultado_CntSrvCod, ref  A584ContagemResultado_ContadorFM, ref  AV69CstUntNrm, out  AV70CalculoPFinal) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69CstUntNrm", StringUtil.LTrim( StringUtil.Str( AV69CstUntNrm, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70CalculoPFinal", AV70CalculoPFinal);
         }
         if ( StringUtil.StrCmp(AV70CalculoPFinal, "BM") == 0 )
         {
            AV66PFB = A682ContagemResultado_PFBFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV66PFB, 14, 5)));
         }
         else if ( StringUtil.StrCmp(AV70CalculoPFinal, "MB") == 0 )
         {
            if ( ( A684ContagemResultado_PFBFSUltima > Convert.ToDecimal( 0 )) )
            {
               if ( A684ContagemResultado_PFBFSUltima < A682ContagemResultado_PFBFMUltima )
               {
                  AV66PFB = A684ContagemResultado_PFBFSUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV66PFB, 14, 5)));
               }
               else
               {
                  AV66PFB = A682ContagemResultado_PFBFMUltima;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV66PFB, 14, 5)));
               }
            }
            else
            {
               AV66PFB = A682ContagemResultado_PFBFMUltima;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV66PFB, 14, 5)));
            }
         }
         else
         {
            AV66PFB = A682ContagemResultado_PFBFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV66PFB, 14, 5)));
         }
         AV81Codigo = A456ContagemResultado_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Codigo), 6, 0)));
         if ( AV87Liquidadas )
         {
            /* Using cursor H00K714 */
            pr_default.execute(11, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A1033ContagemResultadoLiqLog_Codigo = H00K714_A1033ContagemResultadoLiqLog_Codigo[0];
               A1371ContagemResultadoLiqLogOS_OSCod = H00K714_A1371ContagemResultadoLiqLogOS_OSCod[0];
               A1375ContagemResultadoLiqLogOS_Valor = H00K714_A1375ContagemResultadoLiqLogOS_Valor[0];
               AV67ValorL = A1375ContagemResultadoLiqLogOS_Valor;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorl_Internalname, StringUtil.LTrim( StringUtil.Str( AV67ValorL, 18, 5)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(11);
            }
            pr_default.close(11);
            AV16ContagemResultado_LiqLogCod = A1043ContagemResultado_LiqLogCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
            /* Execute user subroutine: 'OUTROSPAGAMENTOS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV61ValorB = (decimal)(AV69CstUntNrm*AV66PFB);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorb_Internalname, StringUtil.LTrim( StringUtil.Str( AV61ValorB, 18, 5)));
            AV67ValorL = AV61ValorB;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorl_Internalname, StringUtil.LTrim( StringUtil.Str( AV67ValorL, 18, 5)));
            /* Execute user subroutine: 'PAGAMENTOSREALIZADOS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV41Selected = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV41Selected);
            if ( ! (Convert.ToDecimal(0)==AV67ValorL) )
            {
               AV46Selecionadas.Add(A456ContagemResultado_Codigo, 0);
               AV41Selected = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV41Selected);
               AV48QtdeSelecionadas = (short)(AV48QtdeSelecionadas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
            }
         }
         AV62PFTotalB = (decimal)(AV62PFTotalB+AV66PFB);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PFTotalB", StringUtil.LTrim( StringUtil.Str( AV62PFTotalB, 14, 5)));
         AV64TotalBruto = (decimal)(AV64TotalBruto+AV61ValorB);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
         AV63GlsTotal = (decimal)(AV63GlsTotal+A1051ContagemResultado_GlsValor);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
         AV65TotalLiquido = (decimal)(AV65TotalLiquido+AV67ValorL);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
         AV61ValorB = NumberUtil.Round( AV61ValorB, 2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorb_Internalname, StringUtil.LTrim( StringUtil.Str( AV61ValorB, 18, 5)));
         AV67ValorL = NumberUtil.Round( AV67ValorL, 2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorl_Internalname, StringUtil.LTrim( StringUtil.Str( AV67ValorL, 18, 5)));
         AV64TotalBruto = NumberUtil.Round( AV64TotalBruto, 2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
         AV63GlsTotal = NumberUtil.Round( AV63GlsTotal, 2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
         AV65TotalLiquido = NumberUtil.Round( AV65TotalLiquido, 2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 195;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_1952( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_195_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(195, GridRow);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Selecionadas", AV46Selecionadas);
      }

      protected void E13K72( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22K72( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV22DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E14K72( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavContagemresultado_cntcod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod1_Internalname, "Values", dynavContagemresultado_cntcod1.ToJavascriptSource());
         dynavContagemresultado_contadorfm1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm1_Internalname, "Values", dynavContagemresultado_contadorfm1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_cntcod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod2_Internalname, "Values", dynavContagemresultado_cntcod2.ToJavascriptSource());
         dynavContagemresultado_contadorfm2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm2_Internalname, "Values", dynavContagemresultado_contadorfm2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContagemresultado_cntcod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod3_Internalname, "Values", dynavContagemresultado_cntcod3.ToJavascriptSource());
         dynavContagemresultado_contadorfm3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm3_Internalname, "Values", dynavContagemresultado_contadorfm3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E23K72( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
      }

      protected void E24K72( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV27DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E15K72( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavContagemresultado_cntcod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod1_Internalname, "Values", dynavContagemresultado_cntcod1.ToJavascriptSource());
         dynavContagemresultado_contadorfm1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm1_Internalname, "Values", dynavContagemresultado_contadorfm1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_cntcod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod2_Internalname, "Values", dynavContagemresultado_cntcod2.ToJavascriptSource());
         dynavContagemresultado_contadorfm2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm2_Internalname, "Values", dynavContagemresultado_contadorfm2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContagemresultado_cntcod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod3_Internalname, "Values", dynavContagemresultado_cntcod3.ToJavascriptSource());
         dynavContagemresultado_contadorfm3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm3_Internalname, "Values", dynavContagemresultado_contadorfm3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E25K72( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
      }

      protected void E16K72( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV27DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavContagemresultado_cntcod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod1_Internalname, "Values", dynavContagemresultado_cntcod1.ToJavascriptSource());
         dynavContagemresultado_contadorfm1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm1_Internalname, "Values", dynavContagemresultado_contadorfm1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_cntcod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod2_Internalname, "Values", dynavContagemresultado_cntcod2.ToJavascriptSource());
         dynavContagemresultado_contadorfm2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm2_Internalname, "Values", dynavContagemresultado_contadorfm2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContagemresultado_cntcod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod3_Internalname, "Values", dynavContagemresultado_cntcod3.ToJavascriptSource());
         dynavContagemresultado_contadorfm3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm3_Internalname, "Values", dynavContagemresultado_contadorfm3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E26K72( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
      }

      protected void E17K72( )
      {
         /* 'DoCleanFilters' Routine */
         AV147PaginaAtual = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV147PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV147PaginaAtual), 10, 0)));
         AV87Liquidadas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Liquidadas", AV87Liquidadas);
         AV45TodasAsAreas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TodasAsAreas", AV45TodasAsAreas);
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavContagemresultado_statusdmn.CurrentValue = StringUtil.RTrim( AV102ContagemResultado_StatusDmn);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn_Internalname, "Values", cmbavContagemresultado_statusdmn.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavContagemresultado_cntcod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod1_Internalname, "Values", dynavContagemresultado_cntcod1.ToJavascriptSource());
         dynavContagemresultado_contadorfm1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm1_Internalname, "Values", dynavContagemresultado_contadorfm1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_cntcod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod2_Internalname, "Values", dynavContagemresultado_cntcod2.ToJavascriptSource());
         dynavContagemresultado_contadorfm2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm2_Internalname, "Values", dynavContagemresultado_contadorfm2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         dynavContagemresultado_cntcod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod3_Internalname, "Values", dynavContagemresultado_cntcod3.ToJavascriptSource());
         dynavContagemresultado_contadorfm3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm3_Internalname, "Values", dynavContagemresultado_contadorfm3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E18K72( )
      {
         /* 'DoLiquidar' Routine */
         if ( AV48QtdeSelecionadas == 0 )
         {
            /* Execute user subroutine: 'PAGEUP' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            GX_msglist.addItem("Selecione as OS que deseja incluir no processo!");
         }
         else if ( (Convert.ToDecimal(0)==AV65TotalLiquido) && (0==AV86ReAbertas) )
         {
            /* Execute user subroutine: 'PAGEUP' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            GX_msglist.addItem("N�o existem valores para liquidar nesta consulta!");
         }
         else
         {
            AV47WebSession.Set("SdtCodigos", AV46Selecionadas.ToXml(false, true, "Collection", ""));
            new prc_liquidarfuncionarios(context ).execute(  AV6WWPContext.gxTpr_Userid,  AV59Observacao) ;
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         }
      }

      protected void E19K72( )
      {
         /* 'DoAtualizar' Routine */
         AV84PreView = 25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84PreView", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84PreView), 4, 0)));
         AV147PaginaAtual = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV147PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV147PaginaAtual), 10, 0)));
         subGrid_Rows = AV84PreView;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV85TudoClicked = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TudoClicked", AV85TudoClicked);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E20K72( )
      {
         /* 'DoMostrarTudo' Routine */
         if ( AV36Registros > AV83LimiteRegistros )
         {
            Confirmpanel_Title = "Consulta bloqueada";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel_Internalname, "Title", Confirmpanel_Title);
            Confirmpanel_Confirmtext = "O resultado destes filtros supera os "+StringUtil.Trim( StringUtil.Str( (decimal)(AV83LimiteRegistros), 4, 0))+" registros!";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel_Internalname, "ConfirmText", Confirmpanel_Confirmtext);
            this.executeUsercontrolMethod("", false, "CONFIRMPANELContainer", "Confirm", "", new Object[] {});
         }
         else
         {
            /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV85TudoClicked = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TudoClicked", AV85TudoClicked);
            AV147PaginaAtual = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV147PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV147PaginaAtual), 10, 0)));
            subGrid_Rows = 0;
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15Contratada_AreaTrabalhoCod, AV18DynamicFiltersSelector1, AV89DynamicFiltersOperator1, AV77ContagemResultado_Demanda1, AV164ContagemResultadoLiqLog_Data1, AV165ContagemResultadoLiqLog_Data_To1, AV55ContagemResultado_Agrupador1, AV152ContagemResultado_CntCod1, AV23DynamicFiltersSelector2, AV90DynamicFiltersOperator2, AV78ContagemResultado_Demanda2, AV166ContagemResultadoLiqLog_Data2, AV167ContagemResultadoLiqLog_Data_To2, AV56ContagemResultado_Agrupador2, AV153ContagemResultado_CntCod2, AV28DynamicFiltersSelector3, AV91DynamicFiltersOperator3, AV79ContagemResultado_Demanda3, AV168ContagemResultadoLiqLog_Data3, AV169ContagemResultadoLiqLog_Data_To3, AV57ContagemResultado_Agrupador3, AV154ContagemResultado_CntCod3, AV87Liquidadas, AV22DynamicFiltersEnabled2, AV27DynamicFiltersEnabled3, AV6WWPContext, AV147PaginaAtual, AV45TodasAsAreas, AV85TudoClicked, AV84PreView, AV148ContagemResultado_ContratadaPessoaCod, AV16ContagemResultado_LiqLogCod, AV102ContagemResultado_StatusDmn, AV19ContagemResultado_DataUltCnt1, AV20ContagemResultado_DataUltCnt_To1, AV21ContagemResultado_ContadorFM1, AV24ContagemResultado_DataUltCnt2, AV25ContagemResultado_DataUltCnt_To2, AV26ContagemResultado_ContadorFM2, AV29ContagemResultado_DataUltCnt3, AV30ContagemResultado_DataUltCnt_To3, AV31ContagemResultado_ContadorFM3, AV216Pgmname, A52Contratada_AreaTrabalhoCod, AV10GridState, A456ContagemResultado_Codigo, A584ContagemResultado_ContadorFM, A1583ContagemResultado_TipoRegistro, AV83LimiteRegistros, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A602ContagemResultado_OSVinculada, A603ContagemResultado_DmnVinculada, A1590ContagemResultado_SiglaSrvVnc, AV35Quantidade, A490ContagemResultado_ContratadaCod, A1480ContagemResultado_CstUntUltima, A1553ContagemResultado_CntSrvCod, AV69CstUntNrm, AV70CalculoPFinal, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A1033ContagemResultadoLiqLog_Codigo, A1043ContagemResultado_LiqLogCod, A1371ContagemResultadoLiqLogOS_OSCod, A1375ContagemResultadoLiqLogOS_Valor, AV46Selecionadas, AV48QtdeSelecionadas, AV62PFTotalB, AV64TotalBruto, AV61ValorB, AV63GlsTotal, A1051ContagemResultado_GlsValor, AV65TotalLiquido, AV81Codigo, A1373ContagemResultadoLiqLogOS_UserNom, A1034ContagemResultadoLiqLog_Data, A1372ContagemResultadoLiqLogOS_UserCod, AV76ContadorFM, AV67ValorL, AV86ReAbertas) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E21K72( )
      {
         /* 'DoExportPDF' Routine */
         if ( (0==AV36Registros) )
         {
            /* Execute user subroutine: 'PAGEUP' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            GX_msglist.addItem("Consulta sem dados para gerar o PDF!");
         }
         else if ( (0==AV48QtdeSelecionadas) )
         {
            /* Execute user subroutine: 'PAGEUP' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            GX_msglist.addItem("Selecione as OS que deseja incluir no relat�rio!");
         }
         else
         {
            AV47WebSession.Set("SdtCodigos", AV46Selecionadas.ToXml(false, true, "Collection", ""));
            if ( AV152ContagemResultado_CntCod1 > 0 )
            {
               AV156Contrato_Codigo = AV152ContagemResultado_CntCod1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156Contrato_Codigo), 6, 0)));
            }
            else if ( AV153ContagemResultado_CntCod2 > 0 )
            {
               AV156Contrato_Codigo = AV153ContagemResultado_CntCod2;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156Contrato_Codigo), 6, 0)));
            }
            else if ( AV154ContagemResultado_CntCod3 > 0 )
            {
               AV156Contrato_Codigo = AV154ContagemResultado_CntCod3;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156Contrato_Codigo), 6, 0)));
            }
            else
            {
               AV156Contrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV156Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV156Contrato_Codigo), 6, 0)));
            }
            if ( AV21ContagemResultado_ContadorFM1 > 0 )
            {
               AV157Usuario_Codigo = AV21ContagemResultado_ContadorFM1;
            }
            else if ( AV26ContagemResultado_ContadorFM2 > 0 )
            {
               AV157Usuario_Codigo = AV26ContagemResultado_ContadorFM2;
            }
            else if ( AV31ContagemResultado_ContadorFM3 > 0 )
            {
               AV157Usuario_Codigo = AV31ContagemResultado_ContadorFM3;
            }
            else
            {
               AV157Usuario_Codigo = 0;
            }
            Innewwindow_Target = formatLink("rel_pgtofnc.aspx") + "?" + UrlEncode("" +AV15Contratada_AreaTrabalhoCod) + "," + UrlEncode("" +AV16ContagemResultado_LiqLogCod) + "," + UrlEncode(StringUtil.BoolToStr(AV87Liquidadas)) + "," + UrlEncode("" +AV156Contrato_Codigo) + "," + UrlEncode("" +AV157Usuario_Codigo);
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
            this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible), 5, 0)));
         edtavContagemresultado_demanda1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda1_Visible), 5, 0)));
         dynavContagemresultado_contadorfm1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfm1.Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible), 5, 0)));
         edtavContagemresultado_agrupador1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador1_Visible), 5, 0)));
         dynavContagemresultado_cntcod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntcod1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
         {
            dynavContagemresultado_contadorfm1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfm1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 )
         {
            dynavContagemresultado_cntcod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntcod1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible), 5, 0)));
         edtavContagemresultado_demanda2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda2_Visible), 5, 0)));
         dynavContagemresultado_contadorfm2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfm2.Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible), 5, 0)));
         edtavContagemresultado_agrupador2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador2_Visible), 5, 0)));
         dynavContagemresultado_cntcod2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntcod2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
         {
            dynavContagemresultado_contadorfm2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfm2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 )
         {
            dynavContagemresultado_cntcod2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntcod2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible), 5, 0)));
         edtavContagemresultado_demanda3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda3_Visible), 5, 0)));
         dynavContagemresultado_contadorfm3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfm3.Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible), 5, 0)));
         edtavContagemresultado_agrupador3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador3_Visible), 5, 0)));
         dynavContagemresultado_cntcod3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntcod3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            edtavContagemresultado_demanda3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
         {
            dynavContagemresultado_contadorfm3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contadorfm3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 )
         {
            dynavContagemresultado_cntcod3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntcod3.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV22DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
         AV23DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DATAULTCNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
         AV24ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataUltCnt2", context.localUtil.Format(AV24ContagemResultado_DataUltCnt2, "99/99/99"));
         AV25ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_DataUltCnt_To2", context.localUtil.Format(AV25ContagemResultado_DataUltCnt_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
         AV28DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DATAULTCNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
         AV29ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataUltCnt3", context.localUtil.Format(AV29ContagemResultado_DataUltCnt3, "99/99/99"));
         AV30ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataUltCnt_To3", context.localUtil.Format(AV30ContagemResultado_DataUltCnt_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15Contratada_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0)));
         AV148ContagemResultado_ContratadaPessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), 6, 0)));
         AV16ContagemResultado_LiqLogCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
         AV102ContagemResultado_StatusDmn = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ContagemResultado_StatusDmn", AV102ContagemResultado_StatusDmn);
         AV18DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DATAULTCNT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
         AV19ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataUltCnt1", context.localUtil.Format(AV19ContagemResultado_DataUltCnt1, "99/99/99"));
         AV20ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataUltCnt_To1", context.localUtil.Format(AV20ContagemResultado_DataUltCnt_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get(AV216Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV216Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV34Session.Get(AV216Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV217GXV1 = 1;
         while ( AV217GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV217GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV15Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CONTRATADAPESSOACOD") == 0 )
            {
               AV148ContagemResultado_ContratadaPessoaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV148ContagemResultado_ContratadaPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_LIQLOGCOD") == 0 )
            {
               AV16ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV102ContagemResultado_StatusDmn = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ContagemResultado_StatusDmn", AV102ContagemResultado_StatusDmn);
            }
            AV217GXV1 = (int)(AV217GXV1+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV18DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector1", AV18DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV19ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataUltCnt1", context.localUtil.Format(AV19ContagemResultado_DataUltCnt1, "99/99/99"));
               AV20ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataUltCnt_To1", context.localUtil.Format(AV20ContagemResultado_DataUltCnt_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV89DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0)));
               AV77ContagemResultado_Demanda1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ContagemResultado_Demanda1", AV77ContagemResultado_Demanda1);
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV21ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_ContadorFM1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 )
            {
               AV164ContagemResultadoLiqLog_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV164ContagemResultadoLiqLog_Data1", context.localUtil.Format(AV164ContagemResultadoLiqLog_Data1, "99/99/99"));
               AV165ContagemResultadoLiqLog_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV165ContagemResultadoLiqLog_Data_To1", context.localUtil.Format(AV165ContagemResultadoLiqLog_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV55ContagemResultado_Agrupador1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ContagemResultado_Agrupador1", AV55ContagemResultado_Agrupador1);
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 )
            {
               AV152ContagemResultado_CntCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV152ContagemResultado_CntCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV22DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled2", AV22DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV23DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector2", AV23DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV24ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_DataUltCnt2", context.localUtil.Format(AV24ContagemResultado_DataUltCnt2, "99/99/99"));
                  AV25ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_DataUltCnt_To2", context.localUtil.Format(AV25ContagemResultado_DataUltCnt_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
               {
                  AV90DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0)));
                  AV78ContagemResultado_Demanda2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ContagemResultado_Demanda2", AV78ContagemResultado_Demanda2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV26ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContagemResultado_ContadorFM2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 )
               {
                  AV166ContagemResultadoLiqLog_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV166ContagemResultadoLiqLog_Data2", context.localUtil.Format(AV166ContagemResultadoLiqLog_Data2, "99/99/99"));
                  AV167ContagemResultadoLiqLog_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV167ContagemResultadoLiqLog_Data_To2", context.localUtil.Format(AV167ContagemResultadoLiqLog_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV56ContagemResultado_Agrupador2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ContagemResultado_Agrupador2", AV56ContagemResultado_Agrupador2);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 )
               {
                  AV153ContagemResultado_CntCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV153ContagemResultado_CntCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV27DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersEnabled3", AV27DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV28DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersSelector3", AV28DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV29ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DataUltCnt3", context.localUtil.Format(AV29ContagemResultado_DataUltCnt3, "99/99/99"));
                     AV30ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataUltCnt_To3", context.localUtil.Format(AV30ContagemResultado_DataUltCnt_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                  {
                     AV91DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0)));
                     AV79ContagemResultado_Demanda3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79ContagemResultado_Demanda3", AV79ContagemResultado_Demanda3);
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV31ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_ContadorFM3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 )
                  {
                     AV168ContagemResultadoLiqLog_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV168ContagemResultadoLiqLog_Data3", context.localUtil.Format(AV168ContagemResultadoLiqLog_Data3, "99/99/99"));
                     AV169ContagemResultadoLiqLog_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV169ContagemResultadoLiqLog_Data_To3", context.localUtil.Format(AV169ContagemResultadoLiqLog_Data_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV57ContagemResultado_Agrupador3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ContagemResultado_Agrupador3", AV57ContagemResultado_Agrupador3);
                  }
                  else if ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                  {
                     AV154ContagemResultado_CntCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV154ContagemResultado_CntCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV34Session.Get(AV216Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV15Contratada_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV148ContagemResultado_ContratadaPessoaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CONTRATADAPESSOACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV16ContagemResultado_LiqLogCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_LIQLOGCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV102ContagemResultado_StatusDmn)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_STATUSDMN";
            AV11GridStateFilterValue.gxTpr_Value = AV102ContagemResultado_StatusDmn;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV216Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 ) && ! ( (DateTime.MinValue==AV19ContagemResultado_DataUltCnt1) && (DateTime.MinValue==AV20ContagemResultado_DataUltCnt_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV19ContagemResultado_DataUltCnt1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV20ContagemResultado_DataUltCnt_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV77ContagemResultado_Demanda1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV77ContagemResultado_Demanda1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV89DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 ) && ! (0==AV21ContagemResultado_ContadorFM1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ! ( (DateTime.MinValue==AV164ContagemResultadoLiqLog_Data1) && (DateTime.MinValue==AV165ContagemResultadoLiqLog_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV164ContagemResultadoLiqLog_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV165ContagemResultadoLiqLog_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_Agrupador1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV55ContagemResultado_Agrupador1;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ! (0==AV152ContagemResultado_CntCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0);
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 ) && ! ( (DateTime.MinValue==AV24ContagemResultado_DataUltCnt2) && (DateTime.MinValue==AV25ContagemResultado_DataUltCnt_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24ContagemResultado_DataUltCnt2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25ContagemResultado_DataUltCnt_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ContagemResultado_Demanda2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV78ContagemResultado_Demanda2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV90DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 ) && ! (0==AV26ContagemResultado_ContadorFM2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ! ( (DateTime.MinValue==AV166ContagemResultadoLiqLog_Data2) && (DateTime.MinValue==AV167ContagemResultadoLiqLog_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV166ContagemResultadoLiqLog_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV167ContagemResultadoLiqLog_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_Agrupador2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV56ContagemResultado_Agrupador2;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ! (0==AV153ContagemResultado_CntCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0);
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV27DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV28DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 ) && ! ( (DateTime.MinValue==AV29ContagemResultado_DataUltCnt3) && (DateTime.MinValue==AV30ContagemResultado_DataUltCnt_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV29ContagemResultado_DataUltCnt3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV30ContagemResultado_DataUltCnt_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV79ContagemResultado_Demanda3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV79ContagemResultado_Demanda3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV91DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 ) && ! (0==AV31ContagemResultado_ContadorFM3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0);
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ! ( (DateTime.MinValue==AV168ContagemResultadoLiqLog_Data3) && (DateTime.MinValue==AV169ContagemResultadoLiqLog_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV168ContagemResultadoLiqLog_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV169ContagemResultadoLiqLog_Data_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemResultado_Agrupador3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV57ContagemResultado_Agrupador3;
            }
            else if ( ( StringUtil.StrCmp(AV28DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ! (0==AV154ContagemResultado_CntCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0);
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV216Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultado";
         AV34Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E27K72( )
      {
         /* Liquidadas_Click Routine */
         if ( AV87Liquidadas )
         {
            AV16ContagemResultado_LiqLogCod = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
         }
         else
         {
            AV16ContagemResultado_LiqLogCod = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0)));
         }
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Filtros alterados. Atualize a consulta antes de gerar o PDF!";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
      }

      protected void E12K72( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Title, "Aten��o") == 0 )
         {
            context.wjLoc = formatLink("wwpbaseobjects.home.aspx") ;
            context.wjLocDisableFrm = 1;
         }
      }

      protected void E37K72( )
      {
         /* Selected_Click Routine */
         if ( ( AV67ValorL > Convert.ToDecimal( 0 )) )
         {
            if ( AV41Selected )
            {
               AV46Selecionadas.Add(A456ContagemResultado_Codigo, 0);
               AV48QtdeSelecionadas = (short)(AV48QtdeSelecionadas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
               AV62PFTotalB = (decimal)(AV62PFTotalB+AV66PFB);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PFTotalB", StringUtil.LTrim( StringUtil.Str( AV62PFTotalB, 14, 5)));
               AV64TotalBruto = (decimal)(AV64TotalBruto+AV61ValorB);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
               AV63GlsTotal = (decimal)(AV63GlsTotal+A1051ContagemResultado_GlsValor);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
               AV65TotalLiquido = (decimal)(AV65TotalLiquido+AV67ValorL);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
            }
            else
            {
               AV46Selecionadas.RemoveItem(AV46Selecionadas.IndexOf(A456ContagemResultado_Codigo));
               AV48QtdeSelecionadas = (short)(AV48QtdeSelecionadas-1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48QtdeSelecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48QtdeSelecionadas), 4, 0)));
               AV62PFTotalB = (decimal)(AV62PFTotalB-AV66PFB);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62PFTotalB", StringUtil.LTrim( StringUtil.Str( AV62PFTotalB, 14, 5)));
               AV64TotalBruto = (decimal)(AV64TotalBruto-AV61ValorB);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TotalBruto", StringUtil.LTrim( StringUtil.Str( AV64TotalBruto, 18, 5)));
               AV63GlsTotal = (decimal)(AV63GlsTotal-A1051ContagemResultado_GlsValor);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GlsTotal", StringUtil.LTrim( StringUtil.Str( AV63GlsTotal, 18, 5)));
               AV65TotalLiquido = (decimal)(AV65TotalLiquido-AV67ValorL);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TotalLiquido", StringUtil.LTrim( StringUtil.Str( AV65TotalLiquido, 18, 5)));
            }
         }
         else
         {
            AV41Selected = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV41Selected);
            AV46Selecionadas.RemoveItem(AV46Selecionadas.IndexOf(A456ContagemResultado_Codigo));
            /* Execute user subroutine: 'PAGEUP' */
            S242 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            GX_msglist.addItem("Esta OS n�o tem valores para liquidar!");
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Selecionadas", AV46Selecionadas);
      }

      protected void E28K72( )
      {
         /* Contagemresultado_contadorfm1_Click Routine */
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Filtros alterados. Atualize a consulta antes de gerar o PDF!";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
      }

      protected void E29K72( )
      {
         /* Contagemresultado_contadorfm2_Click Routine */
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Filtros alterados. Atualize a consulta antes de gerar o PDF!";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
      }

      protected void E30K72( )
      {
         /* Contagemresultado_contadorfm3_Click Routine */
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Filtros alterados. Atualize a consulta antes de gerar o PDF!";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
      }

      protected void E31K72( )
      {
         /* Contagemresultado_cntcod1_Click Routine */
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Filtros alterados. Atualize a consulta antes de gerar o PDF!";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
      }

      protected void E32K72( )
      {
         /* Contagemresultado_cntcod2_Click Routine */
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Filtros alterados. Atualize a consulta antes de gerar o PDF!";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
      }

      protected void E33K72( )
      {
         /* Contagemresultado_cntcod3_Click Routine */
         imgExportpdf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExportpdf_Enabled), 5, 0)));
         imgExportpdf_Tooltiptext = "Filtros alterados. Atualize a consulta antes de gerar o PDF!";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExportpdf_Internalname, "Tooltiptext", imgExportpdf_Tooltiptext);
      }

      protected void S192( )
      {
         /* 'PAGAMENTOSREALIZADOS' Routine */
         AV82ToolTipText = "";
         AV218GXLvl1107 = 0;
         /* Using cursor H00K715 */
         pr_default.execute(12, new Object[] {AV81Codigo});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A1033ContagemResultadoLiqLog_Codigo = H00K715_A1033ContagemResultadoLiqLog_Codigo[0];
            A1374ContagemResultadoLiqLogOS_UserPesCod = H00K715_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
            n1374ContagemResultadoLiqLogOS_UserPesCod = H00K715_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
            A1371ContagemResultadoLiqLogOS_OSCod = H00K715_A1371ContagemResultadoLiqLogOS_OSCod[0];
            A1375ContagemResultadoLiqLogOS_Valor = H00K715_A1375ContagemResultadoLiqLogOS_Valor[0];
            A1034ContagemResultadoLiqLog_Data = H00K715_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = H00K715_n1034ContagemResultadoLiqLog_Data[0];
            A1373ContagemResultadoLiqLogOS_UserNom = H00K715_A1373ContagemResultadoLiqLogOS_UserNom[0];
            n1373ContagemResultadoLiqLogOS_UserNom = H00K715_n1373ContagemResultadoLiqLogOS_UserNom[0];
            A1372ContagemResultadoLiqLogOS_UserCod = H00K715_A1372ContagemResultadoLiqLogOS_UserCod[0];
            A1034ContagemResultadoLiqLog_Data = H00K715_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = H00K715_n1034ContagemResultadoLiqLog_Data[0];
            A1374ContagemResultadoLiqLogOS_UserPesCod = H00K715_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
            n1374ContagemResultadoLiqLogOS_UserPesCod = H00K715_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
            A1373ContagemResultadoLiqLogOS_UserNom = H00K715_A1373ContagemResultadoLiqLogOS_UserNom[0];
            n1373ContagemResultadoLiqLogOS_UserNom = H00K715_n1373ContagemResultadoLiqLogOS_UserNom[0];
            AV218GXLvl1107 = 1;
            edtavWarning_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning_Visible), 5, 0)));
            AV82ToolTipText = AV82ToolTipText + StringUtil.Trim( A1373ContagemResultadoLiqLogOS_UserNom) + ": " + context.localUtil.DToC( DateTimeUtil.ResetTime( A1034ContagemResultadoLiqLog_Data), 2, "/") + " R$ " + StringUtil.Trim( StringUtil.Str( A1375ContagemResultadoLiqLogOS_Valor, 18, 5)) + " ";
            if ( A1372ContagemResultadoLiqLogOS_UserCod == AV76ContadorFM )
            {
               AV67ValorL = (decimal)(AV67ValorL-A1375ContagemResultadoLiqLogOS_Valor);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavValorl_Internalname, StringUtil.LTrim( StringUtil.Str( AV67ValorL, 18, 5)));
            }
            pr_default.readNext(12);
         }
         pr_default.close(12);
         if ( AV218GXLvl1107 == 0 )
         {
            edtavWarning_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning_Visible), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82ToolTipText)) )
         {
            edtavWarning_Tooltiptext = AV82ToolTipText;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning_Internalname, "Tooltiptext", edtavWarning_Tooltiptext);
            AV86ReAbertas = (short)(AV86ReAbertas+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ReAbertas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86ReAbertas), 4, 0)));
         }
      }

      protected void S182( )
      {
         /* 'OUTROSPAGAMENTOS' Routine */
         AV82ToolTipText = "";
         AV219GXLvl1127 = 0;
         /* Using cursor H00K716 */
         pr_default.execute(13, new Object[] {AV81Codigo, AV16ContagemResultado_LiqLogCod});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A1372ContagemResultadoLiqLogOS_UserCod = H00K716_A1372ContagemResultadoLiqLogOS_UserCod[0];
            A1374ContagemResultadoLiqLogOS_UserPesCod = H00K716_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
            n1374ContagemResultadoLiqLogOS_UserPesCod = H00K716_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
            A1033ContagemResultadoLiqLog_Codigo = H00K716_A1033ContagemResultadoLiqLog_Codigo[0];
            A1371ContagemResultadoLiqLogOS_OSCod = H00K716_A1371ContagemResultadoLiqLogOS_OSCod[0];
            A1375ContagemResultadoLiqLogOS_Valor = H00K716_A1375ContagemResultadoLiqLogOS_Valor[0];
            A1034ContagemResultadoLiqLog_Data = H00K716_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = H00K716_n1034ContagemResultadoLiqLog_Data[0];
            A1373ContagemResultadoLiqLogOS_UserNom = H00K716_A1373ContagemResultadoLiqLogOS_UserNom[0];
            n1373ContagemResultadoLiqLogOS_UserNom = H00K716_n1373ContagemResultadoLiqLogOS_UserNom[0];
            A1374ContagemResultadoLiqLogOS_UserPesCod = H00K716_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
            n1374ContagemResultadoLiqLogOS_UserPesCod = H00K716_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
            A1373ContagemResultadoLiqLogOS_UserNom = H00K716_A1373ContagemResultadoLiqLogOS_UserNom[0];
            n1373ContagemResultadoLiqLogOS_UserNom = H00K716_n1373ContagemResultadoLiqLogOS_UserNom[0];
            A1034ContagemResultadoLiqLog_Data = H00K716_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = H00K716_n1034ContagemResultadoLiqLog_Data[0];
            AV219GXLvl1127 = 1;
            edtavWarning_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning_Visible), 5, 0)));
            AV82ToolTipText = AV82ToolTipText + StringUtil.Trim( A1373ContagemResultadoLiqLogOS_UserNom) + ": " + context.localUtil.DToC( DateTimeUtil.ResetTime( A1034ContagemResultadoLiqLog_Data), 2, "/") + " R$ " + StringUtil.Trim( StringUtil.Str( A1375ContagemResultadoLiqLogOS_Valor, 18, 5)) + " ";
            pr_default.readNext(13);
         }
         pr_default.close(13);
         if ( AV219GXLvl1127 == 0 )
         {
            edtavWarning_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWarning_Visible), 5, 0)));
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82ToolTipText)) )
         {
            edtavWarning_Tooltiptext = AV82ToolTipText;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWarning_Internalname, "Tooltiptext", edtavWarning_Tooltiptext);
         }
      }

      protected void S172( )
      {
         /* 'PRECONSULTA' Routine */
         AV36Registros = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Registros), 8, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREGISTROS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")));
         AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod = AV15Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod = AV148ContagemResultado_ContratadaPessoaCod;
         AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod = AV16ContagemResultado_LiqLogCod;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = AV102ContagemResultado_StatusDmn;
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 = AV89DynamicFiltersOperator1;
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = AV19ContagemResultado_DataUltCnt1;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = AV20ContagemResultado_DataUltCnt_To1;
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = AV77ContagemResultado_Demanda1;
         AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = AV21ContagemResultado_ContadorFM1;
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = AV164ContagemResultadoLiqLog_Data1;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = AV165ContagemResultadoLiqLog_Data_To1;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = AV55ContagemResultado_Agrupador1;
         AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 = AV152ContagemResultado_CntCod1;
         AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = AV22DynamicFiltersEnabled2;
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = AV23DynamicFiltersSelector2;
         AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 = AV90DynamicFiltersOperator2;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = AV24ContagemResultado_DataUltCnt2;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = AV25ContagemResultado_DataUltCnt_To2;
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = AV78ContagemResultado_Demanda2;
         AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = AV26ContagemResultado_ContadorFM2;
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = AV166ContagemResultadoLiqLog_Data2;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = AV167ContagemResultadoLiqLog_Data_To2;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = AV56ContagemResultado_Agrupador2;
         AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 = AV153ContagemResultado_CntCod2;
         AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = AV27DynamicFiltersEnabled3;
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = AV28DynamicFiltersSelector3;
         AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 = AV91DynamicFiltersOperator3;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = AV29ContagemResultado_DataUltCnt3;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = AV30ContagemResultado_DataUltCnt_To3;
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = AV79ContagemResultado_Demanda3;
         AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = AV31ContagemResultado_ContadorFM3;
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = AV168ContagemResultadoLiqLog_Data3;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = AV169ContagemResultadoLiqLog_Data_To3;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = AV57ContagemResultado_Agrupador3;
         AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 = AV154ContagemResultado_CntCod3;
         pr_default.dynParam(14, new Object[]{ new Object[]{
                                              AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod ,
                                              AV87Liquidadas ,
                                              AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 ,
                                              AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 ,
                                              AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 ,
                                              AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 ,
                                              AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 ,
                                              AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 ,
                                              AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 ,
                                              AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 ,
                                              AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 ,
                                              AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 ,
                                              AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 ,
                                              AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 ,
                                              AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 ,
                                              AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 ,
                                              AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 ,
                                              AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 ,
                                              AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 ,
                                              AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 ,
                                              AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 ,
                                              AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 ,
                                              AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 ,
                                              AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 ,
                                              AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 ,
                                              AV10GridState.gxTpr_Dynamicfilters.Count ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A1043ContagemResultado_LiqLogCod ,
                                              A457ContagemResultado_Demanda ,
                                              A1034ContagemResultadoLiqLog_Data ,
                                              A1046ContagemResultado_Agrupador ,
                                              A1603ContagemResultado_CntCod ,
                                              A456ContagemResultado_Codigo ,
                                              A484ContagemResultado_StatusDmn ,
                                              A1600ContagemResultado_CntSrvSttPgmFnc ,
                                              A1854ContagemResultado_VlrCnc ,
                                              AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 ,
                                              AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 ,
                                              AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 ,
                                              AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 ,
                                              AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 ,
                                              AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 ,
                                              AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 ,
                                              AV6WWPContext.gxTpr_Userehfinanceiro ,
                                              AV6WWPContext.gxTpr_Userid ,
                                              A499ContagemResultado_ContratadaPessoaCod ,
                                              AV6WWPContext.gxTpr_Contratada_pessoacod ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1), "%", "");
         lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = StringUtil.Concat( StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1), "%", "");
         lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1), 15, "%");
         lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2), "%", "");
         lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2), "%", "");
         lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2), 15, "%");
         lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3), "%", "");
         lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3), "%", "");
         lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3), 15, "%");
         /* Using cursor H00K718 */
         pr_default.execute(14, new Object[] {AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1, AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1, AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1, AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1, AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1, AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1, AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1, AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2, AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2, AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2, AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2, AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2, AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2, AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2, AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2, AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2, AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2, AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3, AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3, AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3, AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3, AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3, AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3, AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3, AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3, AV6WWPContext.gxTpr_Userehfinanceiro, AV6WWPContext.gxTpr_Userid, AV6WWPContext.gxTpr_Contratada_pessoacod, AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod, AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1, lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1, lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1, AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1, AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1, lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1, AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1, AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2, lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2, lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2, AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2, AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2, lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2, AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2, AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3, lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3, lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3, AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3, AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3, lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3, AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A490ContagemResultado_ContratadaCod = H00K718_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00K718_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = H00K718_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00K718_n1553ContagemResultado_CntSrvCod[0];
            A499ContagemResultado_ContratadaPessoaCod = H00K718_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00K718_n499ContagemResultado_ContratadaPessoaCod[0];
            A1583ContagemResultado_TipoRegistro = H00K718_A1583ContagemResultado_TipoRegistro[0];
            A456ContagemResultado_Codigo = H00K718_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = H00K718_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00K718_n1603ContagemResultado_CntCod[0];
            A1046ContagemResultado_Agrupador = H00K718_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = H00K718_n1046ContagemResultado_Agrupador[0];
            A1034ContagemResultadoLiqLog_Data = H00K718_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = H00K718_n1034ContagemResultadoLiqLog_Data[0];
            A508ContagemResultado_Owner = H00K718_A508ContagemResultado_Owner[0];
            A457ContagemResultado_Demanda = H00K718_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00K718_n457ContagemResultado_Demanda[0];
            A1854ContagemResultado_VlrCnc = H00K718_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = H00K718_n1854ContagemResultado_VlrCnc[0];
            A1600ContagemResultado_CntSrvSttPgmFnc = H00K718_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = H00K718_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            A484ContagemResultado_StatusDmn = H00K718_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00K718_n484ContagemResultado_StatusDmn[0];
            A1043ContagemResultado_LiqLogCod = H00K718_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = H00K718_n1043ContagemResultado_LiqLogCod[0];
            A52Contratada_AreaTrabalhoCod = H00K718_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00K718_n52Contratada_AreaTrabalhoCod[0];
            A584ContagemResultado_ContadorFM = H00K718_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = H00K718_A566ContagemResultado_DataUltCnt[0];
            A499ContagemResultado_ContratadaPessoaCod = H00K718_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00K718_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = H00K718_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00K718_n52Contratada_AreaTrabalhoCod[0];
            A1603ContagemResultado_CntCod = H00K718_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00K718_n1603ContagemResultado_CntCod[0];
            A1600ContagemResultado_CntSrvSttPgmFnc = H00K718_A1600ContagemResultado_CntSrvSttPgmFnc[0];
            n1600ContagemResultado_CntSrvSttPgmFnc = H00K718_n1600ContagemResultado_CntSrvSttPgmFnc[0];
            A584ContagemResultado_ContadorFM = H00K718_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = H00K718_A566ContagemResultado_DataUltCnt[0];
            A1034ContagemResultadoLiqLog_Data = H00K718_A1034ContagemResultadoLiqLog_Data[0];
            n1034ContagemResultadoLiqLog_Data = H00K718_n1034ContagemResultadoLiqLog_Data[0];
            if ( ! ( ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADOLIQLOG_DATA") != 0 ) && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADOLIQLOG_DATA") != 0 ) && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADOLIQLOG_DATA") != 0 ) ) || ( (DateTime.MinValue==AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1) && (DateTime.MinValue==AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1) && (DateTime.MinValue==AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2) && (DateTime.MinValue==AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2) && (DateTime.MinValue==AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3) && (DateTime.MinValue==AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3) ) ) || ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, A1600ContagemResultado_CntSrvSttPgmFnc) == 0 ) || ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) && ( A1854ContagemResultado_VlrCnc > Convert.ToDecimal( 0 )) ) || ( new prc_prontoparaliqfnc(context).executeUdp(  A484ContagemResultado_StatusDmn,  A1600ContagemResultado_CntSrvSttPgmFnc) ) ) )
            {
               AV36Registros = (int)(AV36Registros+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Registros), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREGISTROS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")));
            }
            pr_default.readNext(14);
         }
         pr_default.close(14);
         GXt_int2 = AV146GridPageCount;
         new prc_pagecount(context ).execute(  AV36Registros, ref  AV84PreView, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Registros), 8, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vREGISTROS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84PreView", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84PreView), 4, 0)));
         AV146GridPageCount = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV146GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV146GridPageCount), 10, 0)));
         if ( ( AV84PreView > 0 ) && ( AV36Registros > AV84PreView ) )
         {
            bttBtnliquidar_Tooltiptext = "Use 'Mostrar tudo' para poder liquidar as OS selecionadas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnliquidar_Internalname, "Tooltiptext", bttBtnliquidar_Tooltiptext);
            bttBtnliquidar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnliquidar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnliquidar_Enabled), 5, 0)));
            bttBtnmostrartudo_Visible = (!AV87Liquidadas ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnmostrartudo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnmostrartudo_Visible), 5, 0)));
            if ( AV36Registros > AV83LimiteRegistros )
            {
               bttBtnmostrartudo_Tooltiptext = "Sua consulta com "+StringUtil.Trim( StringUtil.Str( (decimal)(AV36Registros), 8, 0))+" registros ser� bloqueada!";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnmostrartudo_Internalname, "Tooltiptext", bttBtnmostrartudo_Tooltiptext);
            }
            else
            {
               bttBtnmostrartudo_Tooltiptext = "Mostrar todos os "+StringUtil.Trim( StringUtil.Str( (decimal)(AV36Registros), 8, 0))+" registros.";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnmostrartudo_Internalname, "Tooltiptext", bttBtnmostrartudo_Tooltiptext);
            }
         }
         else
         {
            bttBtnliquidar_Tooltiptext = "Liquidar as OS selecionadas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnliquidar_Internalname, "Tooltiptext", bttBtnliquidar_Tooltiptext);
            bttBtnliquidar_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnliquidar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnliquidar_Enabled), 5, 0)));
            bttBtnmostrartudo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnmostrartudo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnmostrartudo_Visible), 5, 0)));
         }
      }

      protected void S242( )
      {
         /* 'PAGEUP' Routine */
         GX_FocusControl = cmbavOrderedby_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         GX_FocusControl = subGrid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
      }

      protected void wb_table1_2_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_K72( true) ;
         }
         else
         {
            wb_table2_8_K72( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_186_K72( true) ;
         }
         else
         {
            wb_table3_186_K72( false) ;
         }
         return  ;
      }

      protected void wb_table3_186_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_263_K72( true) ;
         }
         else
         {
            wb_table4_263_K72( false) ;
         }
         return  ;
      }

      protected void wb_table4_263_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_268_K72( true) ;
         }
         else
         {
            wb_table5_268_K72( false) ;
         }
         return  ;
      }

      protected void wb_table5_268_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_K72e( true) ;
         }
         else
         {
            wb_table1_2_K72e( false) ;
         }
      }

      protected void wb_table5_268_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
            context.WriteHtmlText( "</p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_268_K72e( true) ;
         }
         else
         {
            wb_table5_268_K72e( false) ;
         }
      }

      protected void wb_table4_263_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_263_K72e( true) ;
         }
         else
         {
            wb_table4_263_K72e( false) ;
         }
      }

      protected void wb_table3_186_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table6_189_K72( true) ;
         }
         else
         {
            wb_table6_189_K72( false) ;
         }
         return  ;
      }

      protected void wb_table6_189_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_223_K72( true) ;
         }
         else
         {
            wb_table7_223_K72( false) ;
         }
         return  ;
      }

      protected void wb_table7_223_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table8_228_K72( true) ;
         }
         else
         {
            wb_table8_228_K72( false) ;
         }
         return  ;
      }

      protected void wb_table8_228_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_186_K72e( true) ;
         }
         else
         {
            wb_table3_186_K72e( false) ;
         }
      }

      protected void wb_table8_228_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockquantidade_Internalname, "Qtd: ", "", "", lblTextblockquantidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table9_233_K72( true) ;
         }
         else
         {
            wb_table9_233_K72( false) ;
         }
         return  ;
      }

      protected void wb_table9_233_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockqtdeselecionadas_Internalname, "Sel:", "", "", lblTextblockqtdeselecionadas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 244,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtdeselecionadas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48QtdeSelecionadas), 4, 0, ",", "")), ((edtavQtdeselecionadas_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48QtdeSelecionadas), "ZZZ9")) : context.localUtil.Format( (decimal)(AV48QtdeSelecionadas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,244);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtdeselecionadas_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavQtdeselecionadas_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpftotalb_Internalname, "PF Total: ", "", "", lblTextblockpftotalb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 248,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPftotalb_Internalname, StringUtil.LTrim( StringUtil.NToC( AV62PFTotalB, 14, 5, ",", "")), ((edtavPftotalb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV62PFTotalB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV62PFTotalB, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,248);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPftotalb_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavPftotalb_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktotalbruto_Internalname, "Bruto R$: ", "", "", lblTextblocktotalbruto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 252,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTotalbruto_Internalname, StringUtil.LTrim( StringUtil.NToC( AV64TotalBruto, 18, 5, ",", "")), ((edtavTotalbruto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV64TotalBruto, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV64TotalBruto, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,252);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTotalbruto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTotalbruto_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglstotal_Internalname, "Glosas R$:", "", "", lblTextblockglstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 256,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGlstotal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV63GlsTotal, 18, 5, ",", "")), ((edtavGlstotal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV63GlsTotal, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV63GlsTotal, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,256);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGlstotal_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavGlstotal_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktotalliquido_Internalname, "Liquido R$", "", "", lblTextblocktotalliquido_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 260,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTotalliquido_Internalname, StringUtil.LTrim( StringUtil.NToC( AV65TotalLiquido, 18, 5, ",", "")), ((edtavTotalliquido_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV65TotalLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV65TotalLiquido, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,260);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTotalliquido_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavTotalliquido_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_228_K72e( true) ;
         }
         else
         {
            wb_table8_228_K72e( false) ;
         }
      }

      protected void wb_table9_233_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedquantidade_Internalname, tblTablemergedquantidade_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 236,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQuantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Quantidade), 4, 0, ",", "")), ((edtavQuantidade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35Quantidade), "ZZZ9")) : context.localUtil.Format( (decimal)(AV35Quantidade), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,236);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQuantidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavQuantidade_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockregistros_Internalname, "��de", "", "", lblTextblockregistros_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 240,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegistros_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Registros), 8, 0, ",", "")), ((edtavRegistros_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV36Registros), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,240);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegistros_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavRegistros_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_233_K72e( true) ;
         }
         else
         {
            wb_table9_233_K72e( false) ;
         }
      }

      protected void wb_table7_223_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 2, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 226,'',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnliquidar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(195), 3, 0)+","+"null"+");", "Liquidar", bttBtnliquidar_Jsonclick, 5, bttBtnliquidar_Tooltiptext, "", StyleString, ClassString, 1, bttBtnliquidar_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOLIQUIDAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_223_K72e( true) ;
         }
         else
         {
            wb_table7_223_K72e( false) ;
         }
      }

      protected void wb_table6_189_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_192_K72( true) ;
         }
         else
         {
            wb_table10_192_K72( false) ;
         }
         return  ;
      }

      protected void wb_table10_192_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_189_K72e( true) ;
         }
         else
         {
            wb_table6_189_K72e( false) ;
         }
      }

      protected void wb_table10_192_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"195\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((chkavSelected.Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "PF Final") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Liq Log Cod") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtContratada_AreaTrabalhoDes_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "�rea") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(85), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Agrupador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS Refer�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Vnc") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Contagem") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContagemResultado_StatusDmn_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContagemResultado_StatusDmn.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContagemResultado_StatusDmn.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtContagemResultadoLiqLog_Data_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               if ( edtContagemResultadoLiqLog_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoLiqLog_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoLiqLog_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Funcion�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF Bruto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "R$ Bruto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "R$ Glosa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "R$ Liquido") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(20), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavWarning_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV41Selected));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavSelected.Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A825ContagemResultado_HoraUltCnt));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A53Contratada_AreaTrabalhoDes);
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_AreaTrabalhoDes_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1046ContagemResultado_Agrupador));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A457ContagemResultado_Demanda);
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultado_Demanda_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtContagemResultado_Demanda_Linktarget));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV42VinculadaCom));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVinculadacom_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavVinculadacom_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavVinculadacom_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A566ContagemResultado_DataUltCnt, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContagemResultado_StatusDmn.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContagemResultado_StatusDmn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoLiqLog_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLog_Data_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoLiqLog_Data_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A584ContagemResultado_ContadorFM), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV66PFB, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfb_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV61ValorB, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValorb_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1051ContagemResultado_GlsValor, 18, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV67ValorL, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavValorl_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV80Warning));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavWarning_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavWarning_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 195 )
         {
            wbEnd = 0;
            nRC_GXsfl_195 = (short)(nGXsfl_195_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_192_K72e( true) ;
         }
         else
         {
            wb_table10_192_K72e( false) ;
         }
      }

      protected void wb_table2_8_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Extrato de Colaboradores", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_13_K72( true) ;
         }
         else
         {
            wb_table11_13_K72( false) ;
         }
         return  ;
      }

      protected void wb_table11_13_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table12_23_K72( true) ;
         }
         else
         {
            wb_table12_23_K72( false) ;
         }
         return  ;
      }

      protected void wb_table12_23_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_K72e( true) ;
         }
         else
         {
            wb_table2_8_K72e( false) ;
         }
      }

      protected void wb_table12_23_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_26_K72( true) ;
         }
         else
         {
            wb_table13_26_K72( false) ;
         }
         return  ;
      }

      protected void wb_table13_26_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table14_163_K72( true) ;
         }
         else
         {
            wb_table14_163_K72( false) ;
         }
         return  ;
      }

      protected void wb_table14_163_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_23_K72e( true) ;
         }
         else
         {
            wb_table12_23_K72e( false) ;
         }
      }

      protected void wb_table14_163_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktodasasareas_Internalname, "", "", "", lblTextblocktodasasareas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table15_168_K72( true) ;
         }
         else
         {
            wb_table15_168_K72( false) ;
         }
         return  ;
      }

      protected void wb_table15_168_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockliquidadas_Internalname, "", "", "", lblTextblockliquidadas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table16_177_K72( true) ;
         }
         else
         {
            wb_table16_177_K72( false) ;
         }
         return  ;
      }

      protected void wb_table16_177_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_163_K72e( true) ;
         }
         else
         {
            wb_table14_163_K72e( false) ;
         }
      }

      protected void wb_table16_177_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedliquidadas_Internalname, tblTablemergedliquidadas_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 180,'',false,'" + sGXsfl_195_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavLiquidadas_Internalname, StringUtil.BoolToStr( AV87Liquidadas), "", "", 1, 1, "True", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(180, this, 'True', 'False');gx.ajax.executeCliEvent('e27k72_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,180);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLiquidadas_righttext_Internalname, "�OS Liquidadas", "", "", lblLiquidadas_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_177_K72e( true) ;
         }
         else
         {
            wb_table16_177_K72e( false) ;
         }
      }

      protected void wb_table15_168_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedtodasasareas_Internalname, tblTablemergedtodasasareas_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'" + sGXsfl_195_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavTodasasareas_Internalname, StringUtil.BoolToStr( AV45TodasAsAreas), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(171, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,171);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTodasasareas_righttext_Internalname, "�Todas as �reas de Trabalho", "", "", lblTodasasareas_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_168_K72e( true) ;
         }
         else
         {
            wb_table15_168_K72e( false) ;
         }
      }

      protected void wb_table13_26_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable6_Internalname, tblUnnamedtable6_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_areatrabalhocod_Internalname, "", "", "", lblFiltertextcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Contratada_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15Contratada_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_contratadapessoacod_Internalname, "Contratada", "", "", lblFiltertextcontagemresultado_contratadapessoacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_contratadapessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV148ContagemResultado_ContratadaPessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_contratadapessoacod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_liqlogcod_Internalname, "Log Cod", "", "", lblFiltertextcontagemresultado_liqlogcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_liqlogcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ContagemResultado_LiqLogCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16ContagemResultado_LiqLogCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_liqlogcod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_statusdmn_Internalname, "Status", "", "", lblFiltertextcontagemresultado_statusdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn, cmbavContagemresultado_statusdmn_Internalname, StringUtil.RTrim( AV102ContagemResultado_StatusDmn), 1, cmbavContagemresultado_statusdmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavContagemresultado_statusdmn.CurrentValue = StringUtil.RTrim( AV102ContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table17_47_K72( true) ;
         }
         else
         {
            wb_table17_47_K72( false) ;
         }
         return  ;
      }

      protected void wb_table17_47_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnatualizar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(195), 3, 0)+","+"null"+");", "Procurar", bttBtnatualizar_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOATUALIZAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnmostrartudo_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(195), 3, 0)+","+"null"+");", "Mostrar tudo", bttBtnmostrartudo_Jsonclick, 5, bttBtnmostrartudo_Tooltiptext, "", StyleString, ClassString, bttBtnmostrartudo_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOMOSTRARTUDO\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_26_K72e( true) ;
         }
         else
         {
            wb_table13_26_K72e( false) ;
         }
      }

      protected void wb_table17_47_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table18_56_K72( true) ;
         }
         else
         {
            wb_table18_56_K72( false) ;
         }
         return  ;
      }

      protected void wb_table18_56_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table19_92_K72( true) ;
         }
         else
         {
            wb_table19_92_K72( false) ;
         }
         return  ;
      }

      protected void wb_table19_92_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV28DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV28DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table20_128_K72( true) ;
         }
         else
         {
            wb_table20_128_K72( false) ;
         }
         return  ;
      }

      protected void wb_table20_128_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_47_K72e( true) ;
         }
         else
         {
            wb_table17_47_K72e( false) ;
         }
      }

      protected void wb_table20_128_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV91DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table21_133_K72( true) ;
         }
         else
         {
            wb_table21_133_K72( false) ;
         }
         return  ;
      }

      protected void wb_table21_133_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda3_Internalname, AV79ContagemResultado_Demanda3, StringUtil.RTrim( context.localUtil.Format( AV79ContagemResultado_Demanda3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda3_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfm3, dynavContagemresultado_contadorfm3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0)), 1, dynavContagemresultado_contadorfm3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CONTADORFM3.CLICK."+"'", "int", "", dynavContagemresultado_contadorfm3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            dynavContagemresultado_contadorfm3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31ContagemResultado_ContadorFM3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm3_Internalname, "Values", (String)(dynavContagemresultado_contadorfm3.ToJavascriptSource()));
            wb_table22_143_K72( true) ;
         }
         else
         {
            wb_table22_143_K72( false) ;
         }
         return  ;
      }

      protected void wb_table22_143_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador3_Internalname, StringUtil.RTrim( AV57ContagemResultado_Agrupador3), StringUtil.RTrim( context.localUtil.Format( AV57ContagemResultado_Agrupador3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,151);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_cntcod3, dynavContagemresultado_cntcod3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0)), 1, dynavContagemresultado_cntcod3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CNTCOD3.CLICK."+"'", "int", "", dynavContagemresultado_cntcod3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            dynavContagemresultado_cntcod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV154ContagemResultado_CntCod3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod3_Internalname, "Values", (String)(dynavContagemresultado_cntcod3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table20_128_K72e( true) ;
         }
         else
         {
            wb_table20_128_K72e( false) ;
         }
      }

      protected void wb_table22_143_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Internalname, tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoliqlog_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoliqlog_data3_Internalname, context.localUtil.Format(AV168ContagemResultadoLiqLog_Data3, "99/99/99"), context.localUtil.Format( AV168ContagemResultadoLiqLog_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,146);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoliqlog_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoliqlog_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext3_Internalname, "to", "", "", lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoliqlog_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoliqlog_data_to3_Internalname, context.localUtil.Format(AV169ContagemResultadoLiqLog_Data_To3, "99/99/99"), context.localUtil.Format( AV169ContagemResultadoLiqLog_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,150);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoliqlog_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoliqlog_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table22_143_K72e( true) ;
         }
         else
         {
            wb_table22_143_K72e( false) ;
         }
      }

      protected void wb_table21_133_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt3_Internalname, context.localUtil.Format(AV29ContagemResultado_DataUltCnt3, "99/99/99"), context.localUtil.Format( AV29ContagemResultado_DataUltCnt3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt_to3_Internalname, context.localUtil.Format(AV30ContagemResultado_DataUltCnt_To3, "99/99/99"), context.localUtil.Format( AV30ContagemResultado_DataUltCnt_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table21_133_K72e( true) ;
         }
         else
         {
            wb_table21_133_K72e( false) ;
         }
      }

      protected void wb_table19_92_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV90DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table23_97_K72( true) ;
         }
         else
         {
            wb_table23_97_K72( false) ;
         }
         return  ;
      }

      protected void wb_table23_97_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda2_Internalname, AV78ContagemResultado_Demanda2, StringUtil.RTrim( context.localUtil.Format( AV78ContagemResultado_Demanda2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda2_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfm2, dynavContagemresultado_contadorfm2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0)), 1, dynavContagemresultado_contadorfm2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CONTADORFM2.CLICK."+"'", "int", "", dynavContagemresultado_contadorfm2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            dynavContagemresultado_contadorfm2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26ContagemResultado_ContadorFM2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm2_Internalname, "Values", (String)(dynavContagemresultado_contadorfm2.ToJavascriptSource()));
            wb_table24_107_K72( true) ;
         }
         else
         {
            wb_table24_107_K72( false) ;
         }
         return  ;
      }

      protected void wb_table24_107_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador2_Internalname, StringUtil.RTrim( AV56ContagemResultado_Agrupador2), StringUtil.RTrim( context.localUtil.Format( AV56ContagemResultado_Agrupador2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_cntcod2, dynavContagemresultado_cntcod2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0)), 1, dynavContagemresultado_cntcod2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CNTCOD2.CLICK."+"'", "int", "", dynavContagemresultado_cntcod2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            dynavContagemresultado_cntcod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV153ContagemResultado_CntCod2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod2_Internalname, "Values", (String)(dynavContagemresultado_cntcod2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table19_92_K72e( true) ;
         }
         else
         {
            wb_table19_92_K72e( false) ;
         }
      }

      protected void wb_table24_107_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Internalname, tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoliqlog_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoliqlog_data2_Internalname, context.localUtil.Format(AV166ContagemResultadoLiqLog_Data2, "99/99/99"), context.localUtil.Format( AV166ContagemResultadoLiqLog_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoliqlog_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoliqlog_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext2_Internalname, "to", "", "", lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoliqlog_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoliqlog_data_to2_Internalname, context.localUtil.Format(AV167ContagemResultadoLiqLog_Data_To2, "99/99/99"), context.localUtil.Format( AV167ContagemResultadoLiqLog_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoliqlog_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoliqlog_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table24_107_K72e( true) ;
         }
         else
         {
            wb_table24_107_K72e( false) ;
         }
      }

      protected void wb_table23_97_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt2_Internalname, context.localUtil.Format(AV24ContagemResultado_DataUltCnt2, "99/99/99"), context.localUtil.Format( AV24ContagemResultado_DataUltCnt2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt_to2_Internalname, context.localUtil.Format(AV25ContagemResultado_DataUltCnt_To2, "99/99/99"), context.localUtil.Format( AV25ContagemResultado_DataUltCnt_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table23_97_K72e( true) ;
         }
         else
         {
            wb_table23_97_K72e( false) ;
         }
      }

      protected void wb_table18_56_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV89DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table25_61_K72( true) ;
         }
         else
         {
            wb_table25_61_K72( false) ;
         }
         return  ;
      }

      protected void wb_table25_61_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda1_Internalname, AV77ContagemResultado_Demanda1, StringUtil.RTrim( context.localUtil.Format( AV77ContagemResultado_Demanda1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demanda1_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contadorfm1, dynavContagemresultado_contadorfm1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0)), 1, dynavContagemresultado_contadorfm1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CONTADORFM1.CLICK."+"'", "int", "", dynavContagemresultado_contadorfm1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            dynavContagemresultado_contadorfm1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21ContagemResultado_ContadorFM1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contadorfm1_Internalname, "Values", (String)(dynavContagemresultado_contadorfm1.ToJavascriptSource()));
            wb_table26_71_K72( true) ;
         }
         else
         {
            wb_table26_71_K72( false) ;
         }
         return  ;
      }

      protected void wb_table26_71_K72e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_195_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador1_Internalname, StringUtil.RTrim( AV55ContagemResultado_Agrupador1), StringUtil.RTrim( context.localUtil.Format( AV55ContagemResultado_Agrupador1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_195_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_cntcod1, dynavContagemresultado_cntcod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0)), 1, dynavContagemresultado_cntcod1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_CNTCOD1.CLICK."+"'", "int", "", dynavContagemresultado_cntcod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "", true, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            dynavContagemresultado_cntcod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV152ContagemResultado_CntCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntcod1_Internalname, "Values", (String)(dynavContagemresultado_cntcod1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_56_K72e( true) ;
         }
         else
         {
            wb_table18_56_K72e( false) ;
         }
      }

      protected void wb_table26_71_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Internalname, tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoliqlog_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoliqlog_data1_Internalname, context.localUtil.Format(AV164ContagemResultadoLiqLog_Data1, "99/99/99"), context.localUtil.Format( AV164ContagemResultadoLiqLog_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoliqlog_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoliqlog_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext1_Internalname, "to", "", "", lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoliqlog_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoliqlog_data_to1_Internalname, context.localUtil.Format(AV165ContagemResultadoLiqLog_Data_To1, "99/99/99"), context.localUtil.Format( AV165ContagemResultadoLiqLog_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoliqlog_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoliqlog_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table26_71_K72e( true) ;
         }
         else
         {
            wb_table26_71_K72e( false) ;
         }
      }

      protected void wb_table25_61_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt1_Internalname, context.localUtil.Format(AV19ContagemResultado_DataUltCnt1, "99/99/99"), context.localUtil.Format( AV19ContagemResultado_DataUltCnt1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_195_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataultcnt_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataultcnt_to1_Internalname, context.localUtil.Format(AV20ContagemResultado_DataUltCnt_To1, "99/99/99"), context.localUtil.Format( AV20ContagemResultado_DataUltCnt_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataultcnt_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataultcnt_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table25_61_K72e( true) ;
         }
         else
         {
            wb_table25_61_K72e( false) ;
         }
      }

      protected void wb_table11_13_K72( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportpdf_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, imgExportpdf_Enabled, "", imgExportpdf_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExportpdf_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOEXPORTPDF\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoPgtoFnc.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_13_K72e( true) ;
         }
         else
         {
            wb_table11_13_K72e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAK72( ) ;
         WSK72( ) ;
         WEK72( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423465816");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("extrawwcontagemresultadopgtofnc.js", "?202032423465816");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1952( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_195_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_195_idx;
         edtContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD_"+sGXsfl_195_idx;
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD_"+sGXsfl_195_idx;
         edtContagemResultado_PFFinal_Internalname = "CONTAGEMRESULTADO_PFFINAL_"+sGXsfl_195_idx;
         edtContagemResultado_HoraUltCnt_Internalname = "CONTAGEMRESULTADO_HORAULTCNT_"+sGXsfl_195_idx;
         edtContagemResultado_LiqLogCod_Internalname = "CONTAGEMRESULTADO_LIQLOGCOD_"+sGXsfl_195_idx;
         edtContratada_AreaTrabalhoDes_Internalname = "CONTRATADA_AREATRABALHODES_"+sGXsfl_195_idx;
         edtContagemResultado_Agrupador_Internalname = "CONTAGEMRESULTADO_AGRUPADOR_"+sGXsfl_195_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_195_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_195_idx;
         edtavVinculadacom_Internalname = "vVINCULADACOM_"+sGXsfl_195_idx;
         edtContagemResultado_DataUltCnt_Internalname = "CONTAGEMRESULTADO_DATAULTCNT_"+sGXsfl_195_idx;
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_195_idx;
         edtContagemResultadoLiqLog_Data_Internalname = "CONTAGEMRESULTADOLIQLOG_DATA_"+sGXsfl_195_idx;
         dynContagemResultado_ContadorFM_Internalname = "CONTAGEMRESULTADO_CONTADORFM_"+sGXsfl_195_idx;
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_195_idx;
         edtavPfb_Internalname = "vPFB_"+sGXsfl_195_idx;
         edtavValorb_Internalname = "vVALORB_"+sGXsfl_195_idx;
         edtContagemResultado_GlsValor_Internalname = "CONTAGEMRESULTADO_GLSVALOR_"+sGXsfl_195_idx;
         edtavValorl_Internalname = "vVALORL_"+sGXsfl_195_idx;
         edtavWarning_Internalname = "vWARNING_"+sGXsfl_195_idx;
      }

      protected void SubsflControlProps_fel_1952( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_195_fel_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_195_fel_idx;
         edtContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD_"+sGXsfl_195_fel_idx;
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD_"+sGXsfl_195_fel_idx;
         edtContagemResultado_PFFinal_Internalname = "CONTAGEMRESULTADO_PFFINAL_"+sGXsfl_195_fel_idx;
         edtContagemResultado_HoraUltCnt_Internalname = "CONTAGEMRESULTADO_HORAULTCNT_"+sGXsfl_195_fel_idx;
         edtContagemResultado_LiqLogCod_Internalname = "CONTAGEMRESULTADO_LIQLOGCOD_"+sGXsfl_195_fel_idx;
         edtContratada_AreaTrabalhoDes_Internalname = "CONTRATADA_AREATRABALHODES_"+sGXsfl_195_fel_idx;
         edtContagemResultado_Agrupador_Internalname = "CONTAGEMRESULTADO_AGRUPADOR_"+sGXsfl_195_fel_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_195_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_195_fel_idx;
         edtavVinculadacom_Internalname = "vVINCULADACOM_"+sGXsfl_195_fel_idx;
         edtContagemResultado_DataUltCnt_Internalname = "CONTAGEMRESULTADO_DATAULTCNT_"+sGXsfl_195_fel_idx;
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_195_fel_idx;
         edtContagemResultadoLiqLog_Data_Internalname = "CONTAGEMRESULTADOLIQLOG_DATA_"+sGXsfl_195_fel_idx;
         dynContagemResultado_ContadorFM_Internalname = "CONTAGEMRESULTADO_CONTADORFM_"+sGXsfl_195_fel_idx;
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_195_fel_idx;
         edtavPfb_Internalname = "vPFB_"+sGXsfl_195_fel_idx;
         edtavValorb_Internalname = "vVALORB_"+sGXsfl_195_fel_idx;
         edtContagemResultado_GlsValor_Internalname = "CONTAGEMRESULTADO_GLSVALOR_"+sGXsfl_195_fel_idx;
         edtavValorl_Internalname = "vVALORL_"+sGXsfl_195_fel_idx;
         edtavWarning_Internalname = "vWARNING_"+sGXsfl_195_fel_idx;
      }

      protected void sendrow_1952( )
      {
         SubsflControlProps_1952( ) ;
         WBK70( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_195_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_195_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_195_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((chkavSelected.Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 196,'',false,'"+sGXsfl_195_idx+"',195)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV41Selected),(String)"",(String)"",chkavSelected.Visible,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(196, this, 'true', 'false');gx.ajax.executeCliEvent('e37k72_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,196);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContratadaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A490ContagemResultado_ContratadaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContratadaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFFinal_Internalname,StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")),context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFFinal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_HoraUltCnt_Internalname,StringUtil.RTrim( A825ContagemResultado_HoraUltCnt),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_HoraUltCnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_LiqLogCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1043ContagemResultado_LiqLogCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_LiqLogCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtContratada_AreaTrabalhoDes_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AreaTrabalhoDes_Internalname,(String)A53Contratada_AreaTrabalhoDes,StringUtil.RTrim( context.localUtil.Format( A53Contratada_AreaTrabalhoDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AreaTrabalhoDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtContratada_AreaTrabalhoDes_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Agrupador_Internalname,StringUtil.RTrim( A1046ContagemResultado_Agrupador),StringUtil.RTrim( context.localUtil.Format( A1046ContagemResultado_Agrupador, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Agrupador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)85,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContagemResultado_Demanda_Link,(String)edtContagemResultado_Demanda_Linktarget,(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavVinculadacom_Enabled!=0)&&(edtavVinculadacom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 207,'',false,'"+sGXsfl_195_idx+"',195)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVinculadacom_Internalname,StringUtil.RTrim( AV42VinculadaCom),(String)"",TempTags+((edtavVinculadacom_Enabled!=0)&&(edtavVinculadacom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavVinculadacom_Enabled!=0)&&(edtavVinculadacom_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,207);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtavVinculadacom_Link,(String)"",(String)edtavVinculadacom_Tooltiptext,(String)"",(String)edtavVinculadacom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavVinculadacom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataUltCnt_Internalname,context.localUtil.Format(A566ContagemResultado_DataUltCnt, "99/99/99"),context.localUtil.Format( A566ContagemResultado_DataUltCnt, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataUltCnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_195_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_195_idx;
               cmbContagemResultado_StatusDmn.Name = GXCCtl;
               cmbContagemResultado_StatusDmn.WebTags = "";
               cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
               cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
               cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
               cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
               cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
               cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
               cmbContagemResultado_StatusDmn.addItem("D", "Rejeitada", 0);
               cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
               cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
               cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
               cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
               cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
               cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
               cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
               cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
               cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
               cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
               cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
               cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
               cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
               if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
               {
                  A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
                  n484ContagemResultado_StatusDmn = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_StatusDmn,(String)cmbContagemResultado_StatusDmn_Internalname,StringUtil.RTrim( A484ContagemResultado_StatusDmn),(short)1,(String)cmbContagemResultado_StatusDmn_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultado_StatusDmn.CurrentValue = StringUtil.RTrim( A484ContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusDmn_Internalname, "Values", (String)(cmbContagemResultado_StatusDmn.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((edtContagemResultadoLiqLog_Data_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoLiqLog_Data_Internalname,context.localUtil.TToC( A1034ContagemResultadoLiqLog_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1034ContagemResultadoLiqLog_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoLiqLog_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtContagemResultadoLiqLog_Data_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GXACONTAGEMRESULTADO_CONTADORFM_htmlK72( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_195_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADO_CONTADORFM_" + sGXsfl_195_idx;
               dynContagemResultado_ContadorFM.Name = GXCCtl;
               dynContagemResultado_ContadorFM.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynContagemResultado_ContadorFM,(String)dynContagemResultado_ContadorFM_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A584ContagemResultado_ContadorFM), 6, 0)),(short)1,(String)dynContagemResultado_ContadorFM_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynContagemResultado_ContadorFM.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A584ContagemResultado_ContadorFM), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContadorFM_Internalname, "Values", (String)(dynContagemResultado_ContadorFM.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ServicoSigla_Internalname,StringUtil.RTrim( A801ContagemResultado_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ServicoSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPfb_Enabled!=0)&&(edtavPfb_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 213,'',false,'"+sGXsfl_195_idx+"',195)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfb_Internalname,StringUtil.LTrim( StringUtil.NToC( AV66PFB, 14, 5, ",", "")),((edtavPfb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV66PFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV66PFB, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfb_Enabled!=0)&&(edtavPfb_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfb_Enabled!=0)&&(edtavPfb_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,213);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfb_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfb_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavValorb_Enabled!=0)&&(edtavValorb_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 214,'',false,'"+sGXsfl_195_idx+"',195)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavValorb_Internalname,StringUtil.LTrim( StringUtil.NToC( AV61ValorB, 18, 5, ",", "")),((edtavValorb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV61ValorB, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV61ValorB, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavValorb_Enabled!=0)&&(edtavValorb_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavValorb_Enabled!=0)&&(edtavValorb_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,214);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavValorb_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavValorb_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_GlsValor_Internalname,StringUtil.LTrim( StringUtil.NToC( A1051ContagemResultado_GlsValor, 18, 2, ",", "")),context.localUtil.Format( A1051ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_GlsValor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor2Casas",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavValorl_Enabled!=0)&&(edtavValorl_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 216,'',false,'"+sGXsfl_195_idx+"',195)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavValorl_Internalname,StringUtil.LTrim( StringUtil.NToC( AV67ValorL, 18, 5, ",", "")),((edtavValorl_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV67ValorL, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV67ValorL, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavValorl_Enabled!=0)&&(edtavValorl_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavValorl_Enabled!=0)&&(edtavValorl_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,216);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavValorl_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavValorl_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)195,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavWarning_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV80Warning_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV80Warning))&&String.IsNullOrEmpty(StringUtil.RTrim( AV214Warning_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV80Warning)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavWarning_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV80Warning)) ? AV214Warning_GXI : context.PathToRelativeUrl( AV80Warning)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavWarning_Visible,(short)0,(String)"",(String)edtavWarning_Tooltiptext,(short)0,(short)1,(short)20,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV80Warning_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CONTRATADACOD"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, context.localUtil.Format( (decimal)(A490ContagemResultado_ContratadaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_PFFINAL"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, context.localUtil.Format( A574ContagemResultado_PFFinal, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_LIQLOGCOD"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, context.localUtil.Format( (decimal)(A1043ContagemResultado_LiqLogCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_AGRUPADOR"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, StringUtil.RTrim( context.localUtil.Format( A1046ContagemResultado_Agrupador, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDA"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DEMANDAFM"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_STATUSDMN"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_GLSVALOR"+"_"+sGXsfl_195_idx, GetSecureSignedToken( sGXsfl_195_idx, context.localUtil.Format( A1051ContagemResultado_GlsValor, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_195_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_195_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_195_idx+1));
            sGXsfl_195_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_195_idx), 4, 0)), 4, "0");
            SubsflControlProps_1952( ) ;
         }
         /* End function sendrow_1952 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         imgExportpdf_Internalname = "EXPORTPDF";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextcontratada_areatrabalhocod_Internalname = "FILTERTEXTCONTRATADA_AREATRABALHOCOD";
         edtavContratada_areatrabalhocod_Internalname = "vCONTRATADA_AREATRABALHOCOD";
         lblFiltertextcontagemresultado_contratadapessoacod_Internalname = "FILTERTEXTCONTAGEMRESULTADO_CONTRATADAPESSOACOD";
         edtavContagemresultado_contratadapessoacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADAPESSOACOD";
         lblFiltertextcontagemresultado_liqlogcod_Internalname = "FILTERTEXTCONTAGEMRESULTADO_LIQLOGCOD";
         edtavContagemresultado_liqlogcod_Internalname = "vCONTAGEMRESULTADO_LIQLOGCOD";
         lblFiltertextcontagemresultado_statusdmn_Internalname = "FILTERTEXTCONTAGEMRESULTADO_STATUSDMN";
         cmbavContagemresultado_statusdmn_Internalname = "vCONTAGEMRESULTADO_STATUSDMN";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagemresultado_dataultcnt1_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT1";
         lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT_RANGEMIDDLETEXT1";
         edtavContagemresultado_dataultcnt_to1_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT_TO1";
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT1";
         edtavContagemresultado_demanda1_Internalname = "vCONTAGEMRESULTADO_DEMANDA1";
         dynavContagemresultado_contadorfm1_Internalname = "vCONTAGEMRESULTADO_CONTADORFM1";
         edtavContagemresultadoliqlog_data1_Internalname = "vCONTAGEMRESULTADOLIQLOG_DATA1";
         lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA_RANGEMIDDLETEXT1";
         edtavContagemresultadoliqlog_data_to1_Internalname = "vCONTAGEMRESULTADOLIQLOG_DATA_TO1";
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA1";
         edtavContagemresultado_agrupador1_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR1";
         dynavContagemresultado_cntcod1_Internalname = "vCONTAGEMRESULTADO_CNTCOD1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagemresultado_dataultcnt2_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT2";
         lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT_RANGEMIDDLETEXT2";
         edtavContagemresultado_dataultcnt_to2_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT_TO2";
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT2";
         edtavContagemresultado_demanda2_Internalname = "vCONTAGEMRESULTADO_DEMANDA2";
         dynavContagemresultado_contadorfm2_Internalname = "vCONTAGEMRESULTADO_CONTADORFM2";
         edtavContagemresultadoliqlog_data2_Internalname = "vCONTAGEMRESULTADOLIQLOG_DATA2";
         lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA_RANGEMIDDLETEXT2";
         edtavContagemresultadoliqlog_data_to2_Internalname = "vCONTAGEMRESULTADOLIQLOG_DATA_TO2";
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA2";
         edtavContagemresultado_agrupador2_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR2";
         dynavContagemresultado_cntcod2_Internalname = "vCONTAGEMRESULTADO_CNTCOD2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagemresultado_dataultcnt3_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT3";
         lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT_RANGEMIDDLETEXT3";
         edtavContagemresultado_dataultcnt_to3_Internalname = "vCONTAGEMRESULTADO_DATAULTCNT_TO3";
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT3";
         edtavContagemresultado_demanda3_Internalname = "vCONTAGEMRESULTADO_DEMANDA3";
         dynavContagemresultado_contadorfm3_Internalname = "vCONTAGEMRESULTADO_CONTADORFM3";
         edtavContagemresultadoliqlog_data3_Internalname = "vCONTAGEMRESULTADOLIQLOG_DATA3";
         lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA_RANGEMIDDLETEXT3";
         edtavContagemresultadoliqlog_data_to3_Internalname = "vCONTAGEMRESULTADOLIQLOG_DATA_TO3";
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA3";
         edtavContagemresultado_agrupador3_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR3";
         dynavContagemresultado_cntcod3_Internalname = "vCONTAGEMRESULTADO_CNTCOD3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtnatualizar_Internalname = "BTNATUALIZAR";
         bttBtnmostrartudo_Internalname = "BTNMOSTRARTUDO";
         tblUnnamedtable6_Internalname = "UNNAMEDTABLE6";
         lblTextblocktodasasareas_Internalname = "TEXTBLOCKTODASASAREAS";
         chkavTodasasareas_Internalname = "vTODASASAREAS";
         lblTodasasareas_righttext_Internalname = "TODASASAREAS_RIGHTTEXT";
         tblTablemergedtodasasareas_Internalname = "TABLEMERGEDTODASASAREAS";
         lblTextblockliquidadas_Internalname = "TEXTBLOCKLIQUIDADAS";
         chkavLiquidadas_Internalname = "vLIQUIDADAS";
         lblLiquidadas_righttext_Internalname = "LIQUIDADAS_RIGHTTEXT";
         tblTablemergedliquidadas_Internalname = "TABLEMERGEDLIQUIDADAS";
         tblUnnamedtable7_Internalname = "UNNAMEDTABLE7";
         tblUnnamedtable5_Internalname = "UNNAMEDTABLE5";
         tblTableheader_Internalname = "TABLEHEADER";
         chkavSelected_Internalname = "vSELECTED";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD";
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD";
         edtContagemResultado_PFFinal_Internalname = "CONTAGEMRESULTADO_PFFINAL";
         edtContagemResultado_HoraUltCnt_Internalname = "CONTAGEMRESULTADO_HORAULTCNT";
         edtContagemResultado_LiqLogCod_Internalname = "CONTAGEMRESULTADO_LIQLOGCOD";
         edtContratada_AreaTrabalhoDes_Internalname = "CONTRATADA_AREATRABALHODES";
         edtContagemResultado_Agrupador_Internalname = "CONTAGEMRESULTADO_AGRUPADOR";
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         edtavVinculadacom_Internalname = "vVINCULADACOM";
         edtContagemResultado_DataUltCnt_Internalname = "CONTAGEMRESULTADO_DATAULTCNT";
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN";
         edtContagemResultadoLiqLog_Data_Internalname = "CONTAGEMRESULTADOLIQLOG_DATA";
         dynContagemResultado_ContadorFM_Internalname = "CONTAGEMRESULTADO_CONTADORFM";
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA";
         edtavPfb_Internalname = "vPFB";
         edtavValorb_Internalname = "vVALORB";
         edtContagemResultado_GlsValor_Internalname = "CONTAGEMRESULTADO_GLSVALOR";
         edtavValorl_Internalname = "vVALORL";
         edtavWarning_Internalname = "vWARNING";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         bttBtnliquidar_Internalname = "BTNLIQUIDAR";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         lblTextblockquantidade_Internalname = "TEXTBLOCKQUANTIDADE";
         edtavQuantidade_Internalname = "vQUANTIDADE";
         lblTextblockregistros_Internalname = "TEXTBLOCKREGISTROS";
         edtavRegistros_Internalname = "vREGISTROS";
         tblTablemergedquantidade_Internalname = "TABLEMERGEDQUANTIDADE";
         lblTextblockqtdeselecionadas_Internalname = "TEXTBLOCKQTDESELECIONADAS";
         edtavQtdeselecionadas_Internalname = "vQTDESELECIONADAS";
         lblTextblockpftotalb_Internalname = "TEXTBLOCKPFTOTALB";
         edtavPftotalb_Internalname = "vPFTOTALB";
         lblTextblocktotalbruto_Internalname = "TEXTBLOCKTOTALBRUTO";
         edtavTotalbruto_Internalname = "vTOTALBRUTO";
         lblTextblockglstotal_Internalname = "TEXTBLOCKGLSTOTAL";
         edtavGlstotal_Internalname = "vGLSTOTAL";
         lblTextblocktotalliquido_Internalname = "TEXTBLOCKTOTALLIQUIDO";
         edtavTotalliquido_Internalname = "vTOTALLIQUIDO";
         tblUnnamedtable4_Internalname = "UNNAMEDTABLE4";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Innewwindow_Internalname = "INNEWWINDOW";
         Confirmpanel_Internalname = "CONFIRMPANEL";
         tblUsertable_Internalname = "USERTABLE";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavValorl_Jsonclick = "";
         edtavValorl_Visible = -1;
         edtContagemResultado_GlsValor_Jsonclick = "";
         edtavValorb_Jsonclick = "";
         edtavValorb_Visible = -1;
         edtavPfb_Jsonclick = "";
         edtavPfb_Visible = -1;
         edtContagemResultado_ServicoSigla_Jsonclick = "";
         dynContagemResultado_ContadorFM_Jsonclick = "";
         edtContagemResultadoLiqLog_Data_Jsonclick = "";
         cmbContagemResultado_StatusDmn_Jsonclick = "";
         edtContagemResultado_DataUltCnt_Jsonclick = "";
         edtavVinculadacom_Jsonclick = "";
         edtavVinculadacom_Visible = -1;
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         edtContagemResultado_Agrupador_Jsonclick = "";
         edtContratada_AreaTrabalhoDes_Jsonclick = "";
         edtContagemResultado_LiqLogCod_Jsonclick = "";
         edtContagemResultado_HoraUltCnt_Jsonclick = "";
         edtContagemResultado_PFFinal_Jsonclick = "";
         edtContratada_AreaTrabalhoCod_Jsonclick = "";
         edtContagemResultado_ContratadaCod_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         chkavSelected.Enabled = 1;
         imgExportpdf_Enabled = 1;
         edtavContagemresultado_dataultcnt_to1_Jsonclick = "";
         edtavContagemresultado_dataultcnt1_Jsonclick = "";
         edtavContagemresultadoliqlog_data_to1_Jsonclick = "";
         edtavContagemresultadoliqlog_data1_Jsonclick = "";
         dynavContagemresultado_cntcod1_Jsonclick = "";
         edtavContagemresultado_agrupador1_Jsonclick = "";
         dynavContagemresultado_contadorfm1_Jsonclick = "";
         edtavContagemresultado_demanda1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemresultado_dataultcnt_to2_Jsonclick = "";
         edtavContagemresultado_dataultcnt2_Jsonclick = "";
         edtavContagemresultadoliqlog_data_to2_Jsonclick = "";
         edtavContagemresultadoliqlog_data2_Jsonclick = "";
         dynavContagemresultado_cntcod2_Jsonclick = "";
         edtavContagemresultado_agrupador2_Jsonclick = "";
         dynavContagemresultado_contadorfm2_Jsonclick = "";
         edtavContagemresultado_demanda2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemresultado_dataultcnt_to3_Jsonclick = "";
         edtavContagemresultado_dataultcnt3_Jsonclick = "";
         edtavContagemresultadoliqlog_data_to3_Jsonclick = "";
         edtavContagemresultadoliqlog_data3_Jsonclick = "";
         dynavContagemresultado_cntcod3_Jsonclick = "";
         edtavContagemresultado_agrupador3_Jsonclick = "";
         dynavContagemresultado_contadorfm3_Jsonclick = "";
         edtavContagemresultado_demanda3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         bttBtnmostrartudo_Visible = 1;
         cmbavContagemresultado_statusdmn_Jsonclick = "";
         edtavContagemresultado_liqlogcod_Jsonclick = "";
         edtavContagemresultado_contratadapessoacod_Jsonclick = "";
         edtavContratada_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavValorl_Enabled = 1;
         edtavValorb_Enabled = 1;
         edtavPfb_Enabled = 1;
         edtavVinculadacom_Tooltiptext = "";
         edtavVinculadacom_Link = "";
         edtavVinculadacom_Enabled = 1;
         edtContagemResultado_Demanda_Linktarget = "";
         edtContagemResultado_Demanda_Link = "";
         edtContagemResultadoLiqLog_Data_Titleformat = 0;
         cmbContagemResultado_StatusDmn_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         bttBtnliquidar_Enabled = 1;
         edtavRegistros_Jsonclick = "";
         edtavRegistros_Enabled = 1;
         edtavQuantidade_Jsonclick = "";
         edtavQuantidade_Enabled = 1;
         edtavTotalliquido_Jsonclick = "";
         edtavTotalliquido_Enabled = 1;
         edtavGlstotal_Jsonclick = "";
         edtavGlstotal_Enabled = 1;
         edtavTotalbruto_Jsonclick = "";
         edtavTotalbruto_Enabled = 1;
         edtavPftotalb_Jsonclick = "";
         edtavPftotalb_Enabled = 1;
         edtavQtdeselecionadas_Jsonclick = "";
         edtavQtdeselecionadas_Enabled = 1;
         bttBtnmostrartudo_Tooltiptext = "Mostrar tudo";
         edtavWarning_Tooltiptext = "";
         edtavWarning_Visible = -1;
         cmbavDynamicfiltersoperator3.Visible = 1;
         dynavContagemresultado_cntcod3.Visible = 1;
         edtavContagemresultado_agrupador3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible = 1;
         dynavContagemresultado_contadorfm3.Visible = 1;
         edtavContagemresultado_demanda3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         dynavContagemresultado_cntcod2.Visible = 1;
         edtavContagemresultado_agrupador2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible = 1;
         dynavContagemresultado_contadorfm2.Visible = 1;
         edtavContagemresultado_demanda2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         dynavContagemresultado_cntcod1.Visible = 1;
         edtavContagemresultado_agrupador1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible = 1;
         dynavContagemresultado_contadorfm1.Visible = 1;
         edtavContagemresultado_demanda1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible = 1;
         imgExportpdf_Tooltiptext = "Gerar PDF";
         bttBtnliquidar_Tooltiptext = "Liquidar as OS selecionadas";
         chkavSelected.Visible = -1;
         edtContagemResultadoLiqLog_Data_Visible = -1;
         edtContratada_AreaTrabalhoDes_Visible = -1;
         edtContagemResultadoLiqLog_Data_Title = "Liquidada";
         cmbContagemResultado_StatusDmn.Title.Text = "Status";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavSelected.Caption = "";
         chkavLiquidadas.Caption = "";
         chkavTodasasareas.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtavAreatrabalho_codigo_Jsonclick = "";
         edtavAreatrabalho_codigo_Visible = 1;
         Confirmpanel_Draggeable = Convert.ToBoolean( 0);
         Confirmpanel_Confirmtype = "0";
         Confirmpanel_Buttoncanceltext = "Fechar";
         Confirmpanel_Buttonnotext = "N�o";
         Confirmpanel_Buttonyestext = "Fechar";
         Confirmpanel_Confirmtext = "Seu perfil de usu�rio n�o permite acesso ao Financeiro!";
         Confirmpanel_Icon = "4";
         Confirmpanel_Title = "Aten��o";
         Confirmpanel_Height = "50";
         Confirmpanel_Width = "400";
         Innewwindow_Target = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Pagamento Funcion�rios";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'cmbContagemResultado_StatusDmn'},{av:'edtContagemResultadoLiqLog_Data_Titleformat',ctrl:'CONTAGEMRESULTADOLIQLOG_DATA',prop:'Titleformat'},{av:'edtContagemResultadoLiqLog_Data_Title',ctrl:'CONTAGEMRESULTADOLIQLOG_DATA',prop:'Title'},{av:'AV145GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV146GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtContratada_AreaTrabalhoDes_Visible',ctrl:'CONTRATADA_AREATRABALHODES',prop:'Visible'},{av:'edtContagemResultadoLiqLog_Data_Visible',ctrl:'CONTAGEMRESULTADOLIQLOG_DATA',prop:'Visible'},{av:'chkavSelected.Visible',ctrl:'vSELECTED',prop:'Visible'},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{ctrl:'BTNLIQUIDAR',prop:'Tooltiptext'},{ctrl:'BTNLIQUIDAR',prop:'Enabled'},{ctrl:'BTNMOSTRARTUDO',prop:'Visible'},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV36Registros',fld:'vREGISTROS',pic:'ZZZZZZZ9',hsh:true,nv:0},{ctrl:'BTNMOSTRARTUDO',prop:'Tooltiptext'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E36K72',iparms:[{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV80Warning',fld:'vWARNING',pic:'',nv:''},{av:'edtavWarning_Tooltiptext',ctrl:'vWARNING',prop:'Tooltiptext'},{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'},{av:'AV42VinculadaCom',fld:'vVINCULADACOM',pic:'',nv:''},{av:'edtavVinculadacom_Tooltiptext',ctrl:'vVINCULADACOM',prop:'Tooltiptext'},{av:'edtavVinculadacom_Link',ctrl:'vVINCULADACOM',prop:'Link'},{av:'edtContagemResultado_Demanda_Link',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Link'},{av:'edtContagemResultado_Demanda_Linktarget',ctrl:'CONTAGEMRESULTADO_DEMANDA',prop:'Linktarget'},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV66PFB',fld:'vPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV41Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'edtavWarning_Visible',ctrl:'vWARNING',prop:'Visible'},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E13K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22K72',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E14K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'dynavContagemresultado_contadorfm2'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA2',prop:'Visible'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'dynavContagemresultado_cntcod2'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'dynavContagemresultado_contadorfm3'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA3',prop:'Visible'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'dynavContagemresultado_cntcod3'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'dynavContagemresultado_contadorfm1'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA1',prop:'Visible'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'dynavContagemresultado_cntcod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23K72',iparms:[{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'dynavContagemresultado_contadorfm1'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA1',prop:'Visible'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'dynavContagemresultado_cntcod1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24K72',iparms:[],oparms:[{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E15K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'dynavContagemresultado_contadorfm2'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA2',prop:'Visible'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'dynavContagemresultado_cntcod2'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'dynavContagemresultado_contadorfm3'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA3',prop:'Visible'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'dynavContagemresultado_cntcod3'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'dynavContagemresultado_contadorfm1'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA1',prop:'Visible'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'dynavContagemresultado_cntcod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25K72',iparms:[{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'dynavContagemresultado_contadorfm2'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA2',prop:'Visible'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'dynavContagemresultado_cntcod2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E16K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'dynavContagemresultado_contadorfm2'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA2',prop:'Visible'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'dynavContagemresultado_cntcod2'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'dynavContagemresultado_contadorfm3'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA3',prop:'Visible'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'dynavContagemresultado_cntcod3'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'dynavContagemresultado_contadorfm1'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA1',prop:'Visible'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'dynavContagemresultado_cntcod1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26K72',iparms:[{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'dynavContagemresultado_contadorfm3'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA3',prop:'Visible'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'dynavContagemresultado_cntcod3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E17K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT1',prop:'Visible'},{av:'edtavContagemresultado_demanda1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA1',prop:'Visible'},{av:'dynavContagemresultado_contadorfm1'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA1',prop:'Visible'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'dynavContagemresultado_cntcod1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT2',prop:'Visible'},{av:'edtavContagemresultado_demanda2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA2',prop:'Visible'},{av:'dynavContagemresultado_contadorfm2'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA2',prop:'Visible'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'dynavContagemresultado_cntcod2'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAULTCNT3',prop:'Visible'},{av:'edtavContagemresultado_demanda3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDA3',prop:'Visible'},{av:'dynavContagemresultado_contadorfm3'},{av:'tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOLIQLOG_DATA3',prop:'Visible'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'dynavContagemresultado_cntcod3'}]}");
         setEventMetadata("'DOLIQUIDAR'","{handler:'E18K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0},{av:'AV59Observacao',fld:'vOBSERVACAO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOATUALIZAR'","{handler:'E19K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("'DOMOSTRARTUDO'","{handler:'E20K72',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV89DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV77ContagemResultado_Demanda1',fld:'vCONTAGEMRESULTADO_DEMANDA1',pic:'@!',nv:''},{av:'AV164ContagemResultadoLiqLog_Data1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA1',pic:'',nv:''},{av:'AV165ContagemResultadoLiqLog_Data_To1',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO1',pic:'',nv:''},{av:'AV55ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV90DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV78ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'@!',nv:''},{av:'AV166ContagemResultadoLiqLog_Data2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA2',pic:'',nv:''},{av:'AV167ContagemResultadoLiqLog_Data_To2',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO2',pic:'',nv:''},{av:'AV56ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV28DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV91DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV79ContagemResultado_Demanda3',fld:'vCONTAGEMRESULTADO_DEMANDA3',pic:'@!',nv:''},{av:'AV168ContagemResultadoLiqLog_Data3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA3',pic:'',nv:''},{av:'AV169ContagemResultadoLiqLog_Data_To3',fld:'vCONTAGEMRESULTADOLIQLOG_DATA_TO3',pic:'',nv:''},{av:'AV57ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV45TodasAsAreas',fld:'vTODASASAREAS',pic:'',nv:false},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV84PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV148ContagemResultado_ContratadaPessoaCod',fld:'vCONTAGEMRESULTADO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV102ContagemResultado_StatusDmn',fld:'vCONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV19ContagemResultado_DataUltCnt1',fld:'vCONTAGEMRESULTADO_DATAULTCNT1',pic:'',nv:''},{av:'AV20ContagemResultado_DataUltCnt_To1',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO1',pic:'',nv:''},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV24ContagemResultado_DataUltCnt2',fld:'vCONTAGEMRESULTADO_DATAULTCNT2',pic:'',nv:''},{av:'AV25ContagemResultado_DataUltCnt_To2',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO2',pic:'',nv:''},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV29ContagemResultado_DataUltCnt3',fld:'vCONTAGEMRESULTADO_DATAULTCNT3',pic:'',nv:''},{av:'AV30ContagemResultado_DataUltCnt_To3',fld:'vCONTAGEMRESULTADO_DATAULTCNT_TO3',pic:'',nv:''},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV216Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'A1583ContagemResultado_TipoRegistro',fld:'CONTAGEMRESULTADO_TIPOREGISTRO',pic:'ZZZ9',nv:0},{av:'AV83LimiteRegistros',fld:'vLIMITEREGISTROS',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A603ContagemResultado_DmnVinculada',fld:'CONTAGEMRESULTADO_DMNVINCULADA',pic:'@!',nv:''},{av:'A1590ContagemResultado_SiglaSrvVnc',fld:'CONTAGEMRESULTADO_SIGLASRVVNC',pic:'@!',nv:''},{av:'AV35Quantidade',fld:'vQUANTIDADE',pic:'ZZZ9',nv:0},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1480ContagemResultado_CstUntUltima',fld:'CONTAGEMRESULTADO_CSTUNTULTIMA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV69CstUntNrm',fld:'vCSTUNTNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV70CalculoPFinal',fld:'vCALCULOPFINAL',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1033ContagemResultadoLiqLog_Codigo',fld:'CONTAGEMRESULTADOLIQLOG_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1043ContagemResultado_LiqLogCod',fld:'CONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1371ContagemResultadoLiqLogOS_OSCod',fld:'CONTAGEMRESULTADOLIQLOGOS_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1375ContagemResultadoLiqLogOS_Valor',fld:'CONTAGEMRESULTADOLIQLOGOS_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A1373ContagemResultadoLiqLogOS_UserNom',fld:'CONTAGEMRESULTADOLIQLOGOS_USERNOM',pic:'@!',nv:''},{av:'A1034ContagemResultadoLiqLog_Data',fld:'CONTAGEMRESULTADOLIQLOG_DATA',pic:'99/99/99 99:99',nv:''},{av:'A1372ContagemResultadoLiqLogOS_UserCod',fld:'CONTAGEMRESULTADOLIQLOGOS_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV76ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV86ReAbertas',fld:'vREABERTAS',pic:'ZZZ9',nv:0},{av:'AV36Registros',fld:'vREGISTROS',pic:'ZZZZZZZ9',hsh:true,nv:0}],oparms:[{av:'Confirmpanel_Title',ctrl:'CONFIRMPANEL',prop:'Title'},{av:'Confirmpanel_Confirmtext',ctrl:'CONFIRMPANEL',prop:'ConfirmText'},{av:'AV85TudoClicked',fld:'vTUDOCLICKED',pic:'',nv:false},{av:'AV147PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("'DOEXPORTPDF'","{handler:'E21K72',iparms:[{av:'AV36Registros',fld:'vREGISTROS',pic:'ZZZZZZZ9',hsh:true,nv:0},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV152ContagemResultado_CntCod1',fld:'vCONTAGEMRESULTADO_CNTCOD1',pic:'ZZZZZ9',nv:0},{av:'AV153ContagemResultado_CntCod2',fld:'vCONTAGEMRESULTADO_CNTCOD2',pic:'ZZZZZ9',nv:0},{av:'AV154ContagemResultado_CntCod3',fld:'vCONTAGEMRESULTADO_CNTCOD3',pic:'ZZZZZ9',nv:0},{av:'AV21ContagemResultado_ContadorFM1',fld:'vCONTAGEMRESULTADO_CONTADORFM1',pic:'ZZZZZ9',nv:0},{av:'AV26ContagemResultado_ContadorFM2',fld:'vCONTAGEMRESULTADO_CONTADORFM2',pic:'ZZZZZ9',nv:0},{av:'AV31ContagemResultado_ContadorFM3',fld:'vCONTAGEMRESULTADO_CONTADORFM3',pic:'ZZZZZ9',nv:0},{av:'AV15Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false},{av:'AV156Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0}],oparms:[{av:'AV156Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("VLIQUIDADAS.CLICK","{handler:'E27K72',iparms:[{av:'AV87Liquidadas',fld:'vLIQUIDADAS',pic:'',nv:false}],oparms:[{av:'AV16ContagemResultado_LiqLogCod',fld:'vCONTAGEMRESULTADO_LIQLOGCOD',pic:'ZZZZZ9',nv:0},{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'}]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E12K72',iparms:[{av:'Confirmpanel_Title',ctrl:'CONFIRMPANEL',prop:'Title'}],oparms:[]}");
         setEventMetadata("VSELECTED.CLICK","{handler:'E37K72',iparms:[{av:'AV67ValorL',fld:'vVALORL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV41Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null},{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV66PFB',fld:'vPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61ValorB',fld:'vVALORB',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'A1051ContagemResultado_GlsValor',fld:'CONTAGEMRESULTADO_GLSVALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0}],oparms:[{av:'AV48QtdeSelecionadas',fld:'vQTDESELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV62PFTotalB',fld:'vPFTOTALB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TotalBruto',fld:'vTOTALBRUTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV63GlsTotal',fld:'vGLSTOTAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TotalLiquido',fld:'vTOTALLIQUIDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV41Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV46Selecionadas',fld:'vSELECIONADAS',pic:'',nv:null}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTADORFM1.CLICK","{handler:'E28K72',iparms:[],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTADORFM2.CLICK","{handler:'E29K72',iparms:[],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CONTADORFM3.CLICK","{handler:'E30K72',iparms:[],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CNTCOD1.CLICK","{handler:'E31K72',iparms:[],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CNTCOD2.CLICK","{handler:'E32K72',iparms:[],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'}]}");
         setEventMetadata("VCONTAGEMRESULTADO_CNTCOD3.CLICK","{handler:'E33K72',iparms:[],oparms:[{av:'imgExportpdf_Enabled',ctrl:'EXPORTPDF',prop:'Enabled'},{av:'imgExportpdf_Tooltiptext',ctrl:'EXPORTPDF',prop:'Tooltiptext'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A1046ContagemResultado_Agrupador = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV18DynamicFiltersSelector1 = "";
         AV77ContagemResultado_Demanda1 = "";
         AV164ContagemResultadoLiqLog_Data1 = DateTime.MinValue;
         AV165ContagemResultadoLiqLog_Data_To1 = DateTime.MinValue;
         AV55ContagemResultado_Agrupador1 = "";
         AV23DynamicFiltersSelector2 = "";
         AV78ContagemResultado_Demanda2 = "";
         AV166ContagemResultadoLiqLog_Data2 = DateTime.MinValue;
         AV167ContagemResultadoLiqLog_Data_To2 = DateTime.MinValue;
         AV56ContagemResultado_Agrupador2 = "";
         AV28DynamicFiltersSelector3 = "";
         AV79ContagemResultado_Demanda3 = "";
         AV168ContagemResultadoLiqLog_Data3 = DateTime.MinValue;
         AV169ContagemResultadoLiqLog_Data_To3 = DateTime.MinValue;
         AV57ContagemResultado_Agrupador3 = "";
         AV102ContagemResultado_StatusDmn = "";
         AV19ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV20ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV24ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV25ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV29ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV30ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV216Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A603ContagemResultado_DmnVinculada = "";
         A1590ContagemResultado_SiglaSrvVnc = "";
         AV70CalculoPFinal = "";
         AV46Selecionadas = new GxSimpleCollection();
         A1373ContagemResultadoLiqLogOS_UserNom = "";
         A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV59Observacao = "";
         A1600ContagemResultado_CntSrvSttPgmFnc = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A825ContagemResultado_HoraUltCnt = "";
         A53Contratada_AreaTrabalhoDes = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         AV42VinculadaCom = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A801ContagemResultado_ServicoSigla = "";
         AV80Warning = "";
         AV214Warning_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         l1046ContagemResultado_Agrupador = "";
         H00K72_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00K72_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00K73_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00K73_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00K74_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00K74_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00K75_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00K75_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00K75_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00K75_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00K75_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00K75_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00K75_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00K75_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00K75_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00K75_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00K75_A292Usuario_EhContratante = new bool[] {false} ;
         H00K75_n292Usuario_EhContratante = new bool[] {false} ;
         H00K76_A74Contrato_Codigo = new int[1] ;
         H00K76_A77Contrato_Numero = new String[] {""} ;
         H00K76_A39Contratada_Codigo = new int[1] ;
         H00K77_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00K77_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00K77_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00K77_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00K77_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00K77_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00K77_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00K77_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00K77_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00K77_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00K77_A292Usuario_EhContratante = new bool[] {false} ;
         H00K77_n292Usuario_EhContratante = new bool[] {false} ;
         H00K78_A74Contrato_Codigo = new int[1] ;
         H00K78_A77Contrato_Numero = new String[] {""} ;
         H00K78_A39Contratada_Codigo = new int[1] ;
         H00K79_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00K79_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00K79_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00K79_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00K79_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00K79_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00K79_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00K79_A1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00K79_n1394ContratadaUsuario_UsuarioAtivo = new bool[] {false} ;
         H00K79_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00K79_A292Usuario_EhContratante = new bool[] {false} ;
         H00K79_n292Usuario_EhContratante = new bool[] {false} ;
         H00K710_A74Contrato_Codigo = new int[1] ;
         H00K710_A77Contrato_Numero = new String[] {""} ;
         H00K710_A39Contratada_Codigo = new int[1] ;
         H00K711_A57Usuario_PessoaCod = new int[1] ;
         H00K711_A1Usuario_Codigo = new int[1] ;
         H00K711_A58Usuario_PessoaNom = new String[] {""} ;
         H00K711_n58Usuario_PessoaNom = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = "";
         lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = "";
         lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = "";
         lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = "";
         lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = "";
         lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = "";
         AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = "";
         AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 = "";
         AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 = DateTime.MinValue;
         AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 = DateTime.MinValue;
         AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 = "";
         AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = "";
         AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 = "";
         AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 = DateTime.MinValue;
         AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 = DateTime.MinValue;
         AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 = "";
         AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = "";
         AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 = "";
         AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 = DateTime.MinValue;
         AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 = DateTime.MinValue;
         AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 = "";
         AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         H00K713_A146Modulo_Codigo = new int[1] ;
         H00K713_n146Modulo_Codigo = new bool[] {false} ;
         H00K713_A127Sistema_Codigo = new int[1] ;
         H00K713_A1399Sistema_ImpUserCod = new int[1] ;
         H00K713_n1399Sistema_ImpUserCod = new bool[] {false} ;
         H00K713_A601ContagemResultado_Servico = new int[1] ;
         H00K713_n601ContagemResultado_Servico = new bool[] {false} ;
         H00K713_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         H00K713_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         H00K713_A1591ContagemResultado_CodSrvVnc = new int[1] ;
         H00K713_n1591ContagemResultado_CodSrvVnc = new bool[] {false} ;
         H00K713_A1043ContagemResultado_LiqLogCod = new int[1] ;
         H00K713_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         H00K713_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00K713_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00K713_A292Usuario_EhContratante = new bool[] {false} ;
         H00K713_n292Usuario_EhContratante = new bool[] {false} ;
         H00K713_A1583ContagemResultado_TipoRegistro = new short[1] ;
         H00K713_A1603ContagemResultado_CntCod = new int[1] ;
         H00K713_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00K713_A508ContagemResultado_Owner = new int[1] ;
         H00K713_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         H00K713_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         H00K713_A1600ContagemResultado_CntSrvSttPgmFnc = new String[] {""} ;
         H00K713_n1600ContagemResultado_CntSrvSttPgmFnc = new bool[] {false} ;
         H00K713_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00K713_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00K713_A602ContagemResultado_OSVinculada = new int[1] ;
         H00K713_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00K713_A1590ContagemResultado_SiglaSrvVnc = new String[] {""} ;
         H00K713_n1590ContagemResultado_SiglaSrvVnc = new bool[] {false} ;
         H00K713_A603ContagemResultado_DmnVinculada = new String[] {""} ;
         H00K713_n603ContagemResultado_DmnVinculada = new bool[] {false} ;
         H00K713_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00K713_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00K713_A1051ContagemResultado_GlsValor = new decimal[1] ;
         H00K713_n1051ContagemResultado_GlsValor = new bool[] {false} ;
         H00K713_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00K713_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00K713_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         H00K713_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         H00K713_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00K713_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00K713_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00K713_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00K713_A457ContagemResultado_Demanda = new String[] {""} ;
         H00K713_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00K713_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00K713_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00K713_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00K713_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00K713_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00K713_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00K713_A1480ContagemResultado_CstUntUltima = new decimal[1] ;
         H00K713_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00K713_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00K713_A584ContagemResultado_ContadorFM = new int[1] ;
         H00K713_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         H00K713_A825ContagemResultado_HoraUltCnt = new String[] {""} ;
         H00K713_A456ContagemResultado_Codigo = new int[1] ;
         AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn = "";
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         H00K714_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         H00K714_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         H00K714_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         H00K714_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         GridRow = new GXWebRow();
         AV47WebSession = context.GetSession();
         AV34Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         AV82ToolTipText = "";
         H00K715_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         H00K715_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         H00K715_A1374ContagemResultadoLiqLogOS_UserPesCod = new int[1] ;
         H00K715_n1374ContagemResultadoLiqLogOS_UserPesCod = new bool[] {false} ;
         H00K715_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         H00K715_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         H00K715_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         H00K715_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         H00K715_A1373ContagemResultadoLiqLogOS_UserNom = new String[] {""} ;
         H00K715_n1373ContagemResultadoLiqLogOS_UserNom = new bool[] {false} ;
         H00K715_A1372ContagemResultadoLiqLogOS_UserCod = new int[1] ;
         H00K716_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         H00K716_A1372ContagemResultadoLiqLogOS_UserCod = new int[1] ;
         H00K716_A1374ContagemResultadoLiqLogOS_UserPesCod = new int[1] ;
         H00K716_n1374ContagemResultadoLiqLogOS_UserPesCod = new bool[] {false} ;
         H00K716_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         H00K716_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         H00K716_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         H00K716_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         H00K716_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         H00K716_A1373ContagemResultadoLiqLogOS_UserNom = new String[] {""} ;
         H00K716_n1373ContagemResultadoLiqLogOS_UserNom = new bool[] {false} ;
         H00K718_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00K718_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00K718_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00K718_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00K718_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00K718_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00K718_A1583ContagemResultado_TipoRegistro = new short[1] ;
         H00K718_A456ContagemResultado_Codigo = new int[1] ;
         H00K718_A1603ContagemResultado_CntCod = new int[1] ;
         H00K718_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00K718_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00K718_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00K718_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         H00K718_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         H00K718_A508ContagemResultado_Owner = new int[1] ;
         H00K718_A457ContagemResultado_Demanda = new String[] {""} ;
         H00K718_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00K718_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         H00K718_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         H00K718_A1600ContagemResultado_CntSrvSttPgmFnc = new String[] {""} ;
         H00K718_n1600ContagemResultado_CntSrvSttPgmFnc = new bool[] {false} ;
         H00K718_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00K718_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00K718_A1043ContagemResultado_LiqLogCod = new int[1] ;
         H00K718_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         H00K718_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00K718_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00K718_A584ContagemResultado_ContadorFM = new int[1] ;
         H00K718_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         sStyleString = "";
         lblTextblockquantidade_Jsonclick = "";
         lblTextblockqtdeselecionadas_Jsonclick = "";
         lblTextblockpftotalb_Jsonclick = "";
         lblTextblocktotalbruto_Jsonclick = "";
         lblTextblockglstotal_Jsonclick = "";
         lblTextblocktotalliquido_Jsonclick = "";
         lblTextblockregistros_Jsonclick = "";
         bttBtnliquidar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContagemresultadotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblTextblocktodasasareas_Jsonclick = "";
         lblTextblockliquidadas_Jsonclick = "";
         lblLiquidadas_righttext_Jsonclick = "";
         lblTodasasareas_righttext_Jsonclick = "";
         lblFiltertextcontratada_areatrabalhocod_Jsonclick = "";
         lblFiltertextcontagemresultado_contratadapessoacod_Jsonclick = "";
         lblFiltertextcontagemresultado_liqlogcod_Jsonclick = "";
         lblFiltertextcontagemresultado_statusdmn_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtnatualizar_Jsonclick = "";
         bttBtnmostrartudo_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext1_Jsonclick = "";
         imgExportpdf_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.extrawwcontagemresultadopgtofnc__default(),
            new Object[][] {
                new Object[] {
               H00K72_A1046ContagemResultado_Agrupador, H00K72_n1046ContagemResultado_Agrupador
               }
               , new Object[] {
               H00K73_A1046ContagemResultado_Agrupador, H00K73_n1046ContagemResultado_Agrupador
               }
               , new Object[] {
               H00K74_A1046ContagemResultado_Agrupador, H00K74_n1046ContagemResultado_Agrupador
               }
               , new Object[] {
               H00K75_A70ContratadaUsuario_UsuarioPessoaCod, H00K75_n70ContratadaUsuario_UsuarioPessoaCod, H00K75_A69ContratadaUsuario_UsuarioCod, H00K75_A71ContratadaUsuario_UsuarioPessoaNom, H00K75_n71ContratadaUsuario_UsuarioPessoaNom, H00K75_A1228ContratadaUsuario_AreaTrabalhoCod, H00K75_n1228ContratadaUsuario_AreaTrabalhoCod, H00K75_A1394ContratadaUsuario_UsuarioAtivo, H00K75_n1394ContratadaUsuario_UsuarioAtivo, H00K75_A66ContratadaUsuario_ContratadaCod,
               H00K75_A292Usuario_EhContratante, H00K75_n292Usuario_EhContratante
               }
               , new Object[] {
               H00K76_A74Contrato_Codigo, H00K76_A77Contrato_Numero, H00K76_A39Contratada_Codigo
               }
               , new Object[] {
               H00K77_A70ContratadaUsuario_UsuarioPessoaCod, H00K77_n70ContratadaUsuario_UsuarioPessoaCod, H00K77_A69ContratadaUsuario_UsuarioCod, H00K77_A71ContratadaUsuario_UsuarioPessoaNom, H00K77_n71ContratadaUsuario_UsuarioPessoaNom, H00K77_A1228ContratadaUsuario_AreaTrabalhoCod, H00K77_n1228ContratadaUsuario_AreaTrabalhoCod, H00K77_A1394ContratadaUsuario_UsuarioAtivo, H00K77_n1394ContratadaUsuario_UsuarioAtivo, H00K77_A66ContratadaUsuario_ContratadaCod,
               H00K77_A292Usuario_EhContratante, H00K77_n292Usuario_EhContratante
               }
               , new Object[] {
               H00K78_A74Contrato_Codigo, H00K78_A77Contrato_Numero, H00K78_A39Contratada_Codigo
               }
               , new Object[] {
               H00K79_A70ContratadaUsuario_UsuarioPessoaCod, H00K79_n70ContratadaUsuario_UsuarioPessoaCod, H00K79_A69ContratadaUsuario_UsuarioCod, H00K79_A71ContratadaUsuario_UsuarioPessoaNom, H00K79_n71ContratadaUsuario_UsuarioPessoaNom, H00K79_A1228ContratadaUsuario_AreaTrabalhoCod, H00K79_n1228ContratadaUsuario_AreaTrabalhoCod, H00K79_A1394ContratadaUsuario_UsuarioAtivo, H00K79_n1394ContratadaUsuario_UsuarioAtivo, H00K79_A66ContratadaUsuario_ContratadaCod,
               H00K79_A292Usuario_EhContratante, H00K79_n292Usuario_EhContratante
               }
               , new Object[] {
               H00K710_A74Contrato_Codigo, H00K710_A77Contrato_Numero, H00K710_A39Contratada_Codigo
               }
               , new Object[] {
               H00K711_A57Usuario_PessoaCod, H00K711_A1Usuario_Codigo, H00K711_A58Usuario_PessoaNom, H00K711_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00K713_A146Modulo_Codigo, H00K713_n146Modulo_Codigo, H00K713_A127Sistema_Codigo, H00K713_A1399Sistema_ImpUserCod, H00K713_n1399Sistema_ImpUserCod, H00K713_A601ContagemResultado_Servico, H00K713_n601ContagemResultado_Servico, H00K713_A1627ContagemResultado_CntSrvVncCod, H00K713_n1627ContagemResultado_CntSrvVncCod, H00K713_A1591ContagemResultado_CodSrvVnc,
               H00K713_n1591ContagemResultado_CodSrvVnc, H00K713_A1043ContagemResultado_LiqLogCod, H00K713_n1043ContagemResultado_LiqLogCod, H00K713_A52Contratada_AreaTrabalhoCod, H00K713_n52Contratada_AreaTrabalhoCod, H00K713_A292Usuario_EhContratante, H00K713_n292Usuario_EhContratante, H00K713_A1583ContagemResultado_TipoRegistro, H00K713_A1603ContagemResultado_CntCod, H00K713_n1603ContagemResultado_CntCod,
               H00K713_A508ContagemResultado_Owner, H00K713_A1854ContagemResultado_VlrCnc, H00K713_n1854ContagemResultado_VlrCnc, H00K713_A1600ContagemResultado_CntSrvSttPgmFnc, H00K713_n1600ContagemResultado_CntSrvSttPgmFnc, H00K713_A499ContagemResultado_ContratadaPessoaCod, H00K713_n499ContagemResultado_ContratadaPessoaCod, H00K713_A602ContagemResultado_OSVinculada, H00K713_n602ContagemResultado_OSVinculada, H00K713_A1590ContagemResultado_SiglaSrvVnc,
               H00K713_n1590ContagemResultado_SiglaSrvVnc, H00K713_A603ContagemResultado_DmnVinculada, H00K713_n603ContagemResultado_DmnVinculada, H00K713_A1553ContagemResultado_CntSrvCod, H00K713_n1553ContagemResultado_CntSrvCod, H00K713_A1051ContagemResultado_GlsValor, H00K713_n1051ContagemResultado_GlsValor, H00K713_A801ContagemResultado_ServicoSigla, H00K713_n801ContagemResultado_ServicoSigla, H00K713_A1034ContagemResultadoLiqLog_Data,
               H00K713_n1034ContagemResultadoLiqLog_Data, H00K713_A484ContagemResultado_StatusDmn, H00K713_n484ContagemResultado_StatusDmn, H00K713_A493ContagemResultado_DemandaFM, H00K713_n493ContagemResultado_DemandaFM, H00K713_A457ContagemResultado_Demanda, H00K713_n457ContagemResultado_Demanda, H00K713_A1046ContagemResultado_Agrupador, H00K713_n1046ContagemResultado_Agrupador, H00K713_A53Contratada_AreaTrabalhoDes,
               H00K713_n53Contratada_AreaTrabalhoDes, H00K713_A490ContagemResultado_ContratadaCod, H00K713_n490ContagemResultado_ContratadaCod, H00K713_A1480ContagemResultado_CstUntUltima, H00K713_A682ContagemResultado_PFBFMUltima, H00K713_A684ContagemResultado_PFBFSUltima, H00K713_A584ContagemResultado_ContadorFM, H00K713_A566ContagemResultado_DataUltCnt, H00K713_A825ContagemResultado_HoraUltCnt, H00K713_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00K714_A1370ContagemResultadoLiqLogOS_Codigo, H00K714_A1033ContagemResultadoLiqLog_Codigo, H00K714_A1371ContagemResultadoLiqLogOS_OSCod, H00K714_A1375ContagemResultadoLiqLogOS_Valor
               }
               , new Object[] {
               H00K715_A1370ContagemResultadoLiqLogOS_Codigo, H00K715_A1033ContagemResultadoLiqLog_Codigo, H00K715_A1374ContagemResultadoLiqLogOS_UserPesCod, H00K715_n1374ContagemResultadoLiqLogOS_UserPesCod, H00K715_A1371ContagemResultadoLiqLogOS_OSCod, H00K715_A1375ContagemResultadoLiqLogOS_Valor, H00K715_A1034ContagemResultadoLiqLog_Data, H00K715_A1373ContagemResultadoLiqLogOS_UserNom, H00K715_n1373ContagemResultadoLiqLogOS_UserNom, H00K715_A1372ContagemResultadoLiqLogOS_UserCod
               }
               , new Object[] {
               H00K716_A1370ContagemResultadoLiqLogOS_Codigo, H00K716_A1372ContagemResultadoLiqLogOS_UserCod, H00K716_A1374ContagemResultadoLiqLogOS_UserPesCod, H00K716_n1374ContagemResultadoLiqLogOS_UserPesCod, H00K716_A1033ContagemResultadoLiqLog_Codigo, H00K716_A1371ContagemResultadoLiqLogOS_OSCod, H00K716_A1375ContagemResultadoLiqLogOS_Valor, H00K716_A1034ContagemResultadoLiqLog_Data, H00K716_A1373ContagemResultadoLiqLogOS_UserNom, H00K716_n1373ContagemResultadoLiqLogOS_UserNom
               }
               , new Object[] {
               H00K718_A490ContagemResultado_ContratadaCod, H00K718_n490ContagemResultado_ContratadaCod, H00K718_A1553ContagemResultado_CntSrvCod, H00K718_n1553ContagemResultado_CntSrvCod, H00K718_A499ContagemResultado_ContratadaPessoaCod, H00K718_n499ContagemResultado_ContratadaPessoaCod, H00K718_A1583ContagemResultado_TipoRegistro, H00K718_A456ContagemResultado_Codigo, H00K718_A1603ContagemResultado_CntCod, H00K718_n1603ContagemResultado_CntCod,
               H00K718_A1046ContagemResultado_Agrupador, H00K718_n1046ContagemResultado_Agrupador, H00K718_A1034ContagemResultadoLiqLog_Data, H00K718_n1034ContagemResultadoLiqLog_Data, H00K718_A508ContagemResultado_Owner, H00K718_A457ContagemResultado_Demanda, H00K718_n457ContagemResultado_Demanda, H00K718_A1854ContagemResultado_VlrCnc, H00K718_n1854ContagemResultado_VlrCnc, H00K718_A1600ContagemResultado_CntSrvSttPgmFnc,
               H00K718_n1600ContagemResultado_CntSrvSttPgmFnc, H00K718_A484ContagemResultado_StatusDmn, H00K718_n484ContagemResultado_StatusDmn, H00K718_A1043ContagemResultado_LiqLogCod, H00K718_n1043ContagemResultado_LiqLogCod, H00K718_A52Contratada_AreaTrabalhoCod, H00K718_n52Contratada_AreaTrabalhoCod, H00K718_A584ContagemResultado_ContadorFM, H00K718_A566ContagemResultado_DataUltCnt
               }
            }
         );
         AV216Pgmname = "ExtraWWContagemResultadoPgtoFnc";
         /* GeneXus formulas. */
         AV216Pgmname = "ExtraWWContagemResultadoPgtoFnc";
         context.Gx_err = 0;
         edtavVinculadacom_Enabled = 0;
         edtavPfb_Enabled = 0;
         edtavValorb_Enabled = 0;
         edtavValorl_Enabled = 0;
         edtavQuantidade_Enabled = 0;
         edtavRegistros_Enabled = 0;
         edtavQtdeselecionadas_Enabled = 0;
         edtavPftotalb_Enabled = 0;
         edtavTotalbruto_Enabled = 0;
         edtavGlstotal_Enabled = 0;
         edtavTotalliquido_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_195 ;
      private short nGXsfl_195_idx=1 ;
      private short AV13OrderedBy ;
      private short AV89DynamicFiltersOperator1 ;
      private short AV90DynamicFiltersOperator2 ;
      private short AV91DynamicFiltersOperator3 ;
      private short AV84PreView ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short AV83LimiteRegistros ;
      private short AV35Quantidade ;
      private short AV48QtdeSelecionadas ;
      private short AV86ReAbertas ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_195_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV6WWPContext_gxTpr_Userid ;
      private short AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 ;
      private short AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 ;
      private short AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 ;
      private short cmbContagemResultado_StatusDmn_Titleformat ;
      private short edtContagemResultadoLiqLog_Data_Titleformat ;
      private short AV218GXLvl1107 ;
      private short AV219GXLvl1127 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV58AreaTrabalho_Codigo ;
      private int subGrid_Rows ;
      private int AV15Contratada_AreaTrabalhoCod ;
      private int AV152ContagemResultado_CntCod1 ;
      private int AV153ContagemResultado_CntCod2 ;
      private int AV154ContagemResultado_CntCod3 ;
      private int AV148ContagemResultado_ContratadaPessoaCod ;
      private int AV16ContagemResultado_LiqLogCod ;
      private int AV21ContagemResultado_ContadorFM1 ;
      private int AV26ContagemResultado_ContadorFM2 ;
      private int AV31ContagemResultado_ContadorFM3 ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A456ContagemResultado_Codigo ;
      private int A584ContagemResultado_ContadorFM ;
      private int A602ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1033ContagemResultadoLiqLog_Codigo ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A1371ContagemResultadoLiqLogOS_OSCod ;
      private int AV81Codigo ;
      private int A1372ContagemResultadoLiqLogOS_UserCod ;
      private int AV76ContadorFM ;
      private int AV36Registros ;
      private int AV156Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavAreatrabalho_codigo_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavVinculadacom_Enabled ;
      private int edtavPfb_Enabled ;
      private int edtavValorb_Enabled ;
      private int edtavValorl_Enabled ;
      private int edtavQuantidade_Enabled ;
      private int edtavRegistros_Enabled ;
      private int edtavQtdeselecionadas_Enabled ;
      private int edtavPftotalb_Enabled ;
      private int edtavTotalbruto_Enabled ;
      private int edtavGlstotal_Enabled ;
      private int edtavTotalliquido_Enabled ;
      private int AV10GridState_gxTpr_Dynamicfilters_Count ;
      private int AV6WWPContext_gxTpr_Contratada_pessoacod ;
      private int AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod ;
      private int AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 ;
      private int AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 ;
      private int AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 ;
      private int A1603ContagemResultado_CntCod ;
      private int AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 ;
      private int A508ContagemResultado_Owner ;
      private int AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 ;
      private int AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A1399Sistema_ImpUserCod ;
      private int A601ContagemResultado_Servico ;
      private int A1627ContagemResultado_CntSrvVncCod ;
      private int A1591ContagemResultado_CodSrvVnc ;
      private int AV179ExtraWWContagemResultadoPgtoFncDS_2_Contagemresultado_contratadapessoacod ;
      private int AV180ExtraWWContagemResultadoPgtoFncDS_3_Contagemresultado_liqlogcod ;
      private int bttBtnmostrartudo_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtContratada_AreaTrabalhoDes_Visible ;
      private int edtContagemResultadoLiqLog_Data_Visible ;
      private int bttBtnliquidar_Enabled ;
      private int AV144PageToGo ;
      private int imgExportpdf_Enabled ;
      private int AV75ContratadaCod ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int AV157Usuario_Codigo ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Visible ;
      private int edtavContagemresultado_demanda1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Visible ;
      private int edtavContagemresultado_agrupador1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Visible ;
      private int edtavContagemresultado_demanda2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Visible ;
      private int edtavContagemresultado_agrupador2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Visible ;
      private int edtavContagemresultado_demanda3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Visible ;
      private int edtavContagemresultado_agrupador3_Visible ;
      private int AV217GXV1 ;
      private int A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int edtavWarning_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavVinculadacom_Visible ;
      private int edtavPfb_Visible ;
      private int edtavValorb_Visible ;
      private int edtavValorl_Visible ;
      private long AV147PaginaAtual ;
      private long GRID_nFirstRecordOnPage ;
      private long AV145GridCurrentPage ;
      private long AV146GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long GXt_int2 ;
      private decimal A1480ContagemResultado_CstUntUltima ;
      private decimal AV69CstUntNrm ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A1375ContagemResultadoLiqLogOS_Valor ;
      private decimal AV62PFTotalB ;
      private decimal AV64TotalBruto ;
      private decimal AV61ValorB ;
      private decimal AV63GlsTotal ;
      private decimal A1051ContagemResultado_GlsValor ;
      private decimal AV65TotalLiquido ;
      private decimal AV67ValorL ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal AV66PFB ;
      private decimal GXt_decimal1 ;
      private String Gridpaginationbar_Selectedpage ;
      private String Confirmpanel_Title ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String A1046ContagemResultado_Agrupador ;
      private String sGXsfl_195_idx="0001" ;
      private String AV55ContagemResultado_Agrupador1 ;
      private String AV56ContagemResultado_Agrupador2 ;
      private String AV57ContagemResultado_Agrupador3 ;
      private String AV102ContagemResultado_StatusDmn ;
      private String AV216Pgmname ;
      private String A1590ContagemResultado_SiglaSrvVnc ;
      private String AV70CalculoPFinal ;
      private String edtavValorb_Internalname ;
      private String A1373ContagemResultadoLiqLogOS_UserNom ;
      private String edtavValorl_Internalname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1600ContagemResultado_CntSrvSttPgmFnc ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Innewwindow_Target ;
      private String Confirmpanel_Width ;
      private String Confirmpanel_Height ;
      private String Confirmpanel_Icon ;
      private String Confirmpanel_Confirmtext ;
      private String Confirmpanel_Buttonyestext ;
      private String Confirmpanel_Buttonnotext ;
      private String Confirmpanel_Buttoncanceltext ;
      private String Confirmpanel_Confirmtype ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavSelected_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_ContratadaCod_Internalname ;
      private String edtContratada_AreaTrabalhoCod_Internalname ;
      private String edtContagemResultado_PFFinal_Internalname ;
      private String A825ContagemResultado_HoraUltCnt ;
      private String edtContagemResultado_HoraUltCnt_Internalname ;
      private String edtContagemResultado_LiqLogCod_Internalname ;
      private String edtContratada_AreaTrabalhoDes_Internalname ;
      private String edtContagemResultado_Agrupador_Internalname ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String AV42VinculadaCom ;
      private String edtavVinculadacom_Internalname ;
      private String edtContagemResultado_DataUltCnt_Internalname ;
      private String cmbContagemResultado_StatusDmn_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String edtContagemResultadoLiqLog_Data_Internalname ;
      private String dynContagemResultado_ContadorFM_Internalname ;
      private String A801ContagemResultado_ServicoSigla ;
      private String edtContagemResultado_ServicoSigla_Internalname ;
      private String edtavPfb_Internalname ;
      private String edtContagemResultado_GlsValor_Internalname ;
      private String edtavWarning_Internalname ;
      private String chkavTodasasareas_Internalname ;
      private String chkavLiquidadas_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String l1046ContagemResultado_Agrupador ;
      private String edtavQuantidade_Internalname ;
      private String edtavRegistros_Internalname ;
      private String edtavQtdeselecionadas_Internalname ;
      private String edtavPftotalb_Internalname ;
      private String edtavTotalbruto_Internalname ;
      private String edtavGlstotal_Internalname ;
      private String edtavTotalliquido_Internalname ;
      private String lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 ;
      private String lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 ;
      private String lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 ;
      private String AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 ;
      private String AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 ;
      private String AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 ;
      private String AV181ExtraWWContagemResultadoPgtoFncDS_4_Contagemresultado_statusdmn ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavContratada_areatrabalhocod_Internalname ;
      private String edtavContagemresultado_contratadapessoacod_Internalname ;
      private String edtavContagemresultado_liqlogcod_Internalname ;
      private String cmbavContagemresultado_statusdmn_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemresultado_dataultcnt1_Internalname ;
      private String edtavContagemresultado_dataultcnt_to1_Internalname ;
      private String edtavContagemresultado_demanda1_Internalname ;
      private String dynavContagemresultado_contadorfm1_Internalname ;
      private String edtavContagemresultadoliqlog_data1_Internalname ;
      private String edtavContagemresultadoliqlog_data_to1_Internalname ;
      private String edtavContagemresultado_agrupador1_Internalname ;
      private String dynavContagemresultado_cntcod1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemresultado_dataultcnt2_Internalname ;
      private String edtavContagemresultado_dataultcnt_to2_Internalname ;
      private String edtavContagemresultado_demanda2_Internalname ;
      private String dynavContagemresultado_contadorfm2_Internalname ;
      private String edtavContagemresultadoliqlog_data2_Internalname ;
      private String edtavContagemresultadoliqlog_data_to2_Internalname ;
      private String edtavContagemresultado_agrupador2_Internalname ;
      private String dynavContagemresultado_cntcod2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemresultado_dataultcnt3_Internalname ;
      private String edtavContagemresultado_dataultcnt_to3_Internalname ;
      private String edtavContagemresultado_demanda3_Internalname ;
      private String dynavContagemresultado_contadorfm3_Internalname ;
      private String edtavContagemresultadoliqlog_data3_Internalname ;
      private String edtavContagemresultadoliqlog_data_to3_Internalname ;
      private String edtavContagemresultado_agrupador3_Internalname ;
      private String dynavContagemresultado_cntcod3_Internalname ;
      private String hsh ;
      private String bttBtnmostrartudo_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemResultadoLiqLog_Data_Title ;
      private String bttBtnliquidar_Tooltiptext ;
      private String bttBtnliquidar_Internalname ;
      private String Gridpaginationbar_Internalname ;
      private String edtavWarning_Tooltiptext ;
      private String imgExportpdf_Internalname ;
      private String imgExportpdf_Tooltiptext ;
      private String edtavVinculadacom_Tooltiptext ;
      private String edtavVinculadacom_Link ;
      private String edtContagemResultado_Demanda_Link ;
      private String edtContagemResultado_Demanda_Linktarget ;
      private String Confirmpanel_Internalname ;
      private String Innewwindow_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataultcnt1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoliqlog_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataultcnt2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoliqlog_data2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataultcnt3_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoliqlog_data3_Internalname ;
      private String AV82ToolTipText ;
      private String bttBtnmostrartudo_Tooltiptext ;
      private String subGrid_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblUsertable_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblUnnamedtable4_Internalname ;
      private String lblTextblockquantidade_Internalname ;
      private String lblTextblockquantidade_Jsonclick ;
      private String lblTextblockqtdeselecionadas_Internalname ;
      private String lblTextblockqtdeselecionadas_Jsonclick ;
      private String edtavQtdeselecionadas_Jsonclick ;
      private String lblTextblockpftotalb_Internalname ;
      private String lblTextblockpftotalb_Jsonclick ;
      private String edtavPftotalb_Jsonclick ;
      private String lblTextblocktotalbruto_Internalname ;
      private String lblTextblocktotalbruto_Jsonclick ;
      private String edtavTotalbruto_Jsonclick ;
      private String lblTextblockglstotal_Internalname ;
      private String lblTextblockglstotal_Jsonclick ;
      private String edtavGlstotal_Jsonclick ;
      private String lblTextblocktotalliquido_Internalname ;
      private String lblTextblocktotalliquido_Jsonclick ;
      private String edtavTotalliquido_Jsonclick ;
      private String tblTablemergedquantidade_Internalname ;
      private String edtavQuantidade_Jsonclick ;
      private String lblTextblockregistros_Internalname ;
      private String lblTextblockregistros_Jsonclick ;
      private String edtavRegistros_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String bttBtnliquidar_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblUnnamedtable5_Internalname ;
      private String tblUnnamedtable7_Internalname ;
      private String lblTextblocktodasasareas_Internalname ;
      private String lblTextblocktodasasareas_Jsonclick ;
      private String lblTextblockliquidadas_Internalname ;
      private String lblTextblockliquidadas_Jsonclick ;
      private String tblTablemergedliquidadas_Internalname ;
      private String lblLiquidadas_righttext_Internalname ;
      private String lblLiquidadas_righttext_Jsonclick ;
      private String tblTablemergedtodasasareas_Internalname ;
      private String lblTodasasareas_righttext_Internalname ;
      private String lblTodasasareas_righttext_Jsonclick ;
      private String tblUnnamedtable6_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Jsonclick ;
      private String edtavContratada_areatrabalhocod_Jsonclick ;
      private String lblFiltertextcontagemresultado_contratadapessoacod_Internalname ;
      private String lblFiltertextcontagemresultado_contratadapessoacod_Jsonclick ;
      private String edtavContagemresultado_contratadapessoacod_Jsonclick ;
      private String lblFiltertextcontagemresultado_liqlogcod_Internalname ;
      private String lblFiltertextcontagemresultado_liqlogcod_Jsonclick ;
      private String edtavContagemresultado_liqlogcod_Jsonclick ;
      private String lblFiltertextcontagemresultado_statusdmn_Internalname ;
      private String lblFiltertextcontagemresultado_statusdmn_Jsonclick ;
      private String cmbavContagemresultado_statusdmn_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtnatualizar_Internalname ;
      private String bttBtnatualizar_Jsonclick ;
      private String bttBtnmostrartudo_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultado_demanda3_Jsonclick ;
      private String dynavContagemresultado_contadorfm3_Jsonclick ;
      private String edtavContagemresultado_agrupador3_Jsonclick ;
      private String dynavContagemresultado_cntcod3_Jsonclick ;
      private String edtavContagemresultadoliqlog_data3_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultadoliqlog_data_to3_Jsonclick ;
      private String edtavContagemresultado_dataultcnt3_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultado_dataultcnt_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultado_demanda2_Jsonclick ;
      private String dynavContagemresultado_contadorfm2_Jsonclick ;
      private String edtavContagemresultado_agrupador2_Jsonclick ;
      private String dynavContagemresultado_cntcod2_Jsonclick ;
      private String edtavContagemresultadoliqlog_data2_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultadoliqlog_data_to2_Jsonclick ;
      private String edtavContagemresultado_dataultcnt2_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultado_dataultcnt_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultado_demanda1_Jsonclick ;
      private String dynavContagemresultado_contadorfm1_Jsonclick ;
      private String edtavContagemresultado_agrupador1_Jsonclick ;
      private String dynavContagemresultado_cntcod1_Jsonclick ;
      private String edtavContagemresultadoliqlog_data1_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultadoliqlog_data_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultadoliqlog_data_to1_Jsonclick ;
      private String edtavContagemresultado_dataultcnt1_Jsonclick ;
      private String lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultado_dataultcnt_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultado_dataultcnt_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgExportpdf_Jsonclick ;
      private String sGXsfl_195_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_ContratadaCod_Jsonclick ;
      private String edtContratada_AreaTrabalhoCod_Jsonclick ;
      private String edtContagemResultado_PFFinal_Jsonclick ;
      private String edtContagemResultado_HoraUltCnt_Jsonclick ;
      private String edtContagemResultado_LiqLogCod_Jsonclick ;
      private String edtContratada_AreaTrabalhoDes_Jsonclick ;
      private String edtContagemResultado_Agrupador_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtavVinculadacom_Jsonclick ;
      private String edtContagemResultado_DataUltCnt_Jsonclick ;
      private String cmbContagemResultado_StatusDmn_Jsonclick ;
      private String edtContagemResultadoLiqLog_Data_Jsonclick ;
      private String dynContagemResultado_ContadorFM_Jsonclick ;
      private String edtContagemResultado_ServicoSigla_Jsonclick ;
      private String edtavPfb_Jsonclick ;
      private String edtavValorb_Jsonclick ;
      private String edtContagemResultado_GlsValor_Jsonclick ;
      private String edtavValorl_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A1034ContagemResultadoLiqLog_Data ;
      private DateTime AV164ContagemResultadoLiqLog_Data1 ;
      private DateTime AV165ContagemResultadoLiqLog_Data_To1 ;
      private DateTime AV166ContagemResultadoLiqLog_Data2 ;
      private DateTime AV167ContagemResultadoLiqLog_Data_To2 ;
      private DateTime AV168ContagemResultadoLiqLog_Data3 ;
      private DateTime AV169ContagemResultadoLiqLog_Data_To3 ;
      private DateTime AV19ContagemResultado_DataUltCnt1 ;
      private DateTime AV20ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV24ContagemResultado_DataUltCnt2 ;
      private DateTime AV25ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV29ContagemResultado_DataUltCnt3 ;
      private DateTime AV30ContagemResultado_DataUltCnt_To3 ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 ;
      private DateTime AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 ;
      private DateTime AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 ;
      private DateTime AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 ;
      private DateTime AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 ;
      private DateTime AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 ;
      private DateTime AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 ;
      private DateTime AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 ;
      private DateTime AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 ;
      private DateTime AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 ;
      private bool entryPointCalled ;
      private bool AV45TodasAsAreas ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool AV14OrderedDsc ;
      private bool AV87Liquidadas ;
      private bool AV22DynamicFiltersEnabled2 ;
      private bool AV27DynamicFiltersEnabled3 ;
      private bool AV85TudoClicked ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n603ContagemResultado_DmnVinculada ;
      private bool n1590ContagemResultado_SiglaSrvVnc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n1051ContagemResultado_GlsValor ;
      private bool n1373ContagemResultadoLiqLogOS_UserNom ;
      private bool n1034ContagemResultadoLiqLog_Data ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Confirmpanel_Draggeable ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV41Selected ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool AV6WWPContext_gxTpr_Userehfinanceiro ;
      private bool AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 ;
      private bool AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 ;
      private bool n146Modulo_Codigo ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1627ContagemResultado_CntSrvVncCod ;
      private bool n1591ContagemResultado_CodSrvVnc ;
      private bool A292Usuario_EhContratante ;
      private bool n292Usuario_EhContratante ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool AV80Warning_IsBlob ;
      private String AV59Observacao ;
      private String AV18DynamicFiltersSelector1 ;
      private String AV77ContagemResultado_Demanda1 ;
      private String AV23DynamicFiltersSelector2 ;
      private String AV78ContagemResultado_Demanda2 ;
      private String AV28DynamicFiltersSelector3 ;
      private String AV79ContagemResultado_Demanda3 ;
      private String A603ContagemResultado_DmnVinculada ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV214Warning_GXI ;
      private String lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 ;
      private String lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 ;
      private String lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 ;
      private String AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 ;
      private String AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 ;
      private String AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 ;
      private String AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 ;
      private String AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 ;
      private String AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 ;
      private String AV80Warning ;
      private IGxSession AV34Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavContagemresultado_statusdmn ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox dynavContagemresultado_contadorfm1 ;
      private GXCombobox dynavContagemresultado_cntcod1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox dynavContagemresultado_contadorfm2 ;
      private GXCombobox dynavContagemresultado_cntcod2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox dynavContagemresultado_contadorfm3 ;
      private GXCombobox dynavContagemresultado_cntcod3 ;
      private GXCheckbox chkavTodasasareas ;
      private GXCheckbox chkavLiquidadas ;
      private GXCheckbox chkavSelected ;
      private GXCombobox cmbContagemResultado_StatusDmn ;
      private GXCombobox dynContagemResultado_ContadorFM ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00K72_A1046ContagemResultado_Agrupador ;
      private bool[] H00K72_n1046ContagemResultado_Agrupador ;
      private String[] H00K73_A1046ContagemResultado_Agrupador ;
      private bool[] H00K73_n1046ContagemResultado_Agrupador ;
      private String[] H00K74_A1046ContagemResultado_Agrupador ;
      private bool[] H00K74_n1046ContagemResultado_Agrupador ;
      private int[] H00K75_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00K75_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00K75_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00K75_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00K75_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00K75_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00K75_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00K75_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00K75_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00K75_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00K75_A292Usuario_EhContratante ;
      private bool[] H00K75_n292Usuario_EhContratante ;
      private int[] H00K76_A74Contrato_Codigo ;
      private String[] H00K76_A77Contrato_Numero ;
      private int[] H00K76_A39Contratada_Codigo ;
      private int[] H00K77_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00K77_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00K77_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00K77_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00K77_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00K77_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00K77_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00K77_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00K77_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00K77_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00K77_A292Usuario_EhContratante ;
      private bool[] H00K77_n292Usuario_EhContratante ;
      private int[] H00K78_A74Contrato_Codigo ;
      private String[] H00K78_A77Contrato_Numero ;
      private int[] H00K78_A39Contratada_Codigo ;
      private int[] H00K79_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00K79_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00K79_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00K79_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00K79_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00K79_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00K79_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00K79_A1394ContratadaUsuario_UsuarioAtivo ;
      private bool[] H00K79_n1394ContratadaUsuario_UsuarioAtivo ;
      private int[] H00K79_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00K79_A292Usuario_EhContratante ;
      private bool[] H00K79_n292Usuario_EhContratante ;
      private int[] H00K710_A74Contrato_Codigo ;
      private String[] H00K710_A77Contrato_Numero ;
      private int[] H00K710_A39Contratada_Codigo ;
      private int[] H00K711_A57Usuario_PessoaCod ;
      private int[] H00K711_A1Usuario_Codigo ;
      private String[] H00K711_A58Usuario_PessoaNom ;
      private bool[] H00K711_n58Usuario_PessoaNom ;
      private int[] H00K713_A146Modulo_Codigo ;
      private bool[] H00K713_n146Modulo_Codigo ;
      private int[] H00K713_A127Sistema_Codigo ;
      private int[] H00K713_A1399Sistema_ImpUserCod ;
      private bool[] H00K713_n1399Sistema_ImpUserCod ;
      private int[] H00K713_A601ContagemResultado_Servico ;
      private bool[] H00K713_n601ContagemResultado_Servico ;
      private int[] H00K713_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] H00K713_n1627ContagemResultado_CntSrvVncCod ;
      private int[] H00K713_A1591ContagemResultado_CodSrvVnc ;
      private bool[] H00K713_n1591ContagemResultado_CodSrvVnc ;
      private int[] H00K713_A1043ContagemResultado_LiqLogCod ;
      private bool[] H00K713_n1043ContagemResultado_LiqLogCod ;
      private int[] H00K713_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00K713_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00K713_A292Usuario_EhContratante ;
      private bool[] H00K713_n292Usuario_EhContratante ;
      private short[] H00K713_A1583ContagemResultado_TipoRegistro ;
      private int[] H00K713_A1603ContagemResultado_CntCod ;
      private bool[] H00K713_n1603ContagemResultado_CntCod ;
      private int[] H00K713_A508ContagemResultado_Owner ;
      private decimal[] H00K713_A1854ContagemResultado_VlrCnc ;
      private bool[] H00K713_n1854ContagemResultado_VlrCnc ;
      private String[] H00K713_A1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool[] H00K713_n1600ContagemResultado_CntSrvSttPgmFnc ;
      private int[] H00K713_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00K713_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00K713_A602ContagemResultado_OSVinculada ;
      private bool[] H00K713_n602ContagemResultado_OSVinculada ;
      private String[] H00K713_A1590ContagemResultado_SiglaSrvVnc ;
      private bool[] H00K713_n1590ContagemResultado_SiglaSrvVnc ;
      private String[] H00K713_A603ContagemResultado_DmnVinculada ;
      private bool[] H00K713_n603ContagemResultado_DmnVinculada ;
      private int[] H00K713_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00K713_n1553ContagemResultado_CntSrvCod ;
      private decimal[] H00K713_A1051ContagemResultado_GlsValor ;
      private bool[] H00K713_n1051ContagemResultado_GlsValor ;
      private String[] H00K713_A801ContagemResultado_ServicoSigla ;
      private bool[] H00K713_n801ContagemResultado_ServicoSigla ;
      private DateTime[] H00K713_A1034ContagemResultadoLiqLog_Data ;
      private bool[] H00K713_n1034ContagemResultadoLiqLog_Data ;
      private String[] H00K713_A484ContagemResultado_StatusDmn ;
      private bool[] H00K713_n484ContagemResultado_StatusDmn ;
      private String[] H00K713_A493ContagemResultado_DemandaFM ;
      private bool[] H00K713_n493ContagemResultado_DemandaFM ;
      private String[] H00K713_A457ContagemResultado_Demanda ;
      private bool[] H00K713_n457ContagemResultado_Demanda ;
      private String[] H00K713_A1046ContagemResultado_Agrupador ;
      private bool[] H00K713_n1046ContagemResultado_Agrupador ;
      private String[] H00K713_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00K713_n53Contratada_AreaTrabalhoDes ;
      private int[] H00K713_A490ContagemResultado_ContratadaCod ;
      private bool[] H00K713_n490ContagemResultado_ContratadaCod ;
      private decimal[] H00K713_A1480ContagemResultado_CstUntUltima ;
      private decimal[] H00K713_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00K713_A684ContagemResultado_PFBFSUltima ;
      private int[] H00K713_A584ContagemResultado_ContadorFM ;
      private DateTime[] H00K713_A566ContagemResultado_DataUltCnt ;
      private String[] H00K713_A825ContagemResultado_HoraUltCnt ;
      private int[] H00K713_A456ContagemResultado_Codigo ;
      private int[] H00K714_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] H00K714_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] H00K714_A1371ContagemResultadoLiqLogOS_OSCod ;
      private decimal[] H00K714_A1375ContagemResultadoLiqLogOS_Valor ;
      private int[] H00K715_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] H00K715_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] H00K715_A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool[] H00K715_n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int[] H00K715_A1371ContagemResultadoLiqLogOS_OSCod ;
      private decimal[] H00K715_A1375ContagemResultadoLiqLogOS_Valor ;
      private DateTime[] H00K715_A1034ContagemResultadoLiqLog_Data ;
      private bool[] H00K715_n1034ContagemResultadoLiqLog_Data ;
      private String[] H00K715_A1373ContagemResultadoLiqLogOS_UserNom ;
      private bool[] H00K715_n1373ContagemResultadoLiqLogOS_UserNom ;
      private int[] H00K715_A1372ContagemResultadoLiqLogOS_UserCod ;
      private int[] H00K716_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] H00K716_A1372ContagemResultadoLiqLogOS_UserCod ;
      private int[] H00K716_A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool[] H00K716_n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int[] H00K716_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] H00K716_A1371ContagemResultadoLiqLogOS_OSCod ;
      private decimal[] H00K716_A1375ContagemResultadoLiqLogOS_Valor ;
      private DateTime[] H00K716_A1034ContagemResultadoLiqLog_Data ;
      private bool[] H00K716_n1034ContagemResultadoLiqLog_Data ;
      private String[] H00K716_A1373ContagemResultadoLiqLogOS_UserNom ;
      private bool[] H00K716_n1373ContagemResultadoLiqLogOS_UserNom ;
      private int[] H00K718_A490ContagemResultado_ContratadaCod ;
      private bool[] H00K718_n490ContagemResultado_ContratadaCod ;
      private int[] H00K718_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00K718_n1553ContagemResultado_CntSrvCod ;
      private int[] H00K718_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00K718_n499ContagemResultado_ContratadaPessoaCod ;
      private short[] H00K718_A1583ContagemResultado_TipoRegistro ;
      private int[] H00K718_A456ContagemResultado_Codigo ;
      private int[] H00K718_A1603ContagemResultado_CntCod ;
      private bool[] H00K718_n1603ContagemResultado_CntCod ;
      private String[] H00K718_A1046ContagemResultado_Agrupador ;
      private bool[] H00K718_n1046ContagemResultado_Agrupador ;
      private DateTime[] H00K718_A1034ContagemResultadoLiqLog_Data ;
      private bool[] H00K718_n1034ContagemResultadoLiqLog_Data ;
      private int[] H00K718_A508ContagemResultado_Owner ;
      private String[] H00K718_A457ContagemResultado_Demanda ;
      private bool[] H00K718_n457ContagemResultado_Demanda ;
      private decimal[] H00K718_A1854ContagemResultado_VlrCnc ;
      private bool[] H00K718_n1854ContagemResultado_VlrCnc ;
      private String[] H00K718_A1600ContagemResultado_CntSrvSttPgmFnc ;
      private bool[] H00K718_n1600ContagemResultado_CntSrvSttPgmFnc ;
      private String[] H00K718_A484ContagemResultado_StatusDmn ;
      private bool[] H00K718_n484ContagemResultado_StatusDmn ;
      private int[] H00K718_A1043ContagemResultado_LiqLogCod ;
      private bool[] H00K718_n1043ContagemResultado_LiqLogCod ;
      private int[] H00K718_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00K718_n52Contratada_AreaTrabalhoCod ;
      private int[] H00K718_A584ContagemResultado_ContadorFM ;
      private DateTime[] H00K718_A566ContagemResultado_DataUltCnt ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private IGxSession AV47WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV46Selecionadas ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class extrawwcontagemresultadopgtofnc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00K713( IGxContext context ,
                                              int AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod ,
                                              bool AV87Liquidadas ,
                                              String AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 ,
                                              short AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 ,
                                              String AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 ,
                                              DateTime AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 ,
                                              DateTime AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 ,
                                              String AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 ,
                                              int AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 ,
                                              bool AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 ,
                                              String AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 ,
                                              short AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 ,
                                              String AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 ,
                                              DateTime AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 ,
                                              DateTime AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 ,
                                              String AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 ,
                                              int AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 ,
                                              bool AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 ,
                                              String AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 ,
                                              short AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 ,
                                              String AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 ,
                                              DateTime AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 ,
                                              DateTime AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 ,
                                              String AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 ,
                                              int AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 ,
                                              int AV10GridState_gxTpr_Dynamicfilters_Count ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A1043ContagemResultado_LiqLogCod ,
                                              String A457ContagemResultado_Demanda ,
                                              DateTime A1034ContagemResultadoLiqLog_Data ,
                                              String A1046ContagemResultado_Agrupador ,
                                              int A1603ContagemResultado_CntCod ,
                                              int A456ContagemResultado_Codigo ,
                                              short AV13OrderedBy ,
                                              bool AV14OrderedDsc ,
                                              String A484ContagemResultado_StatusDmn ,
                                              String A1600ContagemResultado_CntSrvSttPgmFnc ,
                                              decimal A1854ContagemResultado_VlrCnc ,
                                              DateTime AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 ,
                                              DateTime A566ContagemResultado_DataUltCnt ,
                                              DateTime AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 ,
                                              int AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 ,
                                              int A584ContagemResultado_ContadorFM ,
                                              int A508ContagemResultado_Owner ,
                                              DateTime AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 ,
                                              DateTime AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 ,
                                              int AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 ,
                                              DateTime AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 ,
                                              DateTime AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 ,
                                              int AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 ,
                                              int A499ContagemResultado_ContratadaPessoaCod ,
                                              int AV6WWPContext_gxTpr_Contratada_pessoacod ,
                                              bool AV6WWPContext_gxTpr_Userehfinanceiro ,
                                              short AV6WWPContext_gxTpr_Userid ,
                                              short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [61] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, T9.[Servico_Codigo] AS ContagemResultado_Servico, T6.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod, T7.[Servico_Codigo] AS ContagemResultado_CodSrvVnc, T1.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, T11.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Usuario_EhContratante], T1.[ContagemResultado_TipoRegistro], T9.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_VlrCnc], T9.[ContratoServicos_StatusPagFnc] AS ContagemResultado_CntSrvSttPgmFnc, T11.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T8.[Servico_Sigla] AS ContagemResultado_SiglaSrvVnc, T6.[ContagemResultado_Demanda] AS ContagemResultado_DmnVinculada, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_GlsValor], T10.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[ContagemResultadoLiqLog_Data], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Agrupador], T12.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, COALESCE( T13.[ContagemResultado_CstUntUltima], 0) AS ContagemResultado_CstUntUltima, COALESCE( T13.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T13.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T13.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T13.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt,";
         scmdbuf = scmdbuf + " COALESCE( T13.[ContagemResultado_HoraUltCnt], '') AS ContagemResultado_HoraUltCnt, T1.[ContagemResultado_Codigo] FROM (((((((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[Sistema_ImpUserCod]) LEFT JOIN [ContagemResultadoLiqLog] T5 WITH (NOLOCK) ON T5.[ContagemResultadoLiqLog_Codigo] = T1.[ContagemResultado_LiqLogCod]) LEFT JOIN [ContagemResultado] T6 WITH (NOLOCK) ON T6.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T6.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T8 WITH (NOLOCK) ON T8.[Servico_Codigo] = T7.[Servico_Codigo]) LEFT JOIN [ContratoServicos] T9 WITH (NOLOCK) ON T9.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T10 WITH (NOLOCK) ON T10.[Servico_Codigo] = T9.[Servico_Codigo]) LEFT JOIN [Contratada] T11 WITH (NOLOCK) ON T11.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T12 WITH (NOLOCK) ON T12.[AreaTrabalho_Codigo] = T11.[Contratada_AreaTrabalhoCod]) LEFT JOIN (SELECT MIN([ContagemResultado_CstUntPrd]) AS ContagemResultado_CstUntUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_HoraCnt]) AS ContagemResultado_HoraUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima]";
         scmdbuf = scmdbuf + " = 1 GROUP BY [ContagemResultado_Codigo] ) T13 ON T13.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T13.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T13.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T13.[ContagemResultado_ContadorFM], 0) = @AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T13.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = 1 and @AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T13.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = 1 and @AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T13.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = 1 and @AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T13.[ContagemResultado_ContadorFM], 0) = @AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 or ( (COALESCE( T13.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T13.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T13.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T13.[ContagemResultado_ContadorFM], 0) = @AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 or ( (COALESCE( T13.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (T11.[Contratada_PessoaCod] = @AV6WWPCo_4Contratada_pessoaco)";
         scmdbuf = scmdbuf + " and (@AV6WWPCo_5Userehfinanceiro = 1 or ( COALESCE( T13.[ContagemResultado_ContadorFM], 0) = @AV6WWPContext__Userid))";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ! (0==AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T11.[Contratada_AreaTrabalhoCod] = @AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( ! AV87Liquidadas )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_LiqLogCod] IS NULL)";
         }
         if ( AV87Liquidadas )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_LiqLogCod] > 0)";
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int3[40] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int3[41] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int3[42] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] >= @AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1)";
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] < DATEADD( dd,1, @AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1))";
         }
         else
         {
            GXv_int3[44] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T9.[Contrato_Codigo] = @AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int3[46] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int3[47] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int3[48] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int3[49] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] >= @AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2)";
         }
         else
         {
            GXv_int3[50] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] < DATEADD( dd,1, @AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2))";
         }
         else
         {
            GXv_int3[51] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[52] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ( AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T9.[Contrato_Codigo] = @AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int3[53] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int3[54] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int3[55] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int3[56] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] >= @AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3)";
         }
         else
         {
            GXv_int3[57] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] < DATEADD( dd,1, @AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3))";
         }
         else
         {
            GXv_int3[58] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[59] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ( AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T9.[Contrato_Codigo] = @AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int3[60] = 1;
         }
         if ( AV10GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T12.[AreaTrabalho_Descricao]";
         }
         else if ( AV13OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T5.[ContagemResultadoLiqLog_Data]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T5.[ContagemResultadoLiqLog_Data] DESC";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00K718( IGxContext context ,
                                              int AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod ,
                                              bool AV87Liquidadas ,
                                              String AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 ,
                                              short AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 ,
                                              String AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1 ,
                                              DateTime AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1 ,
                                              DateTime AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1 ,
                                              String AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1 ,
                                              int AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 ,
                                              bool AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 ,
                                              String AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 ,
                                              short AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 ,
                                              String AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2 ,
                                              DateTime AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2 ,
                                              DateTime AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2 ,
                                              String AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2 ,
                                              int AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 ,
                                              bool AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 ,
                                              String AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 ,
                                              short AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 ,
                                              String AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3 ,
                                              DateTime AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3 ,
                                              DateTime AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3 ,
                                              String AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3 ,
                                              int AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 ,
                                              int AV10GridState_gxTpr_Dynamicfilters_Count ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int A1043ContagemResultado_LiqLogCod ,
                                              String A457ContagemResultado_Demanda ,
                                              DateTime A1034ContagemResultadoLiqLog_Data ,
                                              String A1046ContagemResultado_Agrupador ,
                                              int A1603ContagemResultado_CntCod ,
                                              int A456ContagemResultado_Codigo ,
                                              String A484ContagemResultado_StatusDmn ,
                                              String A1600ContagemResultado_CntSrvSttPgmFnc ,
                                              decimal A1854ContagemResultado_VlrCnc ,
                                              DateTime AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 ,
                                              DateTime A566ContagemResultado_DataUltCnt ,
                                              DateTime AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 ,
                                              int AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 ,
                                              int A584ContagemResultado_ContadorFM ,
                                              int A508ContagemResultado_Owner ,
                                              DateTime AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 ,
                                              DateTime AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 ,
                                              int AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 ,
                                              DateTime AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 ,
                                              DateTime AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 ,
                                              int AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 ,
                                              bool AV6WWPContext_gxTpr_Userehfinanceiro ,
                                              short AV6WWPContext_gxTpr_Userid ,
                                              int A499ContagemResultado_ContratadaPessoaCod ,
                                              int AV6WWPContext_gxTpr_Contratada_pessoacod ,
                                              short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [61] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_Codigo], T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Agrupador], T5.[ContagemResultadoLiqLog_Data], T1.[ContagemResultado_Owner], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_VlrCnc], T3.[ContratoServicos_StatusPagFnc] AS ContagemResultado_CntSrvSttPgmFnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, T2.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, COALESCE( T4.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo], MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContagemResultadoLiqLog] T5 WITH (NOLOCK) ON T5.[ContagemResultadoLiqLog_Codigo] = T1.[ContagemResultado_LiqLogCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T4.[ContagemResultado_ContadorFM], 0) = @AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1 or ( (COALESCE( T4.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = 1 and @AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = 1 and @AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 = 1 and @AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T4.[ContagemResultado_ContadorFM], 0) = @AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2 or ( (COALESCE( T4.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 = 1 and @AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T4.[ContagemResultado_ContadorFM], 0) = @AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3 or ( (COALESCE( T4.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (@AV6WWPCo_5Userehfinanceiro = 1 or ( COALESCE( T4.[ContagemResultado_ContadorFM], 0) = @AV6WWPContext__Userid))";
         scmdbuf = scmdbuf + " and (T2.[Contratada_PessoaCod] = @AV6WWPCo_4Contratada_pessoaco)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ! (0==AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T2.[Contratada_AreaTrabalhoCod] = @AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod)";
         }
         else
         {
            GXv_int5[39] = 1;
         }
         if ( ! AV87Liquidadas )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_LiqLogCod] IS NULL)";
         }
         if ( AV87Liquidadas )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_LiqLogCod] > 0)";
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int5[40] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int5[41] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV183ExtraWWContagemResultadoPgtoFncDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1)";
         }
         else
         {
            GXv_int5[42] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] >= @AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1)";
         }
         else
         {
            GXv_int5[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] < DATEADD( dd,1, @AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1))";
         }
         else
         {
            GXv_int5[44] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int5[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int5[46] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int5[47] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int5[48] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV194ExtraWWContagemResultadoPgtoFncDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2)";
         }
         else
         {
            GXv_int5[49] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] >= @AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2)";
         }
         else
         {
            GXv_int5[50] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] < DATEADD( dd,1, @AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2))";
         }
         else
         {
            GXv_int5[51] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int5[52] = 1;
         }
         if ( AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ( AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int5[53] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int5[54] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int5[55] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV205ExtraWWContagemResultadoPgtoFncDS_28_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3)";
         }
         else
         {
            GXv_int5[56] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] >= @AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3)";
         }
         else
         {
            GXv_int5[57] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADOLIQLOG_DATA") == 0 ) && ( ! (DateTime.MinValue==AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3) ) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultadoLiqLog_Data] < DATEADD( dd,1, @AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3))";
         }
         else
         {
            GXv_int5[58] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] like '%' + @lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int5[59] = 1;
         }
         if ( AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ( AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contrato_Codigo] = @AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int5[60] = 1;
         }
         if ( AV10GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Contratada_AreaTrabalhoCod]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 10 :
                     return conditional_H00K713(context, (int)dynConstraints[0] , (bool)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (short)dynConstraints[33] , (bool)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (decimal)dynConstraints[37] , (DateTime)dynConstraints[38] , (DateTime)dynConstraints[39] , (DateTime)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (int)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (int)dynConstraints[46] , (DateTime)dynConstraints[47] , (DateTime)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (bool)dynConstraints[52] , (short)dynConstraints[53] , (short)dynConstraints[54] );
               case 14 :
                     return conditional_H00K718(context, (int)dynConstraints[0] , (bool)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (decimal)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (int)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (int)dynConstraints[47] , (bool)dynConstraints[48] , (short)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (short)dynConstraints[52] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00K72 ;
          prmH00K72 = new Object[] {
          new Object[] {"@l1046ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV45TodasAsAreas",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV58AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K73 ;
          prmH00K73 = new Object[] {
          new Object[] {"@l1046ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV45TodasAsAreas",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV58AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K74 ;
          prmH00K74 = new Object[] {
          new Object[] {"@l1046ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV45TodasAsAreas",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV58AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K75 ;
          prmH00K75 = new Object[] {
          new Object[] {"@AV6WWPCo_1Userehcontratada",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_2Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_3Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K76 ;
          prmH00K76 = new Object[] {
          new Object[] {"@AV6WWPCo_3Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K77 ;
          prmH00K77 = new Object[] {
          new Object[] {"@AV6WWPCo_1Userehcontratada",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_2Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_3Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K78 ;
          prmH00K78 = new Object[] {
          new Object[] {"@AV6WWPCo_3Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K79 ;
          prmH00K79 = new Object[] {
          new Object[] {"@AV6WWPCo_1Userehcontratada",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_2Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_3Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K710 ;
          prmH00K710 = new Object[] {
          new Object[] {"@AV6WWPCo_3Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K711 ;
          prmH00K711 = new Object[] {
          } ;
          Object[] prmH00K714 ;
          prmH00K714 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K715 ;
          prmH00K715 = new Object[] {
          new Object[] {"@AV81Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K716 ;
          prmH00K716 = new Object[] {
          new Object[] {"@AV81Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16ContagemResultado_LiqLogCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K713 ;
          prmH00K713 = new Object[] {
          new Object[] {"@AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_4Contratada_pessoaco",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_5Userehfinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00K718 ;
          prmH00K718 = new Object[] {
          new Object[] {"@AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoPgtoFncDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoPgtoFncDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoPgtoFncDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoPgtoFncDS_10_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoPgtoFncDS_18_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoPgtoFncDS_19_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoPgtoFncDS_15_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoPgtoFncDS_16_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoPgtoFncDS_21_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoPgtoFncDS_29_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoPgtoFncDS_30_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoPgtoFncDS_26_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoPgtoFncDS_27_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoPgtoFncDS_32_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_5Userehfinanceiro",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV6WWPCo_4Contratada_pessoaco",SqlDbType.Int,6,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoPgtoFncDS_1_Contratada_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV186ExtraWWContagemResultadoPgtoFncDS_9_Contagemresultado_demanda1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV188ExtraWWContagemResultadoPgtoFncDS_11_Contagemresultadoliqlog_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoPgtoFncDS_12_Contagemresultadoliqlog_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV190ExtraWWContagemResultadoPgtoFncDS_13_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoPgtoFncDS_14_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoPgtoFncDS_20_Contagemresultado_demanda2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoPgtoFncDS_22_Contagemresultadoliqlog_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoPgtoFncDS_23_Contagemresultadoliqlog_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV201ExtraWWContagemResultadoPgtoFncDS_24_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoPgtoFncDS_25_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoPgtoFncDS_31_Contagemresultado_demanda3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoPgtoFncDS_33_Contagemresultadoliqlog_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoPgtoFncDS_34_Contagemresultadoliqlog_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV212ExtraWWContagemResultadoPgtoFncDS_35_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoPgtoFncDS_36_Contagemresultado_cntcod3",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00K72", "SELECT DISTINCT TOP 5 T1.[ContagemResultado_Agrupador] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (UPPER(T1.[ContagemResultado_Agrupador]) like UPPER(@l1046ContagemResultado_Agrupador)) AND (@AV45TodasAsAreas = 0) AND (T2.[Contratada_AreaTrabalhoCod] = @AV58AreaTrabalho_Codigo) ORDER BY T1.[ContagemResultado_Agrupador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K72,0,0,true,false )
             ,new CursorDef("H00K73", "SELECT DISTINCT TOP 5 T1.[ContagemResultado_Agrupador] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (UPPER(T1.[ContagemResultado_Agrupador]) like UPPER(@l1046ContagemResultado_Agrupador)) AND (@AV45TodasAsAreas = 0) AND (T2.[Contratada_AreaTrabalhoCod] = @AV58AreaTrabalho_Codigo) ORDER BY T1.[ContagemResultado_Agrupador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K73,0,0,true,false )
             ,new CursorDef("H00K74", "SELECT DISTINCT TOP 5 T1.[ContagemResultado_Agrupador] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) WHERE (UPPER(T1.[ContagemResultado_Agrupador]) like UPPER(@l1046ContagemResultado_Agrupador)) AND (@AV45TodasAsAreas = 0) AND (T2.[Contratada_AreaTrabalhoCod] = @AV58AreaTrabalho_Codigo) ORDER BY T1.[ContagemResultado_Agrupador] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K74,0,0,true,false )
             ,new CursorDef("H00K75", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T3.[Usuario_EhContratante] FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (T3.[Usuario_Ativo] = 1) AND (@AV6WWPCo_1Userehcontratada = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_2Areatrabalho_codigo) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV6WWPCo_3Contratada_codigo) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K75,0,0,true,false )
             ,new CursorDef("H00K76", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV6WWPCo_3Contratada_codigo ORDER BY [Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K76,0,0,true,false )
             ,new CursorDef("H00K77", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T3.[Usuario_EhContratante] FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (T3.[Usuario_Ativo] = 1) AND (@AV6WWPCo_1Userehcontratada = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_2Areatrabalho_codigo) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV6WWPCo_3Contratada_codigo) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K77,0,0,true,false )
             ,new CursorDef("H00K78", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV6WWPCo_3Contratada_codigo ORDER BY [Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K78,0,0,true,false )
             ,new CursorDef("H00K79", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_Ativo] AS ContratadaUsuario_UsuarioAtivo, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCo, T3.[Usuario_EhContratante] FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (T3.[Usuario_Ativo] = 1) AND (@AV6WWPCo_1Userehcontratada = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_2Areatrabalho_codigo) AND (T1.[ContratadaUsuario_ContratadaCod] = @AV6WWPCo_3Contratada_codigo) ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K79,0,0,true,false )
             ,new CursorDef("H00K710", "SELECT [Contrato_Codigo], [Contrato_Numero], [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV6WWPCo_3Contratada_codigo ORDER BY [Contrato_Numero] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K710,0,0,true,false )
             ,new CursorDef("H00K711", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K711,0,0,true,false )
             ,new CursorDef("H00K713", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K713,11,0,true,false )
             ,new CursorDef("H00K714", "SELECT TOP 1 [ContagemResultadoLiqLogOS_Codigo], [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_OSCod], [ContagemResultadoLiqLogOS_Valor] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE ([ContagemResultadoLiqLog_Codigo] = @ContagemResultado_LiqLogCod) AND ([ContagemResultadoLiqLogOS_OSCod] = @ContagemResultado_Codigo) ORDER BY [ContagemResultadoLiqLog_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K714,1,0,true,true )
             ,new CursorDef("H00K715", "SELECT T1.[ContagemResultadoLiqLogOS_Codigo], T1.[ContagemResultadoLiqLog_Codigo], T3.[Usuario_PessoaCod] AS ContagemResultadoLiqLogOS_UserPesCod, T1.[ContagemResultadoLiqLogOS_OSCod], T1.[ContagemResultadoLiqLogOS_Valor], T2.[ContagemResultadoLiqLog_Data], T4.[Pessoa_Nome] AS ContagemResultadoLiqLogOS_UserNom, T1.[ContagemResultadoLiqLogOS_UserCod] AS ContagemResultadoLiqLogOS_UserCod FROM ((([ContagemResultadoLiqLogOS] T1 WITH (NOLOCK) INNER JOIN [ContagemResultadoLiqLog] T2 WITH (NOLOCK) ON T2.[ContagemResultadoLiqLog_Codigo] = T1.[ContagemResultadoLiqLog_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultadoLiqLogOS_UserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoLiqLogOS_OSCod] = @AV81Codigo ORDER BY T1.[ContagemResultadoLiqLogOS_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K715,100,0,false,false )
             ,new CursorDef("H00K716", "SELECT T1.[ContagemResultadoLiqLogOS_Codigo], T1.[ContagemResultadoLiqLogOS_UserCod] AS ContagemResultadoLiqLogOS_UserCod, T2.[Usuario_PessoaCod] AS ContagemResultadoLiqLogOS_UserPesCod, T1.[ContagemResultadoLiqLog_Codigo], T1.[ContagemResultadoLiqLogOS_OSCod], T1.[ContagemResultadoLiqLogOS_Valor], T4.[ContagemResultadoLiqLog_Data], T3.[Pessoa_Nome] AS ContagemResultadoLiqLogOS_UserNom FROM ((([ContagemResultadoLiqLogOS] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoLiqLogOS_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultadoLiqLog] T4 WITH (NOLOCK) ON T4.[ContagemResultadoLiqLog_Codigo] = T1.[ContagemResultadoLiqLog_Codigo]) WHERE (T1.[ContagemResultadoLiqLogOS_OSCod] = @AV81Codigo) AND (T1.[ContagemResultadoLiqLog_Codigo] <> @AV16ContagemResultado_LiqLogCod) ORDER BY T1.[ContagemResultadoLiqLogOS_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K716,100,0,false,false )
             ,new CursorDef("H00K718", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00K718,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((bool[]) buf[15])[0] = rslt.getBool(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((decimal[]) buf[21])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getString(17, 15) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((String[]) buf[31])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((int[]) buf[33])[0] = rslt.getInt(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((String[]) buf[37])[0] = rslt.getString(21, 15) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((DateTime[]) buf[39])[0] = rslt.getGXDateTime(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((String[]) buf[41])[0] = rslt.getString(23, 1) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((String[]) buf[43])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(24);
                ((String[]) buf[45])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(25);
                ((String[]) buf[47])[0] = rslt.getString(26, 15) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(26);
                ((String[]) buf[49])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(27);
                ((int[]) buf[51])[0] = rslt.getInt(28) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(28);
                ((decimal[]) buf[53])[0] = rslt.getDecimal(29) ;
                ((decimal[]) buf[54])[0] = rslt.getDecimal(30) ;
                ((decimal[]) buf[55])[0] = rslt.getDecimal(31) ;
                ((int[]) buf[56])[0] = rslt.getInt(32) ;
                ((DateTime[]) buf[57])[0] = rslt.getGXDate(33) ;
                ((String[]) buf[58])[0] = rslt.getString(34, 5) ;
                ((int[]) buf[59])[0] = rslt.getInt(35) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDateTime(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(7) ;
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(17) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[71]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[75]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[77]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[79]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[84]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[88]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[90]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[91]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[92]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[94]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[95]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[98]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[99]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[100]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[104]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[106]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[107]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[109]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[110]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[111]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[112]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[114]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[117]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[120]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[121]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[71]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[75]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[77]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[79]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[84]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[86]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[87]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[88]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[90]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[91]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[92]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[94]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[95]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[97]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[98]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[100]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[104]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[106]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[107]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[109]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[110]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[111]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[112]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[114]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[117]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[120]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[121]);
                }
                return;
       }
    }

 }

}
