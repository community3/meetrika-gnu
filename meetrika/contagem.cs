/*
               File: Contagem
        Description: Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:1.82
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_29") == 0 )
         {
            A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_29( A192Contagem_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_30") == 0 )
         {
            A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_30( A192Contagem_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_24") == 0 )
         {
            A193Contagem_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_24( A193Contagem_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_25") == 0 )
         {
            A213Contagem_UsuarioContadorCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n213Contagem_UsuarioContadorCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_25( A213Contagem_UsuarioContadorCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_28") == 0 )
         {
            A214Contagem_UsuarioContadorPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n214Contagem_UsuarioContadorPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_28( A214Contagem_UsuarioContadorPessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_26") == 0 )
         {
            A940Contagem_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n940Contagem_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_26( A940Contagem_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_27") == 0 )
         {
            A939Contagem_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n939Contagem_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_27( A939Contagem_ProjetoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV20Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contagem_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV20Contagem_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContagem_Tecnica.Name = "CONTAGEM_TECNICA";
         cmbContagem_Tecnica.WebTags = "";
         cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
         cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
         cmbContagem_Tecnica.addItem("2", "Estimada", 0);
         cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
         if ( cmbContagem_Tecnica.ItemCount > 0 )
         {
            A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
         }
         cmbContagem_Tipo.Name = "CONTAGEM_TIPO";
         cmbContagem_Tipo.WebTags = "";
         cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
         cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
         cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
         cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
         cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
         if ( cmbContagem_Tipo.ItemCount > 0 )
         {
            A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
         }
         cmbContagem_Status.Name = "CONTAGEM_STATUS";
         cmbContagem_Status.WebTags = "";
         if ( cmbContagem_Status.ItemCount > 0 )
         {
            A262Contagem_Status = cmbContagem_Status.getValidValue(A262Contagem_Status);
            n262Contagem_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A262Contagem_Status", A262Contagem_Status);
         }
         dynContagem_ProjetoCod.Name = "CONTAGEM_PROJETOCOD";
         dynContagem_ProjetoCod.WebTags = "";
         dynContagem_ProjetoCod.removeAllItems();
         /* Using cursor T001613 */
         pr_default.execute(9);
         while ( (pr_default.getStatus(9) != 101) )
         {
            dynContagem_ProjetoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T001613_A648Projeto_Codigo[0]), 6, 0)), T001613_A650Projeto_Sigla[0], 0);
            pr_default.readNext(9);
         }
         pr_default.close(9);
         if ( dynContagem_ProjetoCod.ItemCount > 0 )
         {
            A939Contagem_ProjetoCod = (int)(NumberUtil.Val( dynContagem_ProjetoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0))), "."));
            n939Contagem_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
         }
         cmbContagem_Lock.Name = "CONTAGEM_LOCK";
         cmbContagem_Lock.WebTags = "";
         cmbContagem_Lock.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbContagem_Lock.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbContagem_Lock.ItemCount > 0 )
         {
            A948Contagem_Lock = StringUtil.StrToBool( cmbContagem_Lock.getValidValue(StringUtil.BoolToStr( A948Contagem_Lock)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A948Contagem_Lock", A948Contagem_Lock);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Contagem_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV20Contagem_Codigo = aP1_Contagem_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContagem_Tecnica = new GXCombobox();
         cmbContagem_Tipo = new GXCombobox();
         cmbContagem_Status = new GXCombobox();
         dynContagem_ProjetoCod = new GXCombobox();
         cmbContagem_Lock = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagem_Tecnica.ItemCount > 0 )
         {
            A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
         }
         if ( cmbContagem_Tipo.ItemCount > 0 )
         {
            A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
         }
         if ( cmbContagem_Status.ItemCount > 0 )
         {
            A262Contagem_Status = cmbContagem_Status.getValidValue(A262Contagem_Status);
            n262Contagem_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A262Contagem_Status", A262Contagem_Status);
         }
         if ( dynContagem_ProjetoCod.ItemCount > 0 )
         {
            A939Contagem_ProjetoCod = (int)(NumberUtil.Val( dynContagem_ProjetoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0))), "."));
            n939Contagem_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
         }
         if ( cmbContagem_Lock.ItemCount > 0 )
         {
            A948Contagem_Lock = StringUtil.StrToBool( cmbContagem_Lock.getValidValue(StringUtil.BoolToStr( A948Contagem_Lock)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A948Contagem_Lock", A948Contagem_Lock);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1643( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1643e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1643( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemtitle_Internalname, "Contagem ", "", "", lblContagemtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_1643( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_1643e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_239_1643( true) ;
         }
         return  ;
      }

      protected void wb_table3_239_1643e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1643e( true) ;
         }
         else
         {
            wb_table1_2_1643e( false) ;
         }
      }

      protected void wb_table3_239_1643( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 242,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 244,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 246,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_239_1643e( true) ;
         }
         else
         {
            wb_table3_239_1643e( false) ;
         }
      }

      protected void wb_table2_8_1643( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_1643( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_1643e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_1643e( true) ;
         }
         else
         {
            wb_table2_8_1643e( false) ;
         }
      }

      protected void wb_table4_16_1643( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_codigo_Internalname, "C�digo", "", "", lblTextblockcontagem_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")), ((edtContagem_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhocod_Internalname, "Cod. �rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AreaTrabalhoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_AreaTrabalhoCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_193_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_193_Link, "", "", context.GetTheme( ), imgprompt_193_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_contratadacod_Internalname, "Contratada", "", "", lblTextblockcontagem_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1118Contagem_ContratadaCod), 6, 0, ",", "")), ((edtContagem_ContratadaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1118Contagem_ContratadaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ContratadaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_ContratadaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_datacriacao_Internalname, "Data da contagem", "", "", lblTextblockcontagem_datacriacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagem_DataCriacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagem_DataCriacao_Internalname, context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"), context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_DataCriacao_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContagem_DataCriacao_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_Contagem.htm");
            GxWebStd.gx_bitmap( context, edtContagem_DataCriacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagem_DataCriacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhodes_Internalname, "�rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhodes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoDes_Internalname, A194Contagem_AreaTrabalhoDes, StringUtil.RTrim( context.localUtil.Format( A194Contagem_AreaTrabalhoDes, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AreaTrabalhoDes_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_AreaTrabalhoDes_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tecnica_Internalname, "T�cnica de Contagem", "", "", lblTextblockcontagem_tecnica_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tecnica, cmbContagem_Tecnica_Internalname, StringUtil.RTrim( A195Contagem_Tecnica), 1, cmbContagem_Tecnica_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagem_Tecnica.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_Contagem.htm");
            cmbContagem_Tecnica.CurrentValue = StringUtil.RTrim( A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Values", (String)(cmbContagem_Tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tipo_Internalname, "Tipo de Contagem", "", "", lblTextblockcontagem_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tipo, cmbContagem_Tipo_Internalname, StringUtil.RTrim( A196Contagem_Tipo), 1, cmbContagem_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagem_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_Contagem.htm");
            cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_proposito_Internalname, "Prop�sito", "", "", lblTextblockcontagem_proposito_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEM_PROPOSITOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_escopo_Internalname, "da Contagem", "", "", lblTextblockcontagem_escopo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEM_ESCOPOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_fronteira_Internalname, "Fronteira", "", "", lblTextblockcontagem_fronteira_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEM_FRONTEIRAContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_observacao_Internalname, "Oberserva��es", "", "", lblTextblockcontagem_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEM_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_notas_Internalname, "Notas", "", "", lblTextblockcontagem_notas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Notas_Internalname, A1059Contagem_Notas, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", 1, 1, edtContagem_Notas_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", 0, true, "Html", "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfb_Internalname, "PFB", "", "", lblTextblockcontagem_pfb_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_PFB_Internalname, StringUtil.LTrim( StringUtil.NToC( A943Contagem_PFB, 13, 5, ",", "")), ((edtContagem_PFB_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")) : context.localUtil.Format( A943Contagem_PFB, "ZZZZZZ9.99999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFB_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_PFB_Enabled, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfl_Internalname, "PFL", "", "", lblTextblockcontagem_pfl_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_PFL_Internalname, StringUtil.LTrim( StringUtil.NToC( A944Contagem_PFL, 13, 5, ",", "")), ((edtContagem_PFL_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")) : context.localUtil.Format( A944Contagem_PFL, "ZZZZZZ9.99999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFL_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_PFL_Enabled, 0, "text", "", 13, "chr", 1, "row", 13, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_divergencia_Internalname, "Contagem_Divergencia", "", "", lblTextblockcontagem_divergencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_Divergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( A1119Contagem_Divergencia, 6, 2, ",", "")), ((edtContagem_Divergencia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99")) : context.localUtil.Format( A1119Contagem_Divergencia, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Divergencia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_Divergencia_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorcod_Internalname, "Usu�rio Contador", "", "", lblTextblockcontagem_usuariocontadorcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_UsuarioContadorCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_213_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_213_Link, "", "", context.GetTheme( ), imgprompt_213_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorpessoacod_Internalname, "Pessoa Contador", "", "", lblTextblockcontagem_usuariocontadorpessoacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ",", "")), ((edtContagem_UsuarioContadorPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorPessoaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_UsuarioContadorPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorpessoanom_Internalname, "do Contador", "", "", lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaNom_Internalname, StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom), StringUtil.RTrim( context.localUtil.Format( A215Contagem_UsuarioContadorPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorPessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_UsuarioContadorPessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_status_Internalname, "Status", "", "", lblTextblockcontagem_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Status, cmbContagem_Status_Internalname, StringUtil.RTrim( A262Contagem_Status), 1, cmbContagem_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagem_Status.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", "", true, "HLP_Contagem.htm");
            cmbContagem_Status.CurrentValue = StringUtil.RTrim( A262Contagem_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Status_Internalname, "Values", (String)(cmbContagem_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_demanda_Internalname, "Demanda", "", "", lblTextblockcontagem_demanda_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_Demanda_Internalname, A945Contagem_Demanda, StringUtil.RTrim( context.localUtil.Format( A945Contagem_Demanda, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Demanda_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_Demanda_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_link_Internalname, "Link", "", "", lblTextblockcontagem_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Link_Internalname, A946Contagem_Link, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, 1, edtContagem_Link_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "255", -1, "", "", -1, true, "", "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_fator_Internalname, "Fator", "", "", lblTextblockcontagem_fator_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_Fator_Internalname, StringUtil.LTrim( StringUtil.NToC( A947Contagem_Fator, 4, 2, ",", "")), ((edtContagem_Fator_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A947Contagem_Fator, "9.99")) : context.localUtil.Format( A947Contagem_Fator, "9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Fator_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_Fator_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_deflator_Internalname, "Deflator", "", "", lblTextblockcontagem_deflator_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_Deflator_Internalname, StringUtil.LTrim( StringUtil.NToC( A1117Contagem_Deflator, 6, 2, ",", "")), ((edtContagem_Deflator_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99")) : context.localUtil.Format( A1117Contagem_Deflator, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Deflator_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_Deflator_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontagem_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A940Contagem_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_SistemaCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemasigla_Internalname, "Sistema", "", "", lblTextblockcontagem_sistemasigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaSigla_Internalname, StringUtil.RTrim( A941Contagem_SistemaSigla), StringUtil.RTrim( context.localUtil.Format( A941Contagem_SistemaSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_SistemaSigla_Enabled, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_sistemacoord_Internalname, "gestora", "", "", lblTextblockcontagem_sistemacoord_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_SistemaCoord_Internalname, A949Contagem_SistemaCoord, StringUtil.RTrim( context.localUtil.Format( A949Contagem_SistemaCoord, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_SistemaCoord_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_SistemaCoord_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_projetocod_Internalname, "Projeto", "", "", lblTextblockcontagem_projetocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagem_ProjetoCod, dynContagem_ProjetoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)), 1, dynContagem_ProjetoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagem_ProjetoCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", "", true, "HLP_Contagem.htm");
            dynContagem_ProjetoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagem_ProjetoCod_Internalname, "Values", (String)(dynContagem_ProjetoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_projetosigla_Internalname, "Projeto", "", "", lblTextblockcontagem_projetosigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_ProjetoSigla_Internalname, StringUtil.RTrim( A942Contagem_ProjetoSigla), StringUtil.RTrim( context.localUtil.Format( A942Contagem_ProjetoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ProjetoSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_ProjetoSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_qtditens_Internalname, "de Itens", "", "", lblTextblockcontagem_qtditens_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_QtdItens_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A264Contagem_QtdItens), 3, 0, ",", "")), ((edtContagem_QtdItens_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A264Contagem_QtdItens), "ZZ9")) : context.localUtil.Format( (decimal)(A264Contagem_QtdItens), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_QtdItens_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_QtdItens_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_qtditensaprovados_Internalname, "Itens Aprovados", "", "", lblTextblockcontagem_qtditensaprovados_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_QtdItensAprovados_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A265Contagem_QtdItensAprovados), 3, 0, ",", "")), ((edtContagem_QtdItensAprovados_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A265Contagem_QtdItensAprovados), "ZZ9")) : context.localUtil.Format( (decimal)(A265Contagem_QtdItensAprovados), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_QtdItensAprovados_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_QtdItensAprovados_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_lock_Internalname, "Bloqueado", "", "", lblTextblockcontagem_lock_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Lock, cmbContagem_Lock_Internalname, StringUtil.BoolToStr( A948Contagem_Lock), 1, cmbContagem_Lock_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbContagem_Lock.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,171);\"", "", true, "HLP_Contagem.htm");
            cmbContagem_Lock.CurrentValue = StringUtil.BoolToStr( A948Contagem_Lock);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Lock_Internalname, "Values", (String)(cmbContagem_Lock.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_datahomologacao_Internalname, "Homologa��o", "", "", lblTextblockcontagem_datahomologacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagem_DataHomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagem_DataHomologacao_Internalname, context.localUtil.TToC( A1111Contagem_DataHomologacao, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1111Contagem_DataHomologacao, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,176);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_DataHomologacao_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagem_DataHomologacao_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Contagem.htm");
            GxWebStd.gx_bitmap( context, edtContagem_DataHomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagem_DataHomologacao_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Contagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_consideracoes_Internalname, "Considera��es", "", "", lblTextblockcontagem_consideracoes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 181,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Consideracoes_Internalname, A1112Contagem_Consideracoes, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,181);\"", 0, 1, edtContagem_Consideracoes_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_descricao_Internalname, "Descricao", "", "", lblTextblockcontagem_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Descricao_Internalname, A1120Contagem_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,186);\"", 0, 1, edtContagem_Descricao_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfba_Internalname, "PFBA", "", "", lblTextblockcontagem_pfba_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 191,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_PFBA_Internalname, StringUtil.LTrim( StringUtil.NToC( A1113Contagem_PFBA, 14, 5, ",", "")), ((edtContagem_PFBA_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1113Contagem_PFBA, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1113Contagem_PFBA, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,191);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFBA_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_PFBA_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfla_Internalname, "PFLA", "", "", lblTextblockcontagem_pfla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 196,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_PFLA_Internalname, StringUtil.LTrim( StringUtil.NToC( A1114Contagem_PFLA, 14, 5, ",", "")), ((edtContagem_PFLA_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1114Contagem_PFLA, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1114Contagem_PFLA, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,196);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFLA_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_PFLA_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfbd_Internalname, "PFBD", "", "", lblTextblockcontagem_pfbd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 201,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_PFBD_Internalname, StringUtil.LTrim( StringUtil.NToC( A1115Contagem_PFBD, 14, 5, ",", "")), ((edtContagem_PFBD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1115Contagem_PFBD, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1115Contagem_PFBD, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,201);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFBD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_PFBD_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_pfld_Internalname, "PFLD", "", "", lblTextblockcontagem_pfld_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 206,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_PFLD_Internalname, StringUtil.LTrim( StringUtil.NToC( A1116Contagem_PFLD, 14, 5, ",", "")), ((edtContagem_PFLD_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1116Contagem_PFLD, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1116Contagem_PFLD, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,206);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_PFLD_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_PFLD_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_aplicabilidade_Internalname, "Contagem_Aplicabilidade", "", "", lblTextblockcontagem_aplicabilidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 211,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_Aplicabilidade_Internalname, A1809Contagem_Aplicabilidade, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,211);\"", 0, 1, edtContagem_Aplicabilidade_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "1048576", -1, "", "", -1, true, "", "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_versao_Internalname, "Contagem_Versao", "", "", lblTextblockcontagem_versao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 216,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_Versao_Internalname, A1810Contagem_Versao, StringUtil.RTrim( context.localUtil.Format( A1810Contagem_Versao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,216);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Versao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_Versao_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_servicocod_Internalname, "Cod", "", "", lblTextblockcontagem_servicocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 221,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1811Contagem_ServicoCod), 6, 0, ",", "")), ((edtContagem_ServicoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1811Contagem_ServicoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1811Contagem_ServicoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,221);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ServicoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_ServicoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_arquivoimp_Internalname, "Imp", "", "", lblTextblockcontagem_arquivoimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 226,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagem_ArquivoImp_Internalname, A1812Contagem_ArquivoImp, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,226);\"", 0, 1, edtContagem_ArquivoImp_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_referenciainm_Internalname, "INM", "", "", lblTextblockcontagem_referenciainm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 231,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_ReferenciaINM_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1813Contagem_ReferenciaINM), 6, 0, ",", "")), ((edtContagem_ReferenciaINM_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1813Contagem_ReferenciaINM), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1813Contagem_ReferenciaINM), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,231);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_ReferenciaINM_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_ReferenciaINM_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_ambientetecnologico_Internalname, "Tecnologico", "", "", lblTextblockcontagem_ambientetecnologico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 236,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagem_AmbienteTecnologico_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0, ",", "")), ((edtContagem_AmbienteTecnologico_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1814Contagem_AmbienteTecnologico), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1814Contagem_AmbienteTecnologico), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,236);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AmbienteTecnologico_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagem_AmbienteTecnologico_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Contagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_1643e( true) ;
         }
         else
         {
            wb_table4_16_1643e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11162 */
         E11162 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_AREATRABALHOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A193Contagem_AreaTrabalhoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
               }
               else
               {
                  A193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_ContratadaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_ContratadaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_CONTRATADACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_ContratadaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1118Contagem_ContratadaCod = 0;
                  n1118Contagem_ContratadaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
               }
               else
               {
                  A1118Contagem_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ContratadaCod_Internalname), ",", "."));
                  n1118Contagem_ContratadaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
               }
               n1118Contagem_ContratadaCod = ((0==A1118Contagem_ContratadaCod) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContagem_DataCriacao_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Cria��o"}), 1, "CONTAGEM_DATACRIACAO");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_DataCriacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A197Contagem_DataCriacao = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               }
               else
               {
                  A197Contagem_DataCriacao = context.localUtil.CToD( cgiGet( edtContagem_DataCriacao_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               }
               A194Contagem_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContagem_AreaTrabalhoDes_Internalname));
               n194Contagem_AreaTrabalhoDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
               cmbContagem_Tecnica.CurrentValue = cgiGet( cmbContagem_Tecnica_Internalname);
               A195Contagem_Tecnica = cgiGet( cmbContagem_Tecnica_Internalname);
               n195Contagem_Tecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
               n195Contagem_Tecnica = (String.IsNullOrEmpty(StringUtil.RTrim( A195Contagem_Tecnica)) ? true : false);
               cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
               A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
               n196Contagem_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
               n196Contagem_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A196Contagem_Tipo)) ? true : false);
               A1059Contagem_Notas = cgiGet( edtContagem_Notas_Internalname);
               n1059Contagem_Notas = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1059Contagem_Notas", A1059Contagem_Notas);
               n1059Contagem_Notas = (String.IsNullOrEmpty(StringUtil.RTrim( A1059Contagem_Notas)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".") > 9999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_PFB");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_PFB_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A943Contagem_PFB = 0;
                  n943Contagem_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
               }
               else
               {
                  A943Contagem_PFB = context.localUtil.CToN( cgiGet( edtContagem_PFB_Internalname), ",", ".");
                  n943Contagem_PFB = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
               }
               n943Contagem_PFB = ((Convert.ToDecimal(0)==A943Contagem_PFB) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".") > 9999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_PFL");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_PFL_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A944Contagem_PFL = 0;
                  n944Contagem_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
               }
               else
               {
                  A944Contagem_PFL = context.localUtil.CToN( cgiGet( edtContagem_PFL_Internalname), ",", ".");
                  n944Contagem_PFL = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
               }
               n944Contagem_PFL = ((Convert.ToDecimal(0)==A944Contagem_PFL) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_Divergencia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_Divergencia_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_DIVERGENCIA");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_Divergencia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1119Contagem_Divergencia = 0;
                  n1119Contagem_Divergencia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.Str( A1119Contagem_Divergencia, 6, 2)));
               }
               else
               {
                  A1119Contagem_Divergencia = context.localUtil.CToN( cgiGet( edtContagem_Divergencia_Internalname), ",", ".");
                  n1119Contagem_Divergencia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.Str( A1119Contagem_Divergencia, 6, 2)));
               }
               n1119Contagem_Divergencia = ((Convert.ToDecimal(0)==A1119Contagem_Divergencia) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_USUARIOCONTADORCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_UsuarioContadorCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A213Contagem_UsuarioContadorCod = 0;
                  n213Contagem_UsuarioContadorCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               }
               else
               {
                  A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
                  n213Contagem_UsuarioContadorCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               }
               n213Contagem_UsuarioContadorCod = ((0==A213Contagem_UsuarioContadorCod) ? true : false);
               A214Contagem_UsuarioContadorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorPessoaCod_Internalname), ",", "."));
               n214Contagem_UsuarioContadorPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
               A215Contagem_UsuarioContadorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioContadorPessoaNom_Internalname));
               n215Contagem_UsuarioContadorPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
               cmbContagem_Status.CurrentValue = cgiGet( cmbContagem_Status_Internalname);
               A262Contagem_Status = cgiGet( cmbContagem_Status_Internalname);
               n262Contagem_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A262Contagem_Status", A262Contagem_Status);
               n262Contagem_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A262Contagem_Status)) ? true : false);
               A945Contagem_Demanda = cgiGet( edtContagem_Demanda_Internalname);
               n945Contagem_Demanda = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A945Contagem_Demanda", A945Contagem_Demanda);
               n945Contagem_Demanda = (String.IsNullOrEmpty(StringUtil.RTrim( A945Contagem_Demanda)) ? true : false);
               A946Contagem_Link = cgiGet( edtContagem_Link_Internalname);
               n946Contagem_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A946Contagem_Link", A946Contagem_Link);
               n946Contagem_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A946Contagem_Link)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_Fator_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_Fator_Internalname), ",", ".") > 9.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_FATOR");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_Fator_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A947Contagem_Fator = 0;
                  n947Contagem_Fator = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A947Contagem_Fator", StringUtil.LTrim( StringUtil.Str( A947Contagem_Fator, 4, 2)));
               }
               else
               {
                  A947Contagem_Fator = context.localUtil.CToN( cgiGet( edtContagem_Fator_Internalname), ",", ".");
                  n947Contagem_Fator = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A947Contagem_Fator", StringUtil.LTrim( StringUtil.Str( A947Contagem_Fator, 4, 2)));
               }
               n947Contagem_Fator = ((Convert.ToDecimal(0)==A947Contagem_Fator) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_Deflator_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_Deflator_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_DEFLATOR");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_Deflator_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1117Contagem_Deflator = 0;
                  n1117Contagem_Deflator = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1117Contagem_Deflator", StringUtil.LTrim( StringUtil.Str( A1117Contagem_Deflator, 6, 2)));
               }
               else
               {
                  A1117Contagem_Deflator = context.localUtil.CToN( cgiGet( edtContagem_Deflator_Internalname), ",", ".");
                  n1117Contagem_Deflator = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1117Contagem_Deflator", StringUtil.LTrim( StringUtil.Str( A1117Contagem_Deflator, 6, 2)));
               }
               n1117Contagem_Deflator = ((Convert.ToDecimal(0)==A1117Contagem_Deflator) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A940Contagem_SistemaCod = 0;
                  n940Contagem_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
               }
               else
               {
                  A940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_SistemaCod_Internalname), ",", "."));
                  n940Contagem_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
               }
               n940Contagem_SistemaCod = ((0==A940Contagem_SistemaCod) ? true : false);
               A941Contagem_SistemaSigla = StringUtil.Upper( cgiGet( edtContagem_SistemaSigla_Internalname));
               n941Contagem_SistemaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
               A949Contagem_SistemaCoord = StringUtil.Upper( cgiGet( edtContagem_SistemaCoord_Internalname));
               n949Contagem_SistemaCoord = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
               dynContagem_ProjetoCod.CurrentValue = cgiGet( dynContagem_ProjetoCod_Internalname);
               A939Contagem_ProjetoCod = (int)(NumberUtil.Val( cgiGet( dynContagem_ProjetoCod_Internalname), "."));
               n939Contagem_ProjetoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
               n939Contagem_ProjetoCod = ((0==A939Contagem_ProjetoCod) ? true : false);
               A942Contagem_ProjetoSigla = StringUtil.Upper( cgiGet( edtContagem_ProjetoSigla_Internalname));
               n942Contagem_ProjetoSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
               A264Contagem_QtdItens = (short)(context.localUtil.CToN( cgiGet( edtContagem_QtdItens_Internalname), ",", "."));
               n264Contagem_QtdItens = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
               A265Contagem_QtdItensAprovados = (short)(context.localUtil.CToN( cgiGet( edtContagem_QtdItensAprovados_Internalname), ",", "."));
               n265Contagem_QtdItensAprovados = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
               cmbContagem_Lock.CurrentValue = cgiGet( cmbContagem_Lock_Internalname);
               A948Contagem_Lock = StringUtil.StrToBool( cgiGet( cmbContagem_Lock_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A948Contagem_Lock", A948Contagem_Lock);
               if ( context.localUtil.VCDateTime( cgiGet( edtContagem_DataHomologacao_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data Homologa��o"}), 1, "CONTAGEM_DATAHOMOLOGACAO");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_DataHomologacao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1111Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
                  n1111Contagem_DataHomologacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1111Contagem_DataHomologacao", context.localUtil.TToC( A1111Contagem_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1111Contagem_DataHomologacao = context.localUtil.CToT( cgiGet( edtContagem_DataHomologacao_Internalname));
                  n1111Contagem_DataHomologacao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1111Contagem_DataHomologacao", context.localUtil.TToC( A1111Contagem_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
               }
               n1111Contagem_DataHomologacao = ((DateTime.MinValue==A1111Contagem_DataHomologacao) ? true : false);
               A1112Contagem_Consideracoes = cgiGet( edtContagem_Consideracoes_Internalname);
               n1112Contagem_Consideracoes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1112Contagem_Consideracoes", A1112Contagem_Consideracoes);
               n1112Contagem_Consideracoes = (String.IsNullOrEmpty(StringUtil.RTrim( A1112Contagem_Consideracoes)) ? true : false);
               A1120Contagem_Descricao = cgiGet( edtContagem_Descricao_Internalname);
               n1120Contagem_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1120Contagem_Descricao", A1120Contagem_Descricao);
               n1120Contagem_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1120Contagem_Descricao)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_PFBA_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_PFBA_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_PFBA");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_PFBA_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1113Contagem_PFBA = 0;
                  n1113Contagem_PFBA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1113Contagem_PFBA", StringUtil.LTrim( StringUtil.Str( A1113Contagem_PFBA, 14, 5)));
               }
               else
               {
                  A1113Contagem_PFBA = context.localUtil.CToN( cgiGet( edtContagem_PFBA_Internalname), ",", ".");
                  n1113Contagem_PFBA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1113Contagem_PFBA", StringUtil.LTrim( StringUtil.Str( A1113Contagem_PFBA, 14, 5)));
               }
               n1113Contagem_PFBA = ((Convert.ToDecimal(0)==A1113Contagem_PFBA) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_PFLA_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_PFLA_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_PFLA");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_PFLA_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1114Contagem_PFLA = 0;
                  n1114Contagem_PFLA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1114Contagem_PFLA", StringUtil.LTrim( StringUtil.Str( A1114Contagem_PFLA, 14, 5)));
               }
               else
               {
                  A1114Contagem_PFLA = context.localUtil.CToN( cgiGet( edtContagem_PFLA_Internalname), ",", ".");
                  n1114Contagem_PFLA = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1114Contagem_PFLA", StringUtil.LTrim( StringUtil.Str( A1114Contagem_PFLA, 14, 5)));
               }
               n1114Contagem_PFLA = ((Convert.ToDecimal(0)==A1114Contagem_PFLA) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_PFBD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_PFBD_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_PFBD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_PFBD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1115Contagem_PFBD = 0;
                  n1115Contagem_PFBD = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1115Contagem_PFBD", StringUtil.LTrim( StringUtil.Str( A1115Contagem_PFBD, 14, 5)));
               }
               else
               {
                  A1115Contagem_PFBD = context.localUtil.CToN( cgiGet( edtContagem_PFBD_Internalname), ",", ".");
                  n1115Contagem_PFBD = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1115Contagem_PFBD", StringUtil.LTrim( StringUtil.Str( A1115Contagem_PFBD, 14, 5)));
               }
               n1115Contagem_PFBD = ((Convert.ToDecimal(0)==A1115Contagem_PFBD) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_PFLD_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_PFLD_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_PFLD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_PFLD_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1116Contagem_PFLD = 0;
                  n1116Contagem_PFLD = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1116Contagem_PFLD", StringUtil.LTrim( StringUtil.Str( A1116Contagem_PFLD, 14, 5)));
               }
               else
               {
                  A1116Contagem_PFLD = context.localUtil.CToN( cgiGet( edtContagem_PFLD_Internalname), ",", ".");
                  n1116Contagem_PFLD = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1116Contagem_PFLD", StringUtil.LTrim( StringUtil.Str( A1116Contagem_PFLD, 14, 5)));
               }
               n1116Contagem_PFLD = ((Convert.ToDecimal(0)==A1116Contagem_PFLD) ? true : false);
               A1809Contagem_Aplicabilidade = cgiGet( edtContagem_Aplicabilidade_Internalname);
               n1809Contagem_Aplicabilidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1809Contagem_Aplicabilidade", A1809Contagem_Aplicabilidade);
               n1809Contagem_Aplicabilidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1809Contagem_Aplicabilidade)) ? true : false);
               A1810Contagem_Versao = cgiGet( edtContagem_Versao_Internalname);
               n1810Contagem_Versao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1810Contagem_Versao", A1810Contagem_Versao);
               n1810Contagem_Versao = (String.IsNullOrEmpty(StringUtil.RTrim( A1810Contagem_Versao)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_ServicoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_ServicoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_SERVICOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_ServicoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1811Contagem_ServicoCod = 0;
                  n1811Contagem_ServicoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
               }
               else
               {
                  A1811Contagem_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_ServicoCod_Internalname), ",", "."));
                  n1811Contagem_ServicoCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
               }
               n1811Contagem_ServicoCod = ((0==A1811Contagem_ServicoCod) ? true : false);
               A1812Contagem_ArquivoImp = cgiGet( edtContagem_ArquivoImp_Internalname);
               n1812Contagem_ArquivoImp = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1812Contagem_ArquivoImp", A1812Contagem_ArquivoImp);
               n1812Contagem_ArquivoImp = (String.IsNullOrEmpty(StringUtil.RTrim( A1812Contagem_ArquivoImp)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_ReferenciaINM_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_ReferenciaINM_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_REFERENCIAINM");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_ReferenciaINM_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1813Contagem_ReferenciaINM = 0;
                  n1813Contagem_ReferenciaINM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A1813Contagem_ReferenciaINM), 6, 0)));
               }
               else
               {
                  A1813Contagem_ReferenciaINM = (int)(context.localUtil.CToN( cgiGet( edtContagem_ReferenciaINM_Internalname), ",", "."));
                  n1813Contagem_ReferenciaINM = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A1813Contagem_ReferenciaINM), 6, 0)));
               }
               n1813Contagem_ReferenciaINM = ((0==A1813Contagem_ReferenciaINM) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagem_AmbienteTecnologico_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagem_AmbienteTecnologico_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEM_AMBIENTETECNOLOGICO");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_AmbienteTecnologico_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1814Contagem_AmbienteTecnologico = 0;
                  n1814Contagem_AmbienteTecnologico = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0)));
               }
               else
               {
                  A1814Contagem_AmbienteTecnologico = (int)(context.localUtil.CToN( cgiGet( edtContagem_AmbienteTecnologico_Internalname), ",", "."));
                  n1814Contagem_AmbienteTecnologico = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0)));
               }
               n1814Contagem_AmbienteTecnologico = ((0==A1814Contagem_AmbienteTecnologico) ? true : false);
               /* Read saved values. */
               Z192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z192Contagem_Codigo"), ",", "."));
               Z1118Contagem_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "Z1118Contagem_ContratadaCod"), ",", "."));
               n1118Contagem_ContratadaCod = ((0==A1118Contagem_ContratadaCod) ? true : false);
               Z197Contagem_DataCriacao = context.localUtil.CToD( cgiGet( "Z197Contagem_DataCriacao"), 0);
               Z195Contagem_Tecnica = cgiGet( "Z195Contagem_Tecnica");
               n195Contagem_Tecnica = (String.IsNullOrEmpty(StringUtil.RTrim( A195Contagem_Tecnica)) ? true : false);
               Z196Contagem_Tipo = cgiGet( "Z196Contagem_Tipo");
               n196Contagem_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A196Contagem_Tipo)) ? true : false);
               Z943Contagem_PFB = context.localUtil.CToN( cgiGet( "Z943Contagem_PFB"), ",", ".");
               n943Contagem_PFB = ((Convert.ToDecimal(0)==A943Contagem_PFB) ? true : false);
               Z944Contagem_PFL = context.localUtil.CToN( cgiGet( "Z944Contagem_PFL"), ",", ".");
               n944Contagem_PFL = ((Convert.ToDecimal(0)==A944Contagem_PFL) ? true : false);
               Z1119Contagem_Divergencia = context.localUtil.CToN( cgiGet( "Z1119Contagem_Divergencia"), ",", ".");
               n1119Contagem_Divergencia = ((Convert.ToDecimal(0)==A1119Contagem_Divergencia) ? true : false);
               Z262Contagem_Status = cgiGet( "Z262Contagem_Status");
               n262Contagem_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A262Contagem_Status)) ? true : false);
               Z945Contagem_Demanda = cgiGet( "Z945Contagem_Demanda");
               n945Contagem_Demanda = (String.IsNullOrEmpty(StringUtil.RTrim( A945Contagem_Demanda)) ? true : false);
               Z946Contagem_Link = cgiGet( "Z946Contagem_Link");
               n946Contagem_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A946Contagem_Link)) ? true : false);
               Z947Contagem_Fator = context.localUtil.CToN( cgiGet( "Z947Contagem_Fator"), ",", ".");
               n947Contagem_Fator = ((Convert.ToDecimal(0)==A947Contagem_Fator) ? true : false);
               Z1117Contagem_Deflator = context.localUtil.CToN( cgiGet( "Z1117Contagem_Deflator"), ",", ".");
               n1117Contagem_Deflator = ((Convert.ToDecimal(0)==A1117Contagem_Deflator) ? true : false);
               Z948Contagem_Lock = StringUtil.StrToBool( cgiGet( "Z948Contagem_Lock"));
               Z1111Contagem_DataHomologacao = context.localUtil.CToT( cgiGet( "Z1111Contagem_DataHomologacao"), 0);
               n1111Contagem_DataHomologacao = ((DateTime.MinValue==A1111Contagem_DataHomologacao) ? true : false);
               Z1120Contagem_Descricao = cgiGet( "Z1120Contagem_Descricao");
               n1120Contagem_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1120Contagem_Descricao)) ? true : false);
               Z1113Contagem_PFBA = context.localUtil.CToN( cgiGet( "Z1113Contagem_PFBA"), ",", ".");
               n1113Contagem_PFBA = ((Convert.ToDecimal(0)==A1113Contagem_PFBA) ? true : false);
               Z1114Contagem_PFLA = context.localUtil.CToN( cgiGet( "Z1114Contagem_PFLA"), ",", ".");
               n1114Contagem_PFLA = ((Convert.ToDecimal(0)==A1114Contagem_PFLA) ? true : false);
               Z1115Contagem_PFBD = context.localUtil.CToN( cgiGet( "Z1115Contagem_PFBD"), ",", ".");
               n1115Contagem_PFBD = ((Convert.ToDecimal(0)==A1115Contagem_PFBD) ? true : false);
               Z1116Contagem_PFLD = context.localUtil.CToN( cgiGet( "Z1116Contagem_PFLD"), ",", ".");
               n1116Contagem_PFLD = ((Convert.ToDecimal(0)==A1116Contagem_PFLD) ? true : false);
               Z1810Contagem_Versao = cgiGet( "Z1810Contagem_Versao");
               n1810Contagem_Versao = (String.IsNullOrEmpty(StringUtil.RTrim( A1810Contagem_Versao)) ? true : false);
               Z1811Contagem_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1811Contagem_ServicoCod"), ",", "."));
               n1811Contagem_ServicoCod = ((0==A1811Contagem_ServicoCod) ? true : false);
               Z1812Contagem_ArquivoImp = cgiGet( "Z1812Contagem_ArquivoImp");
               n1812Contagem_ArquivoImp = (String.IsNullOrEmpty(StringUtil.RTrim( A1812Contagem_ArquivoImp)) ? true : false);
               Z1813Contagem_ReferenciaINM = (int)(context.localUtil.CToN( cgiGet( "Z1813Contagem_ReferenciaINM"), ",", "."));
               n1813Contagem_ReferenciaINM = ((0==A1813Contagem_ReferenciaINM) ? true : false);
               Z1814Contagem_AmbienteTecnologico = (int)(context.localUtil.CToN( cgiGet( "Z1814Contagem_AmbienteTecnologico"), ",", "."));
               n1814Contagem_AmbienteTecnologico = ((0==A1814Contagem_AmbienteTecnologico) ? true : false);
               Z193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z193Contagem_AreaTrabalhoCod"), ",", "."));
               Z213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( "Z213Contagem_UsuarioContadorCod"), ",", "."));
               n213Contagem_UsuarioContadorCod = ((0==A213Contagem_UsuarioContadorCod) ? true : false);
               Z940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z940Contagem_SistemaCod"), ",", "."));
               n940Contagem_SistemaCod = ((0==A940Contagem_SistemaCod) ? true : false);
               Z939Contagem_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "Z939Contagem_ProjetoCod"), ",", "."));
               n939Contagem_ProjetoCod = ((0==A939Contagem_ProjetoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N193Contagem_AreaTrabalhoCod"), ",", "."));
               N213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( "N213Contagem_UsuarioContadorCod"), ",", "."));
               n213Contagem_UsuarioContadorCod = ((0==A213Contagem_UsuarioContadorCod) ? true : false);
               N940Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "N940Contagem_SistemaCod"), ",", "."));
               n940Contagem_SistemaCod = ((0==A940Contagem_SistemaCod) ? true : false);
               N939Contagem_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "N939Contagem_ProjetoCod"), ",", "."));
               n939Contagem_ProjetoCod = ((0==A939Contagem_ProjetoCod) ? true : false);
               AV20Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTAGEM_CODIGO"), ",", "."));
               AV11Insert_Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEM_AREATRABALHOCOD"), ",", "."));
               AV15Insert_Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEM_USUARIOCONTADORCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV18Insert_Contagem_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEM_SISTEMACOD"), ",", "."));
               AV19Insert_Contagem_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEM_PROJETOCOD"), ",", "."));
               A199Contagem_Proposito = cgiGet( "CONTAGEM_PROPOSITO");
               n199Contagem_Proposito = false;
               n199Contagem_Proposito = (String.IsNullOrEmpty(StringUtil.RTrim( A199Contagem_Proposito)) ? true : false);
               A200Contagem_Escopo = cgiGet( "CONTAGEM_ESCOPO");
               n200Contagem_Escopo = false;
               n200Contagem_Escopo = (String.IsNullOrEmpty(StringUtil.RTrim( A200Contagem_Escopo)) ? true : false);
               A201Contagem_Fronteira = cgiGet( "CONTAGEM_FRONTEIRA");
               n201Contagem_Fronteira = false;
               n201Contagem_Fronteira = (String.IsNullOrEmpty(StringUtil.RTrim( A201Contagem_Fronteira)) ? true : false);
               A202Contagem_Observacao = cgiGet( "CONTAGEM_OBSERVACAO");
               n202Contagem_Observacao = false;
               n202Contagem_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A202Contagem_Observacao)) ? true : false);
               AV22Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Contagem_proposito_Width = cgiGet( "CONTAGEM_PROPOSITO_Width");
               Contagem_proposito_Height = cgiGet( "CONTAGEM_PROPOSITO_Height");
               Contagem_proposito_Skin = cgiGet( "CONTAGEM_PROPOSITO_Skin");
               Contagem_proposito_Toolbar = cgiGet( "CONTAGEM_PROPOSITO_Toolbar");
               Contagem_proposito_Color = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_PROPOSITO_Color"), ",", "."));
               Contagem_proposito_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEM_PROPOSITO_Enabled"));
               Contagem_proposito_Class = cgiGet( "CONTAGEM_PROPOSITO_Class");
               Contagem_proposito_Customtoolbar = cgiGet( "CONTAGEM_PROPOSITO_Customtoolbar");
               Contagem_proposito_Customconfiguration = cgiGet( "CONTAGEM_PROPOSITO_Customconfiguration");
               Contagem_proposito_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "CONTAGEM_PROPOSITO_Toolbarcancollapse"));
               Contagem_proposito_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "CONTAGEM_PROPOSITO_Toolbarexpanded"));
               Contagem_proposito_Buttonpressedid = cgiGet( "CONTAGEM_PROPOSITO_Buttonpressedid");
               Contagem_proposito_Captionvalue = cgiGet( "CONTAGEM_PROPOSITO_Captionvalue");
               Contagem_proposito_Captionclass = cgiGet( "CONTAGEM_PROPOSITO_Captionclass");
               Contagem_proposito_Captionposition = cgiGet( "CONTAGEM_PROPOSITO_Captionposition");
               Contagem_proposito_Coltitle = cgiGet( "CONTAGEM_PROPOSITO_Coltitle");
               Contagem_proposito_Coltitlefont = cgiGet( "CONTAGEM_PROPOSITO_Coltitlefont");
               Contagem_proposito_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_PROPOSITO_Coltitlecolor"), ",", "."));
               Contagem_proposito_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "CONTAGEM_PROPOSITO_Usercontroliscolumn"));
               Contagem_proposito_Visible = StringUtil.StrToBool( cgiGet( "CONTAGEM_PROPOSITO_Visible"));
               Contagem_escopo_Width = cgiGet( "CONTAGEM_ESCOPO_Width");
               Contagem_escopo_Height = cgiGet( "CONTAGEM_ESCOPO_Height");
               Contagem_escopo_Skin = cgiGet( "CONTAGEM_ESCOPO_Skin");
               Contagem_escopo_Toolbar = cgiGet( "CONTAGEM_ESCOPO_Toolbar");
               Contagem_escopo_Color = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_ESCOPO_Color"), ",", "."));
               Contagem_escopo_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEM_ESCOPO_Enabled"));
               Contagem_escopo_Class = cgiGet( "CONTAGEM_ESCOPO_Class");
               Contagem_escopo_Customtoolbar = cgiGet( "CONTAGEM_ESCOPO_Customtoolbar");
               Contagem_escopo_Customconfiguration = cgiGet( "CONTAGEM_ESCOPO_Customconfiguration");
               Contagem_escopo_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "CONTAGEM_ESCOPO_Toolbarcancollapse"));
               Contagem_escopo_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "CONTAGEM_ESCOPO_Toolbarexpanded"));
               Contagem_escopo_Buttonpressedid = cgiGet( "CONTAGEM_ESCOPO_Buttonpressedid");
               Contagem_escopo_Captionvalue = cgiGet( "CONTAGEM_ESCOPO_Captionvalue");
               Contagem_escopo_Captionclass = cgiGet( "CONTAGEM_ESCOPO_Captionclass");
               Contagem_escopo_Captionposition = cgiGet( "CONTAGEM_ESCOPO_Captionposition");
               Contagem_escopo_Coltitle = cgiGet( "CONTAGEM_ESCOPO_Coltitle");
               Contagem_escopo_Coltitlefont = cgiGet( "CONTAGEM_ESCOPO_Coltitlefont");
               Contagem_escopo_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_ESCOPO_Coltitlecolor"), ",", "."));
               Contagem_escopo_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "CONTAGEM_ESCOPO_Usercontroliscolumn"));
               Contagem_escopo_Visible = StringUtil.StrToBool( cgiGet( "CONTAGEM_ESCOPO_Visible"));
               Contagem_fronteira_Width = cgiGet( "CONTAGEM_FRONTEIRA_Width");
               Contagem_fronteira_Height = cgiGet( "CONTAGEM_FRONTEIRA_Height");
               Contagem_fronteira_Skin = cgiGet( "CONTAGEM_FRONTEIRA_Skin");
               Contagem_fronteira_Toolbar = cgiGet( "CONTAGEM_FRONTEIRA_Toolbar");
               Contagem_fronteira_Color = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_FRONTEIRA_Color"), ",", "."));
               Contagem_fronteira_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEM_FRONTEIRA_Enabled"));
               Contagem_fronteira_Class = cgiGet( "CONTAGEM_FRONTEIRA_Class");
               Contagem_fronteira_Customtoolbar = cgiGet( "CONTAGEM_FRONTEIRA_Customtoolbar");
               Contagem_fronteira_Customconfiguration = cgiGet( "CONTAGEM_FRONTEIRA_Customconfiguration");
               Contagem_fronteira_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "CONTAGEM_FRONTEIRA_Toolbarcancollapse"));
               Contagem_fronteira_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "CONTAGEM_FRONTEIRA_Toolbarexpanded"));
               Contagem_fronteira_Buttonpressedid = cgiGet( "CONTAGEM_FRONTEIRA_Buttonpressedid");
               Contagem_fronteira_Captionvalue = cgiGet( "CONTAGEM_FRONTEIRA_Captionvalue");
               Contagem_fronteira_Captionclass = cgiGet( "CONTAGEM_FRONTEIRA_Captionclass");
               Contagem_fronteira_Captionposition = cgiGet( "CONTAGEM_FRONTEIRA_Captionposition");
               Contagem_fronteira_Coltitle = cgiGet( "CONTAGEM_FRONTEIRA_Coltitle");
               Contagem_fronteira_Coltitlefont = cgiGet( "CONTAGEM_FRONTEIRA_Coltitlefont");
               Contagem_fronteira_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_FRONTEIRA_Coltitlecolor"), ",", "."));
               Contagem_fronteira_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "CONTAGEM_FRONTEIRA_Usercontroliscolumn"));
               Contagem_fronteira_Visible = StringUtil.StrToBool( cgiGet( "CONTAGEM_FRONTEIRA_Visible"));
               Contagem_observacao_Width = cgiGet( "CONTAGEM_OBSERVACAO_Width");
               Contagem_observacao_Height = cgiGet( "CONTAGEM_OBSERVACAO_Height");
               Contagem_observacao_Skin = cgiGet( "CONTAGEM_OBSERVACAO_Skin");
               Contagem_observacao_Toolbar = cgiGet( "CONTAGEM_OBSERVACAO_Toolbar");
               Contagem_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_OBSERVACAO_Color"), ",", "."));
               Contagem_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEM_OBSERVACAO_Enabled"));
               Contagem_observacao_Class = cgiGet( "CONTAGEM_OBSERVACAO_Class");
               Contagem_observacao_Customtoolbar = cgiGet( "CONTAGEM_OBSERVACAO_Customtoolbar");
               Contagem_observacao_Customconfiguration = cgiGet( "CONTAGEM_OBSERVACAO_Customconfiguration");
               Contagem_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "CONTAGEM_OBSERVACAO_Toolbarcancollapse"));
               Contagem_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "CONTAGEM_OBSERVACAO_Toolbarexpanded"));
               Contagem_observacao_Buttonpressedid = cgiGet( "CONTAGEM_OBSERVACAO_Buttonpressedid");
               Contagem_observacao_Captionvalue = cgiGet( "CONTAGEM_OBSERVACAO_Captionvalue");
               Contagem_observacao_Captionclass = cgiGet( "CONTAGEM_OBSERVACAO_Captionclass");
               Contagem_observacao_Captionposition = cgiGet( "CONTAGEM_OBSERVACAO_Captionposition");
               Contagem_observacao_Coltitle = cgiGet( "CONTAGEM_OBSERVACAO_Coltitle");
               Contagem_observacao_Coltitlefont = cgiGet( "CONTAGEM_OBSERVACAO_Coltitlefont");
               Contagem_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "CONTAGEM_OBSERVACAO_Coltitlecolor"), ",", "."));
               Contagem_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "CONTAGEM_OBSERVACAO_Usercontroliscolumn"));
               Contagem_observacao_Visible = StringUtil.StrToBool( cgiGet( "CONTAGEM_OBSERVACAO_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Contagem";
               A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A192Contagem_Codigo != Z192Contagem_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagem:[SecurityCheckFailed value for]"+"Contagem_Codigo:"+context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contagem:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A192Contagem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode43 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode43;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound43 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_160( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTAGEM_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContagem_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11162 */
                           E11162 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12162 */
                           E12162 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12162 */
            E12162 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1643( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1643( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_160( )
      {
         BeforeValidate1643( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1643( ) ;
            }
            else
            {
               CheckExtendedTable1643( ) ;
               CloseExtendedTableCursors1643( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption160( )
      {
      }

      protected void E11162( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV22Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV23GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23GXV1), 8, 0)));
            while ( AV23GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV17TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV23GXV1));
               if ( StringUtil.StrCmp(AV17TrnContextAtt.gxTpr_Attributename, "Contagem_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Contagem_AreaTrabalhoCod = (int)(NumberUtil.Val( AV17TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contagem_AreaTrabalhoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV17TrnContextAtt.gxTpr_Attributename, "Contagem_UsuarioContadorCod") == 0 )
               {
                  AV15Insert_Contagem_UsuarioContadorCod = (int)(NumberUtil.Val( AV17TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_Contagem_UsuarioContadorCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV17TrnContextAtt.gxTpr_Attributename, "Contagem_SistemaCod") == 0 )
               {
                  AV18Insert_Contagem_SistemaCod = (int)(NumberUtil.Val( AV17TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Insert_Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Insert_Contagem_SistemaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV17TrnContextAtt.gxTpr_Attributename, "Contagem_ProjetoCod") == 0 )
               {
                  AV19Insert_Contagem_ProjetoCod = (int)(NumberUtil.Val( AV17TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Insert_Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Insert_Contagem_ProjetoCod), 6, 0)));
               }
               AV23GXV1 = (int)(AV23GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23GXV1), 8, 0)));
            }
         }
      }

      protected void E12162( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +A192Contagem_Codigo);
            context.wjLocDisableFrm = 1;
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontagem.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1643( short GX_JID )
      {
         if ( ( GX_JID == 22 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1118Contagem_ContratadaCod = T00163_A1118Contagem_ContratadaCod[0];
               Z197Contagem_DataCriacao = T00163_A197Contagem_DataCriacao[0];
               Z195Contagem_Tecnica = T00163_A195Contagem_Tecnica[0];
               Z196Contagem_Tipo = T00163_A196Contagem_Tipo[0];
               Z943Contagem_PFB = T00163_A943Contagem_PFB[0];
               Z944Contagem_PFL = T00163_A944Contagem_PFL[0];
               Z1119Contagem_Divergencia = T00163_A1119Contagem_Divergencia[0];
               Z262Contagem_Status = T00163_A262Contagem_Status[0];
               Z945Contagem_Demanda = T00163_A945Contagem_Demanda[0];
               Z946Contagem_Link = T00163_A946Contagem_Link[0];
               Z947Contagem_Fator = T00163_A947Contagem_Fator[0];
               Z1117Contagem_Deflator = T00163_A1117Contagem_Deflator[0];
               Z948Contagem_Lock = T00163_A948Contagem_Lock[0];
               Z1111Contagem_DataHomologacao = T00163_A1111Contagem_DataHomologacao[0];
               Z1120Contagem_Descricao = T00163_A1120Contagem_Descricao[0];
               Z1113Contagem_PFBA = T00163_A1113Contagem_PFBA[0];
               Z1114Contagem_PFLA = T00163_A1114Contagem_PFLA[0];
               Z1115Contagem_PFBD = T00163_A1115Contagem_PFBD[0];
               Z1116Contagem_PFLD = T00163_A1116Contagem_PFLD[0];
               Z1810Contagem_Versao = T00163_A1810Contagem_Versao[0];
               Z1811Contagem_ServicoCod = T00163_A1811Contagem_ServicoCod[0];
               Z1812Contagem_ArquivoImp = T00163_A1812Contagem_ArquivoImp[0];
               Z1813Contagem_ReferenciaINM = T00163_A1813Contagem_ReferenciaINM[0];
               Z1814Contagem_AmbienteTecnologico = T00163_A1814Contagem_AmbienteTecnologico[0];
               Z193Contagem_AreaTrabalhoCod = T00163_A193Contagem_AreaTrabalhoCod[0];
               Z213Contagem_UsuarioContadorCod = T00163_A213Contagem_UsuarioContadorCod[0];
               Z940Contagem_SistemaCod = T00163_A940Contagem_SistemaCod[0];
               Z939Contagem_ProjetoCod = T00163_A939Contagem_ProjetoCod[0];
            }
            else
            {
               Z1118Contagem_ContratadaCod = A1118Contagem_ContratadaCod;
               Z197Contagem_DataCriacao = A197Contagem_DataCriacao;
               Z195Contagem_Tecnica = A195Contagem_Tecnica;
               Z196Contagem_Tipo = A196Contagem_Tipo;
               Z943Contagem_PFB = A943Contagem_PFB;
               Z944Contagem_PFL = A944Contagem_PFL;
               Z1119Contagem_Divergencia = A1119Contagem_Divergencia;
               Z262Contagem_Status = A262Contagem_Status;
               Z945Contagem_Demanda = A945Contagem_Demanda;
               Z946Contagem_Link = A946Contagem_Link;
               Z947Contagem_Fator = A947Contagem_Fator;
               Z1117Contagem_Deflator = A1117Contagem_Deflator;
               Z948Contagem_Lock = A948Contagem_Lock;
               Z1111Contagem_DataHomologacao = A1111Contagem_DataHomologacao;
               Z1120Contagem_Descricao = A1120Contagem_Descricao;
               Z1113Contagem_PFBA = A1113Contagem_PFBA;
               Z1114Contagem_PFLA = A1114Contagem_PFLA;
               Z1115Contagem_PFBD = A1115Contagem_PFBD;
               Z1116Contagem_PFLD = A1116Contagem_PFLD;
               Z1810Contagem_Versao = A1810Contagem_Versao;
               Z1811Contagem_ServicoCod = A1811Contagem_ServicoCod;
               Z1812Contagem_ArquivoImp = A1812Contagem_ArquivoImp;
               Z1813Contagem_ReferenciaINM = A1813Contagem_ReferenciaINM;
               Z1814Contagem_AmbienteTecnologico = A1814Contagem_AmbienteTecnologico;
               Z193Contagem_AreaTrabalhoCod = A193Contagem_AreaTrabalhoCod;
               Z213Contagem_UsuarioContadorCod = A213Contagem_UsuarioContadorCod;
               Z940Contagem_SistemaCod = A940Contagem_SistemaCod;
               Z939Contagem_ProjetoCod = A939Contagem_ProjetoCod;
            }
         }
         if ( GX_JID == -22 )
         {
            Z192Contagem_Codigo = A192Contagem_Codigo;
            Z1118Contagem_ContratadaCod = A1118Contagem_ContratadaCod;
            Z197Contagem_DataCriacao = A197Contagem_DataCriacao;
            Z195Contagem_Tecnica = A195Contagem_Tecnica;
            Z196Contagem_Tipo = A196Contagem_Tipo;
            Z199Contagem_Proposito = A199Contagem_Proposito;
            Z200Contagem_Escopo = A200Contagem_Escopo;
            Z201Contagem_Fronteira = A201Contagem_Fronteira;
            Z202Contagem_Observacao = A202Contagem_Observacao;
            Z1059Contagem_Notas = A1059Contagem_Notas;
            Z943Contagem_PFB = A943Contagem_PFB;
            Z944Contagem_PFL = A944Contagem_PFL;
            Z1119Contagem_Divergencia = A1119Contagem_Divergencia;
            Z262Contagem_Status = A262Contagem_Status;
            Z945Contagem_Demanda = A945Contagem_Demanda;
            Z946Contagem_Link = A946Contagem_Link;
            Z947Contagem_Fator = A947Contagem_Fator;
            Z1117Contagem_Deflator = A1117Contagem_Deflator;
            Z948Contagem_Lock = A948Contagem_Lock;
            Z1111Contagem_DataHomologacao = A1111Contagem_DataHomologacao;
            Z1112Contagem_Consideracoes = A1112Contagem_Consideracoes;
            Z1120Contagem_Descricao = A1120Contagem_Descricao;
            Z1113Contagem_PFBA = A1113Contagem_PFBA;
            Z1114Contagem_PFLA = A1114Contagem_PFLA;
            Z1115Contagem_PFBD = A1115Contagem_PFBD;
            Z1116Contagem_PFLD = A1116Contagem_PFLD;
            Z1809Contagem_Aplicabilidade = A1809Contagem_Aplicabilidade;
            Z1810Contagem_Versao = A1810Contagem_Versao;
            Z1811Contagem_ServicoCod = A1811Contagem_ServicoCod;
            Z1812Contagem_ArquivoImp = A1812Contagem_ArquivoImp;
            Z1813Contagem_ReferenciaINM = A1813Contagem_ReferenciaINM;
            Z1814Contagem_AmbienteTecnologico = A1814Contagem_AmbienteTecnologico;
            Z193Contagem_AreaTrabalhoCod = A193Contagem_AreaTrabalhoCod;
            Z213Contagem_UsuarioContadorCod = A213Contagem_UsuarioContadorCod;
            Z940Contagem_SistemaCod = A940Contagem_SistemaCod;
            Z939Contagem_ProjetoCod = A939Contagem_ProjetoCod;
            Z264Contagem_QtdItens = A264Contagem_QtdItens;
            Z265Contagem_QtdItensAprovados = A265Contagem_QtdItensAprovados;
            Z194Contagem_AreaTrabalhoDes = A194Contagem_AreaTrabalhoDes;
            Z214Contagem_UsuarioContadorPessoaCod = A214Contagem_UsuarioContadorPessoaCod;
            Z215Contagem_UsuarioContadorPessoaNom = A215Contagem_UsuarioContadorPessoaNom;
            Z941Contagem_SistemaSigla = A941Contagem_SistemaSigla;
            Z949Contagem_SistemaCoord = A949Contagem_SistemaCoord;
            Z942Contagem_ProjetoSigla = A942Contagem_ProjetoSigla;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Enabled), 5, 0)));
         AV22Pgmname = "Contagem";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pgmname", AV22Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         imgprompt_193_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptareatrabalho.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEM_AREATRABALHOCOD"+"'), id:'"+"CONTAGEM_AREATRABALHOCOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTAGEM_AREATRABALHODES"+"'), id:'"+"CONTAGEM_AREATRABALHODES"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_213_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptusuario.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEM_USUARIOCONTADORCOD"+"'), id:'"+"CONTAGEM_USUARIOCONTADORCOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTAGEM_USUARIOCONTADORPESSOANOM"+"'), id:'"+"CONTAGEM_USUARIOCONTADORPESSOANOM"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtContagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV20Contagem_Codigo) )
         {
            A192Contagem_Codigo = AV20Contagem_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contagem_AreaTrabalhoCod) )
         {
            edtContagem_AreaTrabalhoCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         else
         {
            edtContagem_AreaTrabalhoCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_AreaTrabalhoCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_Contagem_UsuarioContadorCod) )
         {
            edtContagem_UsuarioContadorCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorCod_Enabled), 5, 0)));
         }
         else
         {
            edtContagem_UsuarioContadorCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_Contagem_SistemaCod) )
         {
            edtContagem_SistemaCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_SistemaCod_Enabled), 5, 0)));
         }
         else
         {
            edtContagem_SistemaCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_SistemaCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV19Insert_Contagem_ProjetoCod) )
         {
            dynContagem_ProjetoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagem_ProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagem_ProjetoCod.Enabled), 5, 0)));
         }
         else
         {
            dynContagem_ProjetoCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagem_ProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagem_ProjetoCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV19Insert_Contagem_ProjetoCod) )
         {
            A939Contagem_ProjetoCod = AV19Insert_Contagem_ProjetoCod;
            n939Contagem_ProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_Contagem_SistemaCod) )
         {
            A940Contagem_SistemaCod = AV18Insert_Contagem_SistemaCod;
            n940Contagem_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contagem_AreaTrabalhoCod) )
         {
            A193Contagem_AreaTrabalhoCod = AV11Insert_Contagem_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A193Contagem_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_Contagem_UsuarioContadorCod) )
         {
            A213Contagem_UsuarioContadorCod = AV15Insert_Contagem_UsuarioContadorCod;
            n213Contagem_UsuarioContadorCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A213Contagem_UsuarioContadorCod) && ( Gx_BScreen == 0 ) )
            {
               A213Contagem_UsuarioContadorCod = AV8WWPContext.gxTpr_Userid;
               n213Contagem_UsuarioContadorCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A197Contagem_DataCriacao) && ( Gx_BScreen == 0 ) )
         {
            A197Contagem_DataCriacao = DateTimeUtil.ServerDate( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001610 */
            pr_default.execute(7, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(7) != 101) )
            {
               A264Contagem_QtdItens = T001610_A264Contagem_QtdItens[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
               n264Contagem_QtdItens = T001610_n264Contagem_QtdItens[0];
            }
            else
            {
               A264Contagem_QtdItens = 0;
               n264Contagem_QtdItens = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
            }
            pr_default.close(7);
            /* Using cursor T001612 */
            pr_default.execute(8, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(8) != 101) )
            {
               A265Contagem_QtdItensAprovados = T001612_A265Contagem_QtdItensAprovados[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
               n265Contagem_QtdItensAprovados = T001612_n265Contagem_QtdItensAprovados[0];
            }
            else
            {
               A265Contagem_QtdItensAprovados = 0;
               n265Contagem_QtdItensAprovados = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
            }
            pr_default.close(8);
            /* Using cursor T00167 */
            pr_default.execute(5, new Object[] {n939Contagem_ProjetoCod, A939Contagem_ProjetoCod});
            A942Contagem_ProjetoSigla = T00167_A942Contagem_ProjetoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
            n942Contagem_ProjetoSigla = T00167_n942Contagem_ProjetoSigla[0];
            pr_default.close(5);
            /* Using cursor T00166 */
            pr_default.execute(4, new Object[] {n940Contagem_SistemaCod, A940Contagem_SistemaCod});
            A941Contagem_SistemaSigla = T00166_A941Contagem_SistemaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
            n941Contagem_SistemaSigla = T00166_n941Contagem_SistemaSigla[0];
            A949Contagem_SistemaCoord = T00166_A949Contagem_SistemaCoord[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
            n949Contagem_SistemaCoord = T00166_n949Contagem_SistemaCoord[0];
            pr_default.close(4);
            /* Using cursor T00164 */
            pr_default.execute(2, new Object[] {A193Contagem_AreaTrabalhoCod});
            A194Contagem_AreaTrabalhoDes = T00164_A194Contagem_AreaTrabalhoDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
            n194Contagem_AreaTrabalhoDes = T00164_n194Contagem_AreaTrabalhoDes[0];
            pr_default.close(2);
            /* Using cursor T00165 */
            pr_default.execute(3, new Object[] {n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod});
            A214Contagem_UsuarioContadorPessoaCod = T00165_A214Contagem_UsuarioContadorPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
            n214Contagem_UsuarioContadorPessoaCod = T00165_n214Contagem_UsuarioContadorPessoaCod[0];
            pr_default.close(3);
            /* Using cursor T00168 */
            pr_default.execute(6, new Object[] {n214Contagem_UsuarioContadorPessoaCod, A214Contagem_UsuarioContadorPessoaCod});
            A215Contagem_UsuarioContadorPessoaNom = T00168_A215Contagem_UsuarioContadorPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
            n215Contagem_UsuarioContadorPessoaNom = T00168_n215Contagem_UsuarioContadorPessoaNom[0];
            pr_default.close(6);
         }
      }

      protected void Load1643( )
      {
         /* Using cursor T001616 */
         pr_default.execute(10, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound43 = 1;
            A1118Contagem_ContratadaCod = T001616_A1118Contagem_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
            n1118Contagem_ContratadaCod = T001616_n1118Contagem_ContratadaCod[0];
            A197Contagem_DataCriacao = T001616_A197Contagem_DataCriacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
            A194Contagem_AreaTrabalhoDes = T001616_A194Contagem_AreaTrabalhoDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
            n194Contagem_AreaTrabalhoDes = T001616_n194Contagem_AreaTrabalhoDes[0];
            A195Contagem_Tecnica = T001616_A195Contagem_Tecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
            n195Contagem_Tecnica = T001616_n195Contagem_Tecnica[0];
            A196Contagem_Tipo = T001616_A196Contagem_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
            n196Contagem_Tipo = T001616_n196Contagem_Tipo[0];
            A199Contagem_Proposito = T001616_A199Contagem_Proposito[0];
            n199Contagem_Proposito = T001616_n199Contagem_Proposito[0];
            A200Contagem_Escopo = T001616_A200Contagem_Escopo[0];
            n200Contagem_Escopo = T001616_n200Contagem_Escopo[0];
            A201Contagem_Fronteira = T001616_A201Contagem_Fronteira[0];
            n201Contagem_Fronteira = T001616_n201Contagem_Fronteira[0];
            A202Contagem_Observacao = T001616_A202Contagem_Observacao[0];
            n202Contagem_Observacao = T001616_n202Contagem_Observacao[0];
            A1059Contagem_Notas = T001616_A1059Contagem_Notas[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1059Contagem_Notas", A1059Contagem_Notas);
            n1059Contagem_Notas = T001616_n1059Contagem_Notas[0];
            A943Contagem_PFB = T001616_A943Contagem_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
            n943Contagem_PFB = T001616_n943Contagem_PFB[0];
            A944Contagem_PFL = T001616_A944Contagem_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
            n944Contagem_PFL = T001616_n944Contagem_PFL[0];
            A1119Contagem_Divergencia = T001616_A1119Contagem_Divergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.Str( A1119Contagem_Divergencia, 6, 2)));
            n1119Contagem_Divergencia = T001616_n1119Contagem_Divergencia[0];
            A215Contagem_UsuarioContadorPessoaNom = T001616_A215Contagem_UsuarioContadorPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
            n215Contagem_UsuarioContadorPessoaNom = T001616_n215Contagem_UsuarioContadorPessoaNom[0];
            A262Contagem_Status = T001616_A262Contagem_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A262Contagem_Status", A262Contagem_Status);
            n262Contagem_Status = T001616_n262Contagem_Status[0];
            A945Contagem_Demanda = T001616_A945Contagem_Demanda[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A945Contagem_Demanda", A945Contagem_Demanda);
            n945Contagem_Demanda = T001616_n945Contagem_Demanda[0];
            A946Contagem_Link = T001616_A946Contagem_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A946Contagem_Link", A946Contagem_Link);
            n946Contagem_Link = T001616_n946Contagem_Link[0];
            A947Contagem_Fator = T001616_A947Contagem_Fator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A947Contagem_Fator", StringUtil.LTrim( StringUtil.Str( A947Contagem_Fator, 4, 2)));
            n947Contagem_Fator = T001616_n947Contagem_Fator[0];
            A1117Contagem_Deflator = T001616_A1117Contagem_Deflator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1117Contagem_Deflator", StringUtil.LTrim( StringUtil.Str( A1117Contagem_Deflator, 6, 2)));
            n1117Contagem_Deflator = T001616_n1117Contagem_Deflator[0];
            A941Contagem_SistemaSigla = T001616_A941Contagem_SistemaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
            n941Contagem_SistemaSigla = T001616_n941Contagem_SistemaSigla[0];
            A949Contagem_SistemaCoord = T001616_A949Contagem_SistemaCoord[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
            n949Contagem_SistemaCoord = T001616_n949Contagem_SistemaCoord[0];
            A942Contagem_ProjetoSigla = T001616_A942Contagem_ProjetoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
            n942Contagem_ProjetoSigla = T001616_n942Contagem_ProjetoSigla[0];
            A948Contagem_Lock = T001616_A948Contagem_Lock[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A948Contagem_Lock", A948Contagem_Lock);
            A1111Contagem_DataHomologacao = T001616_A1111Contagem_DataHomologacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1111Contagem_DataHomologacao", context.localUtil.TToC( A1111Contagem_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            n1111Contagem_DataHomologacao = T001616_n1111Contagem_DataHomologacao[0];
            A1112Contagem_Consideracoes = T001616_A1112Contagem_Consideracoes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1112Contagem_Consideracoes", A1112Contagem_Consideracoes);
            n1112Contagem_Consideracoes = T001616_n1112Contagem_Consideracoes[0];
            A1120Contagem_Descricao = T001616_A1120Contagem_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1120Contagem_Descricao", A1120Contagem_Descricao);
            n1120Contagem_Descricao = T001616_n1120Contagem_Descricao[0];
            A1113Contagem_PFBA = T001616_A1113Contagem_PFBA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1113Contagem_PFBA", StringUtil.LTrim( StringUtil.Str( A1113Contagem_PFBA, 14, 5)));
            n1113Contagem_PFBA = T001616_n1113Contagem_PFBA[0];
            A1114Contagem_PFLA = T001616_A1114Contagem_PFLA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1114Contagem_PFLA", StringUtil.LTrim( StringUtil.Str( A1114Contagem_PFLA, 14, 5)));
            n1114Contagem_PFLA = T001616_n1114Contagem_PFLA[0];
            A1115Contagem_PFBD = T001616_A1115Contagem_PFBD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1115Contagem_PFBD", StringUtil.LTrim( StringUtil.Str( A1115Contagem_PFBD, 14, 5)));
            n1115Contagem_PFBD = T001616_n1115Contagem_PFBD[0];
            A1116Contagem_PFLD = T001616_A1116Contagem_PFLD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1116Contagem_PFLD", StringUtil.LTrim( StringUtil.Str( A1116Contagem_PFLD, 14, 5)));
            n1116Contagem_PFLD = T001616_n1116Contagem_PFLD[0];
            A1809Contagem_Aplicabilidade = T001616_A1809Contagem_Aplicabilidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1809Contagem_Aplicabilidade", A1809Contagem_Aplicabilidade);
            n1809Contagem_Aplicabilidade = T001616_n1809Contagem_Aplicabilidade[0];
            A1810Contagem_Versao = T001616_A1810Contagem_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1810Contagem_Versao", A1810Contagem_Versao);
            n1810Contagem_Versao = T001616_n1810Contagem_Versao[0];
            A1811Contagem_ServicoCod = T001616_A1811Contagem_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
            n1811Contagem_ServicoCod = T001616_n1811Contagem_ServicoCod[0];
            A1812Contagem_ArquivoImp = T001616_A1812Contagem_ArquivoImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1812Contagem_ArquivoImp", A1812Contagem_ArquivoImp);
            n1812Contagem_ArquivoImp = T001616_n1812Contagem_ArquivoImp[0];
            A1813Contagem_ReferenciaINM = T001616_A1813Contagem_ReferenciaINM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A1813Contagem_ReferenciaINM), 6, 0)));
            n1813Contagem_ReferenciaINM = T001616_n1813Contagem_ReferenciaINM[0];
            A1814Contagem_AmbienteTecnologico = T001616_A1814Contagem_AmbienteTecnologico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0)));
            n1814Contagem_AmbienteTecnologico = T001616_n1814Contagem_AmbienteTecnologico[0];
            A193Contagem_AreaTrabalhoCod = T001616_A193Contagem_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            A213Contagem_UsuarioContadorCod = T001616_A213Contagem_UsuarioContadorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            n213Contagem_UsuarioContadorCod = T001616_n213Contagem_UsuarioContadorCod[0];
            A940Contagem_SistemaCod = T001616_A940Contagem_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            n940Contagem_SistemaCod = T001616_n940Contagem_SistemaCod[0];
            A939Contagem_ProjetoCod = T001616_A939Contagem_ProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
            n939Contagem_ProjetoCod = T001616_n939Contagem_ProjetoCod[0];
            A214Contagem_UsuarioContadorPessoaCod = T001616_A214Contagem_UsuarioContadorPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
            n214Contagem_UsuarioContadorPessoaCod = T001616_n214Contagem_UsuarioContadorPessoaCod[0];
            A264Contagem_QtdItens = T001616_A264Contagem_QtdItens[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
            n264Contagem_QtdItens = T001616_n264Contagem_QtdItens[0];
            A265Contagem_QtdItensAprovados = T001616_A265Contagem_QtdItensAprovados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
            n265Contagem_QtdItensAprovados = T001616_n265Contagem_QtdItensAprovados[0];
            ZM1643( -22) ;
         }
         pr_default.close(10);
         OnLoadActions1643( ) ;
      }

      protected void OnLoadActions1643( )
      {
      }

      protected void CheckExtendedTable1643( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T001610 */
         pr_default.execute(7, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A264Contagem_QtdItens = T001610_A264Contagem_QtdItens[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
            n264Contagem_QtdItens = T001610_n264Contagem_QtdItens[0];
         }
         else
         {
            A264Contagem_QtdItens = 0;
            n264Contagem_QtdItens = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
         }
         pr_default.close(7);
         /* Using cursor T001612 */
         pr_default.execute(8, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            A265Contagem_QtdItensAprovados = T001612_A265Contagem_QtdItensAprovados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
            n265Contagem_QtdItensAprovados = T001612_n265Contagem_QtdItensAprovados[0];
         }
         else
         {
            A265Contagem_QtdItensAprovados = 0;
            n265Contagem_QtdItensAprovados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
         }
         pr_default.close(8);
         /* Using cursor T001617 */
         pr_default.execute(11, new Object[] {n945Contagem_Demanda, A945Contagem_Demanda, n1118Contagem_ContratadaCod, A1118Contagem_ContratadaCod, A192Contagem_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"Demanda"+","+"Contratada"}), 1, "CONTAGEM_DEMANDA");
            AnyError = 1;
            GX_FocusControl = edtContagem_Demanda_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(11);
         /* Using cursor T00164 */
         pr_default.execute(2, new Object[] {A193Contagem_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTAGEM_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A194Contagem_AreaTrabalhoDes = T00164_A194Contagem_AreaTrabalhoDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
         n194Contagem_AreaTrabalhoDes = T00164_n194Contagem_AreaTrabalhoDes[0];
         pr_default.close(2);
         if ( ! ( (DateTime.MinValue==A197Contagem_DataCriacao) || ( A197Contagem_DataCriacao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data Cria��o fora do intervalo", "OutOfRange", 1, "CONTAGEM_DATACRIACAO");
            AnyError = 1;
            GX_FocusControl = edtContagem_DataCriacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A195Contagem_Tecnica, "1") == 0 ) || ( StringUtil.StrCmp(A195Contagem_Tecnica, "2") == 0 ) || ( StringUtil.StrCmp(A195Contagem_Tecnica, "3") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A195Contagem_Tecnica)) ) )
         {
            GX_msglist.addItem("Campo T�cnica fora do intervalo", "OutOfRange", 1, "CONTAGEM_TECNICA");
            AnyError = 1;
            GX_FocusControl = cmbContagem_Tecnica_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A196Contagem_Tipo, "D") == 0 ) || ( StringUtil.StrCmp(A196Contagem_Tipo, "M") == 0 ) || ( StringUtil.StrCmp(A196Contagem_Tipo, "C") == 0 ) || ( StringUtil.StrCmp(A196Contagem_Tipo, "A") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A196Contagem_Tipo)) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "CONTAGEM_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbContagem_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00165 */
         pr_default.execute(3, new Object[] {n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A213Contagem_UsuarioContadorCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contador'.", "ForeignKeyNotFound", 1, "CONTAGEM_USUARIOCONTADORCOD");
               AnyError = 1;
               GX_FocusControl = edtContagem_UsuarioContadorCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A214Contagem_UsuarioContadorPessoaCod = T00165_A214Contagem_UsuarioContadorPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
         n214Contagem_UsuarioContadorPessoaCod = T00165_n214Contagem_UsuarioContadorPessoaCod[0];
         pr_default.close(3);
         /* Using cursor T00168 */
         pr_default.execute(6, new Object[] {n214Contagem_UsuarioContadorPessoaCod, A214Contagem_UsuarioContadorPessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A214Contagem_UsuarioContadorPessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A215Contagem_UsuarioContadorPessoaNom = T00168_A215Contagem_UsuarioContadorPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
         n215Contagem_UsuarioContadorPessoaNom = T00168_n215Contagem_UsuarioContadorPessoaNom[0];
         pr_default.close(6);
         /* Using cursor T00166 */
         pr_default.execute(4, new Object[] {n940Contagem_SistemaCod, A940Contagem_SistemaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A940Contagem_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem_Sistema'.", "ForeignKeyNotFound", 1, "CONTAGEM_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtContagem_SistemaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A941Contagem_SistemaSigla = T00166_A941Contagem_SistemaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
         n941Contagem_SistemaSigla = T00166_n941Contagem_SistemaSigla[0];
         A949Contagem_SistemaCoord = T00166_A949Contagem_SistemaCoord[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
         n949Contagem_SistemaCoord = T00166_n949Contagem_SistemaCoord[0];
         pr_default.close(4);
         /* Using cursor T00167 */
         pr_default.execute(5, new Object[] {n939Contagem_ProjetoCod, A939Contagem_ProjetoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A939Contagem_ProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem_Projeto'.", "ForeignKeyNotFound", 1, "CONTAGEM_PROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynContagem_ProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A942Contagem_ProjetoSigla = T00167_A942Contagem_ProjetoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
         n942Contagem_ProjetoSigla = T00167_n942Contagem_ProjetoSigla[0];
         pr_default.close(5);
         if ( ! ( (DateTime.MinValue==A1111Contagem_DataHomologacao) || ( A1111Contagem_DataHomologacao >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data Homologa��o fora do intervalo", "OutOfRange", 1, "CONTAGEM_DATAHOMOLOGACAO");
            AnyError = 1;
            GX_FocusControl = edtContagem_DataHomologacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors1643( )
      {
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(6);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_29( int A192Contagem_Codigo )
      {
         /* Using cursor T001619 */
         pr_default.execute(12, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            A264Contagem_QtdItens = T001619_A264Contagem_QtdItens[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
            n264Contagem_QtdItens = T001619_n264Contagem_QtdItens[0];
         }
         else
         {
            A264Contagem_QtdItens = 0;
            n264Contagem_QtdItens = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A264Contagem_QtdItens), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_30( int A192Contagem_Codigo )
      {
         /* Using cursor T001621 */
         pr_default.execute(13, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            A265Contagem_QtdItensAprovados = T001621_A265Contagem_QtdItensAprovados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
            n265Contagem_QtdItensAprovados = T001621_n265Contagem_QtdItensAprovados[0];
         }
         else
         {
            A265Contagem_QtdItensAprovados = 0;
            n265Contagem_QtdItensAprovados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A265Contagem_QtdItensAprovados), 3, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_24( int A193Contagem_AreaTrabalhoCod )
      {
         /* Using cursor T001622 */
         pr_default.execute(14, new Object[] {A193Contagem_AreaTrabalhoCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTAGEM_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A194Contagem_AreaTrabalhoDes = T001622_A194Contagem_AreaTrabalhoDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
         n194Contagem_AreaTrabalhoDes = T001622_n194Contagem_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A194Contagem_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_25( int A213Contagem_UsuarioContadorCod )
      {
         /* Using cursor T001623 */
         pr_default.execute(15, new Object[] {n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A213Contagem_UsuarioContadorCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contador'.", "ForeignKeyNotFound", 1, "CONTAGEM_USUARIOCONTADORCOD");
               AnyError = 1;
               GX_FocusControl = edtContagem_UsuarioContadorCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A214Contagem_UsuarioContadorPessoaCod = T001623_A214Contagem_UsuarioContadorPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
         n214Contagem_UsuarioContadorPessoaCod = T001623_n214Contagem_UsuarioContadorPessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_28( int A214Contagem_UsuarioContadorPessoaCod )
      {
         /* Using cursor T001624 */
         pr_default.execute(16, new Object[] {n214Contagem_UsuarioContadorPessoaCod, A214Contagem_UsuarioContadorPessoaCod});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A214Contagem_UsuarioContadorPessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A215Contagem_UsuarioContadorPessoaNom = T001624_A215Contagem_UsuarioContadorPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
         n215Contagem_UsuarioContadorPessoaNom = T001624_n215Contagem_UsuarioContadorPessoaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void gxLoad_26( int A940Contagem_SistemaCod )
      {
         /* Using cursor T001625 */
         pr_default.execute(17, new Object[] {n940Contagem_SistemaCod, A940Contagem_SistemaCod});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A940Contagem_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem_Sistema'.", "ForeignKeyNotFound", 1, "CONTAGEM_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtContagem_SistemaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A941Contagem_SistemaSigla = T001625_A941Contagem_SistemaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
         n941Contagem_SistemaSigla = T001625_n941Contagem_SistemaSigla[0];
         A949Contagem_SistemaCoord = T001625_A949Contagem_SistemaCoord[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
         n949Contagem_SistemaCoord = T001625_n949Contagem_SistemaCoord[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A941Contagem_SistemaSigla))+"\""+","+"\""+GXUtil.EncodeJSConstant( A949Contagem_SistemaCoord)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void gxLoad_27( int A939Contagem_ProjetoCod )
      {
         /* Using cursor T001626 */
         pr_default.execute(18, new Object[] {n939Contagem_ProjetoCod, A939Contagem_ProjetoCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            if ( ! ( (0==A939Contagem_ProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem_Projeto'.", "ForeignKeyNotFound", 1, "CONTAGEM_PROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynContagem_ProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A942Contagem_ProjetoSigla = T001626_A942Contagem_ProjetoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
         n942Contagem_ProjetoSigla = T001626_n942Contagem_ProjetoSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A942Contagem_ProjetoSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void GetKey1643( )
      {
         /* Using cursor T001627 */
         pr_default.execute(19, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound43 = 1;
         }
         else
         {
            RcdFound43 = 0;
         }
         pr_default.close(19);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00163 */
         pr_default.execute(1, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1643( 22) ;
            RcdFound43 = 1;
            A192Contagem_Codigo = T00163_A192Contagem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            A1118Contagem_ContratadaCod = T00163_A1118Contagem_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
            n1118Contagem_ContratadaCod = T00163_n1118Contagem_ContratadaCod[0];
            A197Contagem_DataCriacao = T00163_A197Contagem_DataCriacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
            A195Contagem_Tecnica = T00163_A195Contagem_Tecnica[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
            n195Contagem_Tecnica = T00163_n195Contagem_Tecnica[0];
            A196Contagem_Tipo = T00163_A196Contagem_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
            n196Contagem_Tipo = T00163_n196Contagem_Tipo[0];
            A199Contagem_Proposito = T00163_A199Contagem_Proposito[0];
            n199Contagem_Proposito = T00163_n199Contagem_Proposito[0];
            A200Contagem_Escopo = T00163_A200Contagem_Escopo[0];
            n200Contagem_Escopo = T00163_n200Contagem_Escopo[0];
            A201Contagem_Fronteira = T00163_A201Contagem_Fronteira[0];
            n201Contagem_Fronteira = T00163_n201Contagem_Fronteira[0];
            A202Contagem_Observacao = T00163_A202Contagem_Observacao[0];
            n202Contagem_Observacao = T00163_n202Contagem_Observacao[0];
            A1059Contagem_Notas = T00163_A1059Contagem_Notas[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1059Contagem_Notas", A1059Contagem_Notas);
            n1059Contagem_Notas = T00163_n1059Contagem_Notas[0];
            A943Contagem_PFB = T00163_A943Contagem_PFB[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
            n943Contagem_PFB = T00163_n943Contagem_PFB[0];
            A944Contagem_PFL = T00163_A944Contagem_PFL[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
            n944Contagem_PFL = T00163_n944Contagem_PFL[0];
            A1119Contagem_Divergencia = T00163_A1119Contagem_Divergencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.Str( A1119Contagem_Divergencia, 6, 2)));
            n1119Contagem_Divergencia = T00163_n1119Contagem_Divergencia[0];
            A262Contagem_Status = T00163_A262Contagem_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A262Contagem_Status", A262Contagem_Status);
            n262Contagem_Status = T00163_n262Contagem_Status[0];
            A945Contagem_Demanda = T00163_A945Contagem_Demanda[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A945Contagem_Demanda", A945Contagem_Demanda);
            n945Contagem_Demanda = T00163_n945Contagem_Demanda[0];
            A946Contagem_Link = T00163_A946Contagem_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A946Contagem_Link", A946Contagem_Link);
            n946Contagem_Link = T00163_n946Contagem_Link[0];
            A947Contagem_Fator = T00163_A947Contagem_Fator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A947Contagem_Fator", StringUtil.LTrim( StringUtil.Str( A947Contagem_Fator, 4, 2)));
            n947Contagem_Fator = T00163_n947Contagem_Fator[0];
            A1117Contagem_Deflator = T00163_A1117Contagem_Deflator[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1117Contagem_Deflator", StringUtil.LTrim( StringUtil.Str( A1117Contagem_Deflator, 6, 2)));
            n1117Contagem_Deflator = T00163_n1117Contagem_Deflator[0];
            A948Contagem_Lock = T00163_A948Contagem_Lock[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A948Contagem_Lock", A948Contagem_Lock);
            A1111Contagem_DataHomologacao = T00163_A1111Contagem_DataHomologacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1111Contagem_DataHomologacao", context.localUtil.TToC( A1111Contagem_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
            n1111Contagem_DataHomologacao = T00163_n1111Contagem_DataHomologacao[0];
            A1112Contagem_Consideracoes = T00163_A1112Contagem_Consideracoes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1112Contagem_Consideracoes", A1112Contagem_Consideracoes);
            n1112Contagem_Consideracoes = T00163_n1112Contagem_Consideracoes[0];
            A1120Contagem_Descricao = T00163_A1120Contagem_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1120Contagem_Descricao", A1120Contagem_Descricao);
            n1120Contagem_Descricao = T00163_n1120Contagem_Descricao[0];
            A1113Contagem_PFBA = T00163_A1113Contagem_PFBA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1113Contagem_PFBA", StringUtil.LTrim( StringUtil.Str( A1113Contagem_PFBA, 14, 5)));
            n1113Contagem_PFBA = T00163_n1113Contagem_PFBA[0];
            A1114Contagem_PFLA = T00163_A1114Contagem_PFLA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1114Contagem_PFLA", StringUtil.LTrim( StringUtil.Str( A1114Contagem_PFLA, 14, 5)));
            n1114Contagem_PFLA = T00163_n1114Contagem_PFLA[0];
            A1115Contagem_PFBD = T00163_A1115Contagem_PFBD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1115Contagem_PFBD", StringUtil.LTrim( StringUtil.Str( A1115Contagem_PFBD, 14, 5)));
            n1115Contagem_PFBD = T00163_n1115Contagem_PFBD[0];
            A1116Contagem_PFLD = T00163_A1116Contagem_PFLD[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1116Contagem_PFLD", StringUtil.LTrim( StringUtil.Str( A1116Contagem_PFLD, 14, 5)));
            n1116Contagem_PFLD = T00163_n1116Contagem_PFLD[0];
            A1809Contagem_Aplicabilidade = T00163_A1809Contagem_Aplicabilidade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1809Contagem_Aplicabilidade", A1809Contagem_Aplicabilidade);
            n1809Contagem_Aplicabilidade = T00163_n1809Contagem_Aplicabilidade[0];
            A1810Contagem_Versao = T00163_A1810Contagem_Versao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1810Contagem_Versao", A1810Contagem_Versao);
            n1810Contagem_Versao = T00163_n1810Contagem_Versao[0];
            A1811Contagem_ServicoCod = T00163_A1811Contagem_ServicoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
            n1811Contagem_ServicoCod = T00163_n1811Contagem_ServicoCod[0];
            A1812Contagem_ArquivoImp = T00163_A1812Contagem_ArquivoImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1812Contagem_ArquivoImp", A1812Contagem_ArquivoImp);
            n1812Contagem_ArquivoImp = T00163_n1812Contagem_ArquivoImp[0];
            A1813Contagem_ReferenciaINM = T00163_A1813Contagem_ReferenciaINM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A1813Contagem_ReferenciaINM), 6, 0)));
            n1813Contagem_ReferenciaINM = T00163_n1813Contagem_ReferenciaINM[0];
            A1814Contagem_AmbienteTecnologico = T00163_A1814Contagem_AmbienteTecnologico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0)));
            n1814Contagem_AmbienteTecnologico = T00163_n1814Contagem_AmbienteTecnologico[0];
            A193Contagem_AreaTrabalhoCod = T00163_A193Contagem_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            A213Contagem_UsuarioContadorCod = T00163_A213Contagem_UsuarioContadorCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            n213Contagem_UsuarioContadorCod = T00163_n213Contagem_UsuarioContadorCod[0];
            A940Contagem_SistemaCod = T00163_A940Contagem_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
            n940Contagem_SistemaCod = T00163_n940Contagem_SistemaCod[0];
            A939Contagem_ProjetoCod = T00163_A939Contagem_ProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
            n939Contagem_ProjetoCod = T00163_n939Contagem_ProjetoCod[0];
            Z192Contagem_Codigo = A192Contagem_Codigo;
            sMode43 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1643( ) ;
            if ( AnyError == 1 )
            {
               RcdFound43 = 0;
               InitializeNonKey1643( ) ;
            }
            Gx_mode = sMode43;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound43 = 0;
            InitializeNonKey1643( ) ;
            sMode43 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode43;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1643( ) ;
         if ( RcdFound43 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound43 = 0;
         /* Using cursor T001628 */
         pr_default.execute(20, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            while ( (pr_default.getStatus(20) != 101) && ( ( T001628_A192Contagem_Codigo[0] < A192Contagem_Codigo ) ) )
            {
               pr_default.readNext(20);
            }
            if ( (pr_default.getStatus(20) != 101) && ( ( T001628_A192Contagem_Codigo[0] > A192Contagem_Codigo ) ) )
            {
               A192Contagem_Codigo = T001628_A192Contagem_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               RcdFound43 = 1;
            }
         }
         pr_default.close(20);
      }

      protected void move_previous( )
      {
         RcdFound43 = 0;
         /* Using cursor T001629 */
         pr_default.execute(21, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(21) != 101) )
         {
            while ( (pr_default.getStatus(21) != 101) && ( ( T001629_A192Contagem_Codigo[0] > A192Contagem_Codigo ) ) )
            {
               pr_default.readNext(21);
            }
            if ( (pr_default.getStatus(21) != 101) && ( ( T001629_A192Contagem_Codigo[0] < A192Contagem_Codigo ) ) )
            {
               A192Contagem_Codigo = T001629_A192Contagem_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               RcdFound43 = 1;
            }
         }
         pr_default.close(21);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1643( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1643( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound43 == 1 )
            {
               if ( A192Contagem_Codigo != Z192Contagem_Codigo )
               {
                  A192Contagem_Codigo = Z192Contagem_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEM_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagem_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1643( ) ;
                  GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A192Contagem_Codigo != Z192Contagem_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1643( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEM_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagem_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1643( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A192Contagem_Codigo != Z192Contagem_Codigo )
         {
            A192Contagem_Codigo = Z192Contagem_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEM_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagem_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1643( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00162 */
            pr_default.execute(0, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contagem"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1118Contagem_ContratadaCod != T00162_A1118Contagem_ContratadaCod[0] ) || ( Z197Contagem_DataCriacao != T00162_A197Contagem_DataCriacao[0] ) || ( StringUtil.StrCmp(Z195Contagem_Tecnica, T00162_A195Contagem_Tecnica[0]) != 0 ) || ( StringUtil.StrCmp(Z196Contagem_Tipo, T00162_A196Contagem_Tipo[0]) != 0 ) || ( Z943Contagem_PFB != T00162_A943Contagem_PFB[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z944Contagem_PFL != T00162_A944Contagem_PFL[0] ) || ( Z1119Contagem_Divergencia != T00162_A1119Contagem_Divergencia[0] ) || ( StringUtil.StrCmp(Z262Contagem_Status, T00162_A262Contagem_Status[0]) != 0 ) || ( StringUtil.StrCmp(Z945Contagem_Demanda, T00162_A945Contagem_Demanda[0]) != 0 ) || ( StringUtil.StrCmp(Z946Contagem_Link, T00162_A946Contagem_Link[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z947Contagem_Fator != T00162_A947Contagem_Fator[0] ) || ( Z1117Contagem_Deflator != T00162_A1117Contagem_Deflator[0] ) || ( Z948Contagem_Lock != T00162_A948Contagem_Lock[0] ) || ( Z1111Contagem_DataHomologacao != T00162_A1111Contagem_DataHomologacao[0] ) || ( StringUtil.StrCmp(Z1120Contagem_Descricao, T00162_A1120Contagem_Descricao[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1113Contagem_PFBA != T00162_A1113Contagem_PFBA[0] ) || ( Z1114Contagem_PFLA != T00162_A1114Contagem_PFLA[0] ) || ( Z1115Contagem_PFBD != T00162_A1115Contagem_PFBD[0] ) || ( Z1116Contagem_PFLD != T00162_A1116Contagem_PFLD[0] ) || ( StringUtil.StrCmp(Z1810Contagem_Versao, T00162_A1810Contagem_Versao[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1811Contagem_ServicoCod != T00162_A1811Contagem_ServicoCod[0] ) || ( StringUtil.StrCmp(Z1812Contagem_ArquivoImp, T00162_A1812Contagem_ArquivoImp[0]) != 0 ) || ( Z1813Contagem_ReferenciaINM != T00162_A1813Contagem_ReferenciaINM[0] ) || ( Z1814Contagem_AmbienteTecnologico != T00162_A1814Contagem_AmbienteTecnologico[0] ) || ( Z193Contagem_AreaTrabalhoCod != T00162_A193Contagem_AreaTrabalhoCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z213Contagem_UsuarioContadorCod != T00162_A213Contagem_UsuarioContadorCod[0] ) || ( Z940Contagem_SistemaCod != T00162_A940Contagem_SistemaCod[0] ) || ( Z939Contagem_ProjetoCod != T00162_A939Contagem_ProjetoCod[0] ) )
            {
               if ( Z1118Contagem_ContratadaCod != T00162_A1118Contagem_ContratadaCod[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_ContratadaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1118Contagem_ContratadaCod);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1118Contagem_ContratadaCod[0]);
               }
               if ( Z197Contagem_DataCriacao != T00162_A197Contagem_DataCriacao[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_DataCriacao");
                  GXUtil.WriteLogRaw("Old: ",Z197Contagem_DataCriacao);
                  GXUtil.WriteLogRaw("Current: ",T00162_A197Contagem_DataCriacao[0]);
               }
               if ( StringUtil.StrCmp(Z195Contagem_Tecnica, T00162_A195Contagem_Tecnica[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Tecnica");
                  GXUtil.WriteLogRaw("Old: ",Z195Contagem_Tecnica);
                  GXUtil.WriteLogRaw("Current: ",T00162_A195Contagem_Tecnica[0]);
               }
               if ( StringUtil.StrCmp(Z196Contagem_Tipo, T00162_A196Contagem_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z196Contagem_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T00162_A196Contagem_Tipo[0]);
               }
               if ( Z943Contagem_PFB != T00162_A943Contagem_PFB[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_PFB");
                  GXUtil.WriteLogRaw("Old: ",Z943Contagem_PFB);
                  GXUtil.WriteLogRaw("Current: ",T00162_A943Contagem_PFB[0]);
               }
               if ( Z944Contagem_PFL != T00162_A944Contagem_PFL[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_PFL");
                  GXUtil.WriteLogRaw("Old: ",Z944Contagem_PFL);
                  GXUtil.WriteLogRaw("Current: ",T00162_A944Contagem_PFL[0]);
               }
               if ( Z1119Contagem_Divergencia != T00162_A1119Contagem_Divergencia[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Divergencia");
                  GXUtil.WriteLogRaw("Old: ",Z1119Contagem_Divergencia);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1119Contagem_Divergencia[0]);
               }
               if ( StringUtil.StrCmp(Z262Contagem_Status, T00162_A262Contagem_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Status");
                  GXUtil.WriteLogRaw("Old: ",Z262Contagem_Status);
                  GXUtil.WriteLogRaw("Current: ",T00162_A262Contagem_Status[0]);
               }
               if ( StringUtil.StrCmp(Z945Contagem_Demanda, T00162_A945Contagem_Demanda[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Demanda");
                  GXUtil.WriteLogRaw("Old: ",Z945Contagem_Demanda);
                  GXUtil.WriteLogRaw("Current: ",T00162_A945Contagem_Demanda[0]);
               }
               if ( StringUtil.StrCmp(Z946Contagem_Link, T00162_A946Contagem_Link[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Link");
                  GXUtil.WriteLogRaw("Old: ",Z946Contagem_Link);
                  GXUtil.WriteLogRaw("Current: ",T00162_A946Contagem_Link[0]);
               }
               if ( Z947Contagem_Fator != T00162_A947Contagem_Fator[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Fator");
                  GXUtil.WriteLogRaw("Old: ",Z947Contagem_Fator);
                  GXUtil.WriteLogRaw("Current: ",T00162_A947Contagem_Fator[0]);
               }
               if ( Z1117Contagem_Deflator != T00162_A1117Contagem_Deflator[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Deflator");
                  GXUtil.WriteLogRaw("Old: ",Z1117Contagem_Deflator);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1117Contagem_Deflator[0]);
               }
               if ( Z948Contagem_Lock != T00162_A948Contagem_Lock[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Lock");
                  GXUtil.WriteLogRaw("Old: ",Z948Contagem_Lock);
                  GXUtil.WriteLogRaw("Current: ",T00162_A948Contagem_Lock[0]);
               }
               if ( Z1111Contagem_DataHomologacao != T00162_A1111Contagem_DataHomologacao[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_DataHomologacao");
                  GXUtil.WriteLogRaw("Old: ",Z1111Contagem_DataHomologacao);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1111Contagem_DataHomologacao[0]);
               }
               if ( StringUtil.StrCmp(Z1120Contagem_Descricao, T00162_A1120Contagem_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z1120Contagem_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1120Contagem_Descricao[0]);
               }
               if ( Z1113Contagem_PFBA != T00162_A1113Contagem_PFBA[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_PFBA");
                  GXUtil.WriteLogRaw("Old: ",Z1113Contagem_PFBA);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1113Contagem_PFBA[0]);
               }
               if ( Z1114Contagem_PFLA != T00162_A1114Contagem_PFLA[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_PFLA");
                  GXUtil.WriteLogRaw("Old: ",Z1114Contagem_PFLA);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1114Contagem_PFLA[0]);
               }
               if ( Z1115Contagem_PFBD != T00162_A1115Contagem_PFBD[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_PFBD");
                  GXUtil.WriteLogRaw("Old: ",Z1115Contagem_PFBD);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1115Contagem_PFBD[0]);
               }
               if ( Z1116Contagem_PFLD != T00162_A1116Contagem_PFLD[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_PFLD");
                  GXUtil.WriteLogRaw("Old: ",Z1116Contagem_PFLD);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1116Contagem_PFLD[0]);
               }
               if ( StringUtil.StrCmp(Z1810Contagem_Versao, T00162_A1810Contagem_Versao[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_Versao");
                  GXUtil.WriteLogRaw("Old: ",Z1810Contagem_Versao);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1810Contagem_Versao[0]);
               }
               if ( Z1811Contagem_ServicoCod != T00162_A1811Contagem_ServicoCod[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_ServicoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1811Contagem_ServicoCod);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1811Contagem_ServicoCod[0]);
               }
               if ( StringUtil.StrCmp(Z1812Contagem_ArquivoImp, T00162_A1812Contagem_ArquivoImp[0]) != 0 )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_ArquivoImp");
                  GXUtil.WriteLogRaw("Old: ",Z1812Contagem_ArquivoImp);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1812Contagem_ArquivoImp[0]);
               }
               if ( Z1813Contagem_ReferenciaINM != T00162_A1813Contagem_ReferenciaINM[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_ReferenciaINM");
                  GXUtil.WriteLogRaw("Old: ",Z1813Contagem_ReferenciaINM);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1813Contagem_ReferenciaINM[0]);
               }
               if ( Z1814Contagem_AmbienteTecnologico != T00162_A1814Contagem_AmbienteTecnologico[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_AmbienteTecnologico");
                  GXUtil.WriteLogRaw("Old: ",Z1814Contagem_AmbienteTecnologico);
                  GXUtil.WriteLogRaw("Current: ",T00162_A1814Contagem_AmbienteTecnologico[0]);
               }
               if ( Z193Contagem_AreaTrabalhoCod != T00162_A193Contagem_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z193Contagem_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T00162_A193Contagem_AreaTrabalhoCod[0]);
               }
               if ( Z213Contagem_UsuarioContadorCod != T00162_A213Contagem_UsuarioContadorCod[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_UsuarioContadorCod");
                  GXUtil.WriteLogRaw("Old: ",Z213Contagem_UsuarioContadorCod);
                  GXUtil.WriteLogRaw("Current: ",T00162_A213Contagem_UsuarioContadorCod[0]);
               }
               if ( Z940Contagem_SistemaCod != T00162_A940Contagem_SistemaCod[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z940Contagem_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T00162_A940Contagem_SistemaCod[0]);
               }
               if ( Z939Contagem_ProjetoCod != T00162_A939Contagem_ProjetoCod[0] )
               {
                  GXUtil.WriteLog("contagem:[seudo value changed for attri]"+"Contagem_ProjetoCod");
                  GXUtil.WriteLogRaw("Old: ",Z939Contagem_ProjetoCod);
                  GXUtil.WriteLogRaw("Current: ",T00162_A939Contagem_ProjetoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Contagem"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1643( )
      {
         BeforeValidate1643( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1643( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1643( 0) ;
            CheckOptimisticConcurrency1643( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1643( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1643( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001630 */
                     pr_default.execute(22, new Object[] {n1118Contagem_ContratadaCod, A1118Contagem_ContratadaCod, A197Contagem_DataCriacao, n195Contagem_Tecnica, A195Contagem_Tecnica, n196Contagem_Tipo, A196Contagem_Tipo, n199Contagem_Proposito, A199Contagem_Proposito, n200Contagem_Escopo, A200Contagem_Escopo, n201Contagem_Fronteira, A201Contagem_Fronteira, n202Contagem_Observacao, A202Contagem_Observacao, n1059Contagem_Notas, A1059Contagem_Notas, n943Contagem_PFB, A943Contagem_PFB, n944Contagem_PFL, A944Contagem_PFL, n1119Contagem_Divergencia, A1119Contagem_Divergencia, n262Contagem_Status, A262Contagem_Status, n945Contagem_Demanda, A945Contagem_Demanda, n946Contagem_Link, A946Contagem_Link, n947Contagem_Fator, A947Contagem_Fator, n1117Contagem_Deflator, A1117Contagem_Deflator, A948Contagem_Lock, n1111Contagem_DataHomologacao, A1111Contagem_DataHomologacao, n1112Contagem_Consideracoes, A1112Contagem_Consideracoes, n1120Contagem_Descricao, A1120Contagem_Descricao, n1113Contagem_PFBA, A1113Contagem_PFBA, n1114Contagem_PFLA, A1114Contagem_PFLA, n1115Contagem_PFBD, A1115Contagem_PFBD, n1116Contagem_PFLD, A1116Contagem_PFLD, n1809Contagem_Aplicabilidade, A1809Contagem_Aplicabilidade, n1810Contagem_Versao, A1810Contagem_Versao, n1811Contagem_ServicoCod, A1811Contagem_ServicoCod, n1812Contagem_ArquivoImp, A1812Contagem_ArquivoImp, n1813Contagem_ReferenciaINM, A1813Contagem_ReferenciaINM, n1814Contagem_AmbienteTecnologico, A1814Contagem_AmbienteTecnologico, A193Contagem_AreaTrabalhoCod, n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod, n940Contagem_SistemaCod, A940Contagem_SistemaCod, n939Contagem_ProjetoCod, A939Contagem_ProjetoCod});
                     A192Contagem_Codigo = T001630_A192Contagem_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
                     pr_default.close(22);
                     dsDefault.SmartCacheProvider.SetUpdated("Contagem") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption160( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1643( ) ;
            }
            EndLevel1643( ) ;
         }
         CloseExtendedTableCursors1643( ) ;
      }

      protected void Update1643( )
      {
         BeforeValidate1643( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1643( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1643( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1643( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1643( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001631 */
                     pr_default.execute(23, new Object[] {n1118Contagem_ContratadaCod, A1118Contagem_ContratadaCod, A197Contagem_DataCriacao, n195Contagem_Tecnica, A195Contagem_Tecnica, n196Contagem_Tipo, A196Contagem_Tipo, n199Contagem_Proposito, A199Contagem_Proposito, n200Contagem_Escopo, A200Contagem_Escopo, n201Contagem_Fronteira, A201Contagem_Fronteira, n202Contagem_Observacao, A202Contagem_Observacao, n1059Contagem_Notas, A1059Contagem_Notas, n943Contagem_PFB, A943Contagem_PFB, n944Contagem_PFL, A944Contagem_PFL, n1119Contagem_Divergencia, A1119Contagem_Divergencia, n262Contagem_Status, A262Contagem_Status, n945Contagem_Demanda, A945Contagem_Demanda, n946Contagem_Link, A946Contagem_Link, n947Contagem_Fator, A947Contagem_Fator, n1117Contagem_Deflator, A1117Contagem_Deflator, A948Contagem_Lock, n1111Contagem_DataHomologacao, A1111Contagem_DataHomologacao, n1112Contagem_Consideracoes, A1112Contagem_Consideracoes, n1120Contagem_Descricao, A1120Contagem_Descricao, n1113Contagem_PFBA, A1113Contagem_PFBA, n1114Contagem_PFLA, A1114Contagem_PFLA, n1115Contagem_PFBD, A1115Contagem_PFBD, n1116Contagem_PFLD, A1116Contagem_PFLD, n1809Contagem_Aplicabilidade, A1809Contagem_Aplicabilidade, n1810Contagem_Versao, A1810Contagem_Versao, n1811Contagem_ServicoCod, A1811Contagem_ServicoCod, n1812Contagem_ArquivoImp, A1812Contagem_ArquivoImp, n1813Contagem_ReferenciaINM, A1813Contagem_ReferenciaINM, n1814Contagem_AmbienteTecnologico, A1814Contagem_AmbienteTecnologico, A193Contagem_AreaTrabalhoCod, n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod, n940Contagem_SistemaCod, A940Contagem_SistemaCod, n939Contagem_ProjetoCod, A939Contagem_ProjetoCod, A192Contagem_Codigo});
                     pr_default.close(23);
                     dsDefault.SmartCacheProvider.SetUpdated("Contagem") ;
                     if ( (pr_default.getStatus(23) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Contagem"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1643( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1643( ) ;
         }
         CloseExtendedTableCursors1643( ) ;
      }

      protected void DeferredUpdate1643( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1643( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1643( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1643( ) ;
            AfterConfirm1643( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1643( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001632 */
                  pr_default.execute(24, new Object[] {A192Contagem_Codigo});
                  pr_default.close(24);
                  dsDefault.SmartCacheProvider.SetUpdated("Contagem") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode43 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1643( ) ;
         Gx_mode = sMode43;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1643( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001634 */
            pr_default.execute(25, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               A264Contagem_QtdItens = T001634_A264Contagem_QtdItens[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
               n264Contagem_QtdItens = T001634_n264Contagem_QtdItens[0];
            }
            else
            {
               A264Contagem_QtdItens = 0;
               n264Contagem_QtdItens = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
            }
            pr_default.close(25);
            /* Using cursor T001636 */
            pr_default.execute(26, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               A265Contagem_QtdItensAprovados = T001636_A265Contagem_QtdItensAprovados[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
               n265Contagem_QtdItensAprovados = T001636_n265Contagem_QtdItensAprovados[0];
            }
            else
            {
               A265Contagem_QtdItensAprovados = 0;
               n265Contagem_QtdItensAprovados = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
            }
            pr_default.close(26);
            /* Using cursor T001637 */
            pr_default.execute(27, new Object[] {A193Contagem_AreaTrabalhoCod});
            A194Contagem_AreaTrabalhoDes = T001637_A194Contagem_AreaTrabalhoDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
            n194Contagem_AreaTrabalhoDes = T001637_n194Contagem_AreaTrabalhoDes[0];
            pr_default.close(27);
            /* Using cursor T001638 */
            pr_default.execute(28, new Object[] {n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod});
            A214Contagem_UsuarioContadorPessoaCod = T001638_A214Contagem_UsuarioContadorPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
            n214Contagem_UsuarioContadorPessoaCod = T001638_n214Contagem_UsuarioContadorPessoaCod[0];
            pr_default.close(28);
            /* Using cursor T001639 */
            pr_default.execute(29, new Object[] {n214Contagem_UsuarioContadorPessoaCod, A214Contagem_UsuarioContadorPessoaCod});
            A215Contagem_UsuarioContadorPessoaNom = T001639_A215Contagem_UsuarioContadorPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
            n215Contagem_UsuarioContadorPessoaNom = T001639_n215Contagem_UsuarioContadorPessoaNom[0];
            pr_default.close(29);
            /* Using cursor T001640 */
            pr_default.execute(30, new Object[] {n940Contagem_SistemaCod, A940Contagem_SistemaCod});
            A941Contagem_SistemaSigla = T001640_A941Contagem_SistemaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
            n941Contagem_SistemaSigla = T001640_n941Contagem_SistemaSigla[0];
            A949Contagem_SistemaCoord = T001640_A949Contagem_SistemaCoord[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
            n949Contagem_SistemaCoord = T001640_n949Contagem_SistemaCoord[0];
            pr_default.close(30);
            /* Using cursor T001641 */
            pr_default.execute(31, new Object[] {n939Contagem_ProjetoCod, A939Contagem_ProjetoCod});
            A942Contagem_ProjetoSigla = T001641_A942Contagem_ProjetoSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
            n942Contagem_ProjetoSigla = T001641_n942Contagem_ProjetoSigla[0];
            pr_default.close(31);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001642 */
            pr_default.execute(32, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Historico"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor T001643 */
            pr_default.execute(33, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Baseline"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
            /* Using cursor T001644 */
            pr_default.execute(34, new Object[] {A192Contagem_Codigo});
            if ( (pr_default.getStatus(34) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(34);
         }
      }

      protected void EndLevel1643( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1643( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(27);
            pr_default.close(28);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(29);
            pr_default.close(25);
            pr_default.close(26);
            context.CommitDataStores( "Contagem");
            if ( AnyError == 0 )
            {
               ConfirmValues160( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(27);
            pr_default.close(28);
            pr_default.close(30);
            pr_default.close(31);
            pr_default.close(29);
            pr_default.close(25);
            pr_default.close(26);
            context.RollbackDataStores( "Contagem");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1643( )
      {
         /* Scan By routine */
         /* Using cursor T001645 */
         pr_default.execute(35);
         RcdFound43 = 0;
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound43 = 1;
            A192Contagem_Codigo = T001645_A192Contagem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1643( )
      {
         /* Scan next routine */
         pr_default.readNext(35);
         RcdFound43 = 0;
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound43 = 1;
            A192Contagem_Codigo = T001645_A192Contagem_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1643( )
      {
         pr_default.close(35);
      }

      protected void AfterConfirm1643( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1643( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1643( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1643( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1643( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1643( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1643( )
      {
         edtContagem_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Codigo_Enabled), 5, 0)));
         edtContagem_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_AreaTrabalhoCod_Enabled), 5, 0)));
         edtContagem_ContratadaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_ContratadaCod_Enabled), 5, 0)));
         edtContagem_DataCriacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_DataCriacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_DataCriacao_Enabled), 5, 0)));
         edtContagem_AreaTrabalhoDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AreaTrabalhoDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_AreaTrabalhoDes_Enabled), 5, 0)));
         cmbContagem_Tecnica.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tecnica_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagem_Tecnica.Enabled), 5, 0)));
         cmbContagem_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagem_Tipo.Enabled), 5, 0)));
         edtContagem_Notas_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Notas_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Notas_Enabled), 5, 0)));
         edtContagem_PFB_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFB_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_PFB_Enabled), 5, 0)));
         edtContagem_PFL_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFL_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_PFL_Enabled), 5, 0)));
         edtContagem_Divergencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Divergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Divergencia_Enabled), 5, 0)));
         edtContagem_UsuarioContadorCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorCod_Enabled), 5, 0)));
         edtContagem_UsuarioContadorPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorPessoaCod_Enabled), 5, 0)));
         edtContagem_UsuarioContadorPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_UsuarioContadorPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorPessoaNom_Enabled), 5, 0)));
         cmbContagem_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagem_Status.Enabled), 5, 0)));
         edtContagem_Demanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Demanda_Enabled), 5, 0)));
         edtContagem_Link_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Link_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Link_Enabled), 5, 0)));
         edtContagem_Fator_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Fator_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Fator_Enabled), 5, 0)));
         edtContagem_Deflator_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Deflator_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Deflator_Enabled), 5, 0)));
         edtContagem_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_SistemaCod_Enabled), 5, 0)));
         edtContagem_SistemaSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_SistemaSigla_Enabled), 5, 0)));
         edtContagem_SistemaCoord_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_SistemaCoord_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_SistemaCoord_Enabled), 5, 0)));
         dynContagem_ProjetoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagem_ProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagem_ProjetoCod.Enabled), 5, 0)));
         edtContagem_ProjetoSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_ProjetoSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_ProjetoSigla_Enabled), 5, 0)));
         edtContagem_QtdItens_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_QtdItens_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_QtdItens_Enabled), 5, 0)));
         edtContagem_QtdItensAprovados_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_QtdItensAprovados_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_QtdItensAprovados_Enabled), 5, 0)));
         cmbContagem_Lock.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagem_Lock_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagem_Lock.Enabled), 5, 0)));
         edtContagem_DataHomologacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_DataHomologacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_DataHomologacao_Enabled), 5, 0)));
         edtContagem_Consideracoes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Consideracoes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Consideracoes_Enabled), 5, 0)));
         edtContagem_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Descricao_Enabled), 5, 0)));
         edtContagem_PFBA_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFBA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_PFBA_Enabled), 5, 0)));
         edtContagem_PFLA_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFLA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_PFLA_Enabled), 5, 0)));
         edtContagem_PFBD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFBD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_PFBD_Enabled), 5, 0)));
         edtContagem_PFLD_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_PFLD_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_PFLD_Enabled), 5, 0)));
         edtContagem_Aplicabilidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Aplicabilidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Aplicabilidade_Enabled), 5, 0)));
         edtContagem_Versao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_Versao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_Versao_Enabled), 5, 0)));
         edtContagem_ServicoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_ServicoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_ServicoCod_Enabled), 5, 0)));
         edtContagem_ArquivoImp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_ArquivoImp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_ArquivoImp_Enabled), 5, 0)));
         edtContagem_ReferenciaINM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_ReferenciaINM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_ReferenciaINM_Enabled), 5, 0)));
         edtContagem_AmbienteTecnologico_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagem_AmbienteTecnologico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_AmbienteTecnologico_Enabled), 5, 0)));
         Contagem_proposito_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_proposito_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_proposito_Enabled));
         Contagem_escopo_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_escopo_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_escopo_Enabled));
         Contagem_fronteira_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_fronteira_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_fronteira_Enabled));
         Contagem_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagem_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Contagem_observacao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues160( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117191575");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV20Contagem_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z192Contagem_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z192Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1118Contagem_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z197Contagem_DataCriacao", context.localUtil.DToC( Z197Contagem_DataCriacao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z195Contagem_Tecnica", StringUtil.RTrim( Z195Contagem_Tecnica));
         GxWebStd.gx_hidden_field( context, "Z196Contagem_Tipo", StringUtil.RTrim( Z196Contagem_Tipo));
         GxWebStd.gx_hidden_field( context, "Z943Contagem_PFB", StringUtil.LTrim( StringUtil.NToC( Z943Contagem_PFB, 13, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z944Contagem_PFL", StringUtil.LTrim( StringUtil.NToC( Z944Contagem_PFL, 13, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.NToC( Z1119Contagem_Divergencia, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z262Contagem_Status", StringUtil.RTrim( Z262Contagem_Status));
         GxWebStd.gx_hidden_field( context, "Z945Contagem_Demanda", Z945Contagem_Demanda);
         GxWebStd.gx_hidden_field( context, "Z946Contagem_Link", Z946Contagem_Link);
         GxWebStd.gx_hidden_field( context, "Z947Contagem_Fator", StringUtil.LTrim( StringUtil.NToC( Z947Contagem_Fator, 4, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1117Contagem_Deflator", StringUtil.LTrim( StringUtil.NToC( Z1117Contagem_Deflator, 6, 2, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z948Contagem_Lock", Z948Contagem_Lock);
         GxWebStd.gx_hidden_field( context, "Z1111Contagem_DataHomologacao", context.localUtil.TToC( Z1111Contagem_DataHomologacao, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1120Contagem_Descricao", Z1120Contagem_Descricao);
         GxWebStd.gx_hidden_field( context, "Z1113Contagem_PFBA", StringUtil.LTrim( StringUtil.NToC( Z1113Contagem_PFBA, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1114Contagem_PFLA", StringUtil.LTrim( StringUtil.NToC( Z1114Contagem_PFLA, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1115Contagem_PFBD", StringUtil.LTrim( StringUtil.NToC( Z1115Contagem_PFBD, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1116Contagem_PFLD", StringUtil.LTrim( StringUtil.NToC( Z1116Contagem_PFLD, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1810Contagem_Versao", Z1810Contagem_Versao);
         GxWebStd.gx_hidden_field( context, "Z1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1811Contagem_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1812Contagem_ArquivoImp", Z1812Contagem_ArquivoImp);
         GxWebStd.gx_hidden_field( context, "Z1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1813Contagem_ReferenciaINM), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1814Contagem_AmbienteTecnologico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z193Contagem_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z213Contagem_UsuarioContadorCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z940Contagem_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z939Contagem_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A940Contagem_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A939Contagem_ProjetoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Contagem_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contagem_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEM_USUARIOCONTADORCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Insert_Contagem_UsuarioContadorCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEM_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Insert_Contagem_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEM_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19Insert_Contagem_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PROPOSITO", A199Contagem_Proposito);
         GxWebStd.gx_hidden_field( context, "CONTAGEM_ESCOPO", A200Contagem_Escopo);
         GxWebStd.gx_hidden_field( context, "CONTAGEM_FRONTEIRA", A201Contagem_Fronteira);
         GxWebStd.gx_hidden_field( context, "CONTAGEM_OBSERVACAO", A202Contagem_Observacao);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV22Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV20Contagem_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_PROPOSITO_Enabled", StringUtil.BoolToStr( Contagem_proposito_Enabled));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_ESCOPO_Enabled", StringUtil.BoolToStr( Contagem_escopo_Enabled));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_FRONTEIRA_Enabled", StringUtil.BoolToStr( Contagem_fronteira_Enabled));
         GxWebStd.gx_hidden_field( context, "CONTAGEM_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagem_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Contagem";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagem:[SendSecurityCheck value for]"+"Contagem_Codigo:"+context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contagem:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagem.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV20Contagem_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Contagem" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem " ;
      }

      protected void InitializeNonKey1643( )
      {
         A193Contagem_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
         A940Contagem_SistemaCod = 0;
         n940Contagem_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A940Contagem_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A940Contagem_SistemaCod), 6, 0)));
         n940Contagem_SistemaCod = ((0==A940Contagem_SistemaCod) ? true : false);
         A939Contagem_ProjetoCod = 0;
         n939Contagem_ProjetoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A939Contagem_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A939Contagem_ProjetoCod), 6, 0)));
         n939Contagem_ProjetoCod = ((0==A939Contagem_ProjetoCod) ? true : false);
         A1118Contagem_ContratadaCod = 0;
         n1118Contagem_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1118Contagem_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1118Contagem_ContratadaCod), 6, 0)));
         n1118Contagem_ContratadaCod = ((0==A1118Contagem_ContratadaCod) ? true : false);
         A194Contagem_AreaTrabalhoDes = "";
         n194Contagem_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
         A195Contagem_Tecnica = "";
         n195Contagem_Tecnica = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
         n195Contagem_Tecnica = (String.IsNullOrEmpty(StringUtil.RTrim( A195Contagem_Tecnica)) ? true : false);
         A196Contagem_Tipo = "";
         n196Contagem_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A196Contagem_Tipo", A196Contagem_Tipo);
         n196Contagem_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A196Contagem_Tipo)) ? true : false);
         A199Contagem_Proposito = "";
         n199Contagem_Proposito = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A199Contagem_Proposito", A199Contagem_Proposito);
         A200Contagem_Escopo = "";
         n200Contagem_Escopo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A200Contagem_Escopo", A200Contagem_Escopo);
         A201Contagem_Fronteira = "";
         n201Contagem_Fronteira = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A201Contagem_Fronteira", A201Contagem_Fronteira);
         A202Contagem_Observacao = "";
         n202Contagem_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A202Contagem_Observacao", A202Contagem_Observacao);
         A1059Contagem_Notas = "";
         n1059Contagem_Notas = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1059Contagem_Notas", A1059Contagem_Notas);
         n1059Contagem_Notas = (String.IsNullOrEmpty(StringUtil.RTrim( A1059Contagem_Notas)) ? true : false);
         A943Contagem_PFB = 0;
         n943Contagem_PFB = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A943Contagem_PFB", StringUtil.LTrim( StringUtil.Str( A943Contagem_PFB, 13, 5)));
         n943Contagem_PFB = ((Convert.ToDecimal(0)==A943Contagem_PFB) ? true : false);
         A944Contagem_PFL = 0;
         n944Contagem_PFL = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A944Contagem_PFL", StringUtil.LTrim( StringUtil.Str( A944Contagem_PFL, 13, 5)));
         n944Contagem_PFL = ((Convert.ToDecimal(0)==A944Contagem_PFL) ? true : false);
         A1119Contagem_Divergencia = 0;
         n1119Contagem_Divergencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1119Contagem_Divergencia", StringUtil.LTrim( StringUtil.Str( A1119Contagem_Divergencia, 6, 2)));
         n1119Contagem_Divergencia = ((Convert.ToDecimal(0)==A1119Contagem_Divergencia) ? true : false);
         A214Contagem_UsuarioContadorPessoaCod = 0;
         n214Contagem_UsuarioContadorPessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
         A215Contagem_UsuarioContadorPessoaNom = "";
         n215Contagem_UsuarioContadorPessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
         A262Contagem_Status = "";
         n262Contagem_Status = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A262Contagem_Status", A262Contagem_Status);
         n262Contagem_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A262Contagem_Status)) ? true : false);
         A945Contagem_Demanda = "";
         n945Contagem_Demanda = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A945Contagem_Demanda", A945Contagem_Demanda);
         n945Contagem_Demanda = (String.IsNullOrEmpty(StringUtil.RTrim( A945Contagem_Demanda)) ? true : false);
         A946Contagem_Link = "";
         n946Contagem_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A946Contagem_Link", A946Contagem_Link);
         n946Contagem_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A946Contagem_Link)) ? true : false);
         A947Contagem_Fator = 0;
         n947Contagem_Fator = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A947Contagem_Fator", StringUtil.LTrim( StringUtil.Str( A947Contagem_Fator, 4, 2)));
         n947Contagem_Fator = ((Convert.ToDecimal(0)==A947Contagem_Fator) ? true : false);
         A1117Contagem_Deflator = 0;
         n1117Contagem_Deflator = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1117Contagem_Deflator", StringUtil.LTrim( StringUtil.Str( A1117Contagem_Deflator, 6, 2)));
         n1117Contagem_Deflator = ((Convert.ToDecimal(0)==A1117Contagem_Deflator) ? true : false);
         A941Contagem_SistemaSigla = "";
         n941Contagem_SistemaSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A941Contagem_SistemaSigla", A941Contagem_SistemaSigla);
         A949Contagem_SistemaCoord = "";
         n949Contagem_SistemaCoord = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A949Contagem_SistemaCoord", A949Contagem_SistemaCoord);
         A942Contagem_ProjetoSigla = "";
         n942Contagem_ProjetoSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A942Contagem_ProjetoSigla", A942Contagem_ProjetoSigla);
         A264Contagem_QtdItens = 0;
         n264Contagem_QtdItens = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A264Contagem_QtdItens", StringUtil.LTrim( StringUtil.Str( (decimal)(A264Contagem_QtdItens), 3, 0)));
         A265Contagem_QtdItensAprovados = 0;
         n265Contagem_QtdItensAprovados = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A265Contagem_QtdItensAprovados", StringUtil.LTrim( StringUtil.Str( (decimal)(A265Contagem_QtdItensAprovados), 3, 0)));
         A948Contagem_Lock = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A948Contagem_Lock", A948Contagem_Lock);
         A1111Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
         n1111Contagem_DataHomologacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1111Contagem_DataHomologacao", context.localUtil.TToC( A1111Contagem_DataHomologacao, 8, 5, 0, 3, "/", ":", " "));
         n1111Contagem_DataHomologacao = ((DateTime.MinValue==A1111Contagem_DataHomologacao) ? true : false);
         A1112Contagem_Consideracoes = "";
         n1112Contagem_Consideracoes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1112Contagem_Consideracoes", A1112Contagem_Consideracoes);
         n1112Contagem_Consideracoes = (String.IsNullOrEmpty(StringUtil.RTrim( A1112Contagem_Consideracoes)) ? true : false);
         A1120Contagem_Descricao = "";
         n1120Contagem_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1120Contagem_Descricao", A1120Contagem_Descricao);
         n1120Contagem_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1120Contagem_Descricao)) ? true : false);
         A1113Contagem_PFBA = 0;
         n1113Contagem_PFBA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1113Contagem_PFBA", StringUtil.LTrim( StringUtil.Str( A1113Contagem_PFBA, 14, 5)));
         n1113Contagem_PFBA = ((Convert.ToDecimal(0)==A1113Contagem_PFBA) ? true : false);
         A1114Contagem_PFLA = 0;
         n1114Contagem_PFLA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1114Contagem_PFLA", StringUtil.LTrim( StringUtil.Str( A1114Contagem_PFLA, 14, 5)));
         n1114Contagem_PFLA = ((Convert.ToDecimal(0)==A1114Contagem_PFLA) ? true : false);
         A1115Contagem_PFBD = 0;
         n1115Contagem_PFBD = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1115Contagem_PFBD", StringUtil.LTrim( StringUtil.Str( A1115Contagem_PFBD, 14, 5)));
         n1115Contagem_PFBD = ((Convert.ToDecimal(0)==A1115Contagem_PFBD) ? true : false);
         A1116Contagem_PFLD = 0;
         n1116Contagem_PFLD = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1116Contagem_PFLD", StringUtil.LTrim( StringUtil.Str( A1116Contagem_PFLD, 14, 5)));
         n1116Contagem_PFLD = ((Convert.ToDecimal(0)==A1116Contagem_PFLD) ? true : false);
         A1809Contagem_Aplicabilidade = "";
         n1809Contagem_Aplicabilidade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1809Contagem_Aplicabilidade", A1809Contagem_Aplicabilidade);
         n1809Contagem_Aplicabilidade = (String.IsNullOrEmpty(StringUtil.RTrim( A1809Contagem_Aplicabilidade)) ? true : false);
         A1810Contagem_Versao = "";
         n1810Contagem_Versao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1810Contagem_Versao", A1810Contagem_Versao);
         n1810Contagem_Versao = (String.IsNullOrEmpty(StringUtil.RTrim( A1810Contagem_Versao)) ? true : false);
         A1811Contagem_ServicoCod = 0;
         n1811Contagem_ServicoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1811Contagem_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1811Contagem_ServicoCod), 6, 0)));
         n1811Contagem_ServicoCod = ((0==A1811Contagem_ServicoCod) ? true : false);
         A1812Contagem_ArquivoImp = "";
         n1812Contagem_ArquivoImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1812Contagem_ArquivoImp", A1812Contagem_ArquivoImp);
         n1812Contagem_ArquivoImp = (String.IsNullOrEmpty(StringUtil.RTrim( A1812Contagem_ArquivoImp)) ? true : false);
         A1813Contagem_ReferenciaINM = 0;
         n1813Contagem_ReferenciaINM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1813Contagem_ReferenciaINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A1813Contagem_ReferenciaINM), 6, 0)));
         n1813Contagem_ReferenciaINM = ((0==A1813Contagem_ReferenciaINM) ? true : false);
         A1814Contagem_AmbienteTecnologico = 0;
         n1814Contagem_AmbienteTecnologico = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1814Contagem_AmbienteTecnologico", StringUtil.LTrim( StringUtil.Str( (decimal)(A1814Contagem_AmbienteTecnologico), 6, 0)));
         n1814Contagem_AmbienteTecnologico = ((0==A1814Contagem_AmbienteTecnologico) ? true : false);
         A213Contagem_UsuarioContadorCod = AV8WWPContext.gxTpr_Userid;
         n213Contagem_UsuarioContadorCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
         A197Contagem_DataCriacao = DateTimeUtil.ServerDate( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
         Z1118Contagem_ContratadaCod = 0;
         Z197Contagem_DataCriacao = DateTime.MinValue;
         Z195Contagem_Tecnica = "";
         Z196Contagem_Tipo = "";
         Z943Contagem_PFB = 0;
         Z944Contagem_PFL = 0;
         Z1119Contagem_Divergencia = 0;
         Z262Contagem_Status = "";
         Z945Contagem_Demanda = "";
         Z946Contagem_Link = "";
         Z947Contagem_Fator = 0;
         Z1117Contagem_Deflator = 0;
         Z948Contagem_Lock = false;
         Z1111Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
         Z1120Contagem_Descricao = "";
         Z1113Contagem_PFBA = 0;
         Z1114Contagem_PFLA = 0;
         Z1115Contagem_PFBD = 0;
         Z1116Contagem_PFLD = 0;
         Z1810Contagem_Versao = "";
         Z1811Contagem_ServicoCod = 0;
         Z1812Contagem_ArquivoImp = "";
         Z1813Contagem_ReferenciaINM = 0;
         Z1814Contagem_AmbienteTecnologico = 0;
         Z193Contagem_AreaTrabalhoCod = 0;
         Z213Contagem_UsuarioContadorCod = 0;
         Z940Contagem_SistemaCod = 0;
         Z939Contagem_ProjetoCod = 0;
      }

      protected void InitAll1643( )
      {
         A192Contagem_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
         InitializeNonKey1643( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A193Contagem_AreaTrabalhoCod = i193Contagem_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
         A213Contagem_UsuarioContadorCod = i213Contagem_UsuarioContadorCod;
         n213Contagem_UsuarioContadorCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
         A197Contagem_DataCriacao = i197Contagem_DataCriacao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117191694");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagem.js", "?20203117191695");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblContagemtitle_Internalname = "CONTAGEMTITLE";
         lblTextblockcontagem_codigo_Internalname = "TEXTBLOCKCONTAGEM_CODIGO";
         edtContagem_Codigo_Internalname = "CONTAGEM_CODIGO";
         lblTextblockcontagem_areatrabalhocod_Internalname = "TEXTBLOCKCONTAGEM_AREATRABALHOCOD";
         edtContagem_AreaTrabalhoCod_Internalname = "CONTAGEM_AREATRABALHOCOD";
         lblTextblockcontagem_contratadacod_Internalname = "TEXTBLOCKCONTAGEM_CONTRATADACOD";
         edtContagem_ContratadaCod_Internalname = "CONTAGEM_CONTRATADACOD";
         lblTextblockcontagem_datacriacao_Internalname = "TEXTBLOCKCONTAGEM_DATACRIACAO";
         edtContagem_DataCriacao_Internalname = "CONTAGEM_DATACRIACAO";
         lblTextblockcontagem_areatrabalhodes_Internalname = "TEXTBLOCKCONTAGEM_AREATRABALHODES";
         edtContagem_AreaTrabalhoDes_Internalname = "CONTAGEM_AREATRABALHODES";
         lblTextblockcontagem_tecnica_Internalname = "TEXTBLOCKCONTAGEM_TECNICA";
         cmbContagem_Tecnica_Internalname = "CONTAGEM_TECNICA";
         lblTextblockcontagem_tipo_Internalname = "TEXTBLOCKCONTAGEM_TIPO";
         cmbContagem_Tipo_Internalname = "CONTAGEM_TIPO";
         lblTextblockcontagem_proposito_Internalname = "TEXTBLOCKCONTAGEM_PROPOSITO";
         Contagem_proposito_Internalname = "CONTAGEM_PROPOSITO";
         lblTextblockcontagem_escopo_Internalname = "TEXTBLOCKCONTAGEM_ESCOPO";
         Contagem_escopo_Internalname = "CONTAGEM_ESCOPO";
         lblTextblockcontagem_fronteira_Internalname = "TEXTBLOCKCONTAGEM_FRONTEIRA";
         Contagem_fronteira_Internalname = "CONTAGEM_FRONTEIRA";
         lblTextblockcontagem_observacao_Internalname = "TEXTBLOCKCONTAGEM_OBSERVACAO";
         Contagem_observacao_Internalname = "CONTAGEM_OBSERVACAO";
         lblTextblockcontagem_notas_Internalname = "TEXTBLOCKCONTAGEM_NOTAS";
         edtContagem_Notas_Internalname = "CONTAGEM_NOTAS";
         lblTextblockcontagem_pfb_Internalname = "TEXTBLOCKCONTAGEM_PFB";
         edtContagem_PFB_Internalname = "CONTAGEM_PFB";
         lblTextblockcontagem_pfl_Internalname = "TEXTBLOCKCONTAGEM_PFL";
         edtContagem_PFL_Internalname = "CONTAGEM_PFL";
         lblTextblockcontagem_divergencia_Internalname = "TEXTBLOCKCONTAGEM_DIVERGENCIA";
         edtContagem_Divergencia_Internalname = "CONTAGEM_DIVERGENCIA";
         lblTextblockcontagem_usuariocontadorcod_Internalname = "TEXTBLOCKCONTAGEM_USUARIOCONTADORCOD";
         edtContagem_UsuarioContadorCod_Internalname = "CONTAGEM_USUARIOCONTADORCOD";
         lblTextblockcontagem_usuariocontadorpessoacod_Internalname = "TEXTBLOCKCONTAGEM_USUARIOCONTADORPESSOACOD";
         edtContagem_UsuarioContadorPessoaCod_Internalname = "CONTAGEM_USUARIOCONTADORPESSOACOD";
         lblTextblockcontagem_usuariocontadorpessoanom_Internalname = "TEXTBLOCKCONTAGEM_USUARIOCONTADORPESSOANOM";
         edtContagem_UsuarioContadorPessoaNom_Internalname = "CONTAGEM_USUARIOCONTADORPESSOANOM";
         lblTextblockcontagem_status_Internalname = "TEXTBLOCKCONTAGEM_STATUS";
         cmbContagem_Status_Internalname = "CONTAGEM_STATUS";
         lblTextblockcontagem_demanda_Internalname = "TEXTBLOCKCONTAGEM_DEMANDA";
         edtContagem_Demanda_Internalname = "CONTAGEM_DEMANDA";
         lblTextblockcontagem_link_Internalname = "TEXTBLOCKCONTAGEM_LINK";
         edtContagem_Link_Internalname = "CONTAGEM_LINK";
         lblTextblockcontagem_fator_Internalname = "TEXTBLOCKCONTAGEM_FATOR";
         edtContagem_Fator_Internalname = "CONTAGEM_FATOR";
         lblTextblockcontagem_deflator_Internalname = "TEXTBLOCKCONTAGEM_DEFLATOR";
         edtContagem_Deflator_Internalname = "CONTAGEM_DEFLATOR";
         lblTextblockcontagem_sistemacod_Internalname = "TEXTBLOCKCONTAGEM_SISTEMACOD";
         edtContagem_SistemaCod_Internalname = "CONTAGEM_SISTEMACOD";
         lblTextblockcontagem_sistemasigla_Internalname = "TEXTBLOCKCONTAGEM_SISTEMASIGLA";
         edtContagem_SistemaSigla_Internalname = "CONTAGEM_SISTEMASIGLA";
         lblTextblockcontagem_sistemacoord_Internalname = "TEXTBLOCKCONTAGEM_SISTEMACOORD";
         edtContagem_SistemaCoord_Internalname = "CONTAGEM_SISTEMACOORD";
         lblTextblockcontagem_projetocod_Internalname = "TEXTBLOCKCONTAGEM_PROJETOCOD";
         dynContagem_ProjetoCod_Internalname = "CONTAGEM_PROJETOCOD";
         lblTextblockcontagem_projetosigla_Internalname = "TEXTBLOCKCONTAGEM_PROJETOSIGLA";
         edtContagem_ProjetoSigla_Internalname = "CONTAGEM_PROJETOSIGLA";
         lblTextblockcontagem_qtditens_Internalname = "TEXTBLOCKCONTAGEM_QTDITENS";
         edtContagem_QtdItens_Internalname = "CONTAGEM_QTDITENS";
         lblTextblockcontagem_qtditensaprovados_Internalname = "TEXTBLOCKCONTAGEM_QTDITENSAPROVADOS";
         edtContagem_QtdItensAprovados_Internalname = "CONTAGEM_QTDITENSAPROVADOS";
         lblTextblockcontagem_lock_Internalname = "TEXTBLOCKCONTAGEM_LOCK";
         cmbContagem_Lock_Internalname = "CONTAGEM_LOCK";
         lblTextblockcontagem_datahomologacao_Internalname = "TEXTBLOCKCONTAGEM_DATAHOMOLOGACAO";
         edtContagem_DataHomologacao_Internalname = "CONTAGEM_DATAHOMOLOGACAO";
         lblTextblockcontagem_consideracoes_Internalname = "TEXTBLOCKCONTAGEM_CONSIDERACOES";
         edtContagem_Consideracoes_Internalname = "CONTAGEM_CONSIDERACOES";
         lblTextblockcontagem_descricao_Internalname = "TEXTBLOCKCONTAGEM_DESCRICAO";
         edtContagem_Descricao_Internalname = "CONTAGEM_DESCRICAO";
         lblTextblockcontagem_pfba_Internalname = "TEXTBLOCKCONTAGEM_PFBA";
         edtContagem_PFBA_Internalname = "CONTAGEM_PFBA";
         lblTextblockcontagem_pfla_Internalname = "TEXTBLOCKCONTAGEM_PFLA";
         edtContagem_PFLA_Internalname = "CONTAGEM_PFLA";
         lblTextblockcontagem_pfbd_Internalname = "TEXTBLOCKCONTAGEM_PFBD";
         edtContagem_PFBD_Internalname = "CONTAGEM_PFBD";
         lblTextblockcontagem_pfld_Internalname = "TEXTBLOCKCONTAGEM_PFLD";
         edtContagem_PFLD_Internalname = "CONTAGEM_PFLD";
         lblTextblockcontagem_aplicabilidade_Internalname = "TEXTBLOCKCONTAGEM_APLICABILIDADE";
         edtContagem_Aplicabilidade_Internalname = "CONTAGEM_APLICABILIDADE";
         lblTextblockcontagem_versao_Internalname = "TEXTBLOCKCONTAGEM_VERSAO";
         edtContagem_Versao_Internalname = "CONTAGEM_VERSAO";
         lblTextblockcontagem_servicocod_Internalname = "TEXTBLOCKCONTAGEM_SERVICOCOD";
         edtContagem_ServicoCod_Internalname = "CONTAGEM_SERVICOCOD";
         lblTextblockcontagem_arquivoimp_Internalname = "TEXTBLOCKCONTAGEM_ARQUIVOIMP";
         edtContagem_ArquivoImp_Internalname = "CONTAGEM_ARQUIVOIMP";
         lblTextblockcontagem_referenciainm_Internalname = "TEXTBLOCKCONTAGEM_REFERENCIAINM";
         edtContagem_ReferenciaINM_Internalname = "CONTAGEM_REFERENCIAINM";
         lblTextblockcontagem_ambientetecnologico_Internalname = "TEXTBLOCKCONTAGEM_AMBIENTETECNOLOGICO";
         edtContagem_AmbienteTecnologico_Internalname = "CONTAGEM_AMBIENTETECNOLOGICO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_193_Internalname = "PROMPT_193";
         imgprompt_213_Internalname = "PROMPT_213";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem ";
         edtContagem_AmbienteTecnologico_Jsonclick = "";
         edtContagem_AmbienteTecnologico_Enabled = 1;
         edtContagem_ReferenciaINM_Jsonclick = "";
         edtContagem_ReferenciaINM_Enabled = 1;
         edtContagem_ArquivoImp_Enabled = 1;
         edtContagem_ServicoCod_Jsonclick = "";
         edtContagem_ServicoCod_Enabled = 1;
         edtContagem_Versao_Jsonclick = "";
         edtContagem_Versao_Enabled = 1;
         edtContagem_Aplicabilidade_Enabled = 1;
         edtContagem_PFLD_Jsonclick = "";
         edtContagem_PFLD_Enabled = 1;
         edtContagem_PFBD_Jsonclick = "";
         edtContagem_PFBD_Enabled = 1;
         edtContagem_PFLA_Jsonclick = "";
         edtContagem_PFLA_Enabled = 1;
         edtContagem_PFBA_Jsonclick = "";
         edtContagem_PFBA_Enabled = 1;
         edtContagem_Descricao_Enabled = 1;
         edtContagem_Consideracoes_Enabled = 1;
         edtContagem_DataHomologacao_Jsonclick = "";
         edtContagem_DataHomologacao_Enabled = 1;
         cmbContagem_Lock_Jsonclick = "";
         cmbContagem_Lock.Enabled = 1;
         edtContagem_QtdItensAprovados_Jsonclick = "";
         edtContagem_QtdItensAprovados_Enabled = 0;
         edtContagem_QtdItens_Jsonclick = "";
         edtContagem_QtdItens_Enabled = 0;
         edtContagem_ProjetoSigla_Jsonclick = "";
         edtContagem_ProjetoSigla_Enabled = 0;
         dynContagem_ProjetoCod_Jsonclick = "";
         dynContagem_ProjetoCod.Enabled = 1;
         edtContagem_SistemaCoord_Jsonclick = "";
         edtContagem_SistemaCoord_Enabled = 0;
         edtContagem_SistemaSigla_Jsonclick = "";
         edtContagem_SistemaSigla_Enabled = 0;
         edtContagem_SistemaCod_Jsonclick = "";
         edtContagem_SistemaCod_Enabled = 1;
         edtContagem_Deflator_Jsonclick = "";
         edtContagem_Deflator_Enabled = 1;
         edtContagem_Fator_Jsonclick = "";
         edtContagem_Fator_Enabled = 1;
         edtContagem_Link_Enabled = 1;
         edtContagem_Demanda_Jsonclick = "";
         edtContagem_Demanda_Enabled = 1;
         cmbContagem_Status_Jsonclick = "";
         cmbContagem_Status.Enabled = 1;
         edtContagem_UsuarioContadorPessoaNom_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaNom_Enabled = 0;
         edtContagem_UsuarioContadorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaCod_Enabled = 0;
         imgprompt_213_Visible = 1;
         imgprompt_213_Link = "";
         edtContagem_UsuarioContadorCod_Jsonclick = "";
         edtContagem_UsuarioContadorCod_Enabled = 1;
         edtContagem_Divergencia_Jsonclick = "";
         edtContagem_Divergencia_Enabled = 1;
         edtContagem_PFL_Jsonclick = "";
         edtContagem_PFL_Enabled = 1;
         edtContagem_PFB_Jsonclick = "";
         edtContagem_PFB_Enabled = 1;
         edtContagem_Notas_Enabled = 1;
         Contagem_observacao_Enabled = Convert.ToBoolean( 1);
         Contagem_fronteira_Enabled = Convert.ToBoolean( 1);
         Contagem_escopo_Enabled = Convert.ToBoolean( 1);
         Contagem_proposito_Enabled = Convert.ToBoolean( 1);
         cmbContagem_Tipo_Jsonclick = "";
         cmbContagem_Tipo.Enabled = 1;
         cmbContagem_Tecnica_Jsonclick = "";
         cmbContagem_Tecnica.Enabled = 1;
         edtContagem_AreaTrabalhoDes_Jsonclick = "";
         edtContagem_AreaTrabalhoDes_Enabled = 0;
         edtContagem_DataCriacao_Jsonclick = "";
         edtContagem_DataCriacao_Enabled = 1;
         edtContagem_ContratadaCod_Jsonclick = "";
         edtContagem_ContratadaCod_Enabled = 1;
         imgprompt_193_Visible = 1;
         imgprompt_193_Link = "";
         edtContagem_AreaTrabalhoCod_Jsonclick = "";
         edtContagem_AreaTrabalhoCod_Enabled = 1;
         edtContagem_Codigo_Jsonclick = "";
         edtContagem_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTAGEM_PROJETOCOD161( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEM_PROJETOCOD_data161( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEM_PROJETOCOD_html161( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEM_PROJETOCOD_data161( ) ;
         gxdynajaxindex = 1;
         dynContagem_ProjetoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagem_ProjetoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEM_PROJETOCOD_data161( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T001646 */
         pr_default.execute(36);
         while ( (pr_default.getStatus(36) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001646_A648Projeto_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001646_A650Projeto_Sigla[0]));
            pr_default.readNext(36);
         }
         pr_default.close(36);
      }

      public void Valid_Contagem_codigo( int GX_Parm1 ,
                                         short GX_Parm2 ,
                                         short GX_Parm3 )
      {
         A192Contagem_Codigo = GX_Parm1;
         A264Contagem_QtdItens = GX_Parm2;
         n264Contagem_QtdItens = false;
         A265Contagem_QtdItensAprovados = GX_Parm3;
         n265Contagem_QtdItensAprovados = false;
         /* Using cursor T001634 */
         pr_default.execute(25, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(25) != 101) )
         {
            A264Contagem_QtdItens = T001634_A264Contagem_QtdItens[0];
            n264Contagem_QtdItens = T001634_n264Contagem_QtdItens[0];
         }
         else
         {
            A264Contagem_QtdItens = 0;
            n264Contagem_QtdItens = false;
         }
         pr_default.close(25);
         /* Using cursor T001636 */
         pr_default.execute(26, new Object[] {A192Contagem_Codigo});
         if ( (pr_default.getStatus(26) != 101) )
         {
            A265Contagem_QtdItensAprovados = T001636_A265Contagem_QtdItensAprovados[0];
            n265Contagem_QtdItensAprovados = T001636_n265Contagem_QtdItensAprovados[0];
         }
         else
         {
            A265Contagem_QtdItensAprovados = 0;
            n265Contagem_QtdItensAprovados = false;
         }
         pr_default.close(26);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A264Contagem_QtdItens = 0;
            n264Contagem_QtdItens = false;
            A265Contagem_QtdItensAprovados = 0;
            n265Contagem_QtdItensAprovados = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A264Contagem_QtdItens), 3, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A265Contagem_QtdItensAprovados), 3, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagem_areatrabalhocod( int GX_Parm1 ,
                                                  String GX_Parm2 )
      {
         A193Contagem_AreaTrabalhoCod = GX_Parm1;
         A194Contagem_AreaTrabalhoDes = GX_Parm2;
         n194Contagem_AreaTrabalhoDes = false;
         /* Using cursor T001637 */
         pr_default.execute(27, new Object[] {A193Contagem_AreaTrabalhoCod});
         if ( (pr_default.getStatus(27) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem_Area Trabalho'.", "ForeignKeyNotFound", 1, "CONTAGEM_AREATRABALHOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagem_AreaTrabalhoCod_Internalname;
         }
         A194Contagem_AreaTrabalhoDes = T001637_A194Contagem_AreaTrabalhoDes[0];
         n194Contagem_AreaTrabalhoDes = T001637_n194Contagem_AreaTrabalhoDes[0];
         pr_default.close(27);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A194Contagem_AreaTrabalhoDes = "";
            n194Contagem_AreaTrabalhoDes = false;
         }
         isValidOutput.Add(A194Contagem_AreaTrabalhoDes);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagem_usuariocontadorcod( int GX_Parm1 ,
                                                     int GX_Parm2 ,
                                                     String GX_Parm3 )
      {
         A213Contagem_UsuarioContadorCod = GX_Parm1;
         n213Contagem_UsuarioContadorCod = false;
         A214Contagem_UsuarioContadorPessoaCod = GX_Parm2;
         n214Contagem_UsuarioContadorPessoaCod = false;
         A215Contagem_UsuarioContadorPessoaNom = GX_Parm3;
         n215Contagem_UsuarioContadorPessoaNom = false;
         /* Using cursor T001638 */
         pr_default.execute(28, new Object[] {n213Contagem_UsuarioContadorCod, A213Contagem_UsuarioContadorCod});
         if ( (pr_default.getStatus(28) == 101) )
         {
            if ( ! ( (0==A213Contagem_UsuarioContadorCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contador'.", "ForeignKeyNotFound", 1, "CONTAGEM_USUARIOCONTADORCOD");
               AnyError = 1;
               GX_FocusControl = edtContagem_UsuarioContadorCod_Internalname;
            }
         }
         A214Contagem_UsuarioContadorPessoaCod = T001638_A214Contagem_UsuarioContadorPessoaCod[0];
         n214Contagem_UsuarioContadorPessoaCod = T001638_n214Contagem_UsuarioContadorPessoaCod[0];
         pr_default.close(28);
         /* Using cursor T001639 */
         pr_default.execute(29, new Object[] {n214Contagem_UsuarioContadorPessoaCod, A214Contagem_UsuarioContadorPessoaCod});
         if ( (pr_default.getStatus(29) == 101) )
         {
            if ( ! ( (0==A214Contagem_UsuarioContadorPessoaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A215Contagem_UsuarioContadorPessoaNom = T001639_A215Contagem_UsuarioContadorPessoaNom[0];
         n215Contagem_UsuarioContadorPessoaNom = T001639_n215Contagem_UsuarioContadorPessoaNom[0];
         pr_default.close(29);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A214Contagem_UsuarioContadorPessoaCod = 0;
            n214Contagem_UsuarioContadorPessoaCod = false;
            A215Contagem_UsuarioContadorPessoaNom = "";
            n215Contagem_UsuarioContadorPessoaNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagem_demanda( String GX_Parm1 ,
                                          int GX_Parm2 ,
                                          int GX_Parm3 )
      {
         A945Contagem_Demanda = GX_Parm1;
         n945Contagem_Demanda = false;
         A1118Contagem_ContratadaCod = GX_Parm2;
         n1118Contagem_ContratadaCod = false;
         A192Contagem_Codigo = GX_Parm3;
         /* Using cursor T001647 */
         pr_default.execute(37, new Object[] {n945Contagem_Demanda, A945Contagem_Demanda, n1118Contagem_ContratadaCod, A1118Contagem_ContratadaCod, A192Contagem_Codigo});
         if ( (pr_default.getStatus(37) != 101) )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_1004", new   object[]  {"Demanda"+","+"Contratada"}), 1, "CONTAGEM_DEMANDA");
            AnyError = 1;
            GX_FocusControl = edtContagem_Demanda_Internalname;
         }
         pr_default.close(37);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagem_sistemacod( int GX_Parm1 ,
                                             String GX_Parm2 ,
                                             String GX_Parm3 )
      {
         A940Contagem_SistemaCod = GX_Parm1;
         n940Contagem_SistemaCod = false;
         A941Contagem_SistemaSigla = GX_Parm2;
         n941Contagem_SistemaSigla = false;
         A949Contagem_SistemaCoord = GX_Parm3;
         n949Contagem_SistemaCoord = false;
         /* Using cursor T001640 */
         pr_default.execute(30, new Object[] {n940Contagem_SistemaCod, A940Contagem_SistemaCod});
         if ( (pr_default.getStatus(30) == 101) )
         {
            if ( ! ( (0==A940Contagem_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem_Sistema'.", "ForeignKeyNotFound", 1, "CONTAGEM_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtContagem_SistemaCod_Internalname;
            }
         }
         A941Contagem_SistemaSigla = T001640_A941Contagem_SistemaSigla[0];
         n941Contagem_SistemaSigla = T001640_n941Contagem_SistemaSigla[0];
         A949Contagem_SistemaCoord = T001640_A949Contagem_SistemaCoord[0];
         n949Contagem_SistemaCoord = T001640_n949Contagem_SistemaCoord[0];
         pr_default.close(30);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A941Contagem_SistemaSigla = "";
            n941Contagem_SistemaSigla = false;
            A949Contagem_SistemaCoord = "";
            n949Contagem_SistemaCoord = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A941Contagem_SistemaSigla));
         isValidOutput.Add(A949Contagem_SistemaCoord);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagem_projetocod( GXCombobox dynGX_Parm1 ,
                                             String GX_Parm2 )
      {
         dynContagem_ProjetoCod = dynGX_Parm1;
         A939Contagem_ProjetoCod = (int)(NumberUtil.Val( dynContagem_ProjetoCod.CurrentValue, "."));
         n939Contagem_ProjetoCod = false;
         A942Contagem_ProjetoSigla = GX_Parm2;
         n942Contagem_ProjetoSigla = false;
         /* Using cursor T001641 */
         pr_default.execute(31, new Object[] {n939Contagem_ProjetoCod, A939Contagem_ProjetoCod});
         if ( (pr_default.getStatus(31) == 101) )
         {
            if ( ! ( (0==A939Contagem_ProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem_Projeto'.", "ForeignKeyNotFound", 1, "CONTAGEM_PROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynContagem_ProjetoCod_Internalname;
            }
         }
         A942Contagem_ProjetoSigla = T001641_A942Contagem_ProjetoSigla[0];
         n942Contagem_ProjetoSigla = T001641_n942Contagem_ProjetoSigla[0];
         pr_default.close(31);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A942Contagem_ProjetoSigla = "";
            n942Contagem_ProjetoSigla = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A942Contagem_ProjetoSigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV20Contagem_Codigo',fld:'vCONTAGEM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12162',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(27);
         pr_default.close(28);
         pr_default.close(30);
         pr_default.close(31);
         pr_default.close(29);
         pr_default.close(25);
         pr_default.close(26);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z197Contagem_DataCriacao = DateTime.MinValue;
         Z195Contagem_Tecnica = "";
         Z196Contagem_Tipo = "";
         Z262Contagem_Status = "";
         Z945Contagem_Demanda = "";
         Z946Contagem_Link = "";
         Z1111Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
         Z1120Contagem_Descricao = "";
         Z1810Contagem_Versao = "";
         Z1812Contagem_ArquivoImp = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         A262Contagem_Status = "";
         T001613_A648Projeto_Codigo = new int[1] ;
         T001613_A650Projeto_Sigla = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblContagemtitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagem_codigo_Jsonclick = "";
         lblTextblockcontagem_areatrabalhocod_Jsonclick = "";
         lblTextblockcontagem_contratadacod_Jsonclick = "";
         lblTextblockcontagem_datacriacao_Jsonclick = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         lblTextblockcontagem_areatrabalhodes_Jsonclick = "";
         A194Contagem_AreaTrabalhoDes = "";
         lblTextblockcontagem_tecnica_Jsonclick = "";
         lblTextblockcontagem_tipo_Jsonclick = "";
         lblTextblockcontagem_proposito_Jsonclick = "";
         lblTextblockcontagem_escopo_Jsonclick = "";
         lblTextblockcontagem_fronteira_Jsonclick = "";
         lblTextblockcontagem_observacao_Jsonclick = "";
         lblTextblockcontagem_notas_Jsonclick = "";
         A1059Contagem_Notas = "";
         lblTextblockcontagem_pfb_Jsonclick = "";
         lblTextblockcontagem_pfl_Jsonclick = "";
         lblTextblockcontagem_divergencia_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorcod_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorpessoacod_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick = "";
         A215Contagem_UsuarioContadorPessoaNom = "";
         lblTextblockcontagem_status_Jsonclick = "";
         lblTextblockcontagem_demanda_Jsonclick = "";
         A945Contagem_Demanda = "";
         lblTextblockcontagem_link_Jsonclick = "";
         A946Contagem_Link = "";
         lblTextblockcontagem_fator_Jsonclick = "";
         lblTextblockcontagem_deflator_Jsonclick = "";
         lblTextblockcontagem_sistemacod_Jsonclick = "";
         lblTextblockcontagem_sistemasigla_Jsonclick = "";
         A941Contagem_SistemaSigla = "";
         lblTextblockcontagem_sistemacoord_Jsonclick = "";
         A949Contagem_SistemaCoord = "";
         lblTextblockcontagem_projetocod_Jsonclick = "";
         lblTextblockcontagem_projetosigla_Jsonclick = "";
         A942Contagem_ProjetoSigla = "";
         lblTextblockcontagem_qtditens_Jsonclick = "";
         lblTextblockcontagem_qtditensaprovados_Jsonclick = "";
         lblTextblockcontagem_lock_Jsonclick = "";
         lblTextblockcontagem_datahomologacao_Jsonclick = "";
         A1111Contagem_DataHomologacao = (DateTime)(DateTime.MinValue);
         lblTextblockcontagem_consideracoes_Jsonclick = "";
         A1112Contagem_Consideracoes = "";
         lblTextblockcontagem_descricao_Jsonclick = "";
         A1120Contagem_Descricao = "";
         lblTextblockcontagem_pfba_Jsonclick = "";
         lblTextblockcontagem_pfla_Jsonclick = "";
         lblTextblockcontagem_pfbd_Jsonclick = "";
         lblTextblockcontagem_pfld_Jsonclick = "";
         lblTextblockcontagem_aplicabilidade_Jsonclick = "";
         A1809Contagem_Aplicabilidade = "";
         lblTextblockcontagem_versao_Jsonclick = "";
         A1810Contagem_Versao = "";
         lblTextblockcontagem_servicocod_Jsonclick = "";
         lblTextblockcontagem_arquivoimp_Jsonclick = "";
         A1812Contagem_ArquivoImp = "";
         lblTextblockcontagem_referenciainm_Jsonclick = "";
         lblTextblockcontagem_ambientetecnologico_Jsonclick = "";
         A199Contagem_Proposito = "";
         A200Contagem_Escopo = "";
         A201Contagem_Fronteira = "";
         A202Contagem_Observacao = "";
         AV22Pgmname = "";
         Contagem_proposito_Width = "";
         Contagem_proposito_Height = "";
         Contagem_proposito_Skin = "";
         Contagem_proposito_Toolbar = "";
         Contagem_proposito_Class = "";
         Contagem_proposito_Customtoolbar = "";
         Contagem_proposito_Customconfiguration = "";
         Contagem_proposito_Buttonpressedid = "";
         Contagem_proposito_Captionvalue = "";
         Contagem_proposito_Captionclass = "";
         Contagem_proposito_Captionposition = "";
         Contagem_proposito_Coltitle = "";
         Contagem_proposito_Coltitlefont = "";
         Contagem_escopo_Width = "";
         Contagem_escopo_Height = "";
         Contagem_escopo_Skin = "";
         Contagem_escopo_Toolbar = "";
         Contagem_escopo_Class = "";
         Contagem_escopo_Customtoolbar = "";
         Contagem_escopo_Customconfiguration = "";
         Contagem_escopo_Buttonpressedid = "";
         Contagem_escopo_Captionvalue = "";
         Contagem_escopo_Captionclass = "";
         Contagem_escopo_Captionposition = "";
         Contagem_escopo_Coltitle = "";
         Contagem_escopo_Coltitlefont = "";
         Contagem_fronteira_Width = "";
         Contagem_fronteira_Height = "";
         Contagem_fronteira_Skin = "";
         Contagem_fronteira_Toolbar = "";
         Contagem_fronteira_Class = "";
         Contagem_fronteira_Customtoolbar = "";
         Contagem_fronteira_Customconfiguration = "";
         Contagem_fronteira_Buttonpressedid = "";
         Contagem_fronteira_Captionvalue = "";
         Contagem_fronteira_Captionclass = "";
         Contagem_fronteira_Captionposition = "";
         Contagem_fronteira_Coltitle = "";
         Contagem_fronteira_Coltitlefont = "";
         Contagem_observacao_Width = "";
         Contagem_observacao_Height = "";
         Contagem_observacao_Skin = "";
         Contagem_observacao_Toolbar = "";
         Contagem_observacao_Class = "";
         Contagem_observacao_Customtoolbar = "";
         Contagem_observacao_Customconfiguration = "";
         Contagem_observacao_Buttonpressedid = "";
         Contagem_observacao_Captionvalue = "";
         Contagem_observacao_Captionclass = "";
         Contagem_observacao_Captionposition = "";
         Contagem_observacao_Coltitle = "";
         Contagem_observacao_Coltitlefont = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode43 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV17TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z199Contagem_Proposito = "";
         Z200Contagem_Escopo = "";
         Z201Contagem_Fronteira = "";
         Z202Contagem_Observacao = "";
         Z1059Contagem_Notas = "";
         Z1112Contagem_Consideracoes = "";
         Z1809Contagem_Aplicabilidade = "";
         Z194Contagem_AreaTrabalhoDes = "";
         Z215Contagem_UsuarioContadorPessoaNom = "";
         Z941Contagem_SistemaSigla = "";
         Z949Contagem_SistemaCoord = "";
         Z942Contagem_ProjetoSigla = "";
         T001610_A264Contagem_QtdItens = new short[1] ;
         T001610_n264Contagem_QtdItens = new bool[] {false} ;
         T001612_A265Contagem_QtdItensAprovados = new short[1] ;
         T001612_n265Contagem_QtdItensAprovados = new bool[] {false} ;
         T00167_A942Contagem_ProjetoSigla = new String[] {""} ;
         T00167_n942Contagem_ProjetoSigla = new bool[] {false} ;
         T00166_A941Contagem_SistemaSigla = new String[] {""} ;
         T00166_n941Contagem_SistemaSigla = new bool[] {false} ;
         T00166_A949Contagem_SistemaCoord = new String[] {""} ;
         T00166_n949Contagem_SistemaCoord = new bool[] {false} ;
         T00164_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         T00164_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         T00165_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         T00165_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         T00168_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         T00168_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         T001616_A192Contagem_Codigo = new int[1] ;
         T001616_A1118Contagem_ContratadaCod = new int[1] ;
         T001616_n1118Contagem_ContratadaCod = new bool[] {false} ;
         T001616_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         T001616_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         T001616_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         T001616_A195Contagem_Tecnica = new String[] {""} ;
         T001616_n195Contagem_Tecnica = new bool[] {false} ;
         T001616_A196Contagem_Tipo = new String[] {""} ;
         T001616_n196Contagem_Tipo = new bool[] {false} ;
         T001616_A199Contagem_Proposito = new String[] {""} ;
         T001616_n199Contagem_Proposito = new bool[] {false} ;
         T001616_A200Contagem_Escopo = new String[] {""} ;
         T001616_n200Contagem_Escopo = new bool[] {false} ;
         T001616_A201Contagem_Fronteira = new String[] {""} ;
         T001616_n201Contagem_Fronteira = new bool[] {false} ;
         T001616_A202Contagem_Observacao = new String[] {""} ;
         T001616_n202Contagem_Observacao = new bool[] {false} ;
         T001616_A1059Contagem_Notas = new String[] {""} ;
         T001616_n1059Contagem_Notas = new bool[] {false} ;
         T001616_A943Contagem_PFB = new decimal[1] ;
         T001616_n943Contagem_PFB = new bool[] {false} ;
         T001616_A944Contagem_PFL = new decimal[1] ;
         T001616_n944Contagem_PFL = new bool[] {false} ;
         T001616_A1119Contagem_Divergencia = new decimal[1] ;
         T001616_n1119Contagem_Divergencia = new bool[] {false} ;
         T001616_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         T001616_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         T001616_A262Contagem_Status = new String[] {""} ;
         T001616_n262Contagem_Status = new bool[] {false} ;
         T001616_A945Contagem_Demanda = new String[] {""} ;
         T001616_n945Contagem_Demanda = new bool[] {false} ;
         T001616_A946Contagem_Link = new String[] {""} ;
         T001616_n946Contagem_Link = new bool[] {false} ;
         T001616_A947Contagem_Fator = new decimal[1] ;
         T001616_n947Contagem_Fator = new bool[] {false} ;
         T001616_A1117Contagem_Deflator = new decimal[1] ;
         T001616_n1117Contagem_Deflator = new bool[] {false} ;
         T001616_A941Contagem_SistemaSigla = new String[] {""} ;
         T001616_n941Contagem_SistemaSigla = new bool[] {false} ;
         T001616_A949Contagem_SistemaCoord = new String[] {""} ;
         T001616_n949Contagem_SistemaCoord = new bool[] {false} ;
         T001616_A942Contagem_ProjetoSigla = new String[] {""} ;
         T001616_n942Contagem_ProjetoSigla = new bool[] {false} ;
         T001616_A948Contagem_Lock = new bool[] {false} ;
         T001616_A1111Contagem_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T001616_n1111Contagem_DataHomologacao = new bool[] {false} ;
         T001616_A1112Contagem_Consideracoes = new String[] {""} ;
         T001616_n1112Contagem_Consideracoes = new bool[] {false} ;
         T001616_A1120Contagem_Descricao = new String[] {""} ;
         T001616_n1120Contagem_Descricao = new bool[] {false} ;
         T001616_A1113Contagem_PFBA = new decimal[1] ;
         T001616_n1113Contagem_PFBA = new bool[] {false} ;
         T001616_A1114Contagem_PFLA = new decimal[1] ;
         T001616_n1114Contagem_PFLA = new bool[] {false} ;
         T001616_A1115Contagem_PFBD = new decimal[1] ;
         T001616_n1115Contagem_PFBD = new bool[] {false} ;
         T001616_A1116Contagem_PFLD = new decimal[1] ;
         T001616_n1116Contagem_PFLD = new bool[] {false} ;
         T001616_A1809Contagem_Aplicabilidade = new String[] {""} ;
         T001616_n1809Contagem_Aplicabilidade = new bool[] {false} ;
         T001616_A1810Contagem_Versao = new String[] {""} ;
         T001616_n1810Contagem_Versao = new bool[] {false} ;
         T001616_A1811Contagem_ServicoCod = new int[1] ;
         T001616_n1811Contagem_ServicoCod = new bool[] {false} ;
         T001616_A1812Contagem_ArquivoImp = new String[] {""} ;
         T001616_n1812Contagem_ArquivoImp = new bool[] {false} ;
         T001616_A1813Contagem_ReferenciaINM = new int[1] ;
         T001616_n1813Contagem_ReferenciaINM = new bool[] {false} ;
         T001616_A1814Contagem_AmbienteTecnologico = new int[1] ;
         T001616_n1814Contagem_AmbienteTecnologico = new bool[] {false} ;
         T001616_A193Contagem_AreaTrabalhoCod = new int[1] ;
         T001616_A213Contagem_UsuarioContadorCod = new int[1] ;
         T001616_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         T001616_A940Contagem_SistemaCod = new int[1] ;
         T001616_n940Contagem_SistemaCod = new bool[] {false} ;
         T001616_A939Contagem_ProjetoCod = new int[1] ;
         T001616_n939Contagem_ProjetoCod = new bool[] {false} ;
         T001616_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         T001616_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         T001616_A264Contagem_QtdItens = new short[1] ;
         T001616_n264Contagem_QtdItens = new bool[] {false} ;
         T001616_A265Contagem_QtdItensAprovados = new short[1] ;
         T001616_n265Contagem_QtdItensAprovados = new bool[] {false} ;
         T001617_A945Contagem_Demanda = new String[] {""} ;
         T001617_n945Contagem_Demanda = new bool[] {false} ;
         T001619_A264Contagem_QtdItens = new short[1] ;
         T001619_n264Contagem_QtdItens = new bool[] {false} ;
         T001621_A265Contagem_QtdItensAprovados = new short[1] ;
         T001621_n265Contagem_QtdItensAprovados = new bool[] {false} ;
         T001622_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         T001622_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         T001623_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         T001623_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         T001624_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         T001624_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         T001625_A941Contagem_SistemaSigla = new String[] {""} ;
         T001625_n941Contagem_SistemaSigla = new bool[] {false} ;
         T001625_A949Contagem_SistemaCoord = new String[] {""} ;
         T001625_n949Contagem_SistemaCoord = new bool[] {false} ;
         T001626_A942Contagem_ProjetoSigla = new String[] {""} ;
         T001626_n942Contagem_ProjetoSigla = new bool[] {false} ;
         T001627_A192Contagem_Codigo = new int[1] ;
         T00163_A192Contagem_Codigo = new int[1] ;
         T00163_A1118Contagem_ContratadaCod = new int[1] ;
         T00163_n1118Contagem_ContratadaCod = new bool[] {false} ;
         T00163_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         T00163_A195Contagem_Tecnica = new String[] {""} ;
         T00163_n195Contagem_Tecnica = new bool[] {false} ;
         T00163_A196Contagem_Tipo = new String[] {""} ;
         T00163_n196Contagem_Tipo = new bool[] {false} ;
         T00163_A199Contagem_Proposito = new String[] {""} ;
         T00163_n199Contagem_Proposito = new bool[] {false} ;
         T00163_A200Contagem_Escopo = new String[] {""} ;
         T00163_n200Contagem_Escopo = new bool[] {false} ;
         T00163_A201Contagem_Fronteira = new String[] {""} ;
         T00163_n201Contagem_Fronteira = new bool[] {false} ;
         T00163_A202Contagem_Observacao = new String[] {""} ;
         T00163_n202Contagem_Observacao = new bool[] {false} ;
         T00163_A1059Contagem_Notas = new String[] {""} ;
         T00163_n1059Contagem_Notas = new bool[] {false} ;
         T00163_A943Contagem_PFB = new decimal[1] ;
         T00163_n943Contagem_PFB = new bool[] {false} ;
         T00163_A944Contagem_PFL = new decimal[1] ;
         T00163_n944Contagem_PFL = new bool[] {false} ;
         T00163_A1119Contagem_Divergencia = new decimal[1] ;
         T00163_n1119Contagem_Divergencia = new bool[] {false} ;
         T00163_A262Contagem_Status = new String[] {""} ;
         T00163_n262Contagem_Status = new bool[] {false} ;
         T00163_A945Contagem_Demanda = new String[] {""} ;
         T00163_n945Contagem_Demanda = new bool[] {false} ;
         T00163_A946Contagem_Link = new String[] {""} ;
         T00163_n946Contagem_Link = new bool[] {false} ;
         T00163_A947Contagem_Fator = new decimal[1] ;
         T00163_n947Contagem_Fator = new bool[] {false} ;
         T00163_A1117Contagem_Deflator = new decimal[1] ;
         T00163_n1117Contagem_Deflator = new bool[] {false} ;
         T00163_A948Contagem_Lock = new bool[] {false} ;
         T00163_A1111Contagem_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T00163_n1111Contagem_DataHomologacao = new bool[] {false} ;
         T00163_A1112Contagem_Consideracoes = new String[] {""} ;
         T00163_n1112Contagem_Consideracoes = new bool[] {false} ;
         T00163_A1120Contagem_Descricao = new String[] {""} ;
         T00163_n1120Contagem_Descricao = new bool[] {false} ;
         T00163_A1113Contagem_PFBA = new decimal[1] ;
         T00163_n1113Contagem_PFBA = new bool[] {false} ;
         T00163_A1114Contagem_PFLA = new decimal[1] ;
         T00163_n1114Contagem_PFLA = new bool[] {false} ;
         T00163_A1115Contagem_PFBD = new decimal[1] ;
         T00163_n1115Contagem_PFBD = new bool[] {false} ;
         T00163_A1116Contagem_PFLD = new decimal[1] ;
         T00163_n1116Contagem_PFLD = new bool[] {false} ;
         T00163_A1809Contagem_Aplicabilidade = new String[] {""} ;
         T00163_n1809Contagem_Aplicabilidade = new bool[] {false} ;
         T00163_A1810Contagem_Versao = new String[] {""} ;
         T00163_n1810Contagem_Versao = new bool[] {false} ;
         T00163_A1811Contagem_ServicoCod = new int[1] ;
         T00163_n1811Contagem_ServicoCod = new bool[] {false} ;
         T00163_A1812Contagem_ArquivoImp = new String[] {""} ;
         T00163_n1812Contagem_ArquivoImp = new bool[] {false} ;
         T00163_A1813Contagem_ReferenciaINM = new int[1] ;
         T00163_n1813Contagem_ReferenciaINM = new bool[] {false} ;
         T00163_A1814Contagem_AmbienteTecnologico = new int[1] ;
         T00163_n1814Contagem_AmbienteTecnologico = new bool[] {false} ;
         T00163_A193Contagem_AreaTrabalhoCod = new int[1] ;
         T00163_A213Contagem_UsuarioContadorCod = new int[1] ;
         T00163_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         T00163_A940Contagem_SistemaCod = new int[1] ;
         T00163_n940Contagem_SistemaCod = new bool[] {false} ;
         T00163_A939Contagem_ProjetoCod = new int[1] ;
         T00163_n939Contagem_ProjetoCod = new bool[] {false} ;
         T001628_A192Contagem_Codigo = new int[1] ;
         T001629_A192Contagem_Codigo = new int[1] ;
         T00162_A192Contagem_Codigo = new int[1] ;
         T00162_A1118Contagem_ContratadaCod = new int[1] ;
         T00162_n1118Contagem_ContratadaCod = new bool[] {false} ;
         T00162_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         T00162_A195Contagem_Tecnica = new String[] {""} ;
         T00162_n195Contagem_Tecnica = new bool[] {false} ;
         T00162_A196Contagem_Tipo = new String[] {""} ;
         T00162_n196Contagem_Tipo = new bool[] {false} ;
         T00162_A199Contagem_Proposito = new String[] {""} ;
         T00162_n199Contagem_Proposito = new bool[] {false} ;
         T00162_A200Contagem_Escopo = new String[] {""} ;
         T00162_n200Contagem_Escopo = new bool[] {false} ;
         T00162_A201Contagem_Fronteira = new String[] {""} ;
         T00162_n201Contagem_Fronteira = new bool[] {false} ;
         T00162_A202Contagem_Observacao = new String[] {""} ;
         T00162_n202Contagem_Observacao = new bool[] {false} ;
         T00162_A1059Contagem_Notas = new String[] {""} ;
         T00162_n1059Contagem_Notas = new bool[] {false} ;
         T00162_A943Contagem_PFB = new decimal[1] ;
         T00162_n943Contagem_PFB = new bool[] {false} ;
         T00162_A944Contagem_PFL = new decimal[1] ;
         T00162_n944Contagem_PFL = new bool[] {false} ;
         T00162_A1119Contagem_Divergencia = new decimal[1] ;
         T00162_n1119Contagem_Divergencia = new bool[] {false} ;
         T00162_A262Contagem_Status = new String[] {""} ;
         T00162_n262Contagem_Status = new bool[] {false} ;
         T00162_A945Contagem_Demanda = new String[] {""} ;
         T00162_n945Contagem_Demanda = new bool[] {false} ;
         T00162_A946Contagem_Link = new String[] {""} ;
         T00162_n946Contagem_Link = new bool[] {false} ;
         T00162_A947Contagem_Fator = new decimal[1] ;
         T00162_n947Contagem_Fator = new bool[] {false} ;
         T00162_A1117Contagem_Deflator = new decimal[1] ;
         T00162_n1117Contagem_Deflator = new bool[] {false} ;
         T00162_A948Contagem_Lock = new bool[] {false} ;
         T00162_A1111Contagem_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         T00162_n1111Contagem_DataHomologacao = new bool[] {false} ;
         T00162_A1112Contagem_Consideracoes = new String[] {""} ;
         T00162_n1112Contagem_Consideracoes = new bool[] {false} ;
         T00162_A1120Contagem_Descricao = new String[] {""} ;
         T00162_n1120Contagem_Descricao = new bool[] {false} ;
         T00162_A1113Contagem_PFBA = new decimal[1] ;
         T00162_n1113Contagem_PFBA = new bool[] {false} ;
         T00162_A1114Contagem_PFLA = new decimal[1] ;
         T00162_n1114Contagem_PFLA = new bool[] {false} ;
         T00162_A1115Contagem_PFBD = new decimal[1] ;
         T00162_n1115Contagem_PFBD = new bool[] {false} ;
         T00162_A1116Contagem_PFLD = new decimal[1] ;
         T00162_n1116Contagem_PFLD = new bool[] {false} ;
         T00162_A1809Contagem_Aplicabilidade = new String[] {""} ;
         T00162_n1809Contagem_Aplicabilidade = new bool[] {false} ;
         T00162_A1810Contagem_Versao = new String[] {""} ;
         T00162_n1810Contagem_Versao = new bool[] {false} ;
         T00162_A1811Contagem_ServicoCod = new int[1] ;
         T00162_n1811Contagem_ServicoCod = new bool[] {false} ;
         T00162_A1812Contagem_ArquivoImp = new String[] {""} ;
         T00162_n1812Contagem_ArquivoImp = new bool[] {false} ;
         T00162_A1813Contagem_ReferenciaINM = new int[1] ;
         T00162_n1813Contagem_ReferenciaINM = new bool[] {false} ;
         T00162_A1814Contagem_AmbienteTecnologico = new int[1] ;
         T00162_n1814Contagem_AmbienteTecnologico = new bool[] {false} ;
         T00162_A193Contagem_AreaTrabalhoCod = new int[1] ;
         T00162_A213Contagem_UsuarioContadorCod = new int[1] ;
         T00162_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         T00162_A940Contagem_SistemaCod = new int[1] ;
         T00162_n940Contagem_SistemaCod = new bool[] {false} ;
         T00162_A939Contagem_ProjetoCod = new int[1] ;
         T00162_n939Contagem_ProjetoCod = new bool[] {false} ;
         T001630_A192Contagem_Codigo = new int[1] ;
         T001634_A264Contagem_QtdItens = new short[1] ;
         T001634_n264Contagem_QtdItens = new bool[] {false} ;
         T001636_A265Contagem_QtdItensAprovados = new short[1] ;
         T001636_n265Contagem_QtdItensAprovados = new bool[] {false} ;
         T001637_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         T001637_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         T001638_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         T001638_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         T001639_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         T001639_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         T001640_A941Contagem_SistemaSigla = new String[] {""} ;
         T001640_n941Contagem_SistemaSigla = new bool[] {false} ;
         T001640_A949Contagem_SistemaCoord = new String[] {""} ;
         T001640_n949Contagem_SistemaCoord = new bool[] {false} ;
         T001641_A942Contagem_ProjetoSigla = new String[] {""} ;
         T001641_n942Contagem_ProjetoSigla = new bool[] {false} ;
         T001642_A1157ContagemHistorico_Codigo = new int[1] ;
         T001643_A722Baseline_Codigo = new int[1] ;
         T001644_A224ContagemItem_Lancamento = new int[1] ;
         T001645_A192Contagem_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i197Contagem_DataCriacao = DateTime.MinValue;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001646_A648Projeto_Codigo = new int[1] ;
         T001646_A650Projeto_Sigla = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T001647_A945Contagem_Demanda = new String[] {""} ;
         T001647_n945Contagem_Demanda = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagem__default(),
            new Object[][] {
                new Object[] {
               T00162_A192Contagem_Codigo, T00162_A1118Contagem_ContratadaCod, T00162_n1118Contagem_ContratadaCod, T00162_A197Contagem_DataCriacao, T00162_A195Contagem_Tecnica, T00162_n195Contagem_Tecnica, T00162_A196Contagem_Tipo, T00162_n196Contagem_Tipo, T00162_A199Contagem_Proposito, T00162_n199Contagem_Proposito,
               T00162_A200Contagem_Escopo, T00162_n200Contagem_Escopo, T00162_A201Contagem_Fronteira, T00162_n201Contagem_Fronteira, T00162_A202Contagem_Observacao, T00162_n202Contagem_Observacao, T00162_A1059Contagem_Notas, T00162_n1059Contagem_Notas, T00162_A943Contagem_PFB, T00162_n943Contagem_PFB,
               T00162_A944Contagem_PFL, T00162_n944Contagem_PFL, T00162_A1119Contagem_Divergencia, T00162_n1119Contagem_Divergencia, T00162_A262Contagem_Status, T00162_n262Contagem_Status, T00162_A945Contagem_Demanda, T00162_n945Contagem_Demanda, T00162_A946Contagem_Link, T00162_n946Contagem_Link,
               T00162_A947Contagem_Fator, T00162_n947Contagem_Fator, T00162_A1117Contagem_Deflator, T00162_n1117Contagem_Deflator, T00162_A948Contagem_Lock, T00162_A1111Contagem_DataHomologacao, T00162_n1111Contagem_DataHomologacao, T00162_A1112Contagem_Consideracoes, T00162_n1112Contagem_Consideracoes, T00162_A1120Contagem_Descricao,
               T00162_n1120Contagem_Descricao, T00162_A1113Contagem_PFBA, T00162_n1113Contagem_PFBA, T00162_A1114Contagem_PFLA, T00162_n1114Contagem_PFLA, T00162_A1115Contagem_PFBD, T00162_n1115Contagem_PFBD, T00162_A1116Contagem_PFLD, T00162_n1116Contagem_PFLD, T00162_A1809Contagem_Aplicabilidade,
               T00162_n1809Contagem_Aplicabilidade, T00162_A1810Contagem_Versao, T00162_n1810Contagem_Versao, T00162_A1811Contagem_ServicoCod, T00162_n1811Contagem_ServicoCod, T00162_A1812Contagem_ArquivoImp, T00162_n1812Contagem_ArquivoImp, T00162_A1813Contagem_ReferenciaINM, T00162_n1813Contagem_ReferenciaINM, T00162_A1814Contagem_AmbienteTecnologico,
               T00162_n1814Contagem_AmbienteTecnologico, T00162_A193Contagem_AreaTrabalhoCod, T00162_A213Contagem_UsuarioContadorCod, T00162_n213Contagem_UsuarioContadorCod, T00162_A940Contagem_SistemaCod, T00162_n940Contagem_SistemaCod, T00162_A939Contagem_ProjetoCod, T00162_n939Contagem_ProjetoCod
               }
               , new Object[] {
               T00163_A192Contagem_Codigo, T00163_A1118Contagem_ContratadaCod, T00163_n1118Contagem_ContratadaCod, T00163_A197Contagem_DataCriacao, T00163_A195Contagem_Tecnica, T00163_n195Contagem_Tecnica, T00163_A196Contagem_Tipo, T00163_n196Contagem_Tipo, T00163_A199Contagem_Proposito, T00163_n199Contagem_Proposito,
               T00163_A200Contagem_Escopo, T00163_n200Contagem_Escopo, T00163_A201Contagem_Fronteira, T00163_n201Contagem_Fronteira, T00163_A202Contagem_Observacao, T00163_n202Contagem_Observacao, T00163_A1059Contagem_Notas, T00163_n1059Contagem_Notas, T00163_A943Contagem_PFB, T00163_n943Contagem_PFB,
               T00163_A944Contagem_PFL, T00163_n944Contagem_PFL, T00163_A1119Contagem_Divergencia, T00163_n1119Contagem_Divergencia, T00163_A262Contagem_Status, T00163_n262Contagem_Status, T00163_A945Contagem_Demanda, T00163_n945Contagem_Demanda, T00163_A946Contagem_Link, T00163_n946Contagem_Link,
               T00163_A947Contagem_Fator, T00163_n947Contagem_Fator, T00163_A1117Contagem_Deflator, T00163_n1117Contagem_Deflator, T00163_A948Contagem_Lock, T00163_A1111Contagem_DataHomologacao, T00163_n1111Contagem_DataHomologacao, T00163_A1112Contagem_Consideracoes, T00163_n1112Contagem_Consideracoes, T00163_A1120Contagem_Descricao,
               T00163_n1120Contagem_Descricao, T00163_A1113Contagem_PFBA, T00163_n1113Contagem_PFBA, T00163_A1114Contagem_PFLA, T00163_n1114Contagem_PFLA, T00163_A1115Contagem_PFBD, T00163_n1115Contagem_PFBD, T00163_A1116Contagem_PFLD, T00163_n1116Contagem_PFLD, T00163_A1809Contagem_Aplicabilidade,
               T00163_n1809Contagem_Aplicabilidade, T00163_A1810Contagem_Versao, T00163_n1810Contagem_Versao, T00163_A1811Contagem_ServicoCod, T00163_n1811Contagem_ServicoCod, T00163_A1812Contagem_ArquivoImp, T00163_n1812Contagem_ArquivoImp, T00163_A1813Contagem_ReferenciaINM, T00163_n1813Contagem_ReferenciaINM, T00163_A1814Contagem_AmbienteTecnologico,
               T00163_n1814Contagem_AmbienteTecnologico, T00163_A193Contagem_AreaTrabalhoCod, T00163_A213Contagem_UsuarioContadorCod, T00163_n213Contagem_UsuarioContadorCod, T00163_A940Contagem_SistemaCod, T00163_n940Contagem_SistemaCod, T00163_A939Contagem_ProjetoCod, T00163_n939Contagem_ProjetoCod
               }
               , new Object[] {
               T00164_A194Contagem_AreaTrabalhoDes, T00164_n194Contagem_AreaTrabalhoDes
               }
               , new Object[] {
               T00165_A214Contagem_UsuarioContadorPessoaCod, T00165_n214Contagem_UsuarioContadorPessoaCod
               }
               , new Object[] {
               T00166_A941Contagem_SistemaSigla, T00166_n941Contagem_SistemaSigla, T00166_A949Contagem_SistemaCoord, T00166_n949Contagem_SistemaCoord
               }
               , new Object[] {
               T00167_A942Contagem_ProjetoSigla, T00167_n942Contagem_ProjetoSigla
               }
               , new Object[] {
               T00168_A215Contagem_UsuarioContadorPessoaNom, T00168_n215Contagem_UsuarioContadorPessoaNom
               }
               , new Object[] {
               T001610_A264Contagem_QtdItens, T001610_n264Contagem_QtdItens
               }
               , new Object[] {
               T001612_A265Contagem_QtdItensAprovados, T001612_n265Contagem_QtdItensAprovados
               }
               , new Object[] {
               T001613_A648Projeto_Codigo, T001613_A650Projeto_Sigla
               }
               , new Object[] {
               T001616_A192Contagem_Codigo, T001616_A1118Contagem_ContratadaCod, T001616_n1118Contagem_ContratadaCod, T001616_A197Contagem_DataCriacao, T001616_A194Contagem_AreaTrabalhoDes, T001616_n194Contagem_AreaTrabalhoDes, T001616_A195Contagem_Tecnica, T001616_n195Contagem_Tecnica, T001616_A196Contagem_Tipo, T001616_n196Contagem_Tipo,
               T001616_A199Contagem_Proposito, T001616_n199Contagem_Proposito, T001616_A200Contagem_Escopo, T001616_n200Contagem_Escopo, T001616_A201Contagem_Fronteira, T001616_n201Contagem_Fronteira, T001616_A202Contagem_Observacao, T001616_n202Contagem_Observacao, T001616_A1059Contagem_Notas, T001616_n1059Contagem_Notas,
               T001616_A943Contagem_PFB, T001616_n943Contagem_PFB, T001616_A944Contagem_PFL, T001616_n944Contagem_PFL, T001616_A1119Contagem_Divergencia, T001616_n1119Contagem_Divergencia, T001616_A215Contagem_UsuarioContadorPessoaNom, T001616_n215Contagem_UsuarioContadorPessoaNom, T001616_A262Contagem_Status, T001616_n262Contagem_Status,
               T001616_A945Contagem_Demanda, T001616_n945Contagem_Demanda, T001616_A946Contagem_Link, T001616_n946Contagem_Link, T001616_A947Contagem_Fator, T001616_n947Contagem_Fator, T001616_A1117Contagem_Deflator, T001616_n1117Contagem_Deflator, T001616_A941Contagem_SistemaSigla, T001616_n941Contagem_SistemaSigla,
               T001616_A949Contagem_SistemaCoord, T001616_n949Contagem_SistemaCoord, T001616_A942Contagem_ProjetoSigla, T001616_n942Contagem_ProjetoSigla, T001616_A948Contagem_Lock, T001616_A1111Contagem_DataHomologacao, T001616_n1111Contagem_DataHomologacao, T001616_A1112Contagem_Consideracoes, T001616_n1112Contagem_Consideracoes, T001616_A1120Contagem_Descricao,
               T001616_n1120Contagem_Descricao, T001616_A1113Contagem_PFBA, T001616_n1113Contagem_PFBA, T001616_A1114Contagem_PFLA, T001616_n1114Contagem_PFLA, T001616_A1115Contagem_PFBD, T001616_n1115Contagem_PFBD, T001616_A1116Contagem_PFLD, T001616_n1116Contagem_PFLD, T001616_A1809Contagem_Aplicabilidade,
               T001616_n1809Contagem_Aplicabilidade, T001616_A1810Contagem_Versao, T001616_n1810Contagem_Versao, T001616_A1811Contagem_ServicoCod, T001616_n1811Contagem_ServicoCod, T001616_A1812Contagem_ArquivoImp, T001616_n1812Contagem_ArquivoImp, T001616_A1813Contagem_ReferenciaINM, T001616_n1813Contagem_ReferenciaINM, T001616_A1814Contagem_AmbienteTecnologico,
               T001616_n1814Contagem_AmbienteTecnologico, T001616_A193Contagem_AreaTrabalhoCod, T001616_A213Contagem_UsuarioContadorCod, T001616_n213Contagem_UsuarioContadorCod, T001616_A940Contagem_SistemaCod, T001616_n940Contagem_SistemaCod, T001616_A939Contagem_ProjetoCod, T001616_n939Contagem_ProjetoCod, T001616_A214Contagem_UsuarioContadorPessoaCod, T001616_n214Contagem_UsuarioContadorPessoaCod,
               T001616_A264Contagem_QtdItens, T001616_n264Contagem_QtdItens, T001616_A265Contagem_QtdItensAprovados, T001616_n265Contagem_QtdItensAprovados
               }
               , new Object[] {
               T001617_A945Contagem_Demanda, T001617_n945Contagem_Demanda
               }
               , new Object[] {
               T001619_A264Contagem_QtdItens, T001619_n264Contagem_QtdItens
               }
               , new Object[] {
               T001621_A265Contagem_QtdItensAprovados, T001621_n265Contagem_QtdItensAprovados
               }
               , new Object[] {
               T001622_A194Contagem_AreaTrabalhoDes, T001622_n194Contagem_AreaTrabalhoDes
               }
               , new Object[] {
               T001623_A214Contagem_UsuarioContadorPessoaCod, T001623_n214Contagem_UsuarioContadorPessoaCod
               }
               , new Object[] {
               T001624_A215Contagem_UsuarioContadorPessoaNom, T001624_n215Contagem_UsuarioContadorPessoaNom
               }
               , new Object[] {
               T001625_A941Contagem_SistemaSigla, T001625_n941Contagem_SistemaSigla, T001625_A949Contagem_SistemaCoord, T001625_n949Contagem_SistemaCoord
               }
               , new Object[] {
               T001626_A942Contagem_ProjetoSigla, T001626_n942Contagem_ProjetoSigla
               }
               , new Object[] {
               T001627_A192Contagem_Codigo
               }
               , new Object[] {
               T001628_A192Contagem_Codigo
               }
               , new Object[] {
               T001629_A192Contagem_Codigo
               }
               , new Object[] {
               T001630_A192Contagem_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001634_A264Contagem_QtdItens, T001634_n264Contagem_QtdItens
               }
               , new Object[] {
               T001636_A265Contagem_QtdItensAprovados, T001636_n265Contagem_QtdItensAprovados
               }
               , new Object[] {
               T001637_A194Contagem_AreaTrabalhoDes, T001637_n194Contagem_AreaTrabalhoDes
               }
               , new Object[] {
               T001638_A214Contagem_UsuarioContadorPessoaCod, T001638_n214Contagem_UsuarioContadorPessoaCod
               }
               , new Object[] {
               T001639_A215Contagem_UsuarioContadorPessoaNom, T001639_n215Contagem_UsuarioContadorPessoaNom
               }
               , new Object[] {
               T001640_A941Contagem_SistemaSigla, T001640_n941Contagem_SistemaSigla, T001640_A949Contagem_SistemaCoord, T001640_n949Contagem_SistemaCoord
               }
               , new Object[] {
               T001641_A942Contagem_ProjetoSigla, T001641_n942Contagem_ProjetoSigla
               }
               , new Object[] {
               T001642_A1157ContagemHistorico_Codigo
               }
               , new Object[] {
               T001643_A722Baseline_Codigo
               }
               , new Object[] {
               T001644_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T001645_A192Contagem_Codigo
               }
               , new Object[] {
               T001646_A648Projeto_Codigo, T001646_A650Projeto_Sigla
               }
               , new Object[] {
               T001647_A945Contagem_Demanda, T001647_n945Contagem_Demanda
               }
            }
         );
         AV22Pgmname = "Contagem";
         Z213Contagem_UsuarioContadorCod = AV8WWPContext.gxTpr_Userid;
         n213Contagem_UsuarioContadorCod = false;
         N213Contagem_UsuarioContadorCod = AV8WWPContext.gxTpr_Userid;
         n213Contagem_UsuarioContadorCod = false;
         i213Contagem_UsuarioContadorCod = AV8WWPContext.gxTpr_Userid;
         n213Contagem_UsuarioContadorCod = false;
         A213Contagem_UsuarioContadorCod = AV8WWPContext.gxTpr_Userid;
         n213Contagem_UsuarioContadorCod = false;
         Z197Contagem_DataCriacao = DateTimeUtil.ServerDate( context, "DEFAULT");
         A197Contagem_DataCriacao = DateTimeUtil.ServerDate( context, "DEFAULT");
         i197Contagem_DataCriacao = DateTimeUtil.ServerDate( context, "DEFAULT");
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A264Contagem_QtdItens ;
      private short A265Contagem_QtdItensAprovados ;
      private short Gx_BScreen ;
      private short RcdFound43 ;
      private short GX_JID ;
      private short Z264Contagem_QtdItens ;
      private short Z265Contagem_QtdItensAprovados ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV20Contagem_Codigo ;
      private int Z192Contagem_Codigo ;
      private int Z1118Contagem_ContratadaCod ;
      private int Z1811Contagem_ServicoCod ;
      private int Z1813Contagem_ReferenciaINM ;
      private int Z1814Contagem_AmbienteTecnologico ;
      private int Z193Contagem_AreaTrabalhoCod ;
      private int Z213Contagem_UsuarioContadorCod ;
      private int Z940Contagem_SistemaCod ;
      private int Z939Contagem_ProjetoCod ;
      private int N193Contagem_AreaTrabalhoCod ;
      private int N213Contagem_UsuarioContadorCod ;
      private int N940Contagem_SistemaCod ;
      private int N939Contagem_ProjetoCod ;
      private int A192Contagem_Codigo ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A214Contagem_UsuarioContadorPessoaCod ;
      private int A940Contagem_SistemaCod ;
      private int A939Contagem_ProjetoCod ;
      private int AV20Contagem_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContagem_Codigo_Enabled ;
      private int edtContagem_AreaTrabalhoCod_Enabled ;
      private int imgprompt_193_Visible ;
      private int A1118Contagem_ContratadaCod ;
      private int edtContagem_ContratadaCod_Enabled ;
      private int edtContagem_DataCriacao_Enabled ;
      private int edtContagem_AreaTrabalhoDes_Enabled ;
      private int edtContagem_Notas_Enabled ;
      private int edtContagem_PFB_Enabled ;
      private int edtContagem_PFL_Enabled ;
      private int edtContagem_Divergencia_Enabled ;
      private int edtContagem_UsuarioContadorCod_Enabled ;
      private int imgprompt_213_Visible ;
      private int edtContagem_UsuarioContadorPessoaCod_Enabled ;
      private int edtContagem_UsuarioContadorPessoaNom_Enabled ;
      private int edtContagem_Demanda_Enabled ;
      private int edtContagem_Link_Enabled ;
      private int edtContagem_Fator_Enabled ;
      private int edtContagem_Deflator_Enabled ;
      private int edtContagem_SistemaCod_Enabled ;
      private int edtContagem_SistemaSigla_Enabled ;
      private int edtContagem_SistemaCoord_Enabled ;
      private int edtContagem_ProjetoSigla_Enabled ;
      private int edtContagem_QtdItens_Enabled ;
      private int edtContagem_QtdItensAprovados_Enabled ;
      private int edtContagem_DataHomologacao_Enabled ;
      private int edtContagem_Consideracoes_Enabled ;
      private int edtContagem_Descricao_Enabled ;
      private int edtContagem_PFBA_Enabled ;
      private int edtContagem_PFLA_Enabled ;
      private int edtContagem_PFBD_Enabled ;
      private int edtContagem_PFLD_Enabled ;
      private int edtContagem_Aplicabilidade_Enabled ;
      private int edtContagem_Versao_Enabled ;
      private int A1811Contagem_ServicoCod ;
      private int edtContagem_ServicoCod_Enabled ;
      private int edtContagem_ArquivoImp_Enabled ;
      private int A1813Contagem_ReferenciaINM ;
      private int edtContagem_ReferenciaINM_Enabled ;
      private int A1814Contagem_AmbienteTecnologico ;
      private int edtContagem_AmbienteTecnologico_Enabled ;
      private int AV11Insert_Contagem_AreaTrabalhoCod ;
      private int AV15Insert_Contagem_UsuarioContadorCod ;
      private int AV18Insert_Contagem_SistemaCod ;
      private int AV19Insert_Contagem_ProjetoCod ;
      private int Contagem_proposito_Color ;
      private int Contagem_proposito_Coltitlecolor ;
      private int Contagem_escopo_Color ;
      private int Contagem_escopo_Coltitlecolor ;
      private int Contagem_fronteira_Color ;
      private int Contagem_fronteira_Coltitlecolor ;
      private int Contagem_observacao_Color ;
      private int Contagem_observacao_Coltitlecolor ;
      private int AV23GXV1 ;
      private int Z214Contagem_UsuarioContadorPessoaCod ;
      private int i193Contagem_AreaTrabalhoCod ;
      private int i213Contagem_UsuarioContadorCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z943Contagem_PFB ;
      private decimal Z944Contagem_PFL ;
      private decimal Z1119Contagem_Divergencia ;
      private decimal Z947Contagem_Fator ;
      private decimal Z1117Contagem_Deflator ;
      private decimal Z1113Contagem_PFBA ;
      private decimal Z1114Contagem_PFLA ;
      private decimal Z1115Contagem_PFBD ;
      private decimal Z1116Contagem_PFLD ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private decimal A1119Contagem_Divergencia ;
      private decimal A947Contagem_Fator ;
      private decimal A1117Contagem_Deflator ;
      private decimal A1113Contagem_PFBA ;
      private decimal A1114Contagem_PFLA ;
      private decimal A1115Contagem_PFBD ;
      private decimal A1116Contagem_PFLD ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z195Contagem_Tecnica ;
      private String Z196Contagem_Tipo ;
      private String Z262Contagem_Status ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A195Contagem_Tecnica ;
      private String A196Contagem_Tipo ;
      private String A262Contagem_Status ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagem_AreaTrabalhoCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblContagemtitle_Internalname ;
      private String lblContagemtitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagem_codigo_Internalname ;
      private String lblTextblockcontagem_codigo_Jsonclick ;
      private String edtContagem_Codigo_Internalname ;
      private String edtContagem_Codigo_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhocod_Internalname ;
      private String lblTextblockcontagem_areatrabalhocod_Jsonclick ;
      private String edtContagem_AreaTrabalhoCod_Jsonclick ;
      private String imgprompt_193_Internalname ;
      private String imgprompt_193_Link ;
      private String lblTextblockcontagem_contratadacod_Internalname ;
      private String lblTextblockcontagem_contratadacod_Jsonclick ;
      private String edtContagem_ContratadaCod_Internalname ;
      private String edtContagem_ContratadaCod_Jsonclick ;
      private String lblTextblockcontagem_datacriacao_Internalname ;
      private String lblTextblockcontagem_datacriacao_Jsonclick ;
      private String edtContagem_DataCriacao_Internalname ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhodes_Internalname ;
      private String lblTextblockcontagem_areatrabalhodes_Jsonclick ;
      private String edtContagem_AreaTrabalhoDes_Internalname ;
      private String edtContagem_AreaTrabalhoDes_Jsonclick ;
      private String lblTextblockcontagem_tecnica_Internalname ;
      private String lblTextblockcontagem_tecnica_Jsonclick ;
      private String cmbContagem_Tecnica_Internalname ;
      private String cmbContagem_Tecnica_Jsonclick ;
      private String lblTextblockcontagem_tipo_Internalname ;
      private String lblTextblockcontagem_tipo_Jsonclick ;
      private String cmbContagem_Tipo_Internalname ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String lblTextblockcontagem_proposito_Internalname ;
      private String lblTextblockcontagem_proposito_Jsonclick ;
      private String lblTextblockcontagem_escopo_Internalname ;
      private String lblTextblockcontagem_escopo_Jsonclick ;
      private String lblTextblockcontagem_fronteira_Internalname ;
      private String lblTextblockcontagem_fronteira_Jsonclick ;
      private String lblTextblockcontagem_observacao_Internalname ;
      private String lblTextblockcontagem_observacao_Jsonclick ;
      private String lblTextblockcontagem_notas_Internalname ;
      private String lblTextblockcontagem_notas_Jsonclick ;
      private String edtContagem_Notas_Internalname ;
      private String lblTextblockcontagem_pfb_Internalname ;
      private String lblTextblockcontagem_pfb_Jsonclick ;
      private String edtContagem_PFB_Internalname ;
      private String edtContagem_PFB_Jsonclick ;
      private String lblTextblockcontagem_pfl_Internalname ;
      private String lblTextblockcontagem_pfl_Jsonclick ;
      private String edtContagem_PFL_Internalname ;
      private String edtContagem_PFL_Jsonclick ;
      private String lblTextblockcontagem_divergencia_Internalname ;
      private String lblTextblockcontagem_divergencia_Jsonclick ;
      private String edtContagem_Divergencia_Internalname ;
      private String edtContagem_Divergencia_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorcod_Internalname ;
      private String lblTextblockcontagem_usuariocontadorcod_Jsonclick ;
      private String edtContagem_UsuarioContadorCod_Internalname ;
      private String edtContagem_UsuarioContadorCod_Jsonclick ;
      private String imgprompt_213_Internalname ;
      private String imgprompt_213_Link ;
      private String lblTextblockcontagem_usuariocontadorpessoacod_Internalname ;
      private String lblTextblockcontagem_usuariocontadorpessoacod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaCod_Internalname ;
      private String edtContagem_UsuarioContadorPessoaCod_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Internalname ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaNom_Internalname ;
      private String A215Contagem_UsuarioContadorPessoaNom ;
      private String edtContagem_UsuarioContadorPessoaNom_Jsonclick ;
      private String lblTextblockcontagem_status_Internalname ;
      private String lblTextblockcontagem_status_Jsonclick ;
      private String cmbContagem_Status_Internalname ;
      private String cmbContagem_Status_Jsonclick ;
      private String lblTextblockcontagem_demanda_Internalname ;
      private String lblTextblockcontagem_demanda_Jsonclick ;
      private String edtContagem_Demanda_Internalname ;
      private String edtContagem_Demanda_Jsonclick ;
      private String lblTextblockcontagem_link_Internalname ;
      private String lblTextblockcontagem_link_Jsonclick ;
      private String edtContagem_Link_Internalname ;
      private String lblTextblockcontagem_fator_Internalname ;
      private String lblTextblockcontagem_fator_Jsonclick ;
      private String edtContagem_Fator_Internalname ;
      private String edtContagem_Fator_Jsonclick ;
      private String lblTextblockcontagem_deflator_Internalname ;
      private String lblTextblockcontagem_deflator_Jsonclick ;
      private String edtContagem_Deflator_Internalname ;
      private String edtContagem_Deflator_Jsonclick ;
      private String lblTextblockcontagem_sistemacod_Internalname ;
      private String lblTextblockcontagem_sistemacod_Jsonclick ;
      private String edtContagem_SistemaCod_Internalname ;
      private String edtContagem_SistemaCod_Jsonclick ;
      private String lblTextblockcontagem_sistemasigla_Internalname ;
      private String lblTextblockcontagem_sistemasigla_Jsonclick ;
      private String edtContagem_SistemaSigla_Internalname ;
      private String A941Contagem_SistemaSigla ;
      private String edtContagem_SistemaSigla_Jsonclick ;
      private String lblTextblockcontagem_sistemacoord_Internalname ;
      private String lblTextblockcontagem_sistemacoord_Jsonclick ;
      private String edtContagem_SistemaCoord_Internalname ;
      private String edtContagem_SistemaCoord_Jsonclick ;
      private String lblTextblockcontagem_projetocod_Internalname ;
      private String lblTextblockcontagem_projetocod_Jsonclick ;
      private String dynContagem_ProjetoCod_Internalname ;
      private String dynContagem_ProjetoCod_Jsonclick ;
      private String lblTextblockcontagem_projetosigla_Internalname ;
      private String lblTextblockcontagem_projetosigla_Jsonclick ;
      private String edtContagem_ProjetoSigla_Internalname ;
      private String A942Contagem_ProjetoSigla ;
      private String edtContagem_ProjetoSigla_Jsonclick ;
      private String lblTextblockcontagem_qtditens_Internalname ;
      private String lblTextblockcontagem_qtditens_Jsonclick ;
      private String edtContagem_QtdItens_Internalname ;
      private String edtContagem_QtdItens_Jsonclick ;
      private String lblTextblockcontagem_qtditensaprovados_Internalname ;
      private String lblTextblockcontagem_qtditensaprovados_Jsonclick ;
      private String edtContagem_QtdItensAprovados_Internalname ;
      private String edtContagem_QtdItensAprovados_Jsonclick ;
      private String lblTextblockcontagem_lock_Internalname ;
      private String lblTextblockcontagem_lock_Jsonclick ;
      private String cmbContagem_Lock_Internalname ;
      private String cmbContagem_Lock_Jsonclick ;
      private String lblTextblockcontagem_datahomologacao_Internalname ;
      private String lblTextblockcontagem_datahomologacao_Jsonclick ;
      private String edtContagem_DataHomologacao_Internalname ;
      private String edtContagem_DataHomologacao_Jsonclick ;
      private String lblTextblockcontagem_consideracoes_Internalname ;
      private String lblTextblockcontagem_consideracoes_Jsonclick ;
      private String edtContagem_Consideracoes_Internalname ;
      private String lblTextblockcontagem_descricao_Internalname ;
      private String lblTextblockcontagem_descricao_Jsonclick ;
      private String edtContagem_Descricao_Internalname ;
      private String lblTextblockcontagem_pfba_Internalname ;
      private String lblTextblockcontagem_pfba_Jsonclick ;
      private String edtContagem_PFBA_Internalname ;
      private String edtContagem_PFBA_Jsonclick ;
      private String lblTextblockcontagem_pfla_Internalname ;
      private String lblTextblockcontagem_pfla_Jsonclick ;
      private String edtContagem_PFLA_Internalname ;
      private String edtContagem_PFLA_Jsonclick ;
      private String lblTextblockcontagem_pfbd_Internalname ;
      private String lblTextblockcontagem_pfbd_Jsonclick ;
      private String edtContagem_PFBD_Internalname ;
      private String edtContagem_PFBD_Jsonclick ;
      private String lblTextblockcontagem_pfld_Internalname ;
      private String lblTextblockcontagem_pfld_Jsonclick ;
      private String edtContagem_PFLD_Internalname ;
      private String edtContagem_PFLD_Jsonclick ;
      private String lblTextblockcontagem_aplicabilidade_Internalname ;
      private String lblTextblockcontagem_aplicabilidade_Jsonclick ;
      private String edtContagem_Aplicabilidade_Internalname ;
      private String lblTextblockcontagem_versao_Internalname ;
      private String lblTextblockcontagem_versao_Jsonclick ;
      private String edtContagem_Versao_Internalname ;
      private String edtContagem_Versao_Jsonclick ;
      private String lblTextblockcontagem_servicocod_Internalname ;
      private String lblTextblockcontagem_servicocod_Jsonclick ;
      private String edtContagem_ServicoCod_Internalname ;
      private String edtContagem_ServicoCod_Jsonclick ;
      private String lblTextblockcontagem_arquivoimp_Internalname ;
      private String lblTextblockcontagem_arquivoimp_Jsonclick ;
      private String edtContagem_ArquivoImp_Internalname ;
      private String lblTextblockcontagem_referenciainm_Internalname ;
      private String lblTextblockcontagem_referenciainm_Jsonclick ;
      private String edtContagem_ReferenciaINM_Internalname ;
      private String edtContagem_ReferenciaINM_Jsonclick ;
      private String lblTextblockcontagem_ambientetecnologico_Internalname ;
      private String lblTextblockcontagem_ambientetecnologico_Jsonclick ;
      private String edtContagem_AmbienteTecnologico_Internalname ;
      private String edtContagem_AmbienteTecnologico_Jsonclick ;
      private String AV22Pgmname ;
      private String Contagem_proposito_Width ;
      private String Contagem_proposito_Height ;
      private String Contagem_proposito_Skin ;
      private String Contagem_proposito_Toolbar ;
      private String Contagem_proposito_Class ;
      private String Contagem_proposito_Customtoolbar ;
      private String Contagem_proposito_Customconfiguration ;
      private String Contagem_proposito_Buttonpressedid ;
      private String Contagem_proposito_Captionvalue ;
      private String Contagem_proposito_Captionclass ;
      private String Contagem_proposito_Captionposition ;
      private String Contagem_proposito_Coltitle ;
      private String Contagem_proposito_Coltitlefont ;
      private String Contagem_escopo_Width ;
      private String Contagem_escopo_Height ;
      private String Contagem_escopo_Skin ;
      private String Contagem_escopo_Toolbar ;
      private String Contagem_escopo_Class ;
      private String Contagem_escopo_Customtoolbar ;
      private String Contagem_escopo_Customconfiguration ;
      private String Contagem_escopo_Buttonpressedid ;
      private String Contagem_escopo_Captionvalue ;
      private String Contagem_escopo_Captionclass ;
      private String Contagem_escopo_Captionposition ;
      private String Contagem_escopo_Coltitle ;
      private String Contagem_escopo_Coltitlefont ;
      private String Contagem_fronteira_Width ;
      private String Contagem_fronteira_Height ;
      private String Contagem_fronteira_Skin ;
      private String Contagem_fronteira_Toolbar ;
      private String Contagem_fronteira_Class ;
      private String Contagem_fronteira_Customtoolbar ;
      private String Contagem_fronteira_Customconfiguration ;
      private String Contagem_fronteira_Buttonpressedid ;
      private String Contagem_fronteira_Captionvalue ;
      private String Contagem_fronteira_Captionclass ;
      private String Contagem_fronteira_Captionposition ;
      private String Contagem_fronteira_Coltitle ;
      private String Contagem_fronteira_Coltitlefont ;
      private String Contagem_observacao_Width ;
      private String Contagem_observacao_Height ;
      private String Contagem_observacao_Skin ;
      private String Contagem_observacao_Toolbar ;
      private String Contagem_observacao_Class ;
      private String Contagem_observacao_Customtoolbar ;
      private String Contagem_observacao_Customconfiguration ;
      private String Contagem_observacao_Buttonpressedid ;
      private String Contagem_observacao_Captionvalue ;
      private String Contagem_observacao_Captionclass ;
      private String Contagem_observacao_Captionposition ;
      private String Contagem_observacao_Coltitle ;
      private String Contagem_observacao_Coltitlefont ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode43 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z215Contagem_UsuarioContadorPessoaNom ;
      private String Z941Contagem_SistemaSigla ;
      private String Z942Contagem_ProjetoSigla ;
      private String Contagem_proposito_Internalname ;
      private String Contagem_escopo_Internalname ;
      private String Contagem_fronteira_Internalname ;
      private String Contagem_observacao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1111Contagem_DataHomologacao ;
      private DateTime A1111Contagem_DataHomologacao ;
      private DateTime Z197Contagem_DataCriacao ;
      private DateTime A197Contagem_DataCriacao ;
      private DateTime i197Contagem_DataCriacao ;
      private bool Z948Contagem_Lock ;
      private bool entryPointCalled ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n214Contagem_UsuarioContadorPessoaCod ;
      private bool n940Contagem_SistemaCod ;
      private bool n939Contagem_ProjetoCod ;
      private bool toggleJsOutput ;
      private bool n195Contagem_Tecnica ;
      private bool n196Contagem_Tipo ;
      private bool n262Contagem_Status ;
      private bool A948Contagem_Lock ;
      private bool wbErr ;
      private bool n1118Contagem_ContratadaCod ;
      private bool n194Contagem_AreaTrabalhoDes ;
      private bool n1059Contagem_Notas ;
      private bool n943Contagem_PFB ;
      private bool n944Contagem_PFL ;
      private bool n1119Contagem_Divergencia ;
      private bool n215Contagem_UsuarioContadorPessoaNom ;
      private bool n945Contagem_Demanda ;
      private bool n946Contagem_Link ;
      private bool n947Contagem_Fator ;
      private bool n1117Contagem_Deflator ;
      private bool n941Contagem_SistemaSigla ;
      private bool n949Contagem_SistemaCoord ;
      private bool n942Contagem_ProjetoSigla ;
      private bool n264Contagem_QtdItens ;
      private bool n265Contagem_QtdItensAprovados ;
      private bool n1111Contagem_DataHomologacao ;
      private bool n1112Contagem_Consideracoes ;
      private bool n1120Contagem_Descricao ;
      private bool n1113Contagem_PFBA ;
      private bool n1114Contagem_PFLA ;
      private bool n1115Contagem_PFBD ;
      private bool n1116Contagem_PFLD ;
      private bool n1809Contagem_Aplicabilidade ;
      private bool n1810Contagem_Versao ;
      private bool n1811Contagem_ServicoCod ;
      private bool n1812Contagem_ArquivoImp ;
      private bool n1813Contagem_ReferenciaINM ;
      private bool n1814Contagem_AmbienteTecnologico ;
      private bool n199Contagem_Proposito ;
      private bool n200Contagem_Escopo ;
      private bool n201Contagem_Fronteira ;
      private bool n202Contagem_Observacao ;
      private bool Contagem_proposito_Enabled ;
      private bool Contagem_proposito_Toolbarcancollapse ;
      private bool Contagem_proposito_Toolbarexpanded ;
      private bool Contagem_proposito_Usercontroliscolumn ;
      private bool Contagem_proposito_Visible ;
      private bool Contagem_escopo_Enabled ;
      private bool Contagem_escopo_Toolbarcancollapse ;
      private bool Contagem_escopo_Toolbarexpanded ;
      private bool Contagem_escopo_Usercontroliscolumn ;
      private bool Contagem_escopo_Visible ;
      private bool Contagem_fronteira_Enabled ;
      private bool Contagem_fronteira_Toolbarcancollapse ;
      private bool Contagem_fronteira_Toolbarexpanded ;
      private bool Contagem_fronteira_Usercontroliscolumn ;
      private bool Contagem_fronteira_Visible ;
      private bool Contagem_observacao_Enabled ;
      private bool Contagem_observacao_Toolbarcancollapse ;
      private bool Contagem_observacao_Toolbarexpanded ;
      private bool Contagem_observacao_Usercontroliscolumn ;
      private bool Contagem_observacao_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1059Contagem_Notas ;
      private String A1112Contagem_Consideracoes ;
      private String A1809Contagem_Aplicabilidade ;
      private String A199Contagem_Proposito ;
      private String A200Contagem_Escopo ;
      private String A201Contagem_Fronteira ;
      private String A202Contagem_Observacao ;
      private String Z199Contagem_Proposito ;
      private String Z200Contagem_Escopo ;
      private String Z201Contagem_Fronteira ;
      private String Z202Contagem_Observacao ;
      private String Z1059Contagem_Notas ;
      private String Z1112Contagem_Consideracoes ;
      private String Z1809Contagem_Aplicabilidade ;
      private String Z945Contagem_Demanda ;
      private String Z946Contagem_Link ;
      private String Z1120Contagem_Descricao ;
      private String Z1810Contagem_Versao ;
      private String Z1812Contagem_ArquivoImp ;
      private String A194Contagem_AreaTrabalhoDes ;
      private String A945Contagem_Demanda ;
      private String A946Contagem_Link ;
      private String A949Contagem_SistemaCoord ;
      private String A1120Contagem_Descricao ;
      private String A1810Contagem_Versao ;
      private String A1812Contagem_ArquivoImp ;
      private String Z194Contagem_AreaTrabalhoDes ;
      private String Z949Contagem_SistemaCoord ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T001613_A648Projeto_Codigo ;
      private String[] T001613_A650Projeto_Sigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagem_Tecnica ;
      private GXCombobox cmbContagem_Tipo ;
      private GXCombobox cmbContagem_Status ;
      private GXCombobox dynContagem_ProjetoCod ;
      private GXCombobox cmbContagem_Lock ;
      private short[] T001610_A264Contagem_QtdItens ;
      private bool[] T001610_n264Contagem_QtdItens ;
      private short[] T001612_A265Contagem_QtdItensAprovados ;
      private bool[] T001612_n265Contagem_QtdItensAprovados ;
      private String[] T00167_A942Contagem_ProjetoSigla ;
      private bool[] T00167_n942Contagem_ProjetoSigla ;
      private String[] T00166_A941Contagem_SistemaSigla ;
      private bool[] T00166_n941Contagem_SistemaSigla ;
      private String[] T00166_A949Contagem_SistemaCoord ;
      private bool[] T00166_n949Contagem_SistemaCoord ;
      private String[] T00164_A194Contagem_AreaTrabalhoDes ;
      private bool[] T00164_n194Contagem_AreaTrabalhoDes ;
      private int[] T00165_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] T00165_n214Contagem_UsuarioContadorPessoaCod ;
      private String[] T00168_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] T00168_n215Contagem_UsuarioContadorPessoaNom ;
      private int[] T001616_A192Contagem_Codigo ;
      private int[] T001616_A1118Contagem_ContratadaCod ;
      private bool[] T001616_n1118Contagem_ContratadaCod ;
      private DateTime[] T001616_A197Contagem_DataCriacao ;
      private String[] T001616_A194Contagem_AreaTrabalhoDes ;
      private bool[] T001616_n194Contagem_AreaTrabalhoDes ;
      private String[] T001616_A195Contagem_Tecnica ;
      private bool[] T001616_n195Contagem_Tecnica ;
      private String[] T001616_A196Contagem_Tipo ;
      private bool[] T001616_n196Contagem_Tipo ;
      private String[] T001616_A199Contagem_Proposito ;
      private bool[] T001616_n199Contagem_Proposito ;
      private String[] T001616_A200Contagem_Escopo ;
      private bool[] T001616_n200Contagem_Escopo ;
      private String[] T001616_A201Contagem_Fronteira ;
      private bool[] T001616_n201Contagem_Fronteira ;
      private String[] T001616_A202Contagem_Observacao ;
      private bool[] T001616_n202Contagem_Observacao ;
      private String[] T001616_A1059Contagem_Notas ;
      private bool[] T001616_n1059Contagem_Notas ;
      private decimal[] T001616_A943Contagem_PFB ;
      private bool[] T001616_n943Contagem_PFB ;
      private decimal[] T001616_A944Contagem_PFL ;
      private bool[] T001616_n944Contagem_PFL ;
      private decimal[] T001616_A1119Contagem_Divergencia ;
      private bool[] T001616_n1119Contagem_Divergencia ;
      private String[] T001616_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] T001616_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] T001616_A262Contagem_Status ;
      private bool[] T001616_n262Contagem_Status ;
      private String[] T001616_A945Contagem_Demanda ;
      private bool[] T001616_n945Contagem_Demanda ;
      private String[] T001616_A946Contagem_Link ;
      private bool[] T001616_n946Contagem_Link ;
      private decimal[] T001616_A947Contagem_Fator ;
      private bool[] T001616_n947Contagem_Fator ;
      private decimal[] T001616_A1117Contagem_Deflator ;
      private bool[] T001616_n1117Contagem_Deflator ;
      private String[] T001616_A941Contagem_SistemaSigla ;
      private bool[] T001616_n941Contagem_SistemaSigla ;
      private String[] T001616_A949Contagem_SistemaCoord ;
      private bool[] T001616_n949Contagem_SistemaCoord ;
      private String[] T001616_A942Contagem_ProjetoSigla ;
      private bool[] T001616_n942Contagem_ProjetoSigla ;
      private bool[] T001616_A948Contagem_Lock ;
      private DateTime[] T001616_A1111Contagem_DataHomologacao ;
      private bool[] T001616_n1111Contagem_DataHomologacao ;
      private String[] T001616_A1112Contagem_Consideracoes ;
      private bool[] T001616_n1112Contagem_Consideracoes ;
      private String[] T001616_A1120Contagem_Descricao ;
      private bool[] T001616_n1120Contagem_Descricao ;
      private decimal[] T001616_A1113Contagem_PFBA ;
      private bool[] T001616_n1113Contagem_PFBA ;
      private decimal[] T001616_A1114Contagem_PFLA ;
      private bool[] T001616_n1114Contagem_PFLA ;
      private decimal[] T001616_A1115Contagem_PFBD ;
      private bool[] T001616_n1115Contagem_PFBD ;
      private decimal[] T001616_A1116Contagem_PFLD ;
      private bool[] T001616_n1116Contagem_PFLD ;
      private String[] T001616_A1809Contagem_Aplicabilidade ;
      private bool[] T001616_n1809Contagem_Aplicabilidade ;
      private String[] T001616_A1810Contagem_Versao ;
      private bool[] T001616_n1810Contagem_Versao ;
      private int[] T001616_A1811Contagem_ServicoCod ;
      private bool[] T001616_n1811Contagem_ServicoCod ;
      private String[] T001616_A1812Contagem_ArquivoImp ;
      private bool[] T001616_n1812Contagem_ArquivoImp ;
      private int[] T001616_A1813Contagem_ReferenciaINM ;
      private bool[] T001616_n1813Contagem_ReferenciaINM ;
      private int[] T001616_A1814Contagem_AmbienteTecnologico ;
      private bool[] T001616_n1814Contagem_AmbienteTecnologico ;
      private int[] T001616_A193Contagem_AreaTrabalhoCod ;
      private int[] T001616_A213Contagem_UsuarioContadorCod ;
      private bool[] T001616_n213Contagem_UsuarioContadorCod ;
      private int[] T001616_A940Contagem_SistemaCod ;
      private bool[] T001616_n940Contagem_SistemaCod ;
      private int[] T001616_A939Contagem_ProjetoCod ;
      private bool[] T001616_n939Contagem_ProjetoCod ;
      private int[] T001616_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] T001616_n214Contagem_UsuarioContadorPessoaCod ;
      private short[] T001616_A264Contagem_QtdItens ;
      private bool[] T001616_n264Contagem_QtdItens ;
      private short[] T001616_A265Contagem_QtdItensAprovados ;
      private bool[] T001616_n265Contagem_QtdItensAprovados ;
      private String[] T001617_A945Contagem_Demanda ;
      private bool[] T001617_n945Contagem_Demanda ;
      private short[] T001619_A264Contagem_QtdItens ;
      private bool[] T001619_n264Contagem_QtdItens ;
      private short[] T001621_A265Contagem_QtdItensAprovados ;
      private bool[] T001621_n265Contagem_QtdItensAprovados ;
      private String[] T001622_A194Contagem_AreaTrabalhoDes ;
      private bool[] T001622_n194Contagem_AreaTrabalhoDes ;
      private int[] T001623_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] T001623_n214Contagem_UsuarioContadorPessoaCod ;
      private String[] T001624_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] T001624_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] T001625_A941Contagem_SistemaSigla ;
      private bool[] T001625_n941Contagem_SistemaSigla ;
      private String[] T001625_A949Contagem_SistemaCoord ;
      private bool[] T001625_n949Contagem_SistemaCoord ;
      private String[] T001626_A942Contagem_ProjetoSigla ;
      private bool[] T001626_n942Contagem_ProjetoSigla ;
      private int[] T001627_A192Contagem_Codigo ;
      private int[] T00163_A192Contagem_Codigo ;
      private int[] T00163_A1118Contagem_ContratadaCod ;
      private bool[] T00163_n1118Contagem_ContratadaCod ;
      private DateTime[] T00163_A197Contagem_DataCriacao ;
      private String[] T00163_A195Contagem_Tecnica ;
      private bool[] T00163_n195Contagem_Tecnica ;
      private String[] T00163_A196Contagem_Tipo ;
      private bool[] T00163_n196Contagem_Tipo ;
      private String[] T00163_A199Contagem_Proposito ;
      private bool[] T00163_n199Contagem_Proposito ;
      private String[] T00163_A200Contagem_Escopo ;
      private bool[] T00163_n200Contagem_Escopo ;
      private String[] T00163_A201Contagem_Fronteira ;
      private bool[] T00163_n201Contagem_Fronteira ;
      private String[] T00163_A202Contagem_Observacao ;
      private bool[] T00163_n202Contagem_Observacao ;
      private String[] T00163_A1059Contagem_Notas ;
      private bool[] T00163_n1059Contagem_Notas ;
      private decimal[] T00163_A943Contagem_PFB ;
      private bool[] T00163_n943Contagem_PFB ;
      private decimal[] T00163_A944Contagem_PFL ;
      private bool[] T00163_n944Contagem_PFL ;
      private decimal[] T00163_A1119Contagem_Divergencia ;
      private bool[] T00163_n1119Contagem_Divergencia ;
      private String[] T00163_A262Contagem_Status ;
      private bool[] T00163_n262Contagem_Status ;
      private String[] T00163_A945Contagem_Demanda ;
      private bool[] T00163_n945Contagem_Demanda ;
      private String[] T00163_A946Contagem_Link ;
      private bool[] T00163_n946Contagem_Link ;
      private decimal[] T00163_A947Contagem_Fator ;
      private bool[] T00163_n947Contagem_Fator ;
      private decimal[] T00163_A1117Contagem_Deflator ;
      private bool[] T00163_n1117Contagem_Deflator ;
      private bool[] T00163_A948Contagem_Lock ;
      private DateTime[] T00163_A1111Contagem_DataHomologacao ;
      private bool[] T00163_n1111Contagem_DataHomologacao ;
      private String[] T00163_A1112Contagem_Consideracoes ;
      private bool[] T00163_n1112Contagem_Consideracoes ;
      private String[] T00163_A1120Contagem_Descricao ;
      private bool[] T00163_n1120Contagem_Descricao ;
      private decimal[] T00163_A1113Contagem_PFBA ;
      private bool[] T00163_n1113Contagem_PFBA ;
      private decimal[] T00163_A1114Contagem_PFLA ;
      private bool[] T00163_n1114Contagem_PFLA ;
      private decimal[] T00163_A1115Contagem_PFBD ;
      private bool[] T00163_n1115Contagem_PFBD ;
      private decimal[] T00163_A1116Contagem_PFLD ;
      private bool[] T00163_n1116Contagem_PFLD ;
      private String[] T00163_A1809Contagem_Aplicabilidade ;
      private bool[] T00163_n1809Contagem_Aplicabilidade ;
      private String[] T00163_A1810Contagem_Versao ;
      private bool[] T00163_n1810Contagem_Versao ;
      private int[] T00163_A1811Contagem_ServicoCod ;
      private bool[] T00163_n1811Contagem_ServicoCod ;
      private String[] T00163_A1812Contagem_ArquivoImp ;
      private bool[] T00163_n1812Contagem_ArquivoImp ;
      private int[] T00163_A1813Contagem_ReferenciaINM ;
      private bool[] T00163_n1813Contagem_ReferenciaINM ;
      private int[] T00163_A1814Contagem_AmbienteTecnologico ;
      private bool[] T00163_n1814Contagem_AmbienteTecnologico ;
      private int[] T00163_A193Contagem_AreaTrabalhoCod ;
      private int[] T00163_A213Contagem_UsuarioContadorCod ;
      private bool[] T00163_n213Contagem_UsuarioContadorCod ;
      private int[] T00163_A940Contagem_SistemaCod ;
      private bool[] T00163_n940Contagem_SistemaCod ;
      private int[] T00163_A939Contagem_ProjetoCod ;
      private bool[] T00163_n939Contagem_ProjetoCod ;
      private int[] T001628_A192Contagem_Codigo ;
      private int[] T001629_A192Contagem_Codigo ;
      private int[] T00162_A192Contagem_Codigo ;
      private int[] T00162_A1118Contagem_ContratadaCod ;
      private bool[] T00162_n1118Contagem_ContratadaCod ;
      private DateTime[] T00162_A197Contagem_DataCriacao ;
      private String[] T00162_A195Contagem_Tecnica ;
      private bool[] T00162_n195Contagem_Tecnica ;
      private String[] T00162_A196Contagem_Tipo ;
      private bool[] T00162_n196Contagem_Tipo ;
      private String[] T00162_A199Contagem_Proposito ;
      private bool[] T00162_n199Contagem_Proposito ;
      private String[] T00162_A200Contagem_Escopo ;
      private bool[] T00162_n200Contagem_Escopo ;
      private String[] T00162_A201Contagem_Fronteira ;
      private bool[] T00162_n201Contagem_Fronteira ;
      private String[] T00162_A202Contagem_Observacao ;
      private bool[] T00162_n202Contagem_Observacao ;
      private String[] T00162_A1059Contagem_Notas ;
      private bool[] T00162_n1059Contagem_Notas ;
      private decimal[] T00162_A943Contagem_PFB ;
      private bool[] T00162_n943Contagem_PFB ;
      private decimal[] T00162_A944Contagem_PFL ;
      private bool[] T00162_n944Contagem_PFL ;
      private decimal[] T00162_A1119Contagem_Divergencia ;
      private bool[] T00162_n1119Contagem_Divergencia ;
      private String[] T00162_A262Contagem_Status ;
      private bool[] T00162_n262Contagem_Status ;
      private String[] T00162_A945Contagem_Demanda ;
      private bool[] T00162_n945Contagem_Demanda ;
      private String[] T00162_A946Contagem_Link ;
      private bool[] T00162_n946Contagem_Link ;
      private decimal[] T00162_A947Contagem_Fator ;
      private bool[] T00162_n947Contagem_Fator ;
      private decimal[] T00162_A1117Contagem_Deflator ;
      private bool[] T00162_n1117Contagem_Deflator ;
      private bool[] T00162_A948Contagem_Lock ;
      private DateTime[] T00162_A1111Contagem_DataHomologacao ;
      private bool[] T00162_n1111Contagem_DataHomologacao ;
      private String[] T00162_A1112Contagem_Consideracoes ;
      private bool[] T00162_n1112Contagem_Consideracoes ;
      private String[] T00162_A1120Contagem_Descricao ;
      private bool[] T00162_n1120Contagem_Descricao ;
      private decimal[] T00162_A1113Contagem_PFBA ;
      private bool[] T00162_n1113Contagem_PFBA ;
      private decimal[] T00162_A1114Contagem_PFLA ;
      private bool[] T00162_n1114Contagem_PFLA ;
      private decimal[] T00162_A1115Contagem_PFBD ;
      private bool[] T00162_n1115Contagem_PFBD ;
      private decimal[] T00162_A1116Contagem_PFLD ;
      private bool[] T00162_n1116Contagem_PFLD ;
      private String[] T00162_A1809Contagem_Aplicabilidade ;
      private bool[] T00162_n1809Contagem_Aplicabilidade ;
      private String[] T00162_A1810Contagem_Versao ;
      private bool[] T00162_n1810Contagem_Versao ;
      private int[] T00162_A1811Contagem_ServicoCod ;
      private bool[] T00162_n1811Contagem_ServicoCod ;
      private String[] T00162_A1812Contagem_ArquivoImp ;
      private bool[] T00162_n1812Contagem_ArquivoImp ;
      private int[] T00162_A1813Contagem_ReferenciaINM ;
      private bool[] T00162_n1813Contagem_ReferenciaINM ;
      private int[] T00162_A1814Contagem_AmbienteTecnologico ;
      private bool[] T00162_n1814Contagem_AmbienteTecnologico ;
      private int[] T00162_A193Contagem_AreaTrabalhoCod ;
      private int[] T00162_A213Contagem_UsuarioContadorCod ;
      private bool[] T00162_n213Contagem_UsuarioContadorCod ;
      private int[] T00162_A940Contagem_SistemaCod ;
      private bool[] T00162_n940Contagem_SistemaCod ;
      private int[] T00162_A939Contagem_ProjetoCod ;
      private bool[] T00162_n939Contagem_ProjetoCod ;
      private int[] T001630_A192Contagem_Codigo ;
      private short[] T001634_A264Contagem_QtdItens ;
      private bool[] T001634_n264Contagem_QtdItens ;
      private short[] T001636_A265Contagem_QtdItensAprovados ;
      private bool[] T001636_n265Contagem_QtdItensAprovados ;
      private String[] T001637_A194Contagem_AreaTrabalhoDes ;
      private bool[] T001637_n194Contagem_AreaTrabalhoDes ;
      private int[] T001638_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] T001638_n214Contagem_UsuarioContadorPessoaCod ;
      private String[] T001639_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] T001639_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] T001640_A941Contagem_SistemaSigla ;
      private bool[] T001640_n941Contagem_SistemaSigla ;
      private String[] T001640_A949Contagem_SistemaCoord ;
      private bool[] T001640_n949Contagem_SistemaCoord ;
      private String[] T001641_A942Contagem_ProjetoSigla ;
      private bool[] T001641_n942Contagem_ProjetoSigla ;
      private int[] T001642_A1157ContagemHistorico_Codigo ;
      private int[] T001643_A722Baseline_Codigo ;
      private int[] T001644_A224ContagemItem_Lancamento ;
      private int[] T001645_A192Contagem_Codigo ;
      private int[] T001646_A648Projeto_Codigo ;
      private String[] T001646_A650Projeto_Sigla ;
      private String[] T001647_A945Contagem_Demanda ;
      private bool[] T001647_n945Contagem_Demanda ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV17TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001613 ;
          prmT001613 = new Object[] {
          } ;
          Object[] prmT001616 ;
          prmT001616 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT001616 ;
          cmdBufferT001616=" SELECT TM1.[Contagem_Codigo], TM1.[Contagem_ContratadaCod], TM1.[Contagem_DataCriacao], T4.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, TM1.[Contagem_Tecnica], TM1.[Contagem_Tipo], TM1.[Contagem_Proposito], TM1.[Contagem_Escopo], TM1.[Contagem_Fronteira], TM1.[Contagem_Observacao], TM1.[Contagem_Notas], TM1.[Contagem_PFB], TM1.[Contagem_PFL], TM1.[Contagem_Divergencia], T6.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, TM1.[Contagem_Status], TM1.[Contagem_Demanda], TM1.[Contagem_Link], TM1.[Contagem_Fator], TM1.[Contagem_Deflator], T7.[Sistema_Sigla] AS Contagem_SistemaSigla, T7.[Sistema_Coordenacao] AS Contagem_SistemaCoord, T8.[Projeto_Sigla] AS Contagem_ProjetoSigla, TM1.[Contagem_Lock], TM1.[Contagem_DataHomologacao], TM1.[Contagem_Consideracoes], TM1.[Contagem_Descricao], TM1.[Contagem_PFBA], TM1.[Contagem_PFLA], TM1.[Contagem_PFBD], TM1.[Contagem_PFLD], TM1.[Contagem_Aplicabilidade], TM1.[Contagem_Versao], TM1.[Contagem_ServicoCod], TM1.[Contagem_ArquivoImp], TM1.[Contagem_ReferenciaINM], TM1.[Contagem_AmbienteTecnologico], TM1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, TM1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, TM1.[Contagem_SistemaCod] AS Contagem_SistemaCod, TM1.[Contagem_ProjetoCod] AS Contagem_ProjetoCod, T5.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, COALESCE( T2.[Contagem_QtdItens], 0) AS Contagem_QtdItens, COALESCE( T3.[Contagem_QtdItens], 0) AS Contagem_QtdItensAprovados FROM ((((((([Contagem] TM1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS Contagem_QtdItens, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T2 ON T2.[Contagem_Codigo] = TM1.[Contagem_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Contagem_QtdItens, CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = "
          + " 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END AS ContagemItem_ValidacaoStatusFinal, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END = 1 GROUP BY [ContagemItem_FSValidacao], [ContagemItem_FMValidacao], [Contagem_Codigo] ) T3 ON T3.[Contagem_Codigo] = TM1.[Contagem_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = TM1.[Contagem_AreaTrabalhoCod]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [Sistema] T7 WITH (NOLOCK) ON T7.[Sistema_Codigo] = TM1.[Contagem_SistemaCod]) LEFT JOIN [Projeto] T8 WITH (NOLOCK) ON T8.[Projeto_Codigo] = TM1.[Contagem_ProjetoCod]) WHERE TM1.[Contagem_Codigo] = @Contagem_Codigo ORDER BY TM1.[Contagem_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT001610 ;
          prmT001610 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001612 ;
          prmT001612 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001617 ;
          prmT001617 = new Object[] {
          new Object[] {"@Contagem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@Contagem_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00164 ;
          prmT00164 = new Object[] {
          new Object[] {"@Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00165 ;
          prmT00165 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00168 ;
          prmT00168 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00166 ;
          prmT00166 = new Object[] {
          new Object[] {"@Contagem_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00167 ;
          prmT00167 = new Object[] {
          new Object[] {"@Contagem_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001619 ;
          prmT001619 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001621 ;
          prmT001621 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001622 ;
          prmT001622 = new Object[] {
          new Object[] {"@Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001623 ;
          prmT001623 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001624 ;
          prmT001624 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001625 ;
          prmT001625 = new Object[] {
          new Object[] {"@Contagem_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001626 ;
          prmT001626 = new Object[] {
          new Object[] {"@Contagem_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001627 ;
          prmT001627 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00163 ;
          prmT00163 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001628 ;
          prmT001628 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001629 ;
          prmT001629 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00162 ;
          prmT00162 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001630 ;
          prmT001630 = new Object[] {
          new Object[] {"@Contagem_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_DataCriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contagem_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Proposito",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Escopo",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Fronteira",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Notas",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Contagem_PFB",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_PFL",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@Contagem_Link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@Contagem_Fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@Contagem_Deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_Lock",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contagem_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Contagem_Consideracoes",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Contagem_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@Contagem_PFBA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFLA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFBD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFLD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_Aplicabilidade",SqlDbType.VarChar,1048576,0} ,
          new Object[] {"@Contagem_Versao",SqlDbType.VarChar,20,0} ,
          new Object[] {"@Contagem_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_ArquivoImp",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Contagem_ReferenciaINM",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_AmbienteTecnologico",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_UsuarioContadorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001631 ;
          prmT001631 = new Object[] {
          new Object[] {"@Contagem_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_DataCriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contagem_Tecnica",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Proposito",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Escopo",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Fronteira",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@Contagem_Notas",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Contagem_PFB",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_PFL",SqlDbType.Decimal,13,5} ,
          new Object[] {"@Contagem_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Contagem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@Contagem_Link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@Contagem_Fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@Contagem_Deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contagem_Lock",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contagem_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Contagem_Consideracoes",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Contagem_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@Contagem_PFBA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFLA",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFBD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_PFLD",SqlDbType.Decimal,14,5} ,
          new Object[] {"@Contagem_Aplicabilidade",SqlDbType.VarChar,1048576,0} ,
          new Object[] {"@Contagem_Versao",SqlDbType.VarChar,20,0} ,
          new Object[] {"@Contagem_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_ArquivoImp",SqlDbType.VarChar,250,0} ,
          new Object[] {"@Contagem_ReferenciaINM",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_AmbienteTecnologico",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_UsuarioContadorCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001632 ;
          prmT001632 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001642 ;
          prmT001642 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001643 ;
          prmT001643 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001644 ;
          prmT001644 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001645 ;
          prmT001645 = new Object[] {
          } ;
          Object[] prmT001646 ;
          prmT001646 = new Object[] {
          } ;
          Object[] prmT001634 ;
          prmT001634 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001636 ;
          prmT001636 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001637 ;
          prmT001637 = new Object[] {
          new Object[] {"@Contagem_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001638 ;
          prmT001638 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001639 ;
          prmT001639 = new Object[] {
          new Object[] {"@Contagem_UsuarioContadorPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001647 ;
          prmT001647 = new Object[] {
          new Object[] {"@Contagem_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@Contagem_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001640 ;
          prmT001640 = new Object[] {
          new Object[] {"@Contagem_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001641 ;
          prmT001641 = new Object[] {
          new Object[] {"@Contagem_ProjetoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00162", "SELECT [Contagem_Codigo], [Contagem_ContratadaCod], [Contagem_DataCriacao], [Contagem_Tecnica], [Contagem_Tipo], [Contagem_Proposito], [Contagem_Escopo], [Contagem_Fronteira], [Contagem_Observacao], [Contagem_Notas], [Contagem_PFB], [Contagem_PFL], [Contagem_Divergencia], [Contagem_Status], [Contagem_Demanda], [Contagem_Link], [Contagem_Fator], [Contagem_Deflator], [Contagem_Lock], [Contagem_DataHomologacao], [Contagem_Consideracoes], [Contagem_Descricao], [Contagem_PFBA], [Contagem_PFLA], [Contagem_PFBD], [Contagem_PFLD], [Contagem_Aplicabilidade], [Contagem_Versao], [Contagem_ServicoCod], [Contagem_ArquivoImp], [Contagem_ReferenciaINM], [Contagem_AmbienteTecnologico], [Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, [Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, [Contagem_SistemaCod] AS Contagem_SistemaCod, [Contagem_ProjetoCod] AS Contagem_ProjetoCod FROM [Contagem] WITH (UPDLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00162,1,0,true,false )
             ,new CursorDef("T00163", "SELECT [Contagem_Codigo], [Contagem_ContratadaCod], [Contagem_DataCriacao], [Contagem_Tecnica], [Contagem_Tipo], [Contagem_Proposito], [Contagem_Escopo], [Contagem_Fronteira], [Contagem_Observacao], [Contagem_Notas], [Contagem_PFB], [Contagem_PFL], [Contagem_Divergencia], [Contagem_Status], [Contagem_Demanda], [Contagem_Link], [Contagem_Fator], [Contagem_Deflator], [Contagem_Lock], [Contagem_DataHomologacao], [Contagem_Consideracoes], [Contagem_Descricao], [Contagem_PFBA], [Contagem_PFLA], [Contagem_PFBD], [Contagem_PFLD], [Contagem_Aplicabilidade], [Contagem_Versao], [Contagem_ServicoCod], [Contagem_ArquivoImp], [Contagem_ReferenciaINM], [Contagem_AmbienteTecnologico], [Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, [Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, [Contagem_SistemaCod] AS Contagem_SistemaCod, [Contagem_ProjetoCod] AS Contagem_ProjetoCod FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00163,1,0,true,false )
             ,new CursorDef("T00164", "SELECT [AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contagem_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00164,1,0,true,false )
             ,new CursorDef("T00165", "SELECT [Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contagem_UsuarioContadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00165,1,0,true,false )
             ,new CursorDef("T00166", "SELECT [Sistema_Sigla] AS Contagem_SistemaSigla, [Sistema_Coordenacao] AS Contagem_SistemaCoord FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Contagem_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00166,1,0,true,false )
             ,new CursorDef("T00167", "SELECT [Projeto_Sigla] AS Contagem_ProjetoSigla FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Contagem_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00167,1,0,true,false )
             ,new CursorDef("T00168", "SELECT [Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contagem_UsuarioContadorPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00168,1,0,true,false )
             ,new CursorDef("T001610", "SELECT COALESCE( T1.[Contagem_QtdItens], 0) AS Contagem_QtdItens FROM (SELECT COUNT(*) AS Contagem_QtdItens, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001610,1,0,true,false )
             ,new CursorDef("T001612", "SELECT COALESCE( T1.[Contagem_QtdItens], 0) AS Contagem_QtdItensAprovados FROM (SELECT COUNT(*) AS Contagem_QtdItens, CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END AS ContagemItem_ValidacaoStatusFinal, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END = 1 GROUP BY [ContagemItem_FSValidacao], [ContagemItem_FMValidacao], [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001612,1,0,true,false )
             ,new CursorDef("T001613", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001613,0,0,true,false )
             ,new CursorDef("T001616", cmdBufferT001616,true, GxErrorMask.GX_NOMASK, false, this,prmT001616,100,0,true,false )
             ,new CursorDef("T001617", "SELECT [Contagem_Demanda] FROM [Contagem] WITH (NOLOCK) WHERE ([Contagem_Demanda] = @Contagem_Demanda AND [Contagem_ContratadaCod] = @Contagem_ContratadaCod) AND (Not ( [Contagem_Codigo] = @Contagem_Codigo)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT001617,1,0,true,false )
             ,new CursorDef("T001619", "SELECT COALESCE( T1.[Contagem_QtdItens], 0) AS Contagem_QtdItens FROM (SELECT COUNT(*) AS Contagem_QtdItens, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001619,1,0,true,false )
             ,new CursorDef("T001621", "SELECT COALESCE( T1.[Contagem_QtdItens], 0) AS Contagem_QtdItensAprovados FROM (SELECT COUNT(*) AS Contagem_QtdItens, CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END AS ContagemItem_ValidacaoStatusFinal, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END = 1 GROUP BY [ContagemItem_FSValidacao], [ContagemItem_FMValidacao], [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001621,1,0,true,false )
             ,new CursorDef("T001622", "SELECT [AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contagem_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001622,1,0,true,false )
             ,new CursorDef("T001623", "SELECT [Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contagem_UsuarioContadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001623,1,0,true,false )
             ,new CursorDef("T001624", "SELECT [Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contagem_UsuarioContadorPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001624,1,0,true,false )
             ,new CursorDef("T001625", "SELECT [Sistema_Sigla] AS Contagem_SistemaSigla, [Sistema_Coordenacao] AS Contagem_SistemaCoord FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Contagem_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001625,1,0,true,false )
             ,new CursorDef("T001626", "SELECT [Projeto_Sigla] AS Contagem_ProjetoSigla FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Contagem_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001626,1,0,true,false )
             ,new CursorDef("T001627", "SELECT [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001627,1,0,true,false )
             ,new CursorDef("T001628", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE ( [Contagem_Codigo] > @Contagem_Codigo) ORDER BY [Contagem_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001628,1,0,true,true )
             ,new CursorDef("T001629", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE ( [Contagem_Codigo] < @Contagem_Codigo) ORDER BY [Contagem_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001629,1,0,true,true )
             ,new CursorDef("T001630", "INSERT INTO [Contagem]([Contagem_ContratadaCod], [Contagem_DataCriacao], [Contagem_Tecnica], [Contagem_Tipo], [Contagem_Proposito], [Contagem_Escopo], [Contagem_Fronteira], [Contagem_Observacao], [Contagem_Notas], [Contagem_PFB], [Contagem_PFL], [Contagem_Divergencia], [Contagem_Status], [Contagem_Demanda], [Contagem_Link], [Contagem_Fator], [Contagem_Deflator], [Contagem_Lock], [Contagem_DataHomologacao], [Contagem_Consideracoes], [Contagem_Descricao], [Contagem_PFBA], [Contagem_PFLA], [Contagem_PFBD], [Contagem_PFLD], [Contagem_Aplicabilidade], [Contagem_Versao], [Contagem_ServicoCod], [Contagem_ArquivoImp], [Contagem_ReferenciaINM], [Contagem_AmbienteTecnologico], [Contagem_AreaTrabalhoCod], [Contagem_UsuarioContadorCod], [Contagem_SistemaCod], [Contagem_ProjetoCod]) VALUES(@Contagem_ContratadaCod, @Contagem_DataCriacao, @Contagem_Tecnica, @Contagem_Tipo, @Contagem_Proposito, @Contagem_Escopo, @Contagem_Fronteira, @Contagem_Observacao, @Contagem_Notas, @Contagem_PFB, @Contagem_PFL, @Contagem_Divergencia, @Contagem_Status, @Contagem_Demanda, @Contagem_Link, @Contagem_Fator, @Contagem_Deflator, @Contagem_Lock, @Contagem_DataHomologacao, @Contagem_Consideracoes, @Contagem_Descricao, @Contagem_PFBA, @Contagem_PFLA, @Contagem_PFBD, @Contagem_PFLD, @Contagem_Aplicabilidade, @Contagem_Versao, @Contagem_ServicoCod, @Contagem_ArquivoImp, @Contagem_ReferenciaINM, @Contagem_AmbienteTecnologico, @Contagem_AreaTrabalhoCod, @Contagem_UsuarioContadorCod, @Contagem_SistemaCod, @Contagem_ProjetoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001630)
             ,new CursorDef("T001631", "UPDATE [Contagem] SET [Contagem_ContratadaCod]=@Contagem_ContratadaCod, [Contagem_DataCriacao]=@Contagem_DataCriacao, [Contagem_Tecnica]=@Contagem_Tecnica, [Contagem_Tipo]=@Contagem_Tipo, [Contagem_Proposito]=@Contagem_Proposito, [Contagem_Escopo]=@Contagem_Escopo, [Contagem_Fronteira]=@Contagem_Fronteira, [Contagem_Observacao]=@Contagem_Observacao, [Contagem_Notas]=@Contagem_Notas, [Contagem_PFB]=@Contagem_PFB, [Contagem_PFL]=@Contagem_PFL, [Contagem_Divergencia]=@Contagem_Divergencia, [Contagem_Status]=@Contagem_Status, [Contagem_Demanda]=@Contagem_Demanda, [Contagem_Link]=@Contagem_Link, [Contagem_Fator]=@Contagem_Fator, [Contagem_Deflator]=@Contagem_Deflator, [Contagem_Lock]=@Contagem_Lock, [Contagem_DataHomologacao]=@Contagem_DataHomologacao, [Contagem_Consideracoes]=@Contagem_Consideracoes, [Contagem_Descricao]=@Contagem_Descricao, [Contagem_PFBA]=@Contagem_PFBA, [Contagem_PFLA]=@Contagem_PFLA, [Contagem_PFBD]=@Contagem_PFBD, [Contagem_PFLD]=@Contagem_PFLD, [Contagem_Aplicabilidade]=@Contagem_Aplicabilidade, [Contagem_Versao]=@Contagem_Versao, [Contagem_ServicoCod]=@Contagem_ServicoCod, [Contagem_ArquivoImp]=@Contagem_ArquivoImp, [Contagem_ReferenciaINM]=@Contagem_ReferenciaINM, [Contagem_AmbienteTecnologico]=@Contagem_AmbienteTecnologico, [Contagem_AreaTrabalhoCod]=@Contagem_AreaTrabalhoCod, [Contagem_UsuarioContadorCod]=@Contagem_UsuarioContadorCod, [Contagem_SistemaCod]=@Contagem_SistemaCod, [Contagem_ProjetoCod]=@Contagem_ProjetoCod  WHERE [Contagem_Codigo] = @Contagem_Codigo", GxErrorMask.GX_NOMASK,prmT001631)
             ,new CursorDef("T001632", "DELETE FROM [Contagem]  WHERE [Contagem_Codigo] = @Contagem_Codigo", GxErrorMask.GX_NOMASK,prmT001632)
             ,new CursorDef("T001634", "SELECT COALESCE( T1.[Contagem_QtdItens], 0) AS Contagem_QtdItens FROM (SELECT COUNT(*) AS Contagem_QtdItens, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) GROUP BY [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001634,1,0,true,false )
             ,new CursorDef("T001636", "SELECT COALESCE( T1.[Contagem_QtdItens], 0) AS Contagem_QtdItensAprovados FROM (SELECT COUNT(*) AS Contagem_QtdItens, CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END AS ContagemItem_ValidacaoStatusFinal, [Contagem_Codigo] FROM [ContagemItem] WITH (NOLOCK) WHERE CASE  WHEN ( COALESCE( [ContagemItem_FSValidacao], 0) = 1) and ( COALESCE( [ContagemItem_FMValidacao], 0) = 1) THEN 1 ELSE 2 END = 1 GROUP BY [ContagemItem_FSValidacao], [ContagemItem_FMValidacao], [Contagem_Codigo] ) T1 WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001636,1,0,true,false )
             ,new CursorDef("T001637", "SELECT [AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Contagem_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001637,1,0,true,false )
             ,new CursorDef("T001638", "SELECT [Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Contagem_UsuarioContadorCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001638,1,0,true,false )
             ,new CursorDef("T001639", "SELECT [Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contagem_UsuarioContadorPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001639,1,0,true,false )
             ,new CursorDef("T001640", "SELECT [Sistema_Sigla] AS Contagem_SistemaSigla, [Sistema_Coordenacao] AS Contagem_SistemaCoord FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Contagem_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001640,1,0,true,false )
             ,new CursorDef("T001641", "SELECT [Projeto_Sigla] AS Contagem_ProjetoSigla FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Contagem_ProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001641,1,0,true,false )
             ,new CursorDef("T001642", "SELECT TOP 1 [ContagemHistorico_Codigo] FROM [ContagemHistorico] WITH (NOLOCK) WHERE [ContagemHistorico_Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001642,1,0,true,true )
             ,new CursorDef("T001643", "SELECT TOP 1 [Baseline_Codigo] FROM [Baseline] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001643,1,0,true,true )
             ,new CursorDef("T001644", "SELECT TOP 1 [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001644,1,0,true,true )
             ,new CursorDef("T001645", "SELECT [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) ORDER BY [Contagem_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001645,100,0,true,false )
             ,new CursorDef("T001646", "SELECT [Projeto_Codigo], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001646,0,0,true,false )
             ,new CursorDef("T001647", "SELECT [Contagem_Demanda] FROM [Contagem] WITH (NOLOCK) WHERE ([Contagem_Demanda] = @Contagem_Demanda AND [Contagem_ContratadaCod] = @Contagem_ContratadaCod) AND (Not ( [Contagem_Codigo] = @Contagem_Codigo)) ",true, GxErrorMask.GX_NOMASK, false, this,prmT001647,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((bool[]) buf[34])[0] = rslt.getBool(19) ;
                ((DateTime[]) buf[35])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((String[]) buf[37])[0] = rslt.getLongVarchar(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((String[]) buf[39])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((decimal[]) buf[43])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(24);
                ((decimal[]) buf[45])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(25);
                ((decimal[]) buf[47])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(26);
                ((String[]) buf[49])[0] = rslt.getLongVarchar(27) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(27);
                ((String[]) buf[51])[0] = rslt.getVarchar(28) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(28);
                ((int[]) buf[53])[0] = rslt.getInt(29) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(29);
                ((String[]) buf[55])[0] = rslt.getVarchar(30) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(30);
                ((int[]) buf[57])[0] = rslt.getInt(31) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(31);
                ((int[]) buf[59])[0] = rslt.getInt(32) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(32);
                ((int[]) buf[61])[0] = rslt.getInt(33) ;
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                ((int[]) buf[66])[0] = rslt.getInt(36) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(36);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((decimal[]) buf[30])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((bool[]) buf[34])[0] = rslt.getBool(19) ;
                ((DateTime[]) buf[35])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((String[]) buf[37])[0] = rslt.getLongVarchar(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((String[]) buf[39])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((decimal[]) buf[43])[0] = rslt.getDecimal(24) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(24);
                ((decimal[]) buf[45])[0] = rslt.getDecimal(25) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(25);
                ((decimal[]) buf[47])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(26);
                ((String[]) buf[49])[0] = rslt.getLongVarchar(27) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(27);
                ((String[]) buf[51])[0] = rslt.getVarchar(28) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(28);
                ((int[]) buf[53])[0] = rslt.getInt(29) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(29);
                ((String[]) buf[55])[0] = rslt.getVarchar(30) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(30);
                ((int[]) buf[57])[0] = rslt.getInt(31) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(31);
                ((int[]) buf[59])[0] = rslt.getInt(32) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(32);
                ((int[]) buf[61])[0] = rslt.getInt(33) ;
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                ((int[]) buf[66])[0] = rslt.getInt(36) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(36);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 100) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((decimal[]) buf[36])[0] = rslt.getDecimal(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((String[]) buf[38])[0] = rslt.getString(21, 25) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((String[]) buf[40])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((String[]) buf[42])[0] = rslt.getString(23, 15) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((bool[]) buf[44])[0] = rslt.getBool(24) ;
                ((DateTime[]) buf[45])[0] = rslt.getGXDateTime(25) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(25);
                ((String[]) buf[47])[0] = rslt.getLongVarchar(26) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(26);
                ((String[]) buf[49])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(27);
                ((decimal[]) buf[51])[0] = rslt.getDecimal(28) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(28);
                ((decimal[]) buf[53])[0] = rslt.getDecimal(29) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(29);
                ((decimal[]) buf[55])[0] = rslt.getDecimal(30) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(30);
                ((decimal[]) buf[57])[0] = rslt.getDecimal(31) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(31);
                ((String[]) buf[59])[0] = rslt.getLongVarchar(32) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(32);
                ((String[]) buf[61])[0] = rslt.getVarchar(33) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(33);
                ((int[]) buf[63])[0] = rslt.getInt(34) ;
                ((bool[]) buf[64])[0] = rslt.wasNull(34);
                ((String[]) buf[65])[0] = rslt.getVarchar(35) ;
                ((bool[]) buf[66])[0] = rslt.wasNull(35);
                ((int[]) buf[67])[0] = rslt.getInt(36) ;
                ((bool[]) buf[68])[0] = rslt.wasNull(36);
                ((int[]) buf[69])[0] = rslt.getInt(37) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(37);
                ((int[]) buf[71])[0] = rslt.getInt(38) ;
                ((int[]) buf[72])[0] = rslt.getInt(39) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(39);
                ((int[]) buf[74])[0] = rslt.getInt(40) ;
                ((bool[]) buf[75])[0] = rslt.wasNull(40);
                ((int[]) buf[76])[0] = rslt.getInt(41) ;
                ((bool[]) buf[77])[0] = rslt.wasNull(41);
                ((int[]) buf[78])[0] = rslt.getInt(42) ;
                ((bool[]) buf[79])[0] = rslt.wasNull(42);
                ((short[]) buf[80])[0] = rslt.getShort(43) ;
                ((bool[]) buf[81])[0] = rslt.wasNull(43);
                ((short[]) buf[82])[0] = rslt.getShort(44) ;
                ((bool[]) buf[83])[0] = rslt.wasNull(44);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 31 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 37 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (DateTime)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(17, (decimal)parms[32]);
                }
                stmt.SetParameter(18, (bool)parms[33]);
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(19, (DateTime)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 26 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 27 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(27, (String)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 29 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(29, (String)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[59]);
                }
                stmt.SetParameter(32, (int)parms[60]);
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 34 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(34, (int)parms[64]);
                }
                if ( (bool)parms[65] )
                {
                   stmt.setNull( 35 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(35, (int)parms[66]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (DateTime)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 17 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(17, (decimal)parms[32]);
                }
                stmt.SetParameter(18, (bool)parms[33]);
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(19, (DateTime)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(22, (decimal)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 25 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(25, (decimal)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 26 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 27 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(27, (String)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 29 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(29, (String)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[59]);
                }
                stmt.SetParameter(32, (int)parms[60]);
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 34 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(34, (int)parms[64]);
                }
                if ( (bool)parms[65] )
                {
                   stmt.setNull( 35 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(35, (int)parms[66]);
                }
                stmt.SetParameter(36, (int)parms[67]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
