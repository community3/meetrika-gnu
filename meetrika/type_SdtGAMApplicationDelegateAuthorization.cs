/*
               File: type_SdtGAMApplicationDelegateAuthorization
        Description: GAMApplicationDelegateAuthorization
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:10.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMApplicationDelegateAuthorization : GxUserType, IGxExternalObject
   {
      public SdtGAMApplicationDelegateAuthorization( )
      {
         initialize();
      }

      public SdtGAMApplicationDelegateAuthorization( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMApplicationDelegateAuthorization_externalReference == null )
         {
            GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
         }
         returntostring = "";
         returntostring = (String)(GAMApplicationDelegateAuthorization_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Version
      {
         get {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            return GAMApplicationDelegateAuthorization_externalReference.Version ;
         }

         set {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            GAMApplicationDelegateAuthorization_externalReference.Version = value;
         }

      }

      public String gxTpr_Filename
      {
         get {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            return GAMApplicationDelegateAuthorization_externalReference.FileName ;
         }

         set {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            GAMApplicationDelegateAuthorization_externalReference.FileName = value;
         }

      }

      public String gxTpr_Package
      {
         get {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            return GAMApplicationDelegateAuthorization_externalReference.Package ;
         }

         set {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            GAMApplicationDelegateAuthorization_externalReference.Package = value;
         }

      }

      public String gxTpr_Classname
      {
         get {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            return GAMApplicationDelegateAuthorization_externalReference.ClassName ;
         }

         set {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            GAMApplicationDelegateAuthorization_externalReference.ClassName = value;
         }

      }

      public String gxTpr_Method
      {
         get {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            return GAMApplicationDelegateAuthorization_externalReference.Method ;
         }

         set {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            GAMApplicationDelegateAuthorization_externalReference.Method = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMApplicationDelegateAuthorization_externalReference == null )
            {
               GAMApplicationDelegateAuthorization_externalReference = new Artech.Security.GAMApplicationDelegateAuthorization(context);
            }
            return GAMApplicationDelegateAuthorization_externalReference ;
         }

         set {
            GAMApplicationDelegateAuthorization_externalReference = (Artech.Security.GAMApplicationDelegateAuthorization)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMApplicationDelegateAuthorization GAMApplicationDelegateAuthorization_externalReference=null ;
   }

}
