/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:24:0.4
*/
gx.evt.autoSkip = false;
gx.define('wp_ss', false, function () {
   this.ServerClass =  "wp_ss" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV22WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.A155Servico_Codigo=gx.fn.getIntegerValue("SERVICO_CODIGO",'.') ;
      this.A1551Servico_Responsavel=gx.fn.getIntegerValue("SERVICO_RESPONSAVEL",'.') ;
      this.A1Usuario_Codigo=gx.fn.getIntegerValue("USUARIO_CODIGO",'.') ;
      this.A58Usuario_PessoaNom=gx.fn.getControlValue("USUARIO_PESSOANOM") ;
      this.A1075Usuario_CargoUOCod=gx.fn.getIntegerValue("USUARIO_CARGOUOCOD",'.') ;
      this.A1076Usuario_CargoUONom=gx.fn.getControlValue("USUARIO_CARGOUONOM") ;
      this.AV16SolicitacaoServico=gx.fn.getControlValue("vSOLICITACAOSERVICO") ;
      this.A157ServicoGrupo_Codigo=gx.fn.getIntegerValue("SERVICOGRUPO_CODIGO",'.') ;
      this.A605Servico_Sigla=gx.fn.getControlValue("SERVICO_SIGLA") ;
      this.A158ServicoGrupo_Descricao=gx.fn.getControlValue("SERVICOGRUPO_DESCRICAO") ;
      this.A2047Servico_LinNegCod=gx.fn.getIntegerValue("SERVICO_LINNEGCOD",'.') ;
      this.A632Servico_Ativo=gx.fn.getControlValue("SERVICO_ATIVO") ;
      this.A1635Servico_IsPublico=gx.fn.getControlValue("SERVICO_ISPUBLICO") ;
      this.A63ContratanteUsuario_ContratanteCod=gx.fn.getIntegerValue("CONTRATANTEUSUARIO_CONTRATANTECOD",'.') ;
      this.A60ContratanteUsuario_UsuarioCod=gx.fn.getIntegerValue("CONTRATANTEUSUARIO_USUARIOCOD",'.') ;
      this.A127Sistema_Codigo=gx.fn.getIntegerValue("SISTEMA_CODIGO",'.') ;
      this.A129Sistema_Sigla=gx.fn.getControlValue("SISTEMA_SIGLA") ;
      this.A633Servico_UO=gx.fn.getIntegerValue("SERVICO_UO",'.') ;
      this.AV17Usuario_CargoUOCod=gx.fn.getIntegerValue("vUSUARIO_CARGOUOCOD",'.') ;
      this.A1530Servico_TipoHierarquia=gx.fn.getIntegerValue("SERVICO_TIPOHIERARQUIA",'.') ;
      this.AV106SDT_Requisitos=gx.fn.getControlValue("vSDT_REQUISITOS") ;
      this.AV87Caller=gx.fn.getControlValue("vCALLER") ;
      this.AV113ContagemResultadoRequisito=gx.fn.getControlValue("vCONTAGEMRESULTADOREQUISITO") ;
      this.AV67Codigo=gx.fn.getIntegerValue("vCODIGO",'.') ;
      this.AV73StatusAnterior=gx.fn.getControlValue("vSTATUSANTERIOR") ;
      this.AV5CheckRequiredFieldsResult=gx.fn.getControlValue("vCHECKREQUIREDFIELDSRESULT") ;
      this.AV82DadosDaSS=gx.fn.getControlValue("vDADOSDASS") ;
      this.AV96Requisito=gx.fn.getControlValue("vREQUISITO") ;
      this.A2003ContagemResultadoRequisito_OSCod=gx.fn.getIntegerValue("CONTAGEMRESULTADOREQUISITO_OSCOD",'.') ;
      this.A2004ContagemResultadoRequisito_ReqCod=gx.fn.getIntegerValue("CONTAGEMRESULTADOREQUISITO_REQCOD",'.') ;
      this.A2005ContagemResultadoRequisito_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADOREQUISITO_CODIGO",'.') ;
      this.AV27ContratanteSemEmailSda=gx.fn.getControlValue("vCONTRATANTESEMEMAILSDA") ;
      this.AV93Codigos=gx.fn.getControlValue("vCODIGOS") ;
      this.AV20Usuarios=gx.fn.getControlValue("vUSUARIOS") ;
      this.AV23Attachments=gx.fn.getControlValue("vATTACHMENTS") ;
      this.AV25Resultado=gx.fn.getControlValue("vRESULTADO") ;
      this.AV66Ordem=gx.fn.getIntegerValue("vORDEM",'.') ;
      this.AV104Requisito_Pontuacao=gx.fn.getDecimalValue("vREQUISITO_PONTUACAO",'.',',') ;
      this.AV78i=gx.fn.getIntegerValue("vI",'.') ;
      this.A1926Requisito_Agrupador=gx.fn.getControlValue("REQUISITO_AGRUPADOR") ;
      this.AV107SDT_RequisitoItem=gx.fn.getControlValue("vSDT_REQUISITOITEM") ;
      this.A2001Requisito_Identificador=gx.fn.getControlValue("REQUISITO_IDENTIFICADOR") ;
      this.A1927Requisito_Titulo=gx.fn.getControlValue("REQUISITO_TITULO") ;
      this.A1923Requisito_Descricao=gx.fn.getControlValue("REQUISITO_DESCRICAO") ;
      this.A2002Requisito_Prioridade=gx.fn.getIntegerValue("REQUISITO_PRIORIDADE",'.') ;
      this.A1932Requisito_Pontuacao=gx.fn.getDecimalValue("REQUISITO_PONTUACAO",'.',',') ;
      this.A1934Requisito_Status=gx.fn.getIntegerValue("REQUISITO_STATUS",'.') ;
      this.A586ContagemResultadoEvidencia_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADOEVIDENCIA_CODIGO",'.') ;
      this.A588ContagemResultadoEvidencia_Arquivo=gx.fn.getBlobValue("CONTAGEMRESULTADOEVIDENCIA_ARQUIVO") ;
      this.A1449ContagemResultadoEvidencia_Link=gx.fn.getControlValue("CONTAGEMRESULTADOEVIDENCIA_LINK") ;
      this.A589ContagemResultadoEvidencia_NomeArq=gx.fn.getControlValue("CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") ;
      this.A590ContagemResultadoEvidencia_TipoArq=gx.fn.getControlValue("CONTAGEMRESULTADOEVIDENCIA_TIPOARQ") ;
      this.A591ContagemResultadoEvidencia_Data=gx.fn.getDateTimeValue("CONTAGEMRESULTADOEVIDENCIA_DATA") ;
      this.A587ContagemResultadoEvidencia_Descricao=gx.fn.getControlValue("CONTAGEMRESULTADOEVIDENCIA_DESCRICAO") ;
      this.A1393ContagemResultadoEvidencia_RdmnCreated=gx.fn.getDateTimeValue("CONTAGEMRESULTADOEVIDENCIA_RDMNCREATED") ;
      this.A645TipoDocumento_Codigo=gx.fn.getIntegerValue("TIPODOCUMENTO_CODIGO",'.') ;
      this.A1493ContagemResultadoEvidencia_Owner=gx.fn.getIntegerValue("CONTAGEMRESULTADOEVIDENCIA_OWNER",'.') ;
      this.AV74ActiveTabId=gx.fn.getControlValue("vACTIVETABID") ;
      this.AV106SDT_Requisitos=gx.fn.getControlValue("vSDT_REQUISITOS") ;
      this.AV141GXV14=gx.fn.getIntegerValue("vGXV14",'.') ;
   };
   this.Validv_Servico_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vSERVICO_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Dataprevista=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDATAPREVISTA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV83DataPrevista)==0) || new gx.date.gxdate( this.AV83DataPrevista ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Data Prevista fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Requisito_prioridade=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vREQUISITO_PRIORIDADE");
         this.AnyError  = 0;
         if ( ! ( ( this.AV102Requisito_Prioridade == 1 ) || ( this.AV102Requisito_Prioridade == 2 ) || ( this.AV102Requisito_Prioridade == 3 ) || ( this.AV102Requisito_Prioridade == 4 ) || ( this.AV102Requisito_Prioridade == 5 ) || ( this.AV102Requisito_Prioridade == 6 ) || ( this.AV102Requisito_Prioridade == 7 ) || ( this.AV102Requisito_Prioridade == 8 ) || ( this.AV102Requisito_Prioridade == 9 ) ) )
         {
            try {
               gxballoon.setError("Campo Prioridade fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Requisito_status=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vREQUISITO_STATUS");
         this.AnyError  = 0;
         if ( ! ( ( this.AV103Requisito_Status == 0 ) || ( this.AV103Requisito_Status == 1 ) || ( this.AV103Requisito_Status == 2 ) || ( this.AV103Requisito_Status == 3 ) || ( this.AV103Requisito_Status == 4 ) || ( this.AV103Requisito_Status == 5 ) ) )
         {
            try {
               gxballoon.setError("Campo Status fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contagemresultado_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(178) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_CODIGO", gx.fn.currentGridRowImpl(178));
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Linhanegocio_codigo=function()
   {
      try {
         gx.ajax.validSrvEvt("dyncall","Validv_Linhanegocio_codigo",["gx.O.AV114LinhaNegocio_Codigo", "gx.O.AV115Requisito_TipoReqCod"],["AV115Requisito_TipoReqCod"]);
      } finally {
      }
      return true;
   }
   this.Valid_Contagemresultado_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(178) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("CONTAGEMRESULTADO_CODIGO", gx.fn.currentGridRowImpl(178));
         this.AnyError  = 0;
         this.standaloneModalMJ3( );
         this.standaloneNotModalMJ3( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s182_client=function()
   {
      if ( this.AV35TemServicoPadrao )
      {
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_RESTRICOES","Visible", false );
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA","Visible", false );
         gx.fn.setCtrlProperty("vDATAPREVISTA","Visible", false );
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_REFERENCIA","Visible", false );
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_SS","Visible", false );
         gx.fn.setCtrlProperty("vSERVICOGRUPO_CODIGO","Visible", false );
         gx.fn.setCtrlProperty("vSERVICO_CODIGO","Visible", false );
         gx.fn.setCtrlProperty("TBJAVA","Caption", "<script type='text/javascript'> DADOSDASS_CONTAGEMRESULTADO_DESCRICAO.rows=1; </script>" );
      }
      else
      {
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_RESTRICOES","Visible", true );
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA","Visible", true );
         gx.fn.setCtrlProperty("vDATAPREVISTA","Visible", true );
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_REFERENCIA","Visible", true );
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA","Visible", true );
         gx.fn.setCtrlProperty("DADOSDASS_CONTAGEMRESULTADO_SS","Visible", true );
         gx.fn.setCtrlProperty("vSERVICOGRUPO_CODIGO","Visible", true );
         gx.fn.setCtrlProperty("vSERVICO_CODIGO","Visible", true );
         gx.fn.setCtrlProperty("TBJAVA","Caption", "<script type='text/javascript'> DADOSDASS_CONTAGEMRESULTADO_OBSERVACAO.rows=12; </script>" );
      }
   };
   this.e30mj1_client=function()
   {
      this.clearMessages();
      this.AV141GXV14 = gx.num.trunc( 1 ,0) ;
      while ( this.AV141GXV14 <= this.AV106SDT_Requisitos.length )
      {
         this.AV107SDT_RequisitoItem =  this.AV106SDT_Requisitos[this.AV141GXV14 - 1]  ;
         if ( ( this.AV107SDT_RequisitoItem.Requisito_Codigo >= 0 ) && ( this.AV100Requisito_Identificador == this.AV107SDT_RequisitoItem.Requisito_Identificador ) )
         {
            this.addMessage("Identificador já incluso, altere o existente ou confira o novo a ser inserido!");
            gx.fn.usrSetFocus("vREQUISITO_IDENTIFICADOR") ;
            break;
         }
         this.AV141GXV14 = gx.num.trunc( this.AV141GXV14 + 1 ,0) ;
      }
      this.refreshOutputs([{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null}]);
   };
   this.s212_client=function()
   {
      if ( ( this.AV14Servico_Codigo > 0 ) && ((0==this.AV29Servico_Responsavel)) )
      {
         gx.fn.setCtrlProperty("BTNENTER","Tooltiptext", "O Serviço não tem responsável cadastrado!" );
         this.addMessage("Serviço sem responsável cadastrado. Esta solicitação não poderá ser enviada.");
         gx.fn.setCtrlProperty("BTNENTER","Enabled", false );
      }
      else if ( ! this.AV22WWPContext.Insert )
      {
         gx.fn.setCtrlProperty("BTNENTER","Tooltiptext", "Ação não permitida ao seu usuário!" );
         gx.fn.setCtrlProperty("BTNENTER","Enabled", false );
      }
      else
      {
         gx.fn.setCtrlProperty("BTNENTER","Tooltiptext", "Solicitar serviço" );
         gx.fn.setCtrlProperty("BTNENTER","Enabled", true );
      }
   };
   this.e28mj1_client=function()
   {
      this.clearMessages();
      this.s212_client();
      this.refreshOutputs([{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNENTER',prop:'Enabled'}]);
   };
   this.e31mj1_client=function()
   {
      this.clearMessages();
      this.AV74ActiveTabId =  this.GXUITABSPANEL_TABSContainer.ActiveTabId  ;
      if ( ( this.AV74ActiveTabId == "TabRequisitos" ) && ((''==this.AV97Requisito_Agrupador)) )
      {
         gx.fn.usrSetFocus("vREQUISITO_AGRUPADOR") ;
      }
      gx.fn.setCtrlProperty("TABLEACTIONS","Visible", !((this.AV74ActiveTabId=="TabRascunho")) );
      this.refreshOutputs([{av:'gx.fn.getCtrlProperty("TABLEACTIONS","Visible")',ctrl:'TABLEACTIONS',prop:'Visible'}]);
   };
   this.e29mj3_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_cancelaros.aspx",[this.A456ContagemResultado_Codigo, "Rascunho da SS "+this.A493ContagemResultado_DemandaFM, this.AV22WWPContext.UserId]), []);
      this.refreshOutputs([]);
      this.refreshGrid('Gridrascunho') ;
      this.refreshOutputs([]);
   };
   this.e32mj1_client=function()
   {
      this.clearMessages();
      if ( ((''==this.AV99Requisito_Descricao)) )
      {
         this.AV99Requisito_Descricao =  this.AV101Requisito_Titulo  ;
      }
      this.refreshOutputs([{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''}]);
   };
   this.e12mj2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e13mj2_client=function()
   {
      this.executeServerEvent("'DOFECHAR'", false, null, false, false);
   };
   this.e14mj2_client=function()
   {
      this.executeServerEvent("'DOSALVAR'", false, null, false, false);
   };
   this.e15mj2_client=function()
   {
      this.executeServerEvent("'DOBTNINCLUIRREQNEG'", false, null, false, false);
   };
   this.e16mj2_client=function()
   {
      this.executeServerEvent("VSERVICOGRUPO_CODIGO.CLICK", true, null, false, true);
   };
   this.e17mj2_client=function()
   {
      this.executeServerEvent("VSERVICO_CODIGO.CLICK", true, null, false, true);
   };
   this.e11mj2_client=function()
   {
      this.executeServerEvent("CONFIRMPANEL.CLOSE", false, null, true, true);
   };
   this.e23mj2_client=function()
   {
      this.executeServerEvent("VBTNEXCLUIRREQNEG.CLICK", true, arguments[0], false, false);
   };
   this.e24mj2_client=function()
   {
      this.executeServerEvent("VBTNALTERARREQNEG.CLICK", true, arguments[0], false, false);
   };
   this.e27mj2_client=function()
   {
      this.executeServerEvent("VBTNCONTINUAR.CLICK", true, arguments[0], false, false);
   };
   this.e18mj2_client=function()
   {
      this.executeServerEvent("VINDICADORAUTOMATICO.CLICK", true, null, false, true);
   };
   this.e33mj2_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,11,14,20,23,25,27,29,32,34,39,41,43,45,48,50,55,57,59,61,64,66,69,71,74,76,79,81,87,88,89,90,91,92,93,94,95,96,102,105,108,110,113,115,118,120,122,124,126,128,131,133,135,137,140,142,144,146,149,155,160,164,165,166,167,168,169,170,171,172,175,179,180,181,182,183,184,185,186,187,188,191,201,206,207,208,209,210];
   this.GXLastCtrlId =210;
   this.Gridsdt_requisitosContainer = new gx.grid.grid(this, 2,"WbpLvl2",163,"Gridsdt_requisitos","Gridsdt_requisitos","Gridsdt_requisitosContainer",this.CmpContext,this.IsMasterPage,"wp_ss",[],false,1,false,true,0,true,false,false,"CollSDT_Requisitos.SDT_RequisitosItem",0,"px","Novo registro",true,false,false,null,null,false,"AV106SDT_Requisitos",false,[1,1,1,1]);
   var Gridsdt_requisitosContainer = this.Gridsdt_requisitosContainer;
   Gridsdt_requisitosContainer.addBitmap("&Btnalterarreqneg","vBTNALTERARREQNEG",164,36,"px",17,"px","e24mj2_client","","","Image","");
   Gridsdt_requisitosContainer.addBitmap("&Btnexcluirreqneg","vBTNEXCLUIRREQNEG",165,36,"px",17,"px","e23mj2_client","","","Image","");
   Gridsdt_requisitosContainer.addSingleLineEdit("GXV14M",166,"SDT_REQUISITOS__REQUISITO_CODIGO","Id","","Requisito_Codigo","int",100,"px",6,6,"right",null,[],"GXV14M","GXV14M",false,0,false,false,"BootstrapAttribute",1,"");
   Gridsdt_requisitosContainer.addSingleLineEdit("Agrupador",167,"vAGRUPADOR","Agrupador","","Agrupador","svchar",0,"px",25,25,"left",null,[],"Agrupador","Agrupador",true,0,false,false,"BootstrapAttribute",1,"");
   Gridsdt_requisitosContainer.addSingleLineEdit("GXV14O",168,"SDT_REQUISITOS__REQUISITO_IDENTIFICADOR","Identificador","","Requisito_Identificador","svchar",100,"px",15,15,"left",null,[],"GXV14O","GXV14O",true,0,false,false,"BootstrapAttribute",1,"");
   Gridsdt_requisitosContainer.addSingleLineEdit("GXV14P",169,"SDT_REQUISITOS__REQUISITO_TITULO","Nome","","Requisito_Titulo","svchar",0,"px",250,80,"left",null,[],"GXV14P","GXV14P",true,0,false,false,"BootstrapAttribute",1,"");
   Gridsdt_requisitosContainer.addSingleLineEdit("GXV14Q",170,"SDT_REQUISITOS__REQUISITO_DESCRICAO","Descrição","","Requisito_Descricao","vchar",410,"px",500,80,"left",null,[],"GXV14Q","GXV14Q",true,0,false,false,"BootstrapAttribute",1,"");
   Gridsdt_requisitosContainer.addComboBox("GXV14R",171,"SDT_REQUISITOS__REQUISITO_PRIORIDADE","Prioridade","Requisito_Prioridade","int",null,0,true,false,150,"px","");
   Gridsdt_requisitosContainer.addComboBox("GXV14S",172,"SDT_REQUISITOS__REQUISITO_STATUS","Situação","Requisito_Status","int",null,0,true,false,0,"px","");
   this.setGrid(Gridsdt_requisitosContainer);
   this.GridrascunhoContainer = new gx.grid.grid(this, 3,"WbpLvl3",178,"Gridrascunho","Gridrascunho","GridrascunhoContainer",this.CmpContext,this.IsMasterPage,"wp_ss",[],false,1,false,true,0,true,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridrascunhoContainer = this.GridrascunhoContainer;
   GridrascunhoContainer.addBitmap("&Btncontinuar","vBTNCONTINUAR",179,36,"px",17,"px","e27mj2_client","","","Image","");
   GridrascunhoContainer.addBitmap("&Btncancelar","vBTNCANCELAR",180,36,"px",17,"px","e29mj3_client","","","Image","");
   GridrascunhoContainer.addSingleLineEdit(456,181,"CONTAGEMRESULTADO_CODIGO","Contagem Resultado_Codigo","","ContagemResultado_Codigo","int",0,"px",6,6,"right",null,[],456,"ContagemResultado_Codigo",false,0,false,false,"BootstrapAttribute",1,"");
   GridrascunhoContainer.addSingleLineEdit(471,182,"CONTAGEMRESULTADO_DATADMN","Data da SS","","ContagemResultado_DataDmn","date",63,"px",8,8,"right",null,[],471,"ContagemResultado_DataDmn",true,0,false,false,"BootstrapAttribute",1,"");
   GridrascunhoContainer.addSingleLineEdit(493,183,"CONTAGEMRESULTADO_DEMANDAFM","Nº da SS","","ContagemResultado_DemandaFM","svchar",80,"px",50,50,"left",null,[],493,"ContagemResultado_DemandaFM",true,0,false,false,"BootstrapAttribute",1,"");
   GridrascunhoContainer.addSingleLineEdit(494,184,"CONTAGEMRESULTADO_DESCRICAO","Titulo","","ContagemResultado_Descricao","svchar",410,"px",500,80,"left",null,[],494,"ContagemResultado_Descricao",true,0,false,false,"BootstrapAttribute",1,"");
   GridrascunhoContainer.addSingleLineEdit(472,185,"CONTAGEMRESULTADO_DATAENTREGA","Limite","","ContagemResultado_DataEntrega","date",0,"px",8,8,"right",null,[],472,"ContagemResultado_DataEntrega",true,0,false,false,"BootstrapAttribute",1,"");
   GridrascunhoContainer.addSingleLineEdit(1350,186,"CONTAGEMRESULTADO_DATACADASTRO","Cadastro","","ContagemResultado_DataCadastro","dtime",0,"px",14,14,"right",null,[],1350,"ContagemResultado_DataCadastro",true,5,false,false,"BootstrapAttribute",1,"");
   GridrascunhoContainer.addSingleLineEdit("Qtdrequisitos",187,"vQTDREQUISITOS","Requisitos","","QtdRequisitos","int",0,"px",4,4,"right",null,[],"Qtdrequisitos","QtdRequisitos",true,0,false,false,"BootstrapAttribute",1,"");
   GridrascunhoContainer.addSingleLineEdit("Qtdanexos",188,"vQTDANEXOS","Anexos","","QtdAnexos","int",0,"px",4,4,"right",null,[],"Qtdanexos","QtdAnexos",true,0,false,false,"BootstrapAttribute",1,"");
   this.setGrid(GridrascunhoContainer);
   this.CONFIRMPANELContainer = gx.uc.getNew(this, 204, 25, "BootstrapConfirmPanel", "CONFIRMPANELContainer", "Confirmpanel");
   var CONFIRMPANELContainer = this.CONFIRMPANELContainer;
   CONFIRMPANELContainer.setProp("Width", "Width", "100", "str");
   CONFIRMPANELContainer.setProp("Height", "Height", "100", "str");
   CONFIRMPANELContainer.setProp("Title", "Title", "Confirmação", "str");
   CONFIRMPANELContainer.setDynProp("ConfirmationText", "Confirmationtext", "", "str");
   CONFIRMPANELContainer.setProp("YesButtonCaption", "Yesbuttoncaption", "Fechar", "str");
   CONFIRMPANELContainer.setProp("NoButtonCaption", "Nobuttoncaption", "", "str");
   CONFIRMPANELContainer.setProp("CancelButtonCaption", "Cancelbuttoncaption", "", "str");
   CONFIRMPANELContainer.setProp("YesButtonPosition", "Yesbuttonposition", "left", "str");
   CONFIRMPANELContainer.setProp("ConfirmType", "Confirmtype", "0", "str");
   CONFIRMPANELContainer.setProp("Result", "Result", "", "char");
   CONFIRMPANELContainer.setProp("TextType", "Texttype", "1", "str");
   CONFIRMPANELContainer.setProp("Visible", "Visible", true, "bool");
   CONFIRMPANELContainer.setProp("Enabled", "Enabled", true, "boolean");
   CONFIRMPANELContainer.setProp("Class", "Class", "", "char");
   CONFIRMPANELContainer.setC2ShowFunction(function(UC) { UC.show(); });
   CONFIRMPANELContainer.addEventHandler("Close", this.e11mj2_client);
   this.setUserControl(CONFIRMPANELContainer);
   this.GXUITABSPANEL_TABSContainer = gx.uc.getNew(this, 17, 0, "BootstrapTabsPanel", "GXUITABSPANEL_TABSContainer", "Gxuitabspanel_tabs");
   var GXUITABSPANEL_TABSContainer = this.GXUITABSPANEL_TABSContainer;
   GXUITABSPANEL_TABSContainer.setProp("Width", "Width", "100%", "str");
   GXUITABSPANEL_TABSContainer.setProp("Height", "Height", "100", "str");
   GXUITABSPANEL_TABSContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   GXUITABSPANEL_TABSContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   GXUITABSPANEL_TABSContainer.setProp("AutoScroll", "Autoscroll", true, "bool");
   GXUITABSPANEL_TABSContainer.setProp("Cls", "Cls", "GXUI-DVelop-Tabs", "str");
   GXUITABSPANEL_TABSContainer.setProp("ActiveTabId", "Activetabid", "", "char");
   GXUITABSPANEL_TABSContainer.setProp("DesignTimeTabs", "Designtimetabs", "[{\"id\":\"TabSolicitacao\"},{\"id\":\"TabRequisitos\"},{\"id\":\"TabRascunho\"}]", "str");
   GXUITABSPANEL_TABSContainer.setProp("SelectedTabIndex", "Selectedtabindex", 0, "num");
   GXUITABSPANEL_TABSContainer.setProp("Visible", "Visible", true, "bool");
   GXUITABSPANEL_TABSContainer.setProp("Enabled", "Enabled", true, "boolean");
   GXUITABSPANEL_TABSContainer.setProp("Class", "Class", "", "char");
   GXUITABSPANEL_TABSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GXUITABSPANEL_TABSContainer.addEventHandler("TabClicked", this.e31mj1_client);
   this.setUserControl(GXUITABSPANEL_TABSContainer);
   this.DVPANEL_REQUISITOSContainer = gx.uc.getNew(this, 158, 25, "BootstrapPanel", "DVPANEL_REQUISITOSContainer", "Dvpanel_requisitos");
   var DVPANEL_REQUISITOSContainer = this.DVPANEL_REQUISITOSContainer;
   DVPANEL_REQUISITOSContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_REQUISITOSContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_REQUISITOSContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_REQUISITOSContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_REQUISITOSContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_REQUISITOSContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_REQUISITOSContainer.setDynProp("Title", "Title", "", "str");
   DVPANEL_REQUISITOSContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_REQUISITOSContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_REQUISITOSContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_REQUISITOSContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_REQUISITOSContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_REQUISITOSContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_REQUISITOSContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_REQUISITOSContainer.setProp("Class", "Class", "", "char");
   DVPANEL_REQUISITOSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_REQUISITOSContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[5]={fld:"TABLETITLE",grid:0};
   GXValidFnc[11]={fld:"UNNAMEDTABLE2",grid:0};
   GXValidFnc[14]={fld:"TEXTBLOCKTITLE", format:0,grid:0};
   GXValidFnc[20]={fld:"UNNAMEDTABLE8",grid:0};
   GXValidFnc[23]={fld:"TEXTBLOCKUSUARIO_PESSOANOM", format:0,grid:0};
   GXValidFnc[25]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vUSUARIO_PESSOANOM",gxz:"ZV19Usuario_PessoaNom",gxold:"OV19Usuario_PessoaNom",gxvar:"AV19Usuario_PessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV19Usuario_PessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19Usuario_PessoaNom=Value},v2c:function(){gx.fn.setControlValue("vUSUARIO_PESSOANOM",gx.O.AV19Usuario_PessoaNom,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV19Usuario_PessoaNom=this.val()},val:function(){return gx.fn.getControlValue("vUSUARIO_PESSOANOM")},nac:gx.falseFn};
   GXValidFnc[27]={fld:"TEXTBLOCKUSUARIO_CARGOUONOM", format:0,grid:0};
   GXValidFnc[29]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vUSUARIO_CARGOUONOM",gxz:"ZV18Usuario_CargoUONom",gxold:"OV18Usuario_CargoUONom",gxvar:"AV18Usuario_CargoUONom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV18Usuario_CargoUONom=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV18Usuario_CargoUONom=Value},v2c:function(){gx.fn.setControlValue("vUSUARIO_CARGOUONOM",gx.O.AV18Usuario_CargoUONom,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV18Usuario_CargoUONom=this.val()},val:function(){return gx.fn.getControlValue("vUSUARIO_CARGOUONOM")},nac:gx.falseFn};
   GXValidFnc[32]={fld:"TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_SS", format:0,grid:0};
   GXValidFnc[34]={lvl:0,type:"int",len:8,dec:0,sign:false,pic:"ZZZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"DADOSDASS_CONTAGEMRESULTADO_SS",gxz:"ZV118GXV1",gxold:"OV118GXV1",gxvar:"GXV1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.GXV1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV118GXV1=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("DADOSDASS_CONTAGEMRESULTADO_SS",gx.O.GXV1,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("DADOSDASS_CONTAGEMRESULTADO_SS",'.')},nac:gx.falseFn};
   GXValidFnc[39]={fld:"TEXTBLOCKSERVICOGRUPO_CODIGO", format:0,grid:0};
   GXValidFnc[41]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSERVICOGRUPO_CODIGO",gxz:"ZV15ServicoGrupo_Codigo",gxold:"OV15ServicoGrupo_Codigo",gxvar:"AV15ServicoGrupo_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15ServicoGrupo_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV15ServicoGrupo_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vSERVICOGRUPO_CODIGO",gx.O.AV15ServicoGrupo_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV15ServicoGrupo_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSERVICOGRUPO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[43]={fld:"TEXTBLOCKSERVICO_CODIGO", format:0,grid:0};
   GXValidFnc[45]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Servico_codigo,isvalid:null,rgrid:[],fld:"vSERVICO_CODIGO",gxz:"ZV14Servico_Codigo",gxold:"OV14Servico_Codigo",gxvar:"AV14Servico_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV14Servico_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14Servico_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vSERVICO_CODIGO",gx.O.AV14Servico_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV14Servico_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSERVICO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[48]={fld:"TEXTBLOCKSISTEMA_CODIGO", format:0,grid:0};
   GXValidFnc[50]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSISTEMA_CODIGO",gxz:"ZV36Sistema_Codigo",gxold:"OV36Sistema_Codigo",gxvar:"AV36Sistema_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV36Sistema_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV36Sistema_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vSISTEMA_CODIGO",gx.O.AV36Sistema_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV36Sistema_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSISTEMA_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[55]={fld:"TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_DESCRICAO", format:0,grid:0};
   GXValidFnc[57]={lvl:0,type:"svchar",len:500,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"DADOSDASS_CONTAGEMRESULTADO_DESCRICAO",gxz:"ZV119GXV2",gxold:"OV119GXV2",gxvar:"GXV2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.GXV2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV119GXV2=Value},v2c:function(){gx.fn.setControlValue("DADOSDASS_CONTAGEMRESULTADO_DESCRICAO",gx.O.GXV2,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV2=this.val()},val:function(){return gx.fn.getControlValue("DADOSDASS_CONTAGEMRESULTADO_DESCRICAO")},nac:gx.falseFn};
   GXValidFnc[59]={fld:"TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_OBSERVACAO", format:0,grid:0};
   GXValidFnc[61]={lvl:0,type:"vchar",len:20000,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"DADOSDASS_CONTAGEMRESULTADO_OBSERVACAO",gxz:"ZV120GXV3",gxold:"OV120GXV3",gxvar:"GXV3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.GXV3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV120GXV3=Value},v2c:function(){gx.fn.setControlValue("DADOSDASS_CONTAGEMRESULTADO_OBSERVACAO",gx.O.GXV3,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV3=this.val()},val:function(){return gx.fn.getControlValue("DADOSDASS_CONTAGEMRESULTADO_OBSERVACAO")},nac:gx.falseFn};
   GXValidFnc[64]={fld:"TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_REFERENCIA", format:0,grid:0};
   GXValidFnc[66]={lvl:0,type:"svchar",len:500,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"DADOSDASS_CONTAGEMRESULTADO_REFERENCIA",gxz:"ZV121GXV4",gxold:"OV121GXV4",gxvar:"GXV4",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.GXV4=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV121GXV4=Value},v2c:function(){gx.fn.setControlValue("DADOSDASS_CONTAGEMRESULTADO_REFERENCIA",gx.O.GXV4,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV4=this.val()},val:function(){return gx.fn.getControlValue("DADOSDASS_CONTAGEMRESULTADO_REFERENCIA")},nac:gx.falseFn};
   GXValidFnc[69]={fld:"TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_RESTRICOES", format:0,grid:0};
   GXValidFnc[71]={lvl:0,type:"svchar",len:250,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"DADOSDASS_CONTAGEMRESULTADO_RESTRICOES",gxz:"ZV122GXV5",gxold:"OV122GXV5",gxvar:"GXV5",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.GXV5=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV122GXV5=Value},v2c:function(){gx.fn.setControlValue("DADOSDASS_CONTAGEMRESULTADO_RESTRICOES",gx.O.GXV5,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV5=this.val()},val:function(){return gx.fn.getControlValue("DADOSDASS_CONTAGEMRESULTADO_RESTRICOES")},nac:gx.falseFn};
   GXValidFnc[74]={fld:"TEXTBLOCKDADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA", format:0,grid:0};
   GXValidFnc[76]={lvl:0,type:"svchar",len:250,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA",gxz:"ZV123GXV6",gxold:"OV123GXV6",gxvar:"GXV6",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.GXV6=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV123GXV6=Value},v2c:function(){gx.fn.setControlValue("DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA",gx.O.GXV6,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV6=this.val()},val:function(){return gx.fn.getControlValue("DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA")},nac:gx.falseFn};
   GXValidFnc[79]={fld:"TEXTBLOCKDATAPREVISTA", format:0,grid:0};
   GXValidFnc[81]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Dataprevista,isvalid:null,rgrid:[],fld:"vDATAPREVISTA",gxz:"ZV83DataPrevista",gxold:"OV83DataPrevista",gxvar:"AV83DataPrevista",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[81],ip:[81],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV83DataPrevista=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV83DataPrevista=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDATAPREVISTA",gx.O.AV83DataPrevista,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV83DataPrevista=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vDATAPREVISTA")},nac:gx.falseFn};
   GXValidFnc[87]={fld:"LAYOUT_GROUPANEXOS",grid:0};
   GXValidFnc[88]={fld:"",grid:0};
   GXValidFnc[89]={fld:"UNNAMEDGROUPCONTAINERGROUPANEXOS",grid:0};
   GXValidFnc[90]={fld:"",grid:0};
   GXValidFnc[91]={fld:"",grid:0};
   GXValidFnc[92]={fld:"UNNAMEDGROUP9",grid:0};
   GXValidFnc[93]={fld:"GROUPANEXOS",grid:0};
   GXValidFnc[94]={fld:"",grid:0};
   GXValidFnc[95]={fld:"",grid:0};
   GXValidFnc[96]={fld:"TABLELINE7",grid:0};
   GXValidFnc[102]={fld:"UNNAMEDTABLE4",grid:0};
   GXValidFnc[105]={fld:"UNNAMEDTABLE5",grid:0};
   GXValidFnc[108]={fld:"TEXTBLOCKREQUISITO_AGRUPADOR", format:0,grid:0};
   GXValidFnc[110]={lvl:0,type:"svchar",len:25,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vREQUISITO_AGRUPADOR",gxz:"ZV97Requisito_Agrupador",gxold:"OV97Requisito_Agrupador",gxvar:"AV97Requisito_Agrupador",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV97Requisito_Agrupador=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV97Requisito_Agrupador=Value},v2c:function(){gx.fn.setControlValue("vREQUISITO_AGRUPADOR",gx.O.AV97Requisito_Agrupador,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV97Requisito_Agrupador=this.val()},val:function(){return gx.fn.getControlValue("vREQUISITO_AGRUPADOR")},nac:gx.falseFn};
   GXValidFnc[113]={fld:"TEXTBLOCKREQUISITO_IDENTIFICADOR", format:0,grid:0};
   GXValidFnc[115]={fld:"TABLEMERGEDREQUISITO_IDENTIFICADOR",grid:0};
   GXValidFnc[118]={lvl:0,type:"svchar",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:'e30mj1_client',rgrid:[],fld:"vREQUISITO_IDENTIFICADOR",gxz:"ZV100Requisito_Identificador",gxold:"OV100Requisito_Identificador",gxvar:"AV100Requisito_Identificador",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV100Requisito_Identificador=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV100Requisito_Identificador=Value},v2c:function(){gx.fn.setControlValue("vREQUISITO_IDENTIFICADOR",gx.O.AV100Requisito_Identificador,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV100Requisito_Identificador=this.val()},val:function(){return gx.fn.getControlValue("vREQUISITO_IDENTIFICADOR")},nac:gx.falseFn};
   GXValidFnc[120]={fld:"TEXTBLOCKINDICADORAUTOMATICO", format:0,grid:0};
   GXValidFnc[122]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vINDICADORAUTOMATICO",gxz:"ZV91IndicadorAutomatico",gxold:"OV91IndicadorAutomatico",gxvar:"AV91IndicadorAutomatico",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV91IndicadorAutomatico=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV91IndicadorAutomatico=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vINDICADORAUTOMATICO",gx.O.AV91IndicadorAutomatico,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV91IndicadorAutomatico=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vINDICADORAUTOMATICO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 122 , function() {
   });
   GXValidFnc[124]={fld:"INDICADORAUTOMATICO_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[126]={fld:"TEXTBLOCKREQUISITO_TIPOREQCOD", format:0,grid:0};
   GXValidFnc[128]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vREQUISITO_TIPOREQCOD",gxz:"ZV115Requisito_TipoReqCod",gxold:"OV115Requisito_TipoReqCod",gxvar:"AV115Requisito_TipoReqCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV115Requisito_TipoReqCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV115Requisito_TipoReqCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vREQUISITO_TIPOREQCOD",gx.O.AV115Requisito_TipoReqCod)},c2v:function(){if(this.val()!==undefined)gx.O.AV115Requisito_TipoReqCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vREQUISITO_TIPOREQCOD",'.')},nac:gx.falseFn};
   GXValidFnc[131]={fld:"TEXTBLOCKREQUISITO_TITULO", format:0,grid:0};
   GXValidFnc[133]={lvl:0,type:"svchar",len:250,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:'e32mj1_client',rgrid:[],fld:"vREQUISITO_TITULO",gxz:"ZV101Requisito_Titulo",gxold:"OV101Requisito_Titulo",gxvar:"AV101Requisito_Titulo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV101Requisito_Titulo=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV101Requisito_Titulo=Value},v2c:function(){gx.fn.setControlValue("vREQUISITO_TITULO",gx.O.AV101Requisito_Titulo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV101Requisito_Titulo=this.val()},val:function(){return gx.fn.getControlValue("vREQUISITO_TITULO")},nac:gx.falseFn};
   GXValidFnc[135]={fld:"TEXTBLOCKREQUISITO_DESCRICAO", format:0,grid:0};
   GXValidFnc[137]={lvl:0,type:"vchar",len:500,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vREQUISITO_DESCRICAO",gxz:"ZV99Requisito_Descricao",gxold:"OV99Requisito_Descricao",gxvar:"AV99Requisito_Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV99Requisito_Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV99Requisito_Descricao=Value},v2c:function(){gx.fn.setControlValue("vREQUISITO_DESCRICAO",gx.O.AV99Requisito_Descricao,2)},c2v:function(){if(this.val()!==undefined)gx.O.AV99Requisito_Descricao=this.val()},val:function(){return gx.fn.getControlValue("vREQUISITO_DESCRICAO")},nac:gx.falseFn};
   GXValidFnc[140]={fld:"TEXTBLOCKREQUISITO_PRIORIDADE", format:0,grid:0};
   GXValidFnc[142]={lvl:0,type:"int",len:2,dec:0,sign:false,pic:"Z9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Requisito_prioridade,isvalid:null,rgrid:[],fld:"vREQUISITO_PRIORIDADE",gxz:"ZV102Requisito_Prioridade",gxold:"OV102Requisito_Prioridade",gxvar:"AV102Requisito_Prioridade",ucs:[],op:[142],ip:[142],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV102Requisito_Prioridade=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV102Requisito_Prioridade=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vREQUISITO_PRIORIDADE",gx.O.AV102Requisito_Prioridade)},c2v:function(){if(this.val()!==undefined)gx.O.AV102Requisito_Prioridade=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vREQUISITO_PRIORIDADE",'.')},nac:gx.falseFn};
   GXValidFnc[144]={fld:"TEXTBLOCKREQUISITO_STATUS", format:0,grid:0};
   GXValidFnc[146]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Requisito_status,isvalid:null,rgrid:[],fld:"vREQUISITO_STATUS",gxz:"ZV103Requisito_Status",gxold:"OV103Requisito_Status",gxvar:"AV103Requisito_Status",ucs:[],op:[146],ip:[146],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV103Requisito_Status=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV103Requisito_Status=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vREQUISITO_STATUS",gx.O.AV103Requisito_Status)},c2v:function(){if(this.val()!==undefined)gx.O.AV103Requisito_Status=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vREQUISITO_STATUS",'.')},nac:gx.falseFn};
   GXValidFnc[149]={fld:"UNNAMEDTABLE6",grid:0};
   GXValidFnc[155]={fld:"UNNAMEDTABLE7",grid:0};
   GXValidFnc[160]={fld:"REQUISITOS",grid:0};
   GXValidFnc[164]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"vBTNALTERARREQNEG",gxz:"ZV62btnAlterarReqNeg",gxold:"OV62btnAlterarReqNeg",gxvar:"AV62btnAlterarReqNeg",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV62btnAlterarReqNeg=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV62btnAlterarReqNeg=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vBTNALTERARREQNEG",row || gx.fn.currentGridRowImpl(163),gx.O.AV62btnAlterarReqNeg,gx.O.AV133Btnalterarreqneg_GXI)},c2v:function(){gx.O.AV133Btnalterarreqneg_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV62btnAlterarReqNeg=this.val()},val:function(row){return gx.fn.getGridControlValue("vBTNALTERARREQNEG",row || gx.fn.currentGridRowImpl(163))},val_GXI:function(row){return gx.fn.getGridControlValue("vBTNALTERARREQNEG_GXI",row || gx.fn.currentGridRowImpl(163))}, gxvar_GXI:'AV133Btnalterarreqneg_GXI',nac:gx.falseFn};
   GXValidFnc[165]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"vBTNEXCLUIRREQNEG",gxz:"ZV44btnExcluirReqNeg",gxold:"OV44btnExcluirReqNeg",gxvar:"AV44btnExcluirReqNeg",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV44btnExcluirReqNeg=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV44btnExcluirReqNeg=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vBTNEXCLUIRREQNEG",row || gx.fn.currentGridRowImpl(163),gx.O.AV44btnExcluirReqNeg,gx.O.AV134Btnexcluirreqneg_GXI)},c2v:function(){gx.O.AV134Btnexcluirreqneg_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV44btnExcluirReqNeg=this.val()},val:function(row){return gx.fn.getGridControlValue("vBTNEXCLUIRREQNEG",row || gx.fn.currentGridRowImpl(163))},val_GXI:function(row){return gx.fn.getGridControlValue("vBTNEXCLUIRREQNEG_GXI",row || gx.fn.currentGridRowImpl(163))}, gxvar_GXI:'AV134Btnexcluirreqneg_GXI',nac:gx.falseFn};
   GXValidFnc[166]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"SDT_REQUISITOS__REQUISITO_CODIGO",gxz:"ZV125GXV14M",gxold:"OV125GXV14M",gxvar:"GXV14M",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.GXV14M=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV125GXV14M=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("SDT_REQUISITOS__REQUISITO_CODIGO",row || gx.fn.currentGridRowImpl(163),gx.O.GXV14M,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV14M=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SDT_REQUISITOS__REQUISITO_CODIGO",row || gx.fn.currentGridRowImpl(163),'.')},nac:gx.falseFn};
   GXValidFnc[167]={lvl:2,type:"svchar",len:25,dec:0,sign:false,ro:0,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"vAGRUPADOR",gxz:"ZV64Agrupador",gxold:"OV64Agrupador",gxvar:"AV64Agrupador",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.AV64Agrupador=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV64Agrupador=Value},v2c:function(row){gx.fn.setGridControlValue("vAGRUPADOR",row || gx.fn.currentGridRowImpl(163),gx.O.AV64Agrupador,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV64Agrupador=this.val()},val:function(row){return gx.fn.getGridControlValue("vAGRUPADOR",row || gx.fn.currentGridRowImpl(163))},nac:gx.falseFn};
   GXValidFnc[168]={lvl:2,type:"svchar",len:15,dec:0,sign:false,ro:0,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"SDT_REQUISITOS__REQUISITO_IDENTIFICADOR",gxz:"ZV126GXV14O",gxold:"OV126GXV14O",gxvar:"GXV14O",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.GXV14O=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV126GXV14O=Value},v2c:function(row){gx.fn.setGridControlValue("SDT_REQUISITOS__REQUISITO_IDENTIFICADOR",row || gx.fn.currentGridRowImpl(163),gx.O.GXV14O,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV14O=this.val()},val:function(row){return gx.fn.getGridControlValue("SDT_REQUISITOS__REQUISITO_IDENTIFICADOR",row || gx.fn.currentGridRowImpl(163))},nac:gx.falseFn};
   GXValidFnc[169]={lvl:2,type:"svchar",len:250,dec:0,sign:false,ro:0,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"SDT_REQUISITOS__REQUISITO_TITULO",gxz:"ZV127GXV14P",gxold:"OV127GXV14P",gxvar:"GXV14P",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.GXV14P=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV127GXV14P=Value},v2c:function(row){gx.fn.setGridControlValue("SDT_REQUISITOS__REQUISITO_TITULO",row || gx.fn.currentGridRowImpl(163),gx.O.GXV14P,0)},c2v:function(){if(this.val()!==undefined)gx.O.GXV14P=this.val()},val:function(row){return gx.fn.getGridControlValue("SDT_REQUISITOS__REQUISITO_TITULO",row || gx.fn.currentGridRowImpl(163))},nac:gx.falseFn};
   GXValidFnc[170]={lvl:2,type:"vchar",len:500,dec:0,sign:false,ro:0,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"SDT_REQUISITOS__REQUISITO_DESCRICAO",gxz:"ZV128GXV14Q",gxold:"OV128GXV14Q",gxvar:"GXV14Q",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.GXV14Q=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV128GXV14Q=Value},v2c:function(row){gx.fn.setGridControlValue("SDT_REQUISITOS__REQUISITO_DESCRICAO",row || gx.fn.currentGridRowImpl(163),gx.O.GXV14Q,2)},c2v:function(){if(this.val()!==undefined)gx.O.GXV14Q=this.val()},val:function(row){return gx.fn.getGridControlValue("SDT_REQUISITOS__REQUISITO_DESCRICAO",row || gx.fn.currentGridRowImpl(163))},nac:gx.falseFn};
   GXValidFnc[171]={lvl:2,type:"int",len:2,dec:0,sign:false,pic:"Z9",ro:0,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"SDT_REQUISITOS__REQUISITO_PRIORIDADE",gxz:"ZV129GXV14R",gxold:"OV129GXV14R",gxvar:"GXV14R",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.GXV14R=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV129GXV14R=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("SDT_REQUISITOS__REQUISITO_PRIORIDADE",row || gx.fn.currentGridRowImpl(163),gx.O.GXV14R)},c2v:function(){if(this.val()!==undefined)gx.O.GXV14R=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SDT_REQUISITOS__REQUISITO_PRIORIDADE",row || gx.fn.currentGridRowImpl(163),'.')},nac:gx.falseFn};
   GXValidFnc[172]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,isacc:0,grid:163,gxgrid:this.Gridsdt_requisitosContainer,fnc:null,isvalid:null,rgrid:[],fld:"SDT_REQUISITOS__REQUISITO_STATUS",gxz:"ZV130GXV14S",gxold:"OV130GXV14S",gxvar:"GXV14S",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.GXV14S=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV130GXV14S=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("SDT_REQUISITOS__REQUISITO_STATUS",row || gx.fn.currentGridRowImpl(163),gx.O.GXV14S)},c2v:function(){if(this.val()!==undefined)gx.O.GXV14S=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SDT_REQUISITOS__REQUISITO_STATUS",row || gx.fn.currentGridRowImpl(163),'.')},nac:gx.falseFn};
   GXValidFnc[175]={fld:"UNNAMEDTABLE3",grid:0};
   GXValidFnc[179]={lvl:3,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"vBTNCONTINUAR",gxz:"ZV75btnContinuar",gxold:"OV75btnContinuar",gxvar:"AV75btnContinuar",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV75btnContinuar=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV75btnContinuar=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vBTNCONTINUAR",row || gx.fn.currentGridRowImpl(178),gx.O.AV75btnContinuar,gx.O.AV131Btncontinuar_GXI)},c2v:function(){gx.O.AV131Btncontinuar_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV75btnContinuar=this.val()},val:function(row){return gx.fn.getGridControlValue("vBTNCONTINUAR",row || gx.fn.currentGridRowImpl(178))},val_GXI:function(row){return gx.fn.getGridControlValue("vBTNCONTINUAR_GXI",row || gx.fn.currentGridRowImpl(178))}, gxvar_GXI:'AV131Btncontinuar_GXI',nac:gx.falseFn};
   GXValidFnc[180]={lvl:3,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"vBTNCANCELAR",gxz:"ZV76btnCancelar",gxold:"OV76btnCancelar",gxvar:"AV76btnCancelar",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV76btnCancelar=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV76btnCancelar=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vBTNCANCELAR",row || gx.fn.currentGridRowImpl(178),gx.O.AV76btnCancelar,gx.O.AV132Btncancelar_GXI)},c2v:function(){gx.O.AV132Btncancelar_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV76btnCancelar=this.val()},val:function(row){return gx.fn.getGridControlValue("vBTNCANCELAR",row || gx.fn.currentGridRowImpl(178))},val_GXI:function(row){return gx.fn.getGridControlValue("vBTNCANCELAR_GXI",row || gx.fn.currentGridRowImpl(178))}, gxvar_GXI:'AV132Btncancelar_GXI',nac:gx.falseFn};
   GXValidFnc[181]={lvl:3,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:this.Valid_Contagemresultado_codigo,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_CODIGO",gxz:"Z456ContagemResultado_Codigo",gxold:"O456ContagemResultado_Codigo",gxvar:"A456ContagemResultado_Codigo",ucs:[],op:[],ip:[181],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z456ContagemResultado_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_CODIGO",row || gx.fn.currentGridRowImpl(178),gx.O.A456ContagemResultado_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A456ContagemResultado_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADO_CODIGO",row || gx.fn.currentGridRowImpl(178),'.')},nac:gx.falseFn};
   GXValidFnc[182]={lvl:3,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DATADMN",gxz:"Z471ContagemResultado_DataDmn",gxold:"O471ContagemResultado_DataDmn",gxvar:"A471ContagemResultado_DataDmn",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A471ContagemResultado_DataDmn=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z471ContagemResultado_DataDmn=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DATADMN",row || gx.fn.currentGridRowImpl(178),gx.O.A471ContagemResultado_DataDmn,0)},c2v:function(){if(this.val()!==undefined)gx.O.A471ContagemResultado_DataDmn=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADO_DATADMN",row || gx.fn.currentGridRowImpl(178))},nac:gx.falseFn};
   GXValidFnc[183]={lvl:3,type:"svchar",len:50,dec:0,sign:false,ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DEMANDAFM",gxz:"Z493ContagemResultado_DemandaFM",gxold:"O493ContagemResultado_DemandaFM",gxvar:"A493ContagemResultado_DemandaFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A493ContagemResultado_DemandaFM=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z493ContagemResultado_DemandaFM=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DEMANDAFM",row || gx.fn.currentGridRowImpl(178),gx.O.A493ContagemResultado_DemandaFM,0)},c2v:function(){if(this.val()!==undefined)gx.O.A493ContagemResultado_DemandaFM=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_DEMANDAFM",row || gx.fn.currentGridRowImpl(178))},nac:gx.falseFn};
   GXValidFnc[184]={lvl:3,type:"svchar",len:500,dec:0,sign:false,ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DESCRICAO",gxz:"Z494ContagemResultado_Descricao",gxold:"O494ContagemResultado_Descricao",gxvar:"A494ContagemResultado_Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A494ContagemResultado_Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z494ContagemResultado_Descricao=Value},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DESCRICAO",row || gx.fn.currentGridRowImpl(178),gx.O.A494ContagemResultado_Descricao,0)},c2v:function(){if(this.val()!==undefined)gx.O.A494ContagemResultado_Descricao=this.val()},val:function(row){return gx.fn.getGridControlValue("CONTAGEMRESULTADO_DESCRICAO",row || gx.fn.currentGridRowImpl(178))},nac:gx.falseFn};
   GXValidFnc[185]={lvl:3,type:"date",len:8,dec:0,sign:false,ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DATAENTREGA",gxz:"Z472ContagemResultado_DataEntrega",gxold:"O472ContagemResultado_DataEntrega",gxvar:"A472ContagemResultado_DataEntrega",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A472ContagemResultado_DataEntrega=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z472ContagemResultado_DataEntrega=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DATAENTREGA",row || gx.fn.currentGridRowImpl(178),gx.O.A472ContagemResultado_DataEntrega,0)},c2v:function(){if(this.val()!==undefined)gx.O.A472ContagemResultado_DataEntrega=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADO_DATAENTREGA",row || gx.fn.currentGridRowImpl(178))},nac:gx.falseFn};
   GXValidFnc[186]={lvl:3,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_DATACADASTRO",gxz:"Z1350ContagemResultado_DataCadastro",gxold:"O1350ContagemResultado_DataCadastro",gxvar:"A1350ContagemResultado_DataCadastro",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1350ContagemResultado_DataCadastro=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1350ContagemResultado_DataCadastro=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_DATACADASTRO",row || gx.fn.currentGridRowImpl(178),gx.O.A1350ContagemResultado_DataCadastro,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1350ContagemResultado_DataCadastro=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("CONTAGEMRESULTADO_DATACADASTRO",row || gx.fn.currentGridRowImpl(178))},nac:gx.falseFn};
   GXValidFnc[187]={lvl:3,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"vQTDREQUISITOS",gxz:"ZV77QtdRequisitos",gxold:"OV77QtdRequisitos",gxvar:"AV77QtdRequisitos",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV77QtdRequisitos=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV77QtdRequisitos=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("vQTDREQUISITOS",row || gx.fn.currentGridRowImpl(178),gx.O.AV77QtdRequisitos,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV77QtdRequisitos=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vQTDREQUISITOS",row || gx.fn.currentGridRowImpl(178),'.')},nac:gx.falseFn};
   GXValidFnc[188]={lvl:3,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,isacc:0,grid:178,gxgrid:this.GridrascunhoContainer,fnc:null,isvalid:null,rgrid:[],fld:"vQTDANEXOS",gxz:"ZV86QtdAnexos",gxold:"OV86QtdAnexos",gxvar:"AV86QtdAnexos",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV86QtdAnexos=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV86QtdAnexos=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("vQTDANEXOS",row || gx.fn.currentGridRowImpl(178),gx.O.AV86QtdAnexos,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV86QtdAnexos=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vQTDANEXOS",row || gx.fn.currentGridRowImpl(178),'.')},nac:gx.falseFn};
   GXValidFnc[191]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[201]={fld:"USRTABLE",grid:0};
   GXValidFnc[206]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vSERVICO_RESPONSAVEL",gxz:"ZV29Servico_Responsavel",gxold:"OV29Servico_Responsavel",gxvar:"AV29Servico_Responsavel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV29Servico_Responsavel=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV29Servico_Responsavel=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vSERVICO_RESPONSAVEL",gx.O.AV29Servico_Responsavel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV29Servico_Responsavel=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSERVICO_RESPONSAVEL",'.')},nac:gx.falseFn};
   GXValidFnc[207]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vTEMSERVICOPADRAO",gxz:"ZV35TemServicoPadrao",gxold:"OV35TemServicoPadrao",gxvar:"AV35TemServicoPadrao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV35TemServicoPadrao=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV35TemServicoPadrao=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vTEMSERVICOPADRAO",gx.O.AV35TemServicoPadrao,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV35TemServicoPadrao=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vTEMSERVICOPADRAO")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[208]={fld:"TBJAVA", format:1,grid:0};
   GXValidFnc[209]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Linhanegocio_codigo,isvalid:null,rgrid:[],fld:"vLINHANEGOCIO_CODIGO",gxz:"ZV114LinhaNegocio_Codigo",gxold:"OV114LinhaNegocio_Codigo",gxvar:"AV114LinhaNegocio_Codigo",ucs:[],op:[128],ip:[128,209],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV114LinhaNegocio_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV114LinhaNegocio_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vLINHANEGOCIO_CODIGO",gx.O.AV114LinhaNegocio_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV114LinhaNegocio_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vLINHANEGOCIO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[210]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vREQUISITOINDEX",gxz:"ZV63RequisitoIndex",gxold:"OV63RequisitoIndex",gxvar:"AV63RequisitoIndex",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV63RequisitoIndex=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV63RequisitoIndex=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vREQUISITOINDEX",gx.O.AV63RequisitoIndex,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV63RequisitoIndex=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vREQUISITOINDEX",'.')},nac:gx.falseFn};
   this.AV19Usuario_PessoaNom = "" ;
   this.ZV19Usuario_PessoaNom = "" ;
   this.OV19Usuario_PessoaNom = "" ;
   this.AV18Usuario_CargoUONom = "" ;
   this.ZV18Usuario_CargoUONom = "" ;
   this.OV18Usuario_CargoUONom = "" ;
   this.GXV1 = 0 ;
   this.ZV118GXV1 = 0 ;
   this.OV118GXV1 = 0 ;
   this.AV15ServicoGrupo_Codigo = 0 ;
   this.ZV15ServicoGrupo_Codigo = 0 ;
   this.OV15ServicoGrupo_Codigo = 0 ;
   this.AV14Servico_Codigo = 0 ;
   this.ZV14Servico_Codigo = 0 ;
   this.OV14Servico_Codigo = 0 ;
   this.AV36Sistema_Codigo = 0 ;
   this.ZV36Sistema_Codigo = 0 ;
   this.OV36Sistema_Codigo = 0 ;
   this.GXV2 = "" ;
   this.ZV119GXV2 = "" ;
   this.OV119GXV2 = "" ;
   this.GXV3 = "" ;
   this.ZV120GXV3 = "" ;
   this.OV120GXV3 = "" ;
   this.GXV4 = "" ;
   this.ZV121GXV4 = "" ;
   this.OV121GXV4 = "" ;
   this.GXV5 = "" ;
   this.ZV122GXV5 = "" ;
   this.OV122GXV5 = "" ;
   this.GXV6 = "" ;
   this.ZV123GXV6 = "" ;
   this.OV123GXV6 = "" ;
   this.AV83DataPrevista = gx.date.nullDate() ;
   this.ZV83DataPrevista = gx.date.nullDate() ;
   this.OV83DataPrevista = gx.date.nullDate() ;
   this.AV97Requisito_Agrupador = "" ;
   this.ZV97Requisito_Agrupador = "" ;
   this.OV97Requisito_Agrupador = "" ;
   this.AV100Requisito_Identificador = "" ;
   this.ZV100Requisito_Identificador = "" ;
   this.OV100Requisito_Identificador = "" ;
   this.AV91IndicadorAutomatico = false ;
   this.ZV91IndicadorAutomatico = false ;
   this.OV91IndicadorAutomatico = false ;
   this.AV115Requisito_TipoReqCod = 0 ;
   this.ZV115Requisito_TipoReqCod = 0 ;
   this.OV115Requisito_TipoReqCod = 0 ;
   this.AV101Requisito_Titulo = "" ;
   this.ZV101Requisito_Titulo = "" ;
   this.OV101Requisito_Titulo = "" ;
   this.AV99Requisito_Descricao = "" ;
   this.ZV99Requisito_Descricao = "" ;
   this.OV99Requisito_Descricao = "" ;
   this.AV102Requisito_Prioridade = 0 ;
   this.ZV102Requisito_Prioridade = 0 ;
   this.OV102Requisito_Prioridade = 0 ;
   this.AV103Requisito_Status = 0 ;
   this.ZV103Requisito_Status = 0 ;
   this.OV103Requisito_Status = 0 ;
   this.ZV62btnAlterarReqNeg = "" ;
   this.OV62btnAlterarReqNeg = "" ;
   this.ZV44btnExcluirReqNeg = "" ;
   this.OV44btnExcluirReqNeg = "" ;
   this.ZV125GXV14M = 0 ;
   this.OV125GXV14M = 0 ;
   this.ZV64Agrupador = "" ;
   this.OV64Agrupador = "" ;
   this.ZV126GXV14O = "" ;
   this.OV126GXV14O = "" ;
   this.ZV127GXV14P = "" ;
   this.OV127GXV14P = "" ;
   this.ZV128GXV14Q = "" ;
   this.OV128GXV14Q = "" ;
   this.ZV129GXV14R = 0 ;
   this.OV129GXV14R = 0 ;
   this.ZV130GXV14S = 0 ;
   this.OV130GXV14S = 0 ;
   this.ZV75btnContinuar = "" ;
   this.OV75btnContinuar = "" ;
   this.ZV76btnCancelar = "" ;
   this.OV76btnCancelar = "" ;
   this.Z456ContagemResultado_Codigo = 0 ;
   this.O456ContagemResultado_Codigo = 0 ;
   this.Z471ContagemResultado_DataDmn = gx.date.nullDate() ;
   this.O471ContagemResultado_DataDmn = gx.date.nullDate() ;
   this.Z493ContagemResultado_DemandaFM = "" ;
   this.O493ContagemResultado_DemandaFM = "" ;
   this.Z494ContagemResultado_Descricao = "" ;
   this.O494ContagemResultado_Descricao = "" ;
   this.Z472ContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.O472ContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.Z1350ContagemResultado_DataCadastro = gx.date.nullDate() ;
   this.O1350ContagemResultado_DataCadastro = gx.date.nullDate() ;
   this.ZV77QtdRequisitos = 0 ;
   this.OV77QtdRequisitos = 0 ;
   this.ZV86QtdAnexos = 0 ;
   this.OV86QtdAnexos = 0 ;
   this.AV29Servico_Responsavel = 0 ;
   this.ZV29Servico_Responsavel = 0 ;
   this.OV29Servico_Responsavel = 0 ;
   this.AV35TemServicoPadrao = false ;
   this.ZV35TemServicoPadrao = false ;
   this.OV35TemServicoPadrao = false ;
   this.AV114LinhaNegocio_Codigo = 0 ;
   this.ZV114LinhaNegocio_Codigo = 0 ;
   this.OV114LinhaNegocio_Codigo = 0 ;
   this.AV63RequisitoIndex = 0 ;
   this.ZV63RequisitoIndex = 0 ;
   this.OV63RequisitoIndex = 0 ;
   this.AV19Usuario_PessoaNom = "" ;
   this.AV18Usuario_CargoUONom = "" ;
   this.GXV1 = 0 ;
   this.AV15ServicoGrupo_Codigo = 0 ;
   this.AV14Servico_Codigo = 0 ;
   this.AV36Sistema_Codigo = 0 ;
   this.GXV2 = "" ;
   this.GXV3 = "" ;
   this.GXV4 = "" ;
   this.GXV5 = "" ;
   this.GXV6 = "" ;
   this.AV83DataPrevista = gx.date.nullDate() ;
   this.AV97Requisito_Agrupador = "" ;
   this.AV100Requisito_Identificador = "" ;
   this.AV91IndicadorAutomatico = false ;
   this.AV115Requisito_TipoReqCod = 0 ;
   this.AV101Requisito_Titulo = "" ;
   this.AV99Requisito_Descricao = "" ;
   this.AV102Requisito_Prioridade = 0 ;
   this.AV103Requisito_Status = 0 ;
   this.AV29Servico_Responsavel = 0 ;
   this.AV35TemServicoPadrao = false ;
   this.AV114LinhaNegocio_Codigo = 0 ;
   this.AV63RequisitoIndex = 0 ;
   this.AV62btnAlterarReqNeg = "" ;
   this.AV44btnExcluirReqNeg = "" ;
   this.GXV14M = 0 ;
   this.AV64Agrupador = "" ;
   this.GXV14O = "" ;
   this.GXV14P = "" ;
   this.GXV14Q = "" ;
   this.GXV14R = 0 ;
   this.GXV14S = 0 ;
   this.A2004ContagemResultadoRequisito_ReqCod = 0 ;
   this.A2003ContagemResultadoRequisito_OSCod = 0 ;
   this.A2005ContagemResultadoRequisito_Codigo = 0 ;
   this.AV75btnContinuar = "" ;
   this.AV76btnCancelar = "" ;
   this.AV77QtdRequisitos = 0 ;
   this.AV86QtdAnexos = 0 ;
   this.A60ContratanteUsuario_UsuarioCod = 0 ;
   this.A63ContratanteUsuario_ContratanteCod = 0 ;
   this.A129Sistema_Sigla = "" ;
   this.A127Sistema_Codigo = 0 ;
   this.A1530Servico_TipoHierarquia = 0 ;
   this.A157ServicoGrupo_Codigo = 0 ;
   this.A1635Servico_IsPublico = false ;
   this.A632Servico_Ativo = false ;
   this.A633Servico_UO = 0 ;
   this.A155Servico_Codigo = 0 ;
   this.A605Servico_Sigla = "" ;
   this.A158ServicoGrupo_Descricao = "" ;
   this.A589ContagemResultadoEvidencia_NomeArq = "" ;
   this.A590ContagemResultadoEvidencia_TipoArq = "" ;
   this.A586ContagemResultadoEvidencia_Codigo = 0 ;
   this.A588ContagemResultadoEvidencia_Arquivo = "" ;
   this.A1449ContagemResultadoEvidencia_Link = "" ;
   this.A591ContagemResultadoEvidencia_Data = gx.date.nullDate() ;
   this.A587ContagemResultadoEvidencia_Descricao = "" ;
   this.A1393ContagemResultadoEvidencia_RdmnCreated = gx.date.nullDate() ;
   this.A645TipoDocumento_Codigo = 0 ;
   this.A1493ContagemResultadoEvidencia_Owner = 0 ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A2001Requisito_Identificador = "" ;
   this.A1927Requisito_Titulo = "" ;
   this.A1923Requisito_Descricao = "" ;
   this.A2002Requisito_Prioridade = 0 ;
   this.A1932Requisito_Pontuacao = 0 ;
   this.A1934Requisito_Status = 0 ;
   this.A1926Requisito_Agrupador = "" ;
   this.A1551Servico_Responsavel = 0 ;
   this.A2047Servico_LinNegCod = 0 ;
   this.A1Usuario_Codigo = 0 ;
   this.A58Usuario_PessoaNom = "" ;
   this.A57Usuario_PessoaCod = 0 ;
   this.A1075Usuario_CargoUOCod = 0 ;
   this.A1076Usuario_CargoUONom = "" ;
   this.A1073Usuario_CargoCod = 0 ;
   this.A508ContagemResultado_Owner = 0 ;
   this.A484ContagemResultado_StatusDmn = "" ;
   this.A40000GXC1 = 0 ;
   this.A40001GXC2 = 0 ;
   this.A471ContagemResultado_DataDmn = gx.date.nullDate() ;
   this.A493ContagemResultado_DemandaFM = "" ;
   this.A494ContagemResultado_Descricao = "" ;
   this.A472ContagemResultado_DataEntrega = gx.date.nullDate() ;
   this.A1350ContagemResultado_DataCadastro = gx.date.nullDate() ;
   this.AV16SolicitacaoServico = {} ;
   this.AV22WWPContext = {} ;
   this.AV17Usuario_CargoUOCod = 0 ;
   this.AV106SDT_Requisitos = [ ] ;
   this.AV87Caller = "" ;
   this.AV113ContagemResultadoRequisito = {} ;
   this.AV67Codigo = 0 ;
   this.AV73StatusAnterior = "" ;
   this.AV5CheckRequiredFieldsResult = false ;
   this.AV82DadosDaSS = {} ;
   this.AV96Requisito = {} ;
   this.AV27ContratanteSemEmailSda = false ;
   this.AV93Codigos = [ ] ;
   this.AV20Usuarios = [ ] ;
   this.AV23Attachments = [ ] ;
   this.AV25Resultado = "" ;
   this.AV66Ordem = 0 ;
   this.AV104Requisito_Pontuacao = 0 ;
   this.AV78i = 0 ;
   this.AV107SDT_RequisitoItem = {} ;
   this.AV74ActiveTabId = "" ;
   this.AV141GXV14 = 0 ;
   this.Events = {"e12mj2_client": ["ENTER", true] ,"e13mj2_client": ["'DOFECHAR'", true] ,"e14mj2_client": ["'DOSALVAR'", true] ,"e15mj2_client": ["'DOBTNINCLUIRREQNEG'", true] ,"e16mj2_client": ["VSERVICOGRUPO_CODIGO.CLICK", true] ,"e17mj2_client": ["VSERVICO_CODIGO.CLICK", true] ,"e11mj2_client": ["CONFIRMPANEL.CLOSE", true] ,"e23mj2_client": ["VBTNEXCLUIRREQNEG.CLICK", true] ,"e24mj2_client": ["VBTNALTERARREQNEG.CLICK", true] ,"e27mj2_client": ["VBTNCONTINUAR.CLICK", true] ,"e18mj2_client": ["VINDICADORAUTOMATICO.CLICK", true] ,"e33mj2_client": ["CANCEL", true] ,"e30mj1_client": ["VREQUISITO_IDENTIFICADOR.ISVALID", false] ,"e28mj1_client": ["VSISTEMA_CODIGO.CLICK", false] ,"e31mj1_client": ["GXUITABSPANEL_TABS.TABCLICKED", false] ,"e29mj3_client": ["VBTNCANCELAR.CLICK", false] ,"e32mj1_client": ["VREQUISITO_TITULO.ISVALID", false]};
   this.EvtParms["REFRESH"] = [[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Linktarget")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDRASCUNHO.LOAD"] = [[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV75btnContinuar',fld:'vBTNCONTINUAR',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vBTNCONTINUAR","Tooltiptext")',ctrl:'vBTNCONTINUAR',prop:'Tooltiptext'},{av:'AV76btnCancelar',fld:'vBTNCANCELAR',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vBTNCANCELAR","Tooltiptext")',ctrl:'vBTNCANCELAR',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Link")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Link'},{av:'AV77QtdRequisitos',fld:'vQTDREQUISITOS',pic:'ZZZ9',nv:0},{av:'AV86QtdAnexos',fld:'vQTDANEXOS',pic:'ZZZ9',nv:0}]];
   this.EvtParms["GRIDSDT_REQUISITOS.LOAD"] = [[{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],[{av:'AV62btnAlterarReqNeg',fld:'vBTNALTERARREQNEG',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vBTNALTERARREQNEG","Tooltiptext")',ctrl:'vBTNALTERARREQNEG',prop:'Tooltiptext'},{av:'AV44btnExcluirReqNeg',fld:'vBTNEXCLUIRREQNEG',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vBTNEXCLUIRREQNEG","Tooltiptext")',ctrl:'vBTNEXCLUIRREQNEG',prop:'Tooltiptext'},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vAGRUPADOR","Forecolor")',ctrl:'vAGRUPADOR',prop:'Forecolor'},{av:'gx.fn.getCtrlProperty("vBTNALTERARREQNEG","Visible")',ctrl:'vBTNALTERARREQNEG',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vBTNEXCLUIRREQNEG","Visible")',ctrl:'vBTNEXCLUIRREQNEG',prop:'Visible'},{ctrl:'SDT_REQUISITOS__REQUISITO_IDENTIFICADOR',prop:'Fontstrikethru'},{ctrl:'SDT_REQUISITOS__REQUISITO_TITULO',prop:'Fontstrikethru'},{ctrl:'SDT_REQUISITOS__REQUISITO_PRIORIDADE',prop:'Fontstrikethru'},{ctrl:'SDT_REQUISITOS__REQUISITO_STATUS',prop:'Fontstrikethru'}]];
   this.EvtParms["ENTER"] = [[{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV113ContagemResultadoRequisito',fld:'vCONTAGEMRESULTADOREQUISITO',pic:'',nv:null},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2005ContagemResultadoRequisito_Codigo',fld:'CONTAGEMRESULTADOREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV27ContratanteSemEmailSda',fld:'vCONTRATANTESEMEMAILSDA',pic:'',nv:false},{ctrl:'vSERVICO_CODIGO'},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV93Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV20Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV23Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV25Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],[{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV98Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV113ContagemResultadoRequisito',fld:'vCONTAGEMRESULTADOREQUISITO',pic:'',nv:null},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV93Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV25Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'this.CONFIRMPANELContainer.ConfirmationText',ctrl:'CONFIRMPANEL',prop:'ConfirmationText'},{av:'AV20Usuarios',fld:'vUSUARIOS',pic:'',nv:null}]];
   this.EvtParms["'DOFECHAR'"] = [[],[]];
   this.EvtParms["'DOSALVAR'"] = [[{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'A2005ContagemResultadoRequisito_Codigo',fld:'CONTAGEMRESULTADOREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'this.CONFIRMPANELContainer.ConfirmationText',ctrl:'CONFIRMPANEL',prop:'ConfirmationText'},{av:'AV5CheckRequiredFieldsResult',fld:'vCHECKREQUIREDFIELDSRESULT',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV98Requisito_Codigo',fld:'vREQUISITO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV113ContagemResultadoRequisito',fld:'vCONTAGEMRESULTADOREQUISITO',pic:'',nv:null},{av:'AV96Requisito',fld:'vREQUISITO',pic:'',nv:null}]];
   this.EvtParms["'DOBTNINCLUIRREQNEG'"] = [[{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV101Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV97Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{av:'AV102Requisito_Prioridade',fld:'vREQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'AV104Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV103Requisito_Status',fld:'vREQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'AV91IndicadorAutomatico',fld:'vINDICADORAUTOMATICO',pic:'',nv:false},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV63RequisitoIndex',fld:'vREQUISITOINDEX',pic:'ZZZ9',nv:0},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{av:'this.DVPANEL_REQUISITOSContainer.Title',ctrl:'DVPANEL_REQUISITOS',prop:'Title'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{ctrl:'BTNBTNINCLUIRREQNEG',prop:'Caption'},{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV101Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV102Requisito_Prioridade',fld:'vREQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'AV104Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV103Requisito_Status',fld:'vREQUISITO_STATUS',pic:'ZZZ9',nv:0}]];
   this.EvtParms["GRIDRASCUNHO.REFRESH"] = [[],[]];
   this.EvtParms["GRIDSDT_REQUISITOS.REFRESH"] = [[],[{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}]];
   this.EvtParms["VREQUISITO_IDENTIFICADOR.ISVALID"] = [[{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''}],[{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null}]];
   this.EvtParms["VSERVICOGRUPO_CODIGO.CLICK"] = [[{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["VSERVICO_CODIGO.CLICK"] = [[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],[{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{ctrl:'vSERVICO_CODIGO'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNENTER',prop:'Enabled'}]];
   this.EvtParms["VSISTEMA_CODIGO.CLICK"] = [[{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],[{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNENTER',prop:'Enabled'}]];
   this.EvtParms["CONFIRMPANEL.CLOSE"] = [[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],[]];
   this.EvtParms["VBTNEXCLUIRREQNEG.CLICK"] = [[{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],[{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0}]];
   this.EvtParms["VBTNALTERARREQNEG.CLICK"] = [[{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],[{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'AV101Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''},{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV97Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''},{av:'AV102Requisito_Prioridade',fld:'vREQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'AV103Requisito_Status',fld:'vREQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'AV104Requisito_Pontuacao',fld:'vREQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{ctrl:'BTNBTNINCLUIRREQNEG',prop:'Caption'}]];
   this.EvtParms["GXUITABSPANEL_TABS.TABCLICKED"] = [[{av:'this.GXUITABSPANEL_TABSContainer.ActiveTabId',ctrl:'GXUITABSPANEL_TABS',prop:'ActiveTabId'},{av:'AV97Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("TABLEACTIONS","Visible")',ctrl:'TABLEACTIONS',prop:'Visible'}]];
   this.EvtParms["VBTNCONTINUAR.CLICK"] = [[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1551Servico_Responsavel',fld:'SERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'A2003ContagemResultadoRequisito_OSCod',fld:'CONTAGEMRESULTADOREQUISITO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'A1926Requisito_Agrupador',fld:'REQUISITO_AGRUPADOR',pic:'',nv:''},{av:'A2004ContagemResultadoRequisito_ReqCod',fld:'CONTAGEMRESULTADOREQUISITO_REQCOD',pic:'ZZZZZ9',nv:0},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'A2001Requisito_Identificador',fld:'REQUISITO_IDENTIFICADOR',pic:'',nv:''},{av:'A1927Requisito_Titulo',fld:'REQUISITO_TITULO',pic:'',nv:''},{av:'A1923Requisito_Descricao',fld:'REQUISITO_DESCRICAO',pic:'',nv:''},{av:'A2002Requisito_Prioridade',fld:'REQUISITO_PRIORIDADE',pic:'Z9',nv:0},{av:'A1932Requisito_Pontuacao',fld:'REQUISITO_PONTUACAO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1934Requisito_Status',fld:'REQUISITO_STATUS',pic:'ZZZ9',nv:0},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1449ContagemResultadoEvidencia_Link',fld:'CONTAGEMRESULTADOEVIDENCIA_LINK',pic:'',nv:''},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A591ContagemResultadoEvidencia_Data',fld:'CONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A1393ContagemResultadoEvidencia_RdmnCreated',fld:'CONTAGEMRESULTADOEVIDENCIA_RDMNCREATED',pic:'99/99/99 99:99',nv:''},{av:'A645TipoDocumento_Codigo',fld:'TIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''}],[{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV29Servico_Responsavel',fld:'vSERVICO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV97Requisito_Agrupador',fld:'vREQUISITO_AGRUPADOR',pic:'',nv:''}]];
   this.EvtParms["VBTNCANCELAR.CLICK"] = [[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Linktarget")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''}],[]];
   this.EvtParms["VINDICADORAUTOMATICO.CLICK"] = [[{av:'AV91IndicadorAutomatico',fld:'vINDICADORAUTOMATICO',pic:'',nv:false},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0}],[{av:'gx.fn.getCtrlProperty("vREQUISITO_IDENTIFICADOR","Enabled")',ctrl:'vREQUISITO_IDENTIFICADOR',prop:'Enabled'},{av:'AV107SDT_RequisitoItem',fld:'vSDT_REQUISITOITEM',pic:'',nv:null},{av:'AV78i',fld:'vI',pic:'ZZZ9',nv:0},{av:'AV100Requisito_Identificador',fld:'vREQUISITO_IDENTIFICADOR',pic:'',nv:''}]];
   this.EvtParms["VREQUISITO_TITULO.ISVALID"] = [[{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''},{av:'AV101Requisito_Titulo',fld:'vREQUISITO_TITULO',pic:'',nv:''}],[{av:'AV99Requisito_Descricao',fld:'vREQUISITO_DESCRICAO',pic:'',nv:''}]];
   this.EvtParms["GRIDSDT_REQUISITOS_FIRSTPAGE"] = [[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDSDT_REQUISITOS_PREVPAGE"] = [[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDSDT_REQUISITOS_NEXTPAGE"] = [[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDSDT_REQUISITOS_LASTPAGE"] = [[{av:'GRIDSDT_REQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDSDT_REQUISITOS_nEOF',nv:0},{av:'subGridsdt_requisitos_Rows',nv:0},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDRASCUNHO_FIRSTPAGE"] = [[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Linktarget")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDRASCUNHO_PREVPAGE"] = [[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Linktarget")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDRASCUNHO_NEXTPAGE"] = [[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Linktarget")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRIDRASCUNHO_LASTPAGE"] = [[{av:'GRIDRASCUNHO_nFirstRecordOnPage',nv:0},{av:'GRIDRASCUNHO_nEOF',nv:0},{av:'subGridrascunho_Rows',nv:0},{av:'gx.fn.getCtrlProperty("CONTAGEMRESULTADO_DATADMN","Linktarget")',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Linktarget'},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A1075Usuario_CargoUOCod',fld:'USUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1076Usuario_CargoUONom',fld:'USUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A158ServicoGrupo_Descricao',fld:'SERVICOGRUPO_DESCRICAO',pic:'',nv:''},{av:'A2047Servico_LinNegCod',fld:'SERVICO_LINNEGCOD',pic:'ZZZZZ9',nv:0},{av:'A632Servico_Ativo',fld:'SERVICO_ATIVO',pic:'',nv:false},{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A129Sistema_Sigla',fld:'SISTEMA_SIGLA',pic:'@!',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'A1530Servico_TipoHierarquia',fld:'SERVICO_TIPOHIERARQUIA',pic:'ZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV22WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV87Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNENTER',prop:'Caption'},{ctrl:'BTNENTER',prop:'Tooltiptext'},{ctrl:'BTNSALVAR',prop:'Visible'},{av:'gx.fn.getCtrlProperty("TEXTBLOCKTITLE","Visible")',ctrl:'TEXTBLOCKTITLE',prop:'Visible'},{ctrl:'FORM',prop:'Caption'},{av:'AV67Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV73StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV83DataPrevista',fld:'vDATAPREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV19Usuario_PessoaNom',fld:'vUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV17Usuario_CargoUOCod',fld:'vUSUARIO_CARGOUOCOD',pic:'ZZZZZ9',nv:0},{av:'AV18Usuario_CargoUONom',fld:'vUSUARIO_CARGOUONOM',pic:'@!',nv:''},{av:'AV16SolicitacaoServico',fld:'vSOLICITACAOSERVICO',pic:'',nv:null},{av:'AV35TemServicoPadrao',fld:'vTEMSERVICOPADRAO',pic:'',nv:false},{av:'AV82DadosDaSS',fld:'vDADOSDASS',pic:'',nv:null},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV114LinhaNegocio_Codigo',fld:'vLINHANEGOCIO_CODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_PESSOANOM","Enabled")',ctrl:'vUSUARIO_PESSOANOM',prop:'Enabled'},{av:'gx.fn.getCtrlProperty("vUSUARIO_CARGOUONOM","Enabled")',ctrl:'vUSUARIO_CARGOUONOM',prop:'Enabled'},{av:'AV106SDT_Requisitos',fld:'vSDT_REQUISITOS',grid:163,pic:'',nv:null},{av:'AV64Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV66Ordem',fld:'vORDEM',pic:'ZZZ9',nv:0},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_RESTRICOES',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_PRIORIDADEPREVISTA',prop:'Visible'},{av:'gx.fn.getCtrlProperty("vDATAPREVISTA","Visible")',ctrl:'vDATAPREVISTA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_REFERENCIA',prop:'Visible'},{ctrl:'DADOSDASS_CONTAGEMRESULTADO_SS',prop:'Visible'},{ctrl:'vSERVICOGRUPO_CODIGO'},{ctrl:'vSERVICO_CODIGO'},{av:'gx.fn.getCtrlProperty("TBJAVA","Caption")',ctrl:'TBJAVA',prop:'Caption'},{av:'AV36Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EnterCtrl = ["BTNENTER"];
   this.setVCMap("AV22WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("A155Servico_Codigo", "SERVICO_CODIGO", 0, "int");
   this.setVCMap("A1551Servico_Responsavel", "SERVICO_RESPONSAVEL", 0, "int");
   this.setVCMap("A1Usuario_Codigo", "USUARIO_CODIGO", 0, "int");
   this.setVCMap("A58Usuario_PessoaNom", "USUARIO_PESSOANOM", 0, "char");
   this.setVCMap("A1075Usuario_CargoUOCod", "USUARIO_CARGOUOCOD", 0, "int");
   this.setVCMap("A1076Usuario_CargoUONom", "USUARIO_CARGOUONOM", 0, "char");
   this.setVCMap("AV16SolicitacaoServico", "vSOLICITACAOSERVICO", 0, "SolicitacaoServico");
   this.setVCMap("A157ServicoGrupo_Codigo", "SERVICOGRUPO_CODIGO", 0, "int");
   this.setVCMap("A605Servico_Sigla", "SERVICO_SIGLA", 0, "char");
   this.setVCMap("A158ServicoGrupo_Descricao", "SERVICOGRUPO_DESCRICAO", 0, "svchar");
   this.setVCMap("A2047Servico_LinNegCod", "SERVICO_LINNEGCOD", 0, "int");
   this.setVCMap("A632Servico_Ativo", "SERVICO_ATIVO", 0, "boolean");
   this.setVCMap("A1635Servico_IsPublico", "SERVICO_ISPUBLICO", 0, "boolean");
   this.setVCMap("A63ContratanteUsuario_ContratanteCod", "CONTRATANTEUSUARIO_CONTRATANTECOD", 0, "int");
   this.setVCMap("A60ContratanteUsuario_UsuarioCod", "CONTRATANTEUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("A127Sistema_Codigo", "SISTEMA_CODIGO", 0, "int");
   this.setVCMap("A129Sistema_Sigla", "SISTEMA_SIGLA", 0, "char");
   this.setVCMap("A633Servico_UO", "SERVICO_UO", 0, "int");
   this.setVCMap("AV17Usuario_CargoUOCod", "vUSUARIO_CARGOUOCOD", 0, "int");
   this.setVCMap("A1530Servico_TipoHierarquia", "SERVICO_TIPOHIERARQUIA", 0, "int");
   this.setVCMap("AV106SDT_Requisitos", "vSDT_REQUISITOS", 0, "CollSDT_Requisitos.SDT_RequisitosItem");
   this.setVCMap("AV87Caller", "vCALLER", 0, "char");
   this.setVCMap("AV113ContagemResultadoRequisito", "vCONTAGEMRESULTADOREQUISITO", 0, "ContagemResultadoRequisito");
   this.setVCMap("AV67Codigo", "vCODIGO", 0, "int");
   this.setVCMap("AV73StatusAnterior", "vSTATUSANTERIOR", 0, "char");
   this.setVCMap("AV5CheckRequiredFieldsResult", "vCHECKREQUIREDFIELDSRESULT", 0, "boolean");
   this.setVCMap("AV82DadosDaSS", "vDADOSDASS", 0, "SolicitacaoServico");
   this.setVCMap("AV96Requisito", "vREQUISITO", 0, "Requisito");
   this.setVCMap("A2003ContagemResultadoRequisito_OSCod", "CONTAGEMRESULTADOREQUISITO_OSCOD", 0, "int");
   this.setVCMap("A2004ContagemResultadoRequisito_ReqCod", "CONTAGEMRESULTADOREQUISITO_REQCOD", 0, "int");
   this.setVCMap("A2005ContagemResultadoRequisito_Codigo", "CONTAGEMRESULTADOREQUISITO_CODIGO", 0, "int");
   this.setVCMap("AV27ContratanteSemEmailSda", "vCONTRATANTESEMEMAILSDA", 0, "boolean");
   this.setVCMap("AV93Codigos", "vCODIGOS", 0, "Collint");
   this.setVCMap("AV20Usuarios", "vUSUARIOS", 0, "Collint");
   this.setVCMap("AV23Attachments", "vATTACHMENTS", 0, "Collvchar");
   this.setVCMap("AV25Resultado", "vRESULTADO", 0, "char");
   this.setVCMap("AV66Ordem", "vORDEM", 0, "int");
   this.setVCMap("AV104Requisito_Pontuacao", "vREQUISITO_PONTUACAO", 0, "decimal");
   this.setVCMap("AV78i", "vI", 0, "int");
   this.setVCMap("A1926Requisito_Agrupador", "REQUISITO_AGRUPADOR", 0, "svchar");
   this.setVCMap("AV107SDT_RequisitoItem", "vSDT_REQUISITOITEM", 0, "SDT_Requisitos.SDT_RequisitosItem");
   this.setVCMap("A2001Requisito_Identificador", "REQUISITO_IDENTIFICADOR", 0, "svchar");
   this.setVCMap("A1927Requisito_Titulo", "REQUISITO_TITULO", 0, "svchar");
   this.setVCMap("A1923Requisito_Descricao", "REQUISITO_DESCRICAO", 0, "vchar");
   this.setVCMap("A2002Requisito_Prioridade", "REQUISITO_PRIORIDADE", 0, "int");
   this.setVCMap("A1932Requisito_Pontuacao", "REQUISITO_PONTUACAO", 0, "decimal");
   this.setVCMap("A1934Requisito_Status", "REQUISITO_STATUS", 0, "int");
   this.setVCMap("A586ContagemResultadoEvidencia_Codigo", "CONTAGEMRESULTADOEVIDENCIA_CODIGO", 0, "int");
   this.setVCMap("A588ContagemResultadoEvidencia_Arquivo", "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO", 0, "bitstr");
   this.setVCMap("A1449ContagemResultadoEvidencia_Link", "CONTAGEMRESULTADOEVIDENCIA_LINK", 0, "vchar");
   this.setVCMap("A589ContagemResultadoEvidencia_NomeArq", "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", 0, "char");
   this.setVCMap("A590ContagemResultadoEvidencia_TipoArq", "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ", 0, "char");
   this.setVCMap("A591ContagemResultadoEvidencia_Data", "CONTAGEMRESULTADOEVIDENCIA_DATA", 0, "dtime");
   this.setVCMap("A587ContagemResultadoEvidencia_Descricao", "CONTAGEMRESULTADOEVIDENCIA_DESCRICAO", 0, "vchar");
   this.setVCMap("A1393ContagemResultadoEvidencia_RdmnCreated", "CONTAGEMRESULTADOEVIDENCIA_RDMNCREATED", 0, "dtime");
   this.setVCMap("A645TipoDocumento_Codigo", "TIPODOCUMENTO_CODIGO", 0, "int");
   this.setVCMap("A1493ContagemResultadoEvidencia_Owner", "CONTAGEMRESULTADOEVIDENCIA_OWNER", 0, "int");
   this.setVCMap("AV22WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV74ActiveTabId", "vACTIVETABID", 0, "char");
   this.setVCMap("AV106SDT_Requisitos", "vSDT_REQUISITOS", 0, "CollSDT_Requisitos.SDT_RequisitosItem");
   this.setVCMap("AV141GXV14", "vGXV14", 0, "int");
   this.setVCMap("A1Usuario_Codigo", "USUARIO_CODIGO", 0, "int");
   this.setVCMap("A58Usuario_PessoaNom", "USUARIO_PESSOANOM", 0, "char");
   this.setVCMap("A1075Usuario_CargoUOCod", "USUARIO_CARGOUOCOD", 0, "int");
   this.setVCMap("A1076Usuario_CargoUONom", "USUARIO_CARGOUONOM", 0, "char");
   this.setVCMap("AV16SolicitacaoServico", "vSOLICITACAOSERVICO", 0, "SolicitacaoServico");
   this.setVCMap("A157ServicoGrupo_Codigo", "SERVICOGRUPO_CODIGO", 0, "int");
   this.setVCMap("A605Servico_Sigla", "SERVICO_SIGLA", 0, "char");
   this.setVCMap("A158ServicoGrupo_Descricao", "SERVICOGRUPO_DESCRICAO", 0, "svchar");
   this.setVCMap("A2047Servico_LinNegCod", "SERVICO_LINNEGCOD", 0, "int");
   this.setVCMap("A632Servico_Ativo", "SERVICO_ATIVO", 0, "boolean");
   this.setVCMap("A1635Servico_IsPublico", "SERVICO_ISPUBLICO", 0, "boolean");
   this.setVCMap("A63ContratanteUsuario_ContratanteCod", "CONTRATANTEUSUARIO_CONTRATANTECOD", 0, "int");
   this.setVCMap("A60ContratanteUsuario_UsuarioCod", "CONTRATANTEUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("A127Sistema_Codigo", "SISTEMA_CODIGO", 0, "int");
   this.setVCMap("A129Sistema_Sigla", "SISTEMA_SIGLA", 0, "char");
   this.setVCMap("A633Servico_UO", "SERVICO_UO", 0, "int");
   this.setVCMap("AV17Usuario_CargoUOCod", "vUSUARIO_CARGOUOCOD", 0, "int");
   this.setVCMap("A1530Servico_TipoHierarquia", "SERVICO_TIPOHIERARQUIA", 0, "int");
   this.setVCMap("AV106SDT_Requisitos", "vSDT_REQUISITOS", 0, "CollSDT_Requisitos.SDT_RequisitosItem");
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A1Usuario_Codigo"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A58Usuario_PessoaNom"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A1075Usuario_CargoUOCod"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A1076Usuario_CargoUONom"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"AV16SolicitacaoServico"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A155Servico_Codigo"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A157ServicoGrupo_Codigo"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A605Servico_Sigla"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A158ServicoGrupo_Descricao"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A2047Servico_LinNegCod"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A632Servico_Ativo"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A1635Servico_IsPublico"});
   Gridsdt_requisitosContainer.addRefreshingVar(this.GXValidFnc[207]);
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A63ContratanteUsuario_ContratanteCod"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"AV22WWPContext"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A60ContratanteUsuario_UsuarioCod"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A127Sistema_Codigo"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A129Sistema_Sigla"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A633Servico_UO"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"AV17Usuario_CargoUOCod"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"A1530Servico_TipoHierarquia"});
   Gridsdt_requisitosContainer.addRefreshingVar(this.GXValidFnc[41]);
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"AV64Agrupador", rfrProp:"Value", gxAttId:"Agrupador"});
   Gridsdt_requisitosContainer.addRefreshingVar({rfrVar:"AV106SDT_Requisitos"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"AV22WWPContext"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A471ContagemResultado_DataDmn", rfrProp:"Linktarget", gxAttId:"471"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A1Usuario_Codigo"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A58Usuario_PessoaNom"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A1075Usuario_CargoUOCod"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A1076Usuario_CargoUONom"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"AV16SolicitacaoServico"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A155Servico_Codigo"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A157ServicoGrupo_Codigo"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A605Servico_Sigla"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A158ServicoGrupo_Descricao"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A2047Servico_LinNegCod"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A632Servico_Ativo"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A1635Servico_IsPublico"});
   GridrascunhoContainer.addRefreshingVar(this.GXValidFnc[207]);
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A63ContratanteUsuario_ContratanteCod"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A60ContratanteUsuario_UsuarioCod"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A127Sistema_Codigo"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A129Sistema_Sigla"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A633Servico_UO"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"AV17Usuario_CargoUOCod"});
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A1530Servico_TipoHierarquia"});
   GridrascunhoContainer.addRefreshingVar(this.GXValidFnc[41]);
   GridrascunhoContainer.addRefreshingVar({rfrVar:"A456ContagemResultado_Codigo", rfrProp:"Value", gxAttId:"456"});
   this.addBCProperty("Dadosdass", ["ContagemResultado_SS"], this.GXValidFnc[34], "AV82DadosDaSS");
   this.addBCProperty("Dadosdass", ["ContagemResultado_Descricao"], this.GXValidFnc[57], "AV82DadosDaSS");
   this.addBCProperty("Dadosdass", ["ContagemResultado_Observacao"], this.GXValidFnc[61], "AV82DadosDaSS");
   this.addBCProperty("Dadosdass", ["ContagemResultado_Referencia"], this.GXValidFnc[66], "AV82DadosDaSS");
   this.addBCProperty("Dadosdass", ["ContagemResultado_Restricoes"], this.GXValidFnc[71], "AV82DadosDaSS");
   this.addBCProperty("Dadosdass", ["ContagemResultado_PrioridadePrevista"], this.GXValidFnc[76], "AV82DadosDaSS");
   this.addGridBCProperty("Sdt_requisitos", ["Requisito_Codigo"], this.GXValidFnc[166], "AV106SDT_Requisitos");
   this.addGridBCProperty("Sdt_requisitos", ["Requisito_Identificador"], this.GXValidFnc[168], "AV106SDT_Requisitos");
   this.addGridBCProperty("Sdt_requisitos", ["Requisito_Titulo"], this.GXValidFnc[169], "AV106SDT_Requisitos");
   this.addGridBCProperty("Sdt_requisitos", ["Requisito_Descricao"], this.GXValidFnc[170], "AV106SDT_Requisitos");
   this.addGridBCProperty("Sdt_requisitos", ["Requisito_Prioridade"], this.GXValidFnc[171], "AV106SDT_Requisitos");
   this.addGridBCProperty("Sdt_requisitos", ["Requisito_Status"], this.GXValidFnc[172], "AV106SDT_Requisitos");
   this.InitStandaloneVars( );
   this.setComponent({id: "WCWC_CONTAGEMRESULTADOEVIDENCIAS" ,GXClass: null , Prefix: "W0099" , lvl: 1 });
});
gx.createParentObj(wp_ss);
