/*
               File: SistemaVersao
        Description: Versionamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:5.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemaversao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A1866SistemaVersao_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1866SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1866SistemaVersao_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A1866SistemaVersao_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7SistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7SistemaVersao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMAVERSAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SistemaVersao_Codigo), "ZZZZZ9")));
               AV13SistemaVersao_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13SistemaVersao_SistemaCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Versionamento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtSistemaVersao_Id_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public sistemaversao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public sistemaversao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_SistemaVersao_Codigo ,
                           ref int aP2_SistemaVersao_SistemaCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7SistemaVersao_Codigo = aP1_SistemaVersao_Codigo;
         this.AV13SistemaVersao_SistemaCod = aP2_SistemaVersao_SistemaCod;
         executePrivate();
         aP2_SistemaVersao_SistemaCod=this.AV13SistemaVersao_SistemaCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4O209( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4O209e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ",", "")), ((edtSistemaVersao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSistemaVersao_Codigo_Visible, edtSistemaVersao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaVersao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4O209( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4O209( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4O209e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_51_4O209( true) ;
         }
         return  ;
      }

      protected void wb_table3_51_4O209e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4O209e( true) ;
         }
         else
         {
            wb_table1_2_4O209e( false) ;
         }
      }

      protected void wb_table3_51_4O209( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_51_4O209e( true) ;
         }
         else
         {
            wb_table3_51_4O209e( false) ;
         }
      }

      protected void wb_table2_5_4O209( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4O209( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4O209e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4O209e( true) ;
         }
         else
         {
            wb_table2_5_4O209e( false) ;
         }
      }

      protected void wb_table4_13_4O209( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_id_Internalname, "Vers�o", "", "", lblTextblocksistemaversao_id_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Id_Internalname, StringUtil.RTrim( A1860SistemaVersao_Id), StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Id_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistemaVersao_Id_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_descricao_Internalname, "Descri��o", "", "", lblTextblocksistemaversao_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistemaVersao_Descricao_Internalname, A1861SistemaVersao_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtSistemaVersao_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_evolucao_Internalname, "Evolu��o", "", "", lblTextblocksistemaversao_evolucao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_28_4O209( true) ;
         }
         return  ;
      }

      protected void wb_table5_28_4O209e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_data_Internalname, "Em", "", "", lblTextblocksistemaversao_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtSistemaVersao_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Data_Internalname, context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtSistemaVersao_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersao.htm");
            GxWebStd.gx_bitmap( context, edtSistemaVersao_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtSistemaVersao_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaVersao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4O209e( true) ;
         }
         else
         {
            wb_table4_13_4O209e( false) ;
         }
      }

      protected void wb_table5_28_4O209( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedsistemaversao_evolucao_Internalname, tblTablemergedsistemaversao_evolucao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Evolucao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1862SistemaVersao_Evolucao), 4, 0, ",", "")), ((edtSistemaVersao_Evolucao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9")) : context.localUtil.Format( (decimal)(A1862SistemaVersao_Evolucao), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Evolucao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistemaVersao_Evolucao_Enabled, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_melhoria_Internalname, "Melhoria", "", "", lblTextblocksistemaversao_melhoria_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Melhoria_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1863SistemaVersao_Melhoria), 4, 0, ",", "")), ((edtSistemaVersao_Melhoria_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9")) : context.localUtil.Format( (decimal)(A1863SistemaVersao_Melhoria), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Melhoria_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistemaVersao_Melhoria_Enabled, 0, "text", "", 80, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_correcao_Internalname, "Corre��o", "", "", lblTextblocksistemaversao_correcao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtSistemaVersao_Correcao_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1864SistemaVersao_Correcao), 8, 0, ",", "")), ((edtSistemaVersao_Correcao_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1864SistemaVersao_Correcao), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistemaVersao_Correcao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtSistemaVersao_Correcao_Enabled, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_28_4O209e( true) ;
         }
         else
         {
            wb_table5_28_4O209e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114O2 */
         E114O2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1860SistemaVersao_Id = cgiGet( edtSistemaVersao_Id_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1860SistemaVersao_Id", A1860SistemaVersao_Id);
               A1861SistemaVersao_Descricao = cgiGet( edtSistemaVersao_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1861SistemaVersao_Descricao", A1861SistemaVersao_Descricao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistemaVersao_Evolucao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistemaVersao_Evolucao_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMAVERSAO_EVOLUCAO");
                  AnyError = 1;
                  GX_FocusControl = edtSistemaVersao_Evolucao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1862SistemaVersao_Evolucao = 0;
                  n1862SistemaVersao_Evolucao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1862SistemaVersao_Evolucao), 4, 0)));
               }
               else
               {
                  A1862SistemaVersao_Evolucao = (short)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Evolucao_Internalname), ",", "."));
                  n1862SistemaVersao_Evolucao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1862SistemaVersao_Evolucao), 4, 0)));
               }
               n1862SistemaVersao_Evolucao = ((0==A1862SistemaVersao_Evolucao) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistemaVersao_Melhoria_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistemaVersao_Melhoria_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMAVERSAO_MELHORIA");
                  AnyError = 1;
                  GX_FocusControl = edtSistemaVersao_Melhoria_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1863SistemaVersao_Melhoria = 0;
                  n1863SistemaVersao_Melhoria = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(A1863SistemaVersao_Melhoria), 4, 0)));
               }
               else
               {
                  A1863SistemaVersao_Melhoria = (short)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Melhoria_Internalname), ",", "."));
                  n1863SistemaVersao_Melhoria = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(A1863SistemaVersao_Melhoria), 4, 0)));
               }
               n1863SistemaVersao_Melhoria = ((0==A1863SistemaVersao_Melhoria) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtSistemaVersao_Correcao_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtSistemaVersao_Correcao_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SISTEMAVERSAO_CORRECAO");
                  AnyError = 1;
                  GX_FocusControl = edtSistemaVersao_Correcao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1864SistemaVersao_Correcao = 0;
                  n1864SistemaVersao_Correcao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1864SistemaVersao_Correcao), 8, 0)));
               }
               else
               {
                  A1864SistemaVersao_Correcao = (int)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Correcao_Internalname), ",", "."));
                  n1864SistemaVersao_Correcao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1864SistemaVersao_Correcao), 8, 0)));
               }
               n1864SistemaVersao_Correcao = ((0==A1864SistemaVersao_Correcao) ? true : false);
               if ( context.localUtil.VCDateTime( cgiGet( edtSistemaVersao_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Versionado em"}), 1, "SISTEMAVERSAO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtSistemaVersao_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1865SistemaVersao_Data", context.localUtil.TToC( A1865SistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1865SistemaVersao_Data = context.localUtil.CToT( cgiGet( edtSistemaVersao_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1865SistemaVersao_Data", context.localUtil.TToC( A1865SistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               A1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Codigo_Internalname), ",", "."));
               n1859SistemaVersao_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
               /* Read saved values. */
               Z1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1859SistemaVersao_Codigo"), ",", "."));
               Z1860SistemaVersao_Id = cgiGet( "Z1860SistemaVersao_Id");
               Z1862SistemaVersao_Evolucao = (short)(context.localUtil.CToN( cgiGet( "Z1862SistemaVersao_Evolucao"), ",", "."));
               n1862SistemaVersao_Evolucao = ((0==A1862SistemaVersao_Evolucao) ? true : false);
               Z1863SistemaVersao_Melhoria = (short)(context.localUtil.CToN( cgiGet( "Z1863SistemaVersao_Melhoria"), ",", "."));
               n1863SistemaVersao_Melhoria = ((0==A1863SistemaVersao_Melhoria) ? true : false);
               Z1864SistemaVersao_Correcao = (int)(context.localUtil.CToN( cgiGet( "Z1864SistemaVersao_Correcao"), ",", "."));
               n1864SistemaVersao_Correcao = ((0==A1864SistemaVersao_Correcao) ? true : false);
               Z1865SistemaVersao_Data = context.localUtil.CToT( cgiGet( "Z1865SistemaVersao_Data"), 0);
               Z1866SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z1866SistemaVersao_SistemaCod"), ",", "."));
               A1866SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z1866SistemaVersao_SistemaCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1866SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "N1866SistemaVersao_SistemaCod"), ",", "."));
               AV7SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSISTEMAVERSAO_CODIGO"), ",", "."));
               AV11Insert_SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SISTEMAVERSAO_SISTEMACOD"), ",", "."));
               AV13SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vSISTEMAVERSAO_SISTEMACOD"), ",", "."));
               A1866SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "SISTEMAVERSAO_SISTEMACOD"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "SistemaVersao";
               A1859SistemaVersao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistemaVersao_Codigo_Internalname), ",", "."));
               n1859SistemaVersao_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1866SistemaVersao_SistemaCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1859SistemaVersao_Codigo != Z1859SistemaVersao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("sistemaversao:[SecurityCheckFailed value for]"+"SistemaVersao_Codigo:"+context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("sistemaversao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("sistemaversao:[SecurityCheckFailed value for]"+"SistemaVersao_SistemaCod:"+context.localUtil.Format( (decimal)(A1866SistemaVersao_SistemaCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1859SistemaVersao_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode209 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode209;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound209 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4O0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SISTEMAVERSAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtSistemaVersao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114O2 */
                           E114O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124O2 */
                           E124O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124O2 */
            E124O2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4O209( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4O209( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4O0( )
      {
         BeforeValidate4O209( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4O209( ) ;
            }
            else
            {
               CheckExtendedTable4O209( ) ;
               CloseExtendedTableCursors4O209( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4O0( )
      {
      }

      protected void E114O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "SistemaVersao_SistemaCod") == 0 )
               {
                  AV11Insert_SistemaVersao_SistemaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_SistemaVersao_SistemaCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtSistemaVersao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Codigo_Visible), 5, 0)));
      }

      protected void E124O2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwsistemaversao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV13SistemaVersao_SistemaCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4O209( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1860SistemaVersao_Id = T004O3_A1860SistemaVersao_Id[0];
               Z1862SistemaVersao_Evolucao = T004O3_A1862SistemaVersao_Evolucao[0];
               Z1863SistemaVersao_Melhoria = T004O3_A1863SistemaVersao_Melhoria[0];
               Z1864SistemaVersao_Correcao = T004O3_A1864SistemaVersao_Correcao[0];
               Z1865SistemaVersao_Data = T004O3_A1865SistemaVersao_Data[0];
               Z1866SistemaVersao_SistemaCod = T004O3_A1866SistemaVersao_SistemaCod[0];
            }
            else
            {
               Z1860SistemaVersao_Id = A1860SistemaVersao_Id;
               Z1862SistemaVersao_Evolucao = A1862SistemaVersao_Evolucao;
               Z1863SistemaVersao_Melhoria = A1863SistemaVersao_Melhoria;
               Z1864SistemaVersao_Correcao = A1864SistemaVersao_Correcao;
               Z1865SistemaVersao_Data = A1865SistemaVersao_Data;
               Z1866SistemaVersao_SistemaCod = A1866SistemaVersao_SistemaCod;
            }
         }
         if ( GX_JID == -8 )
         {
            Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
            Z1860SistemaVersao_Id = A1860SistemaVersao_Id;
            Z1861SistemaVersao_Descricao = A1861SistemaVersao_Descricao;
            Z1862SistemaVersao_Evolucao = A1862SistemaVersao_Evolucao;
            Z1863SistemaVersao_Melhoria = A1863SistemaVersao_Melhoria;
            Z1864SistemaVersao_Correcao = A1864SistemaVersao_Correcao;
            Z1865SistemaVersao_Data = A1865SistemaVersao_Data;
            Z1866SistemaVersao_SistemaCod = A1866SistemaVersao_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtSistemaVersao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Codigo_Enabled), 5, 0)));
         AV14Pgmname = "SistemaVersao";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtSistemaVersao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7SistemaVersao_Codigo) )
         {
            A1859SistemaVersao_Codigo = AV7SistemaVersao_Codigo;
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_SistemaVersao_SistemaCod) )
         {
            A1866SistemaVersao_SistemaCod = AV11Insert_SistemaVersao_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1866SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1866SistemaVersao_SistemaCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A1866SistemaVersao_SistemaCod = AV13SistemaVersao_SistemaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1866SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1866SistemaVersao_SistemaCod), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load4O209( )
      {
         /* Using cursor T004O5 */
         pr_default.execute(3, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound209 = 1;
            A1860SistemaVersao_Id = T004O5_A1860SistemaVersao_Id[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1860SistemaVersao_Id", A1860SistemaVersao_Id);
            A1861SistemaVersao_Descricao = T004O5_A1861SistemaVersao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1861SistemaVersao_Descricao", A1861SistemaVersao_Descricao);
            A1862SistemaVersao_Evolucao = T004O5_A1862SistemaVersao_Evolucao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1862SistemaVersao_Evolucao), 4, 0)));
            n1862SistemaVersao_Evolucao = T004O5_n1862SistemaVersao_Evolucao[0];
            A1863SistemaVersao_Melhoria = T004O5_A1863SistemaVersao_Melhoria[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(A1863SistemaVersao_Melhoria), 4, 0)));
            n1863SistemaVersao_Melhoria = T004O5_n1863SistemaVersao_Melhoria[0];
            A1864SistemaVersao_Correcao = T004O5_A1864SistemaVersao_Correcao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1864SistemaVersao_Correcao), 8, 0)));
            n1864SistemaVersao_Correcao = T004O5_n1864SistemaVersao_Correcao[0];
            A1865SistemaVersao_Data = T004O5_A1865SistemaVersao_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1865SistemaVersao_Data", context.localUtil.TToC( A1865SistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            A1866SistemaVersao_SistemaCod = T004O5_A1866SistemaVersao_SistemaCod[0];
            ZM4O209( -8) ;
         }
         pr_default.close(3);
         OnLoadActions4O209( ) ;
      }

      protected void OnLoadActions4O209( )
      {
      }

      protected void CheckExtendedTable4O209( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1865SistemaVersao_Data) || ( A1865SistemaVersao_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Versionado em fora do intervalo", "OutOfRange", 1, "SISTEMAVERSAO_DATA");
            AnyError = 1;
            GX_FocusControl = edtSistemaVersao_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T004O4 */
         pr_default.execute(2, new Object[] {A1866SistemaVersao_SistemaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Sistema Versao_Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors4O209( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A1866SistemaVersao_SistemaCod )
      {
         /* Using cursor T004O6 */
         pr_default.execute(4, new Object[] {A1866SistemaVersao_SistemaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Sistema Versao_Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey4O209( )
      {
         /* Using cursor T004O7 */
         pr_default.execute(5, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound209 = 1;
         }
         else
         {
            RcdFound209 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004O3 */
         pr_default.execute(1, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4O209( 8) ;
            RcdFound209 = 1;
            A1859SistemaVersao_Codigo = T004O3_A1859SistemaVersao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            n1859SistemaVersao_Codigo = T004O3_n1859SistemaVersao_Codigo[0];
            A1860SistemaVersao_Id = T004O3_A1860SistemaVersao_Id[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1860SistemaVersao_Id", A1860SistemaVersao_Id);
            A1861SistemaVersao_Descricao = T004O3_A1861SistemaVersao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1861SistemaVersao_Descricao", A1861SistemaVersao_Descricao);
            A1862SistemaVersao_Evolucao = T004O3_A1862SistemaVersao_Evolucao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1862SistemaVersao_Evolucao), 4, 0)));
            n1862SistemaVersao_Evolucao = T004O3_n1862SistemaVersao_Evolucao[0];
            A1863SistemaVersao_Melhoria = T004O3_A1863SistemaVersao_Melhoria[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(A1863SistemaVersao_Melhoria), 4, 0)));
            n1863SistemaVersao_Melhoria = T004O3_n1863SistemaVersao_Melhoria[0];
            A1864SistemaVersao_Correcao = T004O3_A1864SistemaVersao_Correcao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1864SistemaVersao_Correcao), 8, 0)));
            n1864SistemaVersao_Correcao = T004O3_n1864SistemaVersao_Correcao[0];
            A1865SistemaVersao_Data = T004O3_A1865SistemaVersao_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1865SistemaVersao_Data", context.localUtil.TToC( A1865SistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            A1866SistemaVersao_SistemaCod = T004O3_A1866SistemaVersao_SistemaCod[0];
            Z1859SistemaVersao_Codigo = A1859SistemaVersao_Codigo;
            sMode209 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4O209( ) ;
            if ( AnyError == 1 )
            {
               RcdFound209 = 0;
               InitializeNonKey4O209( ) ;
            }
            Gx_mode = sMode209;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound209 = 0;
            InitializeNonKey4O209( ) ;
            sMode209 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode209;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4O209( ) ;
         if ( RcdFound209 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound209 = 0;
         /* Using cursor T004O8 */
         pr_default.execute(6, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T004O8_A1859SistemaVersao_Codigo[0] < A1859SistemaVersao_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T004O8_A1859SistemaVersao_Codigo[0] > A1859SistemaVersao_Codigo ) ) )
            {
               A1859SistemaVersao_Codigo = T004O8_A1859SistemaVersao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
               n1859SistemaVersao_Codigo = T004O8_n1859SistemaVersao_Codigo[0];
               RcdFound209 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound209 = 0;
         /* Using cursor T004O9 */
         pr_default.execute(7, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T004O9_A1859SistemaVersao_Codigo[0] > A1859SistemaVersao_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T004O9_A1859SistemaVersao_Codigo[0] < A1859SistemaVersao_Codigo ) ) )
            {
               A1859SistemaVersao_Codigo = T004O9_A1859SistemaVersao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
               n1859SistemaVersao_Codigo = T004O9_n1859SistemaVersao_Codigo[0];
               RcdFound209 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4O209( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtSistemaVersao_Id_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4O209( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound209 == 1 )
            {
               if ( A1859SistemaVersao_Codigo != Z1859SistemaVersao_Codigo )
               {
                  A1859SistemaVersao_Codigo = Z1859SistemaVersao_Codigo;
                  n1859SistemaVersao_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SISTEMAVERSAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtSistemaVersao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtSistemaVersao_Id_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4O209( ) ;
                  GX_FocusControl = edtSistemaVersao_Id_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1859SistemaVersao_Codigo != Z1859SistemaVersao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtSistemaVersao_Id_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4O209( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SISTEMAVERSAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtSistemaVersao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtSistemaVersao_Id_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4O209( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1859SistemaVersao_Codigo != Z1859SistemaVersao_Codigo )
         {
            A1859SistemaVersao_Codigo = Z1859SistemaVersao_Codigo;
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SISTEMAVERSAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtSistemaVersao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtSistemaVersao_Id_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4O209( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004O2 */
            pr_default.execute(0, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaVersao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1860SistemaVersao_Id, T004O2_A1860SistemaVersao_Id[0]) != 0 ) || ( Z1862SistemaVersao_Evolucao != T004O2_A1862SistemaVersao_Evolucao[0] ) || ( Z1863SistemaVersao_Melhoria != T004O2_A1863SistemaVersao_Melhoria[0] ) || ( Z1864SistemaVersao_Correcao != T004O2_A1864SistemaVersao_Correcao[0] ) || ( Z1865SistemaVersao_Data != T004O2_A1865SistemaVersao_Data[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1866SistemaVersao_SistemaCod != T004O2_A1866SistemaVersao_SistemaCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z1860SistemaVersao_Id, T004O2_A1860SistemaVersao_Id[0]) != 0 )
               {
                  GXUtil.WriteLog("sistemaversao:[seudo value changed for attri]"+"SistemaVersao_Id");
                  GXUtil.WriteLogRaw("Old: ",Z1860SistemaVersao_Id);
                  GXUtil.WriteLogRaw("Current: ",T004O2_A1860SistemaVersao_Id[0]);
               }
               if ( Z1862SistemaVersao_Evolucao != T004O2_A1862SistemaVersao_Evolucao[0] )
               {
                  GXUtil.WriteLog("sistemaversao:[seudo value changed for attri]"+"SistemaVersao_Evolucao");
                  GXUtil.WriteLogRaw("Old: ",Z1862SistemaVersao_Evolucao);
                  GXUtil.WriteLogRaw("Current: ",T004O2_A1862SistemaVersao_Evolucao[0]);
               }
               if ( Z1863SistemaVersao_Melhoria != T004O2_A1863SistemaVersao_Melhoria[0] )
               {
                  GXUtil.WriteLog("sistemaversao:[seudo value changed for attri]"+"SistemaVersao_Melhoria");
                  GXUtil.WriteLogRaw("Old: ",Z1863SistemaVersao_Melhoria);
                  GXUtil.WriteLogRaw("Current: ",T004O2_A1863SistemaVersao_Melhoria[0]);
               }
               if ( Z1864SistemaVersao_Correcao != T004O2_A1864SistemaVersao_Correcao[0] )
               {
                  GXUtil.WriteLog("sistemaversao:[seudo value changed for attri]"+"SistemaVersao_Correcao");
                  GXUtil.WriteLogRaw("Old: ",Z1864SistemaVersao_Correcao);
                  GXUtil.WriteLogRaw("Current: ",T004O2_A1864SistemaVersao_Correcao[0]);
               }
               if ( Z1865SistemaVersao_Data != T004O2_A1865SistemaVersao_Data[0] )
               {
                  GXUtil.WriteLog("sistemaversao:[seudo value changed for attri]"+"SistemaVersao_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1865SistemaVersao_Data);
                  GXUtil.WriteLogRaw("Current: ",T004O2_A1865SistemaVersao_Data[0]);
               }
               if ( Z1866SistemaVersao_SistemaCod != T004O2_A1866SistemaVersao_SistemaCod[0] )
               {
                  GXUtil.WriteLog("sistemaversao:[seudo value changed for attri]"+"SistemaVersao_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z1866SistemaVersao_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T004O2_A1866SistemaVersao_SistemaCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SistemaVersao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4O209( )
      {
         BeforeValidate4O209( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4O209( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4O209( 0) ;
            CheckOptimisticConcurrency4O209( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4O209( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4O209( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004O10 */
                     pr_default.execute(8, new Object[] {A1860SistemaVersao_Id, A1861SistemaVersao_Descricao, n1862SistemaVersao_Evolucao, A1862SistemaVersao_Evolucao, n1863SistemaVersao_Melhoria, A1863SistemaVersao_Melhoria, n1864SistemaVersao_Correcao, A1864SistemaVersao_Correcao, A1865SistemaVersao_Data, A1866SistemaVersao_SistemaCod});
                     A1859SistemaVersao_Codigo = T004O10_A1859SistemaVersao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
                     n1859SistemaVersao_Codigo = T004O10_n1859SistemaVersao_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaVersao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4O0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4O209( ) ;
            }
            EndLevel4O209( ) ;
         }
         CloseExtendedTableCursors4O209( ) ;
      }

      protected void Update4O209( )
      {
         BeforeValidate4O209( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4O209( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4O209( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4O209( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4O209( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004O11 */
                     pr_default.execute(9, new Object[] {A1860SistemaVersao_Id, A1861SistemaVersao_Descricao, n1862SistemaVersao_Evolucao, A1862SistemaVersao_Evolucao, n1863SistemaVersao_Melhoria, A1863SistemaVersao_Melhoria, n1864SistemaVersao_Correcao, A1864SistemaVersao_Correcao, A1865SistemaVersao_Data, A1866SistemaVersao_SistemaCod, n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("SistemaVersao") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SistemaVersao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4O209( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4O209( ) ;
         }
         CloseExtendedTableCursors4O209( ) ;
      }

      protected void DeferredUpdate4O209( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4O209( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4O209( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4O209( ) ;
            AfterConfirm4O209( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4O209( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004O12 */
                  pr_default.execute(10, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("SistemaVersao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode209 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4O209( ) ;
         Gx_mode = sMode209;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4O209( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004O13 */
            pr_default.execute(11, new Object[] {n1859SistemaVersao_Codigo, A1859SistemaVersao_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
         }
      }

      protected void EndLevel4O209( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4O209( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "SistemaVersao");
            if ( AnyError == 0 )
            {
               ConfirmValues4O0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "SistemaVersao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4O209( )
      {
         /* Scan By routine */
         /* Using cursor T004O14 */
         pr_default.execute(12);
         RcdFound209 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound209 = 1;
            A1859SistemaVersao_Codigo = T004O14_A1859SistemaVersao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            n1859SistemaVersao_Codigo = T004O14_n1859SistemaVersao_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4O209( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound209 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound209 = 1;
            A1859SistemaVersao_Codigo = T004O14_A1859SistemaVersao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            n1859SistemaVersao_Codigo = T004O14_n1859SistemaVersao_Codigo[0];
         }
      }

      protected void ScanEnd4O209( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm4O209( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4O209( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4O209( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4O209( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4O209( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4O209( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4O209( )
      {
         edtSistemaVersao_Id_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Id_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Id_Enabled), 5, 0)));
         edtSistemaVersao_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Descricao_Enabled), 5, 0)));
         edtSistemaVersao_Evolucao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Evolucao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Evolucao_Enabled), 5, 0)));
         edtSistemaVersao_Melhoria_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Melhoria_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Melhoria_Enabled), 5, 0)));
         edtSistemaVersao_Correcao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Correcao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Correcao_Enabled), 5, 0)));
         edtSistemaVersao_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Data_Enabled), 5, 0)));
         edtSistemaVersao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistemaVersao_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4O0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311730692");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SistemaVersao_Codigo) + "," + UrlEncode("" +AV13SistemaVersao_SistemaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1859SistemaVersao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1860SistemaVersao_Id", StringUtil.RTrim( Z1860SistemaVersao_Id));
         GxWebStd.gx_hidden_field( context, "Z1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1862SistemaVersao_Evolucao), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1863SistemaVersao_Melhoria), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1864SistemaVersao_Correcao), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1865SistemaVersao_Data", context.localUtil.TToC( Z1865SistemaVersao_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1866SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1866SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1866SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1866SistemaVersao_SistemaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vSISTEMAVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7SistemaVersao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SISTEMAVERSAO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMAVERSAO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1866SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMAVERSAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7SistemaVersao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "SistemaVersao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1866SistemaVersao_SistemaCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("sistemaversao:[SendSecurityCheck value for]"+"SistemaVersao_Codigo:"+context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("sistemaversao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("sistemaversao:[SendSecurityCheck value for]"+"SistemaVersao_SistemaCod:"+context.localUtil.Format( (decimal)(A1866SistemaVersao_SistemaCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7SistemaVersao_Codigo) + "," + UrlEncode("" +AV13SistemaVersao_SistemaCod) ;
      }

      public override String GetPgmname( )
      {
         return "SistemaVersao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Versionamento" ;
      }

      protected void InitializeNonKey4O209( )
      {
         A1866SistemaVersao_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1866SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1866SistemaVersao_SistemaCod), 6, 0)));
         A1860SistemaVersao_Id = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1860SistemaVersao_Id", A1860SistemaVersao_Id);
         A1861SistemaVersao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1861SistemaVersao_Descricao", A1861SistemaVersao_Descricao);
         A1862SistemaVersao_Evolucao = 0;
         n1862SistemaVersao_Evolucao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1862SistemaVersao_Evolucao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1862SistemaVersao_Evolucao), 4, 0)));
         n1862SistemaVersao_Evolucao = ((0==A1862SistemaVersao_Evolucao) ? true : false);
         A1863SistemaVersao_Melhoria = 0;
         n1863SistemaVersao_Melhoria = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1863SistemaVersao_Melhoria", StringUtil.LTrim( StringUtil.Str( (decimal)(A1863SistemaVersao_Melhoria), 4, 0)));
         n1863SistemaVersao_Melhoria = ((0==A1863SistemaVersao_Melhoria) ? true : false);
         A1864SistemaVersao_Correcao = 0;
         n1864SistemaVersao_Correcao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1864SistemaVersao_Correcao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1864SistemaVersao_Correcao), 8, 0)));
         n1864SistemaVersao_Correcao = ((0==A1864SistemaVersao_Correcao) ? true : false);
         A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1865SistemaVersao_Data", context.localUtil.TToC( A1865SistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
         Z1860SistemaVersao_Id = "";
         Z1862SistemaVersao_Evolucao = 0;
         Z1863SistemaVersao_Melhoria = 0;
         Z1864SistemaVersao_Correcao = 0;
         Z1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         Z1866SistemaVersao_SistemaCod = 0;
      }

      protected void InitAll4O209( )
      {
         A1859SistemaVersao_Codigo = 0;
         n1859SistemaVersao_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
         InitializeNonKey4O209( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1866SistemaVersao_SistemaCod = i1866SistemaVersao_SistemaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1866SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1866SistemaVersao_SistemaCod), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031173079");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("sistemaversao.js", "?202031173079");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksistemaversao_id_Internalname = "TEXTBLOCKSISTEMAVERSAO_ID";
         edtSistemaVersao_Id_Internalname = "SISTEMAVERSAO_ID";
         lblTextblocksistemaversao_descricao_Internalname = "TEXTBLOCKSISTEMAVERSAO_DESCRICAO";
         edtSistemaVersao_Descricao_Internalname = "SISTEMAVERSAO_DESCRICAO";
         lblTextblocksistemaversao_evolucao_Internalname = "TEXTBLOCKSISTEMAVERSAO_EVOLUCAO";
         edtSistemaVersao_Evolucao_Internalname = "SISTEMAVERSAO_EVOLUCAO";
         lblTextblocksistemaversao_melhoria_Internalname = "TEXTBLOCKSISTEMAVERSAO_MELHORIA";
         edtSistemaVersao_Melhoria_Internalname = "SISTEMAVERSAO_MELHORIA";
         lblTextblocksistemaversao_correcao_Internalname = "TEXTBLOCKSISTEMAVERSAO_CORRECAO";
         edtSistemaVersao_Correcao_Internalname = "SISTEMAVERSAO_CORRECAO";
         tblTablemergedsistemaversao_evolucao_Internalname = "TABLEMERGEDSISTEMAVERSAO_EVOLUCAO";
         lblTextblocksistemaversao_data_Internalname = "TEXTBLOCKSISTEMAVERSAO_DATA";
         edtSistemaVersao_Data_Internalname = "SISTEMAVERSAO_DATA";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtSistemaVersao_Codigo_Internalname = "SISTEMAVERSAO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Versionamento";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Versionamento";
         edtSistemaVersao_Correcao_Jsonclick = "";
         edtSistemaVersao_Correcao_Enabled = 1;
         edtSistemaVersao_Melhoria_Jsonclick = "";
         edtSistemaVersao_Melhoria_Enabled = 1;
         edtSistemaVersao_Evolucao_Jsonclick = "";
         edtSistemaVersao_Evolucao_Enabled = 1;
         edtSistemaVersao_Data_Jsonclick = "";
         edtSistemaVersao_Data_Enabled = 1;
         edtSistemaVersao_Descricao_Enabled = 1;
         edtSistemaVersao_Id_Jsonclick = "";
         edtSistemaVersao_Id_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtSistemaVersao_Codigo_Jsonclick = "";
         edtSistemaVersao_Codigo_Enabled = 0;
         edtSistemaVersao_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7SistemaVersao_Codigo',fld:'vSISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV13SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124O2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1860SistemaVersao_Id = "";
         Z1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocksistemaversao_id_Jsonclick = "";
         A1860SistemaVersao_Id = "";
         lblTextblocksistemaversao_descricao_Jsonclick = "";
         A1861SistemaVersao_Descricao = "";
         lblTextblocksistemaversao_evolucao_Jsonclick = "";
         lblTextblocksistemaversao_data_Jsonclick = "";
         A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         lblTextblocksistemaversao_melhoria_Jsonclick = "";
         lblTextblocksistemaversao_correcao_Jsonclick = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode209 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1861SistemaVersao_Descricao = "";
         T004O5_A1859SistemaVersao_Codigo = new int[1] ;
         T004O5_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T004O5_A1860SistemaVersao_Id = new String[] {""} ;
         T004O5_A1861SistemaVersao_Descricao = new String[] {""} ;
         T004O5_A1862SistemaVersao_Evolucao = new short[1] ;
         T004O5_n1862SistemaVersao_Evolucao = new bool[] {false} ;
         T004O5_A1863SistemaVersao_Melhoria = new short[1] ;
         T004O5_n1863SistemaVersao_Melhoria = new bool[] {false} ;
         T004O5_A1864SistemaVersao_Correcao = new int[1] ;
         T004O5_n1864SistemaVersao_Correcao = new bool[] {false} ;
         T004O5_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         T004O5_A1866SistemaVersao_SistemaCod = new int[1] ;
         T004O4_A1866SistemaVersao_SistemaCod = new int[1] ;
         T004O6_A1866SistemaVersao_SistemaCod = new int[1] ;
         T004O7_A1859SistemaVersao_Codigo = new int[1] ;
         T004O7_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T004O3_A1859SistemaVersao_Codigo = new int[1] ;
         T004O3_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T004O3_A1860SistemaVersao_Id = new String[] {""} ;
         T004O3_A1861SistemaVersao_Descricao = new String[] {""} ;
         T004O3_A1862SistemaVersao_Evolucao = new short[1] ;
         T004O3_n1862SistemaVersao_Evolucao = new bool[] {false} ;
         T004O3_A1863SistemaVersao_Melhoria = new short[1] ;
         T004O3_n1863SistemaVersao_Melhoria = new bool[] {false} ;
         T004O3_A1864SistemaVersao_Correcao = new int[1] ;
         T004O3_n1864SistemaVersao_Correcao = new bool[] {false} ;
         T004O3_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         T004O3_A1866SistemaVersao_SistemaCod = new int[1] ;
         T004O8_A1859SistemaVersao_Codigo = new int[1] ;
         T004O8_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T004O9_A1859SistemaVersao_Codigo = new int[1] ;
         T004O9_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T004O2_A1859SistemaVersao_Codigo = new int[1] ;
         T004O2_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T004O2_A1860SistemaVersao_Id = new String[] {""} ;
         T004O2_A1861SistemaVersao_Descricao = new String[] {""} ;
         T004O2_A1862SistemaVersao_Evolucao = new short[1] ;
         T004O2_n1862SistemaVersao_Evolucao = new bool[] {false} ;
         T004O2_A1863SistemaVersao_Melhoria = new short[1] ;
         T004O2_n1863SistemaVersao_Melhoria = new bool[] {false} ;
         T004O2_A1864SistemaVersao_Correcao = new int[1] ;
         T004O2_n1864SistemaVersao_Correcao = new bool[] {false} ;
         T004O2_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         T004O2_A1866SistemaVersao_SistemaCod = new int[1] ;
         T004O10_A1859SistemaVersao_Codigo = new int[1] ;
         T004O10_n1859SistemaVersao_Codigo = new bool[] {false} ;
         T004O13_A127Sistema_Codigo = new int[1] ;
         T004O14_A1859SistemaVersao_Codigo = new int[1] ;
         T004O14_n1859SistemaVersao_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemaversao__default(),
            new Object[][] {
                new Object[] {
               T004O2_A1859SistemaVersao_Codigo, T004O2_A1860SistemaVersao_Id, T004O2_A1861SistemaVersao_Descricao, T004O2_A1862SistemaVersao_Evolucao, T004O2_n1862SistemaVersao_Evolucao, T004O2_A1863SistemaVersao_Melhoria, T004O2_n1863SistemaVersao_Melhoria, T004O2_A1864SistemaVersao_Correcao, T004O2_n1864SistemaVersao_Correcao, T004O2_A1865SistemaVersao_Data,
               T004O2_A1866SistemaVersao_SistemaCod
               }
               , new Object[] {
               T004O3_A1859SistemaVersao_Codigo, T004O3_A1860SistemaVersao_Id, T004O3_A1861SistemaVersao_Descricao, T004O3_A1862SistemaVersao_Evolucao, T004O3_n1862SistemaVersao_Evolucao, T004O3_A1863SistemaVersao_Melhoria, T004O3_n1863SistemaVersao_Melhoria, T004O3_A1864SistemaVersao_Correcao, T004O3_n1864SistemaVersao_Correcao, T004O3_A1865SistemaVersao_Data,
               T004O3_A1866SistemaVersao_SistemaCod
               }
               , new Object[] {
               T004O4_A1866SistemaVersao_SistemaCod
               }
               , new Object[] {
               T004O5_A1859SistemaVersao_Codigo, T004O5_A1860SistemaVersao_Id, T004O5_A1861SistemaVersao_Descricao, T004O5_A1862SistemaVersao_Evolucao, T004O5_n1862SistemaVersao_Evolucao, T004O5_A1863SistemaVersao_Melhoria, T004O5_n1863SistemaVersao_Melhoria, T004O5_A1864SistemaVersao_Correcao, T004O5_n1864SistemaVersao_Correcao, T004O5_A1865SistemaVersao_Data,
               T004O5_A1866SistemaVersao_SistemaCod
               }
               , new Object[] {
               T004O6_A1866SistemaVersao_SistemaCod
               }
               , new Object[] {
               T004O7_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               T004O8_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               T004O9_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               T004O10_A1859SistemaVersao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004O13_A127Sistema_Codigo
               }
               , new Object[] {
               T004O14_A1859SistemaVersao_Codigo
               }
            }
         );
         AV14Pgmname = "SistemaVersao";
      }

      private short Z1862SistemaVersao_Evolucao ;
      private short Z1863SistemaVersao_Melhoria ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1862SistemaVersao_Evolucao ;
      private short A1863SistemaVersao_Melhoria ;
      private short RcdFound209 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7SistemaVersao_Codigo ;
      private int wcpOAV13SistemaVersao_SistemaCod ;
      private int Z1859SistemaVersao_Codigo ;
      private int Z1864SistemaVersao_Correcao ;
      private int Z1866SistemaVersao_SistemaCod ;
      private int N1866SistemaVersao_SistemaCod ;
      private int A1866SistemaVersao_SistemaCod ;
      private int AV7SistemaVersao_Codigo ;
      private int AV13SistemaVersao_SistemaCod ;
      private int trnEnded ;
      private int A1859SistemaVersao_Codigo ;
      private int edtSistemaVersao_Codigo_Enabled ;
      private int edtSistemaVersao_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtSistemaVersao_Id_Enabled ;
      private int edtSistemaVersao_Descricao_Enabled ;
      private int edtSistemaVersao_Data_Enabled ;
      private int edtSistemaVersao_Evolucao_Enabled ;
      private int edtSistemaVersao_Melhoria_Enabled ;
      private int A1864SistemaVersao_Correcao ;
      private int edtSistemaVersao_Correcao_Enabled ;
      private int AV11Insert_SistemaVersao_SistemaCod ;
      private int AV15GXV1 ;
      private int i1866SistemaVersao_SistemaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1860SistemaVersao_Id ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtSistemaVersao_Id_Internalname ;
      private String edtSistemaVersao_Codigo_Internalname ;
      private String edtSistemaVersao_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocksistemaversao_id_Internalname ;
      private String lblTextblocksistemaversao_id_Jsonclick ;
      private String A1860SistemaVersao_Id ;
      private String edtSistemaVersao_Id_Jsonclick ;
      private String lblTextblocksistemaversao_descricao_Internalname ;
      private String lblTextblocksistemaversao_descricao_Jsonclick ;
      private String edtSistemaVersao_Descricao_Internalname ;
      private String lblTextblocksistemaversao_evolucao_Internalname ;
      private String lblTextblocksistemaversao_evolucao_Jsonclick ;
      private String lblTextblocksistemaversao_data_Internalname ;
      private String lblTextblocksistemaversao_data_Jsonclick ;
      private String edtSistemaVersao_Data_Internalname ;
      private String edtSistemaVersao_Data_Jsonclick ;
      private String tblTablemergedsistemaversao_evolucao_Internalname ;
      private String edtSistemaVersao_Evolucao_Internalname ;
      private String edtSistemaVersao_Evolucao_Jsonclick ;
      private String lblTextblocksistemaversao_melhoria_Internalname ;
      private String lblTextblocksistemaversao_melhoria_Jsonclick ;
      private String edtSistemaVersao_Melhoria_Internalname ;
      private String edtSistemaVersao_Melhoria_Jsonclick ;
      private String lblTextblocksistemaversao_correcao_Internalname ;
      private String lblTextblocksistemaversao_correcao_Jsonclick ;
      private String edtSistemaVersao_Correcao_Internalname ;
      private String edtSistemaVersao_Correcao_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode209 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z1865SistemaVersao_Data ;
      private DateTime A1865SistemaVersao_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1862SistemaVersao_Evolucao ;
      private bool n1863SistemaVersao_Melhoria ;
      private bool n1864SistemaVersao_Correcao ;
      private bool n1859SistemaVersao_Codigo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1861SistemaVersao_Descricao ;
      private String Z1861SistemaVersao_Descricao ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_SistemaVersao_SistemaCod ;
      private IDataStoreProvider pr_default ;
      private int[] T004O5_A1859SistemaVersao_Codigo ;
      private bool[] T004O5_n1859SistemaVersao_Codigo ;
      private String[] T004O5_A1860SistemaVersao_Id ;
      private String[] T004O5_A1861SistemaVersao_Descricao ;
      private short[] T004O5_A1862SistemaVersao_Evolucao ;
      private bool[] T004O5_n1862SistemaVersao_Evolucao ;
      private short[] T004O5_A1863SistemaVersao_Melhoria ;
      private bool[] T004O5_n1863SistemaVersao_Melhoria ;
      private int[] T004O5_A1864SistemaVersao_Correcao ;
      private bool[] T004O5_n1864SistemaVersao_Correcao ;
      private DateTime[] T004O5_A1865SistemaVersao_Data ;
      private int[] T004O5_A1866SistemaVersao_SistemaCod ;
      private int[] T004O4_A1866SistemaVersao_SistemaCod ;
      private int[] T004O6_A1866SistemaVersao_SistemaCod ;
      private int[] T004O7_A1859SistemaVersao_Codigo ;
      private bool[] T004O7_n1859SistemaVersao_Codigo ;
      private int[] T004O3_A1859SistemaVersao_Codigo ;
      private bool[] T004O3_n1859SistemaVersao_Codigo ;
      private String[] T004O3_A1860SistemaVersao_Id ;
      private String[] T004O3_A1861SistemaVersao_Descricao ;
      private short[] T004O3_A1862SistemaVersao_Evolucao ;
      private bool[] T004O3_n1862SistemaVersao_Evolucao ;
      private short[] T004O3_A1863SistemaVersao_Melhoria ;
      private bool[] T004O3_n1863SistemaVersao_Melhoria ;
      private int[] T004O3_A1864SistemaVersao_Correcao ;
      private bool[] T004O3_n1864SistemaVersao_Correcao ;
      private DateTime[] T004O3_A1865SistemaVersao_Data ;
      private int[] T004O3_A1866SistemaVersao_SistemaCod ;
      private int[] T004O8_A1859SistemaVersao_Codigo ;
      private bool[] T004O8_n1859SistemaVersao_Codigo ;
      private int[] T004O9_A1859SistemaVersao_Codigo ;
      private bool[] T004O9_n1859SistemaVersao_Codigo ;
      private int[] T004O2_A1859SistemaVersao_Codigo ;
      private bool[] T004O2_n1859SistemaVersao_Codigo ;
      private String[] T004O2_A1860SistemaVersao_Id ;
      private String[] T004O2_A1861SistemaVersao_Descricao ;
      private short[] T004O2_A1862SistemaVersao_Evolucao ;
      private bool[] T004O2_n1862SistemaVersao_Evolucao ;
      private short[] T004O2_A1863SistemaVersao_Melhoria ;
      private bool[] T004O2_n1863SistemaVersao_Melhoria ;
      private int[] T004O2_A1864SistemaVersao_Correcao ;
      private bool[] T004O2_n1864SistemaVersao_Correcao ;
      private DateTime[] T004O2_A1865SistemaVersao_Data ;
      private int[] T004O2_A1866SistemaVersao_SistemaCod ;
      private int[] T004O10_A1859SistemaVersao_Codigo ;
      private bool[] T004O10_n1859SistemaVersao_Codigo ;
      private int[] T004O13_A127Sistema_Codigo ;
      private int[] T004O14_A1859SistemaVersao_Codigo ;
      private bool[] T004O14_n1859SistemaVersao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class sistemaversao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004O5 ;
          prmT004O5 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O4 ;
          prmT004O4 = new Object[] {
          new Object[] {"@SistemaVersao_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O6 ;
          prmT004O6 = new Object[] {
          new Object[] {"@SistemaVersao_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O7 ;
          prmT004O7 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O3 ;
          prmT004O3 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O8 ;
          prmT004O8 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O9 ;
          prmT004O9 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O2 ;
          prmT004O2 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O10 ;
          prmT004O10 = new Object[] {
          new Object[] {"@SistemaVersao_Id",SqlDbType.Char,20,0} ,
          new Object[] {"@SistemaVersao_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@SistemaVersao_Evolucao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@SistemaVersao_Melhoria",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@SistemaVersao_Correcao",SqlDbType.Int,8,0} ,
          new Object[] {"@SistemaVersao_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@SistemaVersao_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O11 ;
          prmT004O11 = new Object[] {
          new Object[] {"@SistemaVersao_Id",SqlDbType.Char,20,0} ,
          new Object[] {"@SistemaVersao_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@SistemaVersao_Evolucao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@SistemaVersao_Melhoria",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@SistemaVersao_Correcao",SqlDbType.Int,8,0} ,
          new Object[] {"@SistemaVersao_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@SistemaVersao_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O12 ;
          prmT004O12 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O13 ;
          prmT004O13 = new Object[] {
          new Object[] {"@SistemaVersao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004O14 ;
          prmT004O14 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T004O2", "SELECT [SistemaVersao_Codigo], [SistemaVersao_Id], [SistemaVersao_Descricao], [SistemaVersao_Evolucao], [SistemaVersao_Melhoria], [SistemaVersao_Correcao], [SistemaVersao_Data], [SistemaVersao_SistemaCod] AS SistemaVersao_SistemaCod FROM [SistemaVersao] WITH (UPDLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004O2,1,0,true,false )
             ,new CursorDef("T004O3", "SELECT [SistemaVersao_Codigo], [SistemaVersao_Id], [SistemaVersao_Descricao], [SistemaVersao_Evolucao], [SistemaVersao_Melhoria], [SistemaVersao_Correcao], [SistemaVersao_Data], [SistemaVersao_SistemaCod] AS SistemaVersao_SistemaCod FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004O3,1,0,true,false )
             ,new CursorDef("T004O4", "SELECT [Sistema_Codigo] AS SistemaVersao_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @SistemaVersao_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004O4,1,0,true,false )
             ,new CursorDef("T004O5", "SELECT TM1.[SistemaVersao_Codigo], TM1.[SistemaVersao_Id], TM1.[SistemaVersao_Descricao], TM1.[SistemaVersao_Evolucao], TM1.[SistemaVersao_Melhoria], TM1.[SistemaVersao_Correcao], TM1.[SistemaVersao_Data], TM1.[SistemaVersao_SistemaCod] AS SistemaVersao_SistemaCod FROM [SistemaVersao] TM1 WITH (NOLOCK) WHERE TM1.[SistemaVersao_Codigo] = @SistemaVersao_Codigo ORDER BY TM1.[SistemaVersao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004O5,100,0,true,false )
             ,new CursorDef("T004O6", "SELECT [Sistema_Codigo] AS SistemaVersao_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @SistemaVersao_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004O6,1,0,true,false )
             ,new CursorDef("T004O7", "SELECT [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004O7,1,0,true,false )
             ,new CursorDef("T004O8", "SELECT TOP 1 [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK) WHERE ( [SistemaVersao_Codigo] > @SistemaVersao_Codigo) ORDER BY [SistemaVersao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004O8,1,0,true,true )
             ,new CursorDef("T004O9", "SELECT TOP 1 [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK) WHERE ( [SistemaVersao_Codigo] < @SistemaVersao_Codigo) ORDER BY [SistemaVersao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004O9,1,0,true,true )
             ,new CursorDef("T004O10", "INSERT INTO [SistemaVersao]([SistemaVersao_Id], [SistemaVersao_Descricao], [SistemaVersao_Evolucao], [SistemaVersao_Melhoria], [SistemaVersao_Correcao], [SistemaVersao_Data], [SistemaVersao_SistemaCod]) VALUES(@SistemaVersao_Id, @SistemaVersao_Descricao, @SistemaVersao_Evolucao, @SistemaVersao_Melhoria, @SistemaVersao_Correcao, @SistemaVersao_Data, @SistemaVersao_SistemaCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004O10)
             ,new CursorDef("T004O11", "UPDATE [SistemaVersao] SET [SistemaVersao_Id]=@SistemaVersao_Id, [SistemaVersao_Descricao]=@SistemaVersao_Descricao, [SistemaVersao_Evolucao]=@SistemaVersao_Evolucao, [SistemaVersao_Melhoria]=@SistemaVersao_Melhoria, [SistemaVersao_Correcao]=@SistemaVersao_Correcao, [SistemaVersao_Data]=@SistemaVersao_Data, [SistemaVersao_SistemaCod]=@SistemaVersao_SistemaCod  WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo", GxErrorMask.GX_NOMASK,prmT004O11)
             ,new CursorDef("T004O12", "DELETE FROM [SistemaVersao]  WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo", GxErrorMask.GX_NOMASK,prmT004O12)
             ,new CursorDef("T004O13", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [SistemaVersao_Codigo] = @SistemaVersao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004O13,1,0,true,true )
             ,new CursorDef("T004O14", "SELECT [SistemaVersao_Codigo] FROM [SistemaVersao] WITH (NOLOCK) ORDER BY [SistemaVersao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004O14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                stmt.SetParameterDatetime(6, (DateTime)parms[8]);
                stmt.SetParameter(7, (int)parms[9]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                stmt.SetParameterDatetime(6, (DateTime)parms[8]);
                stmt.SetParameter(7, (int)parms[9]);
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[11]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
