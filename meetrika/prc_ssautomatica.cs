/*
               File: PRC_SSAutomatica
        Description: SS Automatica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:44.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ssautomatica : GXProcedure
   {
      public prc_ssautomatica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ssautomatica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           out bool aP1_Contratante_SSAutomatica )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8Contratante_SSAutomatica = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Contratante_SSAutomatica=this.AV8Contratante_SSAutomatica;
      }

      public bool executeUdp( ref int aP0_AreaTrabalho_Codigo )
      {
         this.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV8Contratante_SSAutomatica = false ;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Contratante_SSAutomatica=this.AV8Contratante_SSAutomatica;
         return AV8Contratante_SSAutomatica ;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 out bool aP1_Contratante_SSAutomatica )
      {
         prc_ssautomatica objprc_ssautomatica;
         objprc_ssautomatica = new prc_ssautomatica();
         objprc_ssautomatica.A5AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_ssautomatica.AV8Contratante_SSAutomatica = false ;
         objprc_ssautomatica.context.SetSubmitInitialConfig(context);
         objprc_ssautomatica.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ssautomatica);
         aP0_AreaTrabalho_Codigo=this.A5AreaTrabalho_Codigo;
         aP1_Contratante_SSAutomatica=this.AV8Contratante_SSAutomatica;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ssautomatica)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BW2 */
         pr_default.execute(0, new Object[] {A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29Contratante_Codigo = P00BW2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00BW2_n29Contratante_Codigo[0];
            A1652Contratante_SSAutomatica = P00BW2_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = P00BW2_n1652Contratante_SSAutomatica[0];
            A1652Contratante_SSAutomatica = P00BW2_A1652Contratante_SSAutomatica[0];
            n1652Contratante_SSAutomatica = P00BW2_n1652Contratante_SSAutomatica[0];
            AV8Contratante_SSAutomatica = A1652Contratante_SSAutomatica;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BW2_A29Contratante_Codigo = new int[1] ;
         P00BW2_n29Contratante_Codigo = new bool[] {false} ;
         P00BW2_A5AreaTrabalho_Codigo = new int[1] ;
         P00BW2_A1652Contratante_SSAutomatica = new bool[] {false} ;
         P00BW2_n1652Contratante_SSAutomatica = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ssautomatica__default(),
            new Object[][] {
                new Object[] {
               P00BW2_A29Contratante_Codigo, P00BW2_n29Contratante_Codigo, P00BW2_A5AreaTrabalho_Codigo, P00BW2_A1652Contratante_SSAutomatica, P00BW2_n1652Contratante_SSAutomatica
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV8Contratante_SSAutomatica ;
      private bool n29Contratante_Codigo ;
      private bool A1652Contratante_SSAutomatica ;
      private bool n1652Contratante_SSAutomatica ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00BW2_A29Contratante_Codigo ;
      private bool[] P00BW2_n29Contratante_Codigo ;
      private int[] P00BW2_A5AreaTrabalho_Codigo ;
      private bool[] P00BW2_A1652Contratante_SSAutomatica ;
      private bool[] P00BW2_n1652Contratante_SSAutomatica ;
      private bool aP1_Contratante_SSAutomatica ;
   }

   public class prc_ssautomatica__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BW2 ;
          prmP00BW2 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BW2", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_SSAutomatica] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BW2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
