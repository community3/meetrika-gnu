/*
               File: GetWWContagemResultadoIncidentesFilterData
        Description: Get WWContagem Resultado Incidentes Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:7:38.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemresultadoincidentesfilterdata : GXProcedure
   {
      public getwwcontagemresultadoincidentesfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemresultadoincidentesfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemresultadoincidentesfilterdata objgetwwcontagemresultadoincidentesfilterdata;
         objgetwwcontagemresultadoincidentesfilterdata = new getwwcontagemresultadoincidentesfilterdata();
         objgetwwcontagemresultadoincidentesfilterdata.AV16DDOName = aP0_DDOName;
         objgetwwcontagemresultadoincidentesfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwcontagemresultadoincidentesfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemresultadoincidentesfilterdata.AV20OptionsJson = "" ;
         objgetwwcontagemresultadoincidentesfilterdata.AV23OptionsDescJson = "" ;
         objgetwwcontagemresultadoincidentesfilterdata.AV25OptionIndexesJson = "" ;
         objgetwwcontagemresultadoincidentesfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemresultadoincidentesfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemresultadoincidentesfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemresultadoincidentesfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_INCIDENTES_DEMANDANUM") == 0 )
         {
            /* Execute user subroutine: 'LOADINCIDENTES_DEMANDANUMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWContagemResultadoIncidentesGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContagemResultadoIncidentesGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWContagemResultadoIncidentesGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DATA") == 0 )
            {
               AV10TFIncidentes_Data = context.localUtil.CToT( AV30GridStateFilterValue.gxTpr_Value, 2);
               AV11TFIncidentes_Data_To = context.localUtil.CToT( AV30GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DEMANDANUM") == 0 )
            {
               AV12TFIncidentes_DemandaNum = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFINCIDENTES_DEMANDANUM_SEL") == 0 )
            {
               AV13TFIncidentes_DemandaNum_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 )
            {
               AV33Incidentes_Data1 = context.localUtil.CToT( AV31GridStateDynamicFilter.gxTpr_Value, 2);
               AV34Incidentes_Data_To1 = context.localUtil.CToT( AV31GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "INCIDENTES_DEMANDANUM") == 0 )
            {
               AV35Incidentes_DemandaNum1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 )
               {
                  AV38Incidentes_Data2 = context.localUtil.CToT( AV31GridStateDynamicFilter.gxTpr_Value, 2);
                  AV39Incidentes_Data_To2 = context.localUtil.CToT( AV31GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "INCIDENTES_DEMANDANUM") == 0 )
               {
                  AV40Incidentes_DemandaNum2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 )
                  {
                     AV43Incidentes_Data3 = context.localUtil.CToT( AV31GridStateDynamicFilter.gxTpr_Value, 2);
                     AV44Incidentes_Data_To3 = context.localUtil.CToT( AV31GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "INCIDENTES_DEMANDANUM") == 0 )
                  {
                     AV45Incidentes_DemandaNum3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADINCIDENTES_DEMANDANUMOPTIONS' Routine */
         AV12TFIncidentes_DemandaNum = AV14SearchTxt;
         AV13TFIncidentes_DemandaNum_Sel = "";
         AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1 = AV33Incidentes_Data1;
         AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = AV34Incidentes_Data_To1;
         AV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = AV35Incidentes_DemandaNum1;
         AV54WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2 = AV38Incidentes_Data2;
         AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = AV39Incidentes_Data_To2;
         AV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = AV40Incidentes_DemandaNum2;
         AV59WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3 = AV43Incidentes_Data3;
         AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = AV44Incidentes_Data_To3;
         AV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = AV45Incidentes_DemandaNum3;
         AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data = AV10TFIncidentes_Data;
         AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = AV11TFIncidentes_Data_To;
         AV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = AV12TFIncidentes_DemandaNum;
         AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = AV13TFIncidentes_DemandaNum_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ,
                                              AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1 ,
                                              AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ,
                                              AV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ,
                                              AV54WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ,
                                              AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ,
                                              AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2 ,
                                              AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ,
                                              AV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ,
                                              AV59WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ,
                                              AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ,
                                              AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3 ,
                                              AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ,
                                              AV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ,
                                              AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data ,
                                              AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ,
                                              AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ,
                                              AV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ,
                                              A1242Incidentes_Data ,
                                              A1240Incidentes_DemandaNum },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = StringUtil.Concat( StringUtil.RTrim( AV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1), "%", "");
         lV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = StringUtil.Concat( StringUtil.RTrim( AV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2), "%", "");
         lV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = StringUtil.Concat( StringUtil.RTrim( AV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3), "%", "");
         lV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = StringUtil.Concat( StringUtil.RTrim( AV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum), "%", "");
         /* Using cursor P00QG2 */
         pr_default.execute(0, new Object[] {AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1, AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1, lV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1, AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2, AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2, lV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2, AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3, AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3, lV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3, AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data, AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to, lV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum, AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQG2 = false;
            A1239Incidentes_DemandaCod = P00QG2_A1239Incidentes_DemandaCod[0];
            A1240Incidentes_DemandaNum = P00QG2_A1240Incidentes_DemandaNum[0];
            n1240Incidentes_DemandaNum = P00QG2_n1240Incidentes_DemandaNum[0];
            A1242Incidentes_Data = P00QG2_A1242Incidentes_Data[0];
            A1241Incidentes_Codigo = P00QG2_A1241Incidentes_Codigo[0];
            A1240Incidentes_DemandaNum = P00QG2_A1240Incidentes_DemandaNum[0];
            n1240Incidentes_DemandaNum = P00QG2_n1240Incidentes_DemandaNum[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00QG2_A1240Incidentes_DemandaNum[0], A1240Incidentes_DemandaNum) == 0 ) )
            {
               BRKQG2 = false;
               A1239Incidentes_DemandaCod = P00QG2_A1239Incidentes_DemandaCod[0];
               A1241Incidentes_Codigo = P00QG2_A1241Incidentes_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKQG2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1240Incidentes_DemandaNum)) )
            {
               AV18Option = A1240Incidentes_DemandaNum;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQG2 )
            {
               BRKQG2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFIncidentes_Data = (DateTime)(DateTime.MinValue);
         AV11TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
         AV12TFIncidentes_DemandaNum = "";
         AV13TFIncidentes_DemandaNum_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV33Incidentes_Data1 = (DateTime)(DateTime.MinValue);
         AV34Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
         AV35Incidentes_DemandaNum1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38Incidentes_Data2 = (DateTime)(DateTime.MinValue);
         AV39Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
         AV40Incidentes_DemandaNum2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV43Incidentes_Data3 = (DateTime)(DateTime.MinValue);
         AV44Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
         AV45Incidentes_DemandaNum3 = "";
         AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 = "";
         AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1 = (DateTime)(DateTime.MinValue);
         AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 = (DateTime)(DateTime.MinValue);
         AV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = "";
         AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 = "";
         AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2 = (DateTime)(DateTime.MinValue);
         AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 = (DateTime)(DateTime.MinValue);
         AV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = "";
         AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 = "";
         AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3 = (DateTime)(DateTime.MinValue);
         AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 = (DateTime)(DateTime.MinValue);
         AV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = "";
         AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data = (DateTime)(DateTime.MinValue);
         AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to = (DateTime)(DateTime.MinValue);
         AV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = "";
         AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel = "";
         scmdbuf = "";
         lV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 = "";
         lV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 = "";
         lV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 = "";
         lV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum = "";
         A1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         A1240Incidentes_DemandaNum = "";
         P00QG2_A1239Incidentes_DemandaCod = new int[1] ;
         P00QG2_A1240Incidentes_DemandaNum = new String[] {""} ;
         P00QG2_n1240Incidentes_DemandaNum = new bool[] {false} ;
         P00QG2_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         P00QG2_A1241Incidentes_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontagemresultadoincidentesfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QG2_A1239Incidentes_DemandaCod, P00QG2_A1240Incidentes_DemandaNum, P00QG2_n1240Incidentes_DemandaNum, P00QG2_A1242Incidentes_Data, P00QG2_A1241Incidentes_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV48GXV1 ;
      private int A1239Incidentes_DemandaCod ;
      private int A1241Incidentes_Codigo ;
      private long AV26count ;
      private String scmdbuf ;
      private DateTime AV10TFIncidentes_Data ;
      private DateTime AV11TFIncidentes_Data_To ;
      private DateTime AV33Incidentes_Data1 ;
      private DateTime AV34Incidentes_Data_To1 ;
      private DateTime AV38Incidentes_Data2 ;
      private DateTime AV39Incidentes_Data_To2 ;
      private DateTime AV43Incidentes_Data3 ;
      private DateTime AV44Incidentes_Data_To3 ;
      private DateTime AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1 ;
      private DateTime AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ;
      private DateTime AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2 ;
      private DateTime AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ;
      private DateTime AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3 ;
      private DateTime AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ;
      private DateTime AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data ;
      private DateTime AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ;
      private DateTime A1242Incidentes_Data ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool AV54WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ;
      private bool AV59WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ;
      private bool BRKQG2 ;
      private bool n1240Incidentes_DemandaNum ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV12TFIncidentes_DemandaNum ;
      private String AV13TFIncidentes_DemandaNum_Sel ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV35Incidentes_DemandaNum1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV40Incidentes_DemandaNum2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV45Incidentes_DemandaNum3 ;
      private String AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ;
      private String AV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ;
      private String AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ;
      private String AV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ;
      private String AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ;
      private String AV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ;
      private String AV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ;
      private String AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ;
      private String lV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ;
      private String lV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ;
      private String lV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ;
      private String lV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ;
      private String A1240Incidentes_DemandaNum ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00QG2_A1239Incidentes_DemandaCod ;
      private String[] P00QG2_A1240Incidentes_DemandaNum ;
      private bool[] P00QG2_n1240Incidentes_DemandaNum ;
      private DateTime[] P00QG2_A1242Incidentes_Data ;
      private int[] P00QG2_A1241Incidentes_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwcontagemresultadoincidentesfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QG2( IGxContext context ,
                                             String AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1 ,
                                             DateTime AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1 ,
                                             String AV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 ,
                                             bool AV54WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 ,
                                             String AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2 ,
                                             DateTime AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2 ,
                                             DateTime AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2 ,
                                             String AV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 ,
                                             bool AV59WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 ,
                                             String AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3 ,
                                             DateTime AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3 ,
                                             DateTime AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3 ,
                                             String AV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 ,
                                             DateTime AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data ,
                                             DateTime AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to ,
                                             String AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel ,
                                             String AV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum ,
                                             DateTime A1242Incidentes_Data ,
                                             String A1240Incidentes_DemandaNum )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Incidentes_DemandaCod] AS Incidentes_DemandaCod, T2.[ContagemResultado_Demanda] AS Incidentes_DemandaNum, T1.[Incidentes_Data], T1.[Incidentes_Codigo] FROM ([ContagemResultadoIncidentes] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Incidentes_DemandaCod])";
         if ( ( StringUtil.StrCmp(AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV50WWContagemResultadoIncidentesDS_1_Dynamicfiltersselector1, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV54WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV54WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV54WWContagemResultadoIncidentesDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV55WWContagemResultadoIncidentesDS_6_Dynamicfiltersselector2, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2 + '%')";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV59WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV59WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV59WWContagemResultadoIncidentesDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWContagemResultadoIncidentesDS_11_Dynamicfiltersselector3, "INCIDENTES_DEMANDANUM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3 + '%')";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] = @AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_Demanda]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QG2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QG2 ;
          prmP00QG2 = new Object[] {
          new Object[] {"@AV51WWContagemResultadoIncidentesDS_2_Incidentes_data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV52WWContagemResultadoIncidentesDS_3_Incidentes_data_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV53WWContagemResultadoIncidentesDS_4_Incidentes_demandanum1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV56WWContagemResultadoIncidentesDS_7_Incidentes_data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV57WWContagemResultadoIncidentesDS_8_Incidentes_data_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV58WWContagemResultadoIncidentesDS_9_Incidentes_demandanum2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV61WWContagemResultadoIncidentesDS_12_Incidentes_data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV62WWContagemResultadoIncidentesDS_13_Incidentes_data_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV63WWContagemResultadoIncidentesDS_14_Incidentes_demandanum3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV64WWContagemResultadoIncidentesDS_15_Tfincidentes_data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV65WWContagemResultadoIncidentesDS_16_Tfincidentes_data_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV66WWContagemResultadoIncidentesDS_17_Tfincidentes_demandanum",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV67WWContagemResultadoIncidentesDS_18_Tfincidentes_demandanum_sel",SqlDbType.VarChar,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QG2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemresultadoincidentesfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontagemresultadoincidentesfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontagemresultadoincidentesfilterdata") )
          {
             return  ;
          }
          getwwcontagemresultadoincidentesfilterdata worker = new getwwcontagemresultadoincidentesfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
