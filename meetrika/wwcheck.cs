/*
               File: WWCheck
        Description:  Controle
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:6:35.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcheck : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcheck( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcheck( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbCheck_Momento = new GXCombobox();
         cmbCheck_QdoNaoCmp = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_92 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_92_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_92_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Check_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Check_Nome1", AV18Check_Nome1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22Check_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Check_Nome2", AV22Check_Nome2);
               AV24DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
               AV26Check_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Check_Nome3", AV26Check_Nome3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
               AV39TFCheck_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCheck_Nome", AV39TFCheck_Nome);
               AV40TFCheck_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCheck_Nome_Sel", AV40TFCheck_Nome_Sel);
               AV32ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
               AV41ddo_Check_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Check_NomeTitleControlIdToReplace", AV41ddo_Check_NomeTitleControlIdToReplace);
               AV49ddo_Check_MomentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Check_MomentoTitleControlIdToReplace", AV49ddo_Check_MomentoTitleControlIdToReplace);
               AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace", AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace);
               AV15Check_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Check_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV48TFCheck_Momento_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV71TFCheck_QdoNaoCmp_Sels);
               AV94Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
               AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
               A1839Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PANS2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTNS2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311963535");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcheck.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCHECK_NOME1", StringUtil.RTrim( AV18Check_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCHECK_NOME2", StringUtil.RTrim( AV22Check_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCHECK_NOME3", StringUtil.RTrim( AV26Check_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECK_NOME", StringUtil.RTrim( AV39TFCheck_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECK_NOME_SEL", StringUtil.RTrim( AV40TFCheck_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_92", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_92), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV36ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV36ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECK_NOMETITLEFILTERDATA", AV38Check_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECK_NOMETITLEFILTERDATA", AV38Check_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECK_MOMENTOTITLEFILTERDATA", AV46Check_MomentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECK_MOMENTOTITLEFILTERDATA", AV46Check_MomentoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECK_QDONAOCMPTITLEFILTERDATA", AV69Check_QdoNaoCmpTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECK_QDONAOCMPTITLEFILTERDATA", AV69Check_QdoNaoCmpTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCHECK_MOMENTO_SELS", AV48TFCheck_Momento_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCHECK_MOMENTO_SELS", AV48TFCheck_Momento_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCHECK_QDONAOCMP_SELS", AV71TFCheck_QdoNaoCmp_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCHECK_QDONAOCMP_SELS", AV71TFCheck_QdoNaoCmp_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV94Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Caption", StringUtil.RTrim( Ddo_check_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Tooltip", StringUtil.RTrim( Ddo_check_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Cls", StringUtil.RTrim( Ddo_check_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_check_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_check_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_check_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_check_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_check_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_check_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Sortedstatus", StringUtil.RTrim( Ddo_check_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Includefilter", StringUtil.BoolToStr( Ddo_check_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Filtertype", StringUtil.RTrim( Ddo_check_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_check_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_check_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Datalisttype", StringUtil.RTrim( Ddo_check_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Datalistproc", StringUtil.RTrim( Ddo_check_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_check_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Sortasc", StringUtil.RTrim( Ddo_check_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Sortdsc", StringUtil.RTrim( Ddo_check_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Loadingdata", StringUtil.RTrim( Ddo_check_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Cleanfilter", StringUtil.RTrim( Ddo_check_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Noresultsfound", StringUtil.RTrim( Ddo_check_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_check_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Caption", StringUtil.RTrim( Ddo_check_momento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Tooltip", StringUtil.RTrim( Ddo_check_momento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Cls", StringUtil.RTrim( Ddo_check_momento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Selectedvalue_set", StringUtil.RTrim( Ddo_check_momento_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_check_momento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_check_momento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_check_momento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_check_momento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Sortedstatus", StringUtil.RTrim( Ddo_check_momento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Includefilter", StringUtil.BoolToStr( Ddo_check_momento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_check_momento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Datalisttype", StringUtil.RTrim( Ddo_check_momento_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_check_momento_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Datalistfixedvalues", StringUtil.RTrim( Ddo_check_momento_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Sortasc", StringUtil.RTrim( Ddo_check_momento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Sortdsc", StringUtil.RTrim( Ddo_check_momento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Cleanfilter", StringUtil.RTrim( Ddo_check_momento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_check_momento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Caption", StringUtil.RTrim( Ddo_check_qdonaocmp_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Tooltip", StringUtil.RTrim( Ddo_check_qdonaocmp_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Cls", StringUtil.RTrim( Ddo_check_qdonaocmp_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Selectedvalue_set", StringUtil.RTrim( Ddo_check_qdonaocmp_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Dropdownoptionstype", StringUtil.RTrim( Ddo_check_qdonaocmp_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_check_qdonaocmp_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Includesortasc", StringUtil.BoolToStr( Ddo_check_qdonaocmp_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Includesortdsc", StringUtil.BoolToStr( Ddo_check_qdonaocmp_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Sortedstatus", StringUtil.RTrim( Ddo_check_qdonaocmp_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Includefilter", StringUtil.BoolToStr( Ddo_check_qdonaocmp_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Includedatalist", StringUtil.BoolToStr( Ddo_check_qdonaocmp_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Datalisttype", StringUtil.RTrim( Ddo_check_qdonaocmp_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Allowmultipleselection", StringUtil.BoolToStr( Ddo_check_qdonaocmp_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Datalistfixedvalues", StringUtil.RTrim( Ddo_check_qdonaocmp_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Sortasc", StringUtil.RTrim( Ddo_check_qdonaocmp_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Sortdsc", StringUtil.RTrim( Ddo_check_qdonaocmp_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Cleanfilter", StringUtil.RTrim( Ddo_check_qdonaocmp_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Searchbuttontext", StringUtil.RTrim( Ddo_check_qdonaocmp_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Activeeventkey", StringUtil.RTrim( Ddo_check_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_check_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_check_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Activeeventkey", StringUtil.RTrim( Ddo_check_momento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_MOMENTO_Selectedvalue_get", StringUtil.RTrim( Ddo_check_momento_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Activeeventkey", StringUtil.RTrim( Ddo_check_qdonaocmp_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_QDONAOCMP_Selectedvalue_get", StringUtil.RTrim( Ddo_check_qdonaocmp_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WENS2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTNS2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcheck.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWCheck" ;
      }

      public override String GetPgmdesc( )
      {
         return " Controle" ;
      }

      protected void WBNS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_NS2( true) ;
         }
         else
         {
            wb_table1_2_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(104, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(105, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheck.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcheck_nome_Internalname, StringUtil.RTrim( AV39TFCheck_Nome), StringUtil.RTrim( context.localUtil.Format( AV39TFCheck_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcheck_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfcheck_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCheck.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcheck_nome_sel_Internalname, StringUtil.RTrim( AV40TFCheck_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV40TFCheck_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcheck_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcheck_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCheck.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECK_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_check_nometitlecontrolidtoreplace_Internalname, AV41ddo_Check_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_check_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheck.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECK_MOMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_check_momentotitlecontrolidtoreplace_Internalname, AV49ddo_Check_MomentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_check_momentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheck.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECK_QDONAOCMPContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Internalname, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheck.htm");
         }
         wbLoad = true;
      }

      protected void STARTNS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Controle", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPNS0( ) ;
      }

      protected void WSNS2( )
      {
         STARTNS2( ) ;
         EVTNS2( ) ;
      }

      protected void EVTNS2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11NS2 */
                              E11NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12NS2 */
                              E12NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECK_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13NS2 */
                              E13NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECK_MOMENTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14NS2 */
                              E14NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECK_QDONAOCMP.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15NS2 */
                              E15NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16NS2 */
                              E16NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17NS2 */
                              E17NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18NS2 */
                              E18NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19NS2 */
                              E19NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20NS2 */
                              E20NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21NS2 */
                              E21NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22NS2 */
                              E22NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23NS2 */
                              E23NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24NS2 */
                              E24NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25NS2 */
                              E25NS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_92_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
                              SubsflControlProps_922( ) ;
                              AV29Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV91Update_GXI : context.convertURL( context.PathToRelativeUrl( AV29Update))));
                              AV30Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)) ? AV92Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV30Delete))));
                              AV57Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)) ? AV93Display_GXI : context.convertURL( context.PathToRelativeUrl( AV57Display))));
                              A1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", "."));
                              A1841Check_Nome = StringUtil.Upper( cgiGet( edtCheck_Nome_Internalname));
                              cmbCheck_Momento.Name = cmbCheck_Momento_Internalname;
                              cmbCheck_Momento.CurrentValue = cgiGet( cmbCheck_Momento_Internalname);
                              A1843Check_Momento = (short)(NumberUtil.Val( cgiGet( cmbCheck_Momento_Internalname), "."));
                              cmbCheck_QdoNaoCmp.Name = cmbCheck_QdoNaoCmp_Internalname;
                              cmbCheck_QdoNaoCmp.CurrentValue = cgiGet( cmbCheck_QdoNaoCmp_Internalname);
                              A1856Check_QdoNaoCmp = (short)(NumberUtil.Val( cgiGet( cmbCheck_QdoNaoCmp_Internalname), "."));
                              n1856Check_QdoNaoCmp = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26NS2 */
                                    E26NS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27NS2 */
                                    E27NS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28NS2 */
                                    E28NS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Check_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCHECK_NOME1"), AV18Check_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Check_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCHECK_NOME2"), AV22Check_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Check_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCHECK_NOME3"), AV26Check_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcheck_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECK_NOME"), AV39TFCheck_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcheck_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECK_NOME_SEL"), AV40TFCheck_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WENS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PANS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CHECK_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CHECK_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CHECK_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CHECK_MOMENTO_" + sGXsfl_92_idx;
            cmbCheck_Momento.Name = GXCCtl;
            cmbCheck_Momento.WebTags = "";
            cmbCheck_Momento.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            cmbCheck_Momento.addItem("1", "Ao Iniciar uma Captura", 0);
            cmbCheck_Momento.addItem("2", "Ao Terminar uma Captura", 0);
            cmbCheck_Momento.addItem("3", "Ao Iniciar um Cancelamento", 0);
            cmbCheck_Momento.addItem("4", "Ao Terminar um Cancelamento", 0);
            cmbCheck_Momento.addItem("5", "Ao Iniciar uma Rejei��o", 0);
            cmbCheck_Momento.addItem("6", "Ao Terminar uma Rejei��o", 0);
            cmbCheck_Momento.addItem("7", "Ao Enviar para o Backlog", 0);
            cmbCheck_Momento.addItem("8", "Ao Sair do Backlog", 0);
            cmbCheck_Momento.addItem("9", "Ao Iniciar uma An�lise", 0);
            cmbCheck_Momento.addItem("10", "Ao Terminar uma An�lise", 0);
            cmbCheck_Momento.addItem("11", "Ao Iniciar uma Execu��o", 0);
            cmbCheck_Momento.addItem("12", "Ao Terminar uma Execu��o", 0);
            cmbCheck_Momento.addItem("13", "Ao Acatar uma Diverg�ncia", 0);
            cmbCheck_Momento.addItem("14", "Ao Homologar uma demanda", 0);
            cmbCheck_Momento.addItem("15", "Ao Liquidar uma demanda", 0);
            cmbCheck_Momento.addItem("16", "Ao dar o Aceite de uma Demanda", 0);
            if ( cmbCheck_Momento.ItemCount > 0 )
            {
               A1843Check_Momento = (short)(NumberUtil.Val( cmbCheck_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0))), "."));
            }
            GXCCtl = "CHECK_QDONAOCMP_" + sGXsfl_92_idx;
            cmbCheck_QdoNaoCmp.Name = GXCCtl;
            cmbCheck_QdoNaoCmp.WebTags = "";
            cmbCheck_QdoNaoCmp.addItem("0", "N�o proseguir", 0);
            cmbCheck_QdoNaoCmp.addItem("1", "Proseguir", 0);
            if ( cmbCheck_QdoNaoCmp.ItemCount > 0 )
            {
               A1856Check_QdoNaoCmp = (short)(NumberUtil.Val( cmbCheck_QdoNaoCmp.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0))), "."));
               n1856Check_QdoNaoCmp = false;
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_922( ) ;
         while ( nGXsfl_92_idx <= nRC_GXsfl_92 )
         {
            sendrow_922( ) ;
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18Check_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22Check_Nome2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       short AV25DynamicFiltersOperator3 ,
                                       String AV26Check_Nome3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       String AV39TFCheck_Nome ,
                                       String AV40TFCheck_Nome_Sel ,
                                       short AV32ManageFiltersExecutionStep ,
                                       String AV41ddo_Check_NomeTitleControlIdToReplace ,
                                       String AV49ddo_Check_MomentoTitleControlIdToReplace ,
                                       String AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace ,
                                       int AV15Check_AreaTrabalhoCod ,
                                       IGxCollection AV48TFCheck_Momento_Sels ,
                                       IGxCollection AV71TFCheck_QdoNaoCmp_Sels ,
                                       String AV94Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving ,
                                       int A1839Check_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFNS2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CHECK_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CHECK_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1841Check_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "CHECK_NOME", StringUtil.RTrim( A1841Check_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_CHECK_MOMENTO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1843Check_Momento), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CHECK_MOMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1843Check_Momento), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CHECK_QDONAOCMP", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1856Check_QdoNaoCmp), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CHECK_QDONAOCMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1856Check_QdoNaoCmp), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFNS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV94Pgmname = "WWCheck";
         context.Gx_err = 0;
      }

      protected void RFNS2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 92;
         /* Execute user event: E27NS2 */
         E27NS2 ();
         nGXsfl_92_idx = 1;
         sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
         SubsflControlProps_922( ) ;
         nGXsfl_92_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_922( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1843Check_Momento ,
                                                 AV89WWCheckDS_15_Tfcheck_momento_sels ,
                                                 A1856Check_QdoNaoCmp ,
                                                 AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels ,
                                                 AV76WWCheckDS_2_Dynamicfiltersselector1 ,
                                                 AV77WWCheckDS_3_Dynamicfiltersoperator1 ,
                                                 AV78WWCheckDS_4_Check_nome1 ,
                                                 AV79WWCheckDS_5_Dynamicfiltersenabled2 ,
                                                 AV80WWCheckDS_6_Dynamicfiltersselector2 ,
                                                 AV81WWCheckDS_7_Dynamicfiltersoperator2 ,
                                                 AV82WWCheckDS_8_Check_nome2 ,
                                                 AV83WWCheckDS_9_Dynamicfiltersenabled3 ,
                                                 AV84WWCheckDS_10_Dynamicfiltersselector3 ,
                                                 AV85WWCheckDS_11_Dynamicfiltersoperator3 ,
                                                 AV86WWCheckDS_12_Check_nome3 ,
                                                 AV88WWCheckDS_14_Tfcheck_nome_sel ,
                                                 AV87WWCheckDS_13_Tfcheck_nome ,
                                                 AV89WWCheckDS_15_Tfcheck_momento_sels.Count ,
                                                 AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels.Count ,
                                                 A1841Check_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1840Check_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV78WWCheckDS_4_Check_nome1 = StringUtil.PadR( StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1), 50, "%");
            lV78WWCheckDS_4_Check_nome1 = StringUtil.PadR( StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1), 50, "%");
            lV82WWCheckDS_8_Check_nome2 = StringUtil.PadR( StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2), 50, "%");
            lV82WWCheckDS_8_Check_nome2 = StringUtil.PadR( StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2), 50, "%");
            lV86WWCheckDS_12_Check_nome3 = StringUtil.PadR( StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3), 50, "%");
            lV86WWCheckDS_12_Check_nome3 = StringUtil.PadR( StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3), 50, "%");
            lV87WWCheckDS_13_Tfcheck_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWCheckDS_13_Tfcheck_nome), 50, "%");
            /* Using cursor H00NS2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV78WWCheckDS_4_Check_nome1, lV78WWCheckDS_4_Check_nome1, lV82WWCheckDS_8_Check_nome2, lV82WWCheckDS_8_Check_nome2, lV86WWCheckDS_12_Check_nome3, lV86WWCheckDS_12_Check_nome3, lV87WWCheckDS_13_Tfcheck_nome, AV88WWCheckDS_14_Tfcheck_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_92_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1840Check_AreaTrabalhoCod = H00NS2_A1840Check_AreaTrabalhoCod[0];
               A1856Check_QdoNaoCmp = H00NS2_A1856Check_QdoNaoCmp[0];
               n1856Check_QdoNaoCmp = H00NS2_n1856Check_QdoNaoCmp[0];
               A1843Check_Momento = H00NS2_A1843Check_Momento[0];
               A1841Check_Nome = H00NS2_A1841Check_Nome[0];
               A1839Check_Codigo = H00NS2_A1839Check_Codigo[0];
               /* Execute user event: E28NS2 */
               E28NS2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 92;
            WBNS0( ) ;
         }
         nGXsfl_92_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV75WWCheckDS_1_Check_areatrabalhocod = AV15Check_AreaTrabalhoCod;
         AV76WWCheckDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV77WWCheckDS_3_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV78WWCheckDS_4_Check_nome1 = AV18Check_Nome1;
         AV79WWCheckDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV80WWCheckDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV81WWCheckDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV82WWCheckDS_8_Check_nome2 = AV22Check_Nome2;
         AV83WWCheckDS_9_Dynamicfiltersenabled3 = AV23DynamicFiltersEnabled3;
         AV84WWCheckDS_10_Dynamicfiltersselector3 = AV24DynamicFiltersSelector3;
         AV85WWCheckDS_11_Dynamicfiltersoperator3 = AV25DynamicFiltersOperator3;
         AV86WWCheckDS_12_Check_nome3 = AV26Check_Nome3;
         AV87WWCheckDS_13_Tfcheck_nome = AV39TFCheck_Nome;
         AV88WWCheckDS_14_Tfcheck_nome_sel = AV40TFCheck_Nome_Sel;
         AV89WWCheckDS_15_Tfcheck_momento_sels = AV48TFCheck_Momento_Sels;
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV71TFCheck_QdoNaoCmp_Sels;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1843Check_Momento ,
                                              AV89WWCheckDS_15_Tfcheck_momento_sels ,
                                              A1856Check_QdoNaoCmp ,
                                              AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels ,
                                              AV76WWCheckDS_2_Dynamicfiltersselector1 ,
                                              AV77WWCheckDS_3_Dynamicfiltersoperator1 ,
                                              AV78WWCheckDS_4_Check_nome1 ,
                                              AV79WWCheckDS_5_Dynamicfiltersenabled2 ,
                                              AV80WWCheckDS_6_Dynamicfiltersselector2 ,
                                              AV81WWCheckDS_7_Dynamicfiltersoperator2 ,
                                              AV82WWCheckDS_8_Check_nome2 ,
                                              AV83WWCheckDS_9_Dynamicfiltersenabled3 ,
                                              AV84WWCheckDS_10_Dynamicfiltersselector3 ,
                                              AV85WWCheckDS_11_Dynamicfiltersoperator3 ,
                                              AV86WWCheckDS_12_Check_nome3 ,
                                              AV88WWCheckDS_14_Tfcheck_nome_sel ,
                                              AV87WWCheckDS_13_Tfcheck_nome ,
                                              AV89WWCheckDS_15_Tfcheck_momento_sels.Count ,
                                              AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels.Count ,
                                              A1841Check_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1840Check_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV78WWCheckDS_4_Check_nome1 = StringUtil.PadR( StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1), 50, "%");
         lV78WWCheckDS_4_Check_nome1 = StringUtil.PadR( StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1), 50, "%");
         lV82WWCheckDS_8_Check_nome2 = StringUtil.PadR( StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2), 50, "%");
         lV82WWCheckDS_8_Check_nome2 = StringUtil.PadR( StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2), 50, "%");
         lV86WWCheckDS_12_Check_nome3 = StringUtil.PadR( StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3), 50, "%");
         lV86WWCheckDS_12_Check_nome3 = StringUtil.PadR( StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3), 50, "%");
         lV87WWCheckDS_13_Tfcheck_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWCheckDS_13_Tfcheck_nome), 50, "%");
         /* Using cursor H00NS3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV78WWCheckDS_4_Check_nome1, lV78WWCheckDS_4_Check_nome1, lV82WWCheckDS_8_Check_nome2, lV82WWCheckDS_8_Check_nome2, lV86WWCheckDS_12_Check_nome3, lV86WWCheckDS_12_Check_nome3, lV87WWCheckDS_13_Tfcheck_nome, AV88WWCheckDS_14_Tfcheck_nome_sel});
         GRID_nRecordCount = H00NS3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV75WWCheckDS_1_Check_areatrabalhocod = AV15Check_AreaTrabalhoCod;
         AV76WWCheckDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV77WWCheckDS_3_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV78WWCheckDS_4_Check_nome1 = AV18Check_Nome1;
         AV79WWCheckDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV80WWCheckDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV81WWCheckDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV82WWCheckDS_8_Check_nome2 = AV22Check_Nome2;
         AV83WWCheckDS_9_Dynamicfiltersenabled3 = AV23DynamicFiltersEnabled3;
         AV84WWCheckDS_10_Dynamicfiltersselector3 = AV24DynamicFiltersSelector3;
         AV85WWCheckDS_11_Dynamicfiltersoperator3 = AV25DynamicFiltersOperator3;
         AV86WWCheckDS_12_Check_nome3 = AV26Check_Nome3;
         AV87WWCheckDS_13_Tfcheck_nome = AV39TFCheck_Nome;
         AV88WWCheckDS_14_Tfcheck_nome_sel = AV40TFCheck_Nome_Sel;
         AV89WWCheckDS_15_Tfcheck_momento_sels = AV48TFCheck_Momento_Sels;
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV71TFCheck_QdoNaoCmp_Sels;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV75WWCheckDS_1_Check_areatrabalhocod = AV15Check_AreaTrabalhoCod;
         AV76WWCheckDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV77WWCheckDS_3_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV78WWCheckDS_4_Check_nome1 = AV18Check_Nome1;
         AV79WWCheckDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV80WWCheckDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV81WWCheckDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV82WWCheckDS_8_Check_nome2 = AV22Check_Nome2;
         AV83WWCheckDS_9_Dynamicfiltersenabled3 = AV23DynamicFiltersEnabled3;
         AV84WWCheckDS_10_Dynamicfiltersselector3 = AV24DynamicFiltersSelector3;
         AV85WWCheckDS_11_Dynamicfiltersoperator3 = AV25DynamicFiltersOperator3;
         AV86WWCheckDS_12_Check_nome3 = AV26Check_Nome3;
         AV87WWCheckDS_13_Tfcheck_nome = AV39TFCheck_Nome;
         AV88WWCheckDS_14_Tfcheck_nome_sel = AV40TFCheck_Nome_Sel;
         AV89WWCheckDS_15_Tfcheck_momento_sels = AV48TFCheck_Momento_Sels;
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV71TFCheck_QdoNaoCmp_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV75WWCheckDS_1_Check_areatrabalhocod = AV15Check_AreaTrabalhoCod;
         AV76WWCheckDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV77WWCheckDS_3_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV78WWCheckDS_4_Check_nome1 = AV18Check_Nome1;
         AV79WWCheckDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV80WWCheckDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV81WWCheckDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV82WWCheckDS_8_Check_nome2 = AV22Check_Nome2;
         AV83WWCheckDS_9_Dynamicfiltersenabled3 = AV23DynamicFiltersEnabled3;
         AV84WWCheckDS_10_Dynamicfiltersselector3 = AV24DynamicFiltersSelector3;
         AV85WWCheckDS_11_Dynamicfiltersoperator3 = AV25DynamicFiltersOperator3;
         AV86WWCheckDS_12_Check_nome3 = AV26Check_Nome3;
         AV87WWCheckDS_13_Tfcheck_nome = AV39TFCheck_Nome;
         AV88WWCheckDS_14_Tfcheck_nome_sel = AV40TFCheck_Nome_Sel;
         AV89WWCheckDS_15_Tfcheck_momento_sels = AV48TFCheck_Momento_Sels;
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV71TFCheck_QdoNaoCmp_Sels;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV75WWCheckDS_1_Check_areatrabalhocod = AV15Check_AreaTrabalhoCod;
         AV76WWCheckDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV77WWCheckDS_3_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV78WWCheckDS_4_Check_nome1 = AV18Check_Nome1;
         AV79WWCheckDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV80WWCheckDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV81WWCheckDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV82WWCheckDS_8_Check_nome2 = AV22Check_Nome2;
         AV83WWCheckDS_9_Dynamicfiltersenabled3 = AV23DynamicFiltersEnabled3;
         AV84WWCheckDS_10_Dynamicfiltersselector3 = AV24DynamicFiltersSelector3;
         AV85WWCheckDS_11_Dynamicfiltersoperator3 = AV25DynamicFiltersOperator3;
         AV86WWCheckDS_12_Check_nome3 = AV26Check_Nome3;
         AV87WWCheckDS_13_Tfcheck_nome = AV39TFCheck_Nome;
         AV88WWCheckDS_14_Tfcheck_nome_sel = AV40TFCheck_Nome_Sel;
         AV89WWCheckDS_15_Tfcheck_momento_sels = AV48TFCheck_Momento_Sels;
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV71TFCheck_QdoNaoCmp_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV75WWCheckDS_1_Check_areatrabalhocod = AV15Check_AreaTrabalhoCod;
         AV76WWCheckDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV77WWCheckDS_3_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV78WWCheckDS_4_Check_nome1 = AV18Check_Nome1;
         AV79WWCheckDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV80WWCheckDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV81WWCheckDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV82WWCheckDS_8_Check_nome2 = AV22Check_Nome2;
         AV83WWCheckDS_9_Dynamicfiltersenabled3 = AV23DynamicFiltersEnabled3;
         AV84WWCheckDS_10_Dynamicfiltersselector3 = AV24DynamicFiltersSelector3;
         AV85WWCheckDS_11_Dynamicfiltersoperator3 = AV25DynamicFiltersOperator3;
         AV86WWCheckDS_12_Check_nome3 = AV26Check_Nome3;
         AV87WWCheckDS_13_Tfcheck_nome = AV39TFCheck_Nome;
         AV88WWCheckDS_14_Tfcheck_nome_sel = AV40TFCheck_Nome_Sel;
         AV89WWCheckDS_15_Tfcheck_momento_sels = AV48TFCheck_Momento_Sels;
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV71TFCheck_QdoNaoCmp_Sels;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPNS0( )
      {
         /* Before Start, stand alone formulas. */
         AV94Pgmname = "WWCheck";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26NS2 */
         E26NS2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV36ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV53DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECK_NOMETITLEFILTERDATA"), AV38Check_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECK_MOMENTOTITLEFILTERDATA"), AV46Check_MomentoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECK_QDONAOCMPTITLEFILTERDATA"), AV69Check_QdoNaoCmpTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCheck_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCheck_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCHECK_AREATRABALHOCOD");
               GX_FocusControl = edtavCheck_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15Check_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Check_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV15Check_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavCheck_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Check_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18Check_Nome1 = StringUtil.Upper( cgiGet( edtavCheck_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Check_Nome1", AV18Check_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22Check_Nome2 = StringUtil.Upper( cgiGet( edtavCheck_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Check_Nome2", AV22Check_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            AV26Check_Nome3 = StringUtil.Upper( cgiGet( edtavCheck_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Check_Nome3", AV26Check_Nome3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV32ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            }
            AV39TFCheck_Nome = StringUtil.Upper( cgiGet( edtavTfcheck_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCheck_Nome", AV39TFCheck_Nome);
            AV40TFCheck_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcheck_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCheck_Nome_Sel", AV40TFCheck_Nome_Sel);
            AV41ddo_Check_NomeTitleControlIdToReplace = cgiGet( edtavDdo_check_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Check_NomeTitleControlIdToReplace", AV41ddo_Check_NomeTitleControlIdToReplace);
            AV49ddo_Check_MomentoTitleControlIdToReplace = cgiGet( edtavDdo_check_momentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Check_MomentoTitleControlIdToReplace", AV49ddo_Check_MomentoTitleControlIdToReplace);
            AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace = cgiGet( edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace", AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_92 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_92"), ",", "."));
            AV55GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV56GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_check_nome_Caption = cgiGet( "DDO_CHECK_NOME_Caption");
            Ddo_check_nome_Tooltip = cgiGet( "DDO_CHECK_NOME_Tooltip");
            Ddo_check_nome_Cls = cgiGet( "DDO_CHECK_NOME_Cls");
            Ddo_check_nome_Filteredtext_set = cgiGet( "DDO_CHECK_NOME_Filteredtext_set");
            Ddo_check_nome_Selectedvalue_set = cgiGet( "DDO_CHECK_NOME_Selectedvalue_set");
            Ddo_check_nome_Dropdownoptionstype = cgiGet( "DDO_CHECK_NOME_Dropdownoptionstype");
            Ddo_check_nome_Titlecontrolidtoreplace = cgiGet( "DDO_CHECK_NOME_Titlecontrolidtoreplace");
            Ddo_check_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_NOME_Includesortasc"));
            Ddo_check_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_NOME_Includesortdsc"));
            Ddo_check_nome_Sortedstatus = cgiGet( "DDO_CHECK_NOME_Sortedstatus");
            Ddo_check_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECK_NOME_Includefilter"));
            Ddo_check_nome_Filtertype = cgiGet( "DDO_CHECK_NOME_Filtertype");
            Ddo_check_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CHECK_NOME_Filterisrange"));
            Ddo_check_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECK_NOME_Includedatalist"));
            Ddo_check_nome_Datalisttype = cgiGet( "DDO_CHECK_NOME_Datalisttype");
            Ddo_check_nome_Datalistproc = cgiGet( "DDO_CHECK_NOME_Datalistproc");
            Ddo_check_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CHECK_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_check_nome_Sortasc = cgiGet( "DDO_CHECK_NOME_Sortasc");
            Ddo_check_nome_Sortdsc = cgiGet( "DDO_CHECK_NOME_Sortdsc");
            Ddo_check_nome_Loadingdata = cgiGet( "DDO_CHECK_NOME_Loadingdata");
            Ddo_check_nome_Cleanfilter = cgiGet( "DDO_CHECK_NOME_Cleanfilter");
            Ddo_check_nome_Noresultsfound = cgiGet( "DDO_CHECK_NOME_Noresultsfound");
            Ddo_check_nome_Searchbuttontext = cgiGet( "DDO_CHECK_NOME_Searchbuttontext");
            Ddo_check_momento_Caption = cgiGet( "DDO_CHECK_MOMENTO_Caption");
            Ddo_check_momento_Tooltip = cgiGet( "DDO_CHECK_MOMENTO_Tooltip");
            Ddo_check_momento_Cls = cgiGet( "DDO_CHECK_MOMENTO_Cls");
            Ddo_check_momento_Selectedvalue_set = cgiGet( "DDO_CHECK_MOMENTO_Selectedvalue_set");
            Ddo_check_momento_Dropdownoptionstype = cgiGet( "DDO_CHECK_MOMENTO_Dropdownoptionstype");
            Ddo_check_momento_Titlecontrolidtoreplace = cgiGet( "DDO_CHECK_MOMENTO_Titlecontrolidtoreplace");
            Ddo_check_momento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_MOMENTO_Includesortasc"));
            Ddo_check_momento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_MOMENTO_Includesortdsc"));
            Ddo_check_momento_Sortedstatus = cgiGet( "DDO_CHECK_MOMENTO_Sortedstatus");
            Ddo_check_momento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECK_MOMENTO_Includefilter"));
            Ddo_check_momento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECK_MOMENTO_Includedatalist"));
            Ddo_check_momento_Datalisttype = cgiGet( "DDO_CHECK_MOMENTO_Datalisttype");
            Ddo_check_momento_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CHECK_MOMENTO_Allowmultipleselection"));
            Ddo_check_momento_Datalistfixedvalues = cgiGet( "DDO_CHECK_MOMENTO_Datalistfixedvalues");
            Ddo_check_momento_Sortasc = cgiGet( "DDO_CHECK_MOMENTO_Sortasc");
            Ddo_check_momento_Sortdsc = cgiGet( "DDO_CHECK_MOMENTO_Sortdsc");
            Ddo_check_momento_Cleanfilter = cgiGet( "DDO_CHECK_MOMENTO_Cleanfilter");
            Ddo_check_momento_Searchbuttontext = cgiGet( "DDO_CHECK_MOMENTO_Searchbuttontext");
            Ddo_check_qdonaocmp_Caption = cgiGet( "DDO_CHECK_QDONAOCMP_Caption");
            Ddo_check_qdonaocmp_Tooltip = cgiGet( "DDO_CHECK_QDONAOCMP_Tooltip");
            Ddo_check_qdonaocmp_Cls = cgiGet( "DDO_CHECK_QDONAOCMP_Cls");
            Ddo_check_qdonaocmp_Selectedvalue_set = cgiGet( "DDO_CHECK_QDONAOCMP_Selectedvalue_set");
            Ddo_check_qdonaocmp_Dropdownoptionstype = cgiGet( "DDO_CHECK_QDONAOCMP_Dropdownoptionstype");
            Ddo_check_qdonaocmp_Titlecontrolidtoreplace = cgiGet( "DDO_CHECK_QDONAOCMP_Titlecontrolidtoreplace");
            Ddo_check_qdonaocmp_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_QDONAOCMP_Includesortasc"));
            Ddo_check_qdonaocmp_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_QDONAOCMP_Includesortdsc"));
            Ddo_check_qdonaocmp_Sortedstatus = cgiGet( "DDO_CHECK_QDONAOCMP_Sortedstatus");
            Ddo_check_qdonaocmp_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECK_QDONAOCMP_Includefilter"));
            Ddo_check_qdonaocmp_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECK_QDONAOCMP_Includedatalist"));
            Ddo_check_qdonaocmp_Datalisttype = cgiGet( "DDO_CHECK_QDONAOCMP_Datalisttype");
            Ddo_check_qdonaocmp_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CHECK_QDONAOCMP_Allowmultipleselection"));
            Ddo_check_qdonaocmp_Datalistfixedvalues = cgiGet( "DDO_CHECK_QDONAOCMP_Datalistfixedvalues");
            Ddo_check_qdonaocmp_Sortasc = cgiGet( "DDO_CHECK_QDONAOCMP_Sortasc");
            Ddo_check_qdonaocmp_Sortdsc = cgiGet( "DDO_CHECK_QDONAOCMP_Sortdsc");
            Ddo_check_qdonaocmp_Cleanfilter = cgiGet( "DDO_CHECK_QDONAOCMP_Cleanfilter");
            Ddo_check_qdonaocmp_Searchbuttontext = cgiGet( "DDO_CHECK_QDONAOCMP_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_check_nome_Activeeventkey = cgiGet( "DDO_CHECK_NOME_Activeeventkey");
            Ddo_check_nome_Filteredtext_get = cgiGet( "DDO_CHECK_NOME_Filteredtext_get");
            Ddo_check_nome_Selectedvalue_get = cgiGet( "DDO_CHECK_NOME_Selectedvalue_get");
            Ddo_check_momento_Activeeventkey = cgiGet( "DDO_CHECK_MOMENTO_Activeeventkey");
            Ddo_check_momento_Selectedvalue_get = cgiGet( "DDO_CHECK_MOMENTO_Selectedvalue_get");
            Ddo_check_qdonaocmp_Activeeventkey = cgiGet( "DDO_CHECK_QDONAOCMP_Activeeventkey");
            Ddo_check_qdonaocmp_Selectedvalue_get = cgiGet( "DDO_CHECK_QDONAOCMP_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCHECK_NOME1"), AV18Check_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCHECK_NOME2"), AV22Check_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCHECK_NOME3"), AV26Check_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECK_NOME"), AV39TFCheck_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECK_NOME_SEL"), AV40TFCheck_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26NS2 */
         E26NS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26NS2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CHECK_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CHECK_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersSelector3 = "CHECK_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcheck_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcheck_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcheck_nome_Visible), 5, 0)));
         edtavTfcheck_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcheck_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcheck_nome_sel_Visible), 5, 0)));
         Ddo_check_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Check_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "TitleControlIdToReplace", Ddo_check_nome_Titlecontrolidtoreplace);
         AV41ddo_Check_NomeTitleControlIdToReplace = Ddo_check_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Check_NomeTitleControlIdToReplace", AV41ddo_Check_NomeTitleControlIdToReplace);
         edtavDdo_check_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_check_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_check_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_check_momento_Titlecontrolidtoreplace = subGrid_Internalname+"_Check_Momento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_momento_Internalname, "TitleControlIdToReplace", Ddo_check_momento_Titlecontrolidtoreplace);
         AV49ddo_Check_MomentoTitleControlIdToReplace = Ddo_check_momento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Check_MomentoTitleControlIdToReplace", AV49ddo_Check_MomentoTitleControlIdToReplace);
         edtavDdo_check_momentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_check_momentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_check_momentotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_check_qdonaocmp_Titlecontrolidtoreplace = subGrid_Internalname+"_Check_QdoNaoCmp";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_qdonaocmp_Internalname, "TitleControlIdToReplace", Ddo_check_qdonaocmp_Titlecontrolidtoreplace);
         AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace = Ddo_check_qdonaocmp_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace", AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace);
         edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Controle";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "de Trabalho", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Momento", 0);
         cmbavOrderedby.addItem("4", "Se n�o cumpre", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV53DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV53DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27NS2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV38Check_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Check_MomentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Check_QdoNaoCmpTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV32ManageFiltersExecutionStep == 1 )
         {
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV32ManageFiltersExecutionStep == 2 )
         {
            AV32ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCheck_Nome_Titleformat = 2;
         edtCheck_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV41ddo_Check_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Nome_Internalname, "Title", edtCheck_Nome_Title);
         cmbCheck_Momento_Titleformat = 2;
         cmbCheck_Momento.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Momento", AV49ddo_Check_MomentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Title", cmbCheck_Momento.Title.Text);
         cmbCheck_QdoNaoCmp_Titleformat = 2;
         cmbCheck_QdoNaoCmp.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Se n�o cumpre", AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_QdoNaoCmp_Internalname, "Title", cmbCheck_QdoNaoCmp.Title.Text);
         AV55GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridCurrentPage), 10, 0)));
         AV56GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56GridPageCount), 10, 0)));
         AV75WWCheckDS_1_Check_areatrabalhocod = AV15Check_AreaTrabalhoCod;
         AV76WWCheckDS_2_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV77WWCheckDS_3_Dynamicfiltersoperator1 = AV17DynamicFiltersOperator1;
         AV78WWCheckDS_4_Check_nome1 = AV18Check_Nome1;
         AV79WWCheckDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV80WWCheckDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV81WWCheckDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV82WWCheckDS_8_Check_nome2 = AV22Check_Nome2;
         AV83WWCheckDS_9_Dynamicfiltersenabled3 = AV23DynamicFiltersEnabled3;
         AV84WWCheckDS_10_Dynamicfiltersselector3 = AV24DynamicFiltersSelector3;
         AV85WWCheckDS_11_Dynamicfiltersoperator3 = AV25DynamicFiltersOperator3;
         AV86WWCheckDS_12_Check_nome3 = AV26Check_Nome3;
         AV87WWCheckDS_13_Tfcheck_nome = AV39TFCheck_Nome;
         AV88WWCheckDS_14_Tfcheck_nome_sel = AV40TFCheck_Nome_Sel;
         AV89WWCheckDS_15_Tfcheck_momento_sels = AV48TFCheck_Momento_Sels;
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = AV71TFCheck_QdoNaoCmp_Sels;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38Check_NomeTitleFilterData", AV38Check_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Check_MomentoTitleFilterData", AV46Check_MomentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69Check_QdoNaoCmpTitleFilterData", AV69Check_QdoNaoCmpTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36ManageFiltersData", AV36ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12NS2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV54PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV54PageToGo) ;
         }
      }

      protected void E13NS2( )
      {
         /* Ddo_check_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_check_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "SortedStatus", Ddo_check_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "SortedStatus", Ddo_check_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFCheck_Nome = Ddo_check_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCheck_Nome", AV39TFCheck_Nome);
            AV40TFCheck_Nome_Sel = Ddo_check_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCheck_Nome_Sel", AV40TFCheck_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14NS2( )
      {
         /* Ddo_check_momento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_check_momento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_momento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_momento_Internalname, "SortedStatus", Ddo_check_momento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_momento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_momento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_momento_Internalname, "SortedStatus", Ddo_check_momento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_momento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFCheck_Momento_SelsJson = Ddo_check_momento_Selectedvalue_get;
            AV48TFCheck_Momento_Sels.FromJSonString(StringUtil.StringReplace( AV47TFCheck_Momento_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFCheck_Momento_Sels", AV48TFCheck_Momento_Sels);
      }

      protected void E15NS2( )
      {
         /* Ddo_check_qdonaocmp_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_check_qdonaocmp_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_qdonaocmp_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_qdonaocmp_Internalname, "SortedStatus", Ddo_check_qdonaocmp_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_qdonaocmp_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_qdonaocmp_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_qdonaocmp_Internalname, "SortedStatus", Ddo_check_qdonaocmp_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_qdonaocmp_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFCheck_QdoNaoCmp_SelsJson = Ddo_check_qdonaocmp_Selectedvalue_get;
            AV71TFCheck_QdoNaoCmp_Sels.FromJSonString(StringUtil.StringReplace( AV70TFCheck_QdoNaoCmp_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71TFCheck_QdoNaoCmp_Sels", AV71TFCheck_QdoNaoCmp_Sels);
      }

      private void E28NS2( )
      {
         /* Grid_Load Routine */
         AV29Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV29Update);
         AV91Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("check.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1839Check_Codigo);
         AV30Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV30Delete);
         AV92Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("check.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1839Check_Codigo);
         AV57Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV57Display);
         AV93Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewcheck.aspx") + "?" + UrlEncode("" +A1839Check_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 92;
         }
         sendrow_922( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_92_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(92, GridRow);
         }
      }

      protected void E16NS2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21NS2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17NS2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22NS2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23NS2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18NS2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24NS2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19NS2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Check_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Check_Nome2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26Check_Nome3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV39TFCheck_Nome, AV40TFCheck_Nome_Sel, AV32ManageFiltersExecutionStep, AV41ddo_Check_NomeTitleControlIdToReplace, AV49ddo_Check_MomentoTitleControlIdToReplace, AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace, AV15Check_AreaTrabalhoCod, AV48TFCheck_Momento_Sels, AV71TFCheck_QdoNaoCmp_Sels, AV94Pgmname, AV10GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving, A1839Check_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25NS2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11NS2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S232 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWCheckFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWCheckFilters")), new Object[] {});
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV33ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWCheckFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV33ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV94Pgmname+"GridState",  AV33ManageFiltersXml) ;
               AV10GridState.FromXml(AV33ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S222 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFCheck_Momento_Sels", AV48TFCheck_Momento_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV71TFCheck_QdoNaoCmp_Sels", AV71TFCheck_QdoNaoCmp_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E20NS2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("check.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_check_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "SortedStatus", Ddo_check_nome_Sortedstatus);
         Ddo_check_momento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_momento_Internalname, "SortedStatus", Ddo_check_momento_Sortedstatus);
         Ddo_check_qdonaocmp_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_qdonaocmp_Internalname, "SortedStatus", Ddo_check_qdonaocmp_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_check_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "SortedStatus", Ddo_check_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_check_momento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_momento_Internalname, "SortedStatus", Ddo_check_momento_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_check_qdonaocmp_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_qdonaocmp_Internalname, "SortedStatus", Ddo_check_qdonaocmp_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavCheck_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCheck_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCheck_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CHECK_NOME") == 0 )
         {
            edtavCheck_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCheck_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCheck_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavCheck_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCheck_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCheck_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CHECK_NOME") == 0 )
         {
            edtavCheck_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCheck_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCheck_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavCheck_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCheck_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCheck_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CHECK_NOME") == 0 )
         {
            edtavCheck_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCheck_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCheck_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CHECK_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22Check_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Check_Nome2", AV22Check_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "CHECK_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         AV26Check_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Check_Nome3", AV26Check_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV36ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV34ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWCheckFilters"), "");
         AV95GXV1 = 1;
         while ( AV95GXV1 <= AV34ManageFiltersItems.Count )
         {
            AV35ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV34ManageFiltersItems.Item(AV95GXV1));
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Title = AV35ManageFiltersItem.gxTpr_Title;
            AV37ManageFiltersDataItem.gxTpr_Eventkey = AV35ManageFiltersItem.gxTpr_Title;
            AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
            if ( AV36ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV95GXV1 = (int)(AV95GXV1+1);
         }
         if ( AV36ManageFiltersData.Count > 3 )
         {
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15Check_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Check_AreaTrabalhoCod), 6, 0)));
         AV39TFCheck_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCheck_Nome", AV39TFCheck_Nome);
         Ddo_check_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "FilteredText_set", Ddo_check_nome_Filteredtext_set);
         AV40TFCheck_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCheck_Nome_Sel", AV40TFCheck_Nome_Sel);
         Ddo_check_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "SelectedValue_set", Ddo_check_nome_Selectedvalue_set);
         AV48TFCheck_Momento_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_check_momento_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_momento_Internalname, "SelectedValue_set", Ddo_check_momento_Selectedvalue_set);
         AV71TFCheck_QdoNaoCmp_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_check_qdonaocmp_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_qdonaocmp_Internalname, "SelectedValue_set", Ddo_check_qdonaocmp_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "CHECK_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18Check_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Check_Nome1", AV18Check_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV94Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV94Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV31Session.Get(AV94Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV96GXV2 = 1;
         while ( AV96GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV96GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CHECK_AREATRABALHOCOD") == 0 )
            {
               AV15Check_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Check_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECK_NOME") == 0 )
            {
               AV39TFCheck_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCheck_Nome", AV39TFCheck_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFCheck_Nome)) )
               {
                  Ddo_check_nome_Filteredtext_set = AV39TFCheck_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "FilteredText_set", Ddo_check_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECK_NOME_SEL") == 0 )
            {
               AV40TFCheck_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCheck_Nome_Sel", AV40TFCheck_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCheck_Nome_Sel)) )
               {
                  Ddo_check_nome_Selectedvalue_set = AV40TFCheck_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_nome_Internalname, "SelectedValue_set", Ddo_check_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECK_MOMENTO_SEL") == 0 )
            {
               AV47TFCheck_Momento_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV48TFCheck_Momento_Sels.FromJSonString(AV47TFCheck_Momento_SelsJson);
               if ( ! ( AV48TFCheck_Momento_Sels.Count == 0 ) )
               {
                  Ddo_check_momento_Selectedvalue_set = AV47TFCheck_Momento_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_momento_Internalname, "SelectedValue_set", Ddo_check_momento_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECK_QDONAOCMP_SEL") == 0 )
            {
               AV70TFCheck_QdoNaoCmp_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV71TFCheck_QdoNaoCmp_Sels.FromJSonString(AV70TFCheck_QdoNaoCmp_SelsJson);
               if ( ! ( AV71TFCheck_QdoNaoCmp_Sels.Count == 0 ) )
               {
                  Ddo_check_qdonaocmp_Selectedvalue_set = AV70TFCheck_QdoNaoCmp_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_qdonaocmp_Internalname, "SelectedValue_set", Ddo_check_qdonaocmp_Selectedvalue_set);
               }
            }
            AV96GXV2 = (int)(AV96GXV2+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CHECK_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Check_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Check_Nome1", AV18Check_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CHECK_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Check_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Check_Nome2", AV22Check_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CHECK_NOME") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV26Check_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Check_Nome3", AV26Check_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV31Session.Get(AV94Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV15Check_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CHECK_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV15Check_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFCheck_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECK_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFCheck_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCheck_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECK_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFCheck_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV48TFCheck_Momento_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECK_MOMENTO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFCheck_Momento_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV71TFCheck_QdoNaoCmp_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECK_QDONAOCMP_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFCheck_QdoNaoCmp_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV94Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CHECK_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Check_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Check_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CHECK_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Check_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Check_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CHECK_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV26Check_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV26Check_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV94Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Check";
         AV31Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_NS2( true) ;
         }
         else
         {
            wb_table2_8_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_86_NS2( true) ;
         }
         else
         {
            wb_table3_86_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table3_86_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_NS2e( true) ;
         }
         else
         {
            wb_table1_2_NS2e( false) ;
         }
      }

      protected void wb_table3_86_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "ableGridHeaderPrompt", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_89_NS2( true) ;
         }
         else
         {
            wb_table4_89_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table4_89_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_86_NS2e( true) ;
         }
         else
         {
            wb_table3_86_NS2e( false) ;
         }
      }

      protected void wb_table4_89_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"92\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "List") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCheck_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtCheck_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCheck_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbCheck_Momento_Titleformat == 0 )
               {
                  context.SendWebValue( cmbCheck_Momento.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbCheck_Momento.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbCheck_QdoNaoCmp_Titleformat == 0 )
               {
                  context.SendWebValue( cmbCheck_QdoNaoCmp.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbCheck_QdoNaoCmp.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV57Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1841Check_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCheck_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCheck_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1843Check_Momento), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbCheck_Momento.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbCheck_Momento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1856Check_QdoNaoCmp), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbCheck_QdoNaoCmp.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbCheck_QdoNaoCmp_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 92 )
         {
            wbEnd = 0;
            nRC_GXsfl_92 = (short)(nGXsfl_92_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_89_NS2e( true) ;
         }
         else
         {
            wb_table4_89_NS2e( false) ;
         }
      }

      protected void wb_table2_8_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_NS2( true) ;
         }
         else
         {
            wb_table5_11_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_NS2( true) ;
         }
         else
         {
            wb_table6_23_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_NS2e( true) ;
         }
         else
         {
            wb_table2_8_NS2e( false) ;
         }
      }

      protected void wb_table6_23_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcheck_areatrabalhocod_Internalname, "de Trabalho", "", "", lblFiltertextcheck_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCheck_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Check_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15Check_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCheck_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_NS2( true) ;
         }
         else
         {
            wb_table7_32_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_NS2e( true) ;
         }
         else
         {
            wb_table6_23_NS2e( false) ;
         }
      }

      protected void wb_table7_32_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWCheck.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_41_NS2( true) ;
         }
         else
         {
            wb_table8_41_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table8_41_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_WWCheck.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_58_NS2( true) ;
         }
         else
         {
            wb_table9_58_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table9_58_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheck.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWCheck.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_NS2( true) ;
         }
         else
         {
            wb_table10_75_NS2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_NS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_NS2e( true) ;
         }
         else
         {
            wb_table7_32_NS2e( false) ;
         }
      }

      protected void wb_table10_75_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_WWCheck.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCheck_nome3_Internalname, StringUtil.RTrim( AV26Check_Nome3), StringUtil.RTrim( context.localUtil.Format( AV26Check_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCheck_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCheck_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_NS2e( true) ;
         }
         else
         {
            wb_table10_75_NS2e( false) ;
         }
      }

      protected void wb_table9_58_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWCheck.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCheck_nome2_Internalname, StringUtil.RTrim( AV22Check_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22Check_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCheck_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCheck_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_58_NS2e( true) ;
         }
         else
         {
            wb_table9_58_NS2e( false) ;
         }
      }

      protected void wb_table8_41_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WWCheck.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCheck_nome1_Internalname, StringUtil.RTrim( AV18Check_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Check_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCheck_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCheck_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_41_NS2e( true) ;
         }
         else
         {
            wb_table8_41_NS2e( false) ;
         }
      }

      protected void wb_table5_11_NS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblChecktitle_Internalname, "Check Lists", "", "", lblChecktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWCheck.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWCheck.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_NS2e( true) ;
         }
         else
         {
            wb_table5_11_NS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PANS2( ) ;
         WSNS2( ) ;
         WENS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311964154");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcheck.js", "?2020311964154");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_922( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_92_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_92_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_92_idx;
         edtCheck_Codigo_Internalname = "CHECK_CODIGO_"+sGXsfl_92_idx;
         edtCheck_Nome_Internalname = "CHECK_NOME_"+sGXsfl_92_idx;
         cmbCheck_Momento_Internalname = "CHECK_MOMENTO_"+sGXsfl_92_idx;
         cmbCheck_QdoNaoCmp_Internalname = "CHECK_QDONAOCMP_"+sGXsfl_92_idx;
      }

      protected void SubsflControlProps_fel_922( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_92_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_92_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_92_fel_idx;
         edtCheck_Codigo_Internalname = "CHECK_CODIGO_"+sGXsfl_92_fel_idx;
         edtCheck_Nome_Internalname = "CHECK_NOME_"+sGXsfl_92_fel_idx;
         cmbCheck_Momento_Internalname = "CHECK_MOMENTO_"+sGXsfl_92_fel_idx;
         cmbCheck_QdoNaoCmp_Internalname = "CHECK_QDONAOCMP_"+sGXsfl_92_fel_idx;
      }

      protected void sendrow_922( )
      {
         SubsflControlProps_922( ) ;
         WBNS0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_92_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_92_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_92_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV91Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV91Update_GXI : context.PathToRelativeUrl( AV29Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV92Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Delete)) ? AV92Delete_GXI : context.PathToRelativeUrl( AV30Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV57Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV57Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV93Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)) ? AV93Display_GXI : context.PathToRelativeUrl( AV57Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV57Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheck_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheck_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheck_Nome_Internalname,StringUtil.RTrim( A1841Check_Nome),StringUtil.RTrim( context.localUtil.Format( A1841Check_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheck_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECK_MOMENTO_" + sGXsfl_92_idx;
               cmbCheck_Momento.Name = GXCCtl;
               cmbCheck_Momento.WebTags = "";
               cmbCheck_Momento.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
               cmbCheck_Momento.addItem("1", "Ao Iniciar uma Captura", 0);
               cmbCheck_Momento.addItem("2", "Ao Terminar uma Captura", 0);
               cmbCheck_Momento.addItem("3", "Ao Iniciar um Cancelamento", 0);
               cmbCheck_Momento.addItem("4", "Ao Terminar um Cancelamento", 0);
               cmbCheck_Momento.addItem("5", "Ao Iniciar uma Rejei��o", 0);
               cmbCheck_Momento.addItem("6", "Ao Terminar uma Rejei��o", 0);
               cmbCheck_Momento.addItem("7", "Ao Enviar para o Backlog", 0);
               cmbCheck_Momento.addItem("8", "Ao Sair do Backlog", 0);
               cmbCheck_Momento.addItem("9", "Ao Iniciar uma An�lise", 0);
               cmbCheck_Momento.addItem("10", "Ao Terminar uma An�lise", 0);
               cmbCheck_Momento.addItem("11", "Ao Iniciar uma Execu��o", 0);
               cmbCheck_Momento.addItem("12", "Ao Terminar uma Execu��o", 0);
               cmbCheck_Momento.addItem("13", "Ao Acatar uma Diverg�ncia", 0);
               cmbCheck_Momento.addItem("14", "Ao Homologar uma demanda", 0);
               cmbCheck_Momento.addItem("15", "Ao Liquidar uma demanda", 0);
               cmbCheck_Momento.addItem("16", "Ao dar o Aceite de uma Demanda", 0);
               if ( cmbCheck_Momento.ItemCount > 0 )
               {
                  A1843Check_Momento = (short)(NumberUtil.Val( cmbCheck_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheck_Momento,(String)cmbCheck_Momento_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)),(short)1,(String)cmbCheck_Momento_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheck_Momento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Values", (String)(cmbCheck_Momento.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECK_QDONAOCMP_" + sGXsfl_92_idx;
               cmbCheck_QdoNaoCmp.Name = GXCCtl;
               cmbCheck_QdoNaoCmp.WebTags = "";
               cmbCheck_QdoNaoCmp.addItem("0", "N�o proseguir", 0);
               cmbCheck_QdoNaoCmp.addItem("1", "Proseguir", 0);
               if ( cmbCheck_QdoNaoCmp.ItemCount > 0 )
               {
                  A1856Check_QdoNaoCmp = (short)(NumberUtil.Val( cmbCheck_QdoNaoCmp.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0))), "."));
                  n1856Check_QdoNaoCmp = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheck_QdoNaoCmp,(String)cmbCheck_QdoNaoCmp_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)),(short)1,(String)cmbCheck_QdoNaoCmp_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheck_QdoNaoCmp.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_QdoNaoCmp_Internalname, "Values", (String)(cmbCheck_QdoNaoCmp.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECK_CODIGO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECK_NOME"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A1841Check_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECK_MOMENTO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A1843Check_Momento), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECK_QDONAOCMP"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A1856Check_QdoNaoCmp), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         /* End function sendrow_922 */
      }

      protected void init_default_properties( )
      {
         lblChecktitle_Internalname = "CHECKTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblFiltertextcheck_areatrabalhocod_Internalname = "FILTERTEXTCHECK_AREATRABALHOCOD";
         edtavCheck_areatrabalhocod_Internalname = "vCHECK_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavCheck_nome1_Internalname = "vCHECK_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavCheck_nome2_Internalname = "vCHECK_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavCheck_nome3_Internalname = "vCHECK_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtCheck_Codigo_Internalname = "CHECK_CODIGO";
         edtCheck_Nome_Internalname = "CHECK_NOME";
         cmbCheck_Momento_Internalname = "CHECK_MOMENTO";
         cmbCheck_QdoNaoCmp_Internalname = "CHECK_QDONAOCMP";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfcheck_nome_Internalname = "vTFCHECK_NOME";
         edtavTfcheck_nome_sel_Internalname = "vTFCHECK_NOME_SEL";
         Ddo_check_nome_Internalname = "DDO_CHECK_NOME";
         edtavDdo_check_nometitlecontrolidtoreplace_Internalname = "vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE";
         Ddo_check_momento_Internalname = "DDO_CHECK_MOMENTO";
         edtavDdo_check_momentotitlecontrolidtoreplace_Internalname = "vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE";
         Ddo_check_qdonaocmp_Internalname = "DDO_CHECK_QDONAOCMP";
         edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Internalname = "vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbCheck_QdoNaoCmp_Jsonclick = "";
         cmbCheck_Momento_Jsonclick = "";
         edtCheck_Nome_Jsonclick = "";
         edtCheck_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         edtavCheck_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavCheck_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavCheck_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavCheck_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbCheck_QdoNaoCmp_Titleformat = 0;
         cmbCheck_Momento_Titleformat = 0;
         edtCheck_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavCheck_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavCheck_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavCheck_nome1_Visible = 1;
         cmbCheck_QdoNaoCmp.Title.Text = "Se n�o cumpre";
         cmbCheck_Momento.Title.Text = "Momento";
         edtCheck_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Visible = 1;
         edtavDdo_check_momentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_check_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfcheck_nome_sel_Jsonclick = "";
         edtavTfcheck_nome_sel_Visible = 1;
         edtavTfcheck_nome_Jsonclick = "";
         edtavTfcheck_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_check_qdonaocmp_Searchbuttontext = "Filtrar Selecionados";
         Ddo_check_qdonaocmp_Cleanfilter = "Limpar pesquisa";
         Ddo_check_qdonaocmp_Sortdsc = "Ordenar de Z � A";
         Ddo_check_qdonaocmp_Sortasc = "Ordenar de A � Z";
         Ddo_check_qdonaocmp_Datalistfixedvalues = "0:N�o proseguir,1:Proseguir";
         Ddo_check_qdonaocmp_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_check_qdonaocmp_Datalisttype = "FixedValues";
         Ddo_check_qdonaocmp_Includedatalist = Convert.ToBoolean( -1);
         Ddo_check_qdonaocmp_Includefilter = Convert.ToBoolean( 0);
         Ddo_check_qdonaocmp_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_check_qdonaocmp_Includesortasc = Convert.ToBoolean( -1);
         Ddo_check_qdonaocmp_Titlecontrolidtoreplace = "";
         Ddo_check_qdonaocmp_Dropdownoptionstype = "GridTitleSettings";
         Ddo_check_qdonaocmp_Cls = "ColumnSettings";
         Ddo_check_qdonaocmp_Tooltip = "Op��es";
         Ddo_check_qdonaocmp_Caption = "";
         Ddo_check_momento_Searchbuttontext = "Filtrar Selecionados";
         Ddo_check_momento_Cleanfilter = "Limpar pesquisa";
         Ddo_check_momento_Sortdsc = "Ordenar de Z � A";
         Ddo_check_momento_Sortasc = "Ordenar de A � Z";
         Ddo_check_momento_Datalistfixedvalues = "1:Ao Iniciar uma Captura,2:Ao Terminar uma Captura,3:Ao Iniciar um Cancelamento,4:Ao Terminar um Cancelamento,5:Ao Iniciar uma Rejei��o,6:Ao Terminar uma Rejei��o,7:Ao Enviar para o Backlog,8:Ao Sair do Backlog,9:Ao Iniciar uma An�lise,10:Ao Terminar uma An�lise,11:Ao Iniciar uma Execu��o,12:Ao Terminar uma Execu��o,13:Ao Acatar uma Diverg�ncia,14:Ao Homologar uma demanda,15:Ao Liquidar uma demanda,16:Ao dar o Aceite de uma Demanda";
         Ddo_check_momento_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_check_momento_Datalisttype = "FixedValues";
         Ddo_check_momento_Includedatalist = Convert.ToBoolean( -1);
         Ddo_check_momento_Includefilter = Convert.ToBoolean( 0);
         Ddo_check_momento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_check_momento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_check_momento_Titlecontrolidtoreplace = "";
         Ddo_check_momento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_check_momento_Cls = "ColumnSettings";
         Ddo_check_momento_Tooltip = "Op��es";
         Ddo_check_momento_Caption = "";
         Ddo_check_nome_Searchbuttontext = "Pesquisar";
         Ddo_check_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_check_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_check_nome_Loadingdata = "Carregando dados...";
         Ddo_check_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_check_nome_Sortasc = "Ordenar de A � Z";
         Ddo_check_nome_Datalistupdateminimumcharacters = 0;
         Ddo_check_nome_Datalistproc = "GetWWCheckFilterData";
         Ddo_check_nome_Datalisttype = "Dynamic";
         Ddo_check_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_check_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_check_nome_Filtertype = "Character";
         Ddo_check_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_check_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_check_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_check_nome_Titlecontrolidtoreplace = "";
         Ddo_check_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_check_nome_Cls = "ColumnSettings";
         Ddo_check_nome_Tooltip = "Op��es";
         Ddo_check_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Controle";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV38Check_NomeTitleFilterData',fld:'vCHECK_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV46Check_MomentoTitleFilterData',fld:'vCHECK_MOMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV69Check_QdoNaoCmpTitleFilterData',fld:'vCHECK_QDONAOCMPTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtCheck_Nome_Titleformat',ctrl:'CHECK_NOME',prop:'Titleformat'},{av:'edtCheck_Nome_Title',ctrl:'CHECK_NOME',prop:'Title'},{av:'cmbCheck_Momento'},{av:'cmbCheck_QdoNaoCmp'},{av:'AV55GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV56GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV36ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CHECK_NOME.ONOPTIONCLICKED","{handler:'E13NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_check_nome_Activeeventkey',ctrl:'DDO_CHECK_NOME',prop:'ActiveEventKey'},{av:'Ddo_check_nome_Filteredtext_get',ctrl:'DDO_CHECK_NOME',prop:'FilteredText_get'},{av:'Ddo_check_nome_Selectedvalue_get',ctrl:'DDO_CHECK_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_check_nome_Sortedstatus',ctrl:'DDO_CHECK_NOME',prop:'SortedStatus'},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_check_momento_Sortedstatus',ctrl:'DDO_CHECK_MOMENTO',prop:'SortedStatus'},{av:'Ddo_check_qdonaocmp_Sortedstatus',ctrl:'DDO_CHECK_QDONAOCMP',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECK_MOMENTO.ONOPTIONCLICKED","{handler:'E14NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_check_momento_Activeeventkey',ctrl:'DDO_CHECK_MOMENTO',prop:'ActiveEventKey'},{av:'Ddo_check_momento_Selectedvalue_get',ctrl:'DDO_CHECK_MOMENTO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_check_momento_Sortedstatus',ctrl:'DDO_CHECK_MOMENTO',prop:'SortedStatus'},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'Ddo_check_nome_Sortedstatus',ctrl:'DDO_CHECK_NOME',prop:'SortedStatus'},{av:'Ddo_check_qdonaocmp_Sortedstatus',ctrl:'DDO_CHECK_QDONAOCMP',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECK_QDONAOCMP.ONOPTIONCLICKED","{handler:'E15NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_check_qdonaocmp_Activeeventkey',ctrl:'DDO_CHECK_QDONAOCMP',prop:'ActiveEventKey'},{av:'Ddo_check_qdonaocmp_Selectedvalue_get',ctrl:'DDO_CHECK_QDONAOCMP',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_check_qdonaocmp_Sortedstatus',ctrl:'DDO_CHECK_QDONAOCMP',prop:'SortedStatus'},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'Ddo_check_nome_Sortedstatus',ctrl:'DDO_CHECK_NOME',prop:'SortedStatus'},{av:'Ddo_check_momento_Sortedstatus',ctrl:'DDO_CHECK_MOMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28NS2',iparms:[{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV29Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV30Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV57Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21NS2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCheck_nome2_Visible',ctrl:'vCHECK_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCheck_nome3_Visible',ctrl:'vCHECK_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCheck_nome1_Visible',ctrl:'vCHECK_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22NS2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavCheck_nome1_Visible',ctrl:'vCHECK_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23NS2',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCheck_nome2_Visible',ctrl:'vCHECK_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCheck_nome3_Visible',ctrl:'vCHECK_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCheck_nome1_Visible',ctrl:'vCHECK_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24NS2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavCheck_nome2_Visible',ctrl:'vCHECK_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCheck_nome2_Visible',ctrl:'vCHECK_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCheck_nome3_Visible',ctrl:'vCHECK_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCheck_nome1_Visible',ctrl:'vCHECK_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25NS2',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavCheck_nome3_Visible',ctrl:'vCHECK_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11NS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV41ddo_Check_NomeTitleControlIdToReplace',fld:'vDDO_CHECK_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Check_MomentoTitleControlIdToReplace',fld:'vDDO_CHECK_MOMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace',fld:'vDDO_CHECK_QDONAOCMPTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15Check_AreaTrabalhoCod',fld:'vCHECK_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFCheck_Nome',fld:'vTFCHECK_NOME',pic:'@!',nv:''},{av:'Ddo_check_nome_Filteredtext_set',ctrl:'DDO_CHECK_NOME',prop:'FilteredText_set'},{av:'AV40TFCheck_Nome_Sel',fld:'vTFCHECK_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_check_nome_Selectedvalue_set',ctrl:'DDO_CHECK_NOME',prop:'SelectedValue_set'},{av:'AV48TFCheck_Momento_Sels',fld:'vTFCHECK_MOMENTO_SELS',pic:'',nv:null},{av:'Ddo_check_momento_Selectedvalue_set',ctrl:'DDO_CHECK_MOMENTO',prop:'SelectedValue_set'},{av:'AV71TFCheck_QdoNaoCmp_Sels',fld:'vTFCHECK_QDONAOCMP_SELS',pic:'',nv:null},{av:'Ddo_check_qdonaocmp_Selectedvalue_set',ctrl:'DDO_CHECK_QDONAOCMP',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Check_Nome1',fld:'vCHECK_NOME1',pic:'@!',nv:''},{av:'Ddo_check_nome_Sortedstatus',ctrl:'DDO_CHECK_NOME',prop:'SortedStatus'},{av:'Ddo_check_momento_Sortedstatus',ctrl:'DDO_CHECK_MOMENTO',prop:'SortedStatus'},{av:'Ddo_check_qdonaocmp_Sortedstatus',ctrl:'DDO_CHECK_QDONAOCMP',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Check_Nome2',fld:'vCHECK_NOME2',pic:'@!',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26Check_Nome3',fld:'vCHECK_NOME3',pic:'@!',nv:''},{av:'edtavCheck_nome1_Visible',ctrl:'vCHECK_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavCheck_nome2_Visible',ctrl:'vCHECK_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCheck_nome3_Visible',ctrl:'vCHECK_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20NS2',iparms:[{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_check_nome_Activeeventkey = "";
         Ddo_check_nome_Filteredtext_get = "";
         Ddo_check_nome_Selectedvalue_get = "";
         Ddo_check_momento_Activeeventkey = "";
         Ddo_check_momento_Selectedvalue_get = "";
         Ddo_check_qdonaocmp_Activeeventkey = "";
         Ddo_check_qdonaocmp_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Check_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22Check_Nome2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV26Check_Nome3 = "";
         AV39TFCheck_Nome = "";
         AV40TFCheck_Nome_Sel = "";
         AV41ddo_Check_NomeTitleControlIdToReplace = "";
         AV49ddo_Check_MomentoTitleControlIdToReplace = "";
         AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace = "";
         AV48TFCheck_Momento_Sels = new GxSimpleCollection();
         AV71TFCheck_QdoNaoCmp_Sels = new GxSimpleCollection();
         AV94Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV36ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV38Check_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Check_MomentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69Check_QdoNaoCmpTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_check_nome_Filteredtext_set = "";
         Ddo_check_nome_Selectedvalue_set = "";
         Ddo_check_nome_Sortedstatus = "";
         Ddo_check_momento_Selectedvalue_set = "";
         Ddo_check_momento_Sortedstatus = "";
         Ddo_check_qdonaocmp_Selectedvalue_set = "";
         Ddo_check_qdonaocmp_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Update = "";
         AV91Update_GXI = "";
         AV30Delete = "";
         AV92Delete_GXI = "";
         AV57Display = "";
         AV93Display_GXI = "";
         A1841Check_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV89WWCheckDS_15_Tfcheck_momento_sels = new GxSimpleCollection();
         AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels = new GxSimpleCollection();
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV78WWCheckDS_4_Check_nome1 = "";
         lV82WWCheckDS_8_Check_nome2 = "";
         lV86WWCheckDS_12_Check_nome3 = "";
         lV87WWCheckDS_13_Tfcheck_nome = "";
         AV76WWCheckDS_2_Dynamicfiltersselector1 = "";
         AV78WWCheckDS_4_Check_nome1 = "";
         AV80WWCheckDS_6_Dynamicfiltersselector2 = "";
         AV82WWCheckDS_8_Check_nome2 = "";
         AV84WWCheckDS_10_Dynamicfiltersselector3 = "";
         AV86WWCheckDS_12_Check_nome3 = "";
         AV88WWCheckDS_14_Tfcheck_nome_sel = "";
         AV87WWCheckDS_13_Tfcheck_nome = "";
         H00NS2_A1840Check_AreaTrabalhoCod = new int[1] ;
         H00NS2_A1856Check_QdoNaoCmp = new short[1] ;
         H00NS2_n1856Check_QdoNaoCmp = new bool[] {false} ;
         H00NS2_A1843Check_Momento = new short[1] ;
         H00NS2_A1841Check_Nome = new String[] {""} ;
         H00NS2_A1839Check_Codigo = new int[1] ;
         H00NS3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV47TFCheck_Momento_SelsJson = "";
         AV70TFCheck_QdoNaoCmp_SelsJson = "";
         GridRow = new GXWebRow();
         AV33ManageFiltersXml = "";
         GXt_char2 = "";
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV34ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV35ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV31Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextcheck_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblChecktitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcheck__default(),
            new Object[][] {
                new Object[] {
               H00NS2_A1840Check_AreaTrabalhoCod, H00NS2_A1856Check_QdoNaoCmp, H00NS2_n1856Check_QdoNaoCmp, H00NS2_A1843Check_Momento, H00NS2_A1841Check_Nome, H00NS2_A1839Check_Codigo
               }
               , new Object[] {
               H00NS3_AGRID_nRecordCount
               }
            }
         );
         AV94Pgmname = "WWCheck";
         /* GeneXus formulas. */
         AV94Pgmname = "WWCheck";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_92 ;
      private short nGXsfl_92_idx=1 ;
      private short AV13OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV25DynamicFiltersOperator3 ;
      private short AV32ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1843Check_Momento ;
      private short A1856Check_QdoNaoCmp ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_92_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV77WWCheckDS_3_Dynamicfiltersoperator1 ;
      private short AV81WWCheckDS_7_Dynamicfiltersoperator2 ;
      private short AV85WWCheckDS_11_Dynamicfiltersoperator3 ;
      private short edtCheck_Nome_Titleformat ;
      private short cmbCheck_Momento_Titleformat ;
      private short cmbCheck_QdoNaoCmp_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV15Check_AreaTrabalhoCod ;
      private int A1839Check_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_check_nome_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfcheck_nome_Visible ;
      private int edtavTfcheck_nome_sel_Visible ;
      private int edtavDdo_check_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_check_momentotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV89WWCheckDS_15_Tfcheck_momento_sels_Count ;
      private int AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A1840Check_AreaTrabalhoCod ;
      private int AV75WWCheckDS_1_Check_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int AV54PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavCheck_nome1_Visible ;
      private int edtavCheck_nome2_Visible ;
      private int edtavCheck_nome3_Visible ;
      private int AV95GXV1 ;
      private int AV96GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV55GridCurrentPage ;
      private long AV56GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_check_nome_Activeeventkey ;
      private String Ddo_check_nome_Filteredtext_get ;
      private String Ddo_check_nome_Selectedvalue_get ;
      private String Ddo_check_momento_Activeeventkey ;
      private String Ddo_check_momento_Selectedvalue_get ;
      private String Ddo_check_qdonaocmp_Activeeventkey ;
      private String Ddo_check_qdonaocmp_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_92_idx="0001" ;
      private String AV18Check_Nome1 ;
      private String AV22Check_Nome2 ;
      private String AV26Check_Nome3 ;
      private String AV39TFCheck_Nome ;
      private String AV40TFCheck_Nome_Sel ;
      private String AV94Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_check_nome_Caption ;
      private String Ddo_check_nome_Tooltip ;
      private String Ddo_check_nome_Cls ;
      private String Ddo_check_nome_Filteredtext_set ;
      private String Ddo_check_nome_Selectedvalue_set ;
      private String Ddo_check_nome_Dropdownoptionstype ;
      private String Ddo_check_nome_Titlecontrolidtoreplace ;
      private String Ddo_check_nome_Sortedstatus ;
      private String Ddo_check_nome_Filtertype ;
      private String Ddo_check_nome_Datalisttype ;
      private String Ddo_check_nome_Datalistproc ;
      private String Ddo_check_nome_Sortasc ;
      private String Ddo_check_nome_Sortdsc ;
      private String Ddo_check_nome_Loadingdata ;
      private String Ddo_check_nome_Cleanfilter ;
      private String Ddo_check_nome_Noresultsfound ;
      private String Ddo_check_nome_Searchbuttontext ;
      private String Ddo_check_momento_Caption ;
      private String Ddo_check_momento_Tooltip ;
      private String Ddo_check_momento_Cls ;
      private String Ddo_check_momento_Selectedvalue_set ;
      private String Ddo_check_momento_Dropdownoptionstype ;
      private String Ddo_check_momento_Titlecontrolidtoreplace ;
      private String Ddo_check_momento_Sortedstatus ;
      private String Ddo_check_momento_Datalisttype ;
      private String Ddo_check_momento_Datalistfixedvalues ;
      private String Ddo_check_momento_Sortasc ;
      private String Ddo_check_momento_Sortdsc ;
      private String Ddo_check_momento_Cleanfilter ;
      private String Ddo_check_momento_Searchbuttontext ;
      private String Ddo_check_qdonaocmp_Caption ;
      private String Ddo_check_qdonaocmp_Tooltip ;
      private String Ddo_check_qdonaocmp_Cls ;
      private String Ddo_check_qdonaocmp_Selectedvalue_set ;
      private String Ddo_check_qdonaocmp_Dropdownoptionstype ;
      private String Ddo_check_qdonaocmp_Titlecontrolidtoreplace ;
      private String Ddo_check_qdonaocmp_Sortedstatus ;
      private String Ddo_check_qdonaocmp_Datalisttype ;
      private String Ddo_check_qdonaocmp_Datalistfixedvalues ;
      private String Ddo_check_qdonaocmp_Sortasc ;
      private String Ddo_check_qdonaocmp_Sortdsc ;
      private String Ddo_check_qdonaocmp_Cleanfilter ;
      private String Ddo_check_qdonaocmp_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfcheck_nome_Internalname ;
      private String edtavTfcheck_nome_Jsonclick ;
      private String edtavTfcheck_nome_sel_Internalname ;
      private String edtavTfcheck_nome_sel_Jsonclick ;
      private String edtavDdo_check_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_check_momentotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_check_qdonaocmptitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtCheck_Codigo_Internalname ;
      private String A1841Check_Nome ;
      private String edtCheck_Nome_Internalname ;
      private String cmbCheck_Momento_Internalname ;
      private String cmbCheck_QdoNaoCmp_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV78WWCheckDS_4_Check_nome1 ;
      private String lV82WWCheckDS_8_Check_nome2 ;
      private String lV86WWCheckDS_12_Check_nome3 ;
      private String lV87WWCheckDS_13_Tfcheck_nome ;
      private String AV78WWCheckDS_4_Check_nome1 ;
      private String AV82WWCheckDS_8_Check_nome2 ;
      private String AV86WWCheckDS_12_Check_nome3 ;
      private String AV88WWCheckDS_14_Tfcheck_nome_sel ;
      private String AV87WWCheckDS_13_Tfcheck_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavCheck_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavCheck_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavCheck_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavCheck_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_check_nome_Internalname ;
      private String Ddo_check_momento_Internalname ;
      private String Ddo_check_qdonaocmp_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtCheck_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcheck_areatrabalhocod_Internalname ;
      private String lblFiltertextcheck_areatrabalhocod_Jsonclick ;
      private String edtavCheck_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavCheck_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavCheck_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavCheck_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblChecktitle_Internalname ;
      private String lblChecktitle_Jsonclick ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_92_fel_idx="0001" ;
      private String ROClassString ;
      private String edtCheck_Codigo_Jsonclick ;
      private String edtCheck_Nome_Jsonclick ;
      private String cmbCheck_Momento_Jsonclick ;
      private String cmbCheck_QdoNaoCmp_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_check_nome_Includesortasc ;
      private bool Ddo_check_nome_Includesortdsc ;
      private bool Ddo_check_nome_Includefilter ;
      private bool Ddo_check_nome_Filterisrange ;
      private bool Ddo_check_nome_Includedatalist ;
      private bool Ddo_check_momento_Includesortasc ;
      private bool Ddo_check_momento_Includesortdsc ;
      private bool Ddo_check_momento_Includefilter ;
      private bool Ddo_check_momento_Includedatalist ;
      private bool Ddo_check_momento_Allowmultipleselection ;
      private bool Ddo_check_qdonaocmp_Includesortasc ;
      private bool Ddo_check_qdonaocmp_Includesortdsc ;
      private bool Ddo_check_qdonaocmp_Includefilter ;
      private bool Ddo_check_qdonaocmp_Includedatalist ;
      private bool Ddo_check_qdonaocmp_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1856Check_QdoNaoCmp ;
      private bool AV79WWCheckDS_5_Dynamicfiltersenabled2 ;
      private bool AV83WWCheckDS_9_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Update_IsBlob ;
      private bool AV30Delete_IsBlob ;
      private bool AV57Display_IsBlob ;
      private String AV47TFCheck_Momento_SelsJson ;
      private String AV70TFCheck_QdoNaoCmp_SelsJson ;
      private String AV33ManageFiltersXml ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV41ddo_Check_NomeTitleControlIdToReplace ;
      private String AV49ddo_Check_MomentoTitleControlIdToReplace ;
      private String AV72ddo_Check_QdoNaoCmpTitleControlIdToReplace ;
      private String AV91Update_GXI ;
      private String AV92Delete_GXI ;
      private String AV93Display_GXI ;
      private String AV76WWCheckDS_2_Dynamicfiltersselector1 ;
      private String AV80WWCheckDS_6_Dynamicfiltersselector2 ;
      private String AV84WWCheckDS_10_Dynamicfiltersselector3 ;
      private String AV29Update ;
      private String AV30Delete ;
      private String AV57Display ;
      private IGxSession AV31Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbCheck_Momento ;
      private GXCombobox cmbCheck_QdoNaoCmp ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00NS2_A1840Check_AreaTrabalhoCod ;
      private short[] H00NS2_A1856Check_QdoNaoCmp ;
      private bool[] H00NS2_n1856Check_QdoNaoCmp ;
      private short[] H00NS2_A1843Check_Momento ;
      private String[] H00NS2_A1841Check_Nome ;
      private int[] H00NS2_A1839Check_Codigo ;
      private long[] H00NS3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV48TFCheck_Momento_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV71TFCheck_QdoNaoCmp_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV89WWCheckDS_15_Tfcheck_momento_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38Check_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Check_MomentoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69Check_QdoNaoCmpTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV34ManageFiltersItems ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV37ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV53DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV35ManageFiltersItem ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wwcheck__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00NS2( IGxContext context ,
                                             short A1843Check_Momento ,
                                             IGxCollection AV89WWCheckDS_15_Tfcheck_momento_sels ,
                                             short A1856Check_QdoNaoCmp ,
                                             IGxCollection AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels ,
                                             String AV76WWCheckDS_2_Dynamicfiltersselector1 ,
                                             short AV77WWCheckDS_3_Dynamicfiltersoperator1 ,
                                             String AV78WWCheckDS_4_Check_nome1 ,
                                             bool AV79WWCheckDS_5_Dynamicfiltersenabled2 ,
                                             String AV80WWCheckDS_6_Dynamicfiltersselector2 ,
                                             short AV81WWCheckDS_7_Dynamicfiltersoperator2 ,
                                             String AV82WWCheckDS_8_Check_nome2 ,
                                             bool AV83WWCheckDS_9_Dynamicfiltersenabled3 ,
                                             String AV84WWCheckDS_10_Dynamicfiltersselector3 ,
                                             short AV85WWCheckDS_11_Dynamicfiltersoperator3 ,
                                             String AV86WWCheckDS_12_Check_nome3 ,
                                             String AV88WWCheckDS_14_Tfcheck_nome_sel ,
                                             String AV87WWCheckDS_13_Tfcheck_nome ,
                                             int AV89WWCheckDS_15_Tfcheck_momento_sels_Count ,
                                             int AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count ,
                                             String A1841Check_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1840Check_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [14] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Check_AreaTrabalhoCod], [Check_QdoNaoCmp], [Check_Momento], [Check_Nome], [Check_Codigo]";
         sFromString = " FROM [Check] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Check_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV76WWCheckDS_2_Dynamicfiltersselector1, "CHECK_NOME") == 0 ) && ( AV77WWCheckDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV78WWCheckDS_4_Check_nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWCheckDS_2_Dynamicfiltersselector1, "CHECK_NOME") == 0 ) && ( AV77WWCheckDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV78WWCheckDS_4_Check_nome1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV79WWCheckDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWCheckDS_6_Dynamicfiltersselector2, "CHECK_NOME") == 0 ) && ( AV81WWCheckDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV82WWCheckDS_8_Check_nome2)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV79WWCheckDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWCheckDS_6_Dynamicfiltersselector2, "CHECK_NOME") == 0 ) && ( AV81WWCheckDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV82WWCheckDS_8_Check_nome2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV83WWCheckDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWCheckDS_10_Dynamicfiltersselector3, "CHECK_NOME") == 0 ) && ( AV85WWCheckDS_11_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV86WWCheckDS_12_Check_nome3)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV83WWCheckDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWCheckDS_10_Dynamicfiltersselector3, "CHECK_NOME") == 0 ) && ( AV85WWCheckDS_11_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV86WWCheckDS_12_Check_nome3)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWCheckDS_14_Tfcheck_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWCheckDS_13_Tfcheck_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV87WWCheckDS_13_Tfcheck_nome)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWCheckDS_14_Tfcheck_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] = @AV88WWCheckDS_14_Tfcheck_nome_sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV89WWCheckDS_15_Tfcheck_momento_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWCheckDS_15_Tfcheck_momento_sels, "[Check_Momento] IN (", ")") + ")";
         }
         if ( AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels, "[Check_QdoNaoCmp] IN (", ")") + ")";
         }
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY [Check_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Momento]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Momento] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_QdoNaoCmp]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_QdoNaoCmp] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00NS3( IGxContext context ,
                                             short A1843Check_Momento ,
                                             IGxCollection AV89WWCheckDS_15_Tfcheck_momento_sels ,
                                             short A1856Check_QdoNaoCmp ,
                                             IGxCollection AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels ,
                                             String AV76WWCheckDS_2_Dynamicfiltersselector1 ,
                                             short AV77WWCheckDS_3_Dynamicfiltersoperator1 ,
                                             String AV78WWCheckDS_4_Check_nome1 ,
                                             bool AV79WWCheckDS_5_Dynamicfiltersenabled2 ,
                                             String AV80WWCheckDS_6_Dynamicfiltersselector2 ,
                                             short AV81WWCheckDS_7_Dynamicfiltersoperator2 ,
                                             String AV82WWCheckDS_8_Check_nome2 ,
                                             bool AV83WWCheckDS_9_Dynamicfiltersenabled3 ,
                                             String AV84WWCheckDS_10_Dynamicfiltersselector3 ,
                                             short AV85WWCheckDS_11_Dynamicfiltersoperator3 ,
                                             String AV86WWCheckDS_12_Check_nome3 ,
                                             String AV88WWCheckDS_14_Tfcheck_nome_sel ,
                                             String AV87WWCheckDS_13_Tfcheck_nome ,
                                             int AV89WWCheckDS_15_Tfcheck_momento_sels_Count ,
                                             int AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count ,
                                             String A1841Check_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1840Check_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [9] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Check] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Check_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV76WWCheckDS_2_Dynamicfiltersselector1, "CHECK_NOME") == 0 ) && ( AV77WWCheckDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV78WWCheckDS_4_Check_nome1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WWCheckDS_2_Dynamicfiltersselector1, "CHECK_NOME") == 0 ) && ( AV77WWCheckDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWCheckDS_4_Check_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV78WWCheckDS_4_Check_nome1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV79WWCheckDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWCheckDS_6_Dynamicfiltersselector2, "CHECK_NOME") == 0 ) && ( AV81WWCheckDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV82WWCheckDS_8_Check_nome2)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV79WWCheckDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV80WWCheckDS_6_Dynamicfiltersselector2, "CHECK_NOME") == 0 ) && ( AV81WWCheckDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWCheckDS_8_Check_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV82WWCheckDS_8_Check_nome2)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV83WWCheckDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWCheckDS_10_Dynamicfiltersselector3, "CHECK_NOME") == 0 ) && ( AV85WWCheckDS_11_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV86WWCheckDS_12_Check_nome3)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV83WWCheckDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV84WWCheckDS_10_Dynamicfiltersselector3, "CHECK_NOME") == 0 ) && ( AV85WWCheckDS_11_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWCheckDS_12_Check_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like '%' + @lV86WWCheckDS_12_Check_nome3)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWCheckDS_14_Tfcheck_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWCheckDS_13_Tfcheck_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] like @lV87WWCheckDS_13_Tfcheck_nome)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWCheckDS_14_Tfcheck_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Check_Nome] = @AV88WWCheckDS_14_Tfcheck_nome_sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV89WWCheckDS_15_Tfcheck_momento_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWCheckDS_15_Tfcheck_momento_sels, "[Check_Momento] IN (", ")") + ")";
         }
         if ( AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV90WWCheckDS_16_Tfcheck_qdonaocmp_sels, "[Check_QdoNaoCmp] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00NS2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
               case 1 :
                     return conditional_H00NS3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (short)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00NS2 ;
          prmH00NS2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWCheckDS_4_Check_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWCheckDS_4_Check_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWCheckDS_8_Check_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWCheckDS_8_Check_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWCheckDS_12_Check_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWCheckDS_12_Check_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87WWCheckDS_13_Tfcheck_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWCheckDS_14_Tfcheck_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00NS3 ;
          prmH00NS3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWCheckDS_4_Check_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWCheckDS_4_Check_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWCheckDS_8_Check_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWCheckDS_8_Check_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWCheckDS_12_Check_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV86WWCheckDS_12_Check_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87WWCheckDS_13_Tfcheck_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWCheckDS_14_Tfcheck_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00NS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NS2,11,0,true,false )
             ,new CursorDef("H00NS3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NS3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

}
