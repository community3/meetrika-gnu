/*
               File: PRC_UsuarioEhContratanteDaArea
        Description: Usuario Eh Contratante Da Area
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:59.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_usuarioehcontratantedaarea : GXProcedure
   {
      public prc_usuarioehcontratantedaarea( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_usuarioehcontratantedaarea( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratanteUsuario_AreaTrabalhoCod ,
                           ref int aP1_ContratanteUsuario_UsuarioCod ,
                           out bool aP2_EhContratante )
      {
         this.A1020ContratanteUsuario_AreaTrabalhoCod = aP0_ContratanteUsuario_AreaTrabalhoCod;
         this.A60ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         this.AV9EhContratante = false ;
         initialize();
         executePrivate();
         aP0_ContratanteUsuario_AreaTrabalhoCod=this.A1020ContratanteUsuario_AreaTrabalhoCod;
         aP1_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP2_EhContratante=this.AV9EhContratante;
      }

      public bool executeUdp( ref int aP0_ContratanteUsuario_AreaTrabalhoCod ,
                              ref int aP1_ContratanteUsuario_UsuarioCod )
      {
         this.A1020ContratanteUsuario_AreaTrabalhoCod = aP0_ContratanteUsuario_AreaTrabalhoCod;
         this.A60ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         this.AV9EhContratante = false ;
         initialize();
         executePrivate();
         aP0_ContratanteUsuario_AreaTrabalhoCod=this.A1020ContratanteUsuario_AreaTrabalhoCod;
         aP1_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP2_EhContratante=this.AV9EhContratante;
         return AV9EhContratante ;
      }

      public void executeSubmit( ref int aP0_ContratanteUsuario_AreaTrabalhoCod ,
                                 ref int aP1_ContratanteUsuario_UsuarioCod ,
                                 out bool aP2_EhContratante )
      {
         prc_usuarioehcontratantedaarea objprc_usuarioehcontratantedaarea;
         objprc_usuarioehcontratantedaarea = new prc_usuarioehcontratantedaarea();
         objprc_usuarioehcontratantedaarea.A1020ContratanteUsuario_AreaTrabalhoCod = aP0_ContratanteUsuario_AreaTrabalhoCod;
         objprc_usuarioehcontratantedaarea.A60ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         objprc_usuarioehcontratantedaarea.AV9EhContratante = false ;
         objprc_usuarioehcontratantedaarea.context.SetSubmitInitialConfig(context);
         objprc_usuarioehcontratantedaarea.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_usuarioehcontratantedaarea);
         aP0_ContratanteUsuario_AreaTrabalhoCod=this.A1020ContratanteUsuario_AreaTrabalhoCod;
         aP1_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP2_EhContratante=this.AV9EhContratante;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_usuarioehcontratantedaarea)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VU3 */
         pr_default.execute(0, new Object[] {A60ContratanteUsuario_UsuarioCod, n1020ContratanteUsuario_AreaTrabalhoCod, A1020ContratanteUsuario_AreaTrabalhoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = P00VU3_A63ContratanteUsuario_ContratanteCod[0];
            AV9EhContratante = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VU3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00VU3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00VU3_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         P00VU3_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_usuarioehcontratantedaarea__default(),
            new Object[][] {
                new Object[] {
               P00VU3_A63ContratanteUsuario_ContratanteCod, P00VU3_A60ContratanteUsuario_UsuarioCod, P00VU3_A1020ContratanteUsuario_AreaTrabalhoCod, P00VU3_n1020ContratanteUsuario_AreaTrabalhoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private String scmdbuf ;
      private bool AV9EhContratante ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratanteUsuario_AreaTrabalhoCod ;
      private int aP1_ContratanteUsuario_UsuarioCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00VU3_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00VU3_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00VU3_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] P00VU3_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool aP2_EhContratante ;
   }

   public class prc_usuarioehcontratantedaarea__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VU3 ;
          prmP00VU3 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VU3", "SELECT TOP 1 T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod) AND (COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) = @ContratanteUsuario_AreaTrabalhoCod) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VU3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
       }
    }

 }

}
