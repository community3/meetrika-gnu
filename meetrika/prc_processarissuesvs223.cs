/*
               File: PRC_ProcessarIssuesVs223
        Description: Stub for PRC_ProcessarIssuesVs223
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:15:26.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_processarissuesvs223 : GXProcedure
   {
      public prc_processarissuesvs223( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public prc_processarissuesvs223( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Host ,
                           String aP1_Url ,
                           String aP2_Key ,
                           int aP3_Contratada ,
                           int aP4_ContratoServicos_Codigo ,
                           ref int aP5_UserId )
      {
         this.AV2Host = aP0_Host;
         this.AV3Url = aP1_Url;
         this.AV4Key = aP2_Key;
         this.AV5Contratada = aP3_Contratada;
         this.AV6ContratoServicos_Codigo = aP4_ContratoServicos_Codigo;
         this.AV7UserId = aP5_UserId;
         initialize();
         executePrivate();
         aP5_UserId=this.AV7UserId;
      }

      public int executeUdp( String aP0_Host ,
                             String aP1_Url ,
                             String aP2_Key ,
                             int aP3_Contratada ,
                             int aP4_ContratoServicos_Codigo )
      {
         this.AV2Host = aP0_Host;
         this.AV3Url = aP1_Url;
         this.AV4Key = aP2_Key;
         this.AV5Contratada = aP3_Contratada;
         this.AV6ContratoServicos_Codigo = aP4_ContratoServicos_Codigo;
         this.AV7UserId = aP5_UserId;
         initialize();
         executePrivate();
         aP5_UserId=this.AV7UserId;
         return AV7UserId ;
      }

      public void executeSubmit( String aP0_Host ,
                                 String aP1_Url ,
                                 String aP2_Key ,
                                 int aP3_Contratada ,
                                 int aP4_ContratoServicos_Codigo ,
                                 ref int aP5_UserId )
      {
         prc_processarissuesvs223 objprc_processarissuesvs223;
         objprc_processarissuesvs223 = new prc_processarissuesvs223();
         objprc_processarissuesvs223.AV2Host = aP0_Host;
         objprc_processarissuesvs223.AV3Url = aP1_Url;
         objprc_processarissuesvs223.AV4Key = aP2_Key;
         objprc_processarissuesvs223.AV5Contratada = aP3_Contratada;
         objprc_processarissuesvs223.AV6ContratoServicos_Codigo = aP4_ContratoServicos_Codigo;
         objprc_processarissuesvs223.AV7UserId = aP5_UserId;
         objprc_processarissuesvs223.context.SetSubmitInitialConfig(context);
         objprc_processarissuesvs223.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_processarissuesvs223);
         aP5_UserId=this.AV7UserId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_processarissuesvs223)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2Host,(String)AV3Url,(String)AV4Key,(int)AV5Contratada,(int)AV6ContratoServicos_Codigo,(int)AV7UserId} ;
         ClassLoader.Execute("aprc_processarissuesvs223","GeneXus.Programs.aprc_processarissuesvs223", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 6 ) )
         {
            AV7UserId = (int)(args[5]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV5Contratada ;
      private int AV6ContratoServicos_Codigo ;
      private int AV7UserId ;
      private String AV2Host ;
      private String AV3Url ;
      private String AV4Key ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP5_UserId ;
      private Object[] args ;
   }

}
