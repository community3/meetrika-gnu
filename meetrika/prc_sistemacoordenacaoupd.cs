/*
               File: PRC_SistemaCoordenacaoUpd
        Description: Sistema Coordenacao Update
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:46.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sistemacoordenacaoupd : GXProcedure
   {
      public prc_sistemacoordenacaoupd( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_sistemacoordenacaoupd( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Arquivo ,
                           ref int aP1_Sistema_AreaTrabalhoCod )
      {
         this.AV8Arquivo = aP0_Arquivo;
         this.A135Sistema_AreaTrabalhoCod = aP1_Sistema_AreaTrabalhoCod;
         initialize();
         executePrivate();
         aP0_Arquivo=this.AV8Arquivo;
         aP1_Sistema_AreaTrabalhoCod=this.A135Sistema_AreaTrabalhoCod;
      }

      public int executeUdp( ref String aP0_Arquivo )
      {
         this.AV8Arquivo = aP0_Arquivo;
         this.A135Sistema_AreaTrabalhoCod = aP1_Sistema_AreaTrabalhoCod;
         initialize();
         executePrivate();
         aP0_Arquivo=this.AV8Arquivo;
         aP1_Sistema_AreaTrabalhoCod=this.A135Sistema_AreaTrabalhoCod;
         return A135Sistema_AreaTrabalhoCod ;
      }

      public void executeSubmit( ref String aP0_Arquivo ,
                                 ref int aP1_Sistema_AreaTrabalhoCod )
      {
         prc_sistemacoordenacaoupd objprc_sistemacoordenacaoupd;
         objprc_sistemacoordenacaoupd = new prc_sistemacoordenacaoupd();
         objprc_sistemacoordenacaoupd.AV8Arquivo = aP0_Arquivo;
         objprc_sistemacoordenacaoupd.A135Sistema_AreaTrabalhoCod = aP1_Sistema_AreaTrabalhoCod;
         objprc_sistemacoordenacaoupd.context.SetSubmitInitialConfig(context);
         objprc_sistemacoordenacaoupd.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sistemacoordenacaoupd);
         aP0_Arquivo=this.AV8Arquivo;
         aP1_Sistema_AreaTrabalhoCod=this.A135Sistema_AreaTrabalhoCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sistemacoordenacaoupd)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12ErrCod = AV9ExcelDocument.Open(AV8Arquivo);
         AV13Ln = 3;
         while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9ExcelDocument.get_Cells(AV13Ln, 2, 1, 1).Text)) )
         {
            GXt_char1 = AV11Sistema_Sigla;
            new prc_padronizastring(context ).execute(  StringUtil.Trim( AV9ExcelDocument.get_Cells(AV13Ln, 2, 1, 1).Text), out  GXt_char1) ;
            AV11Sistema_Sigla = "%" + GXt_char1 + "%";
            AV10Sistema_Coordenacao = StringUtil.Upper( StringUtil.Trim( AV9ExcelDocument.get_Cells(AV13Ln, 1, 1, 1).Text));
            AV16GXLvl8 = 0;
            /* Using cursor P003D2 */
            pr_default.execute(0, new Object[] {A135Sistema_AreaTrabalhoCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A416Sistema_Nome = P003D2_A416Sistema_Nome[0];
               A513Sistema_Coordenacao = P003D2_A513Sistema_Coordenacao[0];
               n513Sistema_Coordenacao = P003D2_n513Sistema_Coordenacao[0];
               A127Sistema_Codigo = P003D2_A127Sistema_Codigo[0];
               if ( StringUtil.Like( new prc_padronizastring(context).executeUdp(  A416Sistema_Nome) , StringUtil.PadR( AV11Sistema_Sigla , 200 , "%"),  ' ' ) )
               {
                  AV16GXLvl8 = 1;
                  A513Sistema_Coordenacao = AV10Sistema_Coordenacao;
                  n513Sistema_Coordenacao = false;
                  BatchSize = 100;
                  pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp003d3");
                  /* Using cursor P003D3 */
                  pr_default.addRecord(1, new Object[] {n513Sistema_Coordenacao, A513Sistema_Coordenacao, A127Sistema_Codigo});
                  if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
                  {
                     Executebatchp003d3( ) ;
                  }
                  dsDefault.SmartCacheProvider.SetUpdated("Sistema") ;
               }
               pr_default.readNext(0);
            }
            if ( pr_default.getBatchSize(1) > 0 )
            {
               Executebatchp003d3( ) ;
            }
            pr_default.close(0);
            if ( AV16GXLvl8 == 0 )
            {
               AV9ExcelDocument.get_Cells(AV13Ln, 5, 1, 1).Text = "*";
            }
            AV13Ln = (short)(AV13Ln+1);
         }
         AV9ExcelDocument.Save();
         AV12ErrCod = AV9ExcelDocument.Close();
         this.cleanup();
      }

      protected void Executebatchp003d3( )
      {
         /* Using cursor P003D3 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_SistemaCoordenacaoUpd");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         AV9ExcelDocument = new ExcelDocumentI();
         AV11Sistema_Sigla = "";
         GXt_char1 = "";
         AV10Sistema_Coordenacao = "";
         scmdbuf = "";
         P003D2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P003D2_A416Sistema_Nome = new String[] {""} ;
         P003D2_A513Sistema_Coordenacao = new String[] {""} ;
         P003D2_n513Sistema_Coordenacao = new bool[] {false} ;
         P003D2_A127Sistema_Codigo = new int[1] ;
         A416Sistema_Nome = "";
         A513Sistema_Coordenacao = "";
         P003D3_A513Sistema_Coordenacao = new String[] {""} ;
         P003D3_n513Sistema_Coordenacao = new bool[] {false} ;
         P003D3_A127Sistema_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_sistemacoordenacaoupd__default(),
            new Object[][] {
                new Object[] {
               P003D2_A135Sistema_AreaTrabalhoCod, P003D2_A416Sistema_Nome, P003D2_A513Sistema_Coordenacao, P003D2_n513Sistema_Coordenacao, P003D2_A127Sistema_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12ErrCod ;
      private short AV13Ln ;
      private short AV16GXLvl8 ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int BatchSize ;
      private String AV8Arquivo ;
      private String AV11Sistema_Sigla ;
      private String GXt_char1 ;
      private String scmdbuf ;
      private bool n513Sistema_Coordenacao ;
      private String AV10Sistema_Coordenacao ;
      private String A416Sistema_Nome ;
      private String A513Sistema_Coordenacao ;
      private ExcelDocumentI AV9ExcelDocument ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Arquivo ;
      private int aP1_Sistema_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P003D2_A135Sistema_AreaTrabalhoCod ;
      private String[] P003D2_A416Sistema_Nome ;
      private String[] P003D2_A513Sistema_Coordenacao ;
      private bool[] P003D2_n513Sistema_Coordenacao ;
      private int[] P003D2_A127Sistema_Codigo ;
      private String[] P003D3_A513Sistema_Coordenacao ;
      private bool[] P003D3_n513Sistema_Coordenacao ;
      private int[] P003D3_A127Sistema_Codigo ;
   }

   public class prc_sistemacoordenacaoupd__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003D2 ;
          prmP003D2 = new Object[] {
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003D3 ;
          prmP003D3 = new Object[] {
          new Object[] {"@Sistema_Coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003D2", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Nome], [Sistema_Coordenacao], [Sistema_Codigo] FROM [Sistema] WITH (UPDLOCK) WHERE [Sistema_AreaTrabalhoCod] = @Sistema_AreaTrabalhoCod ORDER BY [Sistema_Nome] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003D2,1,0,true,false )
             ,new CursorDef("P003D3", "UPDATE [Sistema] SET [Sistema_Coordenacao]=@Sistema_Coordenacao  WHERE [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003D3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
