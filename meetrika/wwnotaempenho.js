/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:23:2.59
*/
gx.evt.autoSkip = false;
gx.define('wwnotaempenho', false, function () {
   this.ServerClass =  "wwnotaempenho" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV78Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV10GridState=gx.fn.getControlValue("vGRIDSTATE") ;
      this.AV27DynamicFiltersIgnoreFirst=gx.fn.getControlValue("vDYNAMICFILTERSIGNOREFIRST") ;
      this.AV26DynamicFiltersRemoving=gx.fn.getControlValue("vDYNAMICFILTERSREMOVING") ;
      this.A74Contrato_Codigo=gx.fn.getIntegerValue("CONTRATO_CODIGO",'.') ;
   };
   this.Valid_Saldocontrato_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(88) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("SALDOCONTRATO_CODIGO", gx.fn.currentGridRowImpl(88));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfnotaempenho_demissao=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFNOTAEMPENHO_DEMISSAO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV46TFNotaEmpenho_DEmissao)==0) || new gx.date.gxdate( this.AV46TFNotaEmpenho_DEmissao ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFNota Empenho_DEmissao fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tfnotaempenho_demissao_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFNOTAEMPENHO_DEMISSAO_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV47TFNotaEmpenho_DEmissao_To)==0) || new gx.date.gxdate( this.AV47TFNotaEmpenho_DEmissao_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFNota Empenho_DEmissao_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_notaempenho_demissaoauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_NOTAEMPENHO_DEMISSAOAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV48DDO_NotaEmpenho_DEmissaoAuxDate)==0) || new gx.date.gxdate( this.AV48DDO_NotaEmpenho_DEmissaoAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Nota Empenho_DEmissao Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_notaempenho_demissaoauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV49DDO_NotaEmpenho_DEmissaoAuxDateTo)==0) || new gx.date.gxdate( this.AV49DDO_NotaEmpenho_DEmissaoAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Nota Empenho_DEmissao Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("vSALDOCONTRATO_CODIGO1","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR1","Visible", false );
      if ( this.AV15DynamicFiltersSelector1 == "SALDOCONTRATO_CODIGO" )
      {
         gx.fn.setCtrlProperty("vSALDOCONTRATO_CODIGO1","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR1","Visible", true );
      }
   };
   this.s122_client=function()
   {
      gx.fn.setCtrlProperty("vSALDOCONTRATO_CODIGO2","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR2","Visible", false );
      if ( this.AV19DynamicFiltersSelector2 == "SALDOCONTRATO_CODIGO" )
      {
         gx.fn.setCtrlProperty("vSALDOCONTRATO_CODIGO2","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR2","Visible", true );
      }
   };
   this.s132_client=function()
   {
      gx.fn.setCtrlProperty("vSALDOCONTRATO_CODIGO3","Visible", false );
      gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR3","Visible", false );
      if ( this.AV23DynamicFiltersSelector3 == "SALDOCONTRATO_CODIGO" )
      {
         gx.fn.setCtrlProperty("vSALDOCONTRATO_CODIGO3","Visible", true );
         gx.fn.setCtrlProperty("vDYNAMICFILTERSOPERATOR3","Visible", true );
      }
   };
   this.s162_client=function()
   {
      this.s182_client();
      if ( this.AV13OrderedBy == 2 )
      {
         this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 1 )
      {
         this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 3 )
      {
         this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 4 )
      {
         this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 5 )
      {
         this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 6 )
      {
         this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 7 )
      {
         this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 8 )
      {
         this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
      else if ( this.AV13OrderedBy == 9 )
      {
         this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus =  (this.AV14OrderedDsc ? "DSC" : "ASC")  ;
      }
   };
   this.s182_client=function()
   {
      this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus =  ""  ;
      this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus =  ""  ;
      this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus =  ""  ;
      this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus =  ""  ;
      this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus =  ""  ;
      this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus =  ""  ;
      this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus =  ""  ;
      this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus =  ""  ;
      this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus =  ""  ;
   };
   this.s202_client=function()
   {
      this.AV18DynamicFiltersEnabled2 =  false  ;
      this.AV19DynamicFiltersSelector2 =  "SALDOCONTRATO_CODIGO"  ;
      this.AV20DynamicFiltersOperator2 = gx.num.trunc( 0 ,0) ;
      this.AV21SaldoContrato_Codigo2 = gx.num.trunc( 0 ,0) ;
      this.s122_client();
      this.AV22DynamicFiltersEnabled3 =  false  ;
      this.AV23DynamicFiltersSelector3 =  "SALDOCONTRATO_CODIGO"  ;
      this.AV24DynamicFiltersOperator3 = gx.num.trunc( 0 ,0) ;
      this.AV25SaldoContrato_Codigo3 = gx.num.trunc( 0 ,0) ;
      this.s132_client();
   };
   this.e11m42_client=function()
   {
      this.executeServerEvent("GRIDPAGINATIONBAR.CHANGEPAGE", false, null, true, true);
   };
   this.e12m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_CODIGO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13m42_client=function()
   {
      this.executeServerEvent("DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_ITENTIFICADOR.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e15m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_DEMISSAO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e16m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_VALOR.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e17m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_QTD.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e18m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_SALDOANT.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e19m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_SALDOPOS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e20m42_client=function()
   {
      this.executeServerEvent("DDO_NOTAEMPENHO_ATIVO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e21m42_client=function()
   {
      this.executeServerEvent("VORDEREDBY.CLICK", true, null, false, true);
   };
   this.e22m42_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e23m42_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e24m42_client=function()
   {
      this.executeServerEvent("'REMOVEDYNAMICFILTERS3'", true, null, false, false);
   };
   this.e25m42_client=function()
   {
      this.executeServerEvent("'DOCLEANFILTERS'", true, null, false, false);
   };
   this.e26m42_client=function()
   {
      this.executeServerEvent("'DOINSERT'", true, null, false, false);
   };
   this.e27m42_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS1'", true, null, false, false);
   };
   this.e28m42_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR1.CLICK", true, null, false, true);
   };
   this.e29m42_client=function()
   {
      this.executeServerEvent("'ADDDYNAMICFILTERS2'", true, null, false, false);
   };
   this.e30m42_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR2.CLICK", true, null, false, true);
   };
   this.e31m42_client=function()
   {
      this.executeServerEvent("VDYNAMICFILTERSSELECTOR3.CLICK", true, null, false, true);
   };
   this.e35m42_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e36m42_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,16,18,20,21,23,26,28,31,33,35,37,40,42,44,45,48,50,52,54,57,59,61,62,65,67,69,71,74,76,78,79,82,85,89,90,91,92,93,94,95,96,97,98,99,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,127,129,131,133,135,137,139,141,143];
   this.GXLastCtrlId =143;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",88,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wwnotaempenho",[],false,1,false,true,0,false,false,false,"",0,"px","Novo registro",true,false,false,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addBitmap("&Update","vUPDATE",89,36,"px",17,"px",null,"","","Image","");
   GridContainer.addBitmap("&Delete","vDELETE",90,36,"px",17,"px",null,"","","Image","");
   GridContainer.addSingleLineEdit(1560,91,"NOTAEMPENHO_CODIGO","","","NotaEmpenho_Codigo","int",0,"px",6,6,"right",null,[],1560,"NotaEmpenho_Codigo",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1561,92,"SALDOCONTRATO_CODIGO","","","SaldoContrato_Codigo","int",0,"px",6,6,"right",null,[],1561,"SaldoContrato_Codigo",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1564,93,"NOTAEMPENHO_ITENTIFICADOR","","","NotaEmpenho_Itentificador","char",0,"px",15,15,"left",null,[],1564,"NotaEmpenho_Itentificador",true,0,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1565,94,"NOTAEMPENHO_DEMISSAO","","","NotaEmpenho_DEmissao","dtime",0,"px",14,14,"right",null,[],1565,"NotaEmpenho_DEmissao",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1566,95,"NOTAEMPENHO_VALOR","","","NotaEmpenho_Valor","decimal",0,"px",18,18,"right",null,[],1566,"NotaEmpenho_Valor",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1567,96,"NOTAEMPENHO_QTD","","","NotaEmpenho_Qtd","decimal",94,"px",14,14,"right",null,[],1567,"NotaEmpenho_Qtd",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1568,97,"NOTAEMPENHO_SALDOANT","","","NotaEmpenho_SaldoAnt","decimal",0,"px",18,18,"right",null,[],1568,"NotaEmpenho_SaldoAnt",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1569,98,"NOTAEMPENHO_SALDOPOS","","","NotaEmpenho_SaldoPos","decimal",0,"px",18,18,"right",null,[],1569,"NotaEmpenho_SaldoPos",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addCheckBox(1570,99,"NOTAEMPENHO_ATIVO","","","NotaEmpenho_Ativo","boolean","true","false",null,true,false,0,"px","");
   this.setGrid(GridContainer);
   this.WORKWITHPLUSUTILITIES1Container = gx.uc.getNew(this, 103, 20, "DVelop_WorkWithPlusUtilities", "WORKWITHPLUSUTILITIES1Container", "Workwithplusutilities1");
   var WORKWITHPLUSUTILITIES1Container = this.WORKWITHPLUSUTILITIES1Container;
   WORKWITHPLUSUTILITIES1Container.setProp("Width", "Width", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Height", "Height", "100", "str");
   WORKWITHPLUSUTILITIES1Container.setProp("Visible", "Visible", true, "bool");
   WORKWITHPLUSUTILITIES1Container.setProp("Enabled", "Enabled", true, "boolean");
   WORKWITHPLUSUTILITIES1Container.setProp("Class", "Class", "", "char");
   WORKWITHPLUSUTILITIES1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(WORKWITHPLUSUTILITIES1Container);
   this.DDO_NOTAEMPENHO_CODIGOContainer = gx.uc.getNew(this, 126, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_CODIGOContainer", "Ddo_notaempenho_codigo");
   var DDO_NOTAEMPENHO_CODIGOContainer = this.DDO_NOTAEMPENHO_CODIGOContainer;
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_CODIGOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_CODIGOContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_CODIGOContainer.addV2CFunction('AV33NotaEmpenho_CodigoTitleFilterData', "vNOTAEMPENHO_CODIGOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV33NotaEmpenho_CodigoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_CODIGOTITLEFILTERDATA",UC.ParentObject.AV33NotaEmpenho_CodigoTitleFilterData); });
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_CODIGOContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_CODIGOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_CODIGOContainer.addEventHandler("OnOptionClicked", this.e12m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_CODIGOContainer);
   this.DDO_SALDOCONTRATO_CODIGOContainer = gx.uc.getNew(this, 128, 20, "BootstrapDropDownOptions", "DDO_SALDOCONTRATO_CODIGOContainer", "Ddo_saldocontrato_codigo");
   var DDO_SALDOCONTRATO_CODIGOContainer = this.DDO_SALDOCONTRATO_CODIGOContainer;
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Icon", "Icon", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Caption", "Caption", "", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_SALDOCONTRATO_CODIGOContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_SALDOCONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_SALDOCONTRATO_CODIGOContainer.addV2CFunction('AV37SaldoContrato_CodigoTitleFilterData', "vSALDOCONTRATO_CODIGOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_SALDOCONTRATO_CODIGOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV37SaldoContrato_CodigoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vSALDOCONTRATO_CODIGOTITLEFILTERDATA",UC.ParentObject.AV37SaldoContrato_CodigoTitleFilterData); });
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_SALDOCONTRATO_CODIGOContainer.setProp("Class", "Class", "", "char");
   DDO_SALDOCONTRATO_CODIGOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_SALDOCONTRATO_CODIGOContainer.addEventHandler("OnOptionClicked", this.e13m42_client);
   this.setUserControl(DDO_SALDOCONTRATO_CODIGOContainer);
   this.DDO_NOTAEMPENHO_ITENTIFICADORContainer = gx.uc.getNew(this, 130, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_ITENTIFICADORContainer", "Ddo_notaempenho_itentificador");
   var DDO_NOTAEMPENHO_ITENTIFICADORContainer = this.DDO_NOTAEMPENHO_ITENTIFICADORContainer;
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("FilterType", "Filtertype", "Character", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("FilterIsRange", "Filterisrange", false, "bool");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("DataListType", "Datalisttype", "Dynamic", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("DataListProc", "Datalistproc", "GetWWNotaEmpenhoFilterData", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", 0, "num");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("LoadingData", "Loadingdata", "Carregando dados...", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("NoResultsFound", "Noresultsfound", "- Não se encontraram resultados -", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.addV2CFunction('AV41NotaEmpenho_ItentificadorTitleFilterData', "vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.addC2VFunction(function(UC) { UC.ParentObject.AV41NotaEmpenho_ItentificadorTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA",UC.ParentObject.AV41NotaEmpenho_ItentificadorTitleFilterData); });
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_ITENTIFICADORContainer.addEventHandler("OnOptionClicked", this.e14m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_ITENTIFICADORContainer);
   this.DDO_NOTAEMPENHO_DEMISSAOContainer = gx.uc.getNew(this, 132, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_DEMISSAOContainer", "Ddo_notaempenho_demissao");
   var DDO_NOTAEMPENHO_DEMISSAOContainer = this.DDO_NOTAEMPENHO_DEMISSAOContainer;
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_DEMISSAOContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_DEMISSAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_DEMISSAOContainer.addV2CFunction('AV45NotaEmpenho_DEmissaoTitleFilterData', "vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_DEMISSAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV45NotaEmpenho_DEmissaoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA",UC.ParentObject.AV45NotaEmpenho_DEmissaoTitleFilterData); });
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_DEMISSAOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_DEMISSAOContainer.addEventHandler("OnOptionClicked", this.e15m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_DEMISSAOContainer);
   this.DDO_NOTAEMPENHO_VALORContainer = gx.uc.getNew(this, 134, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_VALORContainer", "Ddo_notaempenho_valor");
   var DDO_NOTAEMPENHO_VALORContainer = this.DDO_NOTAEMPENHO_VALORContainer;
   DDO_NOTAEMPENHO_VALORContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_VALORContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_VALORContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_VALORContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_NOTAEMPENHO_VALORContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_NOTAEMPENHO_VALORContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_NOTAEMPENHO_VALORContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_NOTAEMPENHO_VALORContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_NOTAEMPENHO_VALORContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_VALORContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_VALORContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_VALORContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_VALORContainer.addV2CFunction('AV51NotaEmpenho_ValorTitleFilterData', "vNOTAEMPENHO_VALORTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_VALORContainer.addC2VFunction(function(UC) { UC.ParentObject.AV51NotaEmpenho_ValorTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_VALORTITLEFILTERDATA",UC.ParentObject.AV51NotaEmpenho_ValorTitleFilterData); });
   DDO_NOTAEMPENHO_VALORContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_VALORContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_VALORContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_VALORContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_VALORContainer.addEventHandler("OnOptionClicked", this.e16m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_VALORContainer);
   this.DDO_NOTAEMPENHO_QTDContainer = gx.uc.getNew(this, 136, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_QTDContainer", "Ddo_notaempenho_qtd");
   var DDO_NOTAEMPENHO_QTDContainer = this.DDO_NOTAEMPENHO_QTDContainer;
   DDO_NOTAEMPENHO_QTDContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_QTDContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_QTDContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_QTDContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_NOTAEMPENHO_QTDContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_NOTAEMPENHO_QTDContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_NOTAEMPENHO_QTDContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_NOTAEMPENHO_QTDContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_NOTAEMPENHO_QTDContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_QTDContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_QTDContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_QTDContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_QTDContainer.addV2CFunction('AV55NotaEmpenho_QtdTitleFilterData', "vNOTAEMPENHO_QTDTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_QTDContainer.addC2VFunction(function(UC) { UC.ParentObject.AV55NotaEmpenho_QtdTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_QTDTITLEFILTERDATA",UC.ParentObject.AV55NotaEmpenho_QtdTitleFilterData); });
   DDO_NOTAEMPENHO_QTDContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_QTDContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_QTDContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_QTDContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_QTDContainer.addEventHandler("OnOptionClicked", this.e17m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_QTDContainer);
   this.DDO_NOTAEMPENHO_SALDOANTContainer = gx.uc.getNew(this, 138, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_SALDOANTContainer", "Ddo_notaempenho_saldoant");
   var DDO_NOTAEMPENHO_SALDOANTContainer = this.DDO_NOTAEMPENHO_SALDOANTContainer;
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_SALDOANTContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_SALDOANTContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_SALDOANTContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_SALDOANTContainer.addV2CFunction('AV59NotaEmpenho_SaldoAntTitleFilterData', "vNOTAEMPENHO_SALDOANTTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_SALDOANTContainer.addC2VFunction(function(UC) { UC.ParentObject.AV59NotaEmpenho_SaldoAntTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_SALDOANTTITLEFILTERDATA",UC.ParentObject.AV59NotaEmpenho_SaldoAntTitleFilterData); });
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_SALDOANTContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_SALDOANTContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_SALDOANTContainer.addEventHandler("OnOptionClicked", this.e18m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_SALDOANTContainer);
   this.DDO_NOTAEMPENHO_SALDOPOSContainer = gx.uc.getNew(this, 140, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_SALDOPOSContainer", "Ddo_notaempenho_saldopos");
   var DDO_NOTAEMPENHO_SALDOPOSContainer = this.DDO_NOTAEMPENHO_SALDOPOSContainer;
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("FilterType", "Filtertype", "Numeric", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_SALDOPOSContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_SALDOPOSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_SALDOPOSContainer.addV2CFunction('AV63NotaEmpenho_SaldoPosTitleFilterData', "vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_SALDOPOSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV63NotaEmpenho_SaldoPosTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA",UC.ParentObject.AV63NotaEmpenho_SaldoPosTitleFilterData); });
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_SALDOPOSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_SALDOPOSContainer.addEventHandler("OnOptionClicked", this.e19m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_SALDOPOSContainer);
   this.DDO_NOTAEMPENHO_ATIVOContainer = gx.uc.getNew(this, 142, 20, "BootstrapDropDownOptions", "DDO_NOTAEMPENHO_ATIVOContainer", "Ddo_notaempenho_ativo");
   var DDO_NOTAEMPENHO_ATIVOContainer = this.DDO_NOTAEMPENHO_ATIVOContainer;
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("Icon", "Icon", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("Caption", "Caption", "", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("IncludeSortASC", "Includesortasc", true, "bool");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("IncludeSortDSC", "Includesortdsc", true, "bool");
   DDO_NOTAEMPENHO_ATIVOContainer.setDynProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "bool");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "1:Marcado,2:Desmarcado", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("SortASC", "Sortasc", "Ordenar de A à Z", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("SortDSC", "Sortdsc", "Ordenar de Z à A", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_NOTAEMPENHO_ATIVOContainer.addV2CFunction('AV70DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_NOTAEMPENHO_ATIVOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV70DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV70DDO_TitleSettingsIcons); });
   DDO_NOTAEMPENHO_ATIVOContainer.addV2CFunction('AV67NotaEmpenho_AtivoTitleFilterData', "vNOTAEMPENHO_ATIVOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_NOTAEMPENHO_ATIVOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV67NotaEmpenho_AtivoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vNOTAEMPENHO_ATIVOTITLEFILTERDATA",UC.ParentObject.AV67NotaEmpenho_AtivoTitleFilterData); });
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_NOTAEMPENHO_ATIVOContainer.setProp("Class", "Class", "", "char");
   DDO_NOTAEMPENHO_ATIVOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_NOTAEMPENHO_ATIVOContainer.addEventHandler("OnOptionClicked", this.e20m42_client);
   this.setUserControl(DDO_NOTAEMPENHO_ATIVOContainer);
   this.GRIDPAGINATIONBARContainer = gx.uc.getNew(this, 102, 20, "DVelop_DVPaginationBar", "GRIDPAGINATIONBARContainer", "Gridpaginationbar");
   var GRIDPAGINATIONBARContainer = this.GRIDPAGINATIONBARContainer;
   GRIDPAGINATIONBARContainer.setProp("Class", "Class", "PaginationBar", "str");
   GRIDPAGINATIONBARContainer.setProp("ShowFirst", "Showfirst", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowPrevious", "Showprevious", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowNext", "Shownext", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("ShowLast", "Showlast", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("PagesToShow", "Pagestoshow", 5, "num");
   GRIDPAGINATIONBARContainer.setProp("PagingButtonsPosition", "Pagingbuttonsposition", "Right", "str");
   GRIDPAGINATIONBARContainer.setProp("PagingCaptionPosition", "Pagingcaptionposition", "Left", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridClass", "Emptygridclass", "PaginationBarEmptyGrid", "str");
   GRIDPAGINATIONBARContainer.setProp("SelectedPage", "Selectedpage", "", "char");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelector", "Rowsperpageselector", false, "bool");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageSelectedValue", "Rowsperpageselectedvalue", '', "int");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageOptions", "Rowsperpageoptions", "", "char");
   GRIDPAGINATIONBARContainer.setProp("First", "First", "|«", "str");
   GRIDPAGINATIONBARContainer.setProp("Previous", "Previous", "«", "str");
   GRIDPAGINATIONBARContainer.setProp("Next", "Next", "»", "str");
   GRIDPAGINATIONBARContainer.setProp("Last", "Last", "»|", "str");
   GRIDPAGINATIONBARContainer.setProp("Caption", "Caption", "Página <CURRENT_PAGE> de <TOTAL_PAGES>", "str");
   GRIDPAGINATIONBARContainer.setProp("EmptyGridCaption", "Emptygridcaption", "Não encontraram-se registros", "str");
   GRIDPAGINATIONBARContainer.setProp("RowsPerPageCaption", "Rowsperpagecaption", "", "char");
   GRIDPAGINATIONBARContainer.addV2CFunction('AV72GridCurrentPage', "vGRIDCURRENTPAGE", 'SetCurrentPage');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV72GridCurrentPage=UC.GetCurrentPage();gx.fn.setControlValue("vGRIDCURRENTPAGE",UC.ParentObject.AV72GridCurrentPage); });
   GRIDPAGINATIONBARContainer.addV2CFunction('AV73GridPageCount', "vGRIDPAGECOUNT", 'SetPageCount');
   GRIDPAGINATIONBARContainer.addC2VFunction(function(UC) { UC.ParentObject.AV73GridPageCount=UC.GetPageCount();gx.fn.setControlValue("vGRIDPAGECOUNT",UC.ParentObject.AV73GridPageCount); });
   GRIDPAGINATIONBARContainer.setProp("RecordCount", "Recordcount", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Page", "Page", '', "str");
   GRIDPAGINATIONBARContainer.setProp("Visible", "Visible", true, "bool");
   GRIDPAGINATIONBARContainer.setProp("Enabled", "Enabled", true, "boolean");
   GRIDPAGINATIONBARContainer.setC2ShowFunction(function(UC) { UC.show(); });
   GRIDPAGINATIONBARContainer.addEventHandler("ChangePage", this.e11m42_client);
   this.setUserControl(GRIDPAGINATIONBARContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[8]={fld:"TABLEHEADER",grid:0};
   GXValidFnc[11]={fld:"NOTAEMPENHOTITLE", format:0,grid:0};
   GXValidFnc[13]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[16]={fld:"INSERT",grid:0};
   GXValidFnc[18]={fld:"ORDEREDTEXT", format:0,grid:0};
   GXValidFnc[20]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDBY",gxz:"ZV13OrderedBy",gxold:"OV13OrderedBy",gxvar:"AV13OrderedBy",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV13OrderedBy=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV13OrderedBy=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vORDEREDBY",gx.O.AV13OrderedBy)},c2v:function(){if(this.val()!==undefined)gx.O.AV13OrderedBy=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vORDEREDBY",'.')},nac:gx.falseFn};
   GXValidFnc[21]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vORDEREDDSC",gxz:"ZV14OrderedDsc",gxold:"OV14OrderedDsc",gxvar:"AV14OrderedDsc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV14OrderedDsc=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setControlValue("vORDEREDDSC",gx.O.AV14OrderedDsc,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV14OrderedDsc=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vORDEREDDSC")},nac:gx.falseFn};
   GXValidFnc[23]={fld:"TABLEFILTERS",grid:0};
   GXValidFnc[26]={fld:"CLEANFILTERS",grid:0};
   GXValidFnc[28]={fld:"TABLEDYNAMICFILTERS",grid:0};
   GXValidFnc[31]={fld:"DYNAMICFILTERSPREFIX1", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR1",gxz:"ZV15DynamicFiltersSelector1",gxold:"OV15DynamicFiltersSelector1",gxvar:"AV15DynamicFiltersSelector1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV15DynamicFiltersSelector1=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV15DynamicFiltersSelector1=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR1",gx.O.AV15DynamicFiltersSelector1)},c2v:function(){if(this.val()!==undefined)gx.O.AV15DynamicFiltersSelector1=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR1")},nac:gx.falseFn};
   GXValidFnc[35]={fld:"DYNAMICFILTERSMIDDLE1", format:0,grid:0};
   GXValidFnc[37]={fld:"TABLEMERGEDDYNAMICFILTERS1",grid:0};
   GXValidFnc[40]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR1",gxz:"ZV16DynamicFiltersOperator1",gxold:"OV16DynamicFiltersOperator1",gxvar:"AV16DynamicFiltersOperator1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV16DynamicFiltersOperator1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV16DynamicFiltersOperator1=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR1",gx.O.AV16DynamicFiltersOperator1)},c2v:function(){if(this.val()!==undefined)gx.O.AV16DynamicFiltersOperator1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR1",'.')},nac:gx.falseFn};
   GXValidFnc[42]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_CODIGO1",gxz:"ZV17SaldoContrato_Codigo1",gxold:"OV17SaldoContrato_Codigo1",gxvar:"AV17SaldoContrato_Codigo1",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV17SaldoContrato_Codigo1=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV17SaldoContrato_Codigo1=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_CODIGO1",gx.O.AV17SaldoContrato_Codigo1,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV17SaldoContrato_Codigo1=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSALDOCONTRATO_CODIGO1",'.')},nac:gx.falseFn};
   GXValidFnc[44]={fld:"ADDDYNAMICFILTERS1",grid:0};
   GXValidFnc[45]={fld:"REMOVEDYNAMICFILTERS1",grid:0};
   GXValidFnc[48]={fld:"DYNAMICFILTERSPREFIX2", format:0,grid:0};
   GXValidFnc[50]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR2",gxz:"ZV19DynamicFiltersSelector2",gxold:"OV19DynamicFiltersSelector2",gxvar:"AV19DynamicFiltersSelector2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV19DynamicFiltersSelector2=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV19DynamicFiltersSelector2=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR2",gx.O.AV19DynamicFiltersSelector2)},c2v:function(){if(this.val()!==undefined)gx.O.AV19DynamicFiltersSelector2=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR2")},nac:gx.falseFn};
   GXValidFnc[52]={fld:"DYNAMICFILTERSMIDDLE2", format:0,grid:0};
   GXValidFnc[54]={fld:"TABLEMERGEDDYNAMICFILTERS2",grid:0};
   GXValidFnc[57]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR2",gxz:"ZV20DynamicFiltersOperator2",gxold:"OV20DynamicFiltersOperator2",gxvar:"AV20DynamicFiltersOperator2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV20DynamicFiltersOperator2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV20DynamicFiltersOperator2=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR2",gx.O.AV20DynamicFiltersOperator2)},c2v:function(){if(this.val()!==undefined)gx.O.AV20DynamicFiltersOperator2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR2",'.')},nac:gx.falseFn};
   GXValidFnc[59]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_CODIGO2",gxz:"ZV21SaldoContrato_Codigo2",gxold:"OV21SaldoContrato_Codigo2",gxvar:"AV21SaldoContrato_Codigo2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21SaldoContrato_Codigo2=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21SaldoContrato_Codigo2=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_CODIGO2",gx.O.AV21SaldoContrato_Codigo2,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21SaldoContrato_Codigo2=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSALDOCONTRATO_CODIGO2",'.')},nac:gx.falseFn};
   GXValidFnc[61]={fld:"ADDDYNAMICFILTERS2",grid:0};
   GXValidFnc[62]={fld:"REMOVEDYNAMICFILTERS2",grid:0};
   GXValidFnc[65]={fld:"DYNAMICFILTERSPREFIX3", format:0,grid:0};
   GXValidFnc[67]={lvl:0,type:"svchar",len:200,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSSELECTOR3",gxz:"ZV23DynamicFiltersSelector3",gxold:"OV23DynamicFiltersSelector3",gxvar:"AV23DynamicFiltersSelector3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV23DynamicFiltersSelector3=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV23DynamicFiltersSelector3=Value},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSSELECTOR3",gx.O.AV23DynamicFiltersSelector3)},c2v:function(){if(this.val()!==undefined)gx.O.AV23DynamicFiltersSelector3=this.val()},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSSELECTOR3")},nac:gx.falseFn};
   GXValidFnc[69]={fld:"DYNAMICFILTERSMIDDLE3", format:0,grid:0};
   GXValidFnc[71]={fld:"TABLEMERGEDDYNAMICFILTERS3",grid:0};
   GXValidFnc[74]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSOPERATOR3",gxz:"ZV24DynamicFiltersOperator3",gxold:"OV24DynamicFiltersOperator3",gxvar:"AV24DynamicFiltersOperator3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.AV24DynamicFiltersOperator3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24DynamicFiltersOperator3=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vDYNAMICFILTERSOPERATOR3",gx.O.AV24DynamicFiltersOperator3)},c2v:function(){if(this.val()!==undefined)gx.O.AV24DynamicFiltersOperator3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vDYNAMICFILTERSOPERATOR3",'.')},nac:gx.falseFn};
   GXValidFnc[76]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vSALDOCONTRATO_CODIGO3",gxz:"ZV25SaldoContrato_Codigo3",gxold:"OV25SaldoContrato_Codigo3",gxvar:"AV25SaldoContrato_Codigo3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV25SaldoContrato_Codigo3=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV25SaldoContrato_Codigo3=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vSALDOCONTRATO_CODIGO3",gx.O.AV25SaldoContrato_Codigo3,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV25SaldoContrato_Codigo3=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vSALDOCONTRATO_CODIGO3",'.')},nac:gx.falseFn};
   GXValidFnc[78]={fld:"REMOVEDYNAMICFILTERS3",grid:0};
   GXValidFnc[79]={fld:"JSDYNAMICFILTERS", format:1,grid:0};
   GXValidFnc[82]={fld:"TABLEGRIDHEADER",grid:0};
   GXValidFnc[85]={fld:"GRIDTABLEWITHPAGINATIONBAR",grid:0};
   GXValidFnc[89]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vUPDATE",gxz:"ZV28Update",gxold:"OV28Update",gxvar:"AV28Update",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV28Update=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV28Update=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vUPDATE",row || gx.fn.currentGridRowImpl(88),gx.O.AV28Update,gx.O.AV76Update_GXI)},c2v:function(){gx.O.AV76Update_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV28Update=this.val()},val:function(row){return gx.fn.getGridControlValue("vUPDATE",row || gx.fn.currentGridRowImpl(88))},val_GXI:function(row){return gx.fn.getGridControlValue("vUPDATE_GXI",row || gx.fn.currentGridRowImpl(88))}, gxvar_GXI:'AV76Update_GXI',nac:gx.falseFn};
   GXValidFnc[90]={lvl:2,type:"bits",len:1024,dec:0,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDELETE",gxz:"ZV29Delete",gxold:"OV29Delete",gxvar:"AV29Delete",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV29Delete=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV29Delete=Value},v2c:function(row){gx.fn.setGridMultimediaValue("vDELETE",row || gx.fn.currentGridRowImpl(88),gx.O.AV29Delete,gx.O.AV77Delete_GXI)},c2v:function(){gx.O.AV77Delete_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.AV29Delete=this.val()},val:function(row){return gx.fn.getGridControlValue("vDELETE",row || gx.fn.currentGridRowImpl(88))},val_GXI:function(row){return gx.fn.getGridControlValue("vDELETE_GXI",row || gx.fn.currentGridRowImpl(88))}, gxvar_GXI:'AV77Delete_GXI',nac:gx.falseFn};
   GXValidFnc[91]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_CODIGO",gxz:"Z1560NotaEmpenho_Codigo",gxold:"O1560NotaEmpenho_Codigo",gxvar:"A1560NotaEmpenho_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1560NotaEmpenho_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1560NotaEmpenho_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("NOTAEMPENHO_CODIGO",row || gx.fn.currentGridRowImpl(88),gx.O.A1560NotaEmpenho_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1560NotaEmpenho_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("NOTAEMPENHO_CODIGO",row || gx.fn.currentGridRowImpl(88),'.')},nac:gx.falseFn};
   GXValidFnc[92]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:this.Valid_Saldocontrato_codigo,isvalid:null,rgrid:[],fld:"SALDOCONTRATO_CODIGO",gxz:"Z1561SaldoContrato_Codigo",gxold:"O1561SaldoContrato_Codigo",gxvar:"A1561SaldoContrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1561SaldoContrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1561SaldoContrato_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("SALDOCONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(88),gx.O.A1561SaldoContrato_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1561SaldoContrato_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("SALDOCONTRATO_CODIGO",row || gx.fn.currentGridRowImpl(88),'.')},nac:gx.falseFn};
   GXValidFnc[93]={lvl:2,type:"char",len:15,dec:0,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_ITENTIFICADOR",gxz:"Z1564NotaEmpenho_Itentificador",gxold:"O1564NotaEmpenho_Itentificador",gxvar:"A1564NotaEmpenho_Itentificador",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',autoCorrect:"1",v2v:function(Value){if(Value!==undefined)gx.O.A1564NotaEmpenho_Itentificador=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1564NotaEmpenho_Itentificador=Value},v2c:function(row){gx.fn.setGridControlValue("NOTAEMPENHO_ITENTIFICADOR",row || gx.fn.currentGridRowImpl(88),gx.O.A1564NotaEmpenho_Itentificador,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1564NotaEmpenho_Itentificador=this.val()},val:function(row){return gx.fn.getGridControlValue("NOTAEMPENHO_ITENTIFICADOR",row || gx.fn.currentGridRowImpl(88))},nac:gx.falseFn};
   GXValidFnc[94]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_DEMISSAO",gxz:"Z1565NotaEmpenho_DEmissao",gxold:"O1565NotaEmpenho_DEmissao",gxvar:"A1565NotaEmpenho_DEmissao",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1565NotaEmpenho_DEmissao=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1565NotaEmpenho_DEmissao=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("NOTAEMPENHO_DEMISSAO",row || gx.fn.currentGridRowImpl(88),gx.O.A1565NotaEmpenho_DEmissao,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1565NotaEmpenho_DEmissao=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("NOTAEMPENHO_DEMISSAO",row || gx.fn.currentGridRowImpl(88))},nac:gx.falseFn};
   GXValidFnc[95]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_VALOR",gxz:"Z1566NotaEmpenho_Valor",gxold:"O1566NotaEmpenho_Valor",gxvar:"A1566NotaEmpenho_Valor",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1566NotaEmpenho_Valor=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1566NotaEmpenho_Valor=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("NOTAEMPENHO_VALOR",row || gx.fn.currentGridRowImpl(88),gx.O.A1566NotaEmpenho_Valor,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1566NotaEmpenho_Valor=this.val()},val:function(row){return gx.fn.getGridDecimalValue("NOTAEMPENHO_VALOR",row || gx.fn.currentGridRowImpl(88),'.',',')},nac:gx.falseFn};
   GXValidFnc[96]={lvl:2,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_QTD",gxz:"Z1567NotaEmpenho_Qtd",gxold:"O1567NotaEmpenho_Qtd",gxvar:"A1567NotaEmpenho_Qtd",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1567NotaEmpenho_Qtd=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1567NotaEmpenho_Qtd=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("NOTAEMPENHO_QTD",row || gx.fn.currentGridRowImpl(88),gx.O.A1567NotaEmpenho_Qtd,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1567NotaEmpenho_Qtd=this.val()},val:function(row){return gx.fn.getGridDecimalValue("NOTAEMPENHO_QTD",row || gx.fn.currentGridRowImpl(88),'.',',')},nac:gx.falseFn};
   GXValidFnc[97]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_SALDOANT",gxz:"Z1568NotaEmpenho_SaldoAnt",gxold:"O1568NotaEmpenho_SaldoAnt",gxvar:"A1568NotaEmpenho_SaldoAnt",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1568NotaEmpenho_SaldoAnt=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1568NotaEmpenho_SaldoAnt=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("NOTAEMPENHO_SALDOANT",row || gx.fn.currentGridRowImpl(88),gx.O.A1568NotaEmpenho_SaldoAnt,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1568NotaEmpenho_SaldoAnt=this.val()},val:function(row){return gx.fn.getGridDecimalValue("NOTAEMPENHO_SALDOANT",row || gx.fn.currentGridRowImpl(88),'.',',')},nac:gx.falseFn};
   GXValidFnc[98]={lvl:2,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_SALDOPOS",gxz:"Z1569NotaEmpenho_SaldoPos",gxold:"O1569NotaEmpenho_SaldoPos",gxvar:"A1569NotaEmpenho_SaldoPos",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1569NotaEmpenho_SaldoPos=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.Z1569NotaEmpenho_SaldoPos=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(row){gx.fn.setGridDecimalValue("NOTAEMPENHO_SALDOPOS",row || gx.fn.currentGridRowImpl(88),gx.O.A1569NotaEmpenho_SaldoPos,5,',');if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1569NotaEmpenho_SaldoPos=this.val()},val:function(row){return gx.fn.getGridDecimalValue("NOTAEMPENHO_SALDOPOS",row || gx.fn.currentGridRowImpl(88),'.',',')},nac:gx.falseFn};
   GXValidFnc[99]={lvl:2,type:"boolean",len:4,dec:0,sign:false,ro:1,isacc:0,grid:88,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"NOTAEMPENHO_ATIVO",gxz:"Z1570NotaEmpenho_Ativo",gxold:"O1570NotaEmpenho_Ativo",gxvar:"A1570NotaEmpenho_Ativo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1570NotaEmpenho_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1570NotaEmpenho_Ativo=gx.lang.booleanValue(Value)},v2c:function(row){gx.fn.setGridCheckBoxValue("NOTAEMPENHO_ATIVO",row || gx.fn.currentGridRowImpl(88),gx.O.A1570NotaEmpenho_Ativo,true)},c2v:function(){if(this.val()!==undefined)gx.O.A1570NotaEmpenho_Ativo=gx.lang.booleanValue(this.val())},val:function(row){return gx.fn.getGridControlValue("NOTAEMPENHO_ATIVO",row || gx.fn.currentGridRowImpl(88))},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[104]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED2",gxz:"ZV18DynamicFiltersEnabled2",gxold:"OV18DynamicFiltersEnabled2",gxvar:"AV18DynamicFiltersEnabled2",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV18DynamicFiltersEnabled2=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED2",gx.O.AV18DynamicFiltersEnabled2,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV18DynamicFiltersEnabled2=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED2")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[105]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vDYNAMICFILTERSENABLED3",gxz:"ZV22DynamicFiltersEnabled3",gxold:"OV22DynamicFiltersEnabled3",gxvar:"AV22DynamicFiltersEnabled3",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22DynamicFiltersEnabled3=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vDYNAMICFILTERSENABLED3",gx.O.AV22DynamicFiltersEnabled3,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV22DynamicFiltersEnabled3=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vDYNAMICFILTERSENABLED3")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[106]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_CODIGO",gxz:"ZV34TFNotaEmpenho_Codigo",gxold:"OV34TFNotaEmpenho_Codigo",gxvar:"AV34TFNotaEmpenho_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV34TFNotaEmpenho_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV34TFNotaEmpenho_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFNOTAEMPENHO_CODIGO",gx.O.AV34TFNotaEmpenho_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV34TFNotaEmpenho_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFNOTAEMPENHO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[107]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_CODIGO_TO",gxz:"ZV35TFNotaEmpenho_Codigo_To",gxold:"OV35TFNotaEmpenho_Codigo_To",gxvar:"AV35TFNotaEmpenho_Codigo_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV35TFNotaEmpenho_Codigo_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV35TFNotaEmpenho_Codigo_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFNOTAEMPENHO_CODIGO_TO",gx.O.AV35TFNotaEmpenho_Codigo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV35TFNotaEmpenho_Codigo_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFNOTAEMPENHO_CODIGO_TO",'.')},nac:gx.falseFn};
   GXValidFnc[108]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_CODIGO",gxz:"ZV38TFSaldoContrato_Codigo",gxold:"OV38TFSaldoContrato_Codigo",gxvar:"AV38TFSaldoContrato_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV38TFSaldoContrato_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV38TFSaldoContrato_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_CODIGO",gx.O.AV38TFSaldoContrato_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV38TFSaldoContrato_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFSALDOCONTRATO_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[109]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFSALDOCONTRATO_CODIGO_TO",gxz:"ZV39TFSaldoContrato_Codigo_To",gxold:"OV39TFSaldoContrato_Codigo_To",gxvar:"AV39TFSaldoContrato_Codigo_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV39TFSaldoContrato_Codigo_To=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV39TFSaldoContrato_Codigo_To=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFSALDOCONTRATO_CODIGO_TO",gx.O.AV39TFSaldoContrato_Codigo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV39TFSaldoContrato_Codigo_To=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFSALDOCONTRATO_CODIGO_TO",'.')},nac:gx.falseFn};
   GXValidFnc[110]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_ITENTIFICADOR",gxz:"ZV42TFNotaEmpenho_Itentificador",gxold:"OV42TFNotaEmpenho_Itentificador",gxvar:"AV42TFNotaEmpenho_Itentificador",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV42TFNotaEmpenho_Itentificador=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV42TFNotaEmpenho_Itentificador=Value},v2c:function(){gx.fn.setControlValue("vTFNOTAEMPENHO_ITENTIFICADOR",gx.O.AV42TFNotaEmpenho_Itentificador,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV42TFNotaEmpenho_Itentificador=this.val()},val:function(){return gx.fn.getControlValue("vTFNOTAEMPENHO_ITENTIFICADOR")},nac:gx.falseFn};
   GXValidFnc[111]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_ITENTIFICADOR_SEL",gxz:"ZV43TFNotaEmpenho_Itentificador_Sel",gxold:"OV43TFNotaEmpenho_Itentificador_Sel",gxvar:"AV43TFNotaEmpenho_Itentificador_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV43TFNotaEmpenho_Itentificador_Sel=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV43TFNotaEmpenho_Itentificador_Sel=Value},v2c:function(){gx.fn.setControlValue("vTFNOTAEMPENHO_ITENTIFICADOR_SEL",gx.O.AV43TFNotaEmpenho_Itentificador_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV43TFNotaEmpenho_Itentificador_Sel=this.val()},val:function(){return gx.fn.getControlValue("vTFNOTAEMPENHO_ITENTIFICADOR_SEL")},nac:gx.falseFn};
   GXValidFnc[112]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfnotaempenho_demissao,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_DEMISSAO",gxz:"ZV46TFNotaEmpenho_DEmissao",gxold:"OV46TFNotaEmpenho_DEmissao",gxvar:"AV46TFNotaEmpenho_DEmissao",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[112],ip:[112],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV46TFNotaEmpenho_DEmissao=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV46TFNotaEmpenho_DEmissao=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFNOTAEMPENHO_DEMISSAO",gx.O.AV46TFNotaEmpenho_DEmissao,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV46TFNotaEmpenho_DEmissao=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFNOTAEMPENHO_DEMISSAO")},nac:gx.falseFn};
   GXValidFnc[113]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tfnotaempenho_demissao_to,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_DEMISSAO_TO",gxz:"ZV47TFNotaEmpenho_DEmissao_To",gxold:"OV47TFNotaEmpenho_DEmissao_To",gxvar:"AV47TFNotaEmpenho_DEmissao_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[113],ip:[113],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV47TFNotaEmpenho_DEmissao_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV47TFNotaEmpenho_DEmissao_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFNOTAEMPENHO_DEMISSAO_TO",gx.O.AV47TFNotaEmpenho_DEmissao_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV47TFNotaEmpenho_DEmissao_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFNOTAEMPENHO_DEMISSAO_TO")},nac:gx.falseFn};
   GXValidFnc[114]={fld:"DDO_NOTAEMPENHO_DEMISSAOAUXDATES",grid:0};
   GXValidFnc[115]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_notaempenho_demissaoauxdate,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_DEMISSAOAUXDATE",gxz:"ZV48DDO_NotaEmpenho_DEmissaoAuxDate",gxold:"OV48DDO_NotaEmpenho_DEmissaoAuxDate",gxvar:"AV48DDO_NotaEmpenho_DEmissaoAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[115],ip:[115],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV48DDO_NotaEmpenho_DEmissaoAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV48DDO_NotaEmpenho_DEmissaoAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_DEMISSAOAUXDATE",gx.O.AV48DDO_NotaEmpenho_DEmissaoAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV48DDO_NotaEmpenho_DEmissaoAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_DEMISSAOAUXDATE")},nac:gx.falseFn};
   GXValidFnc[116]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_notaempenho_demissaoauxdateto,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO",gxz:"ZV49DDO_NotaEmpenho_DEmissaoAuxDateTo",gxold:"OV49DDO_NotaEmpenho_DEmissaoAuxDateTo",gxvar:"AV49DDO_NotaEmpenho_DEmissaoAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[116],ip:[116],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV49DDO_NotaEmpenho_DEmissaoAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV49DDO_NotaEmpenho_DEmissaoAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO",gx.O.AV49DDO_NotaEmpenho_DEmissaoAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV49DDO_NotaEmpenho_DEmissaoAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_DEMISSAOAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[117]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_VALOR",gxz:"ZV52TFNotaEmpenho_Valor",gxold:"OV52TFNotaEmpenho_Valor",gxvar:"AV52TFNotaEmpenho_Valor",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV52TFNotaEmpenho_Valor=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV52TFNotaEmpenho_Valor=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_VALOR",gx.O.AV52TFNotaEmpenho_Valor,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV52TFNotaEmpenho_Valor=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_VALOR",'.',',')},nac:gx.falseFn};
   GXValidFnc[118]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_VALOR_TO",gxz:"ZV53TFNotaEmpenho_Valor_To",gxold:"OV53TFNotaEmpenho_Valor_To",gxvar:"AV53TFNotaEmpenho_Valor_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV53TFNotaEmpenho_Valor_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV53TFNotaEmpenho_Valor_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_VALOR_TO",gx.O.AV53TFNotaEmpenho_Valor_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV53TFNotaEmpenho_Valor_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_VALOR_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[119]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_QTD",gxz:"ZV56TFNotaEmpenho_Qtd",gxold:"OV56TFNotaEmpenho_Qtd",gxvar:"AV56TFNotaEmpenho_Qtd",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV56TFNotaEmpenho_Qtd=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV56TFNotaEmpenho_Qtd=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_QTD",gx.O.AV56TFNotaEmpenho_Qtd,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV56TFNotaEmpenho_Qtd=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_QTD",'.',',')},nac:gx.falseFn};
   GXValidFnc[120]={lvl:0,type:"decimal",len:14,dec:5,sign:false,pic:"ZZ,ZZZ,ZZ9.999",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_QTD_TO",gxz:"ZV57TFNotaEmpenho_Qtd_To",gxold:"OV57TFNotaEmpenho_Qtd_To",gxvar:"AV57TFNotaEmpenho_Qtd_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV57TFNotaEmpenho_Qtd_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV57TFNotaEmpenho_Qtd_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_QTD_TO",gx.O.AV57TFNotaEmpenho_Qtd_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV57TFNotaEmpenho_Qtd_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_QTD_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[121]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_SALDOANT",gxz:"ZV60TFNotaEmpenho_SaldoAnt",gxold:"OV60TFNotaEmpenho_SaldoAnt",gxvar:"AV60TFNotaEmpenho_SaldoAnt",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV60TFNotaEmpenho_SaldoAnt=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV60TFNotaEmpenho_SaldoAnt=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_SALDOANT",gx.O.AV60TFNotaEmpenho_SaldoAnt,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV60TFNotaEmpenho_SaldoAnt=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_SALDOANT",'.',',')},nac:gx.falseFn};
   GXValidFnc[122]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_SALDOANT_TO",gxz:"ZV61TFNotaEmpenho_SaldoAnt_To",gxold:"OV61TFNotaEmpenho_SaldoAnt_To",gxvar:"AV61TFNotaEmpenho_SaldoAnt_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV61TFNotaEmpenho_SaldoAnt_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV61TFNotaEmpenho_SaldoAnt_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_SALDOANT_TO",gx.O.AV61TFNotaEmpenho_SaldoAnt_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV61TFNotaEmpenho_SaldoAnt_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_SALDOANT_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[123]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_SALDOPOS",gxz:"ZV64TFNotaEmpenho_SaldoPos",gxold:"OV64TFNotaEmpenho_SaldoPos",gxvar:"AV64TFNotaEmpenho_SaldoPos",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV64TFNotaEmpenho_SaldoPos=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV64TFNotaEmpenho_SaldoPos=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_SALDOPOS",gx.O.AV64TFNotaEmpenho_SaldoPos,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV64TFNotaEmpenho_SaldoPos=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_SALDOPOS",'.',',')},nac:gx.falseFn};
   GXValidFnc[124]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_SALDOPOS_TO",gxz:"ZV65TFNotaEmpenho_SaldoPos_To",gxold:"OV65TFNotaEmpenho_SaldoPos_To",gxvar:"AV65TFNotaEmpenho_SaldoPos_To",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV65TFNotaEmpenho_SaldoPos_To=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV65TFNotaEmpenho_SaldoPos_To=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vTFNOTAEMPENHO_SALDOPOS_TO",gx.O.AV65TFNotaEmpenho_SaldoPos_To,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV65TFNotaEmpenho_SaldoPos_To=this.val()},val:function(){return gx.fn.getDecimalValue("vTFNOTAEMPENHO_SALDOPOS_TO",'.',',')},nac:gx.falseFn};
   GXValidFnc[125]={lvl:0,type:"int",len:1,dec:0,sign:false,pic:"9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[this.GridContainer],fld:"vTFNOTAEMPENHO_ATIVO_SEL",gxz:"ZV68TFNotaEmpenho_Ativo_Sel",gxold:"OV68TFNotaEmpenho_Ativo_Sel",gxvar:"AV68TFNotaEmpenho_Ativo_Sel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV68TFNotaEmpenho_Ativo_Sel=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV68TFNotaEmpenho_Ativo_Sel=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vTFNOTAEMPENHO_ATIVO_SEL",gx.O.AV68TFNotaEmpenho_Ativo_Sel,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV68TFNotaEmpenho_Ativo_Sel=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vTFNOTAEMPENHO_ATIVO_SEL",'.')},nac:gx.falseFn};
   GXValidFnc[127]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE",gxz:"ZV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace",gxold:"OV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace",gxvar:"AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE",gx.O.AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[129]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gxz:"ZV40ddo_SaldoContrato_CodigoTitleControlIdToReplace",gxold:"OV40ddo_SaldoContrato_CodigoTitleControlIdToReplace",gxvar:"AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV40ddo_SaldoContrato_CodigoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE",gx.O.AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[131]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE",gxz:"ZV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace",gxold:"OV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace",gxvar:"AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE",gx.O.AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[133]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE",gxz:"ZV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace",gxold:"OV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace",gxvar:"AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE",gx.O.AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[135]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE",gxz:"ZV54ddo_NotaEmpenho_ValorTitleControlIdToReplace",gxold:"OV54ddo_NotaEmpenho_ValorTitleControlIdToReplace",gxvar:"AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV54ddo_NotaEmpenho_ValorTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE",gx.O.AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[137]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE",gxz:"ZV58ddo_NotaEmpenho_QtdTitleControlIdToReplace",gxold:"OV58ddo_NotaEmpenho_QtdTitleControlIdToReplace",gxvar:"AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV58ddo_NotaEmpenho_QtdTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE",gx.O.AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[139]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE",gxz:"ZV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace",gxold:"OV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace",gxvar:"AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE",gx.O.AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[141]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE",gxz:"ZV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace",gxold:"OV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace",gxvar:"AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE",gx.O.AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[143]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE",gxz:"ZV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace",gxold:"OV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace",gxvar:"AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE",gx.O.AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   this.AV13OrderedBy = 0 ;
   this.ZV13OrderedBy = 0 ;
   this.OV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.ZV14OrderedDsc = false ;
   this.OV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.ZV15DynamicFiltersSelector1 = "" ;
   this.OV15DynamicFiltersSelector1 = "" ;
   this.AV16DynamicFiltersOperator1 = 0 ;
   this.ZV16DynamicFiltersOperator1 = 0 ;
   this.OV16DynamicFiltersOperator1 = 0 ;
   this.AV17SaldoContrato_Codigo1 = 0 ;
   this.ZV17SaldoContrato_Codigo1 = 0 ;
   this.OV17SaldoContrato_Codigo1 = 0 ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.ZV19DynamicFiltersSelector2 = "" ;
   this.OV19DynamicFiltersSelector2 = "" ;
   this.AV20DynamicFiltersOperator2 = 0 ;
   this.ZV20DynamicFiltersOperator2 = 0 ;
   this.OV20DynamicFiltersOperator2 = 0 ;
   this.AV21SaldoContrato_Codigo2 = 0 ;
   this.ZV21SaldoContrato_Codigo2 = 0 ;
   this.OV21SaldoContrato_Codigo2 = 0 ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.ZV23DynamicFiltersSelector3 = "" ;
   this.OV23DynamicFiltersSelector3 = "" ;
   this.AV24DynamicFiltersOperator3 = 0 ;
   this.ZV24DynamicFiltersOperator3 = 0 ;
   this.OV24DynamicFiltersOperator3 = 0 ;
   this.AV25SaldoContrato_Codigo3 = 0 ;
   this.ZV25SaldoContrato_Codigo3 = 0 ;
   this.OV25SaldoContrato_Codigo3 = 0 ;
   this.ZV28Update = "" ;
   this.OV28Update = "" ;
   this.ZV29Delete = "" ;
   this.OV29Delete = "" ;
   this.Z1560NotaEmpenho_Codigo = 0 ;
   this.O1560NotaEmpenho_Codigo = 0 ;
   this.Z1561SaldoContrato_Codigo = 0 ;
   this.O1561SaldoContrato_Codigo = 0 ;
   this.Z1564NotaEmpenho_Itentificador = "" ;
   this.O1564NotaEmpenho_Itentificador = "" ;
   this.Z1565NotaEmpenho_DEmissao = gx.date.nullDate() ;
   this.O1565NotaEmpenho_DEmissao = gx.date.nullDate() ;
   this.Z1566NotaEmpenho_Valor = 0 ;
   this.O1566NotaEmpenho_Valor = 0 ;
   this.Z1567NotaEmpenho_Qtd = 0 ;
   this.O1567NotaEmpenho_Qtd = 0 ;
   this.Z1568NotaEmpenho_SaldoAnt = 0 ;
   this.O1568NotaEmpenho_SaldoAnt = 0 ;
   this.Z1569NotaEmpenho_SaldoPos = 0 ;
   this.O1569NotaEmpenho_SaldoPos = 0 ;
   this.Z1570NotaEmpenho_Ativo = false ;
   this.O1570NotaEmpenho_Ativo = false ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.ZV18DynamicFiltersEnabled2 = false ;
   this.OV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.ZV22DynamicFiltersEnabled3 = false ;
   this.OV22DynamicFiltersEnabled3 = false ;
   this.AV34TFNotaEmpenho_Codigo = 0 ;
   this.ZV34TFNotaEmpenho_Codigo = 0 ;
   this.OV34TFNotaEmpenho_Codigo = 0 ;
   this.AV35TFNotaEmpenho_Codigo_To = 0 ;
   this.ZV35TFNotaEmpenho_Codigo_To = 0 ;
   this.OV35TFNotaEmpenho_Codigo_To = 0 ;
   this.AV38TFSaldoContrato_Codigo = 0 ;
   this.ZV38TFSaldoContrato_Codigo = 0 ;
   this.OV38TFSaldoContrato_Codigo = 0 ;
   this.AV39TFSaldoContrato_Codigo_To = 0 ;
   this.ZV39TFSaldoContrato_Codigo_To = 0 ;
   this.OV39TFSaldoContrato_Codigo_To = 0 ;
   this.AV42TFNotaEmpenho_Itentificador = "" ;
   this.ZV42TFNotaEmpenho_Itentificador = "" ;
   this.OV42TFNotaEmpenho_Itentificador = "" ;
   this.AV43TFNotaEmpenho_Itentificador_Sel = "" ;
   this.ZV43TFNotaEmpenho_Itentificador_Sel = "" ;
   this.OV43TFNotaEmpenho_Itentificador_Sel = "" ;
   this.AV46TFNotaEmpenho_DEmissao = gx.date.nullDate() ;
   this.ZV46TFNotaEmpenho_DEmissao = gx.date.nullDate() ;
   this.OV46TFNotaEmpenho_DEmissao = gx.date.nullDate() ;
   this.AV47TFNotaEmpenho_DEmissao_To = gx.date.nullDate() ;
   this.ZV47TFNotaEmpenho_DEmissao_To = gx.date.nullDate() ;
   this.OV47TFNotaEmpenho_DEmissao_To = gx.date.nullDate() ;
   this.AV48DDO_NotaEmpenho_DEmissaoAuxDate = gx.date.nullDate() ;
   this.ZV48DDO_NotaEmpenho_DEmissaoAuxDate = gx.date.nullDate() ;
   this.OV48DDO_NotaEmpenho_DEmissaoAuxDate = gx.date.nullDate() ;
   this.AV49DDO_NotaEmpenho_DEmissaoAuxDateTo = gx.date.nullDate() ;
   this.ZV49DDO_NotaEmpenho_DEmissaoAuxDateTo = gx.date.nullDate() ;
   this.OV49DDO_NotaEmpenho_DEmissaoAuxDateTo = gx.date.nullDate() ;
   this.AV52TFNotaEmpenho_Valor = 0 ;
   this.ZV52TFNotaEmpenho_Valor = 0 ;
   this.OV52TFNotaEmpenho_Valor = 0 ;
   this.AV53TFNotaEmpenho_Valor_To = 0 ;
   this.ZV53TFNotaEmpenho_Valor_To = 0 ;
   this.OV53TFNotaEmpenho_Valor_To = 0 ;
   this.AV56TFNotaEmpenho_Qtd = 0 ;
   this.ZV56TFNotaEmpenho_Qtd = 0 ;
   this.OV56TFNotaEmpenho_Qtd = 0 ;
   this.AV57TFNotaEmpenho_Qtd_To = 0 ;
   this.ZV57TFNotaEmpenho_Qtd_To = 0 ;
   this.OV57TFNotaEmpenho_Qtd_To = 0 ;
   this.AV60TFNotaEmpenho_SaldoAnt = 0 ;
   this.ZV60TFNotaEmpenho_SaldoAnt = 0 ;
   this.OV60TFNotaEmpenho_SaldoAnt = 0 ;
   this.AV61TFNotaEmpenho_SaldoAnt_To = 0 ;
   this.ZV61TFNotaEmpenho_SaldoAnt_To = 0 ;
   this.OV61TFNotaEmpenho_SaldoAnt_To = 0 ;
   this.AV64TFNotaEmpenho_SaldoPos = 0 ;
   this.ZV64TFNotaEmpenho_SaldoPos = 0 ;
   this.OV64TFNotaEmpenho_SaldoPos = 0 ;
   this.AV65TFNotaEmpenho_SaldoPos_To = 0 ;
   this.ZV65TFNotaEmpenho_SaldoPos_To = 0 ;
   this.OV65TFNotaEmpenho_SaldoPos_To = 0 ;
   this.AV68TFNotaEmpenho_Ativo_Sel = 0 ;
   this.ZV68TFNotaEmpenho_Ativo_Sel = 0 ;
   this.OV68TFNotaEmpenho_Ativo_Sel = 0 ;
   this.AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace = "" ;
   this.ZV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace = "" ;
   this.OV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace = "" ;
   this.AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.ZV40ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.OV40ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = "" ;
   this.ZV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = "" ;
   this.OV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = "" ;
   this.AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = "" ;
   this.ZV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = "" ;
   this.OV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = "" ;
   this.AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace = "" ;
   this.ZV54ddo_NotaEmpenho_ValorTitleControlIdToReplace = "" ;
   this.OV54ddo_NotaEmpenho_ValorTitleControlIdToReplace = "" ;
   this.AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace = "" ;
   this.ZV58ddo_NotaEmpenho_QtdTitleControlIdToReplace = "" ;
   this.OV58ddo_NotaEmpenho_QtdTitleControlIdToReplace = "" ;
   this.AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = "" ;
   this.ZV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = "" ;
   this.OV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = "" ;
   this.AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = "" ;
   this.ZV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = "" ;
   this.OV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = "" ;
   this.AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace = "" ;
   this.ZV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace = "" ;
   this.OV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace = "" ;
   this.AV13OrderedBy = 0 ;
   this.AV14OrderedDsc = false ;
   this.AV15DynamicFiltersSelector1 = "" ;
   this.AV16DynamicFiltersOperator1 = 0 ;
   this.AV17SaldoContrato_Codigo1 = 0 ;
   this.AV19DynamicFiltersSelector2 = "" ;
   this.AV20DynamicFiltersOperator2 = 0 ;
   this.AV21SaldoContrato_Codigo2 = 0 ;
   this.AV23DynamicFiltersSelector3 = "" ;
   this.AV24DynamicFiltersOperator3 = 0 ;
   this.AV25SaldoContrato_Codigo3 = 0 ;
   this.AV72GridCurrentPage = 0 ;
   this.AV18DynamicFiltersEnabled2 = false ;
   this.AV22DynamicFiltersEnabled3 = false ;
   this.AV34TFNotaEmpenho_Codigo = 0 ;
   this.AV35TFNotaEmpenho_Codigo_To = 0 ;
   this.AV38TFSaldoContrato_Codigo = 0 ;
   this.AV39TFSaldoContrato_Codigo_To = 0 ;
   this.AV42TFNotaEmpenho_Itentificador = "" ;
   this.AV43TFNotaEmpenho_Itentificador_Sel = "" ;
   this.AV46TFNotaEmpenho_DEmissao = gx.date.nullDate() ;
   this.AV47TFNotaEmpenho_DEmissao_To = gx.date.nullDate() ;
   this.AV48DDO_NotaEmpenho_DEmissaoAuxDate = gx.date.nullDate() ;
   this.AV49DDO_NotaEmpenho_DEmissaoAuxDateTo = gx.date.nullDate() ;
   this.AV52TFNotaEmpenho_Valor = 0 ;
   this.AV53TFNotaEmpenho_Valor_To = 0 ;
   this.AV56TFNotaEmpenho_Qtd = 0 ;
   this.AV57TFNotaEmpenho_Qtd_To = 0 ;
   this.AV60TFNotaEmpenho_SaldoAnt = 0 ;
   this.AV61TFNotaEmpenho_SaldoAnt_To = 0 ;
   this.AV64TFNotaEmpenho_SaldoPos = 0 ;
   this.AV65TFNotaEmpenho_SaldoPos_To = 0 ;
   this.AV68TFNotaEmpenho_Ativo_Sel = 0 ;
   this.AV70DDO_TitleSettingsIcons = {} ;
   this.AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace = "" ;
   this.AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace = "" ;
   this.AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace = "" ;
   this.AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace = "" ;
   this.AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace = "" ;
   this.AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace = "" ;
   this.AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace = "" ;
   this.AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace = "" ;
   this.AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace = "" ;
   this.A74Contrato_Codigo = 0 ;
   this.AV28Update = "" ;
   this.AV29Delete = "" ;
   this.A1560NotaEmpenho_Codigo = 0 ;
   this.A1561SaldoContrato_Codigo = 0 ;
   this.A1564NotaEmpenho_Itentificador = "" ;
   this.A1565NotaEmpenho_DEmissao = gx.date.nullDate() ;
   this.A1566NotaEmpenho_Valor = 0 ;
   this.A1567NotaEmpenho_Qtd = 0 ;
   this.A1568NotaEmpenho_SaldoAnt = 0 ;
   this.A1569NotaEmpenho_SaldoPos = 0 ;
   this.A1570NotaEmpenho_Ativo = false ;
   this.AV78Pgmname = "" ;
   this.AV10GridState = {} ;
   this.AV27DynamicFiltersIgnoreFirst = false ;
   this.AV26DynamicFiltersRemoving = false ;
   this.Events = {"e11m42_client": ["GRIDPAGINATIONBAR.CHANGEPAGE", true] ,"e12m42_client": ["DDO_NOTAEMPENHO_CODIGO.ONOPTIONCLICKED", true] ,"e13m42_client": ["DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED", true] ,"e14m42_client": ["DDO_NOTAEMPENHO_ITENTIFICADOR.ONOPTIONCLICKED", true] ,"e15m42_client": ["DDO_NOTAEMPENHO_DEMISSAO.ONOPTIONCLICKED", true] ,"e16m42_client": ["DDO_NOTAEMPENHO_VALOR.ONOPTIONCLICKED", true] ,"e17m42_client": ["DDO_NOTAEMPENHO_QTD.ONOPTIONCLICKED", true] ,"e18m42_client": ["DDO_NOTAEMPENHO_SALDOANT.ONOPTIONCLICKED", true] ,"e19m42_client": ["DDO_NOTAEMPENHO_SALDOPOS.ONOPTIONCLICKED", true] ,"e20m42_client": ["DDO_NOTAEMPENHO_ATIVO.ONOPTIONCLICKED", true] ,"e21m42_client": ["VORDEREDBY.CLICK", true] ,"e22m42_client": ["'REMOVEDYNAMICFILTERS1'", true] ,"e23m42_client": ["'REMOVEDYNAMICFILTERS2'", true] ,"e24m42_client": ["'REMOVEDYNAMICFILTERS3'", true] ,"e25m42_client": ["'DOCLEANFILTERS'", true] ,"e26m42_client": ["'DOINSERT'", true] ,"e27m42_client": ["'ADDDYNAMICFILTERS1'", true] ,"e28m42_client": ["VDYNAMICFILTERSSELECTOR1.CLICK", true] ,"e29m42_client": ["'ADDDYNAMICFILTERS2'", true] ,"e30m42_client": ["VDYNAMICFILTERSSELECTOR2.CLICK", true] ,"e31m42_client": ["VDYNAMICFILTERSSELECTOR3.CLICK", true] ,"e35m42_client": ["ENTER", true] ,"e36m42_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],[{av:'AV33NotaEmpenho_CodigoTitleFilterData',fld:'vNOTAEMPENHO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37SaldoContrato_CodigoTitleFilterData',fld:'vSALDOCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV41NotaEmpenho_ItentificadorTitleFilterData',fld:'vNOTAEMPENHO_ITENTIFICADORTITLEFILTERDATA',pic:'',nv:null},{av:'AV45NotaEmpenho_DEmissaoTitleFilterData',fld:'vNOTAEMPENHO_DEMISSAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV51NotaEmpenho_ValorTitleFilterData',fld:'vNOTAEMPENHO_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV55NotaEmpenho_QtdTitleFilterData',fld:'vNOTAEMPENHO_QTDTITLEFILTERDATA',pic:'',nv:null},{av:'AV59NotaEmpenho_SaldoAntTitleFilterData',fld:'vNOTAEMPENHO_SALDOANTTITLEFILTERDATA',pic:'',nv:null},{av:'AV63NotaEmpenho_SaldoPosTitleFilterData',fld:'vNOTAEMPENHO_SALDOPOSTITLEFILTERDATA',pic:'',nv:null},{av:'AV67NotaEmpenho_AtivoTitleFilterData',fld:'vNOTAEMPENHO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{ctrl:'NOTAEMPENHO_CODIGO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_CODIGO","Title")',ctrl:'NOTAEMPENHO_CODIGO',prop:'Title'},{ctrl:'SALDOCONTRATO_CODIGO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_CODIGO","Title")',ctrl:'SALDOCONTRATO_CODIGO',prop:'Title'},{ctrl:'NOTAEMPENHO_ITENTIFICADOR',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_ITENTIFICADOR","Title")',ctrl:'NOTAEMPENHO_ITENTIFICADOR',prop:'Title'},{ctrl:'NOTAEMPENHO_DEMISSAO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_DEMISSAO","Title")',ctrl:'NOTAEMPENHO_DEMISSAO',prop:'Title'},{ctrl:'NOTAEMPENHO_VALOR',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_VALOR","Title")',ctrl:'NOTAEMPENHO_VALOR',prop:'Title'},{ctrl:'NOTAEMPENHO_QTD',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_QTD","Title")',ctrl:'NOTAEMPENHO_QTD',prop:'Title'},{ctrl:'NOTAEMPENHO_SALDOANT',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_SALDOANT","Title")',ctrl:'NOTAEMPENHO_SALDOANT',prop:'Title'},{ctrl:'NOTAEMPENHO_SALDOPOS',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_SALDOPOS","Title")',ctrl:'NOTAEMPENHO_SALDOPOS',prop:'Title'},{ctrl:'NOTAEMPENHO_ATIVO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("NOTAEMPENHO_ATIVO","Title")',ctrl:'NOTAEMPENHO_ATIVO',prop:'Title'},{av:'AV72GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV73GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]];
   this.EvtParms["GRIDPAGINATIONBAR.CHANGEPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.GRIDPAGINATIONBARContainer.SelectedPage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],[]];
   this.EvtParms["DDO_NOTAEMPENHO_CODIGO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.FilteredText_get',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredText_get'},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.FilteredTextTo_get',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_SALDOCONTRATO_CODIGO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.ActiveEventKey',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredText_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_get'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredTextTo_get',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_NOTAEMPENHO_ITENTIFICADOR.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.FilteredText_get',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'FilteredText_get'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SelectedValue_get',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SelectedValue_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_NOTAEMPENHO_DEMISSAO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.FilteredText_get',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredText_get'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.FilteredTextTo_get',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_NOTAEMPENHO_VALOR.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_VALORContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.FilteredText_get',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredText_get'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.FilteredTextTo_get',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_NOTAEMPENHO_QTD.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_QTDContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.FilteredText_get',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredText_get'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.FilteredTextTo_get',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_NOTAEMPENHO_SALDOANT.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.FilteredText_get',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredText_get'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.FilteredTextTo_get',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_NOTAEMPENHO_SALDOPOS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.FilteredText_get',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredText_get'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.FilteredTextTo_get',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredTextTo_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'}]];
   this.EvtParms["DDO_NOTAEMPENHO_ATIVO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.ActiveEventKey',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'ActiveEventKey'},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SelectedValue_get',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SelectedValue_get'}],[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SortedStatus'},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.SortedStatus',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_VALORContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_QTDContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'SortedStatus'},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.SortedStatus',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'SortedStatus'}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vUPDATE","Tooltiptext")',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vUPDATE","Link")',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'gx.fn.getCtrlProperty("vDELETE","Tooltiptext")',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'gx.fn.getCtrlProperty("vDELETE","Link")',ctrl:'vDELETE',prop:'Link'},{av:'gx.fn.getCtrlProperty("SALDOCONTRATO_CODIGO","Link")',ctrl:'SALDOCONTRATO_CODIGO',prop:'Link'}]];
   this.EvtParms["VORDEREDBY.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["'ADDDYNAMICFILTERS1'"] = [[],[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS1'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO2","Visible")',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO3","Visible")',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO1","Visible")',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR1.CLICK"] = [[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO1","Visible")',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["'ADDDYNAMICFILTERS2'"] = [[],[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS2'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO2","Visible")',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO3","Visible")',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO1","Visible")',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR2.CLICK"] = [[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO2","Visible")',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'}]];
   this.EvtParms["'REMOVEDYNAMICFILTERS3'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO2","Visible")',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO3","Visible")',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO1","Visible")',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'}]];
   this.EvtParms["VDYNAMICFILTERSSELECTOR3.CLICK"] = [[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],[{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO3","Visible")',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'}]];
   this.EvtParms["'DOCLEANFILTERS'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV36ddo_NotaEmpenho_CodigoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_SaldoContrato_CodigoTitleControlIdToReplace',fld:'vDDO_SALDOCONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_NotaEmpenho_ItentificadorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ITENTIFICADORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_NotaEmpenho_DEmissaoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_DEMISSAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_NotaEmpenho_ValorTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_NotaEmpenho_QtdTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_QTDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_NotaEmpenho_SaldoAntTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOANTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_NotaEmpenho_SaldoPosTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_SALDOPOSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_NotaEmpenho_AtivoTitleControlIdToReplace',fld:'vDDO_NOTAEMPENHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV34TFNotaEmpenho_Codigo',fld:'vTFNOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.FilteredText_set',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredText_set'},{av:'AV35TFNotaEmpenho_Codigo_To',fld:'vTFNOTAEMPENHO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_NOTAEMPENHO_CODIGOContainer.FilteredTextTo_set',ctrl:'DDO_NOTAEMPENHO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV38TFSaldoContrato_Codigo',fld:'vTFSALDOCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredText_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV39TFSaldoContrato_Codigo_To',fld:'vTFSALDOCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'this.DDO_SALDOCONTRATO_CODIGOContainer.FilteredTextTo_set',ctrl:'DDO_SALDOCONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV42TFNotaEmpenho_Itentificador',fld:'vTFNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.FilteredText_set',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'FilteredText_set'},{av:'AV43TFNotaEmpenho_Itentificador_Sel',fld:'vTFNOTAEMPENHO_ITENTIFICADOR_SEL',pic:'',nv:''},{av:'this.DDO_NOTAEMPENHO_ITENTIFICADORContainer.SelectedValue_set',ctrl:'DDO_NOTAEMPENHO_ITENTIFICADOR',prop:'SelectedValue_set'},{av:'AV46TFNotaEmpenho_DEmissao',fld:'vTFNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.FilteredText_set',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredText_set'},{av:'AV47TFNotaEmpenho_DEmissao_To',fld:'vTFNOTAEMPENHO_DEMISSAO_TO',pic:'99/99/99 99:99',nv:''},{av:'this.DDO_NOTAEMPENHO_DEMISSAOContainer.FilteredTextTo_set',ctrl:'DDO_NOTAEMPENHO_DEMISSAO',prop:'FilteredTextTo_set'},{av:'AV52TFNotaEmpenho_Valor',fld:'vTFNOTAEMPENHO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_VALORContainer.FilteredText_set',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredText_set'},{av:'AV53TFNotaEmpenho_Valor_To',fld:'vTFNOTAEMPENHO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_VALORContainer.FilteredTextTo_set',ctrl:'DDO_NOTAEMPENHO_VALOR',prop:'FilteredTextTo_set'},{av:'AV56TFNotaEmpenho_Qtd',fld:'vTFNOTAEMPENHO_QTD',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'this.DDO_NOTAEMPENHO_QTDContainer.FilteredText_set',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredText_set'},{av:'AV57TFNotaEmpenho_Qtd_To',fld:'vTFNOTAEMPENHO_QTD_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'this.DDO_NOTAEMPENHO_QTDContainer.FilteredTextTo_set',ctrl:'DDO_NOTAEMPENHO_QTD',prop:'FilteredTextTo_set'},{av:'AV60TFNotaEmpenho_SaldoAnt',fld:'vTFNOTAEMPENHO_SALDOANT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.FilteredText_set',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredText_set'},{av:'AV61TFNotaEmpenho_SaldoAnt_To',fld:'vTFNOTAEMPENHO_SALDOANT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_SALDOANTContainer.FilteredTextTo_set',ctrl:'DDO_NOTAEMPENHO_SALDOANT',prop:'FilteredTextTo_set'},{av:'AV64TFNotaEmpenho_SaldoPos',fld:'vTFNOTAEMPENHO_SALDOPOS',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.FilteredText_set',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredText_set'},{av:'AV65TFNotaEmpenho_SaldoPos_To',fld:'vTFNOTAEMPENHO_SALDOPOS_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'this.DDO_NOTAEMPENHO_SALDOPOSContainer.FilteredTextTo_set',ctrl:'DDO_NOTAEMPENHO_SALDOPOS',prop:'FilteredTextTo_set'},{av:'AV68TFNotaEmpenho_Ativo_Sel',fld:'vTFNOTAEMPENHO_ATIVO_SEL',pic:'9',nv:0},{av:'this.DDO_NOTAEMPENHO_ATIVOContainer.SelectedValue_set',ctrl:'DDO_NOTAEMPENHO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17SaldoContrato_Codigo1',fld:'vSALDOCONTRATO_CODIGO1',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO1","Visible")',ctrl:'vSALDOCONTRATO_CODIGO1',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21SaldoContrato_Codigo2',fld:'vSALDOCONTRATO_CODIGO2',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25SaldoContrato_Codigo3',fld:'vSALDOCONTRATO_CODIGO3',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS1","Visible")',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS1","Visible")',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ADDDYNAMICFILTERS2","Visible")',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("REMOVEDYNAMICFILTERS2","Visible")',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'gx.fn.getCtrlProperty("JSDYNAMICFILTERS","Caption")',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO2","Visible")',ctrl:'vSALDOCONTRATO_CODIGO2',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR2'},{av:'gx.fn.getCtrlProperty("vSALDOCONTRATO_CODIGO3","Visible")',ctrl:'vSALDOCONTRATO_CODIGO3',prop:'Visible'},{ctrl:'vDYNAMICFILTERSOPERATOR3'}]];
   this.EvtParms["'DOINSERT'"] = [[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.setVCMap("AV78Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A74Contrato_Codigo", "CONTRATO_CODIGO", 0, "int");
   this.setVCMap("AV78Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV10GridState", "vGRIDSTATE", 0, "WWPBaseObjects\WWPGridState");
   this.setVCMap("AV27DynamicFiltersIgnoreFirst", "vDYNAMICFILTERSIGNOREFIRST", 0, "boolean");
   this.setVCMap("AV26DynamicFiltersRemoving", "vDYNAMICFILTERSREMOVING", 0, "boolean");
   this.setVCMap("A74Contrato_Codigo", "CONTRATO_CODIGO", 0, "int");
   GridContainer.addRefreshingVar(this.GXValidFnc[20]);
   GridContainer.addRefreshingVar(this.GXValidFnc[21]);
   GridContainer.addRefreshingVar(this.GXValidFnc[33]);
   GridContainer.addRefreshingVar(this.GXValidFnc[40]);
   GridContainer.addRefreshingVar(this.GXValidFnc[42]);
   GridContainer.addRefreshingVar(this.GXValidFnc[50]);
   GridContainer.addRefreshingVar(this.GXValidFnc[57]);
   GridContainer.addRefreshingVar(this.GXValidFnc[59]);
   GridContainer.addRefreshingVar(this.GXValidFnc[67]);
   GridContainer.addRefreshingVar(this.GXValidFnc[74]);
   GridContainer.addRefreshingVar(this.GXValidFnc[76]);
   GridContainer.addRefreshingVar(this.GXValidFnc[104]);
   GridContainer.addRefreshingVar(this.GXValidFnc[105]);
   GridContainer.addRefreshingVar(this.GXValidFnc[106]);
   GridContainer.addRefreshingVar(this.GXValidFnc[107]);
   GridContainer.addRefreshingVar(this.GXValidFnc[108]);
   GridContainer.addRefreshingVar(this.GXValidFnc[109]);
   GridContainer.addRefreshingVar(this.GXValidFnc[110]);
   GridContainer.addRefreshingVar(this.GXValidFnc[111]);
   GridContainer.addRefreshingVar(this.GXValidFnc[112]);
   GridContainer.addRefreshingVar(this.GXValidFnc[113]);
   GridContainer.addRefreshingVar(this.GXValidFnc[117]);
   GridContainer.addRefreshingVar(this.GXValidFnc[118]);
   GridContainer.addRefreshingVar(this.GXValidFnc[119]);
   GridContainer.addRefreshingVar(this.GXValidFnc[120]);
   GridContainer.addRefreshingVar(this.GXValidFnc[121]);
   GridContainer.addRefreshingVar(this.GXValidFnc[122]);
   GridContainer.addRefreshingVar(this.GXValidFnc[123]);
   GridContainer.addRefreshingVar(this.GXValidFnc[124]);
   GridContainer.addRefreshingVar(this.GXValidFnc[125]);
   GridContainer.addRefreshingVar(this.GXValidFnc[127]);
   GridContainer.addRefreshingVar(this.GXValidFnc[129]);
   GridContainer.addRefreshingVar(this.GXValidFnc[131]);
   GridContainer.addRefreshingVar(this.GXValidFnc[133]);
   GridContainer.addRefreshingVar(this.GXValidFnc[135]);
   GridContainer.addRefreshingVar(this.GXValidFnc[137]);
   GridContainer.addRefreshingVar(this.GXValidFnc[139]);
   GridContainer.addRefreshingVar(this.GXValidFnc[141]);
   GridContainer.addRefreshingVar(this.GXValidFnc[143]);
   GridContainer.addRefreshingVar({rfrVar:"AV78Pgmname"});
   GridContainer.addRefreshingVar({rfrVar:"AV10GridState"});
   GridContainer.addRefreshingVar({rfrVar:"AV27DynamicFiltersIgnoreFirst"});
   GridContainer.addRefreshingVar({rfrVar:"AV26DynamicFiltersRemoving"});
   GridContainer.addRefreshingVar({rfrVar:"A1560NotaEmpenho_Codigo", rfrProp:"Value", gxAttId:"1560"});
   GridContainer.addRefreshingVar({rfrVar:"A74Contrato_Codigo"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wwnotaempenho);
