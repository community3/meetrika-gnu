/*
               File: ContratoServicosCustoGeneral
        Description: Contrato Servicos Custo General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:37.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicoscustogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicoscustogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicoscustogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosCusto_Codigo )
      {
         this.A1473ContratoServicosCusto_Codigo = aP0_ContratoServicosCusto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1473ContratoServicosCusto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1473ContratoServicosCusto_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAL32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ContratoServicosCustoGeneral";
               context.Gx_err = 0;
               WSL32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Custo General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117283728");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicoscustogeneral.aspx") + "?" + UrlEncode("" +A1473ContratoServicosCusto_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1473ContratoServicosCusto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSCUSTO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1472ContratoServicosCusto_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosCustoGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicoscustogeneral:[SendSecurityCheck value for]"+"ContratoServicosCusto_CntSrvCod:"+context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormL32( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicoscustogeneral.js", "?20203117283730");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosCustoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Custo General" ;
      }

      protected void WBL30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicoscustogeneral.aspx");
            }
            wb_table1_2_L32( true) ;
         }
         else
         {
            wb_table1_2_L32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_L32e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTL32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Custo General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPL30( ) ;
            }
         }
      }

      protected void WSL32( )
      {
         STARTL32( ) ;
         EVTL32( ) ;
      }

      protected void EVTL32( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11L32 */
                                    E11L32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12L32 */
                                    E12L32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13L32 */
                                    E13L32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14L32 */
                                    E14L32 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPL30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEL32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormL32( ) ;
            }
         }
      }

      protected void PAL32( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFL32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ContratoServicosCustoGeneral";
         context.Gx_err = 0;
      }

      protected void RFL32( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00L32 */
            pr_default.execute(0, new Object[] {A1473ContratoServicosCusto_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1475ContratoServicosCusto_CstUntPrdExt = H00L32_A1475ContratoServicosCusto_CstUntPrdExt[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n1475ContratoServicosCusto_CstUntPrdExt = H00L32_n1475ContratoServicosCusto_CstUntPrdExt[0];
               A1474ContratoServicosCusto_CstUntPrdNrm = H00L32_A1474ContratoServicosCusto_CstUntPrdNrm[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A1472ContratoServicosCusto_UsuarioCod = H00L32_A1472ContratoServicosCusto_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1472ContratoServicosCusto_UsuarioCod), "ZZZZZ9")));
               A1471ContratoServicosCusto_CntSrvCod = H00L32_A1471ContratoServicosCusto_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9")));
               /* Execute user event: E12L32 */
               E12L32 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBL30( ) ;
         }
      }

      protected void STRUPL30( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ContratoServicosCustoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11L32 */
         E11L32 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1471ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9")));
            A1472ContratoServicosCusto_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_UsuarioCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1472ContratoServicosCusto_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1472ContratoServicosCusto_UsuarioCod), "ZZZZZ9")));
            A1474ContratoServicosCusto_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdNrm_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1474ContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A1475ContratoServicosCusto_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdExt_Internalname), ",", ".");
            n1475ContratoServicosCusto_CstUntPrdExt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1475ContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            /* Read saved values. */
            wcpOA1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1473ContratoServicosCusto_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoServicosCustoGeneral";
            A1471ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSCUSTO_CNTSRVCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratoservicoscustogeneral:[SecurityCheckFailed value for]"+"ContratoServicosCusto_CntSrvCod:"+context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11L32 */
         E11L32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11L32( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12L32( )
      {
         /* Load Routine */
         edtContratoServicosCusto_CntSrvCod_Link = formatLink("viewcontratoservicos.aspx") + "?" + UrlEncode("" +A1471ContratoServicosCusto_CntSrvCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosCusto_CntSrvCod_Internalname, "Link", edtContratoServicosCusto_CntSrvCod_Link);
      }

      protected void E13L32( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoservicoscusto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1473ContratoServicosCusto_Codigo) + "," + UrlEncode("" +AV12ContratoServicosCusto_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E14L32( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoservicoscusto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1473ContratoServicosCusto_Codigo) + "," + UrlEncode("" +AV12ContratoServicosCusto_CntSrvCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosCusto";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoServicosCusto_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosCusto_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_L32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_L32( true) ;
         }
         else
         {
            wb_table2_8_L32( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_L32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_L32( true) ;
         }
         else
         {
            wb_table3_36_L32( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_L32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_L32e( true) ;
         }
         else
         {
            wb_table1_2_L32e( false) ;
         }
      }

      protected void wb_table3_36_L32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_L32e( true) ;
         }
         else
         {
            wb_table3_36_L32e( false) ;
         }
      }

      protected void wb_table2_8_L32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_codigo_Internalname, "Servicos Custo_Codigo", "", "", lblTextblockcontratoservicoscusto_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_cntsrvcod_Internalname, "Srv Cod", "", "", lblTextblockcontratoservicoscusto_cntsrvcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratoServicosCusto_CntSrvCod_Link, "", "", "", edtContratoServicosCusto_CntSrvCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_usuariocod_Internalname, "Custo_Usuario Cod", "", "", lblTextblockcontratoservicoscusto_usuariocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1472ContratoServicosCusto_UsuarioCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1472ContratoServicosCusto_UsuarioCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_UsuarioCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_cstuntprdnrm_Internalname, "R$", "", "", lblTextblockcontratoservicoscusto_cstuntprdnrm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_CstUntPrdNrm_Internalname, StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")), context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_CstUntPrdNrm_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicoscusto_cstuntprdext_Internalname, " R$", "", "", lblTextblockcontratoservicoscusto_cstuntprdext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_CstUntPrdExt_Internalname, StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")), context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_CstUntPrdExt_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoServicosCustoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_L32e( true) ;
         }
         else
         {
            wb_table2_8_L32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1473ContratoServicosCusto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAL32( ) ;
         WSL32( ) ;
         WEL32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1473ContratoServicosCusto_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAL32( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicoscustogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAL32( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1473ContratoServicosCusto_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
         }
         wcpOA1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1473ContratoServicosCusto_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1473ContratoServicosCusto_Codigo != wcpOA1473ContratoServicosCusto_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1473ContratoServicosCusto_Codigo = A1473ContratoServicosCusto_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1473ContratoServicosCusto_Codigo = cgiGet( sPrefix+"A1473ContratoServicosCusto_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1473ContratoServicosCusto_Codigo) > 0 )
         {
            A1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1473ContratoServicosCusto_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1473ContratoServicosCusto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0)));
         }
         else
         {
            A1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1473ContratoServicosCusto_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAL32( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSL32( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSL32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1473ContratoServicosCusto_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1473ContratoServicosCusto_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1473ContratoServicosCusto_Codigo_CTRL", StringUtil.RTrim( sCtrlA1473ContratoServicosCusto_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEL32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117283752");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicoscustogeneral.js", "?20203117283753");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicoscusto_codigo_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSCUSTO_CODIGO";
         edtContratoServicosCusto_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CODIGO";
         lblTextblockcontratoservicoscusto_cntsrvcod_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSCUSTO_CNTSRVCOD";
         edtContratoServicosCusto_CntSrvCod_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CNTSRVCOD";
         lblTextblockcontratoservicoscusto_usuariocod_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSCUSTO_USUARIOCOD";
         edtContratoServicosCusto_UsuarioCod_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_USUARIOCOD";
         lblTextblockcontratoservicoscusto_cstuntprdnrm_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         lblTextblockcontratoservicoscusto_cstuntprdext_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         edtContratoServicosCusto_CstUntPrdExt_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosCusto_CstUntPrdExt_Jsonclick = "";
         edtContratoServicosCusto_CstUntPrdNrm_Jsonclick = "";
         edtContratoServicosCusto_UsuarioCod_Jsonclick = "";
         edtContratoServicosCusto_CntSrvCod_Jsonclick = "";
         edtContratoServicosCusto_Codigo_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratoServicosCusto_CntSrvCod_Link = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13L32',iparms:[{av:'A1473ContratoServicosCusto_Codigo',fld:'CONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14L32',iparms:[{av:'A1473ContratoServicosCusto_Codigo',fld:'CONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00L32_A1473ContratoServicosCusto_Codigo = new int[1] ;
         H00L32_A1475ContratoServicosCusto_CstUntPrdExt = new decimal[1] ;
         H00L32_n1475ContratoServicosCusto_CstUntPrdExt = new bool[] {false} ;
         H00L32_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         H00L32_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         H00L32_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratoservicoscusto_codigo_Jsonclick = "";
         lblTextblockcontratoservicoscusto_cntsrvcod_Jsonclick = "";
         lblTextblockcontratoservicoscusto_usuariocod_Jsonclick = "";
         lblTextblockcontratoservicoscusto_cstuntprdnrm_Jsonclick = "";
         lblTextblockcontratoservicoscusto_cstuntprdext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1473ContratoServicosCusto_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicoscustogeneral__default(),
            new Object[][] {
                new Object[] {
               H00L32_A1473ContratoServicosCusto_Codigo, H00L32_A1475ContratoServicosCusto_CstUntPrdExt, H00L32_n1475ContratoServicosCusto_CstUntPrdExt, H00L32_A1474ContratoServicosCusto_CstUntPrdNrm, H00L32_A1472ContratoServicosCusto_UsuarioCod, H00L32_A1471ContratoServicosCusto_CntSrvCod
               }
            }
         );
         AV15Pgmname = "ContratoServicosCustoGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ContratoServicosCustoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1473ContratoServicosCusto_Codigo ;
      private int wcpOA1473ContratoServicosCusto_Codigo ;
      private int AV12ContratoServicosCusto_CntSrvCod ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int A1472ContratoServicosCusto_UsuarioCod ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoServicosCusto_Codigo ;
      private int idxLst ;
      private decimal A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal A1475ContratoServicosCusto_CstUntPrdExt ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtContratoServicosCusto_CntSrvCod_Internalname ;
      private String edtContratoServicosCusto_UsuarioCod_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdExt_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContratoServicosCusto_CntSrvCod_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicoscusto_codigo_Internalname ;
      private String lblTextblockcontratoservicoscusto_codigo_Jsonclick ;
      private String edtContratoServicosCusto_Codigo_Internalname ;
      private String edtContratoServicosCusto_Codigo_Jsonclick ;
      private String lblTextblockcontratoservicoscusto_cntsrvcod_Internalname ;
      private String lblTextblockcontratoservicoscusto_cntsrvcod_Jsonclick ;
      private String edtContratoServicosCusto_CntSrvCod_Jsonclick ;
      private String lblTextblockcontratoservicoscusto_usuariocod_Internalname ;
      private String lblTextblockcontratoservicoscusto_usuariocod_Jsonclick ;
      private String edtContratoServicosCusto_UsuarioCod_Jsonclick ;
      private String lblTextblockcontratoservicoscusto_cstuntprdnrm_Internalname ;
      private String lblTextblockcontratoservicoscusto_cstuntprdnrm_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Jsonclick ;
      private String lblTextblockcontratoservicoscusto_cstuntprdext_Internalname ;
      private String lblTextblockcontratoservicoscusto_cstuntprdext_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdExt_Jsonclick ;
      private String sCtrlA1473ContratoServicosCusto_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1475ContratoServicosCusto_CstUntPrdExt ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00L32_A1473ContratoServicosCusto_Codigo ;
      private decimal[] H00L32_A1475ContratoServicosCusto_CstUntPrdExt ;
      private bool[] H00L32_n1475ContratoServicosCusto_CstUntPrdExt ;
      private decimal[] H00L32_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private int[] H00L32_A1472ContratoServicosCusto_UsuarioCod ;
      private int[] H00L32_A1471ContratoServicosCusto_CntSrvCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratoservicoscustogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00L32 ;
          prmH00L32 = new Object[] {
          new Object[] {"@ContratoServicosCusto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00L32", "SELECT [ContratoServicosCusto_Codigo], [ContratoServicosCusto_CstUntPrdExt], [ContratoServicosCusto_CstUntPrdNrm], [ContratoServicosCusto_UsuarioCod], [ContratoServicosCusto_CntSrvCod] FROM [ContratoServicosCusto] WITH (NOLOCK) WHERE [ContratoServicosCusto_Codigo] = @ContratoServicosCusto_Codigo ORDER BY [ContratoServicosCusto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00L32,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
